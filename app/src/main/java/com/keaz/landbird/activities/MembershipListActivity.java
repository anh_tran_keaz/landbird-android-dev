package com.keaz.landbird.activities;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.keaz.landbird.R;
import com.keaz.landbird.models.KZMembership;
import com.keaz.landbird.utils.BKNetworkResponseError;
import com.keaz.landbird.utils.CommonUtils;
import com.keaz.landbird.utils.JsonUtils;
import com.keaz.landbird.utils.OnResponseModel;
import com.keaz.landbird.utils.VolleyUtils;
import com.segment.analytics.Analytics;

import java.util.ArrayList;

/**
 * Created by anhtran1810 on 1/11/18.
 */

public class MembershipListActivity extends BaseActivity {

    private MembershipAdapter mAdapter;

    private ArrayList<KZMembership> mMembershipList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_membership_list);

        mMembershipList = new ArrayList<>();

        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        mAdapter = new MembershipAdapter();
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    protected void onStart() {
        super.onStart();

        Analytics.with(this).screen("Memebership List");

        loadMembershipList();
    }

    public void onHeaderBackPress(View view) {
        onBackPressed();
    }

    private void loadMembershipList() {
        mMembershipList.clear();

        showCustomDialogLoading();
        VolleyUtils.getSharedNetwork().loadMembershipList(MembershipListActivity.this,
                new OnResponseModel<JsonArray>() {
            @Override
            public void onResponseSuccess(JsonArray array) {
                dismissCustomDialogLoading();
                for (int i = 0; i < array.size(); i++) {
                    String s = array.get(i).toString();
                    KZMembership membership = JsonUtils.parseByModel(s, KZMembership.class);
                    membership.isApply = false;
                    mMembershipList.add(membership);
                }

                loadMembershipListOfUser();
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                dismissCustomDialogLoading();
                VolleyUtils.handleErrorRespond(MembershipListActivity.this, error);
            }
        });
    }

    private void loadMembershipListOfUser() {
        VolleyUtils.getSharedNetwork().loadMembershipListOfUser(MembershipListActivity.this,
                new OnResponseModel<JsonArray>() {
            @Override
            public void onResponseSuccess(JsonArray array) {
                dismissCustomDialogLoading();

                for (int i = 0; i < array.size(); i++) {
                    Gson gson = new Gson();
                    JsonElement s = array.get(i);
                    KZMembership membershipOfUser = gson.fromJson(s , KZMembership.class);

                    for (int j = 0; j < mMembershipList.size(); j++) {
                        KZMembership membership = mMembershipList.get(j);
                        if (membership.id.equals(membershipOfUser.id)) {
                            membership.isApply = true;
                            mMembershipList.set(j, membership);
                            break;
                        }
                    }
                }
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                dismissCustomDialogLoading();
                VolleyUtils.handleErrorRespond(MembershipListActivity.this, error);
            }
        });
    }

    class MembershipAdapter extends RecyclerView.Adapter<MembershipAdapter.MyViewHolder> {

        class MyViewHolder extends RecyclerView.ViewHolder {

            RelativeLayout relRoot;
            TextView txtName;

            MyViewHolder(View view) {
                super(view);

                relRoot = (RelativeLayout) view.findViewById(R.id.rel_root);
                txtName = (TextView) view.findViewById(R.id.tv_title);
            }
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_account_list, parent, false);

            return new MyViewHolder(itemView);
        }

        @RequiresApi(api = Build.VERSION_CODES.M)
        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
            final KZMembership membership = mMembershipList.get(position);

            holder.txtName.setText(membership.name);
            if (membership.isApply) {
                holder.txtName.setTextColor(getColor(R.color.text_apply));
            } else {
                holder.txtName.setTextColor(getColor(R.color.text_not_apply));
            }

            holder.relRoot.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    CommonUtils.segmentTrackWithMembership(MembershipListActivity.this, "Choose a membership", membership);

                    Intent intent = new Intent(MembershipListActivity.this, MembershipDetailActivity.class);
                    intent.putExtra("membership", membership);
                    MembershipListActivity.this.startActivity(intent);
                }
            });
        }

        @Override
        public int getItemCount() {
            return mMembershipList.size();
        }
    }
}
