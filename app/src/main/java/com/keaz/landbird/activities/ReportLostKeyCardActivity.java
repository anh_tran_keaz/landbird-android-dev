package com.keaz.landbird.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;

import com.keaz.landbird.R;
import com.keaz.landbird.utils.BKNetworkResponseError;
import com.keaz.landbird.utils.DialogUtils;
import com.keaz.landbird.utils.OnResponseModel;
import com.keaz.landbird.utils.VolleyUtils;
import com.segment.analytics.Analytics;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by anhtran1810 on 3/2/18.
 */

public class ReportLostKeyCardActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_report_lost_keycard);

        findViewById(R.id.btnSubmitRegisterKeyCard).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Analytics.with(ReportLostKeyCardActivity.this).track("Click submit_report_lost_key_card_btn");

                DialogUtils.showCustomDialog(ReportLostKeyCardActivity.this,
                        true,
                        getString(R.string.confirm_report_lost_card),
                        "",
                        getString(R.string.btn_yes),
                        getString(R.string.btn_no),
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                actionSubmitLostKeyCard();
                            }
                        },
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                            }
                        });
            }
        });
        findViewById(R.id.img_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        Analytics.with(this).screen("Report Lost GO pass");
    }

    private void actionSubmitLostKeyCard() {
        String content = ((EditText)findViewById(R.id.edt_report)).getText().toString();
        showCustomDialogLoading();

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("subject", "Report Lose GO pass");
            jsonObject.put("content", content);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        VolleyUtils.getSharedNetwork().reportLostKeyCard(this, jsonObject, new OnResponseModel() {
            @Override
            public void onResponseSuccess(Object model) {
                dismissCustomDialogLoading();
                DialogUtils.showCustomDialog(ReportLostKeyCardActivity.this, true, "We receive your report. Please check your email for further instruction", "", "OK", "",
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                finish();
                            }
                        },null);
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                dismissCustomDialogLoading();
                VolleyUtils.handleErrorRespond(ReportLostKeyCardActivity.this, error);
            }
        });

    }
}
