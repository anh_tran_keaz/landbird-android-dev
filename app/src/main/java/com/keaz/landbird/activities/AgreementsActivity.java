package com.keaz.landbird.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.keaz.landbird.R;
import com.keaz.landbird.view.CustomFontTextView;
import com.segment.analytics.Analytics;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class AgreementsActivity extends BaseActivity {


    public static final int TERMS_OF_SERVICE = 0;
    public static final int MEMBERSHIP_CONTRACT = 1;
    public static final int PRIVACY_POLICY = 2;
    public static final int FEES_TO_KNOW_ABOUT = 3;

    private ListView listView;
    private SimpleArrayAdapter adapter;
    private String[] dataList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agreements);


        initUI();
        initData();
        initListener();
    }
    @Override
    protected void onStart() {
        super.onStart();
        Analytics.with(this).screen("Aggreements");
    }

    public void initUI() {
        listView = (ListView) findViewById(R.id.list_agreements);
    }

    public void initData() {
        List<String> temp = new ArrayList<>();
        temp.add("Terms Of Service");
        temp.add("Membership Contract");
        temp.add("Privacy Policy");
        temp.add("Fees To Know About");

        dataList = new String[temp.size()];
        dataList = temp.toArray(dataList);

        adapter = new SimpleArrayAdapter(this, dataList);

        listView.setAdapter(adapter);
    }

    public void initListener() {
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                switch (position) {
                    case TERMS_OF_SERVICE:
                        startActivityWithName(TermsOfService2Activity.class);
                        break;

                    case MEMBERSHIP_CONTRACT:
                        startActivityWithName(MembershipActivity.class);
                        break;

                    case PRIVACY_POLICY:
                        startActivityWithName(PrivacyPolicyActivity.class);
                        break;

                    case FEES_TO_KNOW_ABOUT:
                        startActivityWithName(FeesToKnowActivity.class);
                        break;

                    default:
                        break;
                }
            }
        });
    }

    public void startActivityWithName(Class<?> tClass) {
        startActivity(new Intent(this, tClass));
    }

    public void onHeaderBackPress(View view) {
        onBackPressed();
    }

    private static class SimpleArrayAdapter extends ArrayAdapter<String> {
        private final Context context;
        private final String[] values;

        SimpleArrayAdapter(Context context, String[] values) {
            super(context, -1, values);
            this.context = context;
            this.values = values;
        }

        @Override
        public int getCount() {
            return values != null ? values.length : 0;
        }

        @NotNull
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.item_account_list, parent, false);

            CustomFontTextView textView = rowView.findViewById(R.id.tv_title);

            textView.setText(values[position]);

            return rowView;
        }
    }
}
