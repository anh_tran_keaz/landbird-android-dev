package com.keaz.landbird.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.keaz.landbird.R;
import com.keaz.landbird.fragments.BookingListFragment;
import com.keaz.landbird.utils.ConstantsUtils;
import com.segment.analytics.Analytics;

import java.util.ArrayList;
import java.util.List;

public class BookingListActivity extends BaseActivity {

    private TabLayout tabLayout;
    private String[] titles = {"Upcoming Bookings", "Past Bookings"};
    private int mSelectedTab;
    private BookingListFragment upcomingBookingListFragment;
    private BookingListFragment pastBookingListFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_list);

        String openFrom = getIntent().getStringExtra(ConstantsUtils.INTENT_EXTRA_OPEN_BOOKING_LIST_FROM);
        mSelectedTab = 0;

        iniListener();
        initViewPager();
    }
    @Override
    protected void onStart() {
        super.onStart();

        showFragmentWithTab();
    }
    @Override
    public void onBackPressed() {
        if(mSelectedTab==0){
            if(upcomingBookingListFragment.isVisible()) {
                upcomingBookingListFragment.onBackPressed();
            }else {
                goBackToHome();
            }
        } else if(mSelectedTab==1){
            if(pastBookingListFragment.isVisible()) {
                pastBookingListFragment.onBackPressed();
            }else {
                goBackToHome();
            }
        }
    }

    public void goBackToHome() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        this.finish();
    }

    private void iniListener() {
        findViewById(R.id.rel_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BookingListActivity.this.onBackPressed();
            }
        });
        findViewById(R.id.icMenu).setVisibility(View.GONE);
    }

    private void initViewPager() {
        ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        tabLayout = (TabLayout) findViewById(R.id.tabs);

        //Upcoming fragment
        upcomingBookingListFragment = new BookingListFragment();
        Bundle upcomingBundle = new Bundle();
        upcomingBundle.putBoolean("isUpcoming", true);
        upcomingBookingListFragment.setArguments(upcomingBundle);

        //Past bookings fragment
        pastBookingListFragment = new BookingListFragment();
        Bundle pastBookingBundle = new Bundle();
        pastBookingBundle.putBoolean("isUpcoming", false);
        pastBookingListFragment.setArguments(pastBookingBundle);

        HomePagerAdapter adapter = new HomePagerAdapter(getSupportFragmentManager());
        adapter.addFrag(upcomingBookingListFragment, titles[0]);
        adapter.addFrag(pastBookingListFragment, titles[1]);
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);

        setUpTabIcon();
    }

    private void showFragmentWithTab() {
        switch (mSelectedTab) {
            case 0:
                Analytics.with(BookingListActivity.this).screen("Upcoming Bookings");
                break;

            case 1:
                Analytics.with(BookingListActivity.this).screen("Past Bookings");
                break;
        }
    }

    private void setUpTabIcon() {
        LayoutInflater inflater = (LayoutInflater) getBaseContext().getSystemService(LAYOUT_INFLATER_SERVICE);
        View tab1 = inflater.inflate(R.layout.custom_tab, null);
        TextView textView = (TextView) tab1.findViewById(R.id.txtText);
        //textView.setTextColor(Color.rgb(252, 86, 78));
        textView.setTextColor(ConstantsUtils.COLOR_FOCUS);
        textView.setTypeface(null, Typeface.BOLD);
        ImageView imageView = (ImageView) tab1.findViewById(R.id.imvIcon);
        textView.setText(titles[0]);
        imageView.setBackgroundResource(R.drawable.car_gray);

        View tab2 = inflater.inflate(R.layout.custom_tab, null);
        TextView textView2 = (TextView) tab2.findViewById(R.id.txtText);
        textView2.setTextColor(ConstantsUtils.COLOR_UNFOCUS);
        textView2.setTypeface(null, Typeface.NORMAL);
        ImageView imageView2 = (ImageView) tab2.findViewById(R.id.imvIcon);
        textView2.setText(titles[1]);
        imageView2.setBackgroundResource(R.drawable.dashboard);

        tabLayout.getTabAt(0).setCustomView(tab1);
        tabLayout.getTabAt(1).setCustomView(tab2);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                View view = tab.getCustomView();
                TextView text = (TextView) view.findViewById(R.id.txtText);
                text.setTextColor(ConstantsUtils.COLOR_FOCUS);
                text.setTypeface(null, Typeface.BOLD);
                ImageView image = (ImageView) view.findViewById(R.id.imvIcon);

                mSelectedTab = tab.getPosition();

                // Segment
                showFragmentWithTab();

                switch (mSelectedTab) {
                    case 0:
                        image.setBackgroundResource(R.drawable.car_gray);
                        break;

                    case 1:
                        image.setBackgroundResource(R.drawable.dashboard);
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                View view = tab.getCustomView();
                TextView text = (TextView) view.findViewById(R.id.txtText);
                text.setTextColor(ConstantsUtils.COLOR_UNFOCUS);
                text.setTypeface(null, Typeface.NORMAL);
                ImageView image = (ImageView) view.findViewById(R.id.imvIcon);
                int pos = tab.getPosition();
                switch (pos) {
                    case 0:
                        image.setBackgroundResource(R.drawable.car_gray);
                        break;
                    case 1:
                        image.setBackgroundResource(R.drawable.dashboard);
                        break;
                }
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private class HomePagerAdapter extends FragmentStatePagerAdapter {

        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        HomePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
