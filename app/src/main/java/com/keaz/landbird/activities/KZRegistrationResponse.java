package com.keaz.landbird.activities;

import com.google.gson.annotations.SerializedName;
import com.keaz.landbird.utils.BKNetworkResponse;

/**
 * Created by mac on 2/21/17.
 */
public class KZRegistrationResponse extends BKNetworkResponse {
    @SerializedName("id")
    public String id;

    @SerializedName("email")
    public String email;

    @SerializedName("phone")
    public String phone;

    @SerializedName("verify")
    public String verify;

    @SerializedName("checked_terms_condition")
    public boolean checked_terms_condition;
}
