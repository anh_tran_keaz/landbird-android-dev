package com.keaz.landbird.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.keaz.landbird.R;
import com.keaz.landbird.view.CustomFontTextView;

public class MembershipActivity extends BaseActivity {


    private CustomFontTextView tvContract;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_membership);


        tvContract = (CustomFontTextView) findViewById(R.id.tv_contract);
    }


    public void onHeaderBackPress(View view) {
        onBackPressed();
    }


    public void onAdditionalClick(View view) {
        startActivity(new Intent(this, AdditionalActivity.class));
    }
}
