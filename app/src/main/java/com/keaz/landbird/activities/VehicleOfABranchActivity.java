package com.keaz.landbird.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

import com.keaz.landbird.R;
import com.keaz.landbird.fragments.VehicleOfBranchFragment;
import com.keaz.landbird.models.KZBranch;
import com.keaz.landbird.models.KZObjectTimeLine;
import com.keaz.landbird.models.KZSearchResult;
import com.keaz.landbird.models.KZVehicle;
import com.keaz.landbird.utils.CommonUtils;

import java.util.ArrayList;

public class VehicleOfABranchActivity extends BaseActivity {

    private KZBranch branch;
    private VehicleOfBranchFragment vehicleOfBranchFragment;
    private static final int TODAY  = 0 ;
    private KZSearchResult kzSearchResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vehicle_of_branch);
        if (getIntent() != null) {
            branch = getIntent().getParcelableExtra("branch");
        }
        if(vehicleOfBranchFragment == null) {
            vehicleOfBranchFragment = VehicleOfBranchFragment.newInstance(branch, kzSearchResult, TODAY,new ArrayList<KZVehicle>(),new ArrayList<KZObjectTimeLine>(),true);
        }
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.frame_vehicle_list, vehicleOfBranchFragment);
        transaction.commit();
    }
    @Override
    protected void onStart() {
        super.onStart();
        CommonUtils.segmentTrackWithBranch(this, "Branch Detail", branch);
    }
    @Override
    public void onBackPressed() {
        actionBack();
    }

    private void actionBack() {
        if(vehicleOfBranchFragment.isVisible()) {
            vehicleOfBranchFragment.onBackPressed();
        } else {
            VehicleOfABranchActivity.this.onBackPressed();
        }
    }

}
