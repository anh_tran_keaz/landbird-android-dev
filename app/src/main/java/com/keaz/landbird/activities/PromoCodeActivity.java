package com.keaz.landbird.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.keaz.landbird.R;
import com.keaz.landbird.models.KZPromo;
import com.keaz.landbird.utils.BKNetworkResponseError;
import com.keaz.landbird.utils.ConfigUtil;
import com.keaz.landbird.utils.DateUtils;
import com.keaz.landbird.utils.FormatUtil;
import com.keaz.landbird.utils.OnResponseModel;
import com.keaz.landbird.utils.VolleyUtils;
import com.keaz.landbird.view.CustomEditText;
import com.keaz.landbird.view.CustomFontButton;
import com.keaz.landbird.view.CustomFontTextView;
import com.segment.analytics.Analytics;
import com.segment.analytics.Properties;

import java.util.ArrayList;
import java.util.Date;

public class PromoCodeActivity extends BaseActivity implements View.OnClickListener {

    private CustomFontTextView mTxtCurrentBalance;
    private CustomEditText mExtPromoCode;
    private PromoCodeAdapter mAdapter;

    private ArrayList<KZPromo> mPromoList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_promo_code);

        mTxtCurrentBalance = (CustomFontTextView) findViewById(R.id.txt_current_balance);
        mExtPromoCode = (CustomEditText) findViewById(R.id.ext_promo_code);

        CustomFontButton mBtnApply = (CustomFontButton) findViewById(R.id.btn_apply);
        mBtnApply.setOnClickListener(this);

        mPromoList = new ArrayList<>();
        mAdapter = new PromoCodeAdapter(mPromoList);

        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view_promo_code);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mAdapter);

        loadPromoList();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Analytics.with(this).screen("Promos");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_apply:
                Analytics.with(PromoCodeActivity.this).track("Click apply_btn");
                applyPromo();
                break;
        }
    }

    private void loadPromoList() {
        showCustomDialogLoading();
        VolleyUtils.getSharedNetwork().loadPromoList(
                this,
                new OnResponseModel<JsonObject>() {
            @Override
            public void onResponseSuccess(JsonObject object) {
                dismissCustomDialogLoading();

                if (object != null) {
                    Gson gson = new Gson();

                    mTxtCurrentBalance.setText("$" + FormatUtil.formatString(object.get("total").getAsString()));

                    JsonArray array = object.get("promotions").getAsJsonArray();

                    mPromoList.clear();
                    for (int i = 0; i < array.size(); i++) {
                        KZPromo promo = gson.fromJson(array.get(i), KZPromo.class);
                        mPromoList.add(promo);
                    }
                }
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                dismissCustomDialogLoading();
                VolleyUtils.handleErrorRespond(PromoCodeActivity.this, error);
            }
        });
    }

    private void applyPromo() {
        final String promoCode = mExtPromoCode.getText().toString();
        if (promoCode == null || promoCode.equals("")) {
            Toast.makeText(this, "You must input promo code", Toast.LENGTH_SHORT).show();
            return;
        }

        showCustomDialogLoading();
        VolleyUtils.getSharedNetwork().applyPromo(this, promoCode, new OnResponseModel<JsonObject>() {
            @Override
            public void onResponseSuccess(JsonObject object) {
                dismissCustomDialogLoading();

                Analytics.with(PromoCodeActivity.this).track("Apply Promo successfully", new Properties().putValue("promo_code",promoCode));

                Toast.makeText(PromoCodeActivity.this, getString(R.string.text_success), Toast.LENGTH_SHORT).show();

                loadPromoList();
                mExtPromoCode.setText("");
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                dismissCustomDialogLoading();
                VolleyUtils.handleErrorRespond(PromoCodeActivity.this, error);
            }
        });
    }

    private class PromoCodeAdapter extends RecyclerView.Adapter<PromoCodeAdapter.MyViewHolder> {

        private ArrayList<KZPromo> promoList;

        public class MyViewHolder extends RecyclerView.ViewHolder {

            public CustomFontTextView txtCode;
            public CustomFontTextView txtCost;
            public CustomFontTextView txtAvailableCost;
            public CustomFontTextView txtExpireDate;

            public MyViewHolder(View view) {
                super(view);
                txtCode = (CustomFontTextView) view.findViewById(R.id.txt_code);
                txtCost = (CustomFontTextView) view.findViewById(R.id.txt_cost);
                txtAvailableCost = (CustomFontTextView) view.findViewById(R.id.txt_avail_cost);
                txtExpireDate = (CustomFontTextView) view.findViewById(R.id.txt_expire_date);
            }
        }

        public PromoCodeAdapter(ArrayList<KZPromo> promoList) {
            this.promoList = promoList;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_promo_list, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
            KZPromo promo = promoList.get(position);

            holder.txtCode.setText(promo.code);
            holder.txtCost.setText("$" + FormatUtil.formatString(promo.cost));
            holder.txtAvailableCost.setText("$" + FormatUtil.formatString(promo.available_cost));

            if (promo.expire_date != null && !promo.expire_date.equals("")) {
                int timeZoneOffset = DateUtils.getTimeZoneOffset();
                int promoTimeZoneOffset = ConfigUtil.getTimeZoneOffset();

                //String expireDateStr = DateUtils.getTimeFromSec(Long.parseLong(promo.expire_date) - timeZoneOffset - promoTimZoneOffset);
                Date date = DateUtils.getDateWithMili(Long.parseLong(promo.expire_date)*1000 - timeZoneOffset - promoTimeZoneOffset);
                String expireDateStr = DateUtils.formatDatePattern(date, "yyyy-MM-dd");

                holder.txtExpireDate.setText(expireDateStr);
            } else {
                holder.txtExpireDate.setText("");
            }
        }

        @Override
        public int getItemCount() {
            return promoList.size();
        }
    }
}
