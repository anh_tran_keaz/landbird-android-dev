package com.keaz.landbird.activities;

import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;

import com.keaz.landbird.R;

public class TermsOfService2Activity extends BaseActivity implements View.OnClickListener {

    private WebView mWebview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_of_service2);

        mWebview = (WebView) findViewById(R.id.webview);
        //mWebview.loadUrl("file:///android_asset/Terms_of_Service.html");
    }

    @Override
    public void onClick(View v) {
    }

    public void onHeaderBackPress(View view) {
        onBackPressed();
    }
}
