package com.keaz.landbird.activities;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;

import com.google.gson.JsonObject;
import com.keaz.landbird.R;
import com.keaz.landbird.dialogs.ConfirmCancelBookingDialog;
import com.keaz.landbird.interfaces.CustomListener1;
import com.keaz.landbird.models.KZBooking;
import com.keaz.landbird.utils.BKGlobals;
import com.keaz.landbird.utils.BKNetworkResponseError;
import com.keaz.landbird.utils.BookingInfoHelper;
import com.keaz.landbird.utils.CommonUtils;
import com.keaz.landbird.utils.ConstantsUtils;
import com.keaz.landbird.utils.DateUtils;
import com.keaz.landbird.utils.FormatUtil;
import com.keaz.landbird.utils.JsonUtils;
import com.keaz.landbird.utils.LoadingViewUtils;
import com.keaz.landbird.utils.OnResponseModel;
import com.keaz.landbird.utils.UIUtils;
import com.keaz.landbird.utils.VolleyUtils;
import com.keaz.landbird.view.BatteryView;
import com.keaz.landbird.view.CustomFontTextView;
import com.segment.analytics.Analytics;
import com.segment.analytics.Properties;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

public class ApprovedBookingActivity extends BaseActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {

    private final int REQUEST_CODE_REPORT_PROBLEM = 1;
    public static KZBooking booking;
    private Switch switch1;
    private CustomFontTextView txtSwitch1;
    private boolean updateSuccess = false;
    private Runnable runnable;
    private boolean isFirstTime = true;
    private static boolean allowBackPress = true;;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_begin_reservation);


        txtSwitch1 = (CustomFontTextView) findViewById(R.id.txt_switch_1);
        switch1 = (Switch) findViewById(R.id.switch1);
        iniListener();
        iniInfo();
        runnable = new Runnable() {
            @Override
            public void run() {
                actionLoadBookingRegularlyOnTimer();
            }
        };
    }
    @Override
    protected void onStart() {
        super.onStart();

        CommonUtils.segmentScreenWithBooking(ApprovedBookingActivity.this, "Booking Detail", booking);
    }
    @Override
    public void onBackPressed() {
        if(allowBackPress){
            if (updateSuccess) {
                setResult(RESULT_OK);
            }
            super.onBackPressed();
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        startTimer(BKGlobals.RELOAD_TIME, runnable);
    }
    @Override
    protected void onPause() {
        super.onPause();
        stopTimer(runnable);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE_REPORT_PROBLEM) {
            if (resultCode == RESULT_OK) {
                isFirstTime = false;
            }
        }
    }
    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.switch1:
                break;
        }
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_switch_1:
                showAlertDialogNoTitle(getResources().getString(R.string.text_switch_1));
                break;
        }
    }

    private void iniInfo() {

        actionUpdateVehicleInfo(null);

        Picasso.with(this).load(booking.vehicle.assets.photo)
                .placeholder(R.drawable.new_img_car)
                .error(R.drawable.new_img_car)
                .into((ImageView) findViewById(R.id.imvCar));
    }

    private void iniListener() {
        txtSwitch1.setOnClickListener(this);
        switch1.setOnCheckedChangeListener(this);

        findViewById(R.id.btnSubmit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonUtils.segmentTrackWithBooking(ApprovedBookingActivity.this, "Click Begin_reservation_btn", booking);
                handleBeginReservation(booking);
            }
        });

        findViewById(R.id.rel_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ApprovedBookingActivity.this.onBackPressed();
            }
        });

        findViewById(R.id.cancelBooking).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonUtils.segmentTrackWithBooking(ApprovedBookingActivity.this, "Click Cancel_booking_btn", booking);
                handleCancelTheBooking();
            }
        });
        findViewById(R.id.tvContactUs).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ApprovedBookingActivity.this, MainActivity.class);
                intent.putExtra(ConstantsUtils.INTENT_EXTRA_TAB, 5);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        });
    }

    public void onReportProblemClick(View view) {
        String report = "";
        if (switch1.isChecked()) {
            report += getResources().getString(R.string.text_switch_1);
            report += "\n";
        }
        Analytics.with(this).track("Click report_problem_btn", new Properties().putValue("Report",report));

        ReportProblemActivity.report = report;
        ReportProblemActivity.booking = booking;
        Intent intent = new Intent(ApprovedBookingActivity.this, ReportProblemActivity.class);
        intent.putExtra("from", ApprovedBookingActivity.class.getSimpleName());
        startActivityForResult(intent, REQUEST_CODE_REPORT_PROBLEM);
    }

    private void handleBeginReservation(final KZBooking booking) {
        String report = "";
        if (switch1.isChecked()) {
            report += getResources().getString(R.string.text_switch_1);
            report += "\n";
        }
        if (switch1.isChecked() && isFirstTime) {
            ReportProblemActivity.report = report;
            ReportProblemActivity.booking = booking;
            Intent intent = new Intent(ApprovedBookingActivity.this, ReportProblemActivity.class);
            intent.putExtra("from", ApprovedBookingActivity.class.getSimpleName());
            startActivityForResult(intent, REQUEST_CODE_REPORT_PROBLEM);

        } else {
            if (booking.state.equalsIgnoreCase(BKGlobals.BookingStatus.APPROVED.toString())) {
                JSONObject jsonParams = new JSONObject();
                try {
                    jsonParams.put("state", BKGlobals.BookingStatus.INUSE.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                showOrHideProgressBar(true, this);
                VolleyUtils.getSharedNetwork().beginReservation(ApprovedBookingActivity.this, jsonParams, booking.id, new OnResponseModel<KZBooking>() {
                    @Override
                    public void onResponseSuccess(KZBooking model) {
                        updateSuccess = true;
                        showOrHideProgressBar(false, ApprovedBookingActivity.this);

                        Intent intent = new Intent(ApprovedBookingActivity.this, MainActivity.class);
                        intent.putExtra(ConstantsUtils.INTENT_EXTRA_TAB, 1);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();
                    }

                    @Override
                    public void onResponseError(BKNetworkResponseError error) {
                        showOrHideProgressBar(false, ApprovedBookingActivity.this);
                        updateSuccess = false;

                        if(error != null) {
                            if (error.code != null && error.code.equals(ConstantsUtils.ERROR_CODE)) {
                                showAlertDialog("", error.description, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        showOrHideProgressBar(true, ApprovedBookingActivity.this);
                                        final String currentBookingStatus = booking.state;
                                        VolleyUtils.getSharedNetwork().loadBookingByBookingId(ApprovedBookingActivity.this, booking.id,
                                                new OnResponseModel<KZBooking>() {

                                                    @Override
                                                    public void onResponseSuccess(KZBooking model) {
                                                        showOrHideProgressBar(false, ApprovedBookingActivity.this);
                                                        CommonUtils.handleOpenBooking(ApprovedBookingActivity.this, currentBookingStatus, model);
                                                    }

                                                    @Override
                                                    public void onResponseError(BKNetworkResponseError error) {
                                                        VolleyUtils.handleErrorRespond(ApprovedBookingActivity.this, error);
                                                        showOrHideProgressBar(false, ApprovedBookingActivity.this);
                                                    }
                                                });
                                    }
                                });
                            } else {
                                if (error.description != null && !error.description.equals("")) {
                                    showAlertDialog("", error.description);
                                } else {
                                    showAlertDialog("", getString(R.string.error_from_server));
                                }
                            }
                        } else {
                            showAlertDialog("", getString(R.string.error_from_server));
                        }
                    }
                });
            } else {
                UIUtils.toast(this, "The booking isn't approved.");
            }
        }
    }

    private void handleCancelTheBooking() {
        final ConfirmCancelBookingDialog dialog = new ConfirmCancelBookingDialog(this, "Confirm");
        dialog.setOnConfirmCancelBookingListener(new ConfirmCancelBookingDialog.OnConfirmCancelBookingListener() {
            @Override
            public void onOk() {
                callAPICancelBooking();
            }

            @Override
            public void onCancel() {

            }
        });
        dialog.show();
    }

    private void callAPICancelBooking() {
        showOrHideProgressBar(true, ApprovedBookingActivity.this);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("state", BKGlobals.BookingStatus.CANCELLED.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        VolleyUtils.getSharedNetwork().updateCancelBooking(ApprovedBookingActivity.this, booking.id, new OnResponseModel<Object>() {
            @Override
            public void onResponseSuccess(Object model) {
                booking.state = "cancelled";
                CommonUtils.segmentTrackWithBooking(ApprovedBookingActivity.this, "Cancel Booking Successfully", booking);

                showOrHideProgressBar(false, ApprovedBookingActivity.this);
                Intent intent = new Intent(ApprovedBookingActivity.this, BookingListActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                showOrHideProgressBar(false, ApprovedBookingActivity.this);

                if(error != null) {
                    if (error.code != null && error.code.equals(ConstantsUtils.ERROR_CODE)) {
                        showAlertDialog("", error.description, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                showOrHideProgressBar(true, ApprovedBookingActivity.this);
                                final String currentBookingStatus = booking.state;
                                VolleyUtils.getSharedNetwork().loadBookingByBookingId(ApprovedBookingActivity.this, booking.id,
                                        new OnResponseModel<KZBooking>() {

                                            @Override
                                            public void onResponseSuccess(KZBooking model) {
                                                showOrHideProgressBar(false, ApprovedBookingActivity.this);
                                                CommonUtils.handleOpenBooking(ApprovedBookingActivity.this, currentBookingStatus, model);
                                            }

                                            @Override
                                            public void onResponseError(BKNetworkResponseError error) {
                                                showOrHideProgressBar(false, ApprovedBookingActivity.this);
                                                VolleyUtils.handleErrorRespond(ApprovedBookingActivity.this, error);
                                            }
                                        });
                            }
                        });
                    } else {
                        showAlertDialog("", getString(R.string.error_from_server));
                    }
                }else {
                    showAlertDialog("", getString(R.string.error_from_server));
                }
            }
        });

    }

    private void actionLoadBookingRegularlyOnTimer() {
        findViewById(R.id.smallProgressBar).setVisibility(View.VISIBLE);
        VolleyUtils.getSharedNetwork().loadUpdatedBookingByBookingId(
                ApprovedBookingActivity.this,
                booking.id,
                new OnResponseModel<Object>() {

                    @Override
                    public void onResponseSuccess(Object model) {
                        findViewById(R.id.smallProgressBar).setVisibility(View.INVISIBLE);
                        JsonObject jsonObject = (JsonObject) model;
                        String jsString = jsonObject.toString();
                        if(!jsString.equals("{}")) {
                            booking = JsonUtils.parseByModel(jsonObject.toString(), KZBooking.class);
                            actionUpdateVehicleInfo(new CustomListener1() {
                                @Override
                                public void takeAction() {
                                    startTimer(getTimerStep(booking), runnable);
                                }
                            });
                        }else {
                            startTimer(getTimerStep(booking), runnable);
                        }

                    }

                    @Override
                    public void onResponseError(BKNetworkResponseError error) {
                        VolleyUtils.handleErrorRespond(ApprovedBookingActivity.this, error);
                        findViewById(R.id.smallProgressBar).setVisibility(View.INVISIBLE);
                        startTimer(BKGlobals.RELOAD_TIME_10, runnable);
                    }
                });
    }

    private void actionUpdateVehicleInfo(CustomListener1 customListener1) {

        UIUtils.setTextViewText(this, R.id.tv_car_name , booking.vehicle.name );
        UIUtils.setTextViewText(this, R.id.tv_car_id , booking.vehicle.registration );
        UIUtils.setTextViewText(this, R.id.txtState, booking.state.toUpperCase());

        UIUtils.setTextViewText(this, R.id.tv_hub_name , ""+booking.branch.name );
        UIUtils.setTextViewText(this, R.id.tv_hub_address , ""+booking.branch.address );

        UIUtils.setTextViewText(this, R.id.txtFare, "$" + FormatUtil.formatString(booking.cost_sub) + "/" + booking.cost_type);
        UIUtils.setTextViewText(this, R.id.txtDate, DateUtils.formatDatePattern(DateUtils.getDate(booking.start), ConstantsUtils.DATE_TIME_FORMAT));

        long start = booking.actual_start - DateUtils.getTimeZoneOffset() - booking.start_timezone_offset;
        long end = booking.end - DateUtils.getTimeZoneOffset() - booking.end_timezone_offset;

        UIUtils.setTextViewText(this, R.id.tvFrom1, DateUtils.formatDatePattern(DateUtils.getDate(/*booking.start*/start), ConstantsUtils.TIME_FORMAT_1));
        UIUtils.setTextViewText(this, R.id.tvFrom2, DateUtils.formatDatePattern(DateUtils.getDate(/*booking.start*/start), ConstantsUtils.FORMAT_DATE));
        UIUtils.setTextViewText(this, R.id.tvTo1, DateUtils.formatDatePattern(DateUtils.getDate(/*booking.end*/end), ConstantsUtils.TIME_FORMAT_1));
        UIUtils.setTextViewText(this, R.id.tvTo2, DateUtils.formatDatePattern(DateUtils.getDate(/*booking.end*/end), ConstantsUtils.FORMAT_DATE));

        UIUtils.setTextViewText(this, R.id.txtRef, "#" + booking.id);

        String battery = booking.vehicle.battery;
        int percent = (battery != null && !battery.equals("")) ? Integer.parseInt(battery) : 0;
        ((BatteryView) findViewById(R.id.battery)).drawByPercent(percent);

        BookingInfoHelper.updateBookingTimeZone(booking, null, this);

        UIUtils.setTextViewText(this, R.id.txtPercent, percent + "%");

        BookingInfoHelper.updateBookingInfo2(booking, this, DateUtils.getTimeZoneOffset());

        if(booking.state.equalsIgnoreCase(BKGlobals.BookingStatus.EXPIRED.toString())){
            actionAutoOpenBookingDetailScreen();
            return;
        }else if(booking.state.equalsIgnoreCase(BKGlobals.BookingStatus.EXPIRED.toString())){
            actionAutoOpenInUseBookingScreen() ;
            return;
        }

        if(customListener1!=null) customListener1.takeAction();
    }

    private void actionAutoOpenInUseBookingScreen() {
        Intent intent = new Intent(ApprovedBookingActivity.this, MainActivity.class);
        intent.putExtra(ConstantsUtils.INTENT_EXTRA_TAB, 2);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    private void actionAutoOpenBookingDetailScreen() {
        stopTimer(runnable);
        DetailBookingActivity.booking = booking;
        Intent intent = new Intent(this, DetailBookingActivity.class);
        intent.putExtra(ConstantsUtils.INTENT_EXTRA_BACK_TO_HOME, true);
        /*intent.putExtra(ConstantsUtils.INTENT_EXTRA_CAR_URL, booking.vehicle.assets.photo);
        intent.putExtra(ConstantsUtils.INTENT_EXTRA_VEHICLE, booking.vehicle);
        intent.putExtra(ConstantsUtils.INTENT_EXTRA_BRANCH, booking.branch);
        intent.putExtra(ConstantsUtils.INTENT_EXTRA_START_TIME_IN_MILLIS, booking.start);
        intent.putExtra(ConstantsUtils.INTENT_EXTRA_END_TIME_IN_MILLIS, booking.end);
        intent.putExtra(ConstantsUtils.INTENT_EXTRA_TRIP_TYPE, booking.trip_type);
        intent.putExtra(ConstantsUtils.INTENT_EXTRA_BOOKING_STATUS, booking.state);*/
        startActivity(intent);
        overridePendingTransition(R.anim.fade_in_normal, R.anim.fade_out_normal);
    }

    private int getTimerStep(KZBooking booking) {
        Calendar calendar1 = Calendar.getInstance();
        calendar1.setTimeInMillis(booking.start*1000);
        Calendar calendar2 = Calendar.getInstance();

        if(calendar1.get(Calendar.DATE)==calendar2.get(Calendar.DATE)){
            return BKGlobals.RELOAD_TIME_10;
        }else {
            return BKGlobals.RELOAD_TIME_60;
        }
    }

    public static void showOrHideProgressBar(boolean b, Activity activity) {
        allowBackPress = !LoadingViewUtils.showOrHideProgressBar(b, activity);
    }
}
