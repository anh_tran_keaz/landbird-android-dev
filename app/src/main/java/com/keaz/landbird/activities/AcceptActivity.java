package com.keaz.landbird.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.keaz.landbird.R;
import com.keaz.landbird.utils.ConstantsUtils;
import com.keaz.landbird.utils.DialogUtils;
import com.keaz.landbird.view.CustomFontTextView;
import com.segment.analytics.Analytics;
import com.segment.analytics.Properties;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by anhtran1810 on 1/17/18.
 */

public class AcceptActivity extends BaseActivity implements CompoundButton.OnCheckedChangeListener {

    private CheckBox mCbx1;
    private CheckBox mCbx2;
    private CheckBox mCbx3;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accept);

        findViewById(R.id.img_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                actionClose();
            }
        });
        initContent1();
        initContent2();
        initContent3();
    }

    @Override
    protected void onStart() {
        super.onStart();

        SimpleDateFormat sdf = new SimpleDateFormat(ConstantsUtils.FORMAT_SEGMENT_DATE_TIME);
        String currentDateandTime = sdf.format(new Date());
        Analytics.with(this).screen("Accept Agreements", new Properties().putValue("Accepted Date", currentDateandTime));
    }

    @Override
    public void onBackPressed() {
        actionClose();
    }
    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            if (mCbx1.isChecked() && mCbx2.isChecked() && mCbx3.isChecked()) {
                setResult(RESULT_OK);
                finish();
            }
        }
    }

    private void actionClose() {
        DialogUtils.showMessageDialog(this,
                true,
                "",
                getString(R.string.confirm_to_exit),
                getString(R.string.btn_no),
                getString(R.string.btn_yes),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                },
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent intent = new Intent(AcceptActivity.this, LoginActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();
                    }
                });
    }

    private void initContent1() {
        mCbx1 = (CheckBox) findViewById(R.id.cbx_1);
        mCbx1.setTag("Terms of Service");
        mCbx1.setOnCheckedChangeListener(this);

        CustomFontTextView mTxtContent1 = (CustomFontTextView) findViewById(R.id.txt_content_1);
        mTxtContent1.setText(Html.fromHtml("I hereby confirm that I have reviewed and understand the Landbird Terms of Service and that I accept and agree to the <font color='blue'>Terms of Service</font> contained within those documents."));
        mTxtContent1.setMovementMethod(LinkMovementMethod.getInstance());
    }

    private void initContent2() {
        mCbx2 = (CheckBox) findViewById(R.id.cbx_2);
        mCbx2.setTag("Privacy Policy");
        mCbx2.setOnCheckedChangeListener(this);

        CustomFontTextView mTxtContent2 = (CustomFontTextView) findViewById(R.id.txt_content_2);
        mTxtContent2.setText(Html.fromHtml("I hereby confirm that I have reviewed and understand the Landbird Privacy Policy and that I accept and agree to the <font color='blue'>Privacy Policy</font> contained within those documents."));
        mTxtContent2.setMovementMethod(LinkMovementMethod.getInstance());
    }

    private void initContent3() {
        mCbx3 = (CheckBox) findViewById(R.id.cbx_3);
        mCbx3.setTag("Background Check");
        mCbx3.setOnCheckedChangeListener(this);

        CustomFontTextView mTxtContent3 = (CustomFontTextView) findViewById(R.id.txt_content_3);
        mTxtContent3.setText(Html.fromHtml("I hereby confirm that I have reviewed and understand the Landbird Background Check and that I accept and agree to the <font color='blue'>Background Check</font> contained within those documents."));
        mTxtContent3.setMovementMethod(LinkMovementMethod.getInstance());
    }

}





















































