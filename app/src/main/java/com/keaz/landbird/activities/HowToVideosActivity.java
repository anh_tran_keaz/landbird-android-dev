package com.keaz.landbird.activities;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;

import com.keaz.landbird.R;
import com.keaz.landbird.models.KZAccount;
import com.keaz.landbird.models.VideoModel;
import com.keaz.landbird.view.CustomFontTextView;
import com.segment.analytics.Analytics;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class HowToVideosActivity extends BaseActivity {

    public static final String INTENT_EXTRA_API = "intent extra api";
    public static final String INTENT_EXTRA_TITLE = "intent extra title";
    public static final String INTENT_EXTRA_HTML = "intent extra html";
    private static final String TAG = BenefitListActivity.class.getSimpleName();
    private ListView listView;
    private HowToVideosActivity.VideoAdapter adapter;
    private ArrayList<VideoModel> data = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_how_to_videos);
        initUI();
        initData();
        initListener();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Analytics.with(this).screen("Support Articles and FAQ");
    }

    public void initUI() {

        listView = (ListView) findViewById(R.id.lv_videos);
    }

    public void initData() {
        adapter = new VideoAdapter(this, data);
        listView.setAdapter(adapter);

        /*final String page = getIntent().getStringExtra(INTENT_EXTRA_API);
        if(page!=null){
            VolleyUtils.getSharedNetwork().getPage(this, page, new OnResponseModel<KzPage>() {
                @Override
                public void onResponseSuccess(KzPage model) {
                    try {
                        if("pricing".equalsIgnoreCase(page)) {
                            WebView wv = ((WebView)findViewById(R.id.webview));
                            wv.setVisibility(View.VISIBLE);
                            findViewById(R.id.tv).setVisibility(View.GONE);

                            wv.setFocusable(true);
                            wv.setFocusableInTouchMode(true);

                            wv.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
                            wv.setInitialScale(100);

                            WebSettings webSettings = wv.getSettings();
                            webSettings.setUseWideViewPort(false);
                            webSettings.setBuiltInZoomControls(true);
                            webSettings.setSupportZoom(true);
                            webSettings.setDisplayZoomControls(false);

                            wv.setWebViewClient(new WebViewClient());
                            wv.loadDataWithBaseURL(null, model.content,
                                    "text/html; charset=utf-8","UTF-8", null);
                        } else {
                            ((TextView) (findViewById(R.id.tv))).setText(Html.fromHtml(model.content));
                            ((TextView) (findViewById(R.id.tv))).setMovementMethod(LinkMovementMethod.getInstance());
                            findViewById(R.id.webview).setVisibility(View.GONE);
                            findViewById(R.id.tv).setVisibility(View.VISIBLE);
                        }
                        ((TextView) findViewById(R.id.customFontTextView)).setText(model.title);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }

                @Override
                public void onResponseError(BKNetworkResponseError error) {
                    VolleyUtils.handleErrorRespond(HowToVideosActivity.this, error);
                }
            });
        }

        ((TextView)findViewById(R.id.customFontTextView)).setText(getIntent().getStringExtra(INTENT_EXTRA_TITLE));
        String html = getIntent().getStringExtra(INTENT_EXTRA_HTML);
        if(html!=null){
            ((TextView)(findViewById(R.id.tv))).setText(Html.fromHtml(html));
            ((TextView)(findViewById(R.id.tv))).setMovementMethod(LinkMovementMethod.getInstance());
        }*/

        String faq = KZAccount.getSharedAccount().company.faq;
        WebView wv = ((WebView)findViewById(R.id.webview));
        wv.getSettings().setJavaScriptEnabled(true);
        wv.setWebViewClient(new WebViewClient());
        wv.loadUrl(faq);
    }

    public void onHeaderBackPress(View view) {
        onBackPressed();
    }

    public void initListener() {
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //ToastUtil.showToastMessage(HowToVideosActivity.this, data.get(position).video, false);
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(data.get(position).video));
                startActivity(browserIntent);
            }
        });
    }

    private class VideoAdapter extends BaseAdapter {
        private final Context context;
        private final List<VideoModel> videoModels;

        VideoAdapter(Context context, ArrayList<VideoModel> values) {
            this.context = context;
            this.videoModels = values;
        }

        @Override
        public int getCount() {
            return videoModels != null ? videoModels.size() : 0;
        }

        @Override
        public Object getItem(int position) {
            return videoModels.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View rowView, ViewGroup parent) {

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if(rowView==null) rowView = inflater.inflate(R.layout.item_videos_list, parent, false);


            ImageView imgVideo = (ImageView) rowView.findViewById(R.id.img_video);
            CustomFontTextView tvTittle = (CustomFontTextView) rowView.findViewById(R.id.tv_title);
            CustomFontTextView tvView = (CustomFontTextView) rowView.findViewById(R.id.tv_view);

            tvTittle.setText(videoModels.get(position).name);
            String url = videoModels.get(position).photo;
            Picasso.with(context).load(url).placeholder(R.drawable.img_vehicle_report_right)
                    .error(R.drawable.img_vehicle_report_right)
                    .into(imgVideo);



            return rowView;
        }

    }
}
