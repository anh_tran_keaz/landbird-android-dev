package com.keaz.landbird.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.keaz.landbird.R;
import com.keaz.landbird.models.KZAccount;
import com.keaz.landbird.models.KZLoginResponse;
import com.keaz.landbird.models.UserModel;
import com.keaz.landbird.utils.BKGlobals;
import com.keaz.landbird.utils.BKNetworkResponseError;
import com.keaz.landbird.utils.CommonUtils;
import com.keaz.landbird.utils.ConstantsUtils;
import com.keaz.landbird.utils.DialogUtils;
import com.keaz.landbird.utils.IntentUtil;
import com.keaz.landbird.utils.LoadingViewUtils;
import com.keaz.landbird.utils.ManagePasswordUtil;
import com.keaz.landbird.utils.OnResponseModel;
import com.keaz.landbird.utils.ValidateUtils;
import com.keaz.landbird.utils.VolleyUtils;
import com.segment.analytics.Analytics;

/**
 * Created by mac on 2/28/17.
 */
public class ActivityResetPassword extends BaseActivity{

    private EditText edtPass, edtPass2;
    private TextView btnNext;
    private int mStep = 1;
    private String mPassword = "";
    private String mCode;
    private ImageView mImvEyePassword, mImvEyePassword2;
    private String email;
    private String phone;
    private String countryCode;
    private int registerCase;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);

        findViewById(R.id.imvClose).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickBack();
            }
        });

        mCode = getIntent().getStringExtra(ConstantsUtils.INTENT_EXTRA_CODE);
        email = getIntent().getStringExtra(ConstantsUtils.INTENT_EXTRA_EMAIL);
        phone = getIntent().getStringExtra(ConstantsUtils.INTENT_EXTRA_PHONE);
        countryCode = getIntent().getStringExtra(ConstantsUtils.INTENT_EXTRA_COUNTRY_CODE);
        registerCase = getIntent().getIntExtra(ConstantsUtils.INTENT_EXTRA_VERIFY_EMAIL_OR_PHONE,1);
        edtPass = (EditText) findViewById(R.id.edtPass);
        edtPass2 = (EditText) findViewById(R.id.edtPass2);
        btnNext = (TextView)findViewById(R.id.btnNext);

        mImvEyePassword = (ImageView) findViewById(R.id.imv_eye_password);
        mImvEyePassword2 = (ImageView) findViewById(R.id.imv_eye_password2);

        findViewById(R.id.btnShowHidePassword).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ManagePasswordUtil.changePasswordAndTextType(edtPass, mImvEyePassword);
            }
        });
        findViewById(R.id.btnShowHidePassword2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ManagePasswordUtil.changePasswordAndTextType(edtPass2, mImvEyePassword2);
            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                next();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        UserModel user = KZAccount.getSharedAccount().getUser();
        CommonUtils.segmentIdentifyWithUser(this, user);
        Analytics.with(this).screen("Reset Password");
    }

    @Override
    public void onBackPressed() {
        clickBack();
    }

    private void clickBack() {
        DialogUtils.showCustomDialog(ActivityResetPassword.this, true,
                getString(R.string.confirm_to_exit),
                "",
                getString(R.string.btn_yes),
                getString(R.string.btn_no),
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(ActivityResetPassword.this, LoginActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();
                    }
                },
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                    }
                });
    }

    private void next() {
        if(edtPass.getText().toString().isEmpty()){
            Toast.makeText(this, getString(R.string.error_empty_password), Toast.LENGTH_SHORT).show();
            return;
        }
        if(edtPass2.getText().toString().isEmpty()){
            Toast.makeText(this, getString(R.string.error_empty_confirm_password), Toast.LENGTH_SHORT).show();
            return;
        }
        if(!ValidateUtils.validatePassword(edtPass.getText().toString(), getApplicationContext())){
            //Toast.makeText(this, getString(R.string.error_format_password_limit_character), Toast.LENGTH_SHORT).show();
            return ;
        }
        if(!edtPass.getText().toString().equalsIgnoreCase(edtPass2.getText().toString())){
            Toast.makeText(this, getString(R.string.error_password_diffirent_confirm_password), Toast.LENGTH_SHORT).show();
            return;
        }
        callApiResetPassword(mCode, edtPass.getText().toString());
    }

    private void callApiResetPassword(String code, final String password) {
        Analytics.with(this).track("Click Submit_btn");

        LoadingViewUtils.showOrHideProgressBar(true, this);
        VolleyUtils.getSharedNetwork().resetPassword(this, code, password, new OnResponseModel() {
            @Override
            public void onResponseSuccess(Object model) {
                LoadingViewUtils.showOrHideProgressBar(false, ActivityResetPassword.this);
                actionCallApiToLogin(password);

                Toast.makeText(ActivityResetPassword.this,getString(R.string.text_success),Toast.LENGTH_LONG).show();
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                LoadingViewUtils.showOrHideProgressBar(false, ActivityResetPassword.this);
                VolleyUtils.handleErrorRespond(ActivityResetPassword.this, error);
            }
        });
    }

    private void actionCallApiToLogin(final String pass) {

        if(countryCode == null) this.countryCode = "1";

        LoadingViewUtils.showOrHideProgressBar(true, this);
        switch (registerCase) {
            case 1:
                VolleyUtils.getSharedNetwork().loginUserByEmail(this, email, pass, new OnResponseModel() {
                    @Override
                    public void onResponseSuccess(Object model) {
                        if (model instanceof KZLoginResponse) {
                            handleLoginSuccessRespond(model, email, "", countryCode, pass);
                        }
                    }

                    @Override
                    public void onResponseError(BKNetworkResponseError networkResponse) {
                        VolleyUtils.handleErrorRespond(ActivityResetPassword.this, networkResponse);
                    }
                });
                break;

            case 2:
                VolleyUtils.getSharedNetwork().loginUserByPhone(this, phone, countryCode, pass, new OnResponseModel() {
                    @Override
                    public void onResponseSuccess(Object model) {
                        if (model instanceof KZLoginResponse) {
                            handleLoginSuccessRespond(model, "", phone, countryCode, pass);
                        }
                    }

                    @Override
                    public void onResponseError(BKNetworkResponseError networkResponse) {
                        VolleyUtils.handleErrorRespond(ActivityResetPassword.this, networkResponse);
                    }
                });
                break;
        }

    }

    private void handleLoginSuccessRespond(Object model, String email, String phone, String countryCode, String pass) {
        LoadingViewUtils.showOrHideProgressBar(false, this);
        if (model instanceof KZLoginResponse) {
            KZLoginResponse response = (KZLoginResponse) model;
            String accessToken = response.accessToken;
            BKGlobals.getSharedGlobals().saveStringPreferences(BKGlobals.ACCESS_TOKEN, accessToken);

            BKGlobals.getSharedGlobals().saveStringPreferences(ConstantsUtils.PREF_KEY_EMAIL, email);
            BKGlobals.getSharedGlobals().saveStringPreferences(ConstantsUtils.PREF_KEY_CONTACT_NUMBER, phone);
            BKGlobals.getSharedGlobals().saveStringPreferences(ConstantsUtils.PREF_KEY_COUNTRY_CODE, countryCode);
            BKGlobals.getSharedGlobals().saveStringPreferences(ConstantsUtils.PREF_KEY_PASSWORD, pass);
            BKGlobals.getSharedGlobals().saveBoolPreferences(ConstantsUtils.PREF_KEY_KEEP_SIGN_IN, true);

            Intent intent = new Intent(this, ActivityWelcome.class);
            intent = IntentUtil.addFlag1(intent);
            startActivity(intent);

        }
    }
}
