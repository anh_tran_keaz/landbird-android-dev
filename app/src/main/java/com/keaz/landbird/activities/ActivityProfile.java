package com.keaz.landbird.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.JsonObject;
import com.keaz.landbird.R;
import com.keaz.landbird.helpers.SharedPreferencesUtils;
import com.keaz.landbird.interfaces.CustomListener1;
import com.keaz.landbird.interfaces.CustomListener2;
import com.keaz.landbird.models.KZAccount;
import com.keaz.landbird.models.UserAssetsModel;
import com.keaz.landbird.models.UserModel;
import com.keaz.landbird.utils.BKNetworkResponseError;
import com.keaz.landbird.utils.ConstantsUtils;
import com.keaz.landbird.utils.DialogUtils;
import com.keaz.landbird.utils.EditUtil;
import com.keaz.landbird.utils.ImageUtils;
import com.keaz.landbird.utils.MultipartRequest;
import com.keaz.landbird.utils.OnResponseModel;
import com.keaz.landbird.utils.ParseJSonUtil;
import com.keaz.landbird.utils.PermissionUtils;
import com.keaz.landbird.utils.ValidateUtils;
import com.keaz.landbird.utils.VolleySingleton;
import com.keaz.landbird.utils.VolleyUtils;
import com.keaz.landbird.view.CustomFontTextView;
import com.pkmmte.view.CircularImageView;
import com.thin.downloadmanager.DefaultRetryPolicy;
import com.thin.downloadmanager.DownloadRequest;
import com.thin.downloadmanager.DownloadStatusListener;
import com.thin.downloadmanager.ThinDownloadManager;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import static com.keaz.landbird.utils.ImageUtils.buildPart;

/**
 * Created by keaz on 10/16/17.
 */

public class ActivityProfile extends BaseActivity implements View.OnClickListener {

    public static final int REQUEST_CODE_TAKE_PHOTO_ACTIVITY = 201;
    private String TAG = ActivityProfile.class.getSimpleName();
    private EditText etPhone;
    private EditText etSurName, etName;
    private CustomFontTextView etEmail;
    private ProgressBar progressBar;
    private String countryCode;
    private ArrayList<CountryCodeModels> mCountryCodeModels;
    private UserModel user;
    private boolean avatarIsChanged;
    private ImageView ivUserProfile;
    private byte[] multipartBody;
    private String urlPhoto;
    private ThinDownloadManager thinDownloadManager;
    private String imageFolderPath;
    private String userProfileImagePath;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_profile);

        createFolderHideImage();
        initUI();
        iniListener();
        user = KZAccount.getSharedAccount().getUser();
        thinDownloadManager = new ThinDownloadManager();

        mCountryCodeModels = new ArrayList<>();
        mCountryCodeModels.add(new CountryCodeModels("US +1", "1"));
        mCountryCodeModels.add(new CountryCodeModels("NZ +64", "64"));
        mCountryCodeModels.add(new CountryCodeModels("AU +61", "61"));
        mCountryCodeModels.add(new CountryCodeModels("VN +84", "84"));

        showOrHideProgressBar(true);
        VolleyUtils.getSharedNetwork().loadUserInfo(this,"" + user.id, new OnResponseModel() {
            @Override
            public void onResponseSuccess(Object model) {
                showOrHideProgressBar(false);
                JsonObject jsonObject = (JsonObject) model;
                UserModel userModel = ParseJSonUtil.parseByModel(jsonObject.toString(), UserModel.class);
                KZAccount.getSharedAccount().setUser(userModel);
                initialInfo(userModel);
                user = userModel;

            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                showOrHideProgressBar(false);
                VolleyUtils.handleErrorRespond(ActivityProfile.this, error);
            }
        });
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.tv_country:
                showPopUpCountryMenu();
                break;
            case R.id.btnSave:
                actionClickBtnSave(true, new CustomListener1() {
                    @Override
                    public void takeAction() {

                    }
                }, new CustomListener2() {
                    @Override
                    public void success() {
                        Toast.makeText(ActivityProfile.this, "Success!", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void fail(BKNetworkResponseError error) {
                        DialogUtils.showMessageDialog(ActivityProfile.this, true, "Error in saving data", "Detail: " + error!=null ? error.description : "NA", "Close", "",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                       dialogInterface.cancel();
                                    }
                                }, null);
                    }
                }, null);
                break;
            case R.id.imv_back:
                actionBack();
                break;
            case R.id.btnProfile:
                /*DialogUtils.showMessageDialog(ActivityProfile.this, true, "", "Sorry, this feature is still in progress and will be available in the next version.", "Ok", "",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.cancel();
                            }
                        }, null);*/
                String[] pArr = new String[]{
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.CAMERA
                };
                if(PermissionUtils.checkAndRequestPermissions(ActivityProfile.this, pArr, ConstantsUtils.REQUEST_ID_MULTIPLE_PERMISSIONS)){
                    actionStartIntentTakePicture();
                }
                break;
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case ConstantsUtils.REQUEST_ID_MULTIPLE_PERMISSIONS:
                boolean pass =
                        PermissionUtils.checkImportantPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) &&
                        PermissionUtils.checkImportantPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) &&
                        PermissionUtils.checkImportantPermission(this, Manifest.permission.CAMERA);
                if(!pass){
                    DialogUtils.showMessageDialog(this, true,
                            "Important permission required!",
                            "This app can not work properly without the permission to read, write device's external storage and access your device's camera.", "Ok","",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            },null);
                    return;
                }
                break;
        }
    }
    @Override
    public void onBackPressed() {
    actionBack();
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode){
            case REQUEST_CODE_TAKE_PHOTO_ACTIVITY:
                if (resultCode == Activity.RESULT_OK) {
                    urlPhoto = data.getStringExtra(TakePhotoActivity.EXTRA_PHOTO_URL);
                    showUserPicture(urlPhoto);
                    avatarIsChanged = true;
                } else if (resultCode == TakePhotoActivity.CANCEL_BY_PERMISSIONS) {
                    showAlertDialog("", getString(R.string.message_must_enable_permission_take_phone), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent();
                            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            Uri uri = Uri.fromParts("package", getPackageName(), null);
                            intent.setData(uri);
                            startActivity(intent);
                        }
                    });
                }
                break;
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private void actionBack() {
        if(infoHasChanged()){
            DialogUtils.showMessageDialog(ActivityProfile.this, true, "Save profile info?", "Do you want to save the profile info?",
                    "Yes", "No",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.cancel();
                            actionClickBtnSave(true, new CustomListener1() {
                                @Override
                                public void takeAction() {
                                    finish();
                                }
                            }, new CustomListener2() {
                                @Override
                                public void success() {
                                    Toast.makeText(ActivityProfile.this, "Save info successfully!", Toast.LENGTH_SHORT).show();
                                    finish();
                                }

                                @Override
                                public void fail(BKNetworkResponseError error) {
                                    Toast.makeText(ActivityProfile.this, "There is error in saving data.", Toast.LENGTH_SHORT).show();
                                    finish();
                                }
                            }, new CustomListener1() {
                                @Override
                                public void takeAction() {
                                    finish();
                                }
                            });

                        }
                    },
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.cancel();
                            finish();
                        }
                    });
        }else {
            finish();
        }
    }

    private boolean infoHasChanged() {
        if(!etName.getText().toString().equals(user.name)) {
            return true;
        }
        if(!etSurName.getText().toString().equals(user.surname)){
            return true;
        }
        if(!etEmail.getText().toString().equals(user.email)){
            return true;
        }
        if(!etPhone.getText().toString().equals(user.contact_number_second)){
            return true;
        }
        return false;

    }

    private void actionClickBtnSave(boolean callApiImmediately, final CustomListener1 customListener1, final CustomListener2 customListenerIfDataChanged, final CustomListener1 customListenerIfClickNo) {
        boolean validate = validateInput();
        if (!validate) return;

        final JSONObject json = new JSONObject();
        try {
            if(avatarIsChanged){

                if(!callApiImmediately){
                    DialogUtils.showMessageDialog(ActivityProfile.this, true, "Save profile picture?", "Do you want to save the profile picture?",
                            "Yes", "No",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.cancel();
                                    actionUploadProfilePicture(new CustomListener1() {
                                        @Override
                                        public void takeAction() {
                                            finish();
                                        }
                                    });
                                }
                            },
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.cancel();
                                    //show1 the old image and do nothing
                                    ImageUtils.showImageFromPath(ActivityProfile.this, ivUserProfile, findViewById(R.id.btnProfile), userProfileImagePath);
                                    avatarIsChanged = false;

                                }
                            });
                    return;
                }
                actionUploadProfilePicture(null);
            }

            if(!etName.getText().toString().equals(user.name))
                json.put("name", etName.getText().toString());
            if(!etSurName.getText().toString().equals(user.surname))
                json.put("last_name", etSurName.getText().toString());
            if(!etEmail.getText().toString().equals(user.email))
                json.put("email", etEmail.getText().toString());
            if(!etPhone.getText().toString().equals(user.contact_number_second))
                json.put("contact_number_second", etPhone.getText().toString());

            if(json.length()==0) {
                if(customListener1!=null) customListener1.takeAction();
                return;
            }else {
                if(!callApiImmediately){
                    DialogUtils.showMessageDialog(ActivityProfile.this, true, "Save profile info?", "Do you want to save the profile info?",
                            "Yes", "No",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.cancel();
                                    actionCallApiToSaveInfo(json, customListenerIfDataChanged);
                                }
                            },
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.cancel();
                                    if(customListenerIfClickNo!=null) customListenerIfClickNo.takeAction();

                                }
                            });
                    return;
                }
            }
        }catch (Exception e){
            Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
        }

        actionCallApiToSaveInfo(json, customListenerIfDataChanged);
    }

    private boolean validateInput() {
        if (etEmail.getText().toString().equals("")) {
            Toast.makeText(this, getString(R.string.error_empty_email), Toast.LENGTH_SHORT).show();
            etEmail.requestFocus();
            return false;
        } else {
            if (!ValidateUtils.validateEmail(this, etEmail.getText().toString())) {
                Toast.makeText(this, getString(R.string.error_format_email), Toast.LENGTH_SHORT).show();
                etEmail.requestFocus();
                return false;
            }
        }

        if (!etPhone.getText().toString().equals("")) {
            if(!ValidateUtils.validatePhone(etPhone.getText().toString())){
                Toast.makeText(this, getString(R.string.error_format_phone), Toast.LENGTH_SHORT).show();
                return false;
            }
        }else {
            Toast.makeText(this, getString(R.string.error_empty_phone), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void actionCallApiToSaveInfo(JSONObject json, final CustomListener2 customListener) {
        showOrHideProgressBar(true);
        VolleyUtils.getSharedNetwork().updateUserInfo(this,KZAccount.getSharedAccount().getUser().id+"", json, new OnResponseModel() {
            @Override
            public void onResponseSuccess(Object model) {
                showOrHideProgressBar(false);
                JsonObject jsonObject = (JsonObject) model;
                user = ParseJSonUtil.parseByModel(jsonObject.toString(), UserModel.class);
                initialInfo(user);
                KZAccount.getSharedAccount().setUser(user);
                if(customListener!=null) customListener.success();
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                showOrHideProgressBar(false);
                if(customListener!=null) customListener.fail(error);
            }
        });
    }

    private void iniListener() {
        findViewById(R.id.btnProfile).setOnClickListener(this);
        findViewById(R.id.ll_countryCode).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCountryCodePopUpMenu();
            }
        });
        findViewById(R.id.btnSave).setOnClickListener(this);
        findViewById(R.id.imv_back).setOnClickListener(this);
    }

    public void initUI() {
        ivUserProfile = (CircularImageView)findViewById(R.id.imv_profile);
        etName = (EditText)findViewById(R.id.et_name);
        etSurName = (EditText)findViewById(R.id.et_surname);
        etPhone = (EditText) findViewById(R.id.et_phone);
        etEmail = (CustomFontTextView) findViewById(R.id.et_email);

    }

    private void initialInfo(final UserModel kzUser) {
        EditUtil.setTextOrHint(etName, kzUser.name, "Enter your first name");
        EditUtil.setTextOrHint(etSurName, kzUser.surname, "Enter your surname");
        EditUtil.setTextOrHint(etPhone, kzUser.contact_number_second, "Enter your contact number");
        EditUtil.setTextOrDefaultText(etEmail, kzUser.email, "Enter your email");

        UserAssetsModel userAssetsModel = kzUser.getAssets();
        if (userAssetsModel != null) {
            final String avatar = userAssetsModel.avatar;

            if(avatar!=null){
                downloadPicture(avatar, "avatar", new DownloadStatusListener() {

                    @Override
                    public void onDownloadComplete(int id) {
                        findViewById(R.id.smallProgressBar).setVisibility(View.INVISIBLE);
                        userProfileImagePath = imageFolderPath + File.separator + "avatar";
                        ImageUtils.showImageFromPath(ActivityProfile.this, ivUserProfile, findViewById(R.id.btnProfile), userProfileImagePath);
                    }

                    @Override
                    public void onDownloadFailed(int id, int errorCode, String errorMessage) {
                        findViewById(R.id.smallProgressBar).setVisibility(View.INVISIBLE);
                    }

                    @Override
                    public void onProgress(int id, long totalBytes, long downloadedBytes, int progress) {

                    }
                });
            }
        }
    }

    /*private void updateCitiesWithCountry(ArrayList<CustomLocation> customLocations, String country) {
        for (CustomLocation location: customLocations) {
            if(location.getName().equals(country)){
                cities = CountryCityUtil.getCities(location);
            }
        }
    }*/

    private void showCountryCodePopUpMenu() {
        PopupMenu popup = new PopupMenu(this, findViewById(R.id.ll_countryCode));
        popup.getMenuInflater().inflate(R.menu.empty_menu, popup.getMenu());

        Menu menu = popup.getMenu();
        for (int i = 0; i < mCountryCodeModels.size(); i++) {
            menu.add(0, i, i, mCountryCodeModels.get(i).getValue());
        }

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                countryCode = mCountryCodeModels.get(item.getItemId()).getNumber();
                return true;
            }
        });

        popup.show();
    }

    private void showPopUpCountryMenu() {
        /*PopupMenu popup = new PopupMenu(this, tvCountry);
        popup.getMenuInflater().inflate(R.menu.empty_menu, popup.getMenu());


        Menu menu = popup.getMenu();
        for (int i = 0; i < customLocations.size(); i++) {
            menu.add(0, i, i, customLocations.get(i).getName());
        }

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                tvCountry.setText(item.getTitle().toString());
                cities = CountryCityUtil.getCities(customLocations.get(item.getItemId()));
                tvCity.setText("Select your city");
                return true;
            }
        });

        popup.show();*/
    }

    private void showPopUpCityMenu() {
        /*PopupMenu popup = new PopupMenu(this, tvCity);
        popup.getMenuInflater().inflate(R.menu.empty_menu, popup.getMenu());


        Menu menu = popup.getMenu();
        for (int i = 0; i < cities.size(); i++) {
            menu.add(0, i, i, cities.get(i).getName());
        }

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                tvCity.setText(item.getTitle().toString());
                return true;
            }
        });

        popup.show();*/
    }

    private void showOrHideProgressBar(boolean b) {
        if(b){
//            progressBar.setVisibility(View.VISIBLE);
            showCustomDialogLoading();
        }else {
//            progressBar.setVisibility(View.INVISIBLE);
            dismissCustomDialogLoading();
        }
    }

    private void actionStartIntentTakePicture() {
        Intent intent = new Intent(this, TakePhotoActivity.class);
        intent.putExtra(TakePhotoActivity.KEY_IS_CROP_PHOTO, true);
        startActivityForResult(intent, REQUEST_CODE_TAKE_PHOTO_ACTIVITY);

    }

    private void showUserPicture(String imagePath) {
        SharedPreferencesUtils.setStringPrefValue(ConstantsUtils.PREF_KEY_PROFILE_IMAGE_PATH, imagePath, this);
        if (imagePath != null && !imagePath.equals("")) {
            Bitmap bitmap = BitmapFactory.decodeFile(imagePath);
            ivUserProfile.setImageBitmap(bitmap);
            //imageFrame.setVisibility(View.GONE);
        } else {
            ivUserProfile.setBackgroundResource(R.drawable.account);
            //imageFrame.setVisibility(View.VISIBLE);
        }
    }

    private void actionUploadProfilePicture(CustomListener1 customListener1){
        callApiToUploadPictureApi(ImageUtils.getFileDataFromBitmap(ImageUtils.createBitmapFromFile(urlPhoto)), VolleyUtils.getUrlUploadLicenseImageFace("" + user.id), customListener1);
    }

    private void callApiToUploadPictureApi(byte[] fileData, String url, final CustomListener1 customListener1) {
        showOrHideProgressBar(true);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        DataOutputStream dataOutputStream = new DataOutputStream(byteArrayOutputStream);
        try {
            buildPart(dataOutputStream, fileData);
            multipartBody = byteArrayOutputStream.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
        }

        String mimeType = "multipart/form-data";
        MultipartRequest multipartRequest = new MultipartRequest(
                url,
                VolleyUtils.genHeaders3(),
                mimeType,
                multipartBody,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {
                        Toast.makeText(ActivityProfile.this, "Upload successfully! ", Toast.LENGTH_SHORT).show();
                        showOrHideProgressBar(false);
                        avatarIsChanged = false;
                        if(customListener1!=null) customListener1.takeAction();

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        showOrHideProgressBar(false);
                        Toast.makeText(ActivityProfile.this, "Upload failed!\r\n Detail: " + error.getCause(), Toast.LENGTH_SHORT).show();
                        Toast.makeText(ActivityProfile.this, "Upload failed!\r\n Detail: " + Arrays.toString(error.getStackTrace()), Toast.LENGTH_SHORT).show();
                        Toast.makeText(ActivityProfile.this, "Upload failed!\r\n Detail: " + error.getMessage(), Toast.LENGTH_SHORT).show();
                        Toast.makeText(ActivityProfile.this, "Upload failed!\r\n Detail: " + error.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
        );

        VolleySingleton.getInstance(this).addToRequestQueue(multipartRequest);
    }

    private void downloadPicture(String fileUrl, String fileName, DownloadStatusListener listener) {
        if (fileUrl == null) return;
        findViewById(R.id.smallProgressBar).setVisibility(View.VISIBLE);
        String imageFolderPath = Environment.getExternalStorageDirectory() + File.separator + ".yoogo";
        Uri downloadUri = Uri.parse(fileUrl);
        Uri destinationUri = Uri.parse(imageFolderPath + File.separator + fileName);
        Log.d(TAG, "downloadPicture: " + destinationUri.toString());
        DownloadRequest downloadRequest = downLoadUserImage(ActivityProfile.this, downloadUri, destinationUri, listener);
        thinDownloadManager.add(downloadRequest);
    }

    public static DownloadRequest downLoadUserImage(Context context, Uri downloadUri, Uri destinationUri, DownloadStatusListener downloadListener) {
        return new DownloadRequest(downloadUri)
                .setRetryPolicy(new DefaultRetryPolicy())
                .setDestinationURI(destinationUri).setPriority(DownloadRequest.Priority.HIGH)
                .setDownloadContext(context)//Optional
                .setDownloadListener(downloadListener);
    }

    private void createFolderHideImage() {
        imageFolderPath = Environment.getExternalStorageDirectory() + File.separator + ".yoogo";
        File folder = new File(imageFolderPath);
        if (!folder.exists()) {
            folder.mkdir();
        } else {
            String[] myFiles = folder.list();
            if (myFiles != null && myFiles.length > 0) {
                for (String myFile : myFiles) {
                    ImageUtils.deleteImage(this, imageFolderPath + File.separator + myFile);
                }
            }
        }
    }

}
