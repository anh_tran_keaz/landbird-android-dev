package com.keaz.landbird.activities;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.keaz.landbird.R;
import com.keaz.landbird.dialogs.ReportProblemSentDialog;
import com.keaz.landbird.models.KZBooking;
import com.keaz.landbird.models.KZNotes;
import com.keaz.landbird.utils.BKNetworkResponseError;
import com.keaz.landbird.utils.BitmapUtils;
import com.keaz.landbird.utils.FileUtils;
import com.keaz.landbird.utils.OnResponseModel;
import com.keaz.landbird.utils.UriUtils;
import com.keaz.landbird.utils.Util;
import com.keaz.landbird.utils.VolleyUtils;
import com.keaz.landbird.view.CustomEditText;
import com.segment.analytics.Analytics;
import com.segment.analytics.Properties;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class ReportProblemActivity extends BaseActivity {


    public static final int TAG_CHOOSE_PHOTO_FROM_CAMERA = 1;
    private static final String TAG = ReportProblemActivity.class.getSimpleName();
    private static final int REQUEST_CODE_CAMERA_PERMISSION = 50;
    private static final String ACTION_UPLOAD = "upload";
    private static final int REQUEST_CODE_EXTERNAL_STORAGE_PERMISSION = 51;
    public static KZBooking booking;
    public static String report;
    LinearLayout myGallery;
    ArrayList<Bitmap> bitmapArray;
    int position = 0;
    private ImageView imgArea1, imgArea2, imgArea3, imgArea4;
    private CustomEditText etMessage;
    private HorizontalScrollView hvDamage;
    private int area_selected = 1;
    private File filePhotoCurrent = null;
    private int count = 0;
    private boolean isSuccessfull = false;

    interface MyCustomListener{
        void action();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_problem);

        initUI();
        initData();
        initListener();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Analytics.with(this).screen("Report Problem");
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        super.onBackPressed();
    }

    public void initUI() {
        imgArea1 = (ImageView) findViewById(R.id.img_area1);
        imgArea2 = (ImageView) findViewById(R.id.img_area2);
        imgArea3 = (ImageView) findViewById(R.id.img_area3);
        imgArea4 = (ImageView) findViewById(R.id.img_area4);

        etMessage = (CustomEditText) findViewById(R.id.et_message);
        etMessage.setText(report != null ? report : "");
        int pos = etMessage.getText().length();
        etMessage.setSelection(pos);

        hvDamage = (HorizontalScrollView) findViewById(R.id.hv_damage);

        myGallery = (LinearLayout) findViewById(R.id.mygallery);

        onShowCamera(area_selected);
    }

    View insertPhoto(final Bitmap bitmap) {
        final Bitmap bm = bitmap;//decodeSampledBitmapFromUri(path, 220, 220);
        int paddingLayout = Util.convertDp2Px(this, 5);
        final RelativeLayout layout = new RelativeLayout(getApplicationContext());
        layout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        layout.setPadding(paddingLayout, paddingLayout, paddingLayout, paddingLayout);
        layout.setGravity(Gravity.CENTER);
        layout.setTag(bm);

        final ImageView imageView = new ImageView(getApplicationContext());
        int width = Util.convertDp2Px(this, 130);
        int height = Util.convertDp2Px(this, 80);
        RelativeLayout.LayoutParams params1 = new RelativeLayout.LayoutParams(width, height);
        params1.addRule(RelativeLayout.CENTER_IN_PARENT, 1);
        imageView.setLayoutParams(params1);
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        imageView.setImageBitmap(bm);

        final ImageView img = new ImageView(getApplicationContext());
        img.setImageResource(R.drawable.btn_minus);
        img.setTag(layout);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                try {
                    View view = (View) v.getTag();
                    Bitmap bitmap = (Bitmap) view.getTag();
                    bitmapArray.remove(bitmap);
                    myGallery.removeView(view);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, 1);

        int padding = Util.convertDp2Px(this, 5);

        img.setPadding(0, padding, padding, 0);

        img.setLayoutParams(params);

        layout.addView(imageView);

        layout.addView(img);

        return layout;
    }

    public Bitmap decodeSampledBitmapFromUri(String path, int reqWidth, int reqHeight) {
        Bitmap bm = null;

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        bm = BitmapFactory.decodeFile(path, options);

        return bm;
    }

    public int calculateInSampleSize(

            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            if (width > height) {
                inSampleSize = Math.round((float) height / (float) reqHeight);
            } else {
                inSampleSize = Math.round((float) width / (float) reqWidth);
            }
        }

        return inSampleSize;
    }

    public void onHeaderBackPress(View view) {
        setResult(RESULT_CANCELED);
        onBackPressed();
    }


    public void onSubmitClick(View view) {
        if (!etMessage.getText().toString().isEmpty()) {
            showCustomDialogLoading();
            KZNotes notes = new KZNotes();
            notes.user_id = "";
            notes.type = "damage";
            notes.message = etMessage.getText().toString();


            VolleyUtils.getSharedNetwork().createBookingNotes(ReportProblemActivity.this, booking.id, notes, new OnResponseModel<KZNotes>() {
                @Override
                public void onResponseSuccess(KZNotes model) {
                    Analytics.with(ReportProblemActivity.this).track("Send report problem", new Properties().putValue("message",etMessage.getText().toString()));

                    if (bitmapArray != null && bitmapArray.size() > 0) {

                        String url = String.format(VolleyUtils.URI_UPLOAD_NOTE_IMAGE, model.note_id);
                        for (Bitmap bitmap : bitmapArray) {
                            VolleyUtils.getSharedNetwork().uploadPictureApi(url, BitmapUtils.getFileDataFromBitmap(bitmap), new OnResponseModel<NetworkResponse>() {
                                @Override
                                public void onResponseSuccess(NetworkResponse model) {
                                    isSuccessfull = true;
                                    count++;

                                    ReportProblemSentDialog dialog = new ReportProblemSentDialog(ReportProblemActivity.this);
                                    dialog.setOnReportProblemListener(new ReportProblemSentDialog.OnReportProblemListener() {
                                        @Override
                                        public void onOK() {
                                            checkUpload(count);
                                        }
                                    });
                                    dialog.show();
                                }

                                @Override
                                public void onResponseError(BKNetworkResponseError error) {
                                    VolleyUtils.handleErrorRespond(ReportProblemActivity.this, error);

                                    count++;
                                    checkUpload(count);
                                }
                            });
                        }
                    } else {
                        dismissCustomDialogLoading();
                        isSuccessfull = true;

                        ReportProblemSentDialog dialog = new ReportProblemSentDialog(ReportProblemActivity.this);
                        dialog.setOnReportProblemListener(new ReportProblemSentDialog.OnReportProblemListener() {
                            @Override
                            public void onOK() {
                                handleFinishActivity();
                            }
                        });
                        dialog.show();
                    }
                }

                @Override
                public void onResponseError(BKNetworkResponseError error) {
                    VolleyUtils.handleErrorRespond(ReportProblemActivity.this, error);
                    dismissCustomDialogLoading();
                }
            });
        } else {
            Toast.makeText(this, "Please input problem description", Toast.LENGTH_SHORT).show();
        }

    }

    private void checkUpload(int count) {
        if (count == bitmapArray.size()) {
            // Handle flow
            dismissCustomDialogLoading();
            handleFinishActivity();
        }
    }

    private void handleFinishActivity() {
        String from = getIntent().getStringExtra("from");
        if (from.equalsIgnoreCase(ApprovedBookingActivity.class.getSimpleName())) {
            if (isSuccessfull)
                setResult(RESULT_OK);
            finish();
            return;
        }
        if (from.equalsIgnoreCase(SubmitEndReservationActivity.class.getSimpleName())) {
            setResult(RESULT_OK);
            finish();
            return;
        }
        if (from.equals("")) {
            finish();
            return;
        }
    }

    public void onTouchArea(View view) {
        int id = view.getId();

        if (id == imgArea1.getId()) {
            onShowCamera(1);
        } else if (id == imgArea2.getId()) {
            onShowCamera(2);
        } else if (id == imgArea3.getId()) {
            onShowCamera(3);
        } else if (id == imgArea4.getId()) {
            onShowCamera(4);
        }

        //Check permission for camera
        checkCameraPermission(new MyCustomListener() {
            @Override
            public void action() {
                checkStoragePermission(new MyCustomListener() {
                    @Override
                    public void action() {
                        openIntentTakePhoto();
                    }
                });
            }
        });

    }

    private void checkCameraPermission(MyCustomListener customListener) {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED){
            //ask for camera permission
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, REQUEST_CODE_CAMERA_PERMISSION);
        } else {
            //Now we have camera permission, next we must check permission for external storage
            if(customListener!=null) customListener.action();
        }
    }

    private void checkStoragePermission(MyCustomListener customListener) {
        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            int permission = ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

            if (permission != PackageManager.PERMISSION_GRANTED) {
                //ask for read/write external permission
                String[] permissionsStorage = {
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                };
                ActivityCompat.requestPermissions(this, permissionsStorage, REQUEST_CODE_EXTERNAL_STORAGE_PERMISSION);
            } else {
                if(customListener!=null) customListener.action();
            }
        }else {
            Toast.makeText(getApplicationContext(), Environment.getExternalStorageState(), Toast.LENGTH_SHORT).show();
        }
    }

    public void initData() {

        bitmapArray = new ArrayList<>();

    }

    public void initListener() {


    }

    public void onShowCamera(int selected) {
        area_selected = selected;
        switch (selected) {
            case 1:
                imgArea1.setImageResource(R.drawable.icn_camera_big_ln_e826);
                imgArea2.setImageResource(0);
                imgArea3.setImageResource(0);
                imgArea4.setImageResource(0);
                break;
            case 2:
                imgArea2.setImageResource(R.drawable.icn_camera_big_ln_e826);
                imgArea1.setImageResource(0);
                imgArea3.setImageResource(0);
                imgArea4.setImageResource(0);
                break;
            case 3:
                imgArea3.setImageResource(R.drawable.icn_camera_big_ln_e826);
                imgArea2.setImageResource(0);
                imgArea1.setImageResource(0);
                imgArea4.setImageResource(0);
                break;
            case 4:
                imgArea4.setImageResource(R.drawable.icn_camera_big_ln_e826);
                imgArea2.setImageResource(0);
                imgArea3.setImageResource(0);
                imgArea1.setImageResource(0);
                break;
        }

    }

    private void openIntentTakePhoto() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            filePhotoCurrent = null;
            try {
                filePhotoCurrent = FileUtils.createImageFile();
            } catch (IOException ex) {

            }
            // Continue only if the File was successfully created
            if (filePhotoCurrent != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        UriUtils.getUri(this,filePhotoCurrent));
                startActivityForResult(takePictureIntent, TAG_CHOOSE_PHOTO_FROM_CAMERA);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case TAG_CHOOSE_PHOTO_FROM_CAMERA:
                    handlePhoto();
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_CAMERA_PERMISSION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    checkStoragePermission(new MyCustomListener() {
                        @Override
                        public void action() {
                            openIntentTakePhoto();
                        }
                    });
                } else {
                    showMustEnablePermissionDialog();
                }
                return;
            }
            case REQUEST_CODE_EXTERNAL_STORAGE_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    openIntentTakePhoto();
                }else {
                    showMustEnablePermissionDialog();
                }
                break;

        }
    }

    private void showMustEnablePermissionDialog() {
        showAlertDialog("", getString(R.string.message_must_enable_permission_take_phone), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent();
                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", getPackageName(), null);
                intent.setData(uri);
                startActivity(intent);
            }
        });
    }

    private void handlePhoto() {
        if (filePhotoCurrent != null && filePhotoCurrent.exists()) {
            // Scale bitmap
            Bitmap bitmap = BitmapUtils.resizeImage(filePhotoCurrent.getAbsolutePath());
            bitmapArray.add(bitmap);

//            for(int i = 0 ; i < bitmapArray.size() ; i++)
//            {

            View view = insertPhoto(bitmap);
//            view.setTag(position);
            myGallery.addView(view);
//            }
//            position = position + 1;
            filePhotoCurrent.delete();
            if (bitmap == null)
                return;

        } else {
            handleError();
        }
    }

    private void handleError() {
        Toast.makeText(this, getString(R.string.dialog_message_get_photo_error), Toast.LENGTH_SHORT).show();

    }

    public void onRotateClick(View view) {

    }
}
