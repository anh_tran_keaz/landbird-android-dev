package com.keaz.landbird.activities;

import com.braintreepayments.api.PayPal;

public class PayPalOverrides extends PayPal {

    public static void setFuturePaymentsOverride(boolean useFuturePayments) {
        PayPal.sFuturePaymentsOverride = useFuturePayments;
    }
}
