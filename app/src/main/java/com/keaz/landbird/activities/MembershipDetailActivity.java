package com.keaz.landbird.activities;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.keaz.landbird.R;
import com.keaz.landbird.interfaces.CustomListener2;
import com.keaz.landbird.models.KZMembership;
import com.keaz.landbird.utils.BKNetworkResponseError;
import com.keaz.landbird.utils.ConfigUtil;
import com.keaz.landbird.utils.ConstantsUtils;
import com.keaz.landbird.utils.DialogUtils;
import com.keaz.landbird.utils.FormatUtil;
import com.keaz.landbird.utils.JsonUtils;
import com.keaz.landbird.utils.OnResponseModel;
import com.keaz.landbird.utils.VolleyUtils;
import com.keaz.landbird.view.CustomFontButton;
import com.segment.analytics.Analytics;

import static com.keaz.landbird.utils.ConfigUtil.getConfigTripType;

/**
 * Created by anhtran1810 on 1/12/18.
 */

public class MembershipDetailActivity extends BaseActivity implements View.OnClickListener {

    private CustomFontButton mBtnApply;
    private KZMembership mMembership;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_membership_detail);

        mMembership = (KZMembership) getIntent().getSerializableExtra("membership");

        mBtnApply = (CustomFontButton) findViewById(R.id.btn_apply);
        mBtnApply.setOnClickListener(this);

        actionLoadMembership();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Analytics.with(this).screen("Membership Item Detail");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_apply:
                actionApply();
                break;
        }
    }

    private void updateInfo() {
        if(!ConfigUtil.checkIfEnableMembershipIsNotDiscount()){
            switch (getConfigTripType()){
                case ConstantsUtils.TRIP_TYPE_BUSINESS:
                    if(mMembership.discount_cost.isEmpty() ){
                        findViewById(R.id.cost).setVisibility(View.GONE);
                    }else {
                        ((TextView)findViewById(R.id.tvCostType)).setText("per "+(mMembership.discount_cost_type.isEmpty() ? "NA" : mMembership.discount_cost_type));
                        ((TextView)findViewById(R.id.tvCost)).setText(mMembership.discount_cost.isEmpty() ? "$00.00" : "$"+FormatUtil.formatString(mMembership.discount_cost));
                    }
                    break;
                case ConstantsUtils.TRIP_TYPE_PRIVATE:
                    if(mMembership.discount_private_cost.isEmpty()){
                        findViewById(R.id.cost).setVisibility(View.GONE);
                    }else {
                        ((TextView)findViewById(R.id.tvCostType)).setText("per "+(mMembership.discount_private_cost_type.isEmpty() ? "NA" : mMembership.discount_private_cost_type));
                        ((TextView)findViewById(R.id.tvCost)).setText(mMembership.discount_private_cost.isEmpty() ? "$00.00" : "$"+FormatUtil.formatString(mMembership.discount_private_cost));
                    }
                    break;
                case ConstantsUtils.TRIP_TYPE_BOTH:
                    if(mMembership.discount_cost.isEmpty() && mMembership.discount_private_cost.isEmpty()){
                        findViewById(R.id.cost).setVisibility(View.GONE);
                    }else {
                        ((TextView)findViewById(R.id.tvCostType)).setText("per "+(mMembership.discount_cost_type.isEmpty() ? "NA" : mMembership.discount_cost_type));
                        ((TextView)findViewById(R.id.tvCost)).setText(mMembership.discount_cost.isEmpty() ? "$00.00" : "$"+FormatUtil.formatString(mMembership.discount_cost));
                    }
                    break;
            }
        }else {
            switch (getConfigTripType()){
                case ConstantsUtils.TRIP_TYPE_BUSINESS:
                    if(mMembership.vehicle_cost.isEmpty() ){
                        findViewById(R.id.cost).setVisibility(View.GONE);
                    }else {
                        ((TextView)findViewById(R.id.tvCostType)).setText("per "+(mMembership.vehicle_cost_type.isEmpty() ? "NA" : mMembership.vehicle_cost_type));
                        ((TextView)findViewById(R.id.tvCost)).setText(mMembership.vehicle_cost.isEmpty() ? "$00.00" : "$"+FormatUtil.formatString(mMembership.vehicle_cost));
                    }
                    break;
                case ConstantsUtils.TRIP_TYPE_PRIVATE:
                    if(mMembership.vehicle_private_cost.isEmpty()){
                        findViewById(R.id.cost).setVisibility(View.GONE);
                    }else {
                        ((TextView)findViewById(R.id.tvCostType)).setText("per "+(mMembership.vehicle_private_cost_type.isEmpty() ? "NA" : mMembership.vehicle_private_cost_type));
                        ((TextView)findViewById(R.id.tvCost)).setText(mMembership.vehicle_private_cost.isEmpty() ? "$00.00" : "$"+FormatUtil.formatString(mMembership.vehicle_private_cost));
                    }
                    break;
                case ConstantsUtils.TRIP_TYPE_BOTH:
                    if(mMembership.vehicle_cost.isEmpty() && mMembership.vehicle_private_cost.isEmpty()){
                        findViewById(R.id.cost).setVisibility(View.GONE);
                    }else {
                        ((TextView)findViewById(R.id.tvCostType)).setText("per "+(mMembership.vehicle_cost_type.isEmpty() ? "NA" : mMembership.vehicle_cost_type));
                        ((TextView)findViewById(R.id.tvCost)).setText(mMembership.vehicle_cost.isEmpty() ? "$00.00" : "$"+FormatUtil.formatString(mMembership.vehicle_cost));
                    }
                    break;
            }
        }

        if(mMembership.fee_cost.isEmpty()){
            findViewById(R.id.fee).setVisibility(View.GONE);
        }
        if(findViewById(R.id.cost).getVisibility()==View.GONE && findViewById(R.id.fee).getVisibility()==View.GONE){
            //mBtnApply.setVisibility(View.INVISIBLE);
            //((TextView) findViewById(R.id.txt_description)).setVisibility(View.INVISIBLE);
        }

        ((TextView)findViewById(R.id.tvFee)).setText(mMembership.fee_cost.isEmpty() ? "$00.00" : "$"+FormatUtil.formatString(mMembership.fee_cost));
        ((TextView)findViewById(R.id.tvFeeType)).setText("per "+ (mMembership.fee_cost_type.isEmpty() ? "NA" :mMembership.fee_cost_type));

        ((TextView) findViewById(R.id.txt_description)).setText(mMembership.description!=null ? mMembership.description : "$00.00");
    }

    private void actionLoadMembership() {
        showCustomDialogLoading();
        VolleyUtils.getSharedNetwork().loadMembership(this, mMembership.id, new OnResponseModel() {
            @Override
            public void onResponseSuccess(Object model) {
                dismissCustomDialogLoading();
                mMembership = JsonUtils.parseByModel(model.toString(), KZMembership.class);
                loadMembershipListOfUser(new CustomListener2() {
                    @Override
                    public void success() {
                        updateInfo();
                        showButtonWithStatus();
                    }

                    @Override
                    public void fail(BKNetworkResponseError error) {

                    }
                });

            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                dismissCustomDialogLoading();
                VolleyUtils.handleErrorRespond(MembershipDetailActivity.this, error);
            }
        });
    }

    private void actionApply() {
        if (mMembership.isApply) {
            Analytics.with(MembershipDetailActivity.this).track("Click remove_btn");

            DialogUtils.showMessageDialog(this, true,
                    getString(R.string.title_remove_membership),
                    getString(R.string.confirm_remove_membership),
                    getString(R.string.btn_yes),
                    getString(R.string.btn_no),
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            actionCallApiRemoveMembership();
                        }
                    },
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.cancel();
                        }
                    });

        } else {
            Analytics.with(MembershipDetailActivity.this).track("Click apply_btn");

            if(!mMembership.fee_cost.isEmpty() && Double.parseDouble(mMembership.fee_cost)>0) {
                String strMembershipFeeCost = FormatUtil.formatString(!mMembership.fee_cost.isEmpty() ? mMembership.fee_cost : "0");
                String strMembershipType = !mMembership.fee_cost_type.isEmpty() ? mMembership.fee_cost_type : getString(R.string.text_month);
                String message = getString(R.string.text_memebership_cost) + "\n$" + strMembershipFeeCost + "/" + strMembershipType;

                DialogUtils.showMessageDialog(this, true,
                        getString(R.string.title_apply_membership),
                        message,
                        getString(R.string.btn_i_in),
                        getString(R.string.btn_cancel),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                actionCallApiApplyMembership();
                            }
                        },
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.cancel();
                            }
                        });
            }else {
                actionCallApiApplyMembership();
            }
        }
    }

    private void actionCallApiRemoveMembership() {
        showCustomDialogLoading();
        VolleyUtils.getSharedNetwork().removeMembership(this, mMembership.id, new OnResponseModel() {
            @Override
            public void onResponseSuccess(Object model) {
                dismissCustomDialogLoading();
                MembershipDetailActivity.this.finish();
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                dismissCustomDialogLoading();
                VolleyUtils.handleErrorRespond(MembershipDetailActivity.this, error);
            }
        });
    }

    private void actionCallApiApplyMembership() {
        showCustomDialogLoading();
        VolleyUtils.getSharedNetwork().applyMembership(this, mMembership.id, new OnResponseModel() {
            @Override
            public void onResponseSuccess(Object model) {
                dismissCustomDialogLoading();
                MembershipDetailActivity.this.finish();
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                dismissCustomDialogLoading();
                VolleyUtils.handleErrorRespond(MembershipDetailActivity.this, error);
            }
        });
    }

    private void showButtonWithStatus() {
        if (mMembership.isApply) {
            mBtnApply.setText(getString(R.string.btn_remove));
        } else {
            mBtnApply.setText(getString(R.string.btn_apply));
        }
    }

    private void loadMembershipListOfUser(final CustomListener2 customListener2) {
        VolleyUtils.getSharedNetwork().loadMembershipListOfUser(this, new OnResponseModel<JsonArray>() {
            @Override
            public void onResponseSuccess(JsonArray array) {
                dismissCustomDialogLoading();

                for (int i = 0; i < array.size(); i++) {
                    Gson gson = new Gson();
                    JsonElement s = array.get(i);
                    KZMembership membershipOfUser = gson.fromJson(s , KZMembership.class);

                    if (mMembership.id.equals(membershipOfUser.id)) {
                        mMembership.isApply = true;
                        break;
                    }
                }

                if(customListener2!=null) customListener2.success();
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                dismissCustomDialogLoading();
                VolleyUtils.handleErrorRespond(MembershipDetailActivity.this, error);
                if(customListener2!=null) customListener2.fail(error);
            }
        });
    }

    public void onHeaderBackPress(View view) {
        onBackPressed();
    }
}














































