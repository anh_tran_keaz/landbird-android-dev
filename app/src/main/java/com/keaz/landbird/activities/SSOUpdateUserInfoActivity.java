package com.keaz.landbird.activities;

/**
 * Created by anhtran1810 on 3/30/18.
 */

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.keaz.landbird.R;
import com.keaz.landbird.interfaces.CustomListener5;
import com.keaz.landbird.models.KZAccount;
import com.keaz.landbird.models.KZCostCentre;
import com.keaz.landbird.models.UserModel;
import com.keaz.landbird.utils.BKNetworkResponseError;
import com.keaz.landbird.utils.ConstantsUtils;
import com.keaz.landbird.utils.CostCenterUtil;
import com.keaz.landbird.utils.CountryCodeUtils;
import com.keaz.landbird.utils.DialogUtils;
import com.keaz.landbird.utils.EditUtil;
import com.keaz.landbird.utils.LoadingViewUtils;
import com.keaz.landbird.utils.OnResponseModel;
import com.keaz.landbird.utils.ParseJSonUtil;
import com.keaz.landbird.utils.ValidateUtils;
import com.keaz.landbird.utils.VolleyUtils;
import com.keaz.landbird.view.CustomFontTextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class SSOUpdateUserInfoActivity extends BaseActivity implements View.OnClickListener {

    private static final String TAG = SSOUpdateUserInfoActivity.class.getSimpleName();

    private EditText etPhone;
    private EditText etSurName, etName;
    private EditText etRFID;
    private String countryCode = ConstantsUtils.DEFAULT_COUNTRY_CODE;
    private TextView tvCountryCodeValue;
    private LinearLayout llCountryCode;
    private ArrayList<CountryCodeModels> mCountryCodeModels;

    private ScrollView scrollview;
    private UserModel kzUser;
    private CustomFontTextView tvCostCenter;
    private List<KZCostCentre> costCentres;
    private int costCenterID = -1;
    private SSOUpdateUserInfoActivity getThis() {
        return this;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sso_update_info);

        initUI();
        iniListener();
        setupActionEditText();
        setupInitialInfo();

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnNext:
                actionClickNext();
                break;
            case R.id.imv_back:
                DialogUtils.showMessageDialog(getThis(),
                        false,
                        "",
                        getString(R.string.confirm_to_exit),
                        getString(R.string.btn_yes),
                        getString(R.string.btn_no),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.cancel();
                                Intent intent = new Intent(getThis(), LoginActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                finish();
                            }
                        },
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.cancel();
                            }
                        });
                break;
        }
    }
    @Override
    public void onBackPressed() {
        finish();
    }

    private void setupActionEditText() {
        etRFID.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    actionClickNext();
                }
                return false;
            }
        });
    }

    public void initUI() {
        scrollview = (ScrollView)findViewById(R.id.scrollView);
        etPhone = (EditText) findViewById(R.id.et_phone_or_email);
        etName = (EditText)findViewById(R.id.et_name);
        etSurName = (EditText)findViewById(R.id.et_surname);
        etRFID = (EditText)findViewById(R.id.et_rfid);
        llCountryCode = (LinearLayout) findViewById(R.id.ll_countryCode2);
        tvCountryCodeValue = (TextView) findViewById(R.id.tvCountryCodeValue);
        tvCostCenter = (CustomFontTextView) findViewById(R.id.tv_str_cost_center);
    }

    private void iniListener() {
        findViewById(R.id.ll_costcenter).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CostCenterUtil.showPopUpCostCenterMenu(SSOUpdateUserInfoActivity.this, tvCostCenter, costCentres, new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        tvCostCenter.setText(item.getTitle().toString());
                        costCenterID = costCentres.get(item.getItemId()).id;
                        return true;
                    }
                });
            }
        });
        llCountryCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopUpMenu();
            }
        });
        findViewById(R.id.btnNext).setOnClickListener(this);
        findViewById(R.id.imv_back).setOnClickListener(this);
    }

    private void setupInitialInfo() {
        kzUser = KZAccount.getSharedAccount().getUser();

        ((TextView)findViewById(R.id.tvTittle)).setText("Hello, " + kzUser.name + " " + kzUser.last_name);

        EditUtil.setTextOrHint(etName, kzUser.name, "Enter your first name");
        EditUtil.setTextOrHint(etSurName, kzUser.last_name, "Enter your surname");
        EditUtil.setTextOrHint(etRFID, kzUser.rfid, "Enter your RFID");
        EditUtil.setTextOrHint(etPhone, kzUser.contact_number, "Enter your contact number");
        EditUtil.setTextOrDefaultText(tvCountryCodeValue, CountryCodeUtils.getCountryCodeText(kzUser.country_code), "Select your contact number country code");

        CostCenterUtil.iniCostCenterInfo(this, new CustomListener5() {
            @Override
            public void actionBeforeCallApi() {
                LoadingViewUtils.showOrHideProgressBar(true, SSOUpdateUserInfoActivity.this);
            }

            @Override
            public void actionCallApiSuccess(List<KZCostCentre> c) {
                LoadingViewUtils.showOrHideProgressBar(false, SSOUpdateUserInfoActivity.this);
                costCentres = c;
                EditUtil.setTextOrDefaultText(tvCostCenter, CostCenterUtil.getCostCenterTittle(kzUser.ccs, costCentres), "Select cost center");
            }

            @Override
            public void actionCallApiFail(BKNetworkResponseError error) {
                LoadingViewUtils.showOrHideProgressBar(false, SSOUpdateUserInfoActivity.this);
                VolleyUtils.handleErrorRespond(SSOUpdateUserInfoActivity.this, error);
            }
        });

    }

    public void actionClickNext() {
        boolean validate = validateInput();
        if (!validate) return;

        actionUpdateInfo();
    }

    private void actionUpdateInfo( ) {

        final JSONObject json = new JSONObject();
        try {
            json.put("name", etName.getText().toString());
            json.put("last_name", etSurName.getText().toString());
            json.put("rfid", etRFID.getText().toString());
            json.put("contact_number_second", etPhone.getText().toString());
            json.put("country_code", countryCode);
            if(costCenterID!=-1){
                JSONArray jsonArray = CostCenterUtil.getJsonArray(kzUser.ccs);
                jsonArray.put(costCenterID);
                json.put("ccs", jsonArray);
            }
            LoadingViewUtils.showOrHideProgressBar(true, SSOUpdateUserInfoActivity.this);
            VolleyUtils.getSharedNetwork().updateUserInfo(SSOUpdateUserInfoActivity.this,KZAccount.getSharedAccount().getUser().id+"", json, new OnResponseModel() {
                @Override
                public void onResponseSuccess(Object model) {
                    LoadingViewUtils.showOrHideProgressBar(false, SSOUpdateUserInfoActivity.this);
                    JsonObject jsonObject = (JsonObject) model;
                    UserModel userModel = ParseJSonUtil.parseByModel(jsonObject.toString(), UserModel.class);
                    saveUser(userModel);

                    setResult(RESULT_OK);
                    finish();

                }

                @Override
                public void onResponseError(BKNetworkResponseError error) {
                    VolleyUtils.handleErrorRespond(SSOUpdateUserInfoActivity.this, error);
                    LoadingViewUtils.showOrHideProgressBar(false, SSOUpdateUserInfoActivity.this);
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
        }
    }

    private void showPopUpMenu() {
        PopupMenu popup = new PopupMenu(this, llCountryCode);
        //Inflating the Popup using xml file
        popup.getMenuInflater().inflate(R.menu.empty_menu, popup.getMenu());

        //Hard code for example
        mCountryCodeModels = new ArrayList<>();
//        mCountryCodeModels.add(new CountryCodeModels("US +1", "1"));
//        mCountryCodeModels.add(new CountryCodeModels("NZ +64", "64"));
        mCountryCodeModels.add(new CountryCodeModels("AU +61", "61"));
//        mCountryCodeModels.add(new CountryCodeModels("VN +84", "84"));


        //Programmatically add item
        Menu menu = popup.getMenu();
        for (int i = 0; i < mCountryCodeModels.size(); i++) {
            menu.add(0, i, i, mCountryCodeModels.get(i).getValue());
        }

        //registering popup with OnMenuItemClickListener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                tvCountryCodeValue.setText(item.getTitle().toString());
                countryCode = mCountryCodeModels.get(item.getItemId()).getNumber();
                return true;
            }
        });

        popup.show();
    }

    private boolean validateInput() {
        //Validate email/contact_number_second

        if (!etPhone.getText().toString().equals("")) {
            if(!ValidateUtils.validatePhone(etPhone.getText().toString())){
                Toast.makeText(this, getString(R.string.error_format_phone), Toast.LENGTH_SHORT).show();
                scrollview.fullScroll(ScrollView.FOCUS_UP);
                return false;
            }
        }else {
            Toast.makeText(this, getString(R.string.error_empty_phone), Toast.LENGTH_SHORT).show();
            scrollview.fullScroll(ScrollView.FOCUS_UP);
            return false;
        }

        //Validate name
        if(etName.getText().toString().isEmpty()){
            Toast.makeText(this, getString(R.string.error_empty_name), Toast.LENGTH_SHORT).show();
            scrollview.fullScroll(ScrollView.FOCUS_UP);
            etName.requestFocus();
            return false;
        }

        /*if(etSurName.getText().toString().isEmpty()){
            Toast.makeText(this, getString(R.string.error_empty_surname), Toast.LENGTH_SHORT).show();
            scrollview.fullScroll(ScrollView.FOCUS_UP);
            etSurName.requestFocus();
            return false;
        }*/

        //Validate street address 1
        /*if(etStreetAddress1.getText().toString().isEmpty()){
            Toast.makeText(this, getString(R.string.error_empty_str_address_1), Toast.LENGTH_SHORT).show();
            scrollview.fullScroll(ScrollView.FOCUS_UP);
            etStreetAddress1.requestFocus();
            return false;
        }*/

        //Validate rfid
        /*if(etRFID.getText().toString().isEmpty()){
            Toast.makeText(this, getString(R.string.error_empty_rfid), Toast.LENGTH_SHORT).show1();
            scrollview.fullScroll(ScrollView.FOCUS_UP);
            etRFID.requestFocus();
            return false;
        }*/

        return true;
    }
}

