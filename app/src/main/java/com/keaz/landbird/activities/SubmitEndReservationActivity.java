package com.keaz.landbird.activities;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.Switch;

import com.google.gson.Gson;
import com.keaz.landbird.R;
import com.keaz.landbird.dialogs.ThanksDialog;
import com.keaz.landbird.models.KZAccount;
import com.keaz.landbird.models.KZBooking;
import com.keaz.landbird.models.KZExtraCost;
import com.keaz.landbird.models.KZObjectForMixpanel;
import com.keaz.landbird.models.KZVehicle;
import com.keaz.landbird.utils.BKGlobals;
import com.keaz.landbird.utils.BKNetworkResponseError;
import com.keaz.landbird.utils.CommonUtils;
import com.keaz.landbird.utils.ConfigUtil;
import com.keaz.landbird.utils.ConstantsUtils;
import com.keaz.landbird.utils.DateUtils;
import com.keaz.landbird.utils.FormatUtil;
import com.keaz.landbird.utils.JsonUtils;
import com.keaz.landbird.utils.LoadingViewUtils;
import com.keaz.landbird.utils.OnResponseModel;
import com.keaz.landbird.utils.UIUtils;
import com.keaz.landbird.utils.VolleyUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class SubmitEndReservationActivity extends BaseActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {

    public static KZBooking booking;
    private static boolean allowBackPress = true;
    private RadioButton ckYes;
    private RadioButton ckNo;
    private boolean updateSuccess = false;
    private int checkCount = 0;
    private boolean notDamageReportYet = true;
    private float endLat;
    private float endLng;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_submit_end_reservation);

        endLat = getIntent().getFloatExtra(ConstantsUtils.INTENT_EXTRA_END_BOOKING_LAT,0f);
        endLng = getIntent().getFloatExtra(ConstantsUtils.INTENT_EXTRA_END_BOOKING_LNG,0f);

        findViewById(R.id.rel_back).setOnClickListener(this);
        findViewById(R.id.submit).setOnClickListener(this);
        findViewById(R.id.submit).setEnabled(false);
        ((Button)findViewById(R.id.submit)).setTextColor(Color.rgb(204,204,204));
        UIUtils.setTextViewText(this, R.id.txtRegistration, String.valueOf(booking.vehicle.name));
        CheckBox ck1 = (CheckBox) findViewById(R.id.ck_1);
        CheckBox ck2 = (CheckBox) findViewById(R.id.ck_2);
        CheckBox ck3 = (CheckBox) findViewById(R.id.ck_3);
        ck1.setOnCheckedChangeListener(this);
        ck2.setOnCheckedChangeListener(this);
        ck3.setOnCheckedChangeListener(this);
        ckYes = (RadioButton) findViewById(R.id.yes);
        ckNo = (RadioButton) findViewById(R.id.no);

        ckYes.setChecked(true);
        ckNo.setChecked(false);

        ckYes.setOnCheckedChangeListener(this);
        ckNo.setOnCheckedChangeListener(this);

        ((Switch)findViewById(R.id.switch1)).setChecked(true);
        ((Switch)findViewById(R.id.switch1)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    actionOpenDamageReport();
                }
            }
        });
    }
    @Override
    protected void onStart() {
        super.onStart();
        CommonUtils.segmentScreenWithBooking(this, "End Reservation", booking);
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rel_back:
                SubmitEndReservationActivity.this.onBackPressed();
                break;
            case R.id.submit:
                if(((Switch)findViewById(R.id.switch1)).isChecked()/*((RadioButton)findViewById(R.id.yes)).isChecked()*/ && notDamageReportYet){
                    actionOpenDamageReport();
                    return;
                }

                final AlertDialog.Builder builder ;
                builder = new AlertDialog.Builder(this);
                builder.setTitle("");
                builder.setMessage(getString(R.string.confirm_lock_car));
                builder.setCancelable(true);
                builder.setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        handleEndBooking();
                    }
                });
                builder.create().show();
                break;
        }
    }
    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.ck_1:
            case R.id.ck_3:
            case R.id.ck_2:
                if (isChecked) {
                    checkCount++;
                } else {
                    checkCount--;
                }
                break;
            case R.id.yes:
                if (isChecked) {
                    actionOpenDamageReport();
                }
                break;
            case R.id.no:
                break;
            default:
                break;
        }
        if(checkCount == 2) {
            findViewById(R.id.submit).setEnabled(true);
            ((Button)findViewById(R.id.submit)).setTextColor(Color.rgb(255,255,255));

        } else {
            findViewById(R.id.submit).setEnabled(false);
            ((Button)findViewById(R.id.submit)).setTextColor(Color.rgb(204,204,204));
        }
    }

    private void actionOpenDamageReport() {
        Intent intent = new Intent(SubmitEndReservationActivity.this, ReportProblemActivity.class);
        intent.putExtra("from", SubmitEndReservationActivity.class.getSimpleName());
        ReportProblemActivity.booking = SubmitEndReservationActivity.booking;
        ReportProblemActivity.report = "";
        startActivityForResult(intent, 997);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 997) {
            if (resultCode == Activity.RESULT_CANCELED) {
                ckYes.setChecked(false);
                ckNo.setChecked(true);

                ((Switch)findViewById(R.id.switch1)).setChecked(false);
            }else {
                notDamageReportYet = false;
            }
        }
    }
    @Override
    public void onBackPressed() {
        if(allowBackPress){
            if (updateSuccess) {
                setResult(RESULT_OK);
            }
            super.onBackPressed();
        }
    }

    private void loadUserForMixPanel(final JSONObject jsonObject) {
        showOrHideProgressBar(true, SubmitEndReservationActivity.this);
        VolleyUtils.getSharedNetwork().loadUserForMixpanel(SubmitEndReservationActivity.this, "" + booking.user.id, new OnResponseModel() {
            @Override
            public void onResponseSuccess(Object model) {
                showOrHideProgressBar(false, SubmitEndReservationActivity.this);
                Gson gson = new Gson();
                KZObjectForMixpanel kzObjectForMixpanel = gson.fromJson(model.toString(), KZObjectForMixpanel.class);

                CommonUtils.segmentIdentifyWithExpandUser(SubmitEndReservationActivity.this, KZAccount.getSharedAccount().user, kzObjectForMixpanel, Float.valueOf(booking.cost_charged));
                CommonUtils.segmentTrackWithBooking(SubmitEndReservationActivity.this, "Click Submit_btn", booking);

                showThanksDialog(jsonObject);

                findViewById(R.id.submit).setVisibility(View.GONE);
                updateSuccess = true;
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                showOrHideProgressBar(false, SubmitEndReservationActivity.this);
                VolleyUtils.handleErrorRespond(SubmitEndReservationActivity.this, error);
            }
        });
    }

    private void handleEndBooking() {
        showOrHideProgressBar(true, this);
        final int bookingId = booking.id;
        int vehicleId = booking.vehicle.id;
        /*VolleyUtils.getSharedNetwork().getVehicleById(SubmitEndReservationActivity.this, vehicleId, new OnResponseModel<JSONObject>() {
            @Override
            public void onResponseSuccess(JSONObject model) {
                showOrHideProgressBar(false, SubmitEndReservationActivity.this);
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("state", BKGlobals.BookingStatus.COMPLETE.toString());
                    jsonObject.put("date_created", booking.date_created);
                    jsonObject.put("date_updated", booking.date_updated);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                actionUpdateBooking(bookingId, jsonObject);
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                showOrHideProgressBar(false, SubmitEndReservationActivity.this);
                VolleyUtils.handleErrorRespond(SubmitEndReservationActivity.this, error);
            }
        });*/
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("state", BKGlobals.BookingStatus.COMPLETE.toString());
            jsonObject.put("date_created", booking.date_created);
            jsonObject.put("date_updated", booking.date_updated);
            //if(endLat!=0 & endLng!=0){
            jsonObject.put("lat", endLat);
            jsonObject.put("lng", endLng);
            //}

        } catch (JSONException e) {
            e.printStackTrace();
        }
        actionUpdateBooking(bookingId, jsonObject);
    }

    private void actionUpdateBooking(int bookingId, JSONObject jsonObject) {
        showOrHideProgressBar(true, this);
        VolleyUtils.getSharedNetwork().updateEndBooking(SubmitEndReservationActivity.this, bookingId, jsonObject, new OnResponseModel<JSONObject>() {
            @Override
            public void onResponseSuccess(JSONObject model) {
                showOrHideProgressBar(false, SubmitEndReservationActivity.this);
                booking = JsonUtils.parseByModel(model.toString(), KZBooking.class);
                loadUserForMixPanel(model);
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                showOrHideProgressBar(false, SubmitEndReservationActivity.this);
                updateSuccess = false;

                if(error != null) {
                    if (error.code != null && error.code.equals(ConstantsUtils.ERROR_CODE)) {
                        showAlertDialog("", error.description, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                showOrHideProgressBar(true, SubmitEndReservationActivity.this);
                                final String currentBookingStatus = booking.state;
                                VolleyUtils.getSharedNetwork().loadBookingByBookingId(SubmitEndReservationActivity.this, booking.id,
                                        new OnResponseModel<KZBooking>() {

                                            @Override
                                            public void onResponseSuccess(KZBooking model) {
                                                showOrHideProgressBar(false, SubmitEndReservationActivity.this);
                                                CommonUtils.handleOpenBooking(SubmitEndReservationActivity.this, currentBookingStatus, model);
                                            }

                                            @Override
                                            public void onResponseError(BKNetworkResponseError error) {
                                                showOrHideProgressBar(false, SubmitEndReservationActivity.this);
                                                VolleyUtils.handleErrorRespond(SubmitEndReservationActivity.this, error);
                                            }
                                        });
                            }
                        });
                    } else {
                        showAlertDialog("", getString(R.string.error_from_server));
                    }
                }else {
                    showAlertDialog("", getString(R.string.error_from_server));
                }
            }
        });
    }

    private void gotoMainScreen() {
        Intent intent = new Intent(SubmitEndReservationActivity.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        SubmitEndReservationActivity.this.finish();
    }

    private void showThanksDialog(JSONObject bookingJSON) {
        ThanksDialog dialog = new ThanksDialog(this, booking);
        dialog.setOnThanksDialogListener(new ThanksDialog.OnThanksDialogListener() {
            @Override
            public void onOK() {
                gotoMainScreen();
            }
        });
        dialog.show();
        String str_vehicle = bookingJSON.optString("vehicle");

        KZVehicle vehicle = JsonUtils.parseJsonObject(KZVehicle.class, str_vehicle);
        String registration = vehicle.name;
        String duration = DateUtils.getDurationFormatText1(bookingJSON.optLong("actual_start"), bookingJSON.optLong("actual_end"));
        String startTime = DateUtils.getTimeFromSec(bookingJSON.optLong("actual_start") - DateUtils.getTimeZoneOffset() - bookingJSON.optLong("start_timezone_offset"));
        String endTime = DateUtils.getTimeFromSec(bookingJSON.optLong("actual_end") - DateUtils.getTimeZoneOffset() - bookingJSON.optLong("end_timezone_offset"));
        String time = startTime + " -\n " + endTime;
        String photo = vehicle.assets.photo;

        String subTotal = "$" + FormatUtil.formatString(bookingJSON.optString("cost_total"));
        String cost_credit = bookingJSON.optString("cost_credit");
        String promote;
        if(cost_credit.isEmpty() || Double.parseDouble(cost_credit)==0) {
            promote = null;
        }else {
            promote = "-$" + FormatUtil.formatString(cost_credit);
        }

        String membership_discount_cost = bookingJSON.optString("membership_discount_cost");
        if(membership_discount_cost.isEmpty()) {
            membership_discount_cost = "0";
        }

        String discountMembership = Double.parseDouble(membership_discount_cost)!=0 ? "-$" + FormatUtil.formatString(membership_discount_cost) : "$0.00";
        String total = "$" + FormatUtil.formatString(bookingJSON.optString("cost_charged"));

        String cost_gst = bookingJSON.optString("cost_gst");
        String gst;
        if (cost_gst.isEmpty()  || Double.parseDouble(cost_gst)==0){
            gst = null;
        }else {
            gst = "$" + FormatUtil.formatString(cost_gst);
        }

        if(ConfigUtil.actionDefineTripType().equals(ConstantsUtils.TRIP_TYPE_BUSINESS)){
            total = null;
            subTotal = null;
            promote = null;
            discountMembership = null;
            gst = null;
        }

        String tripType = bookingJSON.optString("trip_type");
        String concierge_delivery = null;
        String concierge_collection = null;
        if(ConstantsUtils.TRIP_TYPE_PRIVATE.equals(bookingJSON.optString("trip_type", ""))) {
            if(bookingJSON.optBoolean("concierge_delivery_enable", false)){
                String c1 = bookingJSON.optString("concierge_delivery_cost");
                if (TextUtils.isEmpty(c1)) {
                    concierge_delivery = "$0.00";
                } else {
                    concierge_delivery = "$" + FormatUtil.formatString(""+c1);
                }
            }
            if(bookingJSON.optBoolean("concierge_collection_enable", false)){
                String c2 = bookingJSON.optString("concierge_collection_cost");
                if (TextUtils.isEmpty(c2)) {
                    concierge_collection = "$0.00";
                } else {
                    concierge_collection = "$" + FormatUtil.formatString(""+c2);
                }
            }
        }

        ArrayList<KZExtraCost> extraCosts = new ArrayList<>();
        try {
            JSONArray jsonArrayExtraCost = bookingJSON.optJSONArray("extra_costs");
            if(null != jsonArrayExtraCost && jsonArrayExtraCost.length() > 0){
                for (int i = 0; i<jsonArrayExtraCost.length(); i++){
                    JSONObject jsonObjectExtraCost = jsonArrayExtraCost.optJSONObject(i);
                    if(null != jsonObjectExtraCost && jsonObjectExtraCost.has("text")
                            && jsonObjectExtraCost.has("cost")){
                        KZExtraCost kzExtraCost = new KZExtraCost();
                        kzExtraCost.text = jsonObjectExtraCost.optString("text");
                        kzExtraCost.cost = FormatUtil.formatString(""+jsonObjectExtraCost.optString("cost"));

                        extraCosts.add(kzExtraCost);
                    }
                }
            }
        } catch (Exception ex){
            ex.printStackTrace();
        }

        dialog.setupData(photo, registration, duration,
                time, total, subTotal, promote,
                discountMembership, gst,
                concierge_delivery, concierge_collection,
                extraCosts);
    }

    public static void showOrHideProgressBar(boolean b, Activity activity) {
        allowBackPress = !LoadingViewUtils.showOrHideProgressBar(b, activity);
    }
}
