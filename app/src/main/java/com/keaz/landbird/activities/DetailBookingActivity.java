package com.keaz.landbird.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.keaz.landbird.R;
import com.keaz.landbird.models.KZBooking;
import com.keaz.landbird.models.KZBranch;
import com.keaz.landbird.utils.BKGlobals;
import com.keaz.landbird.utils.ColorUtil;
import com.keaz.landbird.utils.CommonUtils;
import com.keaz.landbird.utils.ConfigUtil;
import com.keaz.landbird.utils.ConstantsUtils;
import com.keaz.landbird.utils.DateUtils;
import com.keaz.landbird.utils.FormatUtil;
import com.keaz.landbird.utils.UIUtils;
import com.keaz.landbird.view.BatteryView;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * Created by keaz on 10/11/17.
 */

public class DetailBookingActivity extends BaseActivity {

    public static KZBooking booking;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_detail_booking);
        iniInfo();
        iniListener();
    }

    @Override
    protected void onStart() {
        super.onStart();
        CommonUtils.segmentScreenWithBooking(DetailBookingActivity.this, "Booking Detail", booking);
    }

    @Override
    public void onBackPressed() {
        finishAnim();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        booking = null;
    }

    private void iniListener() {
        findViewById(R.id.rel_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finishAnim();
            }
        });
        findViewById(R.id.tvBookingComment).setVisibility(View.GONE);
        /*findViewById(R.id.tvBookingComment).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BookingCommentsList.booking = booking;
                Intent intent = new Intent(DetailBookingActivity.this, BookingCommentsList.class);
                intent.putExtra(ConstantsUtils.INTENT_EXTRA_BOOKING_ID, booking.id);
                startActivity(intent);
            }
        });*/
        findViewById(R.id.tapForMoreDetail).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                findViewById(R.id.tapForMoreDetail).setVisibility(View.GONE);
                findViewById(R.id.viewEstimatedCost).setVisibility(View.VISIBLE);
            }
        });
    }

    private void finishAnim() {
        boolean backToHome = getIntent().getBooleanExtra(ConstantsUtils.INTENT_EXTRA_BACK_TO_HOME, false);
        if(backToHome){
            Intent intent = new Intent(DetailBookingActivity.this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP| Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        }else {
            finish();
            overridePendingTransition(R.anim.fade_in_normal, R.anim.fade_out_normal);
        }

    }

    private void iniInfo() {
        String photo = booking.vehicle.assets.photo /*getIntent().getStringExtra(ConstantsUtils.INTENT_EXTRA_CAR_URL)*/;
        KZBranch kzBranch = booking.branch /*getIntent().getParcelableExtra(ConstantsUtils.INTENT_EXTRA_BRANCH)*/;
        String booking_status = booking.state /*getIntent().getStringExtra(ConstantsUtils.INTENT_EXTRA_BOOKING_STATUS)*/;

        long startTimeStampInSec = booking.actual_start - DateUtils.getTimeZoneOffset() - booking.start_timezone_offset;
        long endTimeStampInSec = booking.actual_end - DateUtils.getTimeZoneOffset() - booking.start_timezone_offset;

        ((TextView)findViewById(R.id.tv_booking_status)).setText(booking_status.toUpperCase());
        //((TextView)findViewById(R.id.tv_booking_status)).setTextColor(ColorUtil.getTextColorOfBooking(this, booking_status));
        ((TextView)findViewById(R.id.tv_booking_status)).setBackground(ColorUtil.getTextBackgroundOfBooking(DetailBookingActivity.this, booking.state));

        ((TextView)findViewById(R.id.tv_car_name)).setText(booking.vehicle.name);
        ((TextView)findViewById(R.id.tv_car_id)).setText(booking.vehicle.registration);
        /*switch (ConfigUtil.checkIfShowCarRegistration0(booking)){
            case CHECK:
                if(ConfigUtil.checkIfShowCarRegistration(booking)){
                    ((TextView)findViewById(R.id.tv_car_name)).setText(booking.vehicle.name);
                    ((TextView)findViewById(R.id.tv_car_id)).setText(booking.vehicle.registration);
                }else {
                    ((TextView)findViewById(R.id.tv_car_name)).setText(booking.vehicle_virtual.name);
                    ((TextView)findViewById(R.id.tv_car_id)).setText("");
                }
                break;
            case SHOW_REAL_CAR:
                ((TextView)findViewById(R.id.tv_car_name)).setText(booking.vehicle.name);
                ((TextView)findViewById(R.id.tv_car_id)).setText(booking.vehicle.registration);
                break;
            case SHOW_VIRTUAL_CAR:
                ((TextView)findViewById(R.id.tv_car_name)).setText(booking.vehicle_virtual.name);
                ((TextView)findViewById(R.id.tv_car_id)).setText("");
                break;
        }*/

        ((TextView)findViewById(R.id.tv_title_time_from)).setText(new SimpleDateFormat(ConstantsUtils.TIME_FORMAT_1 /*ConstantsUtils.FORMAT_TIME*/,Locale.getDefault()).format(startTimeStampInSec*1000));
        ((TextView)findViewById(R.id.tv_title_day_from)).setText(new SimpleDateFormat(ConstantsUtils.FORMAT_DATE,Locale.getDefault()).format(startTimeStampInSec*1000));
        ((TextView)findViewById(R.id.tv_title_time_to)).setText(new SimpleDateFormat(ConstantsUtils.TIME_FORMAT_1 /*ConstantsUtils.FORMAT_TIME*/,Locale.getDefault()).format(endTimeStampInSec*1000));
        ((TextView)findViewById(R.id.tv_title_day_to)).setText(new SimpleDateFormat(ConstantsUtils.FORMAT_DATE,Locale.getDefault()).format(endTimeStampInSec*1000));
        //((TextView)findViewById(R.id.textView17)).setText(kzBranch.name);
        ((TextView)findViewById(R.id.tv_hub_name)).setText(kzBranch.name);
        ((TextView)findViewById(R.id.tv_hub_address)).setText(kzBranch.address);
        UIUtils.setTextViewText(this, R.id.txtBookingID , ""+booking.id );

        String battery = booking.vehicle.battery;
        int percent = (battery != null && !battery.equals("")) ? Integer.parseInt(battery) : 0;
        ((BatteryView) findViewById(R.id.battery)).drawByPercent(percent);
        ((TextView)findViewById(R.id.textPercent)).setText(percent + "%");
        showBookingInfoRelatedToCost();

        Picasso.with(this).load(photo)
                .placeholder(R.drawable.new_img_car)
                .error(R.drawable.new_img_car)
                .into((ImageView) findViewById(R.id.imvCar));

    }

    private void showBookingInfoRelatedToCost() {

        UIUtils.setTextViewText(this, R.id.txtVehicleFareRate, "$" + FormatUtil.formatString(booking.cost_sub) + "/" + booking.cost_type);
        UIUtils.setTextViewText(this, R.id.tvValueBookingCost, "$" + FormatUtil.formatString(""+booking.cost_before_gst));
        UIUtils.setTextViewText(this, R.id.tvValueCostTotal, "$" + (booking.cost_charged!=null?FormatUtil.formatString(""+booking.cost_charged):"0"));
        if(booking.cost_credit!=null){
            UIUtils.setTextViewText(this, R.id.txtPromote, Double.parseDouble(booking.cost_credit)!=0 ? "-$" + FormatUtil.formatString(booking.cost_credit) : "$0.00");
        }
        if(booking.membership_discount_cost!=null){
            UIUtils.setTextViewText(this, R.id.txtDiscountMembership, Double.parseDouble(booking.membership_discount_cost)!=0 ? "-$" + FormatUtil.formatString(booking.membership_discount_cost) : "$0.00");
        }

        if(findViewById(R.id.viewDuration)!=null) {
            findViewById(R.id.viewDuration).setVisibility(View.GONE);
        }

        if(booking.state.equals(BKGlobals.BookingStatus.CANCELLED.toString()) || booking.state.equals(BKGlobals.BookingStatus.EXPIRED.toString())){
            findViewById(R.id.viewTotalCost).setVisibility(View.GONE);
            findViewById(R.id.viewVehicleFareRate).setVisibility(View.GONE);
            findViewById(R.id.line).setVisibility(View.INVISIBLE);
        }

        ConfigUtil.CustomModel customModel = ConfigUtil.checkIfShowBookingCostWithBookingStatus(booking);
        if(customModel.showCost){
            findViewById(R.id.viewVehicleFareRate).setVisibility(View.VISIBLE);
            UIUtils.setTextViewText(this, R.id.txtVehicleFareRate, "$" + booking.cost_sub + "/" + booking.cost_type);

        }else {
            findViewById(R.id.line).setVisibility(View.GONE);
            findViewById(R.id.viewVehicleFareRate).setVisibility(View.GONE);
            findViewById(R.id.viewEstimatedCost).setVisibility(View.GONE);
            findViewById(R.id.viewTotalCost).setVisibility(View.GONE);
            findViewById(R.id.tapForMoreDetail).setVisibility(View.GONE);
        }
    }

    public DetailBookingActivity getThis() {
        return this;
    }
}
