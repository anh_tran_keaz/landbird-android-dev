package com.keaz.landbird.activities;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;

import com.google.gson.internal.LinkedTreeMap;
import com.keaz.landbird.R;
import com.keaz.landbird.utils.BKNetworkResponseError;
import com.keaz.landbird.utils.OnResponseModel;
import com.keaz.landbird.utils.VolleyUtils;
import com.keaz.landbird.view.CustomFontTextView;
import com.segment.analytics.Analytics;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class GetSocialActivity extends BaseActivity {

    private static final String TAG = GetSocialActivity.class.getSimpleName();
    private ListView listView;

    private ArrayList<LinkedTreeMap> socialLinkModels = new ArrayList();
    private SocialLinkAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_social);
        initUI();
        initData();
        initListener();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Analytics.with(this).screen("Get Social");
    }

    public void initUI() {
        listView = (ListView) findViewById(R.id.lv_social);
    }

    public void initData() {

        adapter = new SocialLinkAdapter(this, socialLinkModels);

        listView.setAdapter(adapter);

        showCustomDialogLoading();

        VolleyUtils.getSharedNetwork().loadSocialLink(
                GetSocialActivity.this,
                new OnResponseModel<Object>() {
            @Override
            public void onResponseSuccess(Object obj) {
                dismissCustomDialogLoading();
                socialLinkModels = (ArrayList<LinkedTreeMap>) obj;
                adapter = new SocialLinkAdapter(GetSocialActivity.this, socialLinkModels);
                listView.setAdapter(adapter);

                if(socialLinkModels.isEmpty()){
                    findViewById(R.id.tvEmpty).setVisibility(View.VISIBLE);
                }else {
                    findViewById(R.id.tvEmpty).setVisibility(View.GONE);
                }


            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                dismissCustomDialogLoading();
                VolleyUtils.handleErrorRespond(GetSocialActivity.this, error);
            }
        });

    }

    public void initListener() {
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(socialLinkModels.get(i).get("page").toString()));
                startActivity(browserIntent);
            }
        });
    }

    public void onHeaderBackPress(View view) {
        onBackPressed();
    }

    private class SocialLinkAdapter extends BaseAdapter {
        private final Context context;
        private final ArrayList<LinkedTreeMap> objects;

        SocialLinkAdapter(Context context, ArrayList<LinkedTreeMap> values) {
            this.context = context;
            this.objects = values;
        }

        @Override
        public int getCount() {
            return objects != null ? objects.size() : 0;
        }

        @Override
        public Object getItem(int i) {
            return objects.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if(convertView==null) convertView = inflater.inflate(R.layout.item_social_list, parent, false);

            CustomFontTextView textView = (CustomFontTextView) convertView.findViewById(R.id.tv_title);

            //textView.setCompoundDrawablesWithIntrinsicBounds(0, 0, iconList[position], 0);
            textView.setText(objects.get(position).get("name").toString());
            String url = objects.get(position).get("photo").toString();

            Picasso.with(context).load(url)/*.placeholder(R.drawable.img_vehicle_report_right)*/
                    /*.error(R.drawable.img_vehicle_report_right)*/
                    .into((ImageView)convertView.findViewById(R.id.imvIcon));

            return convertView;
        }
    }

}
