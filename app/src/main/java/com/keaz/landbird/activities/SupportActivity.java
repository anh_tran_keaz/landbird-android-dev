package com.keaz.landbird.activities;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.keaz.landbird.R;
import com.keaz.landbird.view.CustomFontTextView;

public class SupportActivity extends BaseActivity {

    public static final int FEES_TO_KNOW_ABOUT = 0;
    public static final int VEHICLE_GUIDE_ITEM = 1;
    public static final int HOW_TO_VIDEOS_ITEM = 2;
    public static final int CONTACT_US_ITEM = 3;

    private ListView listView;
    private Button mBtnCall;

    private SupportActivity.SimpleArrayAdapter adapter;

    private String[] dataList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_support);

        initUI();
        initData();
        initListener();
    }

    public void initUI() {
        listView = (ListView) findViewById(R.id.lv_support);

        mBtnCall = (Button) findViewById(R.id.btnCall);
        mBtnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:1(888)6100506"));
                startActivity(intent);
            }
        });
    }

    public void initData() {
        dataList = getResources().getStringArray(R.array.str_support_list);

        adapter = new SupportActivity.SimpleArrayAdapter(this, dataList);

        listView.setAdapter(adapter);


    }

    public void initListener() {
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                switch (position) {
                    case FEES_TO_KNOW_ABOUT:
                        startActivityWithName(FeesToKnowActivity.class);
                        break;

                    case VEHICLE_GUIDE_ITEM:
                        break;

                    case HOW_TO_VIDEOS_ITEM:
                        startActivityWithName(HowToVideosActivity.class);
                        break;

                    case CONTACT_US_ITEM:
                        startActivityWithName(ContactUsActivity.class);
                        break;

                    default:
                        break;
                }
            }
        });
    }

    public void onHeaderBackPress(View view) {
        onBackPressed();
    }

    public void onCallUsNowClick(View view) {


    }

    public void startActivityWithName(Class<?> tClass) {
        startActivity(new Intent(this, tClass));
    }

    private class SimpleArrayAdapter extends ArrayAdapter<String> {
        private final Context context;
        private final String[] values;

        public SimpleArrayAdapter(Context context, String[] values) {
            super(context, -1, values);
            this.context = context;
            this.values = values;
        }

        @Override
        public int getCount() {
            return values != null ? values.length : 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.item_account_list, parent, false);
            CustomFontTextView textView = (CustomFontTextView) rowView.findViewById(R.id.tv_title);

            textView.setText(values[position]);

            return rowView;
        }
    }
}
