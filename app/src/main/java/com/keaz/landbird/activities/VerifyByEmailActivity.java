package com.keaz.landbird.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.keaz.landbird.R;
import com.keaz.landbird.utils.BKNetworkResponseError;
import com.keaz.landbird.utils.ConstantsUtils;
import com.keaz.landbird.utils.OnResponseModel;
import com.keaz.landbird.utils.VolleyUtils;

/**
 * Created by mac on 2/22/17.
 */
public class VerifyByEmailActivity extends BaseActivity {


    private TextView mTvEmail;
    private LinearLayout mTvBackToLogIn;
    private Button mTvResendCode;
    private ProgressBar mProgressBar;
    private String mUserId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_verify_by_email);

        mUserId = getIntent().getStringExtra(ConstantsUtils.INTENT_EXTRA_ACCESS_USER_ID);
        String email = getIntent().getStringExtra(ConstantsUtils.INTENT_EXTRA_EMAIL);
        mTvEmail = (TextView) findViewById(R.id.tvEmail);
        mTvBackToLogIn = (LinearLayout) findViewById(R.id.tvBackToLogin);
        mTvResendCode = (Button)findViewById(R.id.btnResendCode);
        mProgressBar = (ProgressBar)findViewById(R.id.progressBar_horizontal);
        mTvResendCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onResendVerifyCode(true);
            }
        });

        findViewById(R.id.imvClose).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        mTvEmail.setText("Your email: " + email);

        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(getString(R.string.str_verify_confirm));
        ForegroundColorSpan foregroundColorSpan = new ForegroundColorSpan(Color.rgb(252, 86, 78));
        spannableStringBuilder.setSpan(foregroundColorSpan, 20, 30, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mTvResendCode.setText(spannableStringBuilder);

        mTvBackToLogIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(VerifyByEmailActivity.this, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        });
    }

    public void onResendVerifyCode(final boolean openDialog) {
        mProgressBar.setVisibility(View.VISIBLE);

        VolleyUtils.getSharedNetwork().resentActivateCode(this, mUserId, new OnResponseModel() {
            @Override
            public void onResponseSuccess(Object model) {
                mProgressBar.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                mProgressBar.setVisibility(View.INVISIBLE);
                VolleyUtils.handleErrorRespond(VerifyByEmailActivity.this, error);
            }
        });
    }

    @Override
    public void onBackPressed() {

    }
}
