package com.keaz.landbird.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.braintreepayments.api.BraintreeFragment;
import com.braintreepayments.api.Card;
import com.braintreepayments.api.PayPal;
import com.braintreepayments.api.exceptions.InvalidArgumentException;
import com.braintreepayments.api.interfaces.BraintreeCancelListener;
import com.braintreepayments.api.interfaces.BraintreeErrorListener;
import com.braintreepayments.api.interfaces.ConfigurationListener;
import com.braintreepayments.api.interfaces.PaymentMethodNonceCreatedListener;
import com.braintreepayments.api.models.CardBuilder;
import com.braintreepayments.api.models.Configuration;
import com.braintreepayments.api.models.PayPalAccountNonce;
import com.braintreepayments.api.models.PaymentMethodNonce;
import com.braintreepayments.api.models.PostalAddress;
import com.keaz.landbird.BuildConfig;
import com.keaz.landbird.R;
import com.keaz.landbird.dialogs.AddPaymentMethodDialog;
import com.keaz.landbird.interfaces.CustomListener;
import com.keaz.landbird.models.KZAccount;
import com.keaz.landbird.models.KZCreditCard;
import com.keaz.landbird.models.KZPaymentDetail;
import com.keaz.landbird.models.KZPaymentMethod;
import com.keaz.landbird.models.KZPaypal;
import com.keaz.landbird.utils.BKNetworkResponseError;
import com.keaz.landbird.utils.ConstantsUtils;
import com.keaz.landbird.utils.DialogUtils;
import com.keaz.landbird.utils.LoadingViewUtils;
import com.keaz.landbird.utils.OnResponseModel;
import com.keaz.landbird.utils.VolleyUtils;
import com.keaz.landbird.view.CustomFontTextView;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.segment.analytics.Analytics;
import com.segment.analytics.Properties;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import io.card.payment.CardIOActivity;
import io.card.payment.CreditCard;

import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class AddPaymentActivity extends BaseActivity implements
        AddPaymentMethodDialog.OnAddPaymentMethod,
        ConfigurationListener,
        PaymentMethodNonceCreatedListener,
        BraintreeErrorListener,
        ActivityCompat.OnRequestPermissionsResultCallback,
        BraintreeCancelListener, View.OnClickListener {

    private static final int MY_SCAN_REQUEST_CODE = 103;
    private static final int ADD_CREDIT_CARD_CODE = 104;
    private static final String TAG = AddPaymentActivity.class.getSimpleName();
    private static final String KEY_AUTHORIZATION = "com.braintreepayments.demo.KEY_AUTHORIZATION";
    private static boolean allowBackPress = true;;
    protected String mAuthorization;
    protected BraintreeFragment mBraintreeFragment;
    private ListView listView;
    private PaymentAdapter adapter;
    private ArrayList<KZPaymentMethod> paymentMethods;
    private KZPaymentDetail paymentDetail;
    private View headerView, footerView;
    private TextView btVerify;
    private boolean openFromRegister;
    private boolean openFromMakeBooking = false; // Chưa dùng đến
    private String selectedPaymentMethodId;
    private int paymentURL;
    private Configuration configuration;
    private boolean isCVVRequired;
    private boolean isPostalCodeRequired;

    private AddPaymentActivity getThis() {
        return AddPaymentActivity.this;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_payment);

        if (savedInstanceState != null && savedInstanceState.containsKey(KEY_AUTHORIZATION)) {
            mAuthorization = savedInstanceState.getString(KEY_AUTHORIZATION);
        }
        iniIntentInfo();
        initUI();
        initData();
        initListener();
    }
    @Override
    protected void onStart() {
        super.onStart();

        SimpleDateFormat sdf = new SimpleDateFormat(ConstantsUtils.FORMAT_SEGMENT_DATE_TIME);
        String currentDateandTime = sdf.format(new Date());
        Analytics.with(this).screen("Add Payment Method", new Properties().putValue("Date", currentDateandTime));
    }
    @Override
    protected void onResume() {
        super.onResume();
        iniRequestPermission();
    }
    @Override
    public void onBackPressed() {
        if(allowBackPress){
            if(openFromRegister || openFromMakeBooking){
                actionClickBtnOk();
            }else {
                finish();
            }
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        String resultDisplayStr = "";
        Bitmap cardTypeImage = null;
        if (requestCode == MY_SCAN_REQUEST_CODE) {

            if (data != null && data.hasExtra(CardIOActivity.EXTRA_SCAN_RESULT)) {
                final CreditCard scanResult = data.getParcelableExtra(CardIOActivity.EXTRA_SCAN_RESULT);

                // Never log a raw card number. Avoid displaying it, but if necessary use getFormattedCardNumber()
                resultDisplayStr = "Card Number: " + scanResult.getRedactedCardNumber() + "\n";

                if (scanResult.isExpiryValid()) {
                    resultDisplayStr += "Expiration Date: " + scanResult.expiryMonth + "/" + scanResult.expiryYear + "\n";
                }

                if (scanResult.cvv != null) {
                    // Never log or display a CVV
                    resultDisplayStr += "CVV has " + scanResult.cvv.length() + " digits.\n";
                }

                if (scanResult.postalCode != null) {
                    resultDisplayStr += "Postal Code: " + scanResult.postalCode + "\n";
                }

                final String cardNumber = scanResult.getFormattedCardNumber().replaceAll(" ", "");

                final String cvv;
                if (scanResult.cvv != null)
                    cvv = scanResult.cvv;
                else
                    cvv = "";
                final String postalCode;
                if (scanResult.postalCode != null)
                    postalCode = scanResult.postalCode;
                else
                    postalCode = "";

                CardBuilder cardBuilder = new CardBuilder()
                        .cardNumber(cardNumber)
                        .expirationMonth(String.valueOf(scanResult.expiryMonth))
                        .expirationYear(String.valueOf(scanResult.expiryYear))
                        .cvv(cvv)
                        .postalCode(postalCode);

                Card.tokenize(mBraintreeFragment, cardBuilder);

                showOrHideProgressBar(true, this);
                actionCallApiLoadPaymentMethodList();

            } else if (data != null && data.hasExtra(CardIOActivity.EXTRA_RETURN_CARD_IMAGE) && data.getBooleanExtra(CardIOActivity.EXTRA_RETURN_CARD_IMAGE, false)) {

                /*ByteArrayOutputStream scaledCardBytes = new ByteArrayOutputStream();
                Log.d(TAG, "[IMGCAP] Attempting return of image");
                mOverlay.getBitmap().compress(Bitmap.CompressFormat.JPEG, 80, scaledCardBytes);
                data.putExtra(CardIOActivity.EXTRA_CAPTURED_CARD_IMAGE, scaledCardBytes.toByteArray());*/
            } else {
                resultDisplayStr = "Scan was canceled.";
            }
            // do something with resultDisplayStr, maybe display it in a textView
            // resultTextView.setText(resultDisplayStr);
            /*Bitmap card = CardIOActivity.getCapturedCardImage(data);
            mResultImage.setImageBitmap(card);*/
            Log.d(TAG, "result : " + resultDisplayStr);

        } else if (requestCode == ADD_CREDIT_CARD_CODE) {
            showOrHideProgressBar(true, this);
            actionCallApiLoadPaymentMethodList();
        }

        // else handle other activity results
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.img_back:
                AddPaymentActivity.this.onBackPressed();
                break;
        }
    }
    @Override
    public void onAddCreditCard() {
        Analytics.with(AddPaymentActivity.this).track("Click Add_payment_btn");

        Intent intent = new Intent(this, CardInfoActivity.class);
        intent.putExtra(CardInfoActivity.KEY_AUTHORIZATION, paymentDetail != null ? paymentDetail.token : "");
        startActivityForResult(intent, ADD_CREDIT_CARD_CODE);
    }
    @Override
    public void onAddPaypal() {
        if (mBraintreeFragment != null) {
            PayPalOverrides.setFuturePaymentsOverride(true);
            PayPal.authorizeAccount(mBraintreeFragment);
        }
    }
    @Override
    public void onConfigurationFetched(Configuration configuration) {
        this.configuration = configuration;
        if(this.configuration !=null){
            if(this.configuration.isCvvChallengePresent()){
                isCVVRequired = true;
            }
            if(this.configuration.isPostalCodeChallengePresent()){
                isPostalCodeRequired = true;
            }
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        //handleAuthorizationState();
    }
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mAuthorization != null) {
            outState.putString(KEY_AUTHORIZATION, mAuthorization);
        }
    }
    @Override
    public void onPaymentMethodNonceCreated(PaymentMethodNonce paymentMethodNonce) {

        PaymentMethodNonce mNonce = paymentMethodNonce;

        String details = "";
        if (mNonce instanceof PayPalAccountNonce) {
            PayPalAccountNonce paypalAccountNonce = (PayPalAccountNonce) mNonce;

            details = "First name: " + paypalAccountNonce.getFirstName() + "\n";
            details += "Last name: " + paypalAccountNonce.getLastName() + "\n";
            details += "Email: " + paypalAccountNonce.getEmail() + "\n";
            details += "Phone: " + paypalAccountNonce.getPhone() + "\n";
            details += "Payer id: " + paypalAccountNonce.getPayerId() + "\n";
            details += "Client metadata id: " + paypalAccountNonce.getClientMetadataId() + "\n";
            details += "Billing address: " + formatAddress(paypalAccountNonce.getBillingAddress()) + "\n";
            details += "Shipping address: " + formatAddress(paypalAccountNonce.getShippingAddress());
        }
        showOrHideProgressBar(true, this);
        actionCallApiLoadPaymentMethodList();
    }
    @Override
    public void onCancel(int requestCode) {

    }
    @Override
    public void onError(Exception error) {
        if(!isFinishing()){
            new AlertDialog.Builder(this)
                    .setMessage(getString(R.string.text_an_error_occurred) + " (" + error.getClass() + "): " + error.getMessage())
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .show();
        }
    }

    private void iniRequestPermission() {
        if (BuildConfig.DEBUG && ContextCompat.checkSelfPermission(this, WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{WRITE_EXTERNAL_STORAGE}, 1);
        } else {
            //handleAuthorizationState();
        }
    }

    private void iniIntentInfo() {
        paymentURL = getIntent().getIntExtra(ConstantsUtils.INTENT_EXTRA_SELECT_PAYMENT_DRIVER_OR_COMPANY_OR_COSTCENTER, ConstantsUtils.PAYMENT_SITUATION_DRIVER);
        getIntent().getBooleanExtra(ConstantsUtils.INTENT_EXTRA_OPEN_PAYMENT_ACTIVITY_IN_MAKE_BOOKING_WORKFLOW,false);
        openFromRegister = getIntent().getBooleanExtra(ConstantsUtils.INTENT_EXTRA_OPEN_PAYMENT_ACTIVITY_IN_REGISTERING,false);
        openFromMakeBooking = getIntent().getBooleanExtra(ConstantsUtils.INTENT_EXTRA_OPEN_PAYMENT_ACTIVITY_IN_MAKE_BOOKING_WORKFLOW,false);
    }

    public void initUI() {
        btVerify = (TextView) findViewById(R.id.bt_verify);
        listView = (ListView) findViewById(R.id.lv_payment_method);
    }

    public void initData() {
        paymentMethods = new ArrayList<>();
        headerView = View.inflate(this, R.layout.item_payment_header, null);
        footerView = View.inflate(this, R.layout.item_payment_footer, null);


        if(openFromMakeBooking){
            btVerify.setText(getString(R.string.btn_okie));
            btVerify.setVisibility(View.VISIBLE);
        }else if(openFromRegister){
            btVerify.setText(getString(R.string.btn_next));
            btVerify.setVisibility(View.VISIBLE);
        }else {
            btVerify.setVisibility(View.GONE);
        }

        switch (paymentURL){
            case ConstantsUtils.PAYMENT_SITUATION_DRIVER:
                listView.addHeaderView(headerView);
                listView.addFooterView(footerView);
                break;
            case ConstantsUtils.PAYMENT_SITUATION_COMPANY:
            case ConstantsUtils.PAYMENT_SITUATION_COST_CENTER:
                break;
        }
        switch (paymentURL){
            case ConstantsUtils.PAYMENT_SITUATION_DRIVER:
                ((TextView)findViewById(R.id.txt_title)).setText(getResources().getString(R.string.str_select_or_add_payment));
                break;
            case ConstantsUtils.PAYMENT_SITUATION_COMPANY:
                ((TextView)findViewById(R.id.txt_title)).setText(getResources().getString(R.string.str_select_a_company_payment));
                break;
            case ConstantsUtils.PAYMENT_SITUATION_COST_CENTER:
                ((TextView)findViewById(R.id.txt_title)).setText(getResources().getString(R.string.str_select_a_cost_center_payment));
                break;
        }

        adapter = new PaymentAdapter(this, paymentMethods);
        listView.setAdapter(adapter);
        showOrHideProgressBar(true, this);
        actionCallApiLoadPaymentMethodList();

    }

    public void initListener() {
        btVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                actionClickBtnOk();
            }
        });
        findViewById(R.id.img_back).setOnClickListener(this);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                adapter.setSelectedPaymentMethodId(adapter.getData().get(position-1).token);
                //Call api update default payment method
                actionUpdateDefaultPaymentMethod(paymentMethods.get(position-1));
                KZAccount.getSharedAccount().paymentDefault = paymentMethods.get(position-1);
                selectedPaymentMethodId = paymentMethods.get(position-1).token;
                adapter.notifyDataSetChanged();
            }
        });
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, final int i, long l) {
                switch (paymentURL){
                    case ConstantsUtils.PAYMENT_SITUATION_DRIVER:
                        DialogUtils.showCustomContextMenu(getThis(), "Delete payment method", null, null,
                                new CustomListener() {
                                    @Override
                                    public void onClick() {
                                        DialogUtils.showMessageDialog(getThis(),
                                                true,
                                                "",
                                                getString(R.string.confirm_delete_payment),
                                                getString(R.string.btn_yes),
                                                getString(R.string.btn_no),
                                                new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialogInterface, int s) {
                                                        dialogInterface.cancel();
                                                        String id = adapter.getData().get(i-1).token;
                                                        VolleyUtils.getSharedNetwork().deletePaymentMethod(AddPaymentActivity.this, id, new OnResponseModel<Object>() {
                                                            @Override
                                                            public void onResponseSuccess(Object model) {
                                                                actionCallApiLoadPaymentMethodList();
                                                            }

                                                            @Override
                                                            public void onResponseError(BKNetworkResponseError error) {
                                                                VolleyUtils.handleErrorRespond(getThis(), error);
                                                            }
                                                        });
                                                    }
                                                },
                                                new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialogInterface, int i) {
                                                        dialogInterface.cancel();
                                                    }
                                                });
                                    }
                                },null,null);
                        break;

                }
                return true;
            }
        });
        headerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent scanIntent = new Intent(AddPaymentActivity.this, CardIOActivity.class);

                // customize these values to suit your needs.
                scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_EXPIRY, true); // default: false
                scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_CVV, isCVVRequired); // default: false
                scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_POSTAL_CODE, isPostalCodeRequired); // default: false
                scanIntent.putExtra(CardIOActivity.EXTRA_CAPTURED_CARD_IMAGE, true);
                // MY_SCAN_REQUEST_CODE is arbitrary and is only used within this activity.
                startActivityForResult(scanIntent, MY_SCAN_REQUEST_CODE);
            }
        });

        footerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AddPaymentMethodDialog dialog = new AddPaymentMethodDialog(AddPaymentActivity.this);
                dialog.setOnAddPaymentMethodListener(AddPaymentActivity.this);
                dialog.show();
            }
        });
    }

    public void onScanClick(View view) {

    }

    public void onPayPalClick(View view) {

    }

    public void onAddPaymentClick(View view) {

    }

    public void actionClickBtnOk() {
        Analytics.with(AddPaymentActivity.this).track("Click Next_btn");

        if(openFromMakeBooking) {
            if(selectedPaymentMethodId!=null){
                Intent resultIntent = new Intent();
                resultIntent.putExtra(ConstantsUtils.INTENT_EXTRA_PAYMENT_METHOD_ID, selectedPaymentMethodId);
                setResult(Activity.RESULT_OK, resultIntent);
                finish();
            }else if(adapter.selectedPaymentMethodId!=null) {
                Intent resultIntent = new Intent();
                resultIntent.putExtra(ConstantsUtils.INTENT_EXTRA_PAYMENT_METHOD_ID, adapter.selectedPaymentMethodId);
                setResult(Activity.RESULT_OK, resultIntent);
                finish();
            }else {
                DialogUtils.showMessageDialog(this, true, "", "Please select one payment method", "Ok", "Later",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.cancel();
                            }
                        },
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                finish();
                            }
                        });
            }

        }else if(openFromRegister) {

            if(selectedPaymentMethodId!=null){
                Intent resultIntent = new Intent();
                resultIntent.putExtra(ConstantsUtils.INTENT_EXTRA_PAYMENT_METHOD_ID, selectedPaymentMethodId);
                setResult(Activity.RESULT_OK, resultIntent);
                finish();
            }else if(adapter.selectedPaymentMethodId!=null) {
                Intent resultIntent = new Intent();
                resultIntent.putExtra(ConstantsUtils.INTENT_EXTRA_PAYMENT_METHOD_ID, adapter.selectedPaymentMethodId);
                setResult(Activity.RESULT_OK, resultIntent);
                finish();
            }else {
                DialogUtils.showMessageDialog(this, true, "", "Please select one payment method", "Ok", "Later",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.cancel();
                            }
                        },
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Intent resultIntent = new Intent();
                                resultIntent.putExtra(ConstantsUtils.INTENT_EXTRA_PAYMENT_METHOD_ID, adapter.selectedPaymentMethodId);
                                setResult(Activity.RESULT_OK, resultIntent);
                                finish();
                            }
                        });
            }

        }
    }

    private void actionCallApiLoadPaymentMethodList() {
        showOrHideProgressBar(true, this);
        switch (paymentURL){
            case ConstantsUtils.PAYMENT_SITUATION_DRIVER:
                VolleyUtils.getSharedNetwork().loadPaymentMethodDriver(
                        AddPaymentActivity.this,
                        new OnResponseModel<KZPaymentDetail>() {
                    @Override
                    public void onResponseSuccess(KZPaymentDetail model) {
                        if (model != null) {
                            paymentMethods.clear();
                            paymentMethods.addAll(model.getPaymentList());
                            if(model.defaultMethod!=null) {
                                adapter.setSelectedPaymentMethodId(model.defaultMethod);
                            }
                            paymentDetail = model;
                            adapter.notifyDataSetChanged();
                            mAuthorization = model.token;

                        }
                        handleAuthorizationState();
                        showOrHideProgressBar(false, AddPaymentActivity.this);
                    }

                    @Override
                    public void onResponseError(BKNetworkResponseError error) {
                        VolleyUtils.handleErrorRespond(AddPaymentActivity.this, error);
                        showOrHideProgressBar(false, AddPaymentActivity.this);
                    }
                });
                break;
            case ConstantsUtils.PAYMENT_SITUATION_COST_CENTER:
                VolleyUtils.getSharedNetwork().loadPaymentMethodCostCenter(AddPaymentActivity.this,
                        new OnResponseModel<KZPaymentDetail>() {
                    @Override
                    public void onResponseSuccess(KZPaymentDetail model) {
                        if (model != null) {
                            paymentMethods.clear();
                            paymentMethods.addAll(model.getPaymentList());
                            if(model.defaultMethod!=null) {
                                adapter.setSelectedPaymentMethodId(model.defaultMethod);
                            }
                            paymentDetail = model;
                            adapter.notifyDataSetChanged();
                            mAuthorization = model.token;

                        }
                        handleAuthorizationState();
                        showOrHideProgressBar(false, AddPaymentActivity.this);
                    }

                    @Override
                    public void onResponseError(BKNetworkResponseError error) {
                        VolleyUtils.handleErrorRespond(getThis(), error);
                        showOrHideProgressBar(false, AddPaymentActivity.this);
                    }
                });
                break;
            case ConstantsUtils.PAYMENT_SITUATION_COMPANY:
                VolleyUtils.getSharedNetwork().loadPaymentMethodCompany(AddPaymentActivity.this, new OnResponseModel<KZPaymentDetail>() {
                    @Override
                    public void onResponseSuccess(KZPaymentDetail model) {
                        if (model != null) {
                            paymentMethods.clear();
                            paymentMethods.addAll(model.getPaymentList());
                            if(model.defaultMethod!=null) {
                                adapter.setSelectedPaymentMethodId(model.defaultMethod);
                            }
                            paymentDetail = model;
                            adapter.notifyDataSetChanged();
                            mAuthorization = model.token;

                        }
                        handleAuthorizationState();
                        showOrHideProgressBar(false, AddPaymentActivity.this);
                    }

                    @Override
                    public void onResponseError(BKNetworkResponseError error) {
                        VolleyUtils.handleErrorRespond(getThis(), error);
                        showOrHideProgressBar(false, AddPaymentActivity.this);
                    }
                });
                break;
        }
    }

    private void actionUpdateDefaultPaymentMethod(KZPaymentMethod kzPaymentMethod) {
        VolleyUtils.getSharedNetwork().updateDefaultPaymentMethod(AddPaymentActivity.this, kzPaymentMethod.token, new OnResponseModel() {
            @Override
            public void onResponseSuccess(Object model) {
                Toast.makeText(AddPaymentActivity.this, "Default payment changed",  Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                Toast.makeText(AddPaymentActivity.this, "There is error in changed default payment",  Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void handleAuthorizationState() {
        if (mAuthorization == null || mAuthorization.isEmpty()) {
            performReset();
        } else {
            onAuthorizationFetched();
        }
    }

    private String formatAddress(PostalAddress address) {
        return address.getRecipientName() + " " + address.getStreetAddress() + " " +
                address.getExtendedAddress() + " " + address.getLocality() + " " + address.getRegion() +
                " " + address.getPostalCode() + " " + address.getCountryCodeAlpha2();
    }

    private void performReset() {
        mAuthorization = null;

        if (mBraintreeFragment != null) {
            getFragmentManager().beginTransaction().remove(mBraintreeFragment).commit();
            mBraintreeFragment = null;
        }

        reset();
        fetchAuthorization();
    }

    protected void reset() {

    }

    protected void onAuthorizationFetched() {
        try {
            mBraintreeFragment = BraintreeFragment.newInstance(this, mAuthorization);
        } catch (InvalidArgumentException e) {
            onError(e);
        }
    }

    protected void fetchAuthorization() {
        if (mAuthorization != null) {
            onAuthorizationFetched();
        } else {
            showOrHideProgressBar(true, this);
            actionCallApiLoadPaymentMethodList();
        }
    }

    private static class PaymentHolder {

        CustomFontTextView tvPaymentMethod;

        ImageView imgPaymentMethod;

        KZPaymentMethod kzPaymentMethod;


        public void initUI(View rowView) {
            tvPaymentMethod = (CustomFontTextView) rowView.findViewById(R.id.tv_payment_method);
            imgPaymentMethod = (ImageView) rowView.findViewById(R.id.img_payment_method);
            rowView.setTag(this);
        }

        void bindData(KZPaymentMethod paymentMethod) {
            this.kzPaymentMethod = paymentMethod;


            if (this.kzPaymentMethod != null) {
                if (this.kzPaymentMethod instanceof KZCreditCard) {

                    KZCreditCard creditCard = (KZCreditCard) this.kzPaymentMethod;
                    ImageLoader.getInstance().displayImage(creditCard.imageUrl, imgPaymentMethod);
                    tvPaymentMethod.setText(creditCard.cardType + " ending in " + creditCard.last4);

                } else if (this.kzPaymentMethod instanceof KZPaypal) {
                    KZPaypal paypal = (KZPaypal) this.kzPaymentMethod;
                    ImageLoader.getInstance().displayImage(paypal.imageUrl, imgPaymentMethod);
                    tvPaymentMethod.setText("Paypal with  " + paypal.email);
                }
            }

        }
    }

    private class PaymentAdapter extends BaseAdapter {
        private final Context context;
        private final ArrayList<KZPaymentMethod> paymentMethods;
        private String selectedPaymentMethodId;

        PaymentAdapter(Context context, ArrayList<KZPaymentMethod> mPaymentList) {
            //super(context,R.layout.item_benefit_list);
            this.context = context;
            this.paymentMethods = mPaymentList;
        }

        @Override
        public int getCount() {
            return paymentMethods != null ? paymentMethods.size() : 0;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            PaymentHolder holder = null;
            if (convertView != null) {
                holder = (PaymentHolder) convertView.getTag();
            } else {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.item_payment_method, null);
            }

            if (holder == null) {
                holder = new PaymentHolder();
                holder.initUI(convertView);
            }
            RadioButton radioButton = (RadioButton) convertView.findViewById(R.id.radioBtn);
            if(selectedPaymentMethodId!=null && paymentMethods.get(position).token.equals(selectedPaymentMethodId)){
                radioButton.setChecked(true);
            }else {
                radioButton.setChecked(false);
            }

            if(paymentMethods.get(position).state!=null){
                convertView.findViewById(R.id.viewStrip).setBackgroundColor(context.getResources().getColor(paymentMethods.get(position).state.equals("valid") ? R.color.geoTab_brandColorF : R.color.red));
            }
            holder.bindData(paymentMethods.get(position));
            return convertView;
        }

        void setSelectedPaymentMethodId(String selectedPaymentMethodId) {
            this.selectedPaymentMethodId = selectedPaymentMethodId;
        }

        public ArrayList<KZPaymentMethod> getData() {
            return AddPaymentActivity.this.paymentMethods;
        }
    }

    public static void showOrHideProgressBar(boolean b, Activity activity) {
        allowBackPress = !LoadingViewUtils.showOrHideProgressBar(b, activity);
    }
}
