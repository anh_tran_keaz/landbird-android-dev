package com.keaz.landbird.activities;

/**
 * Created by mac on 2/20/17.
 */
public class CountryCodeModels {
    private final String mCode;
    private final String mNum;

    public CountryCodeModels(String s, String num) {
        mCode = s;
        mNum = num;
    }

    public String getValue() {
        return mCode;
    }

    public String getNumber() {
        return mNum;
    }
}
