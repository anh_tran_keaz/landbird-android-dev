package com.keaz.landbird.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.keaz.landbird.R;
import com.keaz.landbird.models.KZAccount;
import com.keaz.landbird.models.KZUser;
import com.keaz.landbird.models.UserModel;
import com.keaz.landbird.utils.BKNetworkResponseError;
import com.keaz.landbird.utils.OnResponseModel;
import com.keaz.landbird.utils.ParseJSonUtil;
import com.keaz.landbird.utils.VolleyUtils;
import com.keaz.landbird.view.CustomEditText;
import com.keaz.landbird.view.CustomFontButton;
import com.segment.analytics.Analytics;

import org.json.JSONException;
import org.json.JSONObject;

public class RegisterCardActivity extends BaseActivity implements View.OnClickListener {


    private CustomEditText etCardNumber;
    private CustomEditText etConfirmCardNumber;
    private CustomFontButton mBtnRemove;

    private String mCurrentRfid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_card);
        etCardNumber = (CustomEditText) findViewById(R.id.et_card_number);
        etConfirmCardNumber = (CustomEditText) findViewById(R.id.et_card_confirm);

        etCardNumber.setText("");
        etCardNumber.setHint("Enter your new GO Pass #");
        etConfirmCardNumber.setText("");
        etConfirmCardNumber.setHint("Confirm your new GO Pass #");


        findViewById(R.id.btnSubmitRegisterKeyCard).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                actionClickSubmitRegisterKeyCard();
            }
        });
        findViewById(R.id.img_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onHeaderBackPress(view);
            }
        });

        mBtnRemove = (CustomFontButton) findViewById(R.id.btn_remove);
        mBtnRemove.setVisibility(View.GONE);
        mBtnRemove.setOnClickListener(this);

        loadRegisterKeyCardFromServer();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Analytics.with(this).screen("Link your GO Pass");
    }

    private void loadRegisterKeyCardFromServer() {
        showOrHideProgressBar(true);
        VolleyUtils.getSharedNetwork().loadUserInfo(
                RegisterCardActivity.this,
                KZAccount.getSharedAccount().getUser().id+"",
                new OnResponseModel() {
            @Override
            public void onResponseSuccess(Object model) {
                showOrHideProgressBar(false);
                JsonObject jsonObject = (JsonObject) model;
                KZUser user = ParseJSonUtil.parseByModel(jsonObject.toString(), KZUser.class);

                etCardNumber.setText(user.rfid);

                if (user.rfid != null && !user.rfid.equals("")) {
                    mBtnRemove.setVisibility(View.VISIBLE);
                }

                mCurrentRfid = user.rfid;
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                showOrHideProgressBar(false);
                VolleyUtils.handleErrorRespond(RegisterCardActivity.this, error);
            }
        });
    }

    private void uploadToServerWithKeyCard(final String keyCard) {
        JSONObject json = new JSONObject();
        try {
            json.put("rfid", keyCard);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        showOrHideProgressBar(true);
        VolleyUtils.getSharedNetwork().updateUserInfo(
                RegisterCardActivity.this,
                KZAccount.getSharedAccount().getUser().id+"",
                json,
                new OnResponseModel() {

            @Override
            public void onResponseSuccess(Object model) {
                Analytics.with(RegisterCardActivity.this).track("Updated Key Coin");

                showOrHideProgressBar(false);
                JsonObject jsonObject = (JsonObject) model;
                UserModel user = ParseJSonUtil.parseByModel(jsonObject.toString(), UserModel.class);
                saveUser(user);
                finish();
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                showOrHideProgressBar(false);
                VolleyUtils.handleErrorRespond(RegisterCardActivity.this, error);
            }
        });
    }

    private void actionClickSubmitRegisterKeyCard() {
        Analytics.with(this).track("Click submit_btn");

        String keyCard = etCardNumber.getText().toString();
        String confirmKeyCard = etConfirmCardNumber.getText().toString();

        if(!keyCard.equals(confirmKeyCard)){
            Toast.makeText(this, "Your register GO pass info are not match.", Toast.LENGTH_SHORT).show();
            return;
        }

        uploadToServerWithKeyCard(keyCard);
    }

    public void onHeaderBackPress(View view) {
        onBackPressed();
    }

    private void showOrHideProgressBar(boolean b) {
        if(b){
            showCustomDialogLoading();
        }else {
            dismissCustomDialogLoading();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_remove:
                Analytics.with(RegisterCardActivity.this).track("Click remove_btn");

                String keyCard = etCardNumber.getText().toString();

                if(keyCard.isEmpty()){
                    Toast.makeText(this, "Please enter your register GO pass.", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(!keyCard.equals(mCurrentRfid)){
                    Toast.makeText(this, "You need input correct previous GO pass.", Toast.LENGTH_SHORT).show();
                    return;
                }

                uploadToServerWithKeyCard("");

                break;
        }
    }
}
