package com.keaz.landbird.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.keaz.landbird.R;
import com.keaz.landbird.utils.BKNetworkResponseError;
import com.keaz.landbird.utils.OnResponseModel;
import com.keaz.landbird.utils.ValidateUtils;
import com.keaz.landbird.utils.VolleyUtils;
import com.keaz.landbird.view.CustomEditText;

import org.json.JSONException;
import org.json.JSONObject;

public class ReferActivity extends BaseActivity {

    private CustomEditText tvName, tvEmail;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_refer);

        tvName = (CustomEditText) findViewById(R.id.et_name);
        tvEmail = (CustomEditText) findViewById(R.id.et_email);

    }

    public void onHeaderBackPress(View view) {
        onBackPressed();

    }

    public void onSubmitClick(View view) {

        if(tvName.getText().toString().isEmpty()){
            Toast.makeText(this,getString(R.string.error_empty_name), Toast.LENGTH_SHORT).show();
            return;
        }
        if(!ValidateUtils.validateEmail(this, tvEmail.getText().toString())){
            Toast.makeText(this,getString(R.string.error_format_email), Toast.LENGTH_SHORT).show();
            return;
        }

        JSONObject js = new JSONObject();
        try {
            js.put("email", tvEmail.getText().toString());
            js.put("name", tvName.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        showCustomDialogLoading();
        VolleyUtils.getSharedNetwork().referAFriend(ReferActivity.this, js, new OnResponseModel() {
            @Override
            public void onResponseSuccess(Object model) {
                dismissCustomDialogLoading();
                Toast.makeText(ReferActivity.this,getString(R.string.text_success), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                dismissCustomDialogLoading();
                VolleyUtils.getSharedNetwork().handleVolleyError(ReferActivity.this, error);
            }
        });
    }

}
