package com.keaz.landbird.activities;

import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;

import com.keaz.landbird.R;
import com.keaz.landbird.utils.ConstantsUtils;
import com.keaz.landbird.view.CustomFontButton;
import com.keaz.landbird.view.CustomFontTextView;

public class TermsOfServiceActivity extends BaseActivity implements View.OnClickListener {

    private WebView mWebview;
    private ImageView mImvClose;
    private CustomFontButton mBtnOk;
    private CustomFontTextView mTxtTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_of_service);

        String title = getIntent().getStringExtra(ConstantsUtils.INTENT_EXTRA_TITLE);

        mTxtTitle = (CustomFontTextView) findViewById(R.id.txt_title);
        mTxtTitle.setText(title);

        mWebview = (WebView) findViewById(R.id.webview);
        if (title.equals(getString(R.string.title_term_of_services))) {
            mWebview.loadUrl("file:///android_asset/Terms_of_Service.html");
        } else if (title.equals(getString(R.string.title_vehicle_report))) {
            mWebview.loadUrl("file:///android_asset/Vehicle_Report.html");
        } else if (title.equals(getString(R.string.title_privacy_policy))) {
            mWebview.loadUrl("file:///android_asset/PrivacyPolicy.html");
        }

        mImvClose = (ImageView) findViewById(R.id.imv_close);
        mImvClose.setOnClickListener(this);

        mBtnOk = (CustomFontButton) findViewById(R.id.btn_ok);
        mBtnOk.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imv_close:
                setResult(RESULT_CANCELED);
                finish();
                break;

            case R.id.btn_ok:
                setResult(RESULT_OK);
                finish();
                break;
        }
    }
}
