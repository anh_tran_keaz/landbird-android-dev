package com.keaz.landbird.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.keaz.landbird.R;

/**
 * Created by anhtran1810 on 12/21/17.
 */

public class BenefitDetailActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_benefit_detail);
    }

    public void onHeaderBackPress(View view) {
        onBackPressed();
    }
}
