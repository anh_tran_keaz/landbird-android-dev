package com.keaz.landbird.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.keaz.landbird.R;
import com.keaz.landbird.utils.ConstantsUtils;
import com.keaz.landbird.utils.ValidateUtils;

public class ActivityEnterEmailToResetPassword  extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_info_to_reset_password);

        findViewById(R.id.btnNext).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = ((EditText)findViewById(R.id.edtEmail)).getText().toString().trim();
                if (email.equals("")) {
                    Toast.makeText(ActivityEnterEmailToResetPassword.this, getString(R.string.error_empty_email), Toast.LENGTH_SHORT).show();
                } else {
                    if (!ValidateUtils.validateEmail(ActivityEnterEmailToResetPassword.this, email)) {
                        Toast.makeText(ActivityEnterEmailToResetPassword.this, getString(R.string.error_format_email), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    actionMoveToVerifyCodeScreenToResetPassword();
                }
            }
        });
    }

    private void actionMoveToVerifyCodeScreenToResetPassword() {
        Intent intent = new Intent(this, VerifyPhoneActivity.class);
        intent.putExtra(ConstantsUtils.INTENT_EXTRA_ACTIVATE_ACCOUNT_OR_RESET_PASSWORD,ConstantsUtils.VERIFY_PHONE_CASE_RESET_PASSWORD);
        intent.putExtra(ConstantsUtils.INTENT_EXTRA_VERIFY_EMAIL_OR_PHONE, ConstantsUtils.VERIFY_EMAIL);
        intent.putExtra(ConstantsUtils.INTENT_EXTRA_EMAIL, ((EditText)findViewById(R.id.edtEmail)).getText().toString());
        startActivity(intent);
    }
}
