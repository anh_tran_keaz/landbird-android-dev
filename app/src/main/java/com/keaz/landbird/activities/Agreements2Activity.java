package com.keaz.landbird.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.keaz.landbird.R;
import com.keaz.landbird.utils.DialogUtils;
import com.keaz.landbird.view.CustomFontTextView;

/**
 * Created by anhtran1810 on 3/6/18.
 */

public class Agreements2Activity extends BaseActivity implements CompoundButton.OnCheckedChangeListener {

    static final String INTENT_EXTRA_ASK_TO_CHECK = "intent extra ask to check";
    static String a = "https://www.ohmie.co/goterms";
    static String b = "https://www.ohmie.co/goprivacy";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agreements2);

        initContent1();
        initContent2();
        initContent3();

        if(getIntent().getBooleanExtra(INTENT_EXTRA_ASK_TO_CHECK,false)){
            findViewById(R.id.cbx_1).setVisibility(View.VISIBLE);
            findViewById(R.id.cbx_2).setVisibility(View.VISIBLE);
            findViewById(R.id.cbx_3).setVisibility(View.VISIBLE);
        }else {
            findViewById(R.id.cbx_1).setVisibility(View.GONE);
            findViewById(R.id.cbx_2).setVisibility(View.GONE);
            findViewById(R.id.cbx_3).setVisibility(View.GONE);
        }

        ((CheckBox)findViewById(R.id.cbx_1)).setOnCheckedChangeListener(this);
        ((CheckBox)findViewById(R.id.cbx_2)).setOnCheckedChangeListener(this);
        ((CheckBox)findViewById(R.id.cbx_3)).setOnCheckedChangeListener(this);

        findViewById(R.id.img_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                actionClose();
            }
        });
    }
    @Override
    public void onBackPressed() {
        actionClose();
    }

    private void actionClose() {
        if(!getIntent().getBooleanExtra(INTENT_EXTRA_ASK_TO_CHECK,false)){
            finish();
            return;
        }
        DialogUtils.showMessageDialog(this,
                true,
                "",
                getString(R.string.confirm_to_exit),
                getString(R.string.btn_no),
                getString(R.string.btn_yes),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                },
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent intent = new Intent(Agreements2Activity.this, LoginActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();
                    }
                });
    }

    private void initContent1() {
        CustomFontTextView mTxtContent1 = (CustomFontTextView) findViewById(R.id.txt_content_1);
        mTxtContent1.setText(Html.fromHtml("I hereby confirm that I have reviewed and understand the Landbird's Terms of Service and that I accept and agree to the <font color='blue'><a href='"+a+"'>Term of Service</a></font> contained within those documents."));
        mTxtContent1.setMovementMethod(LinkMovementMethod.getInstance());
    }

    private void initContent2() {
        CustomFontTextView mTxtContent2 = (CustomFontTextView) findViewById(R.id.txt_content_2);
        mTxtContent2.setText(Html.fromHtml("I hereby confirm that I have reviewed and understand the Landbird's Privacy Policy and that I accept and agree to the <font color='blue'><a href='"+b+"'>Privacy Policy</a></font> contained within those documents."));
        mTxtContent2.setMovementMethod(LinkMovementMethod.getInstance());
    }

    private void initContent3() {
        CustomFontTextView mTxtContent3 = (CustomFontTextView) findViewById(R.id.txt_content_3);
        mTxtContent3.setText(Html.fromHtml("I hereby confirm that I have reviewed and understand the Landbird's Background Check and that I accept and agree to the <font color='blue'><a href='https://www.Landbird.com/privacy-policy'>Background Check</a></font> contained within those documents."));
        mTxtContent3.setMovementMethod(LinkMovementMethod.getInstance());
        //https://www.ohmiego.com/tos-background-and-other-consumer-r
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
        if (isChecked) {
            if (((CheckBox)findViewById(R.id.cbx_1)).isChecked()
                    && ((CheckBox)findViewById(R.id.cbx_2)).isChecked()
                    //&& ((CheckBox)findViewById(R.id.cbx_3)).isChecked()
            ) {
                setResult(RESULT_OK);
                finish();
            }
        }
    }
}
