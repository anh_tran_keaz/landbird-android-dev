package com.keaz.landbird.activities;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.BuildConfig;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.geotab.carshare.CarShareClient;
import com.geotab.carshare.CarShareClientDelegate;
import com.geotab.carshare.Command;
import com.geotab.carshare.Error;
import com.github.javiersantos.appupdater.AppUpdaterUtils;
import com.github.javiersantos.appupdater.enums.AppUpdaterError;
import com.github.javiersantos.appupdater.objects.Update;
import com.google.gson.JsonObject;
import com.keaz.landbird.R;
import com.keaz.landbird.dialogs.ConfirmLogoutDialog;
import com.keaz.landbird.enums.BluetoothCarShareStatus;
import com.keaz.landbird.fragments.BookingListFragment;
import com.keaz.landbird.fragments.ProfileFragment;
import com.keaz.landbird.fragments.ReserveFragment;
import com.keaz.landbird.fragments.RideFragment;
import com.keaz.landbird.fragments.SupportFragment;
import com.keaz.landbird.interfaces.CustomActionListener;
import com.keaz.landbird.interfaces.CustomTabSelected;
import com.keaz.landbird.models.KZAccount;
import com.keaz.landbird.models.KZBooking;
import com.keaz.landbird.models.KZBookingsList;
import com.keaz.landbird.models.UserModel;
import com.keaz.landbird.utils.AppUpdaterUtil;
import com.keaz.landbird.utils.BKGlobals;
import com.keaz.landbird.utils.BKNetworkResponseError;
import com.keaz.landbird.utils.CommonUtils;
import com.keaz.landbird.utils.ConstantsUtils;
import com.keaz.landbird.utils.DialogUtils;
import com.keaz.landbird.utils.JsonUtils;
import com.keaz.landbird.utils.LoadingViewUtils;
import com.keaz.landbird.utils.OnResponseModel;
import com.keaz.landbird.utils.ToastUtil;
import com.keaz.landbird.utils.VolleyUtils;
import com.keaz.landbird.view.CustomBottomTab;
import com.keaz.landbird.view.NonSwipeableViewPager;
import com.segment.analytics.Analytics;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

@RequiresApi(api = Build.VERSION_CODES.M)
public class MainActivity extends AppCompatActivity implements CarShareClientDelegate {
    private static final int REQUEST_ENABLE_BLUETOOTH = 1000;
    private static final int REQUEST_LOCATION = 123;
    private static final String TAG = "anhtran";
    private String[] bottomTitles = {"DRIVE", "TRIPS","BOOK", "PROFILE", "HELP"};
    private KZBooking inUseBooking;
    private RideFragment rideFragment;
    private NonSwipeableViewPager viewPager;
    private int tabSelected = 3;
    private final BroadcastReceiver receiverBluetoothStateChange = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(action) && null != intent) {
                int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);
                int preState = intent.getIntExtra(BluetoothAdapter.EXTRA_PREVIOUS_STATE, BluetoothAdapter.ERROR);
                if(state != preState){
                    if(state == BluetoothAdapter.STATE_ON){
                        if(rideFragment!=null) rideFragment.updateAllData(BluetoothCarShareStatus.BLUETOOTH_ENABLE, null);
                    } else if(state == BluetoothAdapter.STATE_OFF){
                        if(rideFragment!=null) rideFragment.updateAllData(BluetoothCarShareStatus.BLUETOOTH_DISABLE, null);
                    }
                }
            }
        }
    };
    public BluetoothAdapter bluetoothAdapter;
    public CarShareClient carShareClient;
    Command command = Command.LOCK;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");

        if (getActionBar() != null) getActionBar().hide();
        if (getSupportActionBar() != null)getSupportActionBar().hide();

        setContentView(R.layout.activity_main);

        AppUpdaterUtil.start(this, new AppUpdaterUtils.UpdateListener() {
            @Override
            public void onSuccess(Update update, Boolean isUpdateAvailable) {
                Log.d("Latest Version", update.getLatestVersion());
                Log.d("Latest Version Code", ""+update.getLatestVersionCode());
                Log.d("Release notes", update.getReleaseNotes());
                Log.d("URL", ""+update.getUrlToDownload());
                Log.d("Is update available?", Boolean.toString(isUpdateAvailable));

                if(update.getLatestVersionCode()!=null && BuildConfig.VERSION_CODE < update.getLatestVersionCode()){
                    DialogUtils.showMessageDialog(MainActivity.this,true, "Update available", "There is new version on Google Play:\nVersion name: "+update.getLatestVersion()+" \nVersion code: "+update.getLatestVersionCode()+" \nRelease note: "+update.getReleaseNotes()+"\nDo you want to update?", "Update", "Later",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    final String appPackageName = getPackageName();
                                    try {
                                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                                    } catch (android.content.ActivityNotFoundException anfe) {
                                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                                    }
                                }
                            },
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.cancel();
                                }
                            });
                }

            }

            @Override
            public void onFailed(AppUpdaterError appUpdaterError) {
                Log.d("AppUpdater Error", "Something went wrong");
            }
        });

        setUpViewPager();

        setUpTabIcon();

        setUpListener();

        actionSwitchTab(3, false);

        /*bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        carShareClient = new CarShareClient (this, new AndroidLogger());
        carShareClient.setDelegate(this);*/

        // Register for broadcasts when a bluetooth state change.
        IntentFilter filter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
        registerReceiver(receiverBluetoothStateChange, filter);

    }
    @Override
    protected void onStart() {
        super.onStart();
    }
    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
        loadBookings(new CustomActionListener() {
            @Override
            public void takeAction() {
                loadInUseBooking(
                        new CustomActionListener() {
                            @Override
                            public void takeAction() {
                                if (inUseBooking != null){
                                    Intent intent = new Intent(MainActivity.this, GeoDeviceActivity.class);
                                    intent.putExtra("token", inUseBooking.geotab_carshare_token);
                                    startActivity(intent);
                                    /*actionSwitchTab(1, true);*/
                                }
                            }
                        }, new CustomActionListener() {
                            @Override
                            public void takeAction() {
                                actionSwitchTab(tabSelected, true);
                            }
                        });
            }
        }, new CustomActionListener() {
            @Override
            public void takeAction() {
                actionSwitchTab(tabSelected, true);
            }
        });

    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiverBluetoothStateChange);
        if(null != carShareClient){
            carShareClient.disconnect();
        }
    }
    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, @Nullable final Intent data) {
        if(requestCode == REQUEST_ENABLE_BLUETOOTH){
            if(resultCode == RESULT_OK){
                if(rideFragment!=null) rideFragment.updateAllData(BluetoothCarShareStatus.BLUETOOTH_ENABLE, null);
            } else if(resultCode == RESULT_CANCELED){
                if(rideFragment!=null) rideFragment.updateAllData(BluetoothCarShareStatus.BLUETOOTH_DISABLE, null);
            }
        } else {
            super.onActivityResult(requestCode,
                    resultCode,
                    data);
        }
    }
    @Override
    public void onRequestPermissionsResult(final int requestCode, @NonNull final String[] permissions, @NonNull final int[] grantResults) {
        if(requestCode == REQUEST_LOCATION) {

            if(grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if(null != carShareClient){
                    //showProcessView();
                    if(rideFragment!=null) {
                        carShareClient.connect(rideFragment.getCarshareToken());
                        ToastUtil.showToastMessage(this,"Connecting..token: "+rideFragment.getCarshareToken().length(),true);
                    }else {
                        ToastUtil.showToastMessage(this, "Ride Fragment is null", false);
                    }
                }else {
                    ToastUtil.showToastMessage(this,"onRequestPermissionsResult carShareClient is null",true);
                }
            }else {
                ToastUtil.showToastMessage(this,"Permission for accessing device's features are denied!",true);
            }

        } else {
            super.onRequestPermissionsResult(requestCode,
                    permissions,
                    grantResults);
        }
    }
    @RequiresApi(api = Build.VERSION_CODES.M)
    private void setUpTabIcon() {
        String[] labels = new String[]{
                "Drive","Trips","Book","Profile","Help"
        };
        int[][] icons = new int[][]{
                new int[]{R.drawable.new_ic_tab_ride_a,R.drawable.new_ic_tab_ride},
                new int[]{R.drawable.new_ic_tab_trip_a,R.drawable.new_ic_tab_trip},
                new int[]{R.drawable.new_ic_tab_book_a,R.drawable.new_ic_tab_book},
                new int[]{R.drawable.new_ic_tab_profile_a,R.drawable.new_ic_tab_profile},
                new int[]{R.drawable.new_ic_tab_support_a,R.drawable.new_ic_tab_support},
        };
        ((CustomBottomTab)findViewById(R.id.bottomTab)).initData(this, labels, icons, tabSelected, new CustomTabSelected(){
            @Override
            public void select(int i) {
                actionSwitchTab(i, false);
            }
        });

    }

    private void updateHeadTitleTab() {
        UserModel user = KZAccount.getSharedAccount().getUser();
        Analytics.with(MainActivity.this).reset();
        CommonUtils.segmentIdentifyWithUser(MainActivity.this, user);
        switch (tabSelected) {
            case 3:
                Analytics.with(MainActivity.this).screen("Branch List");
                ((TextView) findViewById(R.id.txt_title)).setText("My Station");
                break;
            case 2:
                Analytics.with(MainActivity.this).screen("Trips");
                ((TextView) findViewById(R.id.txt_title)).setText("My Trips");
                break;
            case 1:
                if (inUseBooking != null) {
                    CommonUtils.segmentScreenWithBooking(MainActivity.this, "Ride", inUseBooking);
                    ((TextView) findViewById(R.id.txt_title)).setText("" + inUseBooking.id);
                }
                break;
            case 4:
                Analytics.with(MainActivity.this).screen("Profile");
                ((TextView) findViewById(R.id.txt_title)).setText("Profile");
                break;
            case 5:
                Analytics.with(MainActivity.this).screen("Support");
                ((TextView) findViewById(R.id.txt_title)).setText("Help");
                break;
        }
    }

    private void setUpViewPager() {
        rideFragment = new RideFragment();
        viewPager = (NonSwipeableViewPager) findViewById(R.id.pager);
        HomePagerAdapter adapter = new HomePagerAdapter(getSupportFragmentManager(), this);
        adapter.addFrag(rideFragment, bottomTitles[0]);
        adapter.addFrag(BookingListFragment.newInstance(true), bottomTitles[1]);
        adapter.addFrag(new ReserveFragment(), bottomTitles[2]);
        adapter.addFrag(new ProfileFragment(), bottomTitles[3]);
        adapter.addFrag(new SupportFragment(), bottomTitles[4]);

        viewPager.setAdapter(adapter);
    }

    private void setUpListener() {
        findViewById(R.id.btnListReservation).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Analytics.with(MainActivity.this).track("Click Reservation List icon");
                Intent intent = new Intent(MainActivity.this, BookingListActivity.class);
                startActivity(intent);
            }
        });
        findViewById(R.id.btnLogOut).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                actionLogout();
            }
        });
        findViewById(R.id.btnHistory).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, BookingListActivity.class);
                startActivity(intent);
            }
        });
    }

    private void loadBookings(final CustomActionListener customActionListener, final CustomActionListener customActionListener2 ) {
        int userId = KZAccount.getSharedAccount().user.id;

        findViewById(R.id.progressBarTop).setVisibility(View.VISIBLE);
        /*VolleyUtils.getSharedNetwork().loadBookingByUser(this, userId, new OnResponseModel<KZBookingsList>() {
                    @Override
                    public void onResponseSuccess(KZBookingsList model) {
                        findViewById(R.id.progressBarTop).setVisibility(View.INVISIBLE);
                        int count = 0;
                        for (int i = 0; i < model.bookings.size(); i++) {
                            KZBooking booking = model.bookings.get(i);

                            if (!booking.state.equalsIgnoreCase(BKGlobals.BookingStatus.PENDING.toString()) &&
                                    !booking.state.equalsIgnoreCase(BKGlobals.BookingStatus.COMPLETE.toString()) &&
                                    !booking.state.equalsIgnoreCase(BKGlobals.BookingStatus.CANCELLED.toString()) &&
                                    !booking.state.equalsIgnoreCase(BKGlobals.BookingStatus.EXPIRED.toString())) {
                                count++;
                            }
                        }
                        if(count!=0) {
                            ((CustomBottomTab)findViewById(R.id.bottomTab)).notifyTab(2,count);
                            if(customActionListener!=null) customActionListener.takeAction();
                            ((CustomBottomTab)findViewById(R.id.bottomTab)).enable(1);
                        }else {
                            if(customActionListener2!=null) customActionListener2.takeAction();
                            ((CustomBottomTab)findViewById(R.id.bottomTab)).disable(1);
                        }

                    }

                    @Override
                    public void onResponseError(BKNetworkResponseError error) {
                        findViewById(R.id.progressBarTop).setVisibility(View.INVISIBLE);
                        VolleyUtils.handleErrorRespond(MainActivity.this, error);
                        if(customActionListener!=null) customActionListener.takeAction();
                    }
                });*/

        VolleyUtils.getSharedNetwork().loadBookingsByUser(this, userId, 0, true, new OnResponseModel() {
            @Override
            public void onResponseSuccess(Object model) {
                findViewById(R.id.progressBarTop).setVisibility(View.INVISIBLE);

                JsonObject jsonObject = (JsonObject) model;
                KZBookingsList kzBookingsList = JsonUtils.parseByModel(jsonObject.toString(), KZBookingsList.class);

                ArrayList<KZBooking> newBookingList = new ArrayList<>(kzBookingsList.bookings);

                int count = 0;
                for (KZBooking b: newBookingList) {
                    if(b.state.equals(BKGlobals.BookingStatus.INUSE.toString())) count++;
                }

                if (count !=0) {

                    ((CustomBottomTab)findViewById(R.id.bottomTab)).notifyTab(2,count);
                    if(customActionListener!=null) customActionListener.takeAction();
                    ((CustomBottomTab)findViewById(R.id.bottomTab)).enable(1);

                }else {
                    if(customActionListener2!=null) customActionListener2.takeAction();
                    rideFragment.setBooking(null);
                    ((CustomBottomTab)findViewById(R.id.bottomTab)).disable(1);
                }

            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                findViewById(R.id.progressBarTop).setVisibility(View.INVISIBLE);
                VolleyUtils.handleErrorRespond(MainActivity.this, error);
                if(customActionListener!=null) customActionListener.takeAction();
            }
        });
    }

    private void loadInUseBooking(final CustomActionListener customActionListener, final CustomActionListener customActionListener2) {
        int userId = KZAccount.getSharedAccount().user.id;

        VolleyUtils.getSharedNetwork().loadInUseBookingByUser(this, userId, new OnResponseModel<KZBookingsList>() {
                    @Override
                    public void onResponseSuccess(KZBookingsList model) {

                        if (model.bookings != null && model.bookings.size() > 0) {
                            inUseBooking = model.bookings.get(0);
                            rideFragment.setBooking(inUseBooking);
                            //((CustomBottomTab)findViewById(R.id.bottomTab)).enable(1);
                        } else {
                            inUseBooking = null;
                            rideFragment.setBooking(null);
                            ((CustomBottomTab)findViewById(R.id.bottomTab)).disable(1);
                        }

                        if(customActionListener!=null) customActionListener.takeAction();
                    }

                    @Override
                    public void onResponseError(BKNetworkResponseError error) {
                        inUseBooking = null;
                        ((CustomBottomTab)findViewById(R.id.bottomTab)).disable(1);
                        rideFragment.setBooking(null);

                        VolleyUtils.handleErrorRespond(MainActivity.this, error);

                        if(customActionListener2!=null) customActionListener2.takeAction();
                    }



                });
    }

    public void actionSwitchTab(int i, boolean b) {
        if(b)((CustomBottomTab)findViewById(R.id.bottomTab)).selectTab(i);
        tabSelected = i;
        updateHeadTitleTab();
        viewPager.setCurrentItem(tabSelected -1);
        findViewById(R.id.btnLogOut).setVisibility(tabSelected ==4?View.VISIBLE:View.GONE);
        findViewById(R.id.btnHistory).setVisibility(tabSelected ==2?View.VISIBLE:View.GONE);
        findViewById(R.id.activity_main).setBackgroundColor(getResources().getColor(tabSelected==3?R.color.landBird_brandingDarkColor1:R.color.landBird_backgroundColor));
        ((TextView)findViewById(R.id.txt_title)).setTextColor(getResources().getColor(tabSelected==3?R.color.White:R.color.landBird_textDarkColorX1));
    }

    public void actionSetRideFragmentBooking(KZBooking booking) {
        rideFragment.setBooking(booking);
    }
    @Override
    public void onClientConnect(@NotNull CarShareClient carShareClient) {
        ToastUtil.showToastMessage(this, "onClientConnect()", false);
        if(rideFragment!=null) {
            rideFragment.updateAllData(BluetoothCarShareStatus.CAR_SHARE_CONNECTED, null);
        }
    }
    @Override
    public void onClientDisconnectedUnexpectedly(@NotNull final CarShareClient carShareClient, @NotNull final Error error) {
        ToastUtil.showToastMessage(this, "onClientDisconnectedUnexpectedly()", false);
        if(rideFragment!=null) {
            rideFragment.updateAllData(BluetoothCarShareStatus.CAR_SHARE_DISCONNECT, null, error);
        }
    }
    @Override
    public void onCommandSucceed(@NotNull final Command commandResult) {
//        if(Command.CHECK_IN == commandResult){
//            if(null != command){
//                carShareClient.execute(command, CARSHARE_TOKEN);
//            }
//        } else {
            ToastUtil.showToastMessage(this, "onCommandSucceed()", false);
            if(rideFragment!=null) {
                rideFragment.updateAllData(BluetoothCarShareStatus.CAR_SHARE_COMMAND_SUCCESS, commandResult);
            }
//        }
    }
    @Override
    public void onCommandFailed(@NotNull final Command command, @NotNull final Error error) {
        ToastUtil.showToastMessage(this, "onCommandFailed()", false);
        if(rideFragment!=null) {
            rideFragment.updateAllData(BluetoothCarShareStatus.CAR_SHARE_COMMAND_FAILED, command, error);
        }
    }

    private boolean checkToNeedRequestPermission(){
        if ( Build.VERSION.SDK_INT >= 23){
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(this, Manifest.permission.BLUETOOTH) != PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(this, Manifest.permission.BLUETOOTH_ADMIN) != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                                Manifest.permission.ACCESS_COARSE_LOCATION,
                                Manifest.permission.BLUETOOTH,
                                Manifest.permission.BLUETOOTH_ADMIN}, REQUEST_LOCATION);
                return true;

            }
        }
        return false;
    }

    public void actionConnect() {
        if(!checkToNeedRequestPermission()){
            if(null != carShareClient){
                //showProcessView();
                if(rideFragment!=null){
                    ToastUtil.showToastMessage(this,"Connecting..token: "+rideFragment.getCarshareToken().length(),true);
                    carShareClient.connect(rideFragment.getCarshareToken());
                }else {
                    ToastUtil.showToastMessage(this, "Ride Fragment is null", false);
                }
             }
        }else {
            ToastUtil.showToastMessage(this,"Permission for accessing device's features are denied!",true);
        }
    }

    public void actionLock() {
        if(null != carShareClient){
            ToastUtil.showToastMessage(this,"Execute Lock command..",true);
            carShareClient.execute(Command.LOCK, rideFragment.getCarshareToken());
            command = Command.LOCK;
        }
    }

    public void actionUnlock() {
        if(null != carShareClient){
            ToastUtil.showToastMessage(this,"Execute Unlock command..",true);
            carShareClient.execute(Command.UNLOCK_ALL, rideFragment.getCarshareToken());
            command = Command.UNLOCK_ALL;
        }
    }

    public void actionRequestBluetooth() {
        Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        startActivityForResult(enableBtIntent, REQUEST_ENABLE_BLUETOOTH);
    }

    public void actionCheckIn() {
        carShareClient.execute(Command.CHECK_IN, rideFragment.getCarshareToken());
        command = Command.CHECK_IN;
    }

    private static class HomePagerAdapter extends FragmentStatePagerAdapter {

        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        HomePagerAdapter(FragmentManager fm, Context context) {
            super(fm);
        }

        void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    private void actionLogout() {
        Analytics.with(MainActivity.this).track("Click logout_btn");

        final ConfirmLogoutDialog dialog = new ConfirmLogoutDialog(MainActivity.this);
        dialog.setOnConfirmLogoutListener(new ConfirmLogoutDialog.OnConfirmLogoutListener() {
            @Override
            public void onOk() {
                LoadingViewUtils.showOrHideProgressBar(true, MainActivity.this);
                VolleyUtils.getSharedNetwork().logout(MainActivity.this, new OnResponseModel<Object>() {
                    @Override
                    public void onResponseSuccess(Object model) {
                        LoadingViewUtils.showOrHideProgressBar(false, MainActivity.this);

                        Analytics.with(MainActivity.this).track("Logout Account");
                        Analytics.with(MainActivity.this).reset();

                        BKGlobals.getSharedGlobals().saveStringPreferences(BKGlobals.ACCESS_TOKEN, null);
                        BKGlobals.getSharedGlobals().saveStringPreferences(ConstantsUtils.PREF_KEY_EMAIL, null);
                        BKGlobals.getSharedGlobals().saveStringPreferences(ConstantsUtils.PREF_KEY_CONTACT_NUMBER, null);
                        BKGlobals.getSharedGlobals().saveStringPreferences(ConstantsUtils.PREF_KEY_COUNTRY_CODE, null);
                        BKGlobals.getSharedGlobals().saveStringPreferences(ConstantsUtils.PREF_KEY_PASSWORD, null);
                        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        MainActivity.this.finish();
                    }

                    @Override
                    public void onResponseError(BKNetworkResponseError error) {
                        LoadingViewUtils.showOrHideProgressBar(false, MainActivity.this);
                        VolleyUtils.handleErrorRespond(MainActivity.this, error);
                    }
                });
            }

            @Override
            public void onCancel() {

            }
        });
        dialog.show();
    }

}
