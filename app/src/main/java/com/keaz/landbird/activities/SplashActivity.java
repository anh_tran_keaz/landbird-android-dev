package com.keaz.landbird.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.keaz.landbird.R;
import com.keaz.landbird.utils.BKGlobals;
import com.keaz.landbird.utils.ConstantsUtils;
import com.keaz.landbird.utils.IntentUtil;
import com.keaz.landbird.utils.NetworkUtil;

/**
 * Created by anhtran1810 on 4/19/17.
 */

public class SplashActivity extends BaseActivity {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        if (!NetworkUtil.checkInternetStateAndShowDialog(this, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }}))
        {
            return;
        }

        if (!BKGlobals.getSharedGlobals().getBoolPreferences(ConstantsUtils.PREF_KEY_KEEP_SIGN_IN, true)) {

            BKGlobals.getSharedGlobals().saveStringPreferences(ConstantsUtils.PREF_KEY_EMAIL, null);
            BKGlobals.getSharedGlobals().saveStringPreferences(ConstantsUtils.PREF_KEY_CONTACT_NUMBER, null);
            BKGlobals.getSharedGlobals().saveStringPreferences(ConstantsUtils.PREF_KEY_COUNTRY_CODE, null);
            BKGlobals.getSharedGlobals().saveStringPreferences(ConstantsUtils.PREF_KEY_PASSWORD, null);
            BKGlobals.getSharedGlobals().saveStringPreferences(BKGlobals.ACCESS_TOKEN, null);

        }

        //If access token is existed, go straight to
        if (BKGlobals.getSharedGlobals().getStringPreferences(BKGlobals.ACCESS_TOKEN, null) != null ){

            Intent intent = new Intent(this, ActivityWelcome.class);
            intent = IntentUtil.addFlag1(intent);
            startActivity(intent);

        } else {
            Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
            startActivity(intent);
            finish();
        }
    }
}
