package com.keaz.landbird.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.google.gson.JsonObject;
import com.keaz.landbird.R;
import com.keaz.landbird.models.KZAccount;
import com.keaz.landbird.models.KZBranch;
import com.keaz.landbird.utils.BKNetworkResponseError;
import com.keaz.landbird.utils.OnResponseModel;
import com.keaz.landbird.utils.UIUtils;
import com.keaz.landbird.utils.VolleyUtils;
import com.segment.analytics.Analytics;

public class AddLocationActivity extends BaseActivity implements View.OnClickListener {
    private Spinner spinner;
    private EditText editText;
    private String[] items;
    private int[] codes;
    private int selectedId;
    private ArrayAdapter<String> adapter;
    private boolean success = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_location);

        Analytics.with(this).screen("Add A Location");

        spinner = (Spinner) findViewById(R.id.spinner);
        editText = (EditText) findViewById(R.id.edtCode);
        setUpSpinner();
        findViewById(R.id.imvBack).setOnClickListener(this);
        findViewById(R.id.txtSubmit).setOnClickListener(this);
        loadAllBranchOfCompany();
    }

    private void setUpSpinner() {
        if (items == null) {
            items = new String[]{""};
        }
        adapter = new ArrayAdapter<String>(this, R.layout.spinner_item, items);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (codes != null) {
                    selectedId = codes[position];
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imvBack:
                if (success) {
                    setResult(RESULT_OK);
                }
                finish();
                break;
            case R.id.txtSubmit:
                addLocationToUser();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (success) {
            setResult(RESULT_OK);
        }
        super.onBackPressed();
    }

    private void addLocationToUser() {
        JsonObject jsonObject = new JsonObject();
        String code = editText.getText().toString();
        String branch_id = String.valueOf(selectedId);
        jsonObject.addProperty("code", code);
        jsonObject.addProperty("id", branch_id);
        /*final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage("Loading...");
        dialog.show();*/
        showCustomDialogLoading();
        String userId = String.valueOf(KZAccount.getSharedAccount().user.id);
        VolleyUtils.getSharedNetwork().addLocation(this, userId, jsonObject, new OnResponseModel<JsonObject>() {
            @Override
            public void onResponseSuccess(JsonObject model) {
                Analytics.with(AddLocationActivity.this).track("Create a branch");
                UIUtils.toast(AddLocationActivity.this, "Add branch successfully.");
                dismissCustomDialogLoading();
                success = true;
                setResult(RESULT_OK);
                finish();
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                VolleyUtils.handleErrorRespond(AddLocationActivity.this, error);
                dismissCustomDialogLoading();
                success = false;
            }
        });
    }

    private void loadAllBranchOfCompany() {
        VolleyUtils.getSharedNetwork().loadBranches(this, new OnResponseModel<KZBranch[]>() {
            @Override
            public void onResponseSuccess(KZBranch[] model) {
                items = new String[model.length];
                codes = new int[model.length];
                for (int i = 0; i < model.length; i++) {
                    items[i] = model[i].name;
                    codes[i] = model[i].id;
                }
                selectedId = codes[0];
                setUpSpinner();
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                VolleyUtils.handleErrorRespond(AddLocationActivity.this, error);
                dismissCustomDialogLoading();
            }
        });
    }
}
