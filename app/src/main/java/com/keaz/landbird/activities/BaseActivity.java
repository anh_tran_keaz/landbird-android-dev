package com.keaz.landbird.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.keaz.landbird.R;
import com.keaz.landbird.models.KZAccount;
import com.keaz.landbird.models.UserModel;
import com.keaz.landbird.utils.CommonUtils;
import com.keaz.landbird.utils.StringUtils;
import com.segment.analytics.Analytics;

/**
 * Created by Administrator on 14/2/2017.
 */

public class BaseActivity extends AppCompatActivity {

    protected AlertDialog mAlertDialog;
    protected Dialog mDialogLoading;
    private static Handler handler;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getActionBar() != null) {
            getActionBar().hide();
        }
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

    }

    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public void showAlertDialog(String title, String message) {
        Analytics.with(this).track(message);

        dismissAlertDialog();
        AlertDialog.Builder mDialogBuilder = new AlertDialog.Builder(this);
        mDialogBuilder.setTitle(StringUtils.isEmptyString(title) ? "" : title);
        mDialogBuilder.setMessage(message);
        mDialogBuilder.setPositiveButton(R.string.dialog_button_ok, null);
        mAlertDialog = mDialogBuilder.create();
        if (!mAlertDialog.isShowing())
            mAlertDialog.show();
    }

    public void showAlertDialogNoTitle(String message) {
        dismissAlertDialog();
        AlertDialog.Builder mDialogBuilder = new AlertDialog.Builder(this);
        mDialogBuilder.setTitle("");
        mDialogBuilder.setMessage(message);
        mDialogBuilder.setPositiveButton(R.string.dialog_button_ok, null);
        mAlertDialog = mDialogBuilder.create();
        if (!mAlertDialog.isShowing())
            mAlertDialog.show();
    }

    public void showAlertDialog(String title, String message, DialogInterface.OnClickListener okListener) {
        dismissAlertDialog();
        AlertDialog.Builder mDialogBuilder = new AlertDialog.Builder(this);
        mDialogBuilder.setCancelable(false);
        mDialogBuilder.setTitle(StringUtils.isEmptyString(title) ? "" : title);
        mDialogBuilder.setMessage(message);
        mDialogBuilder.setPositiveButton(R.string.dialog_button_ok, okListener);
        mAlertDialog = mDialogBuilder.create();

        if (!mAlertDialog.isShowing())
            mAlertDialog.show();
    }

    public void showAlertDialog(String title, String message, String buttonTitle, DialogInterface.OnClickListener okListener) {
        dismissAlertDialog();
        AlertDialog.Builder mDialogBuilder = new AlertDialog.Builder(this);
        mDialogBuilder.setCancelable(false);
        mDialogBuilder.setTitle(StringUtils.isEmptyString(title) ? "" : title);
        mDialogBuilder.setMessage(message);
        mDialogBuilder.setPositiveButton(buttonTitle, okListener);
        mAlertDialog = mDialogBuilder.create();

        if (!mAlertDialog.isShowing())
            mAlertDialog.show();
    }

    public void showAlertDialog(String title, String message, String button1Title, String button2Title, DialogInterface.OnClickListener button1Listener, DialogInterface.OnClickListener button2Listener) {
        dismissAlertDialog();
        AlertDialog.Builder mDialogBuilder = new AlertDialog.Builder(this);
        mDialogBuilder.setTitle(title);
        mDialogBuilder.setNegativeButton(button1Title, button1Listener);
        mDialogBuilder.setPositiveButton(button2Title, button2Listener);
        mDialogBuilder.setMessage(message);
        mAlertDialog = mDialogBuilder.create();

        if (!mAlertDialog.isShowing())
            mAlertDialog.show();
    }

    public void dismissAlertDialog() {
        if (mAlertDialog != null) {
            mAlertDialog.dismiss();
            mAlertDialog = null;
        }
    }

    public void showCustomDialogLoading() {
        if (mDialogLoading == null) {
            mDialogLoading = new Dialog(this, R.style.CustomDialog);
            mDialogLoading.setContentView(R.layout.dialog_progress_loading);
            mDialogLoading.setCanceledOnTouchOutside(false);
            mDialogLoading.setCancelable(false);
        }

        if (!mDialogLoading.isShowing())
            mDialogLoading.show();
    }

    public void dismissCustomDialogLoading() {
        if (mDialogLoading != null && mDialogLoading.isShowing())
            mDialogLoading.dismiss();
    }

    public class AsteriskPasswordTransformationMethod extends PasswordTransformationMethod {
        @Override
        public CharSequence getTransformation(CharSequence source, View view) {
            return new PasswordCharSequence(source);
        }

        private class PasswordCharSequence implements CharSequence {
            private CharSequence mSource;

            public PasswordCharSequence(CharSequence source) {
                mSource = source; // Store char sequence
            }

            public char charAt(int index) {
                return '*'; // This is the important part
            }

            public int length() {
                return mSource.length(); // Return default
            }

            public CharSequence subSequence(int start, int end) {
                return mSource.subSequence(start, end); // Return default
            }
        }
    }

    public void startTimer(int second, Runnable runnable) {
        getHandler().postDelayed(runnable, second * 1000);
    }

    public static Handler getHandler() {
        if (handler == null) {
            handler = new Handler();
        }
        return handler;
    }

    public void stopTimer(Runnable runnable) {
        if (handler != null) {
            handler.removeCallbacks(runnable);
            handler = null;
        }
    }

    public void onHeaderBackPress(View view) {
        onBackPressed();
    }

    public void saveUser(UserModel user) {
        CommonUtils.segmentIdentifyWithUser(this, user);
        KZAccount.getSharedAccount().setUser(user);
    }
}
