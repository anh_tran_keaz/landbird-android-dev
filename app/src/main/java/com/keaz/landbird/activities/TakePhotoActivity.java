package com.keaz.landbird.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.keaz.landbird.R;
import com.keaz.landbird.utils.BitmapUtils;
import com.keaz.landbird.utils.FileUtils;
import com.keaz.landbird.utils.UriUtils;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by Thinh Lai on 6/10/15.
 * Copyright (c) 2015 Thinh Lai. All rights reserved.
 */
public class TakePhotoActivity extends Activity {

    public static final int CANCEL_BY_PERMISSIONS = 1;

    private static final int CAMERA_REQUEST_CODE = 50 ;
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    public static final int TAG_CHOOSE_PHOTO_FROM_CAMERA = 1;
    public static final int TAG_CHOOSE_PHOTO_FROM_GALLERY = 2;
    public static final String KEY_IS_CROP_PHOTO = "KEY_IS_CROP_PHOTO";
    public static final String EXTRA_PHOTO_URL = "EXTRA_PHOTO_URL";
    public static final String PHOTO_ID = "PHOTO_ID";
    @Bind(R.id.image)
    CropImageView image;
    @Bind(R.id.cropView)
    RelativeLayout cropView;
    @Bind(R.id.dialogView)
    RelativeLayout dialogView;
    @Bind(R.id.btnCancel)
    Button btnCancel;
    @Bind(R.id.btnChoose)
    Button btnChoose;
    private int photoId;
    private File filePhotoCurrent = null;
    private boolean isCrop = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_take_photo);

        String name = dateToString(new Date(), "yyyy-MM-dd-hh-mm-ss");


        ButterKnife.bind(this);

        isCrop = (boolean) getIntent().getExtras().getBoolean(KEY_IS_CROP_PHOTO, false);

        photoId = getIntent().getExtras().getInt(PHOTO_ID, 0);

        dialogView.setVisibility(View.GONE);
        cropView.setVisibility(View.GONE);
        image.setCropShape(CropImageView.CropShape.RECTANGLE);
        image.setFixedAspectRatio(true);

        setFonts();


        //Check permission
        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            //RUNTIME PERMISSION Android M
            int permission = ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

            if (permission != PackageManager.PERMISSION_GRANTED) {
                // We don't have permission so prompt the user
                ActivityCompat.requestPermissions(this,
                        PERMISSIONS_STORAGE,
                        REQUEST_EXTERNAL_STORAGE
                );
            } else {
                dialogView.setVisibility(View.VISIBLE);
            }
//            if (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
//                // Get permission success
//                dialogView.setVisibility(View.VISIBLE);
//            } else {
//                FileUtils.requestPermission(this);
//            }
        }
    }

    private void setFonts() {
//        FontUtils.setFont(this,FontUtils.FONT_TYPE_LATO_LIGHT,btnCancel);
//        FontUtils.setFont(this,FontUtils.FONT_TYPE_LATO_LIGHT,btnChoose);
    }

    @OnClick(R.id.btnLastPhoto)
    void onClickLastPhoto() {
        dialogView.setVisibility(View.GONE);
        getLastPhotoTaken();
    }

    @OnClick(R.id.btnTakeNewPhoto)
    void onClickTakePhoto() {
        dialogView.setVisibility(View.GONE);
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
            //ask for authorisation
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, CAMERA_REQUEST_CODE);
        else
             openIntentTakePhoto();
    }

    @OnClick(R.id.btnOpenLibrary)
    void onClickChooseGallery() {
        dialogView.setVisibility(View.GONE);
        openIntentChooseGallery();
    }

    @OnClick(R.id.btnCancelDialog)
    void onClickDialogCancel() {
        dialogView.setVisibility(View.GONE);
        onBackPressed();
    }

    @OnClick(R.id.btnCancel)
    void onClickCancel() {
        onBackPressed();
    }

    @OnClick(R.id.btnChoose)
    void onClickChoose() {
        Bitmap bitmap = image.getCroppedImage(200, 200);
        saveBitmapToFile(bitmap);
        callBackGetPhotoSuccess();
    }

    private void saveBitmapToFile(Bitmap bitmap) {
        try {
            FileOutputStream fos = new FileOutputStream(filePhotoCurrent.getAbsolutePath());
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.flush();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public String dateToString(Date date, String format) {
        SimpleDateFormat df = new SimpleDateFormat(format);
        return df.format(date);
    }

    private void openIntentTakePhoto() {

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            filePhotoCurrent = null;
            try {
                filePhotoCurrent = FileUtils.createImageFile();
            } catch (IOException ex) {

            }
            // Continue only if the File was successfully created
            Uri photoURI = UriUtils.getUri(this, filePhotoCurrent);

            if (filePhotoCurrent != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, TAG_CHOOSE_PHOTO_FROM_CAMERA);
            }
        }
    }

    private void openIntentChooseGallery() {
        filePhotoCurrent = null;
        Intent gallery = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        gallery.setType("image/*");
        startActivityForResult(Intent.createChooser(gallery,
                "Choose Gallery"), TAG_CHOOSE_PHOTO_FROM_GALLERY);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case TAG_CHOOSE_PHOTO_FROM_CAMERA:
                    handlePhoto(false);
                    break;

                case TAG_CHOOSE_PHOTO_FROM_GALLERY:
                    dispatchFromGallery(data.getData());
                    break;
                default:
                    onBackPressed();
                    break;
            }
        } else {
            onBackPressed();
        }
    }

    private void handleError() {
        Toast.makeText(this, getString(R.string.dialog_message_get_photo_error), Toast.LENGTH_SHORT).show();
        onBackPressed();
    }

    private void dispatchFromGallery(Uri selectedImage) {
        if (selectedImage == null)
            return;
        try {
            // Keep MediaStore.Images.Media.DATA in filePathColumn array
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            // Create cursor to open gallery
            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            // Get column from cursor
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String mCurrentPhotoPath = cursor.getString(columnIndex);
            cursor.close();
            filePhotoCurrent = new File(mCurrentPhotoPath);
            handlePhoto(true);

        } catch (Exception e) {
            handleError();
        }
    }

    private void handlePhoto(boolean isFromGallery) {
        if (filePhotoCurrent != null && filePhotoCurrent.exists()) {
            // Scale bitmap
            Bitmap bitmap = BitmapUtils.resizeImage(filePhotoCurrent.getAbsolutePath());
            if (bitmap == null)
                return;

            addPictureToGallery(filePhotoCurrent.getAbsolutePath());

            if (isFromGallery) {
                filePhotoCurrent = null;
                try {
                    filePhotoCurrent = FileUtils.createImageFile();
                    saveBitmapToFile(bitmap);
                } catch (IOException ex) {

                }
            }

            if (isCrop) {
                cropView.setVisibility(View.VISIBLE);
                image.setImageBitmap(bitmap);
            } else {
                saveBitmapToFile(bitmap);
                callBackGetPhotoSuccess();
            }
        } else {
            handleError();
        }
    }

    private void addPictureToGallery(String path) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(path);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        sendBroadcast(mediaScanIntent);
    }

    public void callBackGetPhotoSuccess() {
        Intent intent = getIntent();
        String absolutePath = filePhotoCurrent.getAbsolutePath();
        intent.putExtra(EXTRA_PHOTO_URL, absolutePath);
        intent.putExtra(PHOTO_ID, photoId);

        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_EXTERNAL_STORAGE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    dialogView.setVisibility(View.VISIBLE);
                } else {
                    Toast.makeText(this,
                            "Get permission failure",
                            Toast.LENGTH_SHORT).show();
                    setResult(CANCEL_BY_PERMISSIONS);
                    onBackPressed();
                }
                return;
            }
            case CAMERA_REQUEST_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    openIntentTakePhoto();
                } else {
                    Toast.makeText(this,"Please grant camera permission" , Toast.LENGTH_SHORT).show();
                    setResult(CANCEL_BY_PERMISSIONS);
                    onBackPressed();
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
        }
    }

    private void getLastPhotoTaken() {
        // Find the last picture
        String[] projection = new String[]{
                MediaStore.Images.ImageColumns._ID,
                MediaStore.Images.ImageColumns.DATA,
                MediaStore.Images.ImageColumns.BUCKET_DISPLAY_NAME,
                MediaStore.Images.ImageColumns.DATE_TAKEN,
                MediaStore.Images.ImageColumns.MIME_TYPE
        };

        final Cursor cursor = getContentResolver()
                .query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, projection, null,
                        null, MediaStore.Images.ImageColumns.DATE_TAKEN + " DESC");

        if (cursor.moveToFirst()) {
            if(cursor != null) {
                String imageLocation = cursor.getString(1);
                filePhotoCurrent = new File(imageLocation);
                handlePhoto(true);
            }
            else
                handleError();
        } else {
            handleError();
        }
    }
}
