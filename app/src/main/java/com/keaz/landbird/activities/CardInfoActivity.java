package com.keaz.landbird.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.braintreepayments.api.BraintreeFragment;
import com.braintreepayments.api.Card;
import com.braintreepayments.api.UnionPay;
import com.braintreepayments.api.exceptions.ErrorWithResponse;
import com.braintreepayments.api.exceptions.InvalidArgumentException;
import com.braintreepayments.api.interfaces.BraintreeCancelListener;
import com.braintreepayments.api.interfaces.BraintreeErrorListener;
import com.braintreepayments.api.interfaces.ConfigurationListener;
import com.braintreepayments.api.interfaces.PaymentMethodNonceCreatedListener;
import com.braintreepayments.api.models.CardBuilder;
import com.braintreepayments.api.models.CardNonce;
import com.braintreepayments.api.models.Configuration;
import com.braintreepayments.api.models.PaymentMethodNonce;
import com.braintreepayments.cardform.OnCardFormFieldFocusedListener;
import com.braintreepayments.cardform.OnCardFormSubmitListener;
import com.braintreepayments.cardform.utils.CardType;
import com.braintreepayments.cardform.view.CardEditText;
import com.braintreepayments.cardform.view.CardForm;
import com.keaz.landbird.R;
import com.keaz.landbird.dialogs.ConfirmBillingDialog;
import com.keaz.landbird.models.KZPaymentDetail;
import com.keaz.landbird.utils.BKNetworkResponseError;
import com.keaz.landbird.utils.OnResponseModel;
import com.keaz.landbird.utils.ToastUtil;
import com.keaz.landbird.utils.VolleyUtils;
import com.segment.analytics.Analytics;

import io.card.payment.CardIOActivity;
import io.card.payment.CreditCard;

import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;


public class CardInfoActivity extends BaseActivity implements
        ConfirmBillingDialog.OnConfirmReservationListener,
        ActivityCompat.OnRequestPermissionsResultCallback,
        PaymentMethodNonceCreatedListener,
        BraintreeCancelListener,
        BraintreeErrorListener,
        OnCardFormSubmitListener,
        OnCardFormFieldFocusedListener,
        ConfigurationListener {

    public static final String KEY_AUTHORIZATION = "com.braintreepayments.demo.KEY_AUTHORIZATION";
    private static final String TAG = CardIOActivity.class.getSimpleName();
    private static final int MY_PERMISSIONS_REQUEST_WRITE_STORAGE = 1;
    protected String mAuthorization;
    protected BraintreeFragment mBraintreeFragment;
    ImageView imgCard;
    Intent cardData;
    private CardForm mCardForm;
    private Configuration mConfiguration;
    private CardType mCardType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_info);

        imgCard = (ImageView) findViewById(R.id.img_card);
        mCardForm = (CardForm) findViewById(R.id.card_form);
        mCardForm.setOnFormFieldFocusedListener(this);
        mCardForm.setOnCardFormSubmitListener(this);

        mAuthorization = getIntent().getStringExtra(KEY_AUTHORIZATION);

        if (savedInstanceState != null && savedInstanceState.containsKey(KEY_AUTHORIZATION)) {
            mAuthorization = savedInstanceState.getString(KEY_AUTHORIZATION);
        }

        cardData = getIntent().getParcelableExtra("CARD_DATA");

        Bitmap bitmap = CardIOActivity.getCapturedCardImage(cardData);

        if (bitmap != null) {
            imgCard.setImageBitmap(bitmap);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        Analytics.with(this).screen("Card Info");
    }

    @Override
    public void onOk() {

//        setResult(RESULT_OK);
//        finish();

        startActivity(new Intent(this, FinishSignUpActivity.class));
    }
    @Override
    public void onCancel() {

    }
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mAuthorization != null) {
            outState.putString(KEY_AUTHORIZATION, mAuthorization);
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        actionRequestPermission();
    }
    @Override
    public void onPaymentMethodNonceCreated(PaymentMethodNonce paymentMethodNonce) {
        Log.d(TAG, "Payment Method Nonce received: " + paymentMethodNonce.getTypeLabel());

//        Intent intent = new Intent()
//                .putExtra(MainActivity.EXTRA_PAYMENT_METHOD_NONCE, paymentMethodNonce)
//                .putExtra(MainActivity.EXTRA_DEVICE_DATA, mDeviceData);
        displayResult(paymentMethodNonce, "");
        dismissCustomDialogLoading();
        setResult(RESULT_OK);
        finish();
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_WRITE_STORAGE:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    handleAuthorizationState();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    ToastUtil.showToastMessage(this,getString(R.string.error_empty_permission), false);
                }
                return;
            }
    }
    @Override
    public void onCancel(int requestCode) {
        Log.d(TAG, "Cancel received: " + requestCode);
        setProgressBarIndeterminateVisibility(false);
        dismissCustomDialogLoading();
    }
    @Override
    public void onError(Exception error) {
        String message = "";
        if(error instanceof ErrorWithResponse)
        {
            ErrorWithResponse reponse = (ErrorWithResponse) error;

            try {
                message = reponse.getFieldErrors().get(0).getFieldErrors().get(0).getMessage();
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
                message = reponse.getMessage();
            }
        }
        showDialog(getString(R.string.text_an_error_occurred) + " " + message);
        setProgressBarIndeterminateVisibility(false);
        dismissCustomDialogLoading();
    }
    @Override
    public void onCardFormFieldFocused(View field) {
        if (!(field instanceof CardEditText) && !TextUtils.isEmpty(mCardForm.getCardNumber())) {
            CardType cardType = CardType.forCardNumber(mCardForm.getCardNumber());
            if (mCardType != cardType) {
                mCardType = cardType;

                if (mConfiguration.getUnionPay().isEnabled()) {
                    UnionPay.fetchCapabilities(mBraintreeFragment, mCardForm.getCardNumber());
                }
            }
        }
    }
    @Override
    public void onCardFormSubmit() {
        onPurchase(null);
    }
    @Override
    public void onConfigurationFetched(Configuration configuration) {
        mConfiguration = configuration;

        boolean isCVV = false;

        if(mConfiguration!=null && mConfiguration.isCvvChallengePresent()){
            isCVV = true;
        }

        mCardForm.cardRequired(true)
                .expirationRequired(true)
                .cvvRequired(isCVV)
                .postalCodeRequired(configuration.isPostalCodeChallengePresent())
                .mobileNumberRequired(false)
                .actionLabel("Confirm")
                .setup(this);

        if (cardData != null)
            mCardForm.handleCardIOResponse(cardData);

        if (configuration.isPayPalEnabled()) {
//            mPayPalButton.setVisibility(VISIBLE);
        }
    }

    private void actionRequestPermission() {
        if (ContextCompat.checkSelfPermission(this, WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_STORAGE);
        } else {
            handleAuthorizationState();
        }
    }

    private void handleAuthorizationState() {
        if (mAuthorization == null || mAuthorization.isEmpty()) {
            performReset();
        } else {
            onAuthorizationFetched();
        }
    }

    public void onHeaderBackPress(View view) {
        onBackPressed();
    }

    public void onConfirmClick(View view) {
        Analytics.with(this).track("Click Submit_btn");

        showCustomDialogLoading();
        onCardFormSubmit();
    }

    private void displayResult(PaymentMethodNonce paymentMethodNonce, String deviceData) {
        PaymentMethodNonce mNonce = paymentMethodNonce;

        String details = "";
        if (mNonce instanceof CardNonce) {
            CardNonce cardNonce = (CardNonce) mNonce;

            details = "Card Last Two: " + cardNonce.getLastTwo() + "\n";
            details += "3DS isLiabilityShifted: " + cardNonce.getThreeDSecureInfo().isLiabilityShifted() + "\n";
            details += "3DS isLiabilityShiftPossible: " + cardNonce.getThreeDSecureInfo().isLiabilityShiftPossible();
        }

        Log.d(TAG, "Payment Method Nonce received data : " + details);
    }

    private void performReset() {
        mAuthorization = null;

        if (mBraintreeFragment != null) {
            getFragmentManager().beginTransaction().remove(mBraintreeFragment).commit();
            mBraintreeFragment = null;
        }

        fetchAuthorization();
    }

    protected void reset() {

    }

    protected void onAuthorizationFetched() {
        try {
            mBraintreeFragment = BraintreeFragment.newInstance(this, mAuthorization);
        } catch (InvalidArgumentException e) {
            onError(e);
        }

        setProgressBarIndeterminateVisibility(false);
    }

    protected void fetchAuthorization() {
        if (mAuthorization != null && !mAuthorization.isEmpty()) {
            onAuthorizationFetched();
        } else {
            showCustomDialogLoading();
            getPaymentMethodList();
        }
    }

    private void getPaymentMethodList() {
        VolleyUtils.getSharedNetwork().loadPaymentMethodDriver(this, new OnResponseModel<KZPaymentDetail>() {
            @Override
            public void onResponseSuccess(KZPaymentDetail model) {
                if (model != null && model.token != null) {
                    if (TextUtils.isEmpty(model.token)) {
                        showDialog("Client token was empty");
                    } else {
                        mAuthorization = model.token;
                        onAuthorizationFetched();
                    }
                }
                dismissCustomDialogLoading();
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                VolleyUtils.handleErrorRespond(CardInfoActivity.this, error);
                dismissCustomDialogLoading();
            }
        });

    }

    protected void showDialog(String message) {
        new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }

    public void onPurchase(View v) {
        setProgressBarIndeterminateVisibility(true);

        CardBuilder cardBuilder = new CardBuilder()
                .cardNumber(mCardForm.getCardNumber())
                .expirationMonth(mCardForm.getExpirationMonth())
                .expirationYear(mCardForm.getExpirationYear())
                .cvv(mCardForm.getCvv())
                .postalCode(mCardForm.getPostalCode());

        Card.tokenize(mBraintreeFragment, cardBuilder);

    }

    public CardBuilder getCardBuiderFromIntent(Intent data) {
        if (data != null && data.hasExtra(CardIOActivity.EXTRA_SCAN_RESULT)) {
            CreditCard scanResult = data.getParcelableExtra(CardIOActivity.EXTRA_SCAN_RESULT);
            if (scanResult != null) {

                CardBuilder cardBuilder = new CardBuilder()
                        .cardNumber(scanResult.getRedactedCardNumber())
                        .expirationMonth(scanResult.expiryMonth + "")
                        .expirationYear(scanResult.expiryYear + "")
                        .cvv(scanResult.cvv)
                        .postalCode(scanResult.postalCode);
                return cardBuilder;
            }
        }
        return null;
    }

}
