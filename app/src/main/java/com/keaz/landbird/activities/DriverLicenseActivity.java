package com.keaz.landbird.activities;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.PopupMenu;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.acuant.mobilesdk.AcuantAndroidMobileSDKController;
import com.acuant.mobilesdk.AcuantErrorListener;
import com.acuant.mobilesdk.Card;
import com.acuant.mobilesdk.CardCroppingListener;
import com.acuant.mobilesdk.CardType;
import com.acuant.mobilesdk.DriversLicenseCard;
import com.acuant.mobilesdk.ErrorType;
import com.acuant.mobilesdk.FacialData;
import com.acuant.mobilesdk.FacialRecognitionListener;
import com.acuant.mobilesdk.LicenseDetails;
import com.acuant.mobilesdk.MedicalCard;
import com.acuant.mobilesdk.PassportCard;
import com.acuant.mobilesdk.ProcessImageRequestOptions;
import com.acuant.mobilesdk.Region;
import com.acuant.mobilesdk.WebServiceListener;
import com.acuant.mobilesdk.util.Utils;
import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.JsonObject;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;
import com.keaz.landbird.BuildConfig;
import com.keaz.landbird.R;
import com.keaz.landbird.activities.acuant.ImageConfirmationActivity;
import com.keaz.landbird.helpers.DriverLicenseActivityHelper;
import com.keaz.landbird.interfaces.CustomActionListener;
import com.keaz.landbird.interfaces.CustomListener;
import com.keaz.landbird.interfaces.CustomListener1;
import com.keaz.landbird.interfaces.ListSelectListener;
import com.keaz.landbird.models.KZAccount;
import com.keaz.landbird.models.MainActivityModel;
import com.keaz.landbird.models.StateModel;
import com.keaz.landbird.models.UserAssetsModel;
import com.keaz.landbird.models.UserModel;
import com.keaz.landbird.utils.AcuantUtil;
import com.keaz.landbird.utils.BKNetworkResponseError;
import com.keaz.landbird.utils.ConfirmationListener;
import com.keaz.landbird.utils.ConstantsUtils;
import com.keaz.landbird.utils.DataContext;
import com.keaz.landbird.utils.DialogUtils;
import com.keaz.landbird.utils.FileUtils;
import com.keaz.landbird.utils.FormatTimeUtils;
import com.keaz.landbird.utils.FormatUtil;
import com.keaz.landbird.utils.ImageUtils;
import com.keaz.landbird.utils.LoadingViewUtils;
import com.keaz.landbird.utils.LogUtils;
import com.keaz.landbird.utils.MultipartRequest;
import com.keaz.landbird.utils.OnResponseModel;
import com.keaz.landbird.utils.ParseJSonUtil;
import com.keaz.landbird.utils.PermissionsUtils;
import com.keaz.landbird.utils.RequestPermissionHandler;
import com.keaz.landbird.utils.TempImageStore;
import com.keaz.landbird.utils.ToastUtil;
import com.keaz.landbird.utils.Util;
import com.keaz.landbird.utils.ViewUtil;
import com.keaz.landbird.utils.VolleySingleton;
import com.keaz.landbird.utils.VolleyUtils;
import com.keaz.landbird.view.CustomEditText;
import com.keaz.landbird.view.CustomFontButton;
import com.keaz.landbird.view.CustomFontTextView;
import com.keaz.landbird.view.CustomLicenseItem;
import com.microblink.util.Log;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.segment.analytics.Analytics;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.thin.downloadmanager.DefaultRetryPolicy;
import com.thin.downloadmanager.DownloadRequest;
import com.thin.downloadmanager.DownloadStatusListener;
import com.thin.downloadmanager.ThinDownloadManager;

import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.EnumMap;
import java.util.Map;

import static android.graphics.Color.BLACK;
import static android.graphics.Color.WHITE;

/**
 * Created by anhtran1810 on 12/22/17.
 * updateModelAndUIFromCroppedCard
 * processImageServiceCompleted
 */

@RequiresApi(api = Build.VERSION_CODES.M)
public class DriverLicenseActivity extends BaseActivity  implements
        View.OnClickListener,
        WebServiceListener,
        CardCroppingListener,
        AcuantErrorListener,
        FacialRecognitionListener, CustomLicenseItem.OnUploadLicenseListener {

    private static final String TAG = DriverLicenseActivity.class.getName();
    public static final int REQUEST_CODE_TAKE_PHOTO_ACTIVITY = 201;
    public static final int MAX_DOWNLOAD_FINISHED = 3;
    private static final int REGISTER = 1;
    private static final int CONFIRM_BOOKING = 3;
    private static final int SETTING = 2;
    private static final int FORCE_RE_UPLOAD_WITH_ACUANT = 4;

    private final String IS_SHOWING_DIALOG_KEY = "isShowingDialog";
    private final String IS_PROCESSING_DIALOG_KEY = "isProcessing";
    private final String IS_CROPPING_DIALOG_KEY = "isCropping";
    private final String IS_VALIDATING_DIALOG_KEY = "isValidating";
    private final String IS_ACTIVATING_DIALOG_KEY = "isActivating";
    private final String REGION_USA = "USA";
    private final String REGION_CANADA = "Canada";
    private final String REGION_EUROPE = "Europe";
    private final String REGION_AFRICA = "Africa";
    private final String REGION_ASIA = "Asia";
    private final String REGION_LATIN_AMERICA = "Latin America";
    private final String REGION_AUSTRALIA = "Australia";

    private CustomFontButton mBtnVerify;
    private CustomEditText mTxtLicense;
    private CustomFontTextView mTxtExpireMonth;
    private CustomFontTextView mTxtExpireYear;
    private CustomFontTextView mTxtExpireDay;

    private TextView tvMenuCountry;
    private CustomLicenseItem mLicenseBack;
    private CustomLicenseItem mLicenseFront;
    private CustomLicenseItem mLicenseFace;
    private RelativeLayout mRelProgressBar;
    private ProgressBar mProgressHorizontal;
    private ScrollView mScrollView;
    private ProgressDialog progressDialog;
    private AlertDialog alertDialog;
    private AlertDialog showDuplexAlertDialog;
    public MainActivityModel mainActivityModel = null;
    private ArrayList<String> mMonthList;
    private ArrayList<String> mYearList;
    private ArrayList<String> mDayList;

    private int cardRegion;
    private int mUserId;
    private int mDownloadFinished;
    private int mCount;
    private Runnable mUploadPictureRunnable ;
    private int[] imgRes = {R.drawable.new_ic_license, R.drawable.new_ic_license_back, R.drawable.new_ic_face};
    private int[] titleRes = {R.string.str_license_front, R.string.str_license_back, R.string.str_license_face};
    private int[] descRes = {R.string.str_license_front_desc, R.string.str_license_back_desc, R.string.str_license_face_desc};

    public String sPdf417String = "";
    private boolean isShowErrorAlertDialog;
    private boolean isShowDuplexDialog;
    private boolean isProcessing;
    private boolean isValidating;
    private boolean isActivating;
    private boolean isCropping;
    private boolean isBackSide;
    private boolean isProcessingFacial;
    private  boolean isFacialFlow;
    private boolean isStop;
    boolean isFrontVerified = false;
    boolean isBackVerified = false;
    boolean isFaceVerified = false;
    private int situation;
    private Card processedCardInformation;
    private FacialData processedFacialData;
    private Bitmap avatarFromDriverLicense;
    private Bitmap originalImage;
    private String mCurrentState;
    private File mFilePhotoFront;
    private Handler mHandler;
    private byte[] multipartBody;
    private ThinDownloadManager thinDownloadManager;
    AcuantAndroidMobileSDKController acuantAndroidMobileSdkControllerInstance = null;
    private boolean processAvatarFromDriverLicense;
    private String facialMatchRate = "0";
    private String licenseNumber;
    private IniInfo iniInfo;
    private boolean devMode;
    private DriverLicenseActivity mainActivity;
    private int region = Region.REGION_AUSTRALIA;
    final int PERMISSION_ALL = 1;
    final int PERMISSION_CAMERA = 2;
    private RequestPermissionHandler mRequestPermissionHandler;
    private String imageFolderPath;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_license);

        iniSomeData();

        mRequestPermissionHandler = new RequestPermissionHandler();

        actionRequestPermission();

        if (savedInstanceState == null) {
            mainActivityModel = new MainActivityModel();
        } else {
            mainActivityModel = DataContext.getInstance().getMainActivityModel();
            // if coming from background and kill the app, restart the model
            if (mainActivityModel == null) {
                mainActivityModel = new MainActivityModel();
            }
        }
        DataContext.getInstance().setContext(getApplicationContext());

        iniView();
        initializeSDK();
        initData();
        initListener();
        createFolderHideImage();
        loadUserLicensePictures();
//        actionLoadUserData();

    }
    @Override
    protected void onStart() {
        super.onStart();
        Analytics.with(this).screen("DL Upload");
    }
    @Override
    public void onBackPressed() {
        actionBack();
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == REQUEST_CODE_TAKE_PHOTO_ACTIVITY && resultCode == Activity.RESULT_OK) {
            String urlPhoto = data.getStringExtra(TakePhotoActivity.EXTRA_PHOTO_URL);

            Uri uri = data.getData();
            int id = data.getIntExtra(TakePhotoActivity.PHOTO_ID, 0);

            if (id == mLicenseFront.getId()) {
                mLicenseFront.updateLicenseImage(urlPhoto);
                uploadPicture(this, urlPhoto ,VolleyUtils.getUrlUploadLicenseImageFront(""+mUserId));

            } else if (id == mLicenseBack.getId()) {
                mLicenseBack.updateLicenseImage(urlPhoto);
                uploadPicture(this, urlPhoto ,VolleyUtils.getUrlUploadLicenseImageBack(""+mUserId));

            } else if (id == mLicenseFace.getId()) {
                mLicenseFace.updateLicenseImage(urlPhoto);
                uploadPicture(this, urlPhoto ,VolleyUtils.getUrlUploadLicenseImageFace(""+mUserId));
            }
        }
    }
    @Override
    protected void onPostResume() {
        super.onPostResume();
        LogUtils.log(TAG,"onPostResume");
        showErrorMessage();
    }
    @Override
    public void onCardImageCaptured(){
        LogUtils.log(TAG,"onCardImageCaptured");
        MediaPlayer mp = MediaPlayer.create(getApplicationContext(), R.raw.shutter);
        mp.start();
    }
    @Override
    public void onCardCroppingStart(Activity activity) {
        LogUtils.log(TAG,"onCardCroppingStart");
        System.gc();
        System.runFinalization();

        cardRegion = DataContext.getInstance().getCardRegion();
        if(progressDialog!=null && progressDialog.isShowing()) {
            Util.dismissDialog(progressDialog);
        }
        Util.lockScreen(this);
        progressDialog = Util.showProgessDialog(activity, getString(R.string.waiting_crop_image));
        isCropping = true;
    }
    @Override
    public void onCardCroppingFinish(final Bitmap bitmap,int detectedCardType) {
        LogUtils.log(TAG,"onCardCroppingFinish");
        if(progressDialog!=null && progressDialog.isShowing()) {
            Util.dismissDialog(progressDialog);
            progressDialog = null;
        }
        TempImageStore.setBitmapImage(bitmap);
        TempImageStore.setImageConfirmationListener(new ConfirmationListener() {
            @Override
            public void confirmed() {
                if (bitmap != null) {
                    updateModelAndUIFromCroppedCard(bitmap);
                }else{
                    // set an error to be shown in the onResume method.
                    mainActivityModel.setErrorMessage(getString(R.string.error_unable_to_detect_card));
                    updateModelAndUIFromCroppedCard(originalImage);
                }
                Util.unLockScreen(DriverLicenseActivity.this);

                isCropping = false;
            }

            @Override
            public void retry() {
                showCameraInterface();
            }

            @Override
            public void tryNewWay() {

            }
        });

        Intent imageConfirmationIntent = new Intent(this, ImageConfirmationActivity.class);
        if(bitmap==null){
            TempImageStore.setCroppingPassed(false);
        }else{
            TempImageStore.setCroppingPassed(true);
        }
        TempImageStore.setCardType(mainActivityModel.getCurrentOptionType());
        startActivity(imageConfirmationIntent);

    }
    @Override
    public void onCardCroppingFinish(final Bitmap bitmap, final boolean scanBackSide,int detectedCardType) {
        LogUtils.log(TAG,"onCardCroppingFinish");
        if(progressDialog!=null && progressDialog.isShowing()) {
            Util.dismissDialog(progressDialog);
            progressDialog = null;
        }
        TempImageStore.setBitmapImage(bitmap);
        TempImageStore.setImageConfirmationListener(new ConfirmationListener() {
            @Override
            public void confirmed() {
                presentCameraForBackSide(bitmap,scanBackSide);
            }

            @Override
            public void retry() {
                if ((cardRegion == Region.REGION_UNITED_STATES || cardRegion == Region.REGION_CANADA)
                        &&  mainActivityModel.getCurrentOptionType()== CardType.DRIVERS_LICENSE
                        && isBackSide) {
                    acuantAndroidMobileSdkControllerInstance.setInitialMessageDescriptor(R.layout.tap_to_focus);
                    acuantAndroidMobileSdkControllerInstance.showCameraInterfacePDF417(DriverLicenseActivity.this, CardType.DRIVERS_LICENSE, cardRegion);
                } else {
                    showCameraInterface();
                }
            }

            @Override
            public void tryNewWay() {

            }
        });
        Intent imageConfirmationIntent = new Intent(this, ImageConfirmationActivity.class);
        if(bitmap==null){
            TempImageStore.setCroppingPassed(false);
        }else{
            TempImageStore.setCroppingPassed(true);
        }
        TempImageStore.setCardType(mainActivityModel.getCurrentOptionType());
        startActivity(imageConfirmationIntent);
    }
    @Override
    public void onPDF417Finish(String result) {
        LogUtils.log(TAG,"onPDF417Finish");
        sPdf417String = result;
        MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
        try {
            BitMatrix bitMatrix = multiFormatWriter.encode(sPdf417String, BarcodeFormat.PDF_417,200,200);
            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
            Bitmap bitmap = barcodeEncoder.createBitmap(bitMatrix);
            mLicenseBack.getImageView().setImageBitmap(bitmap);
            mLicenseBack.updateLicenseImage("sampleUrl");
        } catch (WriterException e) {
            e.printStackTrace();
        }
        processAvatarFromDriverLicense = false;
        actionProcessCard(true);
    }
    @Override
    public void onOriginalCapture(Bitmap bitmap) {
        LogUtils.log(TAG,"onOriginalCapture");
        originalImage = bitmap;
    }
    @Override
    public void onCancelCapture(Bitmap croppedImage,Bitmap originalImage) {
        Log.d("Acuant", "onCancelCapture");
        if(croppedImage!=null || originalImage!=null) {
            final Bitmap cImage = croppedImage;
            final Bitmap oImage = originalImage;
            if (cImage != null) {
                //mainActivityModel.setBackSideCardImage(cImage);
                mLicenseBack.getImageView().setImageBitmap(cImage);
            } else if (oImage != null) {
                //mainActivityModel.setBackSideCardImage(oImage);
                mLicenseBack.getImageView().setImageBitmap(oImage);
            }
        }
    }
    @Override
    public void onBarcodeTimeOut(Bitmap croppedImage,Bitmap originalImage) {
        final Bitmap cImage = croppedImage;
        final Bitmap oImage = originalImage;
        acuantAndroidMobileSdkControllerInstance.pauseScanningBarcodeCamera();
        AlertDialog.Builder builder = new AlertDialog.Builder(acuantAndroidMobileSdkControllerInstance.getBarcodeCameraContext());
        // barcode Dialog "ignore" option
        builder.setNegativeButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked OK button
                if(cImage!=null){
                    //mainActivityModel.setBackSideCardImage(cImage);
                    mLicenseBack.getImageView().setImageBitmap(cImage);
                }else if (oImage != null) {
                    mLicenseBack.getImageView().setImageBitmap(oImage);
                    //mainActivityModel.setBackSideCardImage(oImage);
                }
                acuantAndroidMobileSdkControllerInstance.finishScanningBarcodeCamera();
                dialog.dismiss();
            }
        });
        // barcode Dialog "retry" option
        builder.setPositiveButton("Try Again", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                acuantAndroidMobileSdkControllerInstance.resumeScanningBarcodeCamera();
                dialog.dismiss();
            }
        });
        //barcode Dialog title and main message
        builder.setMessage("Unable to scan the barcode?");
        builder.setTitle("AcuantMobileSDK");
        builder.create().show();
    }
    @Override
    public void processImageServiceCompleted(Card card) {
        LogUtils.log(TAG,"processImageServiceCompleted");
        LogUtils.log("driver_license", "processImageServiceCompleted");
        Util.dismissDialog(progressDialog);

        if(mainActivityModel.getCurrentOptionType() != CardType.FACIAL_RECOGNITION) {
            isProcessing = false;
            processedCardInformation = card;
            DriversLicenseCard driversLicenseCard = (DriversLicenseCard) card;
            if(processAvatarFromDriverLicense){

                if (driversLicenseCard.getFaceImage() != null) {
                    avatarFromDriverLicense = driversLicenseCard.getFaceImage();
                    //Fake toast to let me know that we can get the profile image
                    ToastUtil.showToastMessage(this,"Info updated!", false);
//                    mLicenseFace.getImageView().setImageBitmap(driversLicenseCard.getFaceImage());
//                    try {
//                        Analytics.with(DriverLicenseActivity.this).track("DL_Face_btn upload successfully");
//
//                        File fileBack = FileUtils.createImageFile();
//                        actionSaveBitmapToFile(driversLicenseCard.getFaceImage(), fileBack);
//                        mLicenseFace.updateLicenseImage(fileBack.getAbsolutePath());
//                        mLicenseFace.getImageView().setImageBitmap(driversLicenseCard.getFaceImage());
//
//                    } catch (IOException ex) {
//                        Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
//                    }
                }
                processAvatarFromDriverLicense = false;
                return;
            }
            updateLicenseInfo(driversLicenseCard, new CustomActionListener() {
                @Override
                public void takeAction() {
                    isProcessingFacial = true;
                    showFacialDialog();
                }
            });

        }else{
            isProcessingFacial=false;
            FacialData processedFacialData = (FacialData) card;
            if(processedFacialData !=null){
                if(processedFacialData.facialMatchConfidenceRating!=null){
                    facialMatchRate = processedFacialData.facialMatchConfidenceRating ;
                }
                LogUtils.log("facial_match_rating","Facial Match Rate: "+facialMatchRate);
                if(devMode){
                    DialogUtils.showMessageDialog(DriverLicenseActivity.this, true, "", "Facial match confidence rating: " + (facialMatchRate!=null?facialMatchRate : "null -> 0") + "%", "Ok", "",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.cancel();
                                }
                            },null);
                }
            }
        }

        //Method show result data as sample project
    }
    @Override
    public void validateLicenseKeyCompleted(LicenseDetails details) {
        LogUtils.log(TAG,"validateLicenseKeyCompleted");
        Util.dismissDialog(progressDialog);
        Util.unLockScreen(DriverLicenseActivity.this);

        LicenseDetails cssnLicenseDetails = DataContext.getInstance().getCssnLicenseDetails();
        DataContext.getInstance().setCssnLicenseDetails(details);

        // update model
        mainActivityModel.setState(MainActivityModel.State.VALIDATED);
        if (cssnLicenseDetails != null && cssnLicenseDetails.isLicenseKeyActivated()) {
            mainActivityModel.setValidatedStateActivation(MainActivityModel.State.ValidatedStateActivation.ACTIVATED);
        } else {
            mainActivityModel.setValidatedStateActivation(MainActivityModel.State.ValidatedStateActivation.NO_ACTIVATED);
        }
        // message dialogs
        acuantAndroidMobileSdkControllerInstance.enableLocationAuthentication(this);
        isValidating = false;

    }
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        LogUtils.log(TAG,"onRestoreInstanceState");

        isShowErrorAlertDialog = savedInstanceState.getBoolean(IS_SHOWING_DIALOG_KEY, false);
        isProcessing = savedInstanceState.getBoolean(IS_PROCESSING_DIALOG_KEY, false);
        isCropping = savedInstanceState.getBoolean(IS_CROPPING_DIALOG_KEY, false);
        isValidating = savedInstanceState.getBoolean(IS_VALIDATING_DIALOG_KEY, false);
        isActivating = savedInstanceState.getBoolean(IS_ACTIVATING_DIALOG_KEY, false);

        if(progressDialog!=null && progressDialog.isShowing()){
            Util.dismissDialog(progressDialog);
        }

        if (isProcessing) {
            progressDialog = Util.showProgessDialog(DriverLicenseActivity.this, getString(R.string.waiting_capture_image));
        }
        if (isCropping){
            progressDialog = Util.showProgessDialog(DriverLicenseActivity.this, getString(R.string.waiting_crop_image));
        }
        if (isValidating){
            progressDialog = Util.showProgessDialog(DriverLicenseActivity.this, getString(R.string.waiting_validate_license));
        }
        if (isActivating){
            progressDialog = Util.showProgessDialog(DriverLicenseActivity.this, getString(R.string.waiting_active_license));
        }
        if (isShowErrorAlertDialog){
            alertDialog.show();
        }
        showErrorMessage();
    }
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        LogUtils.log(TAG,"onSaveInstanceState");

        DataContext.getInstance().setMainActivityModel(mainActivityModel);
        outState.putBoolean(IS_SHOWING_DIALOG_KEY, isShowErrorAlertDialog);
        outState.putBoolean(IS_PROCESSING_DIALOG_KEY, isProcessing);
        outState.putBoolean(IS_CROPPING_DIALOG_KEY, isCropping);
        outState.putBoolean(IS_ACTIVATING_DIALOG_KEY, isActivating);
        outState.putBoolean(IS_VALIDATING_DIALOG_KEY, isValidating);
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        LogUtils.log(TAG,"onDestroy");
        TempImageStore.cleanup();
        acuantAndroidMobileSdkControllerInstance.cleanup();
    }
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        LogUtils.log(TAG,"onConfigurationChanged");
    }
    @Override
    public void didFailWithError(int code, String message) {
        LogUtils.log(TAG,"didFailWithError");
        Util.dismissDialog(progressDialog);
        Util.unLockScreen(DriverLicenseActivity.this);
        String msg = message;
        if (code == ErrorType.AcuantErrorCouldNotReachServer) {
            msg = getString(R.string.error_no_internet_message);
        }else if (code == ErrorType.AcuantErrorUnableToCrop){
            updateModelAndUIFromCroppedCard(originalImage);
        }
        if(alertDialog!=null && !alertDialog.isShowing()) {
            alertDialog = Util.showDialog(this, msg, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    isShowErrorAlertDialog = false;
                }
            });
            isShowErrorAlertDialog = true;
        }

        if (Util.LOG_ENABLED) {
            Utils.appendLog(TAG, "didFailWithError:" + message);
        }
        // message dialogs
        isValidating = false;
        isProcessing = false;
        isActivating = false;
    }
    @Override
    public void onClick(View v) {
        LogUtils.log(TAG,"onClick");
        switch (v.getId()) {
            case R.id.imv_back:
                actionBack();
                break;
            case R.id.txt_expire_month:
                showPopUpMenuMonth();
                break;
            case R.id.txt_expire_day:
                showPopUpMenuDay();
                break;
                case R.id.txt_expire_year:
                showPopUpMenuYear();
                break;
            case R.id.btn_verify: {
                switch (situation){
                    case REGISTER:
                        if (mBtnVerify.getText().toString().equals(getString(R.string.btn_next))) {
                            actionFinishUploadDriverLicensePicture();
                        } else if (mBtnVerify.getText().toString().equals(getString(R.string.btn_upload_and_verify))) {
                            if (isFrontVerified && isBackVerified && isFaceVerified) {
                                actionCallApiUploadLicenseStateToServer(new CustomListener1() {
                                    @Override
                                    public void takeAction() {
                                        actionUploadPictures(true, true, true);
                                    }
                                });
                            }
                        }
                        break;
                    case CONFIRM_BOOKING:
                    case SETTING:
                        actionCallApiUploadLicenseStateToServer(new CustomListener1() {
                            @Override
                            public void takeAction() {
                                boolean[] c = checkChangedNumber();
                                actionUploadPictures(c[0], c[1], c[2]);
                                ToastUtil.showToastMessage(DriverLicenseActivity.this,"Updated successfully!", false);
                            }
                        });
                        break;
                    case FORCE_RE_UPLOAD_WITH_ACUANT:
                        actionCallApiUploadLicenseStateToServer(new CustomListener1() {
                            @Override
                            public void takeAction() {
                                boolean[] c = checkChangedNumber();
                                actionUploadPictures(c[0], c[1], c[2]);
                                ToastUtil.showToastMessage(DriverLicenseActivity.this,"Updated successfully!", false);
                                KZAccount.getSharedAccount().user.is_acuant = true;
                            }
                        });
                        break;
                }
                break;
            }
        }
    }

    private void iniSomeData() {
        mUploadPictureRunnable = new Runnable() {
            @Override
            public void run() {
                if (isStop) {
                    isStop = false;
                    mDownloadFinished = 0;
                    mCount = 0;
                    mProgressHorizontal.setProgress(0);

                    mHandler.removeCallbacks(mUploadPictureRunnable);
                    mRelProgressBar.setVisibility(View.GONE);
                    return;
                }

                try {
                    if (mDownloadFinished >= MAX_DOWNLOAD_FINISHED) {
                        mCount = 100;
                        mProgressHorizontal.setProgress(mCount);
                        isStop = true;
                    } else {
                        mCount += 10;
                        mProgressHorizontal.setProgress(mCount);
                    }
                } finally {
                    int mInterval = 1000;
                    mHandler.postDelayed(mUploadPictureRunnable, mInterval);
                }
            }
        };
        thinDownloadManager = new ThinDownloadManager();

        if(getIntent().getBooleanExtra(ConstantsUtils.INTENT_EXTRA_VIEW_DRIVER_LICENSE_FROM_SETTING, false)) situation = SETTING;
        if(getIntent().getBooleanExtra(ConstantsUtils.INTENT_EXTRA_OPEN_FROM_CONFIRM_BOOKING, false)) situation = CONFIRM_BOOKING;
        if(getIntent().getBooleanExtra(ConstantsUtils.INTENT_EXTRA_OPEN_FROM_REGISTERING, false)) situation = REGISTER;
        if(getIntent().getBooleanExtra(ConstantsUtils.INTENT_EXTRA_FORCE_USER_RE_UPLOAD_DL_WITH_ACUANT, false)) situation = FORCE_RE_UPLOAD_WITH_ACUANT;


        DataContext.getInstance().setCardRegion(Region.REGION_UNITED_STATES);
        try {
            mUserId = KZAccount.getSharedAccount().getUser().id;
        } catch (Exception e) {
            mUserId = 0;
        }

        isStop = false;
        mDownloadFinished = 0;
        mCount = 0;
    }

    private void iniView() {
        findViewById(R.id.btnScanAgain).setVisibility(View.INVISIBLE);
        findViewById(R.id.tittle).setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                ToastUtil.showToastMessage(DriverLicenseActivity.this,"Developer mode enable",false);
                devMode = true;
                return false;
            }
        });

        final ImageView imvBack = (ImageView) findViewById(R.id.imv_back);
        imvBack.setOnClickListener(this);

        mRelProgressBar = (RelativeLayout) findViewById(R.id.rel_progress_bar);
        mRelProgressBar.setVisibility(View.GONE);

        mScrollView = (ScrollView) findViewById(R.id.scrollview);

        mProgressHorizontal = (ProgressBar)findViewById(R.id.progressBar_horizontal);

        mBtnVerify = (CustomFontButton) findViewById(R.id.btn_verify);
        mBtnVerify.setBackground(getResources().getDrawable(R.drawable.round_gray_big_rad));
        mBtnVerify.setOnClickListener(this);

        mTxtLicense = (CustomEditText) findViewById(R.id.txt_license);
        mTxtLicense.setText("");

        final Calendar nowCalendar = Calendar.getInstance();
        mTxtExpireMonth = (CustomFontTextView) findViewById(R.id.txt_expire_month);
        //mTxtExpireMonth.setOnClickListener(this);
        mTxtExpireMonth.setText(FormatUtil.formatIn2Digit(nowCalendar.get(Calendar.MONTH)));

        mTxtExpireDay = (CustomFontTextView) findViewById(R.id.txt_expire_day);
        //mTxtExpireDay.setOnClickListener(this);
        mTxtExpireDay.setText(FormatUtil.formatIn2Digit(nowCalendar.get(Calendar.DATE)));

        mTxtExpireYear = (CustomFontTextView) findViewById(R.id.txt_expire_year);
        //mTxtExpireYear.setOnClickListener(this);
        mTxtExpireYear.setText(""+nowCalendar.get(Calendar.YEAR));

        findViewById(R.id.view_setDate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(DriverLicenseActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        mTxtExpireDay.setText(FormatUtil.formatIn2Digit(dayOfMonth));
                        mTxtExpireMonth.setText(FormatUtil.formatIn2Digit((month+1)));
                        mTxtExpireYear.setText(""+year);
                    }
                },nowCalendar.get(Calendar.YEAR), nowCalendar.get(Calendar.MONTH), nowCalendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.show();
            }
        });

        tvMenuCountry = (TextView) findViewById(R.id.tv_country_menu);
        //Default
        tvMenuCountry.setText("Victoria");
        mCurrentState = "VIC";
        tvMenuCountry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                actionClickMenuStateCountry(view);
            }
        });

        CustomLicenseItem.OnUploadLicenseListener onUploadLicenseListener = new CustomLicenseItem.OnUploadLicenseListener() {
            @Override
            public void onUploadLicenseListener(int id) {
                actionSideCaptureClick(id);
            }
        };

        mLicenseFront = (CustomLicenseItem) findViewById(R.id.item_license_front);
        mLicenseFront.setOnUploadLicenseListener(onUploadLicenseListener, mLicenseFront.getId());
        mLicenseFront.initData(imgRes[0], titleRes[0], descRes[0]);

        mLicenseBack = (CustomLicenseItem) findViewById(R.id.item_license_back);
        mLicenseBack.setOnUploadLicenseListener(onUploadLicenseListener, mLicenseBack.getId());
        mLicenseBack.initData(imgRes[1], titleRes[1], descRes[1]);

        mLicenseFace = (CustomLicenseItem) findViewById(R.id.item_license_face);
        mLicenseFace.setOnUploadLicenseListener(onUploadLicenseListener, mLicenseFace.getId());
        mLicenseFace.initData(imgRes[2], titleRes[2], descRes[2]);

        ((EditText)findViewById(R.id.edt_zipcode)).setText("");
        ((EditText)findViewById(R.id.edt_firstName)).setText("");
        ((EditText)findViewById(R.id.edt_lastName)).setText("");
        ((TextView)findViewById(R.id.txt_dob)).setText("Select your date of birth");


        findViewById(R.id.btnScanAgain).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                actionSideCaptureClick(mLicenseFront.getId());
            }
        });

        findViewById(R.id.txt_dob).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar nowCal = Calendar.getInstance();
                new DatePickerDialog(DriverLicenseActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int y, int m, int d) {
                        Calendar cal = Calendar.getInstance();
                        cal.set(y, m, d);
                        ((TextView)findViewById(R.id.txt_dob)).setText(FormatTimeUtils.formatDate2(cal));
                    }
                }, nowCal.get(Calendar.YEAR), nowCal.get(Calendar.MONTH), nowCal.get(Calendar.DATE)).show();
            }
        });

        switch (situation){
            case SETTING:
            case CONFIRM_BOOKING:
            case FORCE_RE_UPLOAD_WITH_ACUANT:
                mBtnVerify.setText("UPDATE");
                mBtnVerify.setBackground(getResources().getDrawable(R.drawable.round_brand_color_big_rad));
                break;
            case REGISTER:
                mBtnVerify.setText(getString(R.string.btn_upload_and_verify));
                break;
        }
    }

    private void actionClickMenuStateCountry(View view) {
        ViewUtil.showMenuStates(view, DriverLicenseActivity.this, R.menu.empty_menu, new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                ArrayList<StateModel> states = ViewUtil.getStates(DriverLicenseActivity.this, 4);
                tvMenuCountry.setText(item.getTitle());
                for (StateModel stateModel: states) {
                    if(stateModel.getName().trim().equals(item.getTitle())){
                        mCurrentState = stateModel.getCode();
                    }
                }

                //Analytics.with(DriverLicenseActivity.this).track("Click StateSelection_btn", new Properties().putValue("State", mCurrentState.getName()));
                return false;
            }
        });

//        String fileName = null;
//        switch (region){
//            case Region.REGION_UNITED_STATES:
//                break;
//            case Region.REGION_CANADA:
//                break;
//            case Region.REGION_EUROPE:
//                fileName = "europe.json";
//                break;
//            case Region.REGION_AFRICA:
//                fileName = "africa.json";
//                break;
//            case Region.REGION_ASIA:
//                fileName = "asia.json";
//                break;
//            case Region.REGION_AMERICA:
//                fileName = "latin.json";
//                break;
//            case Region.REGION_AUSTRALIA:
//                break;
//            default:
//                break;
//        }
//        if(fileName!=null){
//            final JSONArray jsonArray = JsonUtils.createJSONArray(this,fileName);
//            String[] arr = JsonUtils.getArr(jsonArray);
//            DialogUtils.showCustomListDialog(this, true, "Select country", arr, 1, new ListSelectListener() {
//                @Override
//                public void action(DialogInterface dialog, String selectionResult, int selection) {
//                    JSONObject jsonObject;
//                    try {
//                        jsonObject = jsonArray.getJSONObject(selection);
//                        String alpha3Code = jsonObject.getString("alpha3Code");
//                        String alpha2Code = jsonObject.getString("alpha2Code");
//                        tvMenuCountry.setText(selectionResult);
//                        mCurrentState = alpha3Code;
//                        Log.d("current_state", mCurrentState +"mCurrentState");
//                        dialog.cancel();
//
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                }
//            });
//        }else {
//            final ArrayList<StateModel> states = ViewUtil.getStates(DriverLicenseActivity.this, region);
//            String[] arr = new String[states.size()];
//            for (int i = 0; i < states.size(); i++) {
//                arr[i] = states.get(i).getName();
//            }
//            DialogUtils.showCustomListDialog(this, true, "Select state", arr, 1, new ListSelectListener() {
//                @Override
//                public void action(DialogInterface dialog, String selectionResult, int selection) {
//                    tvMenuCountry.setText(selectionResult);
//                    for (StateModel stateModel: states) {
//                        if(stateModel.getName().trim().equals(selectionResult)){
//                            mCurrentState = stateModel.getCode();
//                            Log.d("current_state", mCurrentState +"mCurrentState");
//                        }
//                    }
//                    Analytics.with(DriverLicenseActivity.this).track("Click StateSelection_btn", new Properties().putValue("State", mCurrentState));
//                    dialog.cancel();
//                }
//            });
//        }

    }

    private void initData() {
        System.gc();
        System.runFinalization();

        mainActivityModel.setCurrentOptionType(CardType.DRIVERS_LICENSE);

        isProcessing = false;
        isProcessingFacial=false;

        mMonthList = new ArrayList<>();
        mMonthList.add("01");
        mMonthList.add("02");
        mMonthList.add("03");
        mMonthList.add("04");
        mMonthList.add("05");
        mMonthList.add("06");
        mMonthList.add("07");
        mMonthList.add("08");
        mMonthList.add("09");
        mMonthList.add("10");
        mMonthList.add("11");
        mMonthList.add("12");

        mDayList = new ArrayList<>();
        for (int i = 1; i <= 31; i++) {
            mDayList.add(""+i);
        }

        mYearList = new ArrayList<>();

        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        for (int i = currentYear; i < (currentYear + 50); i++) {
            mYearList.add("" + i);
        }
    }

    private void initializeSDK(){
        LogUtils.log(TAG,"initializeSDK");
        String licenseKey;//Set license key here
        if(BuildConfig.DEBUG){
            licenseKey = getString(R.string.acuant_license_key_dev);
        }else {
            licenseKey = getString(R.string.acuant_license_key_production);
        }
        Util.lockScreen(this);

        acuantAndroidMobileSdkControllerInstance = AcuantAndroidMobileSDKController.getInstance(this, licenseKey);


        Util.lockScreen(this);
        if (!Util.isTablet(this)) {
            acuantAndroidMobileSdkControllerInstance.setPdf417BarcodeImageDrawable(getResources().getDrawable(R.drawable.barcode));
        }


        acuantAndroidMobileSdkControllerInstance.setWebServiceListener(this);
        acuantAndroidMobileSdkControllerInstance.setWatermarkText(null, 0, 0, 30, 0);
        acuantAndroidMobileSdkControllerInstance.setFacialRecognitionTimeoutInSeconds(20);
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;
        int width = displaymetrics.widthPixels;
        final Paint textPaint = new Paint(Paint.ANTI_ALIAS_FLAG | Paint.DITHER_FLAG);
        Typeface currentTypeFace =   textPaint.getTypeface();
        Typeface bold = Typeface.create(currentTypeFace, Typeface.BOLD);
        textPaint.setColor(WHITE);
        if (!Util.isTablet(this)) {
            Display display = getWindowManager().getDefaultDisplay();
            if(display.getWidth()<1000 || display.getHeight()<1000){
                textPaint.setTextSize(30);
            }else {
                textPaint.setTextSize(50);
            }

        }else{
            textPaint.setTextSize(30);
        }
        textPaint.setTextAlign(Paint.Align.LEFT);
        textPaint.setTypeface(bold);

        Paint subtextPaint = new Paint(Paint.ANTI_ALIAS_FLAG | Paint.DITHER_FLAG);
        subtextPaint.setColor(Color.RED);
        if (!Util.isTablet(this)) {
            Display display = getWindowManager().getDefaultDisplay();
            if(display.getWidth()<1000 || display.getHeight()<1000){
                textPaint.setTextSize(20);
            }else {
                subtextPaint.setTextSize(40);
            }
        }else{
            subtextPaint.setTextSize(25);
        }
        subtextPaint.setTextAlign(Paint.Align.LEFT);
        subtextPaint.setTypeface(Typeface.create(subtextPaint.getTypeface(), Typeface.BOLD));

        final String instructionStr = "Get closer until Red Rectangle appears and Blink";
        final String subInstString = "Analyzing...";
        Rect bounds = new Rect();
        textPaint.getTextBounds(instructionStr, 0, instructionStr.length(), bounds);
        int top = (int)(height*0.05);
        if (Util.isTablet(this)) {
            top = top - 20;
        }
        int left = (width-bounds.width())/2;

        textPaint.getTextBounds(subInstString, 0, subInstString.length(), bounds);
        textPaint.setTextAlign(Paint.Align.LEFT);
        int subLeft = (width-bounds.width())/2;

        acuantAndroidMobileSdkControllerInstance.setInstructionText(instructionStr, left,top,textPaint);
        if (!Util.isTablet(this)) {
            Display display = getWindowManager().getDefaultDisplay();
            if(display.getWidth()<1000 || display.getHeight()<1000){
                acuantAndroidMobileSdkControllerInstance.setSubInstructionText(subInstString, subLeft,top+15,subtextPaint);
            }else {
                acuantAndroidMobileSdkControllerInstance.setSubInstructionText(subInstString, subLeft,top+60,subtextPaint);
            }
        }else{
            acuantAndroidMobileSdkControllerInstance.setSubInstructionText(subInstString, subLeft,top+30,subtextPaint);
        }

        acuantAndroidMobileSdkControllerInstance.setFlashlight(false);
        acuantAndroidMobileSdkControllerInstance.setCropBarcode(false);
        acuantAndroidMobileSdkControllerInstance.setCaptureOriginalCapture(false);
        acuantAndroidMobileSdkControllerInstance.setCardCroppingListener(this);
        acuantAndroidMobileSdkControllerInstance.setAcuantErrorListener(this);
    }

    public void initListener() {
        mLicenseFront.setOnUploadLicenseListener(this, mLicenseFront.getId());
        mLicenseFace.setOnUploadLicenseListener(this, mLicenseFace.getId());
        mLicenseBack.setOnUploadLicenseListener(this, mLicenseBack.getId());

    }

    private void loadUserLicensePictures() {
        mRelProgressBar.setVisibility(View.VISIBLE);
        VolleyUtils.getSharedNetwork().loadUserInfo(DriverLicenseActivity.this,""+mUserId, new OnResponseModel() {
            @Override
            public void onResponseSuccess(Object model) {
                mRelProgressBar.setVisibility(View.INVISIBLE);
                JsonObject jsonObject = (JsonObject)model;
                UserModel userModel = ParseJSonUtil.parseByModel(jsonObject.toString(), UserModel.class);

                if(userModel.license_state!=null){
                    actionUpdateState(userModel.license_state);
                }

                mTxtLicense.setText(userModel.license_number);
                mTxtExpireMonth.setText(userModel.license_expiry_month.isEmpty() ? "" : FormatUtil.formatIn2Digit(Integer.parseInt(userModel.license_expiry_month)));
                mTxtExpireDay.setText(userModel.license_expiry_day.isEmpty() ? "" : FormatUtil.formatIn2Digit(Integer.parseInt(userModel.license_expiry_day)));
                mTxtExpireYear.setText(userModel.license_expiry_year.isEmpty() ? "" : userModel.license_expiry_year);

                //etLicenseVersion.setText(userModel.license_version);

                ((EditText)findViewById(R.id.edt_zipcode)).setText(userModel.post_code);
                ((EditText)findViewById(R.id.edt_firstName)).setText(userModel.name);
                String lastName;
                if(userModel.last_name.isEmpty()){
                    String name = userModel.name;
                    lastName = name.contains(" ")? (name.split(" ").length>1 ? name.split(" ")[1] : "") : "";
                }else {
                    lastName = userModel.last_name;
                }
                ((EditText)findViewById(R.id.edt_lastName)).setText(lastName);
                ((TextView)findViewById(R.id.txt_dob)).setText(userModel.dob);

                saveIniInfoToTrackChanging(userModel);


                if(userModel!=null){
                    UserAssetsModel userAssetsModel = userModel.getAssets();
                    if(userAssetsModel!=null){
                        final String avatar = userAssetsModel.avatar;
                        final String license = userAssetsModel.license;
                        final String license_after = userAssetsModel.license_after;
                        Log.d(TAG,"avatar: "+avatar);
                        Log.d(TAG,"license: "+license);
                        Log.d(TAG,"license_after: "+license_after);

                        /**
                         * Download driver's license pictures and set to view
                         *
                         * */
                        if(license!=null){
                            ImageLoader.getInstance().displayImage(license,mLicenseFront.getImgLicense());
                            mLicenseFront.updateVerifyLicense(true);
                            isFrontVerified = true;
                        }
                        if(license_after!=null){
                            ImageLoader.getInstance().displayImage(license_after,mLicenseBack.getImgLicense());
                            mLicenseBack.updateVerifyLicense(true);
                            isBackVerified = true;
                        }
                        if(avatar!=null){
                            ImageLoader.getInstance().displayImage(avatar,mLicenseFace.getImgLicense());
                            mLicenseFace.updateVerifyLicense(true);
                            isFaceVerified = true;
                        }
                        downloadPicture(license,"license", new DownloadStatusListener() {

                            @Override
                            public void onDownloadComplete(int id) {
                                mRelProgressBar.setVisibility(View.INVISIBLE);
                                final String imagePath = imageFolderPath + File.separator + "license";
                                //ShareReferenceUtil.saveStringReference(SettingActivity.this, Constants.KEY_USER_IMAGE_PATH, imagePath);
                                ImageUtils.showImageFromPath(DriverLicenseActivity.this,mLicenseFront.getImgLicense(),mLicenseFront.getLlImgLicenseContainer(),imagePath);
                                mLicenseFront.updateVerifyLicense(true);
                                isFrontVerified = true;
                                //Toast.makeText(DriverLicenseActivity.this,"Download picture successfully",Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onDownloadFailed(int id, int errorCode, String errorMessage) {
                                mRelProgressBar.setVisibility(View.INVISIBLE);
                                //Toast.makeText(DriverLicenseActivity.this,"Download picture fail",Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onProgress(int id, long totalBytes, long downloadedBytes, int progress) {

                            }
                        });
                        downloadPicture(license_after,"license_after", new DownloadStatusListener() {

                            @Override
                            public void onDownloadComplete(int id) {
                                mRelProgressBar.setVisibility(View.INVISIBLE);
                                final String imagePath = imageFolderPath + File.separator + "license_after";
                                ImageUtils.showImageFromPath(DriverLicenseActivity.this,mLicenseBack.getImgLicense(),mLicenseBack.getLlImgLicenseContainer(),imagePath);
                                mLicenseBack.updateVerifyLicense(true);
                                isBackVerified = true;
                                //Toast.makeText(DriverLicenseActivity.this,"Download picture successfully",Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onDownloadFailed(int id, int errorCode, String errorMessage) {
                                mRelProgressBar.setVisibility(View.INVISIBLE);
                                //Toast.makeText(DriverLicenseActivity.this,"Download picture fail",Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onProgress(int id, long totalBytes, long downloadedBytes, int progress) {

                            }
                        });
                        downloadPicture(avatar,"avatar", new DownloadStatusListener() {

                            @Override
                            public void onDownloadComplete(int id) {
                                mRelProgressBar.setVisibility(View.INVISIBLE);
                                final String imagePath = imageFolderPath + File.separator + "avatar";
                                ImageUtils.showImageFromPath(DriverLicenseActivity.this,mLicenseFace.getImgLicense(),mLicenseFace.getLlImgLicenseContainer(),imagePath);
                                mLicenseFace.updateVerifyLicense(true);
                                isFaceVerified = true;
                                //Toast.makeText(DriverLicenseActivity.this,"Download picture successfully",Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onDownloadFailed(int id, int errorCode, String errorMessage) {
                                mRelProgressBar.setVisibility(View.INVISIBLE);
                                //Toast.makeText(DriverLicenseActivity.this,"Download picture fail",Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onProgress(int id, long totalBytes, long downloadedBytes, int progress) {

                            }
                        });
                    }
                }

            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {

            }
        });
    }

    public void driverCardWithFacialButtonPressed(final CustomListener customListener2){
        System.gc();
        System.runFinalization();
        // update the model
        processedCardInformation = null;
        processedFacialData=null;
        mainActivityModel.setCurrentOptionType(CardType.DRIVERS_LICENSE);
        //mainActivityModel.clearImages();
        isProcessing=false;
        isProcessingFacial=false;
        /*if(DataContext.getInstance().getCssnLicenseDetails()!=null && DataContext.getInstance().getCssnLicenseDetails().isFacialAllowed()) {
            isFacialFlow = true;
            ToastUtil.showToastMessage(this, "isFacial: "+isFacialFlow, true);
        }else{
            isFacialFlow = false;
            ToastUtil.showToastMessage(this, "isFacial: "+isFacialFlow, true);
        }*/
        String[] data = new String[]{
                "USA","Canada","Europe",
                "Asia","Africa","Latin America","Australia"
        };
        DialogUtils.showCustomListDialog(this, true, "Select region", data, 0, new ListSelectListener() {
            @Override
            public void action(DialogInterface dialog, String selectionResult, int selection) {
                actionSelectRegion(selectionResult);
                if(customListener2!=null) customListener2.onClick();
            }
        });
    }

    private void actionSelectRegion(String selectionResult) {
        String tittle = "State";
        switch (selectionResult){
            case REGION_USA:
                tittle = "State";
                region = Region.REGION_UNITED_STATES;
                break;
            case REGION_CANADA:
                tittle = "State";
                region = Region.REGION_CANADA;
                break;
            case REGION_EUROPE:
                tittle = "Country";
                region = Region.REGION_EUROPE;
                break;
            case REGION_AFRICA:
                tittle = "Country";
                region = Region.REGION_AFRICA;
                break;
            case REGION_ASIA:
                tittle = "Country";
                region = Region.REGION_ASIA;
                break;
            case REGION_LATIN_AMERICA:
                tittle = "Country";
                region = Region.REGION_AMERICA;
                break;
            case REGION_AUSTRALIA:
                tittle = "State";
                region = Region.REGION_AUSTRALIA;
                break;
            default:
                region = Region.REGION_UNITED_STATES;
                break;
        }
        DataContext.getInstance().setCardRegion(region);

        ((TextView)findViewById(R.id.tvStateCountry)).setText(tittle);
    }

    public void presentCameraForBackSide(final Bitmap bitmap, boolean scanBackSide) {
        if (Util.LOG_ENABLED) {
            Log.d("appendLog", "public void onCardCroppedFinish(final Bitmap bitmap) - begin");
        }
        cardRegion = DataContext.getInstance().getCardRegion();
        if (bitmap != null) {
            isBackSide = scanBackSide;
            if (isBackSide) {
                mainActivityModel.setCardSideSelected(MainActivityModel.CardSide.FRONT);
            } else {
                mainActivityModel.setCardSideSelected(MainActivityModel.CardSide.BACK);
            }

            if (mainActivityModel.getCurrentOptionType() == CardType.DRIVERS_LICENSE && isBackSide) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        showDuplexDialog();
                    }
                }, 100);
            }
            updateModelAndUIFromCroppedCard(bitmap);
        } else {
            // set an error to be shown in the onResume method.
            mainActivityModel.setErrorMessage("Unable to detect the card. Please try again.");
            updateModelAndUIFromCroppedCard(originalImage);
        }

        Util.unLockScreen(this);

        if (Util.LOG_ENABLED) {
            Log.d("appendLog", "public void onCardCroppedFinish(final Bitmap bitmap) - end");
        }
        isCropping = false;
    }

    private void updateModelAndUIFromCroppedCard(final Bitmap bitmap) {
        LogUtils.log(TAG,"updateModelAndUIFromCroppedCard");
        switch (mainActivityModel.getCardSideSelected()) {
            case FRONT:
                mFilePhotoFront = null;
                try {
                    Analytics.with(DriverLicenseActivity.this).track("DL_Front_btn upload successfully");

                    mFilePhotoFront = FileUtils.createImageFile();
                    actionSaveBitmapToFile(bitmap, mFilePhotoFront);
                    mLicenseFront.updateLicenseImage(mFilePhotoFront.getAbsolutePath());
                    mLicenseFront.getImageView().setImageBitmap(bitmap);
                    actionUpdateBtnUploadAction(1);

                    //actionProcessCard(true);

                    findViewById(R.id.btnScanAgain).setVisibility(View.VISIBLE);

                } catch (IOException ex) {
                    if(ex.getMessage() == null){
                        Analytics.with(DriverLicenseActivity.this).track("Error has null value (updateModelAndUIFromCroppedCard)");
                    }else {
                        Toast.makeText(this, "Error: " + ex.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
                break;

            case BACK:
                File mFilePhotoBack = null;
                try {
                    Analytics.with(DriverLicenseActivity.this).track("DL_Back_btn upload successfully");

                    mFilePhotoBack = FileUtils.createImageFile();
                    actionSaveBitmapToFile(bitmap, mFilePhotoBack);
                    mLicenseBack.updateLicenseImage(mFilePhotoBack.getAbsolutePath());
                    mLicenseBack.getImageView().setImageBitmap(bitmap);
                    actionUpdateBtnUploadAction(1);

                    //actionProcessCard(true);

                    findViewById(R.id.btnScanAgain).setVisibility(View.VISIBLE);

                } catch (IOException ex) {
                    if(ex.getMessage() == null){
                        Analytics.with(DriverLicenseActivity.this).track("Error has null value (updateModelAndUIFromCroppedCard)");
                    }else {
                        Toast.makeText(this, "Error: " + ex.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
                break;

            default:
                throw new IllegalStateException("This method is bad implemented, there is not processing for the cardSide '"
                        + mainActivityModel.getCardSideSelected() + "'");
        }
        showErrorMessage();
    }

    private void updateLicenseInfo(DriversLicenseCard driversLicenseCard, CustomActionListener customActionListener) {
        // Show state
        if(driversLicenseCard==null){
            ToastUtil.showToastMessage(this,"Fail to get driver licence info", false);
            return;
        }
        String state = driversLicenseCard.getState();
        if(state!=null){
            actionUpdateState(state);
        }

        // Show license
        mTxtLicense.setText(driversLicenseCard.getLicense());

        // Show expire
        String licenseExpireStr = driversLicenseCard.getExpirationDate4();
        if (licenseExpireStr == null || licenseExpireStr.equals("")) {
            Toast.makeText(this, getString(R.string.error_empty_expire_date), Toast.LENGTH_LONG).show();
            return;
        } else {
            try {
                String month = licenseExpireStr.split("-")[0];

                int monthIndex = mMonthList.indexOf(month);
                if (monthIndex > -1) {
                    mTxtExpireMonth.setText(FormatUtil.formatIn2Digit(Integer.parseInt(month)));
                } else {
                    mTxtExpireMonth.setText(FormatUtil.formatIn2Digit(Integer.parseInt(mMonthList.get(0))));
                }

                String day = licenseExpireStr.split("-")[1];
                int dayIndex = mDayList.indexOf(day);
                if (dayIndex > -1) {
                    mTxtExpireDay.setText(FormatUtil.formatIn2Digit(Integer.parseInt(day)));
                } else {
                    mTxtExpireDay.setText(FormatUtil.formatIn2Digit(Integer.parseInt(mDayList.get(0))));
                }

                String year = licenseExpireStr.split("-")[2];

                int yearIndex = mYearList.indexOf(year);
                if (yearIndex > -1) {
                    mTxtExpireYear.setText(year);
                } else {
                    mTxtExpireYear.setText("2018");
                }

            } catch (Exception e) {
                if(e.getMessage() == null){
                    Analytics.with(DriverLicenseActivity.this).track("Error has null value (updateLicenseInfo)");
                }else {
                    Toast.makeText(DriverLicenseActivity.this, "Error: " + e.toString(), Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }

        //Show name
        if(driversLicenseCard.getNameFirst()!=null){
            ((EditText)findViewById(R.id.edt_firstName)).setText(driversLicenseCard.getNameFirst());
        }
        if(driversLicenseCard.getNameLast()!=null){
            ((EditText)findViewById(R.id.edt_lastName)).setText(driversLicenseCard.getNameLast());
        }
        if(driversLicenseCard.getZip()!=null){
            ((EditText)findViewById(R.id.edt_zipcode)).setText(driversLicenseCard.getZip());
        }
        if(driversLicenseCard.getDateOfBirth4()!=null){
            String s = driversLicenseCard.getDateOfBirth4();
            ((TextView)findViewById(R.id.txt_dob)).setText(FormatTimeUtils.changeFormatDate("MM-dd-yyyy","yyyy-MM-dd", s));
        }

//        mScrollView.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                mScrollView.fullScroll(ScrollView.FOCUS_DOWN);
//            }
//        },1000);

        actionUpdateBtnUploadAction(1);

        if(customActionListener!=null) customActionListener.takeAction();
    }

    private void actionRequestPermission() {

        String[] PERMISSIONS = {Manifest.permission.CAMERA,Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.NFC, Manifest.permission.ACCESS_FINE_LOCATION};
        if(!PermissionsUtils.checkPermissions(this, PERMISSIONS)){
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);

        }
    }

    private Boolean actionRequestCameraPermission() {

        String[] PERMISSIONS = {Manifest.permission.CAMERA,Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
        if(!PermissionsUtils.checkPermissions(this, PERMISSIONS)){
//            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_CAMERA);

            mRequestPermissionHandler.requestPermission(this, PERMISSIONS , 123, new RequestPermissionHandler.RequestPermissionListener() {
                @Override
                public void onSuccess() {
//                    Toast.makeText(DriverLicenseActivity.this, "request permission success", Toast.LENGTH_SHORT).show();

                    showCameraInterface();
                }

                @Override
                public void onFailed() {
//                    Toast.makeText(DriverLicenseActivity.this, "request permission failed", Toast.LENGTH_SHORT).show();
                }
            });

            return false;
        }

        return true;
    }

    private void actionLoadUserData() {
        LogUtils.log(TAG,"actionLoadUserData");
        showOrHideProgressBar(true);
        VolleyUtils.getSharedNetwork().loadUserInfo(this,"" + mUserId, new OnResponseModel() {
            @Override
            public void onResponseSuccess(Object model) {
                showOrHideProgressBar(false);
                JsonObject jsonObject = (JsonObject) model;
                UserModel userModel = ParseJSonUtil.parseByModel(jsonObject.toString(), UserModel.class);

                if(userModel.license_state!=null){
                    actionUpdateState(userModel.license_state);
                }

                mTxtLicense.setText(userModel.license_number);
                mTxtExpireMonth.setText(userModel.license_expiry_month.isEmpty() ? "" : userModel.license_expiry_month);
                mTxtExpireDay.setText(userModel.license_expiry_day.isEmpty() ? "" : userModel.license_expiry_day);
                mTxtExpireYear.setText(userModel.license_expiry_year.isEmpty() ? "" : userModel.license_expiry_year);

                //etLicenseVersion.setText(userModel.license_version);

                ((EditText)findViewById(R.id.edt_zipcode)).setText(userModel.post_code);
                ((EditText)findViewById(R.id.edt_firstName)).setText(userModel.name);
                String lastName;
                if(userModel.last_name.isEmpty()){
                    String name = userModel.name;
                    lastName = name.contains(" ")? (name.split(" ").length>1 ? name.split(" ")[1] : "") : "";
                }else {
                    lastName = userModel.last_name;
                }
                ((EditText)findViewById(R.id.edt_lastName)).setText(lastName);
                ((TextView)findViewById(R.id.txt_dob)).setText(userModel.dob);

                saveIniInfoToTrackChanging(userModel);

                UserAssetsModel userAssetsModel = userModel.getAssets();
                if (userAssetsModel != null) {
                    final String avatar = userAssetsModel.avatar;
                    final String license = userAssetsModel.license;
                    final String license_after = userAssetsModel.license_after;

                    if(license!=null){
                        ImageLoader.getInstance().displayImage(license, mLicenseFront.getImgLicense());
                        mLicenseFront.updateVerifyLicense(true);
                        isFrontVerified = true;
                    }
                    if(license_after!=null){
                        ImageLoader.getInstance().displayImage(license_after,mLicenseBack.getImgLicense());
                        mLicenseBack.updateVerifyLicense(true);
                        isBackVerified = true;
                    }
                    if(avatar!=null){
                        ImageLoader.getInstance().displayImage(avatar,mLicenseFace.getImgLicense());
                        mLicenseFace.updateVerifyLicense(true);
                        isFaceVerified = true;
                    }

                    Target target = new Target() {
                        @Override
                        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                            LogUtils.log("bitmap","bitmap: "+bitmap);
                            LogUtils.log("bitmap","byteCount: "+bitmap.getByteCount());
                            try {
                                isProcessingFacial = false;
                                mFilePhotoFront = FileUtils.createImageFile();
                                actionSaveBitmapToFile(bitmap, mFilePhotoFront);
                                cardRegion = DataContext.getInstance().getCardRegion();
                                actionProcessCard(false);
                                processAvatarFromDriverLicense = true;

                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onBitmapFailed(Drawable errorDrawable) {

                        }

                        @Override
                        public void onPrepareLoad(Drawable placeHolderDrawable) {

                        }
                    };
                    Picasso.with(DriverLicenseActivity.this).load(license).into(target);

                    if(isFrontVerified && isBackVerified && isFaceVerified){
                        findViewById(R.id.btnScanAgain).setVisibility(View.VISIBLE);
                    }

                    /*downloadPicture(license,"license", new DownloadStatusListener() {

                        @Override
                        public void onDownloadComplete(int id) {
                            showOrHideProgressBar(false);
                            final String imagePath = imageFolderPath + File.separator + "license";
                            //ShareReferenceUtil.saveStringReference(SettingActivity.this, Constants.KEY_USER_IMAGE_PATH, imagePath);
                            ImageUtils.showImageFromPath(getThis(), mLicenseFront.getImgLicense(), mLicenseFront.getLlImgLicenseContainer(), imagePath);
                            mLicenseFront.updateVerifyLicense(true);
                            isFrontVerified = true;

                            actionStartRequest(ImageUtils.createBitmapFromFile(imagePath, mLicenseFront.getLlImgLicenseContainer()));
                        }

                        @Override
                        public void onDownloadFailed(int id, int errorCode, String errorMessage) {
                            showOrHideProgressBar(false);
                        }

                        @Override
                        public void onProgress(int id, long totalBytes, long downloadedBytes, int progress) {

                        }
                    });
                    downloadPicture(license_after, "license_after", new DownloadStatusListener() {

                        @Override
                        public void onDownloadComplete(int id) {
                            showOrHideProgressBar(false);
                            final String imagePath = imageFolderPath + File.separator + "license_after";
                            ImageUtils.showImageFromPath(getThis(), mLicenseBack.getImgLicense(), mLicenseBack.getLlImgLicenseContainer(), imagePath);
                            mLicenseBack.updateVerifyLicense(true);
                            isBackVerified = true;
                        }

                        @Override
                        public void onDownloadFailed(int id, int errorCode, String errorMessage) {
                            showOrHideProgressBar(false);
                        }

                        @Override
                        public void onProgress(int id, long totalBytes, long downloadedBytes, int progress) {

                        }
                    });
                    downloadPicture(avatar, "avatar", new DownloadStatusListener() {

                        @Override
                        public void onDownloadComplete(int id) {
                            showOrHideProgressBar(false);
                            final String imagePath = imageFolderPath + File.separator + "avatar";
                            avatarBitmap = ImageUtils.showImageFromPath(getThis(), mLicenseFace.getImgLicense(), mLicenseFace.getLlImgLicenseContainer(), imagePath);
                            mLicenseFace.updateVerifyLicense(true);
                            isFaceVerified = true;
                        }

                        @Override
                        public void onDownloadFailed(int id, int errorCode, String errorMessage) {
                            showOrHideProgressBar(false);
                        }

                        @Override
                        public void onProgress(int id, long totalBytes, long downloadedBytes, int progress) {

                        }
                    });*/
                }

            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                showOrHideProgressBar(false);
                VolleyUtils.handleErrorRespond(DriverLicenseActivity.this, error);
            }
        });
    }

    private void saveIniInfoToTrackChanging(UserModel userModel) {
        if(userModel==null) return;
        iniInfo = new IniInfo();
        iniInfo.licenseNumber = userModel.license_number;
        iniInfo.licenseExpMonth = userModel.license_expiry_month;
        iniInfo.licenseExpYear = userModel.license_expiry_year;
        iniInfo.zipCode = userModel.post_code;
        iniInfo.name = userModel.name;
        iniInfo.lastName = userModel.last_name;
        iniInfo.ssn = userModel.ssn;
        iniInfo.dob = userModel.dob;
    }

    private void actionSaveBitmapToFile(Bitmap bitmap, File file) {
        LogUtils.log(TAG,"actionSaveBitmapToFile");
        try {
            FileOutputStream fos = new FileOutputStream(file.getAbsolutePath());
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.flush();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void actionProcessCard(boolean showLoadingView) {
      //  ToastUtil.showToastMessage(this, "process card", true);

        LogUtils.log(TAG,"actionProcessCard");
        if(showLoadingView){
            if(progressDialog!=null && progressDialog.isShowing()){
                Util.dismissDialog(progressDialog);
            }
            progressDialog = Util.showProgessDialog(DriverLicenseActivity.this, getString(R.string.waiting_capture_image));
            Util.lockScreen(this);
        }
        if ((!isProcessing && processedCardInformation==null)) {
            isProcessing = true;
            // check for the internet connection
            if (!Utils.isNetworkAvailable(this)) {
                String msg = getString(R.string.error_no_internet_message);
                Utils.appendLog(TAG, msg);
                Util.dismissDialog(alertDialog);
                alertDialog = Util.showDialog(this, msg,new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        isShowErrorAlertDialog = false;
                    }
                });
                isShowErrorAlertDialog = true;
                return;
            }

            ProcessImageRequestOptions options = ProcessImageRequestOptions.getInstance();
            options.autoDetectState = true;
            options.stateID = -1;
            options.reformatImage = true;
            options.reformatImageColor = 0;
            options.DPI = 150;
            options.cropImage = false;
            options.faceDetec = true;
            options.signDetec = true;
            options.iRegion = DataContext.getInstance().getCardRegion();
            options.acuantCardType = mainActivityModel.getCurrentOptionType();
            BitmapDrawable drawable = (BitmapDrawable) mLicenseFront.getImageView().getDrawable();
            Bitmap front_bitmap = drawable.getBitmap();

            BitmapDrawable back_drawable = (BitmapDrawable) mLicenseBack.getImageView().getDrawable();
            Bitmap back_bitmap = null;
            if(back_drawable!=null) {
                back_bitmap = back_drawable.getBitmap();
            }
            acuantAndroidMobileSdkControllerInstance.callProcessImageServices(front_bitmap, back_bitmap, sPdf417String, this, options);
            resetPdf417String();
        }
    }

    private void actionUploadPictures(boolean change1, boolean change2, boolean change3) {
        LogUtils.log(TAG,"actionUploadPictures");
        if(change1){
            mLicenseFront.updateVerifyLicense(true);
            BitmapDrawable drawableFront = (BitmapDrawable) mLicenseFront.getImageView().getDrawable();
            actionCallApiToUploadPicture(ImageUtils.getFileDataFromBitmap(drawableFront.getBitmap()), VolleyUtils.getUrlUploadLicenseImageFront("" + mUserId));
        }
        if(change2){
            mLicenseBack.updateVerifyLicense(true);
            BitmapDrawable drawableBack = (BitmapDrawable) mLicenseBack.getImageView().getDrawable();
            actionCallApiToUploadPicture(ImageUtils.getFileDataFromBitmap(drawableBack.getBitmap()), VolleyUtils.getUrlUploadLicenseImageBack("" + mUserId));
        }
        if(change3){
            mLicenseFace.updateVerifyLicense(true);
            BitmapDrawable drawableFace = (BitmapDrawable) mLicenseFace.getImageView().getDrawable();
            actionCallApiToUploadPicture(ImageUtils.getFileDataFromBitmap(drawableFace.getBitmap()), VolleyUtils.getUrlUploadLicenseImageFace("" + mUserId));
        }
    }

    private void actionBack() {
        switch (situation){
            case SETTING:
            case CONFIRM_BOOKING:
            case FORCE_RE_UPLOAD_WITH_ACUANT:
                finish();
                return;
            case REGISTER:
                DialogUtils.showMessageDialog(this, true, null, getString(R.string.confirm_to_exit),
                        DriverLicenseActivity.this.getString(R.string.btn_yes),
                        DriverLicenseActivity.this.getString(R.string.btn_no),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Intent intent = new Intent(DriverLicenseActivity.this, LoginActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                finish();
                            }
                        },
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.cancel();
                            }
                        });
                break;
        }
    }

    private void actionCallApiUploadLicenseStateToServer(final CustomListener1 customListener) {
        Analytics.with(DriverLicenseActivity.this).track("Click DL_upload_verify_btn");

        String licenseNumber = mTxtLicense.getText().toString();
        if(mCurrentState==null){
            Toast.makeText(this, getString(R.string.error_empty_state), Toast.LENGTH_LONG).show();
            return;
        }
        if (((EditText)findViewById(R.id.edt_zipcode)).getText().toString().trim().equals("")) {
            Toast.makeText(this, getString(R.string.error_empty_zipcode), Toast.LENGTH_LONG).show();
            return;
        }
        if (licenseNumber.equals("")) {
            Toast.makeText(this, getString(R.string.error_empty_license), Toast.LENGTH_LONG).show();
            return;
        }
        if(mTxtExpireMonth.getText().toString().equals("") || mTxtExpireYear.getText().toString().equals("")){
            Toast.makeText(this, getString(R.string.error_empty_expire_date), Toast.LENGTH_LONG).show();
            return;
        }
        if (((EditText)findViewById(R.id.edt_firstName)).getText().toString().trim().equals("")) {
            Toast.makeText(this, getString(R.string.error_empty_fisrtname), Toast.LENGTH_LONG).show();
            return;
        }
        if (((EditText)findViewById(R.id.edt_lastName)).getText().toString().trim().equals("")) {
            Toast.makeText(this, getString(R.string.error_empty_lastname), Toast.LENGTH_LONG).show();
            return;
        }
        if (((TextView)findViewById(R.id.txt_dob)).getText().toString().trim().equals("")) {
            Toast.makeText(this, getString(R.string.error_empty_dob), Toast.LENGTH_LONG).show();
            return;
        }
        // Validate expire time
        String month = mTxtExpireMonth.getText().toString();
        String day = mTxtExpireDay.getText().toString();
        String year = mTxtExpireYear.getText().toString();

        Calendar nowCalendar = Calendar.getInstance();

        Calendar expireCalendar = Calendar.getInstance();
        expireCalendar.set(Calendar.MONTH, Integer.parseInt(month) - 1);
        expireCalendar.set(Calendar.YEAR, Integer.parseInt(year));
        expireCalendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(day));

        if (nowCalendar.getTimeInMillis() > expireCalendar.getTimeInMillis()) {
            Toast.makeText(this, getString(R.string.error_expire_date_greater_than_current_date), Toast.LENGTH_SHORT).show();
            return;
        }

        //
        JSONObject json = new JSONObject();
        try {
            json.put("facial_match_rating", facialMatchRate);
            json.put("license_state", mCurrentState);
            json.put("license_number", licenseNumber);
            json.put("license_expiry_month", month);
            json.put("license_expiry_day", day);
            json.put("license_expiry_year", year);
            json.put("post_code", ((EditText)findViewById(R.id.edt_zipcode)).getText().toString().trim());
            json.put("name", ((EditText)findViewById(R.id.edt_firstName)).getText().toString().trim());
            json.put("last_name", ((EditText)findViewById(R.id.edt_lastName)).getText().toString().trim());
            json.put("dob", ((TextView)findViewById(R.id.txt_dob)).getText().toString().trim()); //1990-12-23
            json.put("license_country", "USA");
            json.put("is_acuant", true);

        } catch (Exception e) {
            Toast.makeText(this, "Error: "+e.toString(), Toast.LENGTH_SHORT).show();
        }

        LoadingViewUtils.showOrHideProgressBar(true, this);
        VolleyUtils.getSharedNetwork().updateUserInfo(this, mUserId+"", json, new OnResponseModel() {
            @Override
            public void onResponseSuccess(Object model) {
                LoadingViewUtils.showOrHideProgressBar(false, DriverLicenseActivity.this);

                JsonObject jsonObject = (JsonObject) model;
                UserModel user = ParseJSonUtil.parseByModel(jsonObject.toString(), UserModel.class);
                saveUser(user);

                if(customListener!=null) customListener.takeAction();
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                VolleyUtils.handleErrorRespond(DriverLicenseActivity.this, error);
                LoadingViewUtils.showOrHideProgressBar(false, DriverLicenseActivity.this);
            }
        });
    }

    private void actionFinishUploadDriverLicensePicture() {
        LogUtils.log(TAG,"actionFinishUploadDriverLicensePicture");
        setResult(RESULT_OK);
        finish();
        overridePendingTransition(R.anim.fade_in_normal, R.anim.fade_out_normal);
    }

    private void actionUpdateBtnUploadAction(int i) {
        LogUtils.log(TAG,"actionUpdateBtnUploadAction");

        if(i==1){
            boolean[] c = checkChangedNumber();
            switch (situation){
                case REGISTER:
                    if(c[0]&c[1]&c[2]){
                        mBtnVerify.setBackground(getResources().getDrawable(R.drawable.round_brand_color_big_rad));
                        mBtnVerify.setClickable(true);
                    } else {
                        mBtnVerify.setBackground(getResources().getDrawable(R.drawable.round_gray_big_rad));
                        mBtnVerify.setClickable(false);
                    }
                    break;
                case SETTING:
                case CONFIRM_BOOKING:
                    /*if(c[0]|c[1]|c[2]){
                        mBtnVerify.setBackgroundColor(getColor(R.color.buttonColor));
                        mBtnVerify.setClickable(true);
                    } else {
                        mBtnVerify.setBackgroundColor(getColor(R.color.bg_btn_disable));
                        mBtnVerify.setClickable(false);
                    }*/
                    break;
                case FORCE_RE_UPLOAD_WITH_ACUANT:
                    if(c[0]&c[1]){
                        mBtnVerify.setBackground(getResources().getDrawable(R.drawable.round_brand_color_big_rad));
                        mBtnVerify.setClickable(true);
                    } else {
                        mBtnVerify.setBackground(getResources().getDrawable(R.drawable.round_gray_big_rad));
                        mBtnVerify.setClickable(false);
                    }
                    break;

            }
        }

        //i = 2: After upload picture finished
        if(i==2){
            switch (situation){
                case REGISTER:
                    mBtnVerify.setBackground(getResources().getDrawable(R.drawable.round_brand_color_big_rad));
                    mBtnVerify.setText(getString(R.string.btn_next));
                    break;
                case SETTING:
                case CONFIRM_BOOKING:
                    /*mBtnVerify.setBackgroundColor(getColor(R.color.bg_btn_disable));
                    mBtnVerify.setClickable(false);*/
                    break;
                case FORCE_RE_UPLOAD_WITH_ACUANT:
                    break;
            }
        }
    }

    private void actionSideCaptureClick(int id) {
        LogUtils.log(TAG,"onUploadLicenseListener");

        if (id == mLicenseFront.getId()) {
            driverCardWithFacialButtonPressed(new CustomListener() {
                @Override
                public void onClick() {
                    isBackSide = false;
                    mainActivityModel.setCardSideSelected(MainActivityModel.CardSide.FRONT);

                    if(actionRequestCameraPermission())
                      showCameraInterface();
                }
            });

        } else if (id == mLicenseBack.getId()) {
            isBackSide = true;
            mainActivityModel.setCardSideSelected(MainActivityModel.CardSide.BACK);
            showCameraInterface();

        } else {
            Intent intent = new Intent(this, TakePhotoActivity.class);
            intent.putExtra(TakePhotoActivity.KEY_IS_CROP_PHOTO, true);
            intent.putExtra(TakePhotoActivity.PHOTO_ID, id);
            startActivityForResult(intent, REQUEST_CODE_TAKE_PHOTO_ACTIVITY);

            isProcessingFacial = false;
            //showFacialDialog();
        }

    }

    private void actionCallApiToUploadPicture(byte[] fileData, String url) {
        LogUtils.log(TAG,"actionCallApiToUploadPicture");
        mRelProgressBar.setVisibility(View.VISIBLE);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        DataOutputStream dataOutputStream = new DataOutputStream(byteArrayOutputStream);
        try {
            ImageUtils.buildPart(dataOutputStream, fileData);
            multipartBody = byteArrayOutputStream.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
        }


        if (mHandler != null) {
            mHandler.removeCallbacks(mUploadPictureRunnable);
            mHandler = null;
        }
        mHandler = new Handler();
        mUploadPictureRunnable.run();

        String mimeType = "multipart/form-data";
        MultipartRequest multipartRequest = new MultipartRequest(
                url,
                VolleyUtils.genHeaders3(),
                mimeType,
                multipartBody,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {
                        tvMenuCountry.setEnabled(false);

                        mLicenseFront.setEnabled(false);
                        mLicenseBack.setEnabled(false);
                        mLicenseFace.setEnabled(false);
                        mTxtLicense.setEnabled(false);
                        mTxtExpireMonth.setEnabled(false);
                        mTxtExpireDay.setEnabled(false);
                        mTxtExpireYear.setEnabled(false);

                        mDownloadFinished = MAX_DOWNLOAD_FINISHED;

                        actionUpdateBtnUploadAction(2);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mDownloadFinished = MAX_DOWNLOAD_FINISHED;
                        if(error.getMessage() == null){
                            Analytics.with(DriverLicenseActivity.this).track("Error has null value (actionCallApiToUploadPicture)");
                        }else {
                            Toast.makeText(DriverLicenseActivity.this, "Error: "+ error.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }
        );

        VolleySingleton.getInstance(DriverLicenseActivity.this).addToRequestQueue(multipartRequest);
    }

    private void actionUpdateState(String state) {
        for (StateModel s: ViewUtil.getStates(this, region)) {
            if(s.getCode().equals(state)){
                tvMenuCountry.setText(s.getName());
                mCurrentState = s.getCode();
            }
        }
    }

    public void processImageValidation(Bitmap faceImage,Bitmap idCroppedFaceImage) {
        LogUtils.log(TAG, "processImageValidation");
        LogUtils.log("driver_licence","processImageValidation, bitmap: "+faceImage+", compareBitmap: "+idCroppedFaceImage );
        if(processedCardInformation!=null){
            isProcessingFacial=false;
        }
        mainActivityModel.setCurrentOptionType(CardType.FACIAL_RECOGNITION);
        if (!Util.isNetworkAvailable(this)) {
            String msg = getString(R.string.no_internet_message);
            Log.d(TAG, msg);
            Util.dismissDialog(alertDialog);
            alertDialog = Util.showDialog(this, msg,new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    isShowErrorAlertDialog = false;
                }
            });
            isShowErrorAlertDialog = true;
            return;
        }


        ProcessImageRequestOptions options = ProcessImageRequestOptions.getInstance();
        options.acuantCardType = CardType.FACIAL_RECOGNITION;
        acuantAndroidMobileSdkControllerInstance.callProcessImageServices(faceImage, idCroppedFaceImage, null, this, options);
    }
    @Override
    public void onFacialRecognitionCompleted(final Bitmap newAvatarBitmap) {
        LogUtils.log(TAG, "onFacialRecognitionCompleted");
        LogUtils.log("driver_licence","onFacialRecognitionCompleted, bitmap: "+newAvatarBitmap+", processCardInformation: "+processedCardInformation);
        if(isShowErrorAlertDialog){
            return;
        }
        runOnUiThread(new Runnable(){
            @Override
            public void run() {
                Util.lockScreen(DriverLicenseActivity.this);
                if(progressDialog!=null && progressDialog.isShowing()){
                    Util.dismissDialog(progressDialog);
                }
                progressDialog = Util.showProgessDialog(DriverLicenseActivity.this, "Capturing data ...");
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        while(isProcessing){
                            try {
                                Thread.sleep(100);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                        // process the card
                        if(processedCardInformation instanceof DriversLicenseCard) {
                            DriversLicenseCard dlCard = (DriversLicenseCard)processedCardInformation;
                            if(dlCard!=null) {
                                processImageValidation(newAvatarBitmap, dlCard.getFaceImage());
                            }else{
                                processImageValidation(newAvatarBitmap, null);
                            }
                        }else if(processedCardInformation instanceof PassportCard) {
                            PassportCard passportCard = (PassportCard) processedCardInformation;
                            if(passportCard!=null) {
                                processImageValidation(newAvatarBitmap, passportCard.getFaceImage());
                            }else{
                                processImageValidation(newAvatarBitmap, null);
                            }
                        }else {
                            progressDialog.cancel();
                            if(avatarFromDriverLicense!=null){
                                processImageValidation(newAvatarBitmap, avatarFromDriverLicense);
                            }
                        }
                        if(newAvatarBitmap!=null){
                            mLicenseFace.post(new Runnable() {
                                @Override
                                public void run() {
                                    mLicenseFace.getImageView().setImageBitmap(newAvatarBitmap);

                                    try {
                                        Analytics.with(DriverLicenseActivity.this).track("DL_Face_btn upload successfully");

                                        File fileBack = FileUtils.createImageFile();
                                        actionSaveBitmapToFile(newAvatarBitmap, fileBack);
                                        mLicenseFace.updateLicenseImage(fileBack.getAbsolutePath());
                                        mLicenseFace.getImageView().setImageBitmap(newAvatarBitmap);
                                        actionUpdateBtnUploadAction(1);

                                        Analytics.with(DriverLicenseActivity.this).track("DL_Face_btn upload successfully");

                                    } catch (IOException ex) {
                                        if(ex.getMessage() == null){
                                            Analytics.with(DriverLicenseActivity.this).track("Error has null value (onFacialRecognitionCompleted)");
                                        }else {
                                            Toast.makeText(DriverLicenseActivity.this, "Error: "+ex.getMessage(), Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                }
                            });
                        }
                    }
                }).start();

            }
        });
    }
    @Override
    public void onFacialRecognitionCanceled() {
        LogUtils.log(TAG, "onFacialRecognitionCanceled");
        LogUtils.log("driver_licence","onFacialRecognitionCanceled");
        isProcessingFacial=false;
    }
    @Override
    public void onFacialRecognitionTimedOut(Bitmap bitmap) {
        LogUtils.log(TAG, "onFacialRecognitionTimedOut");
        LogUtils.log("driver_licence","onFacialRecognitionTimedOut");
        isProcessingFacial=false;
        onFacialRecognitionCompleted(bitmap);
    }

    private void showCameraInterface() {
        LogUtils.log(TAG,"showCameraInterface");
        final int currentOptionType = mainActivityModel.getCurrentOptionType();
        cardRegion = DataContext.getInstance().getCardRegion();
        alertDialog = new AlertDialog.Builder(this).create();
        LicenseDetails license_details = DataContext.getInstance().getCssnLicenseDetails();
        if (currentOptionType == CardType.PASSPORT) {
            acuantAndroidMobileSdkControllerInstance.setWidth(AcuantUtil.DEFAULT_CROP_PASSPORT_WIDTH);
        }else if (currentOptionType == CardType.MEDICAL_INSURANCE) {
            acuantAndroidMobileSdkControllerInstance.setWidth(AcuantUtil.DEFAULT_CROP_MEDICAL_INSURANCE);
        } else {
            if(license_details!=null && license_details.isAssureIDAllowed()) {
                acuantAndroidMobileSdkControllerInstance.setWidth(AcuantUtil.DEFAULT_CROP_DRIVERS_LICENSE_WIDTH_FOR_AUTHENTICATION);
            }else {
                acuantAndroidMobileSdkControllerInstance.setWidth(AcuantUtil.DEFAULT_CROP_DRIVERS_LICENSE_WIDTH);
            }
            //acuantAndroidMobileSdkControllerInstance.setWidth(2600);
        }
        acuantAndroidMobileSdkControllerInstance.setInitialMessageDescriptor(R.layout.align_and_tap);
        acuantAndroidMobileSdkControllerInstance.setFinalMessageDescriptor(R.layout.hold_steady);
        acuantAndroidMobileSdkControllerInstance.showManualCameraInterface(this, currentOptionType, cardRegion, isBackSide);
    }

    private void showDuplexDialog() {
        mainActivity = this;
        cardRegion = DataContext.getInstance().getCardRegion();
        Util.dismissDialog(showDuplexAlertDialog);
        Util.dismissDialog(alertDialog);
        showDuplexAlertDialog = new AlertDialog.Builder(this).create();
        showDuplexAlertDialog = Util.showDialog(this, getString(R.string.dl_duplex_dialog), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (cardRegion == Region.REGION_UNITED_STATES || cardRegion == Region.REGION_CANADA) {
                    acuantAndroidMobileSdkControllerInstance.setInitialMessageDescriptor(R.layout.tap_to_focus);
                    acuantAndroidMobileSdkControllerInstance.showCameraInterfacePDF417(mainActivity, CardType.DRIVERS_LICENSE, cardRegion);
                } else {
                    acuantAndroidMobileSdkControllerInstance.showManualCameraInterface(mainActivity, CardType.DRIVERS_LICENSE, cardRegion, isBackSide);
                }
                showDuplexAlertDialog.dismiss();
                isShowDuplexDialog = false;
            }
        });
        isShowDuplexDialog = true;
    }

    private void showFacialDialog() {
        LogUtils.log(TAG, "showFacialDialog");
        LogUtils.log("driver_licence","showFacialDialog");
        try {
            new AlertDialog.Builder(this)
                    .setCancelable(false)
                    .setMessage(getString(R.string.facial_instruction_dialog))
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            acuantAndroidMobileSdkControllerInstance.setFacialListener(DriverLicenseActivity.this);
                            isProcessingFacial = acuantAndroidMobileSdkControllerInstance.showManualFacialCameraInterface(DriverLicenseActivity.this);
                            dialog.dismiss();
                        }
                    }).show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void showErrorMessage() {
        LogUtils.log(TAG,"showErrorMessage");
        if (mainActivityModel.getErrorMessage() != null) {
            Util.dismissDialog(alertDialog);

            alertDialog = Util.showDialog(this, mainActivityModel.getErrorMessage(), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    mainActivityModel.setErrorMessage(null);
                    isShowErrorAlertDialog = false;
                }
            });
            isShowErrorAlertDialog = true;
        }
    }

    private void showPopUpMenuMonth() {
        PopupMenu popup = new PopupMenu(this, mTxtExpireMonth);
        //Inflating the Popup using xml file
        popup.getMenuInflater().inflate(R.menu.postcode_menu, popup.getMenu());

        //Programmatically add item
        Menu menu = popup.getMenu();
        for (int i = 0; i < mMonthList.size(); i++) {
            menu.add(0, i, i, mMonthList.get(i));
        }

        //registering popup with OnMenuItemClickListener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                mTxtExpireMonth.setText(item.getTitle().toString());
                return true;
            }
        });

        popup.show();
    }

    private void showPopUpMenuDay() {
        PopupMenu popup = new PopupMenu(this, mTxtExpireDay);
        //Inflating the Popup using xml file
        popup.getMenuInflater().inflate(R.menu.postcode_menu, popup.getMenu());

        //Programmatically add item
        Menu menu = popup.getMenu();
        for (int i = 0; i < mDayList.size(); i++) {
            menu.add(0, i, i, mDayList.get(i));
        }

        //registering popup with OnMenuItemClickListener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                mTxtExpireDay.setText(item.getTitle().toString());
                return true;
            }
        });

        popup.show();
    }

    private void showPopUpMenuYear() {
        PopupMenu popup = new PopupMenu(this, mTxtExpireYear);
        //Inflating the Popup using xml file
        popup.getMenuInflater().inflate(R.menu.postcode_menu, popup.getMenu());

        //Programmatically add item
        Menu menu = popup.getMenu();
        for (int i = 0; i < mYearList.size(); i++) {
            menu.add(0, i, i, mYearList.get(i));
        }

        //registering popup with OnMenuItemClickListener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                mTxtExpireYear.setText(item.getTitle().toString());
                return true;
            }
        });

        popup.show();
    }

    private void showOrHideProgressBar(boolean val) {
        if(val){
            //progressBar.setVisibility(View.VISIBLE);
            mRelProgressBar.setVisibility(View.VISIBLE);
        }else {
            //progressBar.setVisibility(View.INVISIBLE);
            mRelProgressBar.setVisibility(View.INVISIBLE);
        }
    }

    public void showData(Card card,FacialData facialData){
        String dialogMessage = null;
        try {
            DataContext.getInstance().setCardType(mainActivityModel.getCurrentOptionType());

            if (card == null || card.isEmpty()) {
                dialogMessage = "No data found for this license card!";
            } else {

                switch (mainActivityModel.getCurrentOptionType()) {
                    case CardType.DRIVERS_LICENSE:
                        DataContext.getInstance().setProcessedLicenseCard((DriversLicenseCard) card);
                        break;

                    case CardType.MEDICAL_INSURANCE:
                        DataContext.getInstance().setProcessedMedicalCard((MedicalCard) card);
                        break;

                    case CardType.PASSPORT:
                        DataContext.getInstance().setProcessedPassportCard((PassportCard) card);
                        break;
                    case CardType.FACIAL_RECOGNITION:
                        if( processedCardInformation instanceof DriversLicenseCard) {
                            DriversLicenseCard dlCard = (DriversLicenseCard)processedCardInformation;
                            DataContext.getInstance().setProcessedLicenseCard(dlCard);
                            DataContext.getInstance().setCardType(CardType.DRIVERS_LICENSE);
                        }else if(processedCardInformation instanceof PassportCard) {
                            PassportCard passportCard = (PassportCard) processedCardInformation;
                            DataContext.getInstance().setProcessedPassportCard(passportCard);
                            DataContext.getInstance().setCardType(CardType.PASSPORT);
                        }
                        DataContext.getInstance().setProcessedFacialData(processedFacialData);
                        break;
                    default:
                        throw new IllegalStateException("There is not implementation for processing the card type '"
                                + mainActivityModel.getCurrentOptionType() + "'");
                }

                Util.unLockScreen(DriverLicenseActivity.this);
                Intent showDataActivityIntent = new Intent(this, ShowDataActivity.class);
                showDataActivityIntent.putExtra("FACIAL",isFacialFlow);
                this.startActivityForResult(showDataActivityIntent,100);
            }


        } catch (Exception e) {
            Log.d(TAG, e.getMessage());
            dialogMessage = "Sorry! Internal error has occurred, please contact us!";

        }

        if (dialogMessage != null) {
            Util.dismissDialog(alertDialog);
            alertDialog = Util.showDialog(this, dialogMessage, new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    isShowErrorAlertDialog = false;
                }
            });
            isShowErrorAlertDialog = true;
        }
    }

    private void resetPdf417String() {
        LogUtils.log(TAG,"resetPdf417String");
        sPdf417String = "";
    }

    private boolean[] checkChangedNumber() {
        boolean change1 = false;
        boolean change2 = false;
        boolean change3 = false;
        if (mLicenseFront.getUrl() != null && !mLicenseFront.getUrl().equals("")) {
            change1 = true;
        }
        if (mLicenseBack.getUrl() != null && !mLicenseBack.getUrl().equals("")) {
            change2 = true;
        }
        if (mLicenseFace.getUrl() != null && !mLicenseFace.getUrl().equals("")) {
            change3 = true;
        }
        return new boolean[]{change1,change2,change3};
    }

    private void downloadPicture(String fileUrl, String fileName, DownloadStatusListener listener) {
        if (fileUrl == null) return;
        showOrHideProgressBar(true);
        String imageFolderPath = Environment.getExternalStorageDirectory() + File.separator + ".yoogo";
        Uri downloadUri = Uri.parse(fileUrl);
        Uri destinationUri = Uri.parse(imageFolderPath + File.separator + fileName);

        DownloadRequest downloadRequest = new DownloadRequest(downloadUri)
                //.addCustomHeader("Auth-Token", "YourTokenApiKey")
                .setRetryPolicy(new DefaultRetryPolicy())
                .setDestinationURI(destinationUri).setPriority(DownloadRequest.Priority.HIGH)
                .setDownloadContext(DriverLicenseActivity.this)//Optional
                .setDownloadListener(listener);

        thinDownloadManager.add(downloadRequest);
    }

    private void createFolderHideImage() {
        imageFolderPath = Environment.getExternalStorageDirectory() + File.separator + ".geotab";
        File folder = new File(imageFolderPath);
        if (!folder.exists()) {
            folder.mkdir();
        } else {
            String[] myFiles = folder.list();
            if (myFiles != null && myFiles.length > 0) {
                for (String myFile : myFiles) {
                    deleteImage(this, imageFolderPath + File.separator + myFile);
                }
            }
        }
    }

    public static void deleteImage(Activity activity, String imagePath) {
        File fdelete = new File(imagePath);
        if (fdelete.exists()) {
            if (fdelete.delete()) {
                Log.e("-->", "file Deleted :" + imagePath);
                callBroadCast(activity);
            } else {
                Log.e("-->", "file not Deleted :" + imagePath);
            }
        }
    }

    public static void callBroadCast(Activity activity) {
        if (Build.VERSION.SDK_INT >= 14) {
            MediaScannerConnection.scanFile(activity, new String[]{
                    Environment.getExternalStorageDirectory().toString()}, null, new MediaScannerConnection.OnScanCompletedListener() {

                public void onScanCompleted(String path, Uri uri) {
                    Log.e("ExternalStorage", "Scanned " + path + ":");
                    Log.e("ExternalStorage", "-> uri=" + uri);
                }
            });
        } else {
            activity.sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED,
                    Uri.parse("file://" + Environment.getExternalStorageDirectory())));
        }
    }

    @Override
    public void onUploadLicenseListener(int id) {
        Intent intent = new Intent(this, TakePhotoActivity.class);
        intent.putExtra(TakePhotoActivity.KEY_IS_CROP_PHOTO, true);
        intent.putExtra(TakePhotoActivity.PHOTO_ID, id);

        startActivityForResult(intent, REQUEST_CODE_TAKE_PHOTO_ACTIVITY);
    }

    private void uploadPicture(Context context, String imagePath, String url) {
        if (imagePath != null) {
            //Upload image To Server
            Log.d(TAG, "Uploading...");
            Log.d(TAG, "bitmap path: " + imagePath);

            uploadPictureApi(
                    getFileDataFromBitmap(createBitmapFromFile(imagePath)),
                    url);
        }
    }

    private void uploadPictureApi(byte[] fileData, String url){
        mRelProgressBar.setVisibility(View.VISIBLE);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        DataOutputStream dataOutputStream = new DataOutputStream(byteArrayOutputStream);
        try {
            buildPart(dataOutputStream, fileData);
            multipartBody = byteArrayOutputStream.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
        }

        String mimeType = "multipart/form-data";
        MultipartRequest multipartRequest = new MultipartRequest(
                url,
                VolleyUtils.genHeaders3(),
                mimeType,
                multipartBody,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {
                        Toast.makeText(DriverLicenseActivity.this, "Upload successfully! ", Toast.LENGTH_SHORT).show();
                        mRelProgressBar.setVisibility(View.INVISIBLE);
                        /**
                         * After successfully uploading driver's license photos, call api to download them and set to images as a way to refresh.
                         * */
                        loadUserLicensePictures();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mRelProgressBar.setVisibility(View.INVISIBLE);
                        Toast.makeText(DriverLicenseActivity.this, "Upload failed!\r\n" + error.getCause(), Toast.LENGTH_SHORT).show();
                        Toast.makeText(DriverLicenseActivity.this, "Upload failed!\r\n" + error.getStackTrace(), Toast.LENGTH_SHORT).show();
                        Toast.makeText(DriverLicenseActivity.this, "Upload failed!\r\n" + error.getMessage(), Toast.LENGTH_SHORT).show();
                        Toast.makeText(DriverLicenseActivity.this, "Upload failed!\r\n" + error.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
        );

        VolleySingleton.getInstance(DriverLicenseActivity.this).addToRequestQueue(multipartRequest);
    }

    private void buildPart(DataOutputStream dataOutputStream, byte[] fileData) throws IOException {

        ByteArrayInputStream fileInputStream = new ByteArrayInputStream(fileData);
        int bytesAvailable = fileInputStream.available();

        int maxBufferSize = 1024 * 1024;
        int bufferSize = Math.min(bytesAvailable, maxBufferSize);
        byte[] buffer = new byte[bufferSize];

        // read file and write it into form...
        int bytesRead = fileInputStream.read(buffer, 0, bufferSize);

        while (bytesRead > 0) {
            dataOutputStream.write(buffer, 0, bufferSize);
            bytesAvailable = fileInputStream.available();
            bufferSize = Math.min(bytesAvailable, maxBufferSize);
            bytesRead = fileInputStream.read(buffer, 0, bufferSize);
        }
    }

    private byte[] getFileDataFromDrawable(Context context, int id) {
        Drawable drawable = ContextCompat.getDrawable(context, id);
        Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    public static Bitmap createBitmapFromFile(String imagePath){
        File image = new File(imagePath);
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        Bitmap bitmap = BitmapFactory.decodeFile(image.getAbsolutePath(),bmOptions);
        return bitmap;
    }

    public static byte[] getFileDataFromBitmap(Bitmap bitmap){
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    private class IniInfo {
        String licenseNumber;
        String licenseExpMonth;
        String licenseExpYear;
        String zipCode;
        public String name;
        String lastName;
        String ssn;
        String dob;
    }

    Bitmap encodeAsBitmap(String contents, BarcodeFormat format, int img_width, int img_height) throws WriterException {
        String contentsToEncode = contents;
        if (contentsToEncode == null) {
            return null;
        }
        Map<EncodeHintType, Object> hints = null;
        String encoding = guessAppropriateEncoding(contentsToEncode);
        if (encoding != null) {
            hints = new EnumMap<EncodeHintType, Object>(EncodeHintType.class);
            hints.put(EncodeHintType.CHARACTER_SET, encoding);
        }
        MultiFormatWriter writer = new MultiFormatWriter();
        BitMatrix result;
        try {
            result = writer.encode(contentsToEncode, format, img_width, img_height, hints);
        } catch (IllegalArgumentException iae) {
            // Unsupported format
            return null;
        }
        int width = result.getWidth();
        int height = result.getHeight();
        int[] pixels = new int[width * height];
        for (int y = 0; y < height; y++) {
            int offset = y * width;
            for (int x = 0; x < width; x++) {
                pixels[offset + x] = result.get(x, y) ? BLACK : WHITE;
            }
        }

        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
        return bitmap;
    }

    private static String guessAppropriateEncoding(CharSequence contents) {
        // Very crude at the moment
        for (int i = 0; i < contents.length(); i++) {
            if (contents.charAt(i) > 0xFF) {
                return "UTF-8";
            }
        }
        return null;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
//        switch (requestCode) {
//            case PERMISSION_CAMERA: {
//                // If request is cancelled, the result arrays are empty.
//                if (grantResults.length > 0
//                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    // permission was granted, yay! Do the
//                    // contacts-related task you need to do.
//                    showCameraInterface();
//                } else {
//                    // permission denied, boo! Disable the
//                    // functionality that depends on this permission.
//                }
//                return;
//            }
//
//            // other 'case' lines to check for other
//            // permissions this app might request.
//        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        mRequestPermissionHandler.onRequestPermissionsResult(requestCode, permissions,
                grantResults);
    }

}
