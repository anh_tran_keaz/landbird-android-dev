package com.keaz.landbird.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.keaz.landbird.R;

public class MyTripsActivity extends BaseActivity {

    private RecyclerView recycleView;
    private MyTripAdapter myTripAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_trips);
        recycleView = (RecyclerView) findViewById(R.id.recycleView);
        myTripAdapter = new MyTripAdapter(this);
        recycleView.setAdapter(myTripAdapter);
        recycleView.setLayoutManager(new LinearLayoutManager(this));
        recycleView.setHasFixedSize(true);
    }

    public class MyTripAdapter extends RecyclerView.Adapter<MyTripViewHolder> {
        public MyTripAdapter(Context context) {
            this.mContext = context;
        }
        private Context mContext;
        @Override
        public MyTripViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.my_trip_item, parent, false);
            return new MyTripViewHolder(view);
        }

        @Override
        public void onBindViewHolder(MyTripViewHolder holder, int position) {

        }

        @Override
        public int getItemCount() {
            return 3;
        }
    }

    public class MyTripViewHolder extends RecyclerView.ViewHolder {

        public MyTripViewHolder(View itemView) {
            super(itemView);
        }
    }
}
