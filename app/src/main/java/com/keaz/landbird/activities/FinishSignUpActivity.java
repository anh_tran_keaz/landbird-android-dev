package com.keaz.landbird.activities;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.keaz.landbird.R;
import com.keaz.landbird.models.KZAccount;
import com.keaz.landbird.models.KZCreditCard;
import com.keaz.landbird.models.KZPaymentMethod;
import com.keaz.landbird.models.KZPaypal;
import com.keaz.landbird.utils.BKGlobals;
import com.keaz.landbird.utils.ConstantsUtils;
import com.segment.analytics.Analytics;

public class FinishSignUpActivity extends BaseActivity {

    private static final String TAG = FinishSignUpActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finish_sign_up);

        TextView tvCongra = (TextView)findViewById(R.id.tv_congrats);
        String name = KZAccount.getSharedAccount().getUser().name;

        String[] split = name.split(" ");
        String fixedName = "";
        for (int i = 0; i < split.length; i++) {
            fixedName += split[i].substring(0, 1).toUpperCase() + split[i].substring(1) + " ";
        }
        tvCongra.setText("Congratulations "+ fixedName +"!");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Analytics.with(this).screen("Signup Complete");
    }

    public void onHeaderBackPress(View view) {
        onBackPressed();
    }

    public void onFinishSignUp(View view) {
        Analytics.with(this).track("Click Finish_btn");

        KZPaymentMethod method = KZAccount.getSharedAccount().paymentDefault;

        String log = "";

        if (method instanceof KZPaypal) {
            log = ((KZPaypal) method).toJSON();
        } else if (method instanceof KZCreditCard) {
            log = ((KZCreditCard) method).toJSON();
        }

        Log.d(TAG, log);
        BKGlobals.getSharedGlobals().saveBoolPreferences(ConstantsUtils.PREF_KEY_KEEP_SIGN_IN, true);
        setResult(RESULT_OK);
        finish();

    }
}
