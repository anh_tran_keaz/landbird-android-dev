package com.keaz.landbird.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.PopupMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.RelativeLayout;

import com.keaz.landbird.R;
import com.keaz.landbird.adapters.ListReservationAdapter;
import com.keaz.landbird.models.KZAccount;
import com.keaz.landbird.models.KZBooking;
import com.keaz.landbird.models.KZBookingsList;
import com.keaz.landbird.utils.BKNetworkResponseError;
import com.keaz.landbird.utils.DateUtils;
import com.keaz.landbird.utils.MyLog;
import com.keaz.landbird.utils.OnResponseModel;
import com.keaz.landbird.utils.ViewUtil;
import com.keaz.landbird.utils.VolleyUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;

public class ListReservationActivity extends BaseActivity implements View.OnClickListener {

    private static final String TODAY = "Today";
    private static final String TOMORROW = "Tomorrow";
    private static final String TAG = ListReservationActivity.class.getSimpleName();
    private KZBookingsList bookingList;
    private ListReservationAdapter adapter;
    private RelativeLayout mRelEmptyVehicle;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private ExpandableListView expandableListView;

    enum BookingFilter{ APPROVED, COMPLETED, ALL}

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_reservation);

        expandableListView = (ExpandableListView) findViewById(R.id.expandableListView);
        adapter = new ListReservationAdapter(new LinkedHashMap<String, ArrayList<KZBooking>>(), this);
        expandableListView.setAdapter(adapter);

        mRelEmptyVehicle = (RelativeLayout) findViewById(R.id.rel_empty_vehicle);
        mRelEmptyVehicle.setVisibility(View.GONE);

        findViewById(R.id.btnAddLocation).setOnClickListener(this);
        findViewById(R.id.rel_back).setOnClickListener(this);
        findViewById(R.id.icMenu).setVisibility(View.GONE);

        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeToRefresh);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadBookingByUser(BookingFilter.ALL);
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        loadBookingByUser(BookingFilter.ALL);
    }

    @Override
    public void onClick(final View v) {
        switch (v.getId()){
            case R.id.rel_back:
                Intent intent = new Intent(ListReservationActivity.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                ListReservationActivity.this.finish();
                break;
            case R.id.icMenu:
                ViewUtil.showMenu(v, this, R.menu.menu_2, new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()){
                            case R.id.item1_show_all_booking:
                                loadBookingByUser(BookingFilter.ALL);
                                break;
                            case R.id.item2_show_approved_bookings:
                                loadBookingByUser(BookingFilter.APPROVED);
                                break;
                            case R.id.item3_show_complete_bookings:
                                loadBookingByUser(BookingFilter.COMPLETED);
                                break;
                        }
                        return false;
                    }
                });
                break;

            case R.id.btnAddLocation:
                finish();
                break;
        }
    }

    private void loadBookingByUser(final BookingFilter all) {
        showCustomDialogLoading();
        int userId = KZAccount.getSharedAccount().user.id;
        VolleyUtils.getSharedNetwork().loadBookingByUser(this, userId,
                new OnResponseModel<KZBookingsList>() {
                    @Override
                    public void onResponseSuccess(KZBookingsList model) {
                        bookingList = model;
                        adapter.notifyDataChanged(splitBookingIntoGroup(model,all));
                        for (int i = 0; i < adapter.getGroupCount(); i++) {
                            expandableListView.expandGroup(i);
                        }

                        if (bookingList == null || bookingList.bookings == null || bookingList.bookings.size() == 0) {
                            mRelEmptyVehicle.setVisibility(View.VISIBLE);
                        } else {
                            mRelEmptyVehicle.setVisibility(View.GONE);
                        }

                        dismissCustomDialogLoading();
                    }

                    @Override
                    public void onResponseError(BKNetworkResponseError error) {
                        VolleyUtils.handleErrorRespond(ListReservationActivity.this, error);
                        dismissCustomDialogLoading();
                    }
                });
    }

    private LinkedHashMap<String, ArrayList<KZBooking>> splitBookingIntoGroup(KZBookingsList model, BookingFilter filter) {
        LinkedHashMap<String, ArrayList<KZBooking>> result = new LinkedHashMap<>();

        for (KZBooking booking : model.bookings) {
            Date date = DateUtils.getDate(booking.end);
            String sectionName = DateUtils.compareTwoDate(date);
            switch (sectionName) {
                case TODAY:
                    if (!result.containsKey(TODAY)) {
                        ArrayList<KZBooking> bookings = new ArrayList<>();
                        bookings.add(booking);
                        result.put(TODAY, bookings);
                    } else {
                        result.get(TODAY).add(booking);
                    }
                    break;
                case TOMORROW:
                    if (!result.containsKey(TOMORROW)) {
                        ArrayList<KZBooking> bookings = new ArrayList<>();
                        bookings.add(booking);
                        result.put(TOMORROW, bookings);
                    } else {
                        result.get(TOMORROW).add(booking);
                    }
                    break;
                default:
                    if (!result.containsKey(sectionName)) {
                        ArrayList<KZBooking> bookings = new ArrayList<>();
                        bookings.add(booking);
                        result.put(sectionName, bookings);
                    } else {
                        result.get(sectionName).add(booking);
                    }
                    break;
            }
        }
        MyLog.debug(TAG, String.valueOf(result.size()));
        return result;
    }


}
