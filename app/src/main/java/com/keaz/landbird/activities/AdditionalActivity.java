package com.keaz.landbird.activities;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.keaz.landbird.R;
import com.keaz.landbird.view.CustomFontTextView;
import com.segment.analytics.Analytics;

public class AdditionalActivity extends BaseActivity {

    private static final String TAG = AdditionalActivity.class.getSimpleName();

    private ListView listView;

    private AdditionalActivity.SimpleArrayAdapter adapter;

    private String[] dataList = {"Smoking", "Detailing", "Accident"};

    private String[] feeList = {"$50", "$150", "Varies"};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_additional);


        initUI();
        initData();
        initListener();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Analytics.with(this).screen("Agreements");
    }

    public void initUI() {
        listView = (ListView) findViewById(R.id.lv_additional);
    }

    public void initData() {

        adapter = new AdditionalActivity.SimpleArrayAdapter(this, dataList, feeList);

        listView.setAdapter(adapter);


    }

    public void initListener() {
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d(TAG, dataList[position]);
            }
        });
    }

    public void onHeaderBackPress(View view) {
        onBackPressed();
    }

    private class SimpleArrayAdapter extends ArrayAdapter<String> {
        private final Context context;
        private final String[] values;

        private final String[] fees;

        public SimpleArrayAdapter(Context context, String[] values, String[] fee) {
            super(context, -1, values);
            this.context = context;
            this.values = values;
            this.fees = fee;
        }

        @Override
        public int getCount() {
            return values != null ? values.length : 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.item_additional_list, parent, false);
            CustomFontTextView tvTitle = (CustomFontTextView) rowView.findViewById(R.id.tv_additional_title);
            CustomFontTextView tvFee = (CustomFontTextView) rowView.findViewById(R.id.tv_additional_fee);
            tvTitle.setText(values[position]);
            tvFee.setText(fees[position]);

            return rowView;
        }
    }
}
