package com.keaz.landbird.activities;

import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.keaz.landbird.R;
import com.keaz.landbird.models.KZBenefit;
import com.keaz.landbird.utils.BKNetworkResponseError;
import com.keaz.landbird.utils.OnResponseModel;
import com.keaz.landbird.utils.VolleyUtils;
import com.keaz.landbird.view.CustomFontTextView;
import com.segment.analytics.Analytics;

import java.util.ArrayList;

public class BenefitListActivity extends BaseActivity {

    private RecyclerView mRecyclerView;
    private BenefitAdapter mAdapter;

    private ArrayList<KZBenefit> mBenefitList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_benfit_list);

        ((CustomFontTextView)findViewById(R.id.txt_title)).setText(getString(R.string.title_benefit_list));

        if (mBenefitList == null) {
            mBenefitList = new ArrayList<>();
        }
        mAdapter = new BenefitAdapter(mBenefitList);

        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mAdapter);

        loadBenefitList();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Analytics.with(this).screen("Benefit List");
    }

    private void loadBenefitList() {
        showCustomDialogLoading();
        VolleyUtils.getSharedNetwork().loadBenefitList(this, new OnResponseModel<JsonArray>() {
            @Override
            public void onResponseSuccess(JsonArray array) {
                dismissCustomDialogLoading();

                for (int i = 0; i < array.size(); i++) {
                    Gson gson = new Gson();
                    KZBenefit benefit = gson.fromJson(array.get(i), KZBenefit.class);
                    mBenefitList.add(benefit);
                }
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                dismissCustomDialogLoading();
                VolleyUtils.handleErrorRespond(BenefitListActivity.this, error);
            }
        });
    }

    private class BenefitAdapter extends RecyclerView.Adapter<BenefitAdapter.MyViewHolder> {

        private ArrayList<KZBenefit> benefitList;

        public class MyViewHolder extends RecyclerView.ViewHolder {

            public ImageView imgBenefit;
            public CustomFontTextView txtTitle;
            public CustomFontTextView txtRedeem;

            public MyViewHolder(View view) {
                super(view);
                imgBenefit = (ImageView) view.findViewById(R.id.img_benefit);
                txtTitle = (CustomFontTextView) view.findViewById(R.id.tv_title);
                txtRedeem = (CustomFontTextView) view.findViewById(R.id.tv_redeem);
            }
        }

        public BenefitAdapter(ArrayList<KZBenefit> benefitList) {
            this.benefitList = benefitList;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_benefit_list, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
            KZBenefit benefit = benefitList.get(position);

            holder.txtTitle.setText(benefit.name);
        }

        @Override
        public int getItemCount() {
            return benefitList.size();
        }
    }
}
