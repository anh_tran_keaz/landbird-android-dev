package com.keaz.landbird.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.keaz.landbird.R;
import com.keaz.landbird.view.CustomEditText;

public class ChangePasswordActivity extends BaseActivity {

    private CustomEditText etPassword;
    private CustomEditText etConfirmPassword;
    private CustomEditText etCurrentPassword;

    private ImageView imgCurrentPassword;
    private ImageView imgPassword;
    private ImageView imgPasswordConfirm;

    private boolean isShowCurrentPassword = false;
    private boolean isShowPassword = false;
    private boolean isShowPasswordConfirm = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        initUI();
        initData();
        initListener();
    }


    public void initUI() {
        etCurrentPassword = (CustomEditText) findViewById(R.id.et_current_password);
        imgCurrentPassword = (ImageView) findViewById(R.id.img_current_password);
        etPassword = (CustomEditText) findViewById(R.id.et_password);
        etConfirmPassword = (CustomEditText) findViewById(R.id.et_confirm_password);
        imgPassword = (ImageView) findViewById(R.id.img_password);
        imgPasswordConfirm = (ImageView) findViewById(R.id.img_password_confirm);

        showPassword(isShowCurrentPassword, etCurrentPassword, imgCurrentPassword);
        showPassword(isShowPassword, etPassword, imgPassword);
        showPassword(isShowPasswordConfirm, etConfirmPassword, imgPasswordConfirm);
    }


    public void initData() {

    }

    public void initListener() {
        hideKeyboardWhenTouchOutside();
    }

    public void showPassword(View view) {
        int id = view.getId();
        if (id == imgCurrentPassword.getId()) {
            isShowCurrentPassword = !isShowCurrentPassword;
            showPassword(isShowCurrentPassword, etCurrentPassword, imgCurrentPassword);
        } else if (id == imgPassword.getId()) {
            isShowPassword = !isShowPassword;
            showPassword(isShowPassword, etPassword, imgPassword);

        } else if (id == imgPasswordConfirm.getId()) {
            isShowPasswordConfirm = !isShowPasswordConfirm;
            showPassword(isShowPasswordConfirm, etConfirmPassword, imgPasswordConfirm);
        }

    }

    public void showPassword(boolean isShowPassword, CustomEditText editText, ImageView imgView) {
        editText.setTransformationMethod(isShowPassword ? null : new AsteriskPasswordTransformationMethod());
        imgView.setBackgroundResource(isShowPassword ? R.drawable.new_ic_hide_pass : R.drawable.new_ic_show_pass);
    }

    public void checkPasswordMatchOrNot() {
        String password = etPassword.getText().toString();
        String passwordConfirm = etConfirmPassword.getText().toString();


        if (password.equalsIgnoreCase(passwordConfirm)) {
            Toast.makeText(this, "Sign up validated", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(this, AccountActivity.class));
        } else {
            Toast.makeText(this, "Password and password confirm is not matched", Toast.LENGTH_SHORT).show();
        }
    }

    public void hideKeyboardWhenTouchOutside() {
        etPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    hideKeyboard(v);
                }
            }
        });
        etConfirmPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    hideKeyboard(v);
                }
            }
        });
        etCurrentPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    hideKeyboard(v);
                }
            }
        });
    }


    public void onSubmitClick(View view) {
        checkPasswordMatchOrNot();
    }


    public void onHeaderBackPress(View view) {
        onBackPressed();
    }

}
