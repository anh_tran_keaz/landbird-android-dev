package com.keaz.landbird.activities;

import android.app.Application;
import android.content.Context;

import com.keaz.landbird.BuildConfig;
import com.keaz.landbird.utils.BKGlobals;
import com.keaz.landbird.utils.CustomVolleyRequestQueue;
import com.keaz.landbird.utils.MixpanelIntegration;
import com.keaz.landbird.utils.Util;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.segment.analytics.Analytics;

/**
 * Created by Administrator on 16/2/2017.
 */

public class EnvoyApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        CustomVolleyRequestQueue.init(getApplicationContext());
        BKGlobals.createSharedGlobals(getApplicationContext());
        initImageLoader(this);
        Util.getScreenSize(this);

        // Create an analytics client with the given context and Segment write key.
        Analytics analytics = new Analytics.Builder(this, BuildConfig.SEGMENT_API_KEY)
                .logLevel(Analytics.LogLevel.VERBOSE)
                .use(MixpanelIntegration.FACTORY)
                .build();

        // Set the initialized instance as a globally accessible instance.
        Analytics.setSingletonInstance(analytics);
    }


    public void initImageLoader(Context context) {
        ImageLoaderConfiguration.Builder config = new ImageLoaderConfiguration.Builder(context);
        config.threadPoolSize(3);
        config.threadPriority(Thread.NORM_PRIORITY - 1);
        config.memoryCache(new WeakMemoryCache());
        config.denyCacheImageMultipleSizesInMemory();
        config.discCacheExtraOptions(480, 320, null);
        config.diskCacheFileNameGenerator(new Md5FileNameGenerator());
        config.tasksProcessingOrder(QueueProcessingType.LIFO);
        config.writeDebugLogs(); // Remove for release app
        DisplayImageOptions.Builder builder = new DisplayImageOptions.Builder()
                .cacheOnDisk(true)
                .cacheInMemory(true);

        config.defaultDisplayImageOptions(builder.build());
        // Initialize ImageLoader with configuration.
        ImageLoader.getInstance().init(config.build());
    }
}
