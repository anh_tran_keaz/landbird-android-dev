package com.keaz.landbird.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.PopupMenu;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;
import com.keaz.landbird.R;
import com.keaz.landbird.interfaces.CustomListener2;
import com.keaz.landbird.models.KZAccount;
import com.keaz.landbird.models.KZConfigData;
import com.keaz.landbird.models.KZLoginResponse;
import com.keaz.landbird.models.KZObjectForMixpanel;
import com.keaz.landbird.models.KZUser;
import com.keaz.landbird.models.KZUser2;
import com.keaz.landbird.models.UserModel;
import com.keaz.landbird.utils.BKGlobals;
import com.keaz.landbird.utils.BKNetworkResponseError;
import com.keaz.landbird.utils.CommonUtils;
import com.keaz.landbird.utils.ConstantsUtils;
import com.keaz.landbird.utils.DialogUtils;
import com.keaz.landbird.utils.IntentUtil;
import com.keaz.landbird.utils.JsonUtils;
import com.keaz.landbird.utils.LoadingViewUtils;
import com.keaz.landbird.utils.ManagePasswordUtil;
import com.keaz.landbird.utils.OnResponseModel;
import com.keaz.landbird.utils.ValidateUtils;
import com.keaz.landbird.utils.VolleyUtils;
import com.segment.analytics.Analytics;
import com.segment.analytics.Properties;
import com.segment.analytics.Traits;

import java.util.ArrayList;
import java.util.Arrays;

public class LoginActivity extends BaseActivity implements View.OnClickListener {

    private static final String TAG = LoginActivity.class.getSimpleName();
    private EditText edtEmailMobile;
    private EditText edtPass;
    private TextView tvEmail;
    private TextView tvMobile;
    private LinearLayout llCountryCode;
    private TextView tvCountryCodeValue;
    private ImageView mImvEyePassword;
    private CheckBox mCbxSignIn;
    private ArrayList<CountryCodeModels> mCountryCodeModels;
    private RegisterCase registerCase = RegisterCase.EMAIL;
    private String contactNumber;
    private String countryCode = ConstantsUtils.DEFAULT_COUNTRY_CODE;
    private boolean isLoginByEmail;
    private Object mModelSelected;
    private String mEmailOrPhoneSelected;
    private String mPostCodeSelected;
    private String mPassSelected;
    private GoogleSignInClient mGoogleSignInClient;
    private CallbackManager callbackManager;

    enum RegisterCase {EMAIL, MOBILE}

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        loadLanding1Data(null);

        isLoginByEmail = true;

        tvEmail = (TextView) findViewById(R.id.textView_email);
        tvMobile = (TextView) findViewById(R.id.textView_mobile);
        tvEmail.setTextColor(getResources().getColor(R.color.tvColorGreen));
        tvMobile.setOnClickListener(this);
        tvEmail.setOnClickListener(this);

        llCountryCode = (LinearLayout) findViewById(R.id.ll_countryCode2);
        llCountryCode.setVisibility(View.GONE);
        llCountryCode.setOnClickListener(this);
        tvCountryCodeValue = (TextView) findViewById(R.id.tvCountryCodeValue);

        LinearLayout btnShowHidePassword = (LinearLayout) findViewById(R.id.btnShowHidePassword);
        btnShowHidePassword.setOnClickListener(this);

        edtEmailMobile = (EditText) findViewById(R.id.edtEmail);
        edtPass = (EditText) findViewById(R.id.edtPass);

        mImvEyePassword = (ImageView) findViewById(R.id.imv_eye_password);

        mCbxSignIn = (CheckBox) findViewById(R.id.cbx_logged);
        mCbxSignIn.setChecked(true);

        /*
         * Get data from share preference and set to view
         * */
        iniValue();

        findViewById(R.id.signIn).setOnClickListener(this);

        findViewById(R.id.signUpBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                moveToSignUpActivity(false);
            }
        });
        findViewById(R.id.tvForgotPassword).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this, ActivityEnterEmailToResetPassword.class));
            }
        });


        setUpGoogleLogIn();

        setUpFaceBookLogIn();
    }
    @Override
    protected void onStart() {
        super.onStart();
        Analytics.with(this).screen("Signin", new Properties().putValue("Mode", "Email"));
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case ConstantsUtils.REQUEST_CODE_RC_SIGN_IN:
                // The Task returned from this call is always completed, no need to attach
                // a listener.
                Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
                handleSignInResult(task);

                break;
            case ConstantsUtils.REQUEST_CODE_ACCEPT:
                if (resultCode == RESULT_OK) {
                    if (isLoginByEmail) {
                        handleLoginSuccessRespond(mModelSelected, mEmailOrPhoneSelected, "", mPostCodeSelected, mPassSelected);
                    } else {
                        handleLoginSuccessRespond(mModelSelected, "", mEmailOrPhoneSelected, countryCode, mPassSelected);
                    }
                } else {
                    LoadingViewUtils.showOrHideProgressBar(false, this);
                }
                break;
        }
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnShowHidePassword:
                showOrHidePassword();
                break;

            case R.id.ll_countryCode2:
                showPopUpMenu();
                break;

            case R.id.textView_mobile:
                Analytics.with(LoginActivity.this).track("Change Model to Phone");
                changeLoginRegisterMethod(RegisterCase.MOBILE);
                break;

            case R.id.textView_email:
                Analytics.with(LoginActivity.this).track("Change Model to Email");
                changeLoginRegisterMethod(RegisterCase.EMAIL);
                break;

            case R.id.signIn:
                Analytics.with(LoginActivity.this).track("Click Signin_btn");
                String emailOrPhone = edtEmailMobile.getText().toString().trim();
                handleLogin(registerCase, emailOrPhone, countryCode, edtPass.getText().toString().trim());
                break;
        }
    }

    private void loadLanding1Data(final CustomListener2 customListener2) {
        VolleyUtils.getSharedNetwork().loadConfigData(LoginActivity.this,
                new OnResponseModel<KZConfigData>() {
            @Override
            public void onResponseSuccess(KZConfigData model) {
                KZAccount.getSharedAccount().setPasswordMinLength(model.password_min_length);
                KZAccount.getSharedAccount().setPasswordMaxLength(model.password_max_length);
                KZAccount.getSharedAccount().setKzLandingData1(model);
                if(customListener2!=null) customListener2.success();
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                LoadingViewUtils.showOrHideProgressBar(false, LoginActivity.this);
                VolleyUtils.handleErrorRespond(LoginActivity.this, error);
                if(customListener2!=null) customListener2.fail(error);
            }
        });
    }

    private void iniValue() {
        String email = BKGlobals.getSharedGlobals().getStringPreferences(ConstantsUtils.PREF_KEY_EMAIL, null);
        contactNumber = BKGlobals.getSharedGlobals().getStringPreferences(ConstantsUtils.PREF_KEY_CONTACT_NUMBER, null);
        countryCode = BKGlobals.getSharedGlobals().getStringPreferences(ConstantsUtils.PREF_KEY_COUNTRY_CODE, null);

        if (email != null && contactNumber == null) {
            edtEmailMobile.setText(email);
            changeLoginRegisterMethod(RegisterCase.EMAIL);
        }else if(email ==null && contactNumber!=null){
            changeLoginRegisterMethod(RegisterCase.MOBILE);
            edtEmailMobile.setText(contactNumber);
            tvCountryCodeValue.setText(getCountryCodeText(countryCode));
        }else if(email !=null & contactNumber!=null){
            edtEmailMobile.setText(email);
        }
    }

    private void setUpGoogleLogIn() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        findViewById(R.id.btnGoogle).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                actionSignInGoogle();
            }
        });
    }

    private void setUpFaceBookLogIn() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        boolean isLoggedIn = accessToken != null && !accessToken.isExpired();
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        // App code
                    }

                    @Override
                    public void onCancel() {
                        // App code
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                    }
                });

        final LoginButton loginButton = (LoginButton) findViewById(R.id.fb_login_button);
        //loginButton.setReadPermissions(Arrays.asList(EMAIL));
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
            }

            @Override
            public void onCancel() {
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
            }

        });
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LoginManager.getInstance().logInWithReadPermissions(LoginActivity.this, Arrays.asList("public_profile"));
            }
        });
        findViewById(R.id.btnFacebook).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginButton.performClick();
            }
        });
    }

    private String getCountryCodeText(String countryCode) {
        switch (Integer.parseInt(countryCode)){
            case 1:
                return "US +1";
            case 61:
                return "AU +61";
            case 64:
                return "NZ +64";
            case 84:
                return "VN +84";
            default:
                return "";
        }
    }

    private void handleLogin(final RegisterCase registerCase, final String emailOrPhone, final String postCode, final String pass) {

        if(postCode == null) this.countryCode = ConstantsUtils.DEFAULT_COUNTRY_CODE;

        boolean validated = validateLogInInput();
        if(!validated) return;

        LoadingViewUtils.showOrHideProgressBar(true, this);
        switch (registerCase) {
            case EMAIL:
                VolleyUtils.getSharedNetwork().loginUserByEmail(LoginActivity.this, emailOrPhone, pass, new OnResponseModel() {
                    @Override
                    public void onResponseSuccess(Object model) {
                        isLoginByEmail = true;
                        mModelSelected = model;
                        mEmailOrPhoneSelected = emailOrPhone;
                        mPostCodeSelected = postCode;
                        mPassSelected = pass;

                        if (model instanceof KZLoginResponse) {
                            KZLoginResponse response = (KZLoginResponse) model;
                            String accessToken = response.accessToken;
                            BKGlobals.getSharedGlobals().saveStringPreferences(BKGlobals.ACCESS_TOKEN, accessToken);

                            loadUserForMixPanel((KZLoginResponse) model, response.id, true, emailOrPhone, postCode, pass);
                        }
                    }

                    @Override
                    public void onResponseError(BKNetworkResponseError networkResponse) {
                        handleLoginRespondError(networkResponse);
                    }
                });
                break;

            case MOBILE:
                VolleyUtils.getSharedNetwork().loginUserByPhone(
                        LoginActivity.this,
                        emailOrPhone, countryCode, pass, new OnResponseModel() {
                    @Override
                    public void onResponseSuccess(Object model) {
                        isLoginByEmail = false;
                        mModelSelected = model;
                        mEmailOrPhoneSelected = emailOrPhone;
                        mPassSelected = pass;
                        if (model instanceof KZLoginResponse) {
                            KZLoginResponse response = (KZLoginResponse) model;
                            String accessToken = response.accessToken;
                            BKGlobals.getSharedGlobals().saveStringPreferences(BKGlobals.ACCESS_TOKEN, accessToken);

                            loadUserForMixPanel((KZLoginResponse)model, response.id, false, emailOrPhone, postCode, pass);
                        }
                    }

                    @Override
                    public void onResponseError(BKNetworkResponseError networkResponse) {
                        handleLoginRespondError(networkResponse);
                    }
                });
                break;
        }
    }

    private void loadUserForMixPanel(final KZLoginResponse modelLogin, final String userId, final boolean isLoginByEmail, final String emailOrPhone, final String postCode, final String pass) {
        VolleyUtils.getSharedNetwork().loadUserForMixpanel(this, userId, new OnResponseModel() {
            @Override
            public void onResponseSuccess(Object model) {
                Gson gson = new Gson();
                KZObjectForMixpanel kzObjectForMixpanel = gson.fromJson(model.toString(), KZObjectForMixpanel.class);

                UserModel userModel = new UserModel();
                userModel.id = Integer.parseInt(kzObjectForMixpanel.userId);
                userModel.name = kzObjectForMixpanel.firstName;
                userModel.last_name = kzObjectForMixpanel.lastName;
                userModel.email = modelLogin.email;
                userModel.phone = modelLogin.phone;

                CommonUtils.segmentIdentifyWithExpandUser(LoginActivity.this, userModel, kzObjectForMixpanel, 0.0f);
                Analytics.with(LoginActivity.this).track("Login user successfully");

                if (isLoginByEmail) {
                    handleLoginSuccessRespond(modelLogin, emailOrPhone, "", postCode, pass);
                } else {
                    handleLoginSuccessRespond(modelLogin, "", emailOrPhone, countryCode, pass);
                }
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                VolleyUtils.handleErrorRespond(LoginActivity.this, error);
            }
        });
    }

    private boolean validateLogInInput() {
        if(edtEmailMobile.getText().toString().isEmpty()){
            if(registerCase==RegisterCase.EMAIL) {
                Toast.makeText(this, getString(R.string.error_empty_email), Toast.LENGTH_SHORT).show();
            }else {
                Toast.makeText(this,getString(R.string.error_empty_phone), Toast.LENGTH_SHORT).show();
            }
            return false;
        }
        if(registerCase == RegisterCase.EMAIL){
            if(!ValidateUtils.validateEmail(this,edtEmailMobile.getText().toString())) {
                Toast.makeText(this,getString(R.string.error_format_email), Toast.LENGTH_SHORT).show();
                return false;
            }
        } else if(registerCase==RegisterCase.MOBILE) {
            if(!ValidateUtils.validatePhone(edtEmailMobile.getText().toString())){
                Toast.makeText(this,getString(R.string.error_format_phone), Toast.LENGTH_SHORT).show();
                return false;
            }
        }
        if(edtPass.getText().toString().isEmpty()){
            Toast.makeText(this,getString(R.string.error_empty_password), Toast.LENGTH_SHORT).show();
            return false;
        }
        /*if(!ValidateUtils.validatePassword(edtPass.getText().toString(), getApplicationContext())) {
            //Toast.makeText(this,getString(R.string.error_format_password_limit_character), Toast.LENGTH_SHORT).show();
            return false;
        }*/
        return true;
    }

    private void showSegmentError(String message) {
        if (registerCase == RegisterCase.EMAIL) {
            Analytics.with(LoginActivity.this).track(message,
                    new Properties()
                            .putValue("email",edtEmailMobile.getText().toString().trim()));
        } else {
            Analytics.with(LoginActivity.this).track(message,
                    new Properties()
                            .putValue("phone",edtEmailMobile.getText().toString().trim()));
        }
    }

    private void handleLoginRespondError(BKNetworkResponseError networkResponse) {
        LoadingViewUtils.showOrHideProgressBar(false, this);
        if(networkResponse==null){
            VolleyUtils.handleErrorRespond(this, networkResponse);
            return;
        }
        String tittle = networkResponse.code;

        if (networkResponse.status != null && !networkResponse.status.equals("")) {
            if (networkResponse.status.contains("401")) {
                if(tittle == null || tittle.equalsIgnoreCase("user_account")) {
                    showSegmentError(getString(R.string.error_account_not_found));

                    final AlertDialog.Builder builder ;
                    builder = new AlertDialog.Builder(this);
                    builder.setTitle("");
                    builder.setMessage(getString(R.string.error_account_not_found));
                    builder.setCancelable(true);

                    builder.setPositiveButton(getString(R.string.btn_no), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    builder.setNegativeButton(getString(R.string.text_create_account_now), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                            moveToSignUpActivity(true);
                        }
                    });
                    builder.create().show();

                }else if(tittle.equalsIgnoreCase("user_pending")) {
                    actionHandleLogInResponseWithPendingUser(networkResponse);

                } else if(tittle.equalsIgnoreCase("user_exists")){
                    actionHandleLoginResponseWithExistedUserButNotInCompany(networkResponse);

                } else if(tittle.equalsIgnoreCase("user_password")){
                    showSegmentError(getString(R.string.error_wrong_password));
                    int userID = 0;
                    KZUser kzUser = JsonUtils.parseJsonObject(KZUser.class, networkResponse.data.toString());
                    UserModel user = new UserModel();
                    user.email = kzUser.email;
                    user.id = kzUser.id;
                    user.phone = kzUser.phone;
                    user.name = kzUser.name;
                    saveUser(user);

                    openWrongPasswordMessageDialog(userID);

                } else {
                    VolleyUtils.handleErrorRespond(LoginActivity.this, networkResponse);
                }
            } else {
                VolleyUtils.handleErrorRespond(LoginActivity.this, networkResponse);
            }
        } else {
            VolleyUtils.handleErrorRespond(LoginActivity.this, networkResponse);
        }

    }

    private void actionHandleLogInResponseWithPendingUser(BKNetworkResponseError networkResponse) {
        showSegmentError(getString(R.string.text_account_not_activated));

        KZUser user = JsonUtils.parseJsonObject(KZUser.class, networkResponse.data.toString());
        final KZUser userObj = user;

        final AlertDialog.Builder builder ;
        builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.text_account_not_activated));
        builder.setMessage("We will resend you the code to activate your account.");
        builder.setCancelable(true);
        builder.setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();

            }
        });
        builder.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                int userID = userObj.id;
                String email = userObj.email;
                String contact_number = userObj.phone;
                String country_code = userObj.country_code;
                String password = userObj.password;
                String name = userObj.name;

                UserModel user = new UserModel();
                user.email = email;
                user.id = userID;
                user.phone = contact_number;
                user.name = name;
                saveUser(user);

                moveToVerifyActivity(userID, email, contact_number, country_code, password, ConstantsUtils.VERIFY_PHONE_CASE_ACTIVATE_ACCOUNT);
            }
        });
        builder.create().show();
    }

    private void actionHandleLoginResponseWithExistedUserButNotInCompany(BKNetworkResponseError networkResponse) {
        showSegmentError(getString(R.string.error_account_not_exist));

        KZUser2 kzUser = JsonUtils.parseJsonObject(KZUser2.class, networkResponse.data.toString());
        final KZUser2 userObj = kzUser;

        DialogUtils.showMessageDialog(this, true,
                getString(R.string.error_account_not_exist),
                getString(R.string.confirm_verify_account),
                "Cancel", "Ok",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                },
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        int userID = userObj.id;
                        String email = userObj.email;
                        String contact_number = userObj.phone;
                        String country_code = userObj.country_code;
                        String password = userObj.password;

                        UserModel user = new UserModel();
                        user.email = email;
                        user.id = userID;
                        user.phone = contact_number;
                        user.name = userObj.name;
                        saveUser(user);

                        moveToVerifyActivity(userID, email, contact_number, country_code, password, ConstantsUtils.VERIFY_PHONE_CASE_INVITE_USER);
                    }
                });
    }

    private void moveToVerifyActivity(int userID, String email, String contact_number, String country_code, String password, String purpose) {
        Intent intent;
        switch (registerCase) {
            case EMAIL:
                intent = new Intent(this, VerifyPhoneActivity.class);
                intent.putExtra(ConstantsUtils.INTENT_EXTRA_ACTIVATE_ACCOUNT_OR_RESET_PASSWORD, purpose);
                intent.putExtra(ConstantsUtils.INTENT_EXTRA_CALL_API_RESEND_CODE,true);
                intent.putExtra(ConstantsUtils.INTENT_EXTRA_EMAIL, email);
                intent.putExtra(ConstantsUtils.INTENT_EXTRA_PHONE, edtEmailMobile.getText().toString());
                intent.putExtra(ConstantsUtils.INTENT_EXTRA_VERIFY_EMAIL_OR_PHONE, 1);
                intent.putExtra(ConstantsUtils.INTENT_EXTRA_ACCESS_USER_ID, userID+"");
                intent.putExtra(ConstantsUtils.INTENT_EXTRA_PHONE, contact_number);
                intent.putExtra(ConstantsUtils.INTENT_EXTRA_COUNTRY_CODE, country_code);
                intent.putExtra(ConstantsUtils.INTENT_EXTRA_PASSWORD, password);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
                break;
            case MOBILE:
                intent = new Intent(this, VerifyPhoneActivity.class);
                intent.putExtra(ConstantsUtils.INTENT_EXTRA_ACTIVATE_ACCOUNT_OR_RESET_PASSWORD, purpose);
                intent.putExtra(ConstantsUtils.INTENT_EXTRA_CALL_API_RESEND_CODE,true);
                intent.putExtra(ConstantsUtils.INTENT_EXTRA_PHONE, edtEmailMobile.getText().toString());
                intent.putExtra(ConstantsUtils.INTENT_EXTRA_VERIFY_EMAIL_OR_PHONE, 2);
                intent.putExtra(ConstantsUtils.INTENT_EXTRA_ACCESS_USER_ID, userID+"");
                intent.putExtra(ConstantsUtils.INTENT_EXTRA_PHONE, contact_number);
                intent.putExtra(ConstantsUtils.INTENT_EXTRA_COUNTRY_CODE, country_code);
                intent.putExtra(ConstantsUtils.INTENT_EXTRA_PASSWORD, password);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
                break;
        }
    }

    private void moveToSignUpActivity(boolean askAndSendRegisterInfo) {
        if(countryCode == null) this.countryCode = ConstantsUtils.DEFAULT_COUNTRY_CODE;
        if(askAndSendRegisterInfo){
            if(!validateLogInInput()) return;
        }

        Intent intent = new Intent(LoginActivity.this, SignUpActivity.class);
        if(askAndSendRegisterInfo){
            intent.putExtra(ConstantsUtils.INTENT_EXTRA_PASSWORD, edtPass.getText().toString());
            intent.putExtra(ConstantsUtils.INTENT_EXTRA_COUNTRY_CODE, countryCode!=null?countryCode:ConstantsUtils.DEFAULT_COUNTRY_CODE);

            switch (registerCase) {
                case EMAIL:
                    intent.putExtra(ConstantsUtils.INTENT_EXTRA_EMAIL, edtEmailMobile.getText().toString());
                    intent.putExtra(ConstantsUtils.INTENT_EXTRA_VERIFY_EMAIL_OR_PHONE, 1);
                    break;

                case MOBILE:
                    intent.putExtra(ConstantsUtils.INTENT_EXTRA_PHONE, edtEmailMobile.getText().toString());
                    intent.putExtra(ConstantsUtils.INTENT_EXTRA_VERIFY_EMAIL_OR_PHONE, 2);
                    break;
            }
        }
        startActivity(intent);
    }

    private void handleLoginSuccessRespond(Object model, String email, String phone, String countryCode, String pass) {
        if (model instanceof KZLoginResponse) {
            KZLoginResponse response = (KZLoginResponse) model;
            String accessToken = response.accessToken;

            if (email != null && !email.equals("")) {
                Analytics.with(this).identify(null, new Traits().putValue("email",email), null);
            } else {
                Analytics.with(this).identify(null, new Traits().putValue("phone",contactNumber), null);
            }

            BKGlobals.getSharedGlobals().saveStringPreferences(BKGlobals.ACCESS_TOKEN, accessToken);

            BKGlobals.getSharedGlobals().saveStringPreferences(ConstantsUtils.PREF_KEY_EMAIL, email);
            BKGlobals.getSharedGlobals().saveStringPreferences(ConstantsUtils.PREF_KEY_CONTACT_NUMBER, contactNumber);
            BKGlobals.getSharedGlobals().saveStringPreferences(ConstantsUtils.PREF_KEY_COUNTRY_CODE, countryCode);
            BKGlobals.getSharedGlobals().saveStringPreferences(ConstantsUtils.PREF_KEY_PASSWORD, pass);
            BKGlobals.getSharedGlobals().saveBoolPreferences(ConstantsUtils.PREF_KEY_KEEP_SIGN_IN, mCbxSignIn.isChecked());

            Intent intent = new Intent(this, ActivityWelcome.class);
            intent = IntentUtil.addFlag1(intent);
            startActivity(intent);

        } else {
            LoadingViewUtils.showOrHideProgressBar(false, this);
        }
    }

    private void changeLoginRegisterMethod(RegisterCase mobile) {
        switch (mobile) {
            case MOBILE:
                registerCase = RegisterCase.MOBILE;
                edtEmailMobile.setText("");
                edtEmailMobile.setHint("");
                tvEmail.setTextColor(getResources().getColor(R.color.tvColorGray));
                tvMobile.setTextColor(getResources().getColor(R.color.tvColorGreen));
                llCountryCode.setVisibility(View.VISIBLE);
                edtEmailMobile.setInputType(InputType.TYPE_CLASS_PHONE);
                break;

            case EMAIL:
                registerCase = RegisterCase.EMAIL;
                edtEmailMobile.setText("");
                edtEmailMobile.setHint("");
                llCountryCode.setVisibility(View.GONE);
                tvMobile.setTextColor(getResources().getColor(R.color.tvColorGray));
                tvEmail.setTextColor(getResources().getColor(R.color.tvColorGreen));
                edtEmailMobile.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
                break;
        }
    }

    private void showOrHidePassword() {
        ManagePasswordUtil.changePasswordAndTextType(edtPass, mImvEyePassword);
    }

    private void showPopUpMenu() {
        PopupMenu popup = new PopupMenu(this, llCountryCode);
        //Inflating the Popup using xml file
        popup.getMenuInflater().inflate(R.menu.postcode_menu, popup.getMenu());

        //Hard code for example
        mCountryCodeModels = new ArrayList<>();
        //mCountryCodeModels.add(new CountryCodeModels("US +1", "1"));
        //mCountryCodeModels.add(new CountryCodeModels("NZ +64", "64"));
        mCountryCodeModels.add(new CountryCodeModels("AU +61", "61"));
        //mCountryCodeModels.add(new CountryCodeModels("VN +84", "84"));


        //Programmatically add item
        Menu menu = popup.getMenu();
        for (int i = 0; i < mCountryCodeModels.size(); i++) {
            menu.add(0, i, i, mCountryCodeModels.get(i).getValue());
        }

        //registering popup with OnMenuItemClickListener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                tvCountryCodeValue.setText(item.getTitle().toString());
                countryCode = mCountryCodeModels.get(item.getItemId()).getNumber();
                return true;
            }
        });

        popup.show();
    }

    private void openWrongPasswordMessageDialog(final int userID) {
        /*AlertDialog.Builder builder ;
        builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.error_wrong_password));
        builder.setMessage(getString(R.string.confirm_forget_password));
        builder.setCancelable(true);
        builder.setPositiveButton("Try again", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();

            }
        });
        builder.setNegativeButton("Reset", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();

            }
        });
        builder.create().show();*/
        DialogUtils.showCustomDialog(this, true, getString(R.string.error_wrong_password), getString(R.string.confirm_forget_password), "Try again", "Reset",
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                    }
                },
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        moveToResetPasswordActivity(userID);
                    }
                });
    }

    private void moveToResetPasswordActivity(int userID) {
        Intent intent;
        switch (registerCase) {
            case EMAIL:
                intent = new Intent(LoginActivity.this, VerifyPhoneActivity.class);
                intent.putExtra(ConstantsUtils.INTENT_EXTRA_ACTIVATE_ACCOUNT_OR_RESET_PASSWORD,ConstantsUtils.VERIFY_PHONE_CASE_RESET_PASSWORD);
                intent.putExtra(ConstantsUtils.INTENT_EXTRA_VERIFY_EMAIL_OR_PHONE, ConstantsUtils.VERIFY_EMAIL);
                intent.putExtra(ConstantsUtils.INTENT_EXTRA_EMAIL, edtEmailMobile.getText().toString());
                intent.putExtra(ConstantsUtils.INTENT_EXTRA_ACCESS_USER_ID, userID);
                startActivity(intent);
                break;

            case MOBILE:
                intent = new Intent(LoginActivity.this, VerifyPhoneActivity.class);
                intent.putExtra(ConstantsUtils.INTENT_EXTRA_ACTIVATE_ACCOUNT_OR_RESET_PASSWORD, ConstantsUtils.VERIFY_PHONE_CASE_RESET_PASSWORD);
                intent.putExtra(ConstantsUtils.INTENT_EXTRA_VERIFY_EMAIL_OR_PHONE, ConstantsUtils.VERIFY_PHONE);
                intent.putExtra(ConstantsUtils.INTENT_EXTRA_ACCESS_USER_ID, userID+"");
                intent.putExtra(ConstantsUtils.INTENT_EXTRA_COUNTRY_CODE, countryCode!=null?countryCode:ConstantsUtils.DEFAULT_COUNTRY_CODE);
                intent.putExtra(ConstantsUtils.INTENT_EXTRA_PHONE, edtEmailMobile.getText().toString());
                startActivity(intent);
                break;
        }

    }

    private void actionSignInGoogle() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, ConstantsUtils.REQUEST_CODE_RC_SIGN_IN);
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            // Signed in successfully, show authenticated UI.
            Toast.makeText(this, getString(R.string.text_signin_google_success),Toast.LENGTH_LONG).show();
            //updateUI(account);
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
            //updateUI(null);
        }
    }

}
