package com.keaz.landbird.activities;


import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.PopupMenu;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.keaz.landbird.R;
import com.keaz.landbird.models.UserModel;
import com.keaz.landbird.utils.BKGlobals;
import com.keaz.landbird.utils.BKNetworkResponseError;
import com.keaz.landbird.utils.ConfigUtil;
import com.keaz.landbird.utils.ConstantsUtils;
import com.keaz.landbird.utils.OnResponseModel;
import com.keaz.landbird.utils.ToastUtil;
import com.keaz.landbird.utils.ValidateUtils;
import com.keaz.landbird.utils.VolleyUtils;
import com.keaz.landbird.view.CustomEditText;
import com.segment.analytics.Analytics;

import java.util.ArrayList;

public class SignUpActivity extends BaseActivity implements View.OnClickListener {

    private static final String TAG = SignUpActivity.class.getSimpleName();
    private static final String EMPTY = "                    ";
    private CustomEditText etEmail;
    private CustomEditText etPassword;
    private CustomEditText etConfirmPassword;
    private CustomEditText etExtraPhoneOrEmail;
    private CustomEditText etFirstName;
    private CustomEditText etLastName;
    private ImageView imgPasswordConfirm;
    private boolean isShowPassword = false;
    private boolean isShowPasswordConfirm = false;
    private TextView tvEmailMobile;
    private String email;
    private String password;
    private String phone;
    private String countryCode = ConstantsUtils.DEFAULT_COUNTRY_CODE;
    private ProgressBar progressBar;
    private int registerCase;
    private ArrayList<CountryCodeModels> mCountryCodeModels;
    private String mNewUserId;
    private String firstName;
    private String lastName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        BKGlobals.getSharedGlobals().saveStringPreferences(ConstantsUtils.PREF_KEY_EMAIL, null);
        BKGlobals.getSharedGlobals().saveStringPreferences(ConstantsUtils.PREF_KEY_CONTACT_NUMBER, null);
        BKGlobals.getSharedGlobals().saveStringPreferences(ConstantsUtils.PREF_KEY_COUNTRY_CODE, null);
        BKGlobals.getSharedGlobals().saveStringPreferences(ConstantsUtils.PREF_KEY_PASSWORD, null);
        BKGlobals.getSharedGlobals().saveStringPreferences(BKGlobals.ACCESS_TOKEN, null);

        progressBar = (ProgressBar)findViewById(R.id.progressBar_horizontal);
        registerCase = getIntent().getIntExtra(ConstantsUtils.INTENT_EXTRA_VERIFY_EMAIL_OR_PHONE,ConstantsUtils.VERIFY_EMAIL);

        email = getIntent().getStringExtra(ConstantsUtils.INTENT_EXTRA_EMAIL);
        password = getIntent().getStringExtra(ConstantsUtils.INTENT_EXTRA_PASSWORD);
        phone = getIntent().getStringExtra(ConstantsUtils.INTENT_EXTRA_PHONE);
        countryCode = getIntent().getStringExtra(ConstantsUtils.INTENT_EXTRA_COUNTRY_CODE);

        initUIElement();

        if (email != null && phone == null) {
            findViewById(R.id.view_1).setVisibility(View.VISIBLE);
            setupExtraPhoneEmailInfoView(1);
        }else if(email == null && phone != null) {
            findViewById(R.id.view_1).setVisibility(View.VISIBLE);
            setupExtraPhoneEmailInfoView(2);
        }else {
            findViewById(R.id.view_1).setVisibility(View.GONE);
            setupExtraPhoneEmailInfoView(3);
        }

        tvEmailMobile.setOnClickListener(this);
        etPassword.setText(password);
        etConfirmPassword.setText("");
        etConfirmPassword.setHint(EMPTY);
        etFirstName.setText("");
        etFirstName.setHint(EMPTY);
        etLastName.setText("");
        etLastName.setHint(EMPTY);

        showPassword(isShowPassword);
        showConfirmPassword(isShowPasswordConfirm);
    }
    @Override
    protected void onStart() {
        super.onStart();
        Analytics.with(this).screen("Create Account");
    }

    private void setupExtraPhoneEmailInfoView(int i) {
        findViewById(R.id.view_extra).setVisibility(View.VISIBLE);
        etExtraPhoneOrEmail.setText("");
        etExtraPhoneOrEmail.setHint(EMPTY);
        switch (i){
            case 1:
                tvEmailMobile.setText(email);
                etEmail.setText(email);
                ((TextView) findViewById(R.id.tvExtraPhoneMobileTitle)).setText("PHONE");

                etExtraPhoneOrEmail.setInputType(InputType.TYPE_CLASS_PHONE);
                findViewById(R.id.ll_countryCode2).setVisibility(View.VISIBLE);

                RelativeLayout.LayoutParams params1 = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                params1.addRule(RelativeLayout.RIGHT_OF, findViewById(R.id.ll_countryCode2).getId());
                etExtraPhoneOrEmail.setLayoutParams(params1);
                break;

            case 2:
                tvEmailMobile.setText("("+countryCode + ") " + phone);
                ((TextView) findViewById(R.id.tvExtraPhoneMobileTitle)).setText("EMAIL");

                etExtraPhoneOrEmail.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
                findViewById(R.id.ll_countryCode2).setVisibility(View.GONE);

                RelativeLayout.LayoutParams params2 = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                params2.addRule(RelativeLayout.CENTER_HORIZONTAL);
                etExtraPhoneOrEmail.setLayoutParams(params2);
                break;
            case 3:
                //findViewById(R.id.view_extra).setVisibility(View.GONE);
                changeLoginRegisterMethod(LoginActivity.RegisterCase.EMAIL);
                break;
        }
    }

    public void initUIElement() {
        tvEmailMobile = (TextView) findViewById(R.id.tv_email);
        etEmail = (CustomEditText) findViewById(R.id.ed_email);
        etPassword = (CustomEditText) findViewById(R.id.et_password);
        etConfirmPassword = (CustomEditText) findViewById(R.id.et_confirm_password);
        imgPasswordConfirm = (ImageView) findViewById(R.id.img_password_confirm);
        etExtraPhoneOrEmail = (CustomEditText) findViewById(R.id.et_phone_or_email);
        etFirstName = (CustomEditText)findViewById(R.id.et_FirstName);
        etLastName = (CustomEditText)findViewById(R.id.et_lastName);

        findViewById(R.id.ll_countryCode2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopUpMenu(findViewById(R.id.ll_countryCode2));
            }
        });
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.CENTER_HORIZONTAL);
        etExtraPhoneOrEmail.setLayoutParams(params);

        findViewById(R.id.imv_back).setOnClickListener(this);
        findViewById(R.id.tvTermCondition).setOnClickListener(this);
        findViewById(R.id.tvPrivacy).setOnClickListener(this);
    }

    public void showPassword(View view) {
        isShowPassword = !isShowPassword;
        showPassword(isShowPassword);
    }

    public void showConfirmPassword(View view) {
        isShowPasswordConfirm = !isShowPasswordConfirm;
        showConfirmPassword(isShowPasswordConfirm);
    }

    public void showPassword(boolean isShowPassword) {
        etPassword.setTransformationMethod(isShowPassword ? null : new AsteriskPasswordTransformationMethod());
        findViewById(R.id.img_password).setBackgroundResource(isShowPassword ? R.drawable.new_ic_show_pass : R.drawable.new_ic_hide_pass);
    }

    public void showConfirmPassword(boolean isShowPassword) {
        etConfirmPassword.setTransformationMethod(isShowPassword ? null : new AsteriskPasswordTransformationMethod());
        imgPasswordConfirm.setBackgroundResource(isShowPassword ? R.drawable.new_ic_show_pass : R.drawable.new_ic_hide_pass);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case ConstantsUtils.REQUEST_CODE_ACCEPT:
                if (resultCode == RESULT_OK) {
                    actionCallApiToRegister();
                }else {
                    ToastUtil.showToastMessage(SignUpActivity.this,"You must accept all agreements in order to be able to register", false);
                }
                break;
        }
    }

    public void onSignUpClick(View view) {
        Analytics.with(this).track("Click Create_account_btn");

        boolean validate = validateLogInInput();
        if (!validate)
            return;

        if(ConfigUtil.checkIfNeedTermOfService()) {
            UserModel user = new UserModel();
            user.email = email;
            user.phone = phone;
            user.name = firstName;
            user.last_name = lastName;
            saveUser(user);

            if(!(((CheckBox)findViewById(R.id.cbAccept1))).isChecked()||!(((CheckBox)findViewById(R.id.cbAccept2))).isChecked()){
                ToastUtil.showToastMessage(SignUpActivity.this,"You must accept all agreements in order to be able to register", false);
            }else {
                actionCallApiToRegister();
            }

            /*Intent intent = new Intent(SignUpActivity.this, AcceptActivity.class);
            startActivityForResult(intent, ConstantsUtils.REQUEST_CODE_ACCEPT);*/
        }else {
            actionCallApiToRegister();
        }
    }

    private void actionCallApiToRegister() {
        progressBar.setVisibility(View.VISIBLE);
        VolleyUtils.getSharedNetwork().registerUser(this, firstName, lastName, email, countryCode, phone, password,
                ConfigUtil.checkIfNeedTermOfService(),
                new OnResponseModel() {

            @Override
            public void onResponseSuccess(Object model) {
                progressBar.setVisibility(View.GONE);

                KZRegistrationResponse response = (KZRegistrationResponse) model;
                mNewUserId = response.id;

                moveToVerifyScreen();
            }

            @Override
            public void onResponseError(BKNetworkResponseError networkResponse) {
                progressBar.setVisibility(View.INVISIBLE);
                VolleyUtils.handleErrorRespond(SignUpActivity.this, networkResponse);

            }
        });
    }

    private void moveToVerifyScreen() {
        UserModel user = new UserModel();
        user.email = email;
        user.name = firstName;
        user.last_name = lastName;
        user.phone = phone;
        user.id = Integer.parseInt(mNewUserId);

        saveUser(user);

        switch (registerCase){
            case ConstantsUtils.VERIFY_EMAIL:
                //If register by email, ..
                Intent intent = new Intent(SignUpActivity.this, VerifyPhoneActivity.class);
                intent.putExtra(ConstantsUtils.INTENT_EXTRA_EMAIL,email);
                intent.putExtra(ConstantsUtils.INTENT_EXTRA_ACTIVATE_ACCOUNT_OR_RESET_PASSWORD,ConstantsUtils.VERIFY_PHONE_CASE_ACTIVATE_ACCOUNT);
                intent.putExtra(ConstantsUtils.INTENT_EXTRA_ACCESS_USER_ID, mNewUserId);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
                break;
            case ConstantsUtils.VERIFY_PHONE:
                intent = new Intent(SignUpActivity.this, VerifyPhoneActivity.class);
                intent.putExtra(ConstantsUtils.INTENT_EXTRA_VERIFY_EMAIL_OR_PHONE, registerCase);
                intent.putExtra(ConstantsUtils.INTENT_EXTRA_ACTIVATE_ACCOUNT_OR_RESET_PASSWORD,ConstantsUtils.VERIFY_PHONE_CASE_ACTIVATE_ACCOUNT);
                intent.putExtra(ConstantsUtils.INTENT_EXTRA_ACCESS_USER_ID, mNewUserId);
                intent.putExtra(ConstantsUtils.INTENT_EXTRA_EMAIL, email);
                intent.putExtra(ConstantsUtils.INTENT_EXTRA_PHONE, phone);
                intent.putExtra(ConstantsUtils.INTENT_EXTRA_COUNTRY_CODE, countryCode);
                intent.putExtra(ConstantsUtils.INTENT_EXTRA_PASSWORD, password);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
                break;
        }
    }

    private boolean validateLogInInput() {
        switch (registerCase){
            case ConstantsUtils.VERIFY_EMAIL:
                if (etEmail.getText().toString().equals("")) {
                    Toast.makeText(this, getString(R.string.error_empty_email), Toast.LENGTH_SHORT).show();
                    return false;
                } else {
                    if (!ValidateUtils.validateEmail(this, etEmail.getText().toString())) {
                        Toast.makeText(this, getString(R.string.error_format_email), Toast.LENGTH_SHORT).show();
                        return false;
                    }
                }
                email = etEmail.getText().toString();
                break;
            case ConstantsUtils.VERIFY_PHONE:
                if (phone.equals("")) {
                    Toast.makeText(this, getString(R.string.error_empty_phone), Toast.LENGTH_SHORT).show();
                    return false;
                } else {
                    if(!ValidateUtils.validatePhone(phone)){
                        Toast.makeText(this, getString(R.string.error_format_phone), Toast.LENGTH_SHORT).show();
                        return false;
                    }
                }
                break;
        }

        if(!checkPasswordMatchOrNot()) {
            return false;
        } else {
            password = etPassword.getText().toString().trim();
        }
        if(etPassword.getText().toString().isEmpty()){
            Toast.makeText(this, getString(R.string.error_empty_password), Toast.LENGTH_SHORT).show();
            return false;
        }
        if(!ValidateUtils.validatePassword(etPassword.getText().toString(), getApplicationContext())){
            //Toast.makeText(this, getString(R.string.error_format_password_limit_character), Toast.LENGTH_SHORT).show();
            return false;
        }
        if(etFirstName.getText().toString().isEmpty()){
            Toast.makeText(this, getString(R.string.error_empty_first_name), Toast.LENGTH_SHORT).show();
            return false;
        }
        if(etLastName.getText().toString().isEmpty()){
            Toast.makeText(this, getString(R.string.error_empty_lastname), Toast.LENGTH_SHORT).show();
            return false;
        }
        firstName = etFirstName.getText().toString().trim();
        lastName = etLastName.getText().toString().trim();

        switch (registerCase){
            case ConstantsUtils.VERIFY_EMAIL:
                String temp = etExtraPhoneOrEmail.getText().toString();
                if (temp.equals("")) {
                    Toast.makeText(this, getString(R.string.error_empty_phone), Toast.LENGTH_SHORT).show();
                    return false;
                } else {
                    if (!ValidateUtils.validatePhone(temp)) {
                        Toast.makeText(this, getString(R.string.error_format_phone), Toast.LENGTH_SHORT).show();
                        return false;
                    }
                    phone = etExtraPhoneOrEmail.getText().toString().trim();
                }
                break;
            case ConstantsUtils.VERIFY_PHONE:
                temp = etExtraPhoneOrEmail.getText().toString();
                if (temp.equals("")) {
                    Toast.makeText(this, getString(R.string.error_empty_email), Toast.LENGTH_SHORT).show();
                    return false;
                } else {
                    if(!ValidateUtils.validateEmail(this, temp)){
                        Toast.makeText(this, getString(R.string.error_format_email), Toast.LENGTH_SHORT).show();
                        return false;
                    }
                    email = etExtraPhoneOrEmail.getText().toString().trim();
                }
                break;
        }

        return true;
    }

    public boolean checkPasswordMatchOrNot() {
        String password = etPassword.getText().toString();
        String passwordConfirm = etConfirmPassword.getText().toString();


        if (password.equalsIgnoreCase(passwordConfirm)) {
            return true;
        } else {
            Toast.makeText(this, "Your passwords are not matched", Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    private void showPopUpMenu(View v) {
        PopupMenu popup = new PopupMenu(this, v);
        //Inflating the Popup using xml file
        popup.getMenuInflater().inflate(R.menu.postcode_menu, popup.getMenu());

        //Hard code for example
        mCountryCodeModels = new ArrayList<>();
        //mCountryCodeModels.add(new CountryCodeModels("US +1", "1"));
        //mCountryCodeModels.add(new CountryCodeModels("NZ +64", "64"));
        mCountryCodeModels.add(new CountryCodeModels("AU +61", "61"));
        //mCountryCodeModels.add(new CountryCodeModels("VN +84", "84"));


        //Programmatically add item
        Menu menu = popup.getMenu();
        for (int i = 0; i < mCountryCodeModels.size(); i++) {
            menu.add(0, i, i, mCountryCodeModels.get(i).getValue());
        }

        //registering popup with OnMenuItemClickListener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                (((TextView) findViewById(R.id.tvCountryCodeValue))).setText(item.getTitle().toString());
                countryCode = mCountryCodeModels.get(item.getItemId()).getNumber();
                return true;
            }
        });

        popup.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imv_back:
                String confirmPassword = etConfirmPassword.getText().toString().trim();
                String name = etFirstName.getText().toString().trim();
                String emailOrPhone = etExtraPhoneOrEmail.getText().toString().trim();
                if (!confirmPassword.equals("") || !name.equals("") || !emailOrPhone.equals("")) {
                    openCancelSignUpDialog();
                } else {
                    finish();
                }
                break;

            case R.id.tv_email:
                openDialogToEditEmail();
                break;
            case R.id.tvTermCondition:
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Agreements2Activity.a));
                startActivity(browserIntent);
                break;
            case R.id.tvPrivacy:
                browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Agreements2Activity.b));
                startActivity(browserIntent);
                break;

        }
    }

    private void openCancelSignUpDialog() {
        final AlertDialog.Builder builder ;
        builder = new AlertDialog.Builder(this);
        builder.setTitle("");
        builder.setMessage(getString(R.string.confirm_cancel_signup));
        builder.setCancelable(true);
        builder.setPositiveButton(getString(R.string.btn_no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.setNegativeButton(getString(R.string.btn_yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                finish();
            }
        });
        builder.create().show();
    }

    private void openDialogToEditEmail() {
        AlertDialog.Builder builder ;
        builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.custom_dialog_1, null);
        final EditText edt = (EditText) v.findViewById(R.id.edt);
        builder.setView(v);
        switch (registerCase){
            case ConstantsUtils.VERIFY_EMAIL:
                edt.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
                edt.setText(email);
                builder.setTitle("Edit your email");
                break;
            case ConstantsUtils.VERIFY_PHONE:
                edt.setText(phone);
                edt.setInputType(InputType.TYPE_CLASS_PHONE);
                builder.setTitle("Edit your contact number");
                break;
        }

        builder.setCancelable(true);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                boolean validate = isValidated(edt);
                if(!validate) return;
                switch (registerCase){
                    case ConstantsUtils.VERIFY_EMAIL:
                        email = edt.getText().toString().trim();
                        tvEmailMobile.setText(edt.getText().toString());
                        break;
                    case ConstantsUtils.VERIFY_PHONE:
                        phone = edt.getText().toString().trim();
                        tvEmailMobile.setText("("+countryCode+") "+edt.getText().toString());
                        break;
                }

            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.create().show();
    }

    private boolean isValidated(EditText edt) {
        if(edt.getText().toString().isEmpty()){
            Toast.makeText(this,"This information can not be empty", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    private void changeLoginRegisterMethod(LoginActivity.RegisterCase mobile) {
        switch (mobile) {
            case MOBILE:
                /*registerCase = ConstantsUtils.VERIFY_PHONE;
                findViewById(R.id.ll_countryCode1).setVisibility(View.VISIBLE);
                edtEmailMobile.setInputType(InputType.TYPE_CLASS_PHONE);
                setupExtraPhoneEmailInfoView(2);*/
                break;

            case EMAIL:
                registerCase = ConstantsUtils.VERIFY_EMAIL;
                setupExtraPhoneEmailInfoView(1);
                break;
        }
    }
}
