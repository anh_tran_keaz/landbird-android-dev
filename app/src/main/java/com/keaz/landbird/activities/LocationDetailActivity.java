package com.keaz.landbird.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.keaz.landbird.R;
import com.keaz.landbird.models.KZAssets;
import com.keaz.landbird.models.KZBranch;
import com.keaz.landbird.utils.BKNetworkResponseError;
import com.keaz.landbird.utils.JsonUtils;
import com.keaz.landbird.utils.OnResponseModel;
import com.keaz.landbird.utils.UIUtils;
import com.keaz.landbird.utils.Util;
import com.keaz.landbird.utils.VolleyUtils;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class LocationDetailActivity extends BaseActivity {

    public static KZAssets assets;
    public static HashMap<String, Object> totals;
    private Button btnSelect;
    private KZBranch branch;
    private FrameLayout frameLayout;
    private ImageView ivLocation;
    private HorizontalScrollView photoScroll;
    private LinearLayout lnPhotos;

    private String mUrlSelected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getIntent() != null) {
            branch = getIntent().getParcelableExtra("branch");
        }
        setContentView(R.layout.activity_location_detail);
        btnSelect = (Button) findViewById(R.id.btnSelect);
        btnSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LocationDetailActivity.this, VehicleOfABranchActivity.class);
                intent.putExtra("branch", branch);
                startActivity(intent);
                finish();
            }
        });
        frameLayout = (FrameLayout) findViewById(R.id.frameBg);
        ivLocation = (ImageView) findViewById(R.id.ivLocation);
        photoScroll = (HorizontalScrollView) findViewById(R.id.hv_photos);
        lnPhotos = (LinearLayout) findViewById(R.id.photos);
        findViewById(R.id.backDetail).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        CharSequence charSequence = totals != null ?
                ((Double) totals.get("vehicles")).intValue() + " Vehicles"
                : "0 Vehicle";
        UIUtils.setTextViewText(this, R.id.branchName, branch.name);
        UIUtils.setTextViewText(this, R.id.vehicles, charSequence);

        mUrlSelected = assets.photo;
        if (mUrlSelected == null || mUrlSelected.equals("")) {
            frameLayout.setBackgroundColor(Color.rgb(179,179,179));
        }
        String description = branch.address;
        UIUtils.setTextViewText(this, R.id.details, description);
        /*Picasso.with(this).load(url).into(target());*/
        ImageLoader imageLoader = ImageLoader.getInstance();
        imageLoader.loadImage(mUrlSelected, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {

            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                frameLayout.setBackgroundColor(Color.rgb(179,179,179));
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                if (loadedImage == null) {
                    frameLayout.setBackgroundColor(Color.rgb(179,179,179));
                } else {
                    frameLayout.setBackground(new BitmapDrawable(getResources(), loadedImage));
                }
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {
                frameLayout.setBackgroundColor(Color.rgb(179,179,179));
            }
        });

        loadBranchAssets(branch.id+"");
    }

    private void loadBranchAssets(String id) {
        VolleyUtils.getSharedNetwork().loadBranchAsset(
                LocationDetailActivity.this,
                id, new OnResponseModel<JSONObject>() {
            @Override
            public void onResponseSuccess(JSONObject model) {
                ArrayList<String> list = new ArrayList<>();
                try {
                    Map<String, Object> map = JsonUtils.toMap(model);
                    for (Object s:map.values()) {
                        String url = (String)s;
                        if(url==null || url.equals("")){
                            break;
                        }else {
                            list.add(url);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if(!list.isEmpty()){
                    showPictures(list);
                }
            }
            @Override
            public void onResponseError(BKNetworkResponseError error) {
                VolleyUtils.handleErrorRespond(LocationDetailActivity.this, error);
            }
        });
    }

    private void showPictures(ArrayList<String> list) {
        for (String url: list) {
            if (!url.equals(mUrlSelected)) {
                ImageLoader imageLoader = ImageLoader.getInstance();
                imageLoader.loadImage(url, new ImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String imageUri, View view) {
                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        handleInsertPhotos(loadedImage);
                    }

                    @Override
                    public void onLoadingCancelled(String imageUri, View view) {
                    }
                });
            }
        }
    }

    private void handleInsertPhotos(Bitmap bitmap) {
        final ImageView imageView = new ImageView(this);
        int width = Util.convertDp2Px(this, 150);
        int height = Util.convertDp2Px(this, 100);
        RelativeLayout.LayoutParams params1 = new RelativeLayout.LayoutParams(width, height);
        params1.addRule(RelativeLayout.CENTER_IN_PARENT, 1);
        imageView.setLayoutParams(params1);
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        imageView.setImageBitmap(bitmap);
        int margin = Util.convertDp2Px(this, 10);
        imageView.setPadding(margin, margin, margin, margin);
        lnPhotos.addView(imageView);
    }

    private Target target() {
        return new Target() {
            @Override
            public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Drawable d = new BitmapDrawable(getResources(), bitmap);
                        //frameLayout.setBackgroundColor(Color.argb(255,255,255,255));
                        //ivLocation.setImageBitmap(bitmap);
                        frameLayout.setBackground(d);
                        handleInsertPhotos(bitmap);
                    }
                });
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        };
    }
}
