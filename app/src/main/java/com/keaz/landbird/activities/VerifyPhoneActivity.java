package com.keaz.landbird.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.keaz.landbird.R;
import com.keaz.landbird.models.KZLoginResponse;
import com.keaz.landbird.models.KZVerifyResponse;
import com.keaz.landbird.utils.BKGlobals;
import com.keaz.landbird.utils.BKNetworkResponseError;
import com.keaz.landbird.utils.ConstantsUtils;
import com.keaz.landbird.utils.DialogUtils;
import com.keaz.landbird.utils.FontUtil;
import com.keaz.landbird.utils.IntentUtil;
import com.keaz.landbird.utils.OnResponseModel;
import com.keaz.landbird.utils.VolleyUtils;
import com.keaz.landbird.view.CustomEditText;
import com.segment.analytics.Analytics;
import com.segment.analytics.Properties;

import java.text.SimpleDateFormat;
import java.util.Date;

public class VerifyPhoneActivity extends BaseActivity {


    private CustomEditText mEd1, mEd2, mEd3, mEd4;
    private ProgressBar mProgressBar;
    private String mEmail;
    private String mContactNumber;
    private String mPassword;
    private String mCountryCode;
    private String mName;
    private String mUserId;
    private int mVerifyEmailOrPhone;
    private TextView mTxtTitle;
    private String mActivityAccountOrResetPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        BKGlobals.getSharedGlobals().saveStringPreferences(ConstantsUtils.PREF_KEY_EMAIL, null);
        BKGlobals.getSharedGlobals().saveStringPreferences(ConstantsUtils.PREF_KEY_CONTACT_NUMBER, null);
        BKGlobals.getSharedGlobals().saveStringPreferences(ConstantsUtils.PREF_KEY_COUNTRY_CODE, null);
        BKGlobals.getSharedGlobals().saveStringPreferences(ConstantsUtils.PREF_KEY_PASSWORD, null);
        BKGlobals.getSharedGlobals().saveStringPreferences(BKGlobals.ACCESS_TOKEN, null);

        setContentView(R.layout.activity_verify_phone);

        mVerifyEmailOrPhone = getIntent().getIntExtra(ConstantsUtils.INTENT_EXTRA_VERIFY_EMAIL_OR_PHONE, ConstantsUtils.VERIFY_EMAIL);
        mActivityAccountOrResetPassword = getIntent().getStringExtra(ConstantsUtils.INTENT_EXTRA_ACTIVATE_ACCOUNT_OR_RESET_PASSWORD);

        mUserId = getIntent().getStringExtra(ConstantsUtils.INTENT_EXTRA_ACCESS_USER_ID);
        mEmail = getIntent().getStringExtra(ConstantsUtils.INTENT_EXTRA_EMAIL);
        mContactNumber = getIntent().getStringExtra(ConstantsUtils.INTENT_EXTRA_PHONE);
        if(mEmail!=null) mEmail = mEmail.trim();
        if(mContactNumber!=null) mContactNumber = mContactNumber.trim();
        mPassword = getIntent().getStringExtra(ConstantsUtils.INTENT_EXTRA_PASSWORD);
        mCountryCode = getIntent().getStringExtra(ConstantsUtils.INTENT_EXTRA_COUNTRY_CODE);
        mName = getIntent().getStringExtra(ConstantsUtils.INTENT_EXTRA_NAME);

        initLayout();
        setUpEditTexts();
        initListener();

        switch (mActivityAccountOrResetPassword){
            case ConstantsUtils.VERIFY_PHONE_CASE_ACTIVATE_ACCOUNT:
                openVerifyToActivateAccount();
                break;
            case ConstantsUtils.VERIFY_PHONE_CASE_RESET_PASSWORD:
                openVerifyToResetPassword();
                break;
            case ConstantsUtils.VERIFY_PHONE_CASE_INVITE_USER:
                setupVerifyToInviteUser();
                break;
        }
    }
    @Override
    protected void onStart() {
        super.onStart();

        SimpleDateFormat sdf = new SimpleDateFormat(ConstantsUtils.FORMAT_SEGMENT_DATE_TIME);
        String currentDateandTime = sdf.format(new Date());
        Analytics.with(this).screen("Verify", new Properties().putValue("Date", currentDateandTime));
    }
    @Override
    public void onBackPressed() {
        clickBack();
    }

    private void clickBack() {
        DialogUtils.showCustomDialog(this,
                true,
                getString(R.string.confirm_to_exit),
                "",
                getString(R.string.btn_yes),
                getString(R.string.btn_no),
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(VerifyPhoneActivity.this, LoginActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();
                    }
                },
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                    }
                });
    }

    private void setupVerifyToInviteUser() {
        mTxtTitle.setText("Activate");
        ((TextView)findViewById(R.id.tvMessage1)).setText("A verification code was sent to your email. ");
        ((TextView)findViewById(R.id.tvMessage2)).setText("Please enter it here:");
        switch (mVerifyEmailOrPhone) {
            case ConstantsUtils.VERIFY_EMAIL:
                //((TextView)findViewById(R.id.tvMessage2)).setText(mEmail);
                break;

            case ConstantsUtils.VERIFY_PHONE:
                //((TextView)findViewById(R.id.tvMessage2)).setText("(" + mCountryCode + ") " + mContactNumber);
                break;
        }
        actionCallApiToSendVerifyCodeInviteUser();
    }

    private void openVerifyToResetPassword() {
        mTxtTitle.setText("Reset Password");
        ((TextView)findViewById(R.id.tvMessage1)).setText("Please enter code that was sent to");
        switch (mVerifyEmailOrPhone) {
            case ConstantsUtils.VERIFY_EMAIL:
                ((TextView)findViewById(R.id.tvMessage2)).setText(mEmail);
                break;

            case ConstantsUtils.VERIFY_PHONE:
                ((TextView)findViewById(R.id.tvMessage2)).setText("(" + mCountryCode + ") " + mContactNumber);
                break;
        }

        callApiToSendResetPasswordCode();
    }

    private void callApiToSendResetPasswordCode() {
        mProgressBar.setVisibility(View.VISIBLE);
        VolleyUtils.getSharedNetwork().getActiveCodeToResetPassword(
                VerifyPhoneActivity.this,
                mEmail,
                mContactNumber, mCountryCode, new OnResponseModel() {
            @Override
            public void onResponseSuccess(Object model) {
                mProgressBar.setVisibility(View.INVISIBLE);
                openMessageDialog();
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                mProgressBar.setVisibility(View.INVISIBLE);
                VolleyUtils.handleErrorRespond(VerifyPhoneActivity.this, error);
            }
        });
    }

    private void openVerifyToActivateAccount() {
        boolean callApiResendCode = getIntent().getBooleanExtra(ConstantsUtils.INTENT_EXTRA_CALL_API_RESEND_CODE,false);
        mTxtTitle.setText("Account Verification");
        ((TextView)findViewById(R.id.tvMessage1)).setText("A verification code was sent to your email. ");
        switch (mVerifyEmailOrPhone) {
            case ConstantsUtils.VERIFY_EMAIL:
                //((TextView)findViewById(R.id.tvMessage2)).setText(mEmail);
                ((TextView)findViewById(R.id.tvMessage2)).setText("Please enter it here:");
                break;

            case ConstantsUtils.VERIFY_PHONE:
                ((TextView)findViewById(R.id.tvMessage2)).setText("(" + mCountryCode + ") " + mContactNumber);
                break;
        }

        if(callApiResendCode){
            onResendVerifyCode(true);
        }
    }

    private void openMessageDialog() {
        AlertDialog.Builder builder ;
        builder = new AlertDialog.Builder(this);
        builder.setTitle(mVerifyEmailOrPhone == ConstantsUtils.VERIFY_PHONE ? "A verification code has been sent to your phone number.":"A verification code has been sent to your email.");
        builder.setMessage("Enter it to activate your account.");
        builder.setCancelable(true);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();

            }
        });
        builder.create().show();
    }

    public void initLayout() {
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar_horizontal);
        mEd1 = (CustomEditText) findViewById(R.id.verify_phone_number_edit_text1);
        mEd2 = (CustomEditText) findViewById(R.id.verify_phone_number_edit_text2);
        mEd3 = (CustomEditText) findViewById(R.id.verify_phone_number_edit_text3);
        mEd4 = (CustomEditText) findViewById(R.id.verify_phone_number_edit_text4);
        mEd1.setTypeface(FontUtil.robotoLight(getApplicationContext()));
        mEd2.setTypeface(FontUtil.robotoLight(getApplicationContext()));
        mEd3.setTypeface(FontUtil.robotoLight(getApplicationContext()));
        mEd4.setTypeface(FontUtil.robotoLight(getApplicationContext()));

        mTxtTitle = (TextView)findViewById(R.id.txt_title);

        findViewById(R.id.imvClose).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickBack();
            }
        });

        findViewById(R.id.btnResendCode).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Analytics.with(VerifyPhoneActivity.this).track("Click Resend_SMS_btn");

                switch (mActivityAccountOrResetPassword){
                    case ConstantsUtils.VERIFY_PHONE_CASE_ACTIVATE_ACCOUNT:
                        onResendVerifyCode(false);
                        break;
                    case ConstantsUtils.VERIFY_PHONE_CASE_RESET_PASSWORD:
                        callApiToSendResetPasswordCode();
                        break;
                    case ConstantsUtils.VERIFY_PHONE_CASE_INVITE_USER:
                        actionCallApiToSendVerifyCodeInviteUser();
                        break;
                }
            }
        });
    }

    public void initListener() {
        mEd4.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    // do your stuff here
                }
                return false;
            }
        });
    }

    private void setUpEditTexts() {

        mEd1.addTextChangedListener(new CustomTextWatcher(mEd1));
        mEd2.addTextChangedListener(new CustomTextWatcher(mEd2));
        mEd3.addTextChangedListener(new CustomTextWatcher(mEd3));
        mEd4.addTextChangedListener(new CustomTextWatcher(mEd4));

        mEd1.findFocus();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        mEd1.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
                if (keyCode == KeyEvent.KEYCODE_DEL || keyCode == KeyEvent.KEYCODE_BACK) {
                    //this is for backspace
                    switchFocusBack(mEd1);
                }
                return false;
            }
        });
        mEd2.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
                if (keyCode == KeyEvent.KEYCODE_DEL || keyCode == KeyEvent.KEYCODE_BACK) {
                    //this is for backspace
                    switchFocusBack(mEd2);
                }
                return false;
            }
        });
        mEd3.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
                if (keyCode == KeyEvent.KEYCODE_DEL || keyCode == KeyEvent.KEYCODE_BACK) {
                    //this is for backspace
                    switchFocusBack(mEd3);
                }
                return false;
            }
        });
        mEd4.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
                if (keyCode == KeyEvent.KEYCODE_DEL || keyCode == KeyEvent.KEYCODE_BACK) {
                    //this is for backspace
                    switchFocusBack(mEd4);
                }
                return false;
            }
        });

        mEd1.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE) || (actionId == EditorInfo.IME_ACTION_NEXT)) {
                    switchFocus(mEd1);
                }
                return false;
            }
        });
        mEd2.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE) || (actionId == EditorInfo.IME_ACTION_NEXT)) {
                    switchFocus(mEd2);
                }
                return false;
            }
        });
        mEd3.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE) || (actionId == EditorInfo.IME_ACTION_NEXT)) {
                    switchFocus(mEd3);
                }
                return false;
            }
        });
        mEd4.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    switchFocus(mEd4);
                }
                return false;
            }
        });

    }

    private void switchFocus(EditText editText) {
        switch (editText.getId()) {
            case R.id.verify_phone_number_edit_text1:
                mEd2.requestFocus();
                break;
            case R.id.verify_phone_number_edit_text2:
                mEd3.requestFocus();
                break;
            case R.id.verify_phone_number_edit_text3:
                mEd4.requestFocus();
                break;
            case R.id.verify_phone_number_edit_text4:
                verify();
                mEd4.requestFocus();
                break;

        }
    }

    public void onResendVerifyCode(final boolean openDialog) {
        mProgressBar.setVisibility(View.VISIBLE);
        VolleyUtils.getSharedNetwork().resentActivateCode(this, mUserId, new OnResponseModel() {
            @Override
            public void onResponseSuccess(Object model) {
                mProgressBar.setVisibility(View.INVISIBLE);
                if(openDialog) {
                    openMessageDialog();
                }
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                mProgressBar.setVisibility(View.INVISIBLE);
                VolleyUtils.handleErrorRespond(VerifyPhoneActivity.this, error);
            }
        });
    }

    private void verify() {
        String verifyCode = mEd1.getText().toString() + mEd2.getText().toString() + mEd3.getText().toString() + mEd4.getText().toString();
        switch (mActivityAccountOrResetPassword){
            case ConstantsUtils.VERIFY_PHONE_CASE_ACTIVATE_ACCOUNT:
                actionCallApiToVerifyActivateUser(verifyCode);
                break;
            case ConstantsUtils.VERIFY_PHONE_CASE_RESET_PASSWORD:
                actionCallApiToVerifyResetPassword(verifyCode);
                break;
            case ConstantsUtils.VERIFY_PHONE_CASE_INVITE_USER:
                actionCallApiToVerifyInviteUser(verifyCode);
                break;
        }

    }

    private void actionCallApiToVerifyResetPassword(final String verifyCode) {
        mProgressBar.setVisibility(View.VISIBLE);
        VolleyUtils.getSharedNetwork().verifyToResetPassword(this, verifyCode, new OnResponseModel() {
            @Override
            public void onResponseSuccess(Object model) {
                Analytics.with(VerifyPhoneActivity.this).track("Verified successfully");

                mProgressBar.setVisibility(View.INVISIBLE);
                Intent intent = new Intent(VerifyPhoneActivity.this, ActivityResetPassword.class);
                intent.putExtra(ConstantsUtils.INTENT_EXTRA_VERIFY_EMAIL_OR_PHONE, mVerifyEmailOrPhone);
                intent.putExtra(ConstantsUtils.INTENT_EXTRA_CODE, verifyCode);
                intent.putExtra(ConstantsUtils.INTENT_EXTRA_EMAIL, mEmail);
                intent.putExtra(ConstantsUtils.INTENT_EXTRA_PHONE, mContactNumber);
                intent.putExtra(ConstantsUtils.PREF_KEY_COUNTRY_CODE, mCountryCode);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                mProgressBar.setVisibility(View.INVISIBLE);
                VolleyUtils.handleErrorRespond(VerifyPhoneActivity.this, error);
            }
        });
    }

    private void actionCallApiToVerifyActivateUser(String verifyCode) {
        mProgressBar.setVisibility(View.VISIBLE);
        VolleyUtils.getSharedNetwork().activateUser(this, verifyCode, new OnResponseModel() {
            @Override
            public void onResponseSuccess(Object model) {
                Analytics.with(VerifyPhoneActivity.this).track("Verified successfully");
                mProgressBar.setVisibility(View.INVISIBLE);

                KZVerifyResponse verifyResponse = (KZVerifyResponse) model;
                String accessToken = verifyResponse.accessToken;
                BKGlobals.getSharedGlobals().saveStringPreferences(BKGlobals.ACCESS_TOKEN, accessToken);
                actionFinishVerifyActivateAccount(mEmail,mContactNumber,mPassword,mCountryCode,mName);

            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                mProgressBar.setVisibility(View.INVISIBLE);
                VolleyUtils.handleErrorRespond(VerifyPhoneActivity.this, error);
            }
        });
    }

    private void actionCallApiToVerifyInviteUser(final String verifyCode) {
        mProgressBar.setVisibility(View.VISIBLE);
        VolleyUtils.getSharedNetwork().verifyToInviteUser(this, mEmail, mContactNumber, mCountryCode, verifyCode, new OnResponseModel() {
            @Override
            public void onResponseSuccess(Object model) {
                Analytics.with(VerifyPhoneActivity.this).track("Verified successfully");

                mProgressBar.setVisibility(View.INVISIBLE);
                KZLoginResponse verifyResponse = (KZLoginResponse) model;
                actionFinishVerifyInviteUser(verifyResponse);
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                mProgressBar.setVisibility(View.INVISIBLE);
                VolleyUtils.handleErrorRespond(VerifyPhoneActivity.this, error);
            }
        });
    }

    private void actionCallApiToSendVerifyCodeInviteUser() {
        mProgressBar.setVisibility(View.VISIBLE);
        VolleyUtils.getSharedNetwork().getActiveCodeToInviteUser(this, mEmail, mContactNumber, mCountryCode, new OnResponseModel() {
            @Override
            public void onResponseSuccess(Object model) {
                mProgressBar.setVisibility(View.INVISIBLE);
                openMessageDialog();
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                mProgressBar.setVisibility(View.INVISIBLE);
                VolleyUtils.handleErrorRespond(VerifyPhoneActivity.this, error);
            }
        });
    }

    private void actionFinishVerifyInviteUser(KZLoginResponse verifyResponse) {
        String accessToken = verifyResponse.accessToken;
        BKGlobals.getSharedGlobals().saveStringPreferences(BKGlobals.ACCESS_TOKEN, accessToken);

        Intent intent = new Intent(VerifyPhoneActivity.this, ActivityWelcome.class);
        intent = IntentUtil.addFlag1(intent);
        startActivity(intent);
    }

    private void actionFinishVerifyActivateAccount(final String email, String contactNumber, String password, String countryCode, String name) {

        BKGlobals.getSharedGlobals().saveStringPreferences(ConstantsUtils.PREF_KEY_EMAIL, email);
        BKGlobals.getSharedGlobals().saveStringPreferences(ConstantsUtils.PREF_KEY_CONTACT_NUMBER, contactNumber);
        BKGlobals.getSharedGlobals().saveStringPreferences(ConstantsUtils.PREF_KEY_PASSWORD, password);
        BKGlobals.getSharedGlobals().saveStringPreferences(ConstantsUtils.PREF_KEY_COUNTRY_CODE, countryCode);
        BKGlobals.getSharedGlobals().saveStringPreferences(ConstantsUtils.PREF_KEY_NAME, name);

        Intent intent = new Intent(VerifyPhoneActivity.this, ActivityWelcome.class);
        intent.putExtra(ConstantsUtils.INTENT_EXTRA_SHOW_FINISH_SIGN_UP, true);
        intent = IntentUtil.addFlag1(intent);
        startActivity(intent);
    }

    private void switchFocusBack(EditText editText) {
        if (editText.getText().toString().length() > 0) {
            editText.setText("");
            return;
        }
        switch (editText.getId()) {
            case R.id.verify_phone_number_edit_text4:
                mEd3.requestFocus();
                break;
            case R.id.verify_phone_number_edit_text3:
                mEd2.requestFocus();
                break;
            case R.id.verify_phone_number_edit_text2:
                mEd1.requestFocus();
                break;
            case R.id.verify_phone_number_edit_text1:

                break;

        }
    }

    private class CustomTextWatcher implements TextWatcher {

        private final EditText mEd;

        CustomTextWatcher(EditText ed) {
            mEd = ed;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            int iLen = s.length();
            if (iLen > 0 && !Character.isDigit((s.charAt(iLen - 1)))) {
                s.delete(iLen - 1, iLen);
                return;
            }
            if (iLen > 1) {
                s.delete(0, 1);
            }
            if (iLen > 0) switchFocus(mEd);
        }
    }

}
