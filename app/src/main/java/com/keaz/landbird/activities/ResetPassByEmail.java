package com.keaz.landbird.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.keaz.landbird.R;
import com.keaz.landbird.utils.BKNetworkResponseError;
import com.keaz.landbird.utils.ConstantsUtils;
import com.keaz.landbird.utils.OnResponseModel;
import com.keaz.landbird.utils.VolleyUtils;

/**
 * Created by mac on 2/28/17.
 */
public class ResetPassByEmail extends BaseActivity{

    private String mUserId;
    private TextView mTvEmail;
    private LinearLayout mTvBackToLogIn;
    private Button mTvResendCode;
    private ProgressBar mProgressBar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_reset_password_by_email);

        mUserId = getIntent().getStringExtra(ConstantsUtils.INTENT_EXTRA_ACCESS_USER_ID);
        final String email = getIntent().getStringExtra(ConstantsUtils.INTENT_EXTRA_EMAIL);
        mTvEmail = (TextView) findViewById(R.id.tvEmail);
        mTvBackToLogIn = (LinearLayout) findViewById(R.id.tvBackToLogin);
        mTvResendCode = (Button)findViewById(R.id.btnResendCode);
        mProgressBar = (ProgressBar)findViewById(R.id.progressBar_horizontal);
        mTvResendCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callApiToSendResetPasswordCode(email,null,null);
            }
        });

        findViewById(R.id.imvClose).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        mTvEmail.setText("Your email: " + email);
        mTvBackToLogIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ResetPassByEmail.this, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        });

        callApiToSendResetPasswordCode(email,null,null);

    }

    private void callApiToSendResetPasswordCode(String email, String contactNumber, String countryCode) {
        mProgressBar.setVisibility(View.VISIBLE);
        VolleyUtils.getSharedNetwork().getActiveCodeToResetPassword(this, email, contactNumber, countryCode, new OnResponseModel() {
            @Override
            public void onResponseSuccess(Object model) {
                mProgressBar.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                mProgressBar.setVisibility(View.INVISIBLE);
                VolleyUtils.handleErrorRespond(ResetPassByEmail.this, error);
            }
        });
    }


}
