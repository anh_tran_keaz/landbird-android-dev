package com.keaz.landbird.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.keaz.landbird.R;
import com.keaz.landbird.interfaces.CustomAnimationListener;
import com.keaz.landbird.interfaces.CustomListener2;
import com.keaz.landbird.models.KZAccount;
import com.keaz.landbird.models.KZLandingData;
import com.keaz.landbird.models.KZPaymentDetail;
import com.keaz.landbird.models.UserAssetsModel;
import com.keaz.landbird.models.UserModel;
import com.keaz.landbird.utils.BKNetworkResponseError;
import com.keaz.landbird.utils.CommonUtils;
import com.keaz.landbird.utils.ConfigUtil;
import com.keaz.landbird.utils.ConstantsUtils;
import com.keaz.landbird.utils.CustomAnimationUtil;
import com.keaz.landbird.utils.LoadingViewUtils;
import com.keaz.landbird.utils.OnResponseModel;
import com.keaz.landbird.utils.ParseJSonUtil;
import com.keaz.landbird.utils.VolleyUtils;
import com.segment.analytics.Analytics;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by anhtran1810 on 11/29/17.
 */

public class ActivityWelcome extends BaseActivity {

    private String TAG = ActivityWelcome.class.getSimpleName();
    private UserModel userModel;
    private KZPaymentDetail kzPayment;
    private TextView tvName;
    private boolean showFinishSignUp;
    private boolean skipPaymentInRegister = false;
    private ImageView logoImv;
    private boolean isCheckAgreement;

    private ActivityWelcome getThis() {
        return this;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        Analytics.with(this).reset();

        //CharSequence s = tvName.getText();
        tvName = (TextView) findViewById(R.id.tvName);
        findViewById(R.id.view_welcome_back).setVisibility(View.INVISIBLE);
        findViewById(R.id.tvLoading).setVisibility(View.VISIBLE);

        logoImv = (ImageView) findViewById(R.id.imv_logo);

        showFinishSignUp = getIntent().getBooleanExtra(ConstantsUtils.INTENT_EXTRA_SHOW_FINISH_SIGN_UP, false);
        actionLoadLanding2();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode){
            case ConstantsUtils.REQUEST_CODE_ACCEPT:
                if (resultCode == RESULT_OK) {
                    isCheckAgreement = true;
                    actionUpdateUserTermCondition(new CustomListener2() {
                        @Override
                        public void success() {
                            actionCheckAndContinue();
                        }

                        @Override
                        public void fail(BKNetworkResponseError error) {
                            actionCheckAndContinue();
                        }
                    });
                }
                break;

            case ConstantsUtils.REQUEST_CODE_ADD_DRIVER_LICENSE:
                if(resultCode == RESULT_OK) {
                    actionLoadUserInfo(KZAccount.getSharedAccount().user.id, new CustomListener2() {
                        @Override
                        public void success() {
                            showUserName(new CustomAnimationListener() {
                                @Override
                                public void onAnimationEnd() {
                                    actionCheckAndContinue();
                                }
                            });

                        }

                        @Override
                        public void fail(BKNetworkResponseError error) {

                        }
                    });
                }else {
                    LoadingViewUtils.showOrHideProgressBar(false, this);
                }
                break;

            case ConstantsUtils.REQUEST_CODE_SELECT_OR_ADD_PAYMENT_METHOD:
                if(resultCode == RESULT_OK){
                    skipPaymentInRegister = true;
                    actionLoadPayment(new CustomListener2() {
                        @Override
                        public void success() {
                            actionCheckAndContinue();
                        }

                        @Override
                        public void fail(BKNetworkResponseError error) {
                            actionCheckAndContinue();
                        }
                    });
                }
                break;

            case ConstantsUtils.REQUEST_CODE_FINISH_SIGN_UP_SCREEN:
                showFinishSignUp = false;
                actionCheckAndContinue();
                break;
            case ConstantsUtils.REQUEST_CODE_SHOW_SSO_UPDATE:
                actionLoadUserInfo(userModel.id, new CustomListener2() {
                    @Override
                    public void success() {
                        actionCheckAndContinue();
                    }

                    @Override
                    public void fail(BKNetworkResponseError error) {

                    }
                });
                break;

        }

    }

    private void showUserName(CustomAnimationListener customAnimationListener) {
        tvName.setText(new StringBuilder().append(userModel.name).append(" ").append(userModel.last_name != null ? userModel.last_name : "").toString());
        findViewById(R.id.tvLoading).setVisibility(View.INVISIBLE);
        CustomAnimationUtil animationObject = new CustomAnimationUtil();
        animationObject.animation(this, true, findViewById(R.id.view_welcome_back), R.anim.bt_slide_in_up, 0, 1000, customAnimationListener);
    }

    private void actionUpdateUserTermCondition(final CustomListener2 customListener ) {
        LoadingViewUtils.showOrHideProgressBar(true, this);
        final JSONObject json = new JSONObject();
        try {
            json.put("checked_privacy_policy", true);
            json.put("checked_report_consent", true);
            json.put("checked_terms_condition", true);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        VolleyUtils.getSharedNetwork().updateUserInfo(this,KZAccount.getSharedAccount().getUser().id+"", json, new OnResponseModel() {
            @Override
            public void onResponseSuccess(Object model) {
                LoadingViewUtils.showOrHideProgressBar(false, ActivityWelcome.this);

                JsonObject jsonObject = (JsonObject) model;
                UserModel user = ParseJSonUtil.parseByModel(jsonObject.toString(), UserModel.class);
                saveUser(user);

                if(customListener!=null) customListener.success();
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                LoadingViewUtils.showOrHideProgressBar(false, ActivityWelcome.this);
                if(customListener!=null) customListener.fail(error);
            }
        });
    }

    private void openScreenForPrivacyPolicy() {
        Intent intent = new Intent(this, Agreements2Activity.class);
        intent.putExtra(Agreements2Activity.INTENT_EXTRA_ASK_TO_CHECK, true);
        startActivityForResult(intent, ConstantsUtils.REQUEST_CODE_ACCEPT);
    }

    private void actionLoadLanding2() {
        VolleyUtils.getSharedNetwork().loadLandingData2(this, new OnResponseModel<KZLandingData>() {
            @Override
            public void onResponseSuccess(KZLandingData model) {
                CommonUtils.segmentIdentifyWithUser(ActivityWelcome.this, model.user);
                userModel = model.user;
                saveUser(userModel);

                Analytics.with(ActivityWelcome.this).screen("Welcome");

                KZAccount.getSharedAccount().setCompany(model.company);
                KZAccount.getSharedAccount().setDateTime(model.datetime);

                actionLoadUserInfo(model.user.id, new CustomListener2() {
                    @Override
                    public void success() {
                        actionLoadPayment(new CustomListener2() {
                            @Override
                            public void success() {
                                showUserName(new CustomAnimationListener() {
                                    @Override
                                    public void onAnimationEnd() {
                                        actionCheckAndContinue();
                                    }
                                });
                            }

                            @Override
                            public void fail(BKNetworkResponseError error) {
                                actionCheckAndContinue();
                            }
                        });
                    }

                    @Override
                    public void fail(BKNetworkResponseError error) {

                    }
                });
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                Intent intent = new Intent(getThis(), LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    private void actionLoadUserInfo(final int userId, final CustomListener2 listener) {

        VolleyUtils.getSharedNetwork().loadUserInfo(this,"" + userId, new OnResponseModel() {
            @Override
            public void onResponseSuccess(Object model) {
                JsonObject jsonObject = (JsonObject) model;
                userModel = ParseJSonUtil.parseByModel(jsonObject.toString(), UserModel.class);
                saveUser(userModel);

                isCheckAgreement = userModel.checked_terms_condition.equalsIgnoreCase("true") && userModel.checked_privacy_policy.equalsIgnoreCase("true") && userModel.checked_report_consent.equalsIgnoreCase("true");

                if (userModel == null) {
                    Toast.makeText(ActivityWelcome.this, "User model is null", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(listener!=null) listener.success();
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                VolleyUtils.handleErrorRespond(getThis(), error);
                if(listener!=null) listener.fail(error);
            }
        });
    }

    private void actionLoadPayment(final CustomListener2 listener) {
        VolleyUtils.getSharedNetwork().loadPaymentMethodDriver(this,
                new OnResponseModel<KZPaymentDetail>() {
            @Override
            public void onResponseSuccess(KZPaymentDetail model) {
                kzPayment = model;

                if(listener!=null) listener.success();
            }

            @Override
            public void onResponseError(final BKNetworkResponseError error) {
                if(listener!=null) listener.fail(error);
            }
        });
    }

    private void actionCheckAndContinue() {
        //if(!checkIfShowSSOUpdate()) return;
        if(!checkAgreement()) return;
        if(!checkDriverLicence()) return;
        if(!checkPayment()) return;
        if(!checkIfShowFinishSignUpScreen()) return;

        CommonUtils.uploadDeviceId(getThis(), new CustomListener2() {
            @Override
            public void success() {
                Intent intent = new Intent(ActivityWelcome.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }

            @Override
            public void fail(BKNetworkResponseError error) {
                Intent intent = new Intent(ActivityWelcome.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        });


    }

    private boolean checkAgreement() {
        if(ConfigUtil.checkIfNeedTermOfService() && !isCheckAgreement) {
            openScreenForPrivacyPolicy();
            return false;
        }
        return true;
    }

    private boolean checkIfShowFinishSignUpScreen() {
        if(showFinishSignUp){
            Intent intent = new Intent(getThis(), FinishSignUpActivity.class);
            startActivityForResult(intent, ConstantsUtils.REQUEST_CODE_FINISH_SIGN_UP_SCREEN);
            return false;
        }
        return true;
    }

    private boolean checkDriverLicence() {
        if(ConfigUtil.checkIfNeedCheckDriverLicense()){
            UserAssetsModel userAssetsModel = userModel.getAssets();
            if (userAssetsModel != null) {
                final String avatar = userAssetsModel.avatar;
                final String license = userAssetsModel.license;
                final String license_after = userAssetsModel.license_after;


                boolean hasThreePicture = avatar!=null && license!=null && license_after!=null;

                Intent intent;
                if(hasThreePicture){
                    return true;

                }else {
                    //If user has not upload 3 license pictures, move to Driver's license picture screen
                    intent = new Intent(getThis(), DriverLicenseActivity.class);
                    intent.putExtra(ConstantsUtils.INTENT_EXTRA_OPEN_FROM_REGISTERING, true);
                    startActivityForResult(intent, ConstantsUtils.REQUEST_CODE_ADD_DRIVER_LICENSE);
                    overridePendingTransition(R.anim.fade_in_normal, R.anim.fade_out_normal);
                    return false;
                }
            }
        }else {
            return true;
        }
        return false;
    }

    private boolean checkPayment(){
        if(ConfigUtil.checkIfNeedCheckPaymentMethod()){
            if(skipPaymentInRegister){
                return true;
            }
            if(kzPayment != null) {
                if(!kzPayment.getPaymentList().isEmpty()){
                    return true;

                }else {
                    Intent intent = new Intent(getThis(), AddPaymentActivity.class);
                    intent.putExtra(ConstantsUtils.INTENT_EXTRA_OPEN_PAYMENT_ACTIVITY_IN_REGISTERING, true);
                    startActivityForResult(intent, ConstantsUtils.REQUEST_CODE_SELECT_OR_ADD_PAYMENT_METHOD);
                    overridePendingTransition(R.anim.fade_in_normal, R.anim.fade_out_normal);
                    return false;
                }
            }else {
                Intent intent = new Intent(getThis(), AddPaymentActivity.class);
                intent.putExtra(ConstantsUtils.INTENT_EXTRA_OPEN_PAYMENT_ACTIVITY_IN_REGISTERING, true);
                startActivityForResult(intent, ConstantsUtils.REQUEST_CODE_SELECT_OR_ADD_PAYMENT_METHOD);
                overridePendingTransition(R.anim.fade_in_normal, R.anim.fade_out_normal);
                return false;
            }
        }else {
            return true;
        }
    }

    /*private void actionAfterLoadLogoFail() {
        Toast.makeText(ActivityWelcome.this,"Fail to load logo", Toast.LENGTH_SHORT).show();
        CustomAnimationUtil animationObject = new CustomAnimationUtil();
        animationObject.animation(ActivityWelcome.this, true, logoImv, R.anim.fade_in_animation, 0, 1000, new CustomAnimationListener() {
            @Override
            public void onAnimationEnd() {
                actionLoadPayment(new CustomListener2() {
                    @Override
                    public void success() {
                        showUserName(new CustomAnimationListener() {
                            @Override
                            public void onAnimationEnd() {
                                actionCheckAndContinue();
                            }
                        });
                    }

                    @Override
                    public void fail(BKNetworkResponseError error) {
                        actionCheckAndContinue();
                    }
                });

            }
        });
    }*/

    /*private void actionAfterLoadLogoSuccess() {
        final String imagePath = imageFolderPath + File.separator + "license";
        ImageUtils.showImageFromPath2(logoImv, logoImv, imagePath);

        CustomAnimationUtil animationObject = new CustomAnimationUtil();
        animationObject.toggleAnimation(ActivityWelcome.this, 1, logoImv_default, logoImv, new CustomAnimationListener() {
            @Override
            public void onAnimationEnd() {
                actionLoadPayment(new CustomListener2() {
                    @Override
                    public void success() {
                        showUserName(new CustomAnimationListener() {
                            @Override
                            public void onAnimationEnd() {
                                actionCheckAndContinue();
                            }
                        });
                    }

                    @Override
                    public void fail(BKNetworkResponseError error) {
                        actionCheckAndContinue();
                    }
                });

            }
        });
    }*/

    private boolean checkIfShowSSOUpdate() {
        if(userModel.is_new){
            Intent intent = new Intent(this, SSOUpdateUserInfoActivity.class);
            startActivityForResult(intent, ConstantsUtils.REQUEST_CODE_SHOW_SSO_UPDATE);
            return false;
        }
        return true;
    }

}
