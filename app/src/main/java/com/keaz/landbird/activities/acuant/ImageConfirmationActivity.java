package com.keaz.landbird.activities.acuant;

/**
 * Created by tapasbehera on 5/9/16.
 */

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.acuant.mobilesdk.CardType;
import com.keaz.landbird.R;
import com.keaz.landbird.helpers.DriverLicenseActivityHelper;
import com.keaz.landbird.utils.TempImageStore;


/**
 * Created by tapasbehera on 5/9/16.
 */


public class ImageConfirmationActivity extends Activity {
    Bitmap image;
    ImageView cropImageViewer;
    TextView messageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /*if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
            setContentView(R.layout.image_confirmation_landscape);
        }else {
            setContentView(R.layout.image_confirmation);
        }*/
        setContentView(R.layout.image_confirmation);
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;

        findViewById(R.id.buttonTryAnotherWay).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                actionPressCancelButton();
            }
        });

        findViewById(R.id.cancelButtonLayout).setVisibility(View.GONE);

        cropImageViewer = findViewById(R.id.cropImage);
        messageView = findViewById(R.id.messageTextView);
        image = TempImageStore.getBitmapImage();
        if(TempImageStore.isCroppingPassed()) {
            cropImageViewer.setImageBitmap(image);

            ImageView titleImg = findViewById(R.id.titleImg);
            titleImg.setVisibility(View.GONE);

        }else{
            findViewById(R.id.buttonConfirm).setVisibility(View.GONE);
            if(DriverLicenseActivityHelper.checkIfDecideToSwitchToTakePicture(this, null, null)){
                findViewById(R.id.cancelButtonLayout).setVisibility(View.VISIBLE);
            }
            cropImageViewer.getLayoutParams().height = height*5/10;
            cropImageViewer.getLayoutParams().width = width*8/10;
            cropImageViewer.setImageResource(R.drawable.help_screen_tip_1);

            ImageView titleImg = findViewById(R.id.titleImg);
            titleImg.setVisibility(View.VISIBLE);
            titleImg.getLayoutParams().width = width*8/10;
            titleImg.setImageResource(R.drawable.help_screen_tip_title_1);

        }
        messageView.setText(getMessage());
    }
    @Override
    protected void onPause() {
        super.onPause();
    }
    @Override
    protected void onStop() {
        super.onStop();
        image = null;
        cropImageViewer = null;
        messageView = null;
        System.runFinalization();
        Runtime.getRuntime().gc();
        System.gc();

    }

    public void clearImage(){
        if(image!=null){
            image.recycle();
            image = null;
        }
    }

    public void retryButtonPressed(View v) {
        clearImage();
        System.gc();
        System.runFinalization();
        if(TempImageStore.getImageConfirmationListener()!=null) {
            TempImageStore.getImageConfirmationListener().retry();
        }
        finish();
    }

    public void confirmButtonPressed(View v) {
        if(TempImageStore.getImageConfirmationListener()!=null) {
            TempImageStore.getImageConfirmationListener().confirmed();
        }
        finish();
    }

    private void actionPressCancelButton() {
        clearImage();
        System.gc();
        System.runFinalization();
        if(TempImageStore.getImageConfirmationListener()!=null) {
            TempImageStore.getImageConfirmationListener().tryNewWay();
        }
        finish();
    }

    public String getMessage(){
        String retString = "Please make sure all the text on the ID image is readable,otherwise retry.";
        if(TempImageStore.isCroppingPassed()){
            if(TempImageStore.getCardType()== CardType.DRIVERS_LICENSE){
                retString = "Please make sure all the text on the ID image is readable,otherwise retry.";
            }else if(TempImageStore.getCardType()== CardType.PASSPORT){
                retString = "Please make sure all the text on the Passport image is readable,otherwise retry.";
            }else if(TempImageStore.getCardType()== CardType.MEDICAL_INSURANCE){
                retString = "Please make sure all the text on the Insurance Card image is readable,otherwise retry.";
            }

        }else{
            if(TempImageStore.getCardType()== CardType.DRIVERS_LICENSE){
                retString = "Unable to detect ID, please retry.";
            }else if(TempImageStore.getCardType()== CardType.PASSPORT){
                retString = "Unable to detect Passport, please retry.";
            }else if(TempImageStore.getCardType()== CardType.MEDICAL_INSURANCE){
                retString = "Unable to detect Insurance Card, please retry.";
            }
        }
        return retString;
    }

    public static boolean isTabletDevice(Context context) {
        return (context.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK)
                >= Configuration.SCREENLAYOUT_SIZE_LARGE;
    }

}
