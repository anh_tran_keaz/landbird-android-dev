package com.keaz.landbird.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.keaz.landbird.R;
import com.keaz.landbird.adapters.BranchAdapter;
import com.keaz.landbird.models.KZAccount;
import com.keaz.landbird.models.KZBranch;
import com.keaz.landbird.utils.BKNetworkResponseError;
import com.keaz.landbird.utils.LoadingViewUtils;
import com.keaz.landbird.utils.OnResponseModel;
import com.keaz.landbird.utils.ToastUtil;
import com.keaz.landbird.utils.ValidateUtils;
import com.keaz.landbird.utils.VolleyUtils;
import com.keaz.landbird.view.CustomEditText;
import com.segment.analytics.Analytics;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

public class AddAGuestActivity extends BaseActivity implements AdapterView.OnItemSelectedListener {

    private Spinner spinLocation;
    private ImageView imgLocation;
    private CustomEditText etName, etEmail;
    private BranchAdapter spinAdapter;
    private int branchID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_a_guest);

        initUI();
        initData();
        initListener();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Analytics.with(this).screen("Add A Guest");
    }


    public void initUI() {
        spinLocation = (Spinner) findViewById(R.id.spin_location);
        imgLocation = (ImageView) findViewById(R.id.img_location);

        etName = (CustomEditText) findViewById(R.id.et_guest_name);
        etEmail = (CustomEditText) findViewById(R.id.et_guest_email);
    }

    public void initData() {

        String userId = String.valueOf(KZAccount.getSharedAccount().user.id);
        LoadingViewUtils.showOrHideProgressBar(true, this);
        VolleyUtils.getSharedNetwork().loadBranches(this, new OnResponseModel<KZBranch[]>() {
            @Override
            public void onResponseSuccess(KZBranch[] model) {
                LoadingViewUtils.showOrHideProgressBar(false, AddAGuestActivity.this);

                KZAccount.getSharedAccount().setBranches(model);
                List<KZBranch> branchList = Arrays.asList(model);
                spinAdapter = new BranchAdapter(AddAGuestActivity.this, branchList);
                spinLocation.setAdapter(spinAdapter);
                spinLocation.setSaveEnabled(false);

            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                VolleyUtils.handleErrorRespond(AddAGuestActivity.this, error);
                LoadingViewUtils.showOrHideProgressBar(false, AddAGuestActivity.this);
            }
        });
    }

    public void initListener() {
        spinLocation.setOnItemSelectedListener(this);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        spinLocation.setSelection(position);
        branchID = spinAdapter.getItem(position).id;
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public void onDropDown(View view) {

        spinLocation.performClick();

    }

    public void onHeaderBackPress(View view) {
        onBackPressed();
    }

    public void onSubmitClick(View view) {
        Analytics.with(this).track("Click add_a_guest_btn");

        if(branchID==0) {
            Toast.makeText(this,"Please select a branch.", Toast.LENGTH_SHORT).show();
            return;
        }

        if(etName.getText().toString().isEmpty()){
            Toast.makeText(this,"Please enter a name.", Toast.LENGTH_SHORT).show();
            return;
        }

        if(!ValidateUtils.validateEmail(this, etEmail.getText().toString())){
            Toast.makeText(this,"The email is invalid. Please try again.", Toast.LENGTH_SHORT).show();
            return;
        }

        LoadingViewUtils.showOrHideProgressBar(true, this);
        JSONObject js = new JSONObject();
        try {
            js.put("email", etEmail.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        VolleyUtils.getSharedNetwork().addAGuest(this, branchID, js, new OnResponseModel() {
            @Override
            public void onResponseSuccess(Object model) {
                LoadingViewUtils.showOrHideProgressBar(false, AddAGuestActivity.this);
                ToastUtil.showToastMessage(AddAGuestActivity.this, "Success", false);
                finish();
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                VolleyUtils.handleErrorRespond(AddAGuestActivity.this, error);
                LoadingViewUtils.showOrHideProgressBar(false, AddAGuestActivity.this);
            }
        });
    }
}
