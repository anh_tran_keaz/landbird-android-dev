package com.keaz.landbird.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.keaz.landbird.R;
import com.keaz.landbird.models.KZNotes;
import com.keaz.landbird.utils.BKNetworkResponseError;
import com.keaz.landbird.utils.OnResponseModel;
import com.keaz.landbird.utils.VolleyUtils;
import com.keaz.landbird.view.CustomEditText;

public class ContactUsActivity extends BaseActivity {


    private CustomEditText etMessage;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);

        etMessage = (CustomEditText) findViewById(R.id.et_message);

    }


    public void onSubmitClick(View view) {
        String message = etMessage.getText().toString();

        if (message == null || message.equals("")) {
            Toast.makeText(this, getString(R.string.error_empty_message), Toast.LENGTH_SHORT).show();
            return;
        }

        showCustomDialogLoading();
        VolleyUtils.getSharedNetwork().postContactUs(this, etMessage.getText().toString(), new OnResponseModel<KZNotes>() {
            @Override
            public void onResponseSuccess(KZNotes model) {
                dismissCustomDialogLoading();
                finish();
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                VolleyUtils.handleErrorRespond(ContactUsActivity.this, error);
                dismissCustomDialogLoading();
            }
        });
    }

    public void onHeaderBackPress(View view) {
        onBackPressed();
    }
}
