package com.keaz.landbird.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.keaz.landbird.R;
import com.keaz.landbird.dialogs.ConfirmLogoutDialog;
import com.keaz.landbird.models.KZAccount;
import com.keaz.landbird.utils.BKGlobals;
import com.keaz.landbird.utils.BKNetworkResponseError;
import com.keaz.landbird.utils.ConstantsUtils;
import com.keaz.landbird.utils.LoadingViewUtils;
import com.keaz.landbird.utils.OnResponseModel;
import com.keaz.landbird.utils.VolleyUtils;
import com.keaz.landbird.view.CustomFontTextView;
import com.segment.analytics.Analytics;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class AccountActivity extends BaseActivity {


    public static final int BENEFITS_ITEM = 0;
    public static final int REFER_A_FRIEND_ITEM = 1;
    public static final int CHANGE_PASSWORD_ITEM = 2;
    public static final int REGISTER_KEY_CARD_ITEM = 3;
    public static final int REPORT_LOST_KEY_CARD_ITEM = 4;
    public static final int AGREEMENTS_ITEM = 5;
    public static final int GET_SOCIAL_ITEM = 6;
    public static final int MEMBERSHIPS = 7;
    public static final int PROMO_CODE = 8;

    private ListView listView;
    private SimpleArrayAdapter adapter;
    private TextView mTxtAccount;
    private String[] dataList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);

        initUI();
        initData();
        initListener();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Analytics.with(this).screen("Profile");
    }


    public void initUI() {
        listView = (ListView) findViewById(R.id.lv_account);
        mTxtAccount = (TextView) findViewById(R.id.txt_account_email_and_phone);
    }

    public void initData() {
        List<String> temp = new ArrayList<>();
        temp.add("Benefits");
        temp.add("Refer A Friend");
        temp.add("Change Password");
        temp.add("Link your GO Pass");
        temp.add("Report Lost GO pass");
        temp.add("Agreements");
        temp.add("Get Social");
        temp.add("Memberships");
        temp.add("Promo code");

        String email = KZAccount.getSharedAccount().user.email;
        String phone = KZAccount.getSharedAccount().user.phone;
        String message = "";
        if (email != null && !email.equals("")) {
            message += email;
            if (phone != null && !phone.equals("")) {
                message += " ("+ phone +")";
            }
        } else if (phone != null && !phone.equals("")) {
            message += phone;
            if (email != null && !email.equals("")) {
                message = " ("+ email +")";
            }
        }
        mTxtAccount.setText(message);

        dataList = new String[temp.size()];
        dataList = temp.toArray(dataList);

        adapter = new SimpleArrayAdapter(this, dataList);

        listView.setAdapter(adapter);
    }

    public void initListener() {
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                switch (position) {
                    case BENEFITS_ITEM:
                        Analytics.with(AccountActivity.this).track("Click Benefits button");
                        startActivityWithName(BenefitListActivity.class);
                        break;

                    case REFER_A_FRIEND_ITEM:
                        break;

                    case CHANGE_PASSWORD_ITEM:
                        break;

                    case REGISTER_KEY_CARD_ITEM:
                        Analytics.with(AccountActivity.this).track("Click Link your GO Pass button");
                        startActivityWithName(RegisterCardActivity.class);
                        break;

                    case REPORT_LOST_KEY_CARD_ITEM:
                        break;

                    case AGREEMENTS_ITEM:
                        Analytics.with(AccountActivity.this).track("Click Agreements button");
                        startActivityWithName(AgreementsActivity.class);
                        break;

                    case GET_SOCIAL_ITEM:
                        break;

                    case MEMBERSHIPS:
                        Analytics.with(AccountActivity.this).track("Click Memberships button");
                        startActivityWithName(MembershipListActivity.class);
                        break;

                    case PROMO_CODE:
                        Analytics.with(AccountActivity.this).track("Click Promo Code button");
                        startActivityWithName(PromoCodeActivity.class);
                        break;

                    default:
                        break;
                }
            }
        });
    }

    public void onHeaderBackPress(View view) {
        onBackPressed();
    }

    public void onLogOutClick(View view) {
        Analytics.with(this).track("Logout");

        final ConfirmLogoutDialog dialog = new ConfirmLogoutDialog(this);
        dialog.setOnConfirmLogoutListener(new ConfirmLogoutDialog.OnConfirmLogoutListener() {
            @Override
            public void onOk() {
                LoadingViewUtils.showOrHideProgressBar(true, AccountActivity.this);
                VolleyUtils.getSharedNetwork().logout(AccountActivity.this,
                        new OnResponseModel<Object>() {
                    @Override
                    public void onResponseSuccess(Object model) {
                        LoadingViewUtils.showOrHideProgressBar(false, AccountActivity.this);

                        BKGlobals.getSharedGlobals().saveStringPreferences(BKGlobals.ACCESS_TOKEN, null);
                        BKGlobals.getSharedGlobals().saveStringPreferences(ConstantsUtils.PREF_KEY_EMAIL, null);
                        BKGlobals.getSharedGlobals().saveStringPreferences(ConstantsUtils.PREF_KEY_CONTACT_NUMBER, null);
                        BKGlobals.getSharedGlobals().saveStringPreferences(ConstantsUtils.PREF_KEY_COUNTRY_CODE, null);
                        BKGlobals.getSharedGlobals().saveStringPreferences(ConstantsUtils.PREF_KEY_PASSWORD, null);
                        Intent intent = new Intent(AccountActivity.this, LoginActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();
                    }

                    @Override
                    public void onResponseError(BKNetworkResponseError error) {
                        LoadingViewUtils.showOrHideProgressBar(false, AccountActivity.this);
                        VolleyUtils.handleErrorRespond(AccountActivity.this, error);
                    }
                });
            }

            @Override
            public void onCancel() {

            }
        });
        dialog.show();
    }

    public void startActivityWithName(Class<?> tClass) {
        startActivity(new Intent(this, tClass));
    }

    private static class SimpleArrayAdapter extends ArrayAdapter<String> {
        private final Context context;
        private final String[] values;

        SimpleArrayAdapter(Context context, String[] values) {
            super(context, -1, values);
            this.context = context;
            this.values = values;
        }

        @Override
        public int getCount() {
            return values != null ? values.length : 0;
        }

        @NotNull
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.item_account_list, parent, false);

            CustomFontTextView textView = (CustomFontTextView) rowView.findViewById(R.id.tv_title);

            textView.setText(values[position]);

            return rowView;
        }
    }
}
