package com.keaz.landbird.helpers;

import android.content.Context;
import android.util.Log;

import com.keaz.landbird.interfaces.CustomActionListener;

public class DriverLicenseActivityHelper {
    private static final String TAG = DriverLicenseActivityHelper.class.getSimpleName();
    private static final int FAIL_LIMIT = 2;
    private static int failToCropCount;
    private static int failToGetBarCodeCount;
    private static int failWithGeneralReasonCount;
    private static int failToGetImageFromFrontSide;
    private static int failtToGetInformationFromCardCount;
    private static int failCount;

    public static boolean checkIfDecideToSwitchToTakePicture(Context context, CustomActionListener customActionListener1, CustomActionListener customActionListener2) {
        boolean rs = failToCropCount>=FAIL_LIMIT
                || failToGetBarCodeCount>=FAIL_LIMIT
                || failWithGeneralReasonCount>=FAIL_LIMIT
                || failToGetImageFromFrontSide>=FAIL_LIMIT
                ;

        rs = failCount>=2;

        if(rs){
            if(customActionListener1!=null) customActionListener1.takeAction();
        }else {
            if(customActionListener2!=null) customActionListener2.takeAction();
        }

        return rs;
    }

    public static void actionRecordAErrorInAccuantProcess(Context context, String calledMethod, String errorDescription) {
        Log.d(TAG, calledMethod);
        Log.d(TAG, errorDescription);

        failCount++;

        if(calledMethod.equals("onCardCroppingFinish")){
            failToCropCount++;
        }
        if(calledMethod.equals("onBarcodeTimeOut")){
            failToGetBarCodeCount++;
        }
        if(calledMethod.equals("didFailWithError")){
            failWithGeneralReasonCount++;
        }
        if(calledMethod.equals("actionGetImageAndStartToPresentCameraForBackSide")){
            failToGetImageFromFrontSide++;
        }
        if(calledMethod.equals("updateLicenseInfo")){
            failtToGetInformationFromCardCount++;
        }
    }

    public static void resetFailCount() {
        failCount=0;
    }
}
