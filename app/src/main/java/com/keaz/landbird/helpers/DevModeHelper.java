package com.keaz.landbird.helpers;

import android.content.Context;

public class DevModeHelper {
    private static final String PREF_KEY_DEV_MODE = "pref key dev mode";
    public static boolean isTesting = false;

    public static boolean isEnabled(Context context) {
        return SharedPreferencesUtils.getBoolean(context, PREF_KEY_DEV_MODE, false);
    }
}
