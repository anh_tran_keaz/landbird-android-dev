package com.keaz.landbird.helpers;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class SharedPreferencesUtils {
    public static void setIntPrefValue(String preKey, int value, Context context) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        sharedPref.edit().putInt(preKey, value).apply();
    }

    public static void setBooleanPrefValue(String prefKey, boolean value, Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        sharedPreferences.edit().putBoolean(prefKey,value).apply();
    }

    public static void setStringPrefValue(String preKey, String value, Context context) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        sharedPref.edit().putString(preKey, value).apply();
    }

    public static String getStringPrefValue(String prefKey, Context context, String defaultValue) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getString(prefKey,defaultValue);
    }

    public static boolean getBooleanPrefValue(String prefKey, Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getBoolean(prefKey,false);
    }

    public static boolean getBooleanPrefValue(String prefKey, Context context, boolean defaultVal) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getBoolean(prefKey,defaultVal);
    }

    public static int getIntPrefValue(String prefKey, Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getInt(prefKey,0);
    }

    public static int getIntPrefValue(String prefKey, Context context, int defaultVal) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getInt(prefKey,defaultVal);
    }

    public static boolean getBoolean(Context context, String key, boolean defVal) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getBoolean(key, defVal);
    }

    public static void setBoolean(Context context, String key, boolean val) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        sharedPreferences.edit().putBoolean(key, val).apply();
    }

    public static String getString(Context context, String key, String defVal) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getString(key, defVal);
    }

    public static void setString(Context context, String key, String val) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        sharedPreferences.edit().putString(key, val).apply();
    }

    public static void setString2(String val, Context context, String key) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        sharedPreferences.edit().putString(key, val).apply();
    }

    public static void setLong(Context context, long value, String key) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        sharedPreferences.edit().putFloat(key,value).apply();
    }

    public static long getLong(Context context, String key, long defaultValue) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        float rs = sharedPreferences.getFloat(key,defaultValue);
        return (long) rs;
    }

    public static int getInt(Context context, String key, int defaultVal) {
        int rs;
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        rs = sharedPreferences.getInt(key, defaultVal);
        return rs;
    }

    public static int getInt2(String key, Context context, int defaultVal) {
        int rs;
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        rs = sharedPreferences.getInt(key, defaultVal);
        return rs;
    }

    public static void setInt(Context context, String key, int val) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        sharedPreferences.edit().putInt(key, val).apply();
    }
}
