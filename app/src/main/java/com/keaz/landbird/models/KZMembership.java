package com.keaz.landbird.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by anhtran1810 on 1/12/18.
 */

public class KZMembership implements Serializable {

    @SerializedName("id")
    public String id;

    @SerializedName("name")
    public String name;

    @SerializedName("description")
    public String description;

    @SerializedName("fee_cost")
    public String fee_cost;

    @SerializedName("fee_cost_type")
    public String fee_cost_type;

    @SerializedName("vehicle_cost")
    public String vehicle_cost;

    @SerializedName("vehicle_cost_type")
    public String vehicle_cost_type;

    @SerializedName("vehicle_private_cost")
    public String vehicle_private_cost;

    @SerializedName("vehicle_private_cost_type")
    public String vehicle_private_cost_type;

    public boolean isApply;

    @SerializedName("discount_cost")
    public String discount_cost;

    @SerializedName("discount_cost_type")
    public String discount_cost_type;

    @SerializedName("discount_private_cost_type")
    public String discount_private_cost_type;

    @SerializedName("discount_private_cost")
    public String discount_private_cost;
}
