package com.keaz.landbird.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Harradine on 11/15/2014.
 */
public class KZFeatures {
    @SerializedName("id")
    public int id;

    @SerializedName("location")
    public String location;

    @SerializedName("name")
    public String name;

    @SerializedName("slug")
    public String slug;
}
