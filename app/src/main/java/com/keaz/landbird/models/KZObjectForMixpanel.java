package com.keaz.landbird.models;

import com.google.gson.annotations.SerializedName;
import com.keaz.landbird.utils.BKNetworkResponse;

public class KZObjectForMixpanel extends BKNetworkResponse {

    @SerializedName("user_id")
    public String userId;

    @SerializedName("first_name")
    public String firstName;

    @SerializedName("last_name")
    public String lastName;

    @SerializedName("total_charge")
    public String totalCharge;

    @SerializedName("total")
    public String total;

    @SerializedName("last_complete_booking")
    public KZBooking lastCompleteBooking;
}
