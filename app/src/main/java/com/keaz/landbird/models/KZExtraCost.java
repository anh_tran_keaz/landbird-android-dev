package com.keaz.landbird.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class KZExtraCost implements Parcelable {

    public static final Creator<KZExtraCost> CREATOR = new Creator<KZExtraCost>() {
        @Override
        public KZExtraCost createFromParcel(Parcel in) {
            return new KZExtraCost(in);
        }

        @Override
        public KZExtraCost[] newArray(int size) {
            return new KZExtraCost[size];
        }
    };

    @SerializedName("text")
    public String text;

    @SerializedName("cost")
    public String cost;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(final Parcel dest,
                              final int flags) {
        dest.writeString(text);
        dest.writeString(cost);
    }

    protected KZExtraCost(Parcel in) {
        text = in.readString();
        cost = in.readString();
    }

    public KZExtraCost(){};
}
