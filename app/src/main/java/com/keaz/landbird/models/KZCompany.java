package com.keaz.landbird.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Harradine on 11/15/2014.
 */
public class KZCompany {
    @SerializedName("id")
    public int id;

    @SerializedName("name")
    public String name;

    @SerializedName("slug")
    public String slug;

    @SerializedName("assets")
    public KZAssets assets;

    @SerializedName("enable_recurring")
    public Boolean enable_recurring;

    @SerializedName("enable_node")
    public Boolean enable_node;

    @SerializedName("enable_usage_policy")
    public Boolean enable_usage_policy;

    @SerializedName("vehicle_private_cost_max")
    public String vehicle_private_cost_max;

    @SerializedName("booking_max_duration")
    public String booking_max_duration;

    @SerializedName("booking_max_duration_type")
    public String booking_max_duration_type;

    @SerializedName("enable_datetime_format")
    public String enable_datetime_format;

    @SerializedName("enable_tandem_booking")
    public String enable_tandem_booking;

    @SerializedName("enable_change_driver")
    public String enable_change_driver;

    @SerializedName("company_timezone_offset")
    public int company_timezone_offset;

    @SerializedName("enable_fixed_timezone")
    public boolean enable_fixed_timezone;

    @SerializedName("enable_branch_timezone")
    public boolean enable_branch_timezone;

    @SerializedName("company_booking_block_duration")
    public long company_booking_block_duration;

    @SerializedName("durations")
    public KZDurations durations;

    @SerializedName("page_item")
    public int page_item;

    @SerializedName("limit_booking_type")
    public String limit_booking_type;

    @SerializedName("trip_types")
    public Object trip_types;

    @SerializedName("enable_charge_business")
    public boolean enable_charge_business;

    @SerializedName("enable_charge_private")
    public boolean enable_charge_private;

    @SerializedName("require_payment_business")
    public String require_payment_business;

    @SerializedName("require_payment_private")
    public String require_payment_private;

    @SerializedName("booking_business_charge_to")
    public String booking_business_charge_to;

    @SerializedName("enable_warning_rfid")
    public boolean enable_warning_rfid;

    @SerializedName("booking_private_charge_to")
    public String booking_private_charge_to;

    @SerializedName("booking_checkout")
    public String booking_checkout;

    @SerializedName("company_booking_min_duration")
    public String company_booking_min_duration;

    @SerializedName("start_booking_early_time")
    public long start_booking_early_time;

    @SerializedName("enable_first_payment")
    public boolean enable_first_payment;

    @SerializedName("enable_first_profile")
    public boolean enable_first_profile;

    @SerializedName("enable_zendesk")
    public boolean enable_zendesk;

    @SerializedName("enable_agreements_register")
    public boolean enable_agreements_register;

    @SerializedName("enable_estimate_cost_list")
    public boolean enable_estimate_cost_list;

    @SerializedName("enable_usage_policy_onetime")
    public boolean enable_usage_policy_onetime;

    @SerializedName("date_time_format")
    public KZDateTimeFormat date_time_format;

    @SerializedName("contact_number")
    public String contact_number;

    @SerializedName("contact_email")
    public String contact_email;

    @SerializedName("enable_membership")
    public boolean enable_membership;

    @SerializedName("enable_promotion")
    public boolean enable_promotion;

    @SerializedName("enable_membership_is_not_discount")
    public boolean enable_membership_is_not_discount;

    @SerializedName("faq")
    public String faq;

    @SerializedName("company_booking_block_mobile_app_parsed")
    public ArrayList<BlockModal> company_booking_block_mobile_app_parsed;

    @SerializedName("booking_checkin")
    public String booking_checkin;

    @SerializedName("enable_geofence_distance")
    public String enable_geofence_distance;

    @SerializedName("extend_booking_distance")
    public String extend_booking_distance;

    @SerializedName("enable_override_gps")
    public String enable_override_gps;

    @SerializedName("enable_drop_off")
    public boolean enable_drop_off;

    @SerializedName("accident_assistance_number")
    public String accident_assistance_number;

    @SerializedName("roadside_assistance_number")
    public String roadside_assistance_number;

    @SerializedName("stripe_public_key")
    public String stripe_public_key;

    @SerializedName("enable_concierge")
    public boolean enable_concierge;

    @SerializedName("concierge_option")
    public String concierge_option;

    @SerializedName("enable_include_gst")
    public boolean enable_include_gst;

    @SerializedName("booking_cost_gst")
    public double booking_cost_gst;

    @SerializedName("state")
    public String state;

    @SerializedName("enable_addons")
    public boolean enable_addons;
}
