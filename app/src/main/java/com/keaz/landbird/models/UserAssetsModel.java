package com.keaz.landbird.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mac on 2/23/17.
 */
public class UserAssetsModel {
    @SerializedName("avatar")
    public String avatar;

    @SerializedName("license")
    public String license;

    @SerializedName("license_after")
    public String license_after;
}
