package com.keaz.landbird.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by anhtran1810 on 1/12/18.
 */

public class KZBenefit implements Serializable {

    @SerializedName("name")
    public String name;

    @SerializedName("description")
    public String description;

    @SerializedName("codes")
    public String codes;

    @SerializedName("value")
    public String value;

    @SerializedName("type")
    public String type;

    @SerializedName("expired")
    public String expired;

    @SerializedName("condition")
    public String condition;
}
