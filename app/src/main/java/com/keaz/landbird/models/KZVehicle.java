package com.keaz.landbird.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Harradine on 11/16/2014.
 */
public class KZVehicle implements Parcelable {
    public static final Creator<KZVehicle> CREATOR = new Creator<KZVehicle>() {
        @Override
        public KZVehicle createFromParcel(Parcel in) {
            return new KZVehicle(in);
        }

        @Override
        public KZVehicle[] newArray(int size) {
            return new KZVehicle[size];
        }
    };
    @SerializedName("id")
    public int id;
    @SerializedName("name")
    public String name;
    @SerializedName("registration")
    public String registration;
    @SerializedName("year")
    public String year;
    @SerializedName("transmission")
    public String transmission;
    @SerializedName("seat_number")
    public int seats;
    @SerializedName("fuel_type")
    public String fuelType;
    @SerializedName("body_colour")
    public String bodyColour;
    @SerializedName("model")
    public KZVehicleModel model;
    @SerializedName("branch")
    public KZBranch branch;
    @SerializedName("assets")
    public KZAssets assets;
    @SerializedName("body")
    public KZBodyType body;
    @SerializedName("doors")
    public String door;
    @SerializedName("features")
    public ArrayList<String> features;
    @SerializedName("vehicle_cost")
    public String vehicle_cost;
    @SerializedName("vehicle_cost_type")
    public String vehicle_cost_type;
    @SerializedName("cost_currency")
    public String cost_currency;
    @SerializedName("battery")
    public String battery;
    @SerializedName("available_to_block")
    public String available_to_block;
    @SerializedName("reasons")
    public ArrayList<String> reasons;
    @SerializedName("trip_types")
    public ArrayList<String> trip_types;

    @SerializedName("vehicle_private_cost")
    public String vehicle_private_cost;
    @SerializedName("vehicle_private_cost_type")
    public String vehicle_private_cost_type;

    @SerializedName("cost_private_total")
    public double cost_private_total;
    @SerializedName("cost_business_total")
    public double cost_business_total;

    @SerializedName("cost_private_sub")
    public double cost_private_sub;
    @SerializedName("cost_private_type")
    public String cost_private_type;
    @SerializedName("cost_business_sub")
    public double cost_business_sub;
    @SerializedName("cost_business_type")
    public String cost_business_type;
    @SerializedName("daily_estimate_cost")
    public double daily_estimate_cost;
    @SerializedName("weekly_estimate_cost")
    public double weekly_estimate_cost;
    @SerializedName("monthly_estimate_cost")
    public double monthly_estimate_cost;

    @SerializedName("cost_total")
    public double cost_total;

    @SerializedName("status")
    public String status;

    @SerializedName("kms_current")
    public String kms_current;

    protected KZVehicle(Parcel in) {
        id = in.readInt();
        name = in.readString();
        registration = in.readString();
        year = in.readString();
        transmission = in.readString();
        seats = in.readInt();
        fuelType = in.readString();
        bodyColour = in.readString();
        door = in.readString();
        features = in.createStringArrayList();
        vehicle_cost = in.readString();
        vehicle_cost_type = in.readString();
        cost_currency = in.readString();
        battery = in.readString();
        available_to_block = in.readString();
        reasons = in.createStringArrayList();
        trip_types = in.createStringArrayList();
        vehicle_private_cost = in.readString();
        vehicle_private_cost_type = in.readString();
        cost_business_total = in.readDouble();
        cost_private_total = in.readDouble();
        cost_business_sub = in.readDouble();
        cost_private_sub = in.readDouble();
        cost_business_type = in.readString();
        cost_private_type = in.readString();
        daily_estimate_cost = in.readDouble();
        monthly_estimate_cost = in.readDouble();
        weekly_estimate_cost = in.readDouble();
        status = in.readString();
        cost_total = in.readDouble();
        kms_current = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(registration);
        dest.writeString(year);
        dest.writeString(transmission);
        dest.writeInt(seats);
        dest.writeString(fuelType);
        dest.writeString(bodyColour);
        dest.writeString(door);
        dest.writeStringList(features);
        dest.writeString(vehicle_private_cost);
        dest.writeString(vehicle_private_cost_type);
        dest.writeString(vehicle_cost);
        dest.writeString(vehicle_cost_type);
        dest.writeString(cost_currency);
        dest.writeString(battery);
        dest.writeString(available_to_block);
        dest.writeStringList(reasons);
        dest.writeStringList(trip_types);
        dest.writeDouble(cost_business_total);
        dest.writeDouble(cost_private_total);
        dest.writeDouble(cost_business_sub);
        dest.writeDouble(cost_private_sub);
        dest.writeString(cost_business_type);
        dest.writeString(cost_private_type);
        dest.writeDouble(daily_estimate_cost);
        dest.writeDouble(weekly_estimate_cost);
        dest.writeDouble(monthly_estimate_cost);
        dest.writeString(status);
        dest.writeDouble(cost_total);
        dest.writeString(kms_current);
    }

    public boolean containTripType(String trip_type) {
        if(trip_types.isEmpty()) return true;
        for (String s: trip_types) {
            if(s.equalsIgnoreCase(trip_type)) return true;
        }
        return false;
    }
}