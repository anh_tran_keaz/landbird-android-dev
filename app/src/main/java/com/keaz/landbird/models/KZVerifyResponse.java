package com.keaz.landbird.models;

import com.google.gson.annotations.SerializedName;
import com.keaz.landbird.utils.BKNetworkResponse;

/**
 * Created by mac on 2/21/17.
 */
public class KZVerifyResponse extends BKNetworkResponse {
    @SerializedName("token")
    public String accessToken;
}
