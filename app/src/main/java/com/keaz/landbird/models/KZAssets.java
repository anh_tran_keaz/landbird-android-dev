package com.keaz.landbird.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Harradine on 11/15/2014.
 */
public class KZAssets {
    @SerializedName("logotype")
    public String logoType;

    @SerializedName("avatar")
    public String avatar;

    @SerializedName("photo")
    public String photo;

    @SerializedName("background")
    public String background;

    @SerializedName("license")
    public String license;

    @SerializedName("license_after")
    public String license_after;


}
