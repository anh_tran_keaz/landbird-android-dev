package com.keaz.landbird.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by anhtran1810 on 1/16/18.
 */

public class KZPromo implements Serializable {

    @SerializedName("code")
    public String code;

    @SerializedName("cost")
    public String cost;

    @SerializedName("available_cost")
    public String available_cost;

    @SerializedName("expire_date")
    public String expire_date;
}
