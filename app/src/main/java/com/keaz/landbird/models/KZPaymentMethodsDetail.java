package com.keaz.landbird.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class KZPaymentMethodsDetail implements Parcelable {

    protected KZPaymentMethodsDetail(Parcel in) {
        detail_payment_methods = in.createTypedArrayList(StripePaymentModel.CREATOR);
        payment_methods = in.createStringArrayList();
        default_method = in.readString();
        customer_id = in.readString();
        method = in.createStringArrayList();
    }

    public static final Creator<KZPaymentMethodsDetail> CREATOR = new Creator<KZPaymentMethodsDetail>() {
        @Override
        public KZPaymentMethodsDetail createFromParcel(Parcel in) {
            return new KZPaymentMethodsDetail(in);
        }

        @Override
        public KZPaymentMethodsDetail[] newArray(int size) {
            return new KZPaymentMethodsDetail[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeTypedList(detail_payment_methods);
        parcel.writeStringList(payment_methods);
        parcel.writeString(default_method);
        parcel.writeString(customer_id);
        parcel.writeStringList(method);
    }

    @SerializedName("detail_default_method")
    public StripePaymentModel detail_default_method;

    @SerializedName("detail_payment_methods")
    public ArrayList<StripePaymentModel> detail_payment_methods;

    @SerializedName("payment_methods")
    public ArrayList<String> payment_methods;

    @SerializedName("default_method")
    public String default_method;

    @SerializedName("customer_id")
    public String customer_id;

    @SerializedName("method")
    public ArrayList<String> method;

    public ArrayList<StripePaymentModel> getPaymentList() {
        return detail_payment_methods;
    }
}
