package com.keaz.landbird.models;

import com.keaz.landbird.utils.MyLog;

import java.text.SimpleDateFormat;


/**
 * Created by Harradine on 11/15/2014.
 */
public class KZNetwork {
    private static final int TIMED_OUT = 60000;
    private static final String TAG = KZNetwork.class.getSimpleName();
    private static KZNetwork sharedNetwork;
    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    private SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");

    private String accessToken;
    private String subDomainHeader;

    private KZNetwork() {

    }

    public static KZNetwork getSharedNetwork() {
        if (null == sharedNetwork) {
            sharedNetwork = new KZNetwork();
        }
        return sharedNetwork;
    }

    /**
     * Append accesstoken
     *
     * @param url
     * @return
     */
    public String appendAccessToken(String url) {
        url = url.concat("?token=").concat(accessToken);
        MyLog.debug(KZNetwork.class.getSimpleName(), url);
        return url;
    }
}
