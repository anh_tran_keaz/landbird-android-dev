package com.keaz.landbird.models;

import com.google.gson.annotations.SerializedName;
import com.keaz.landbird.utils.BKNetworkResponse;

/**
 * Created by anhtran1810 on 2/27/18.
 */

public class KZConfigData extends BKNetworkResponse {

    @SerializedName("password_min_length")
    public int password_min_length;

    @SerializedName("password_max_length")
    public int password_max_length;

    @SerializedName("sso_saml")
    public SSOUrl ss_saml;

    @SerializedName("enable_agreements_register")
    public boolean enable_agreements_register;
}
