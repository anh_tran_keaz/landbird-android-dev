package com.keaz.landbird.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by anhtran1810 on 3/29/18.
 */

public class SSOUrl {

    @SerializedName("url")
    public String url;
}
