package com.keaz.landbird.models;

import com.google.gson.annotations.SerializedName;
import com.keaz.landbird.utils.BKNetworkResponse;


/**
 * Created by Harradine on 11/15/2014.
 */
public class KZLoginResponse extends BKNetworkResponse {
    @SerializedName("token")
    public String accessToken;

    @SerializedName("id")
    public String id;

    @SerializedName("email")
    public String email;

    @SerializedName("phone")
    public String phone;

//    @SerializedName("companies")
//    public ArrayList<KZCompany> companies;
//
//    @SerializedName("checked_terms_condition")
//    public boolean checked_terms_condition;
//
//    @SerializedName("checked_privacy_policy")
//    public boolean checked_privacy_policy;
//
//    @SerializedName("checked_report_consent")
//    public boolean checked_report_consent;
}
