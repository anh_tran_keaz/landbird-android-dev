package com.keaz.landbird.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by anhtran1810 on 6/14/18.
 */

public class KZDateTime {

    @SerializedName("company_timezone_offset")
    public int company_timezone_offset;

    @SerializedName("default_timezone_offset")
    public String default_timezone_offset;

    @SerializedName("user_timezone_offset")
    public int user_timezone_offset;

    @SerializedName("company_timezone_abbr")
    public String company_timezone_abbr;

    @SerializedName("company_timezone_id")
    public String company_timezone_id;

    @SerializedName("company_timezone_name")
    public String company_timezone_name;


}
