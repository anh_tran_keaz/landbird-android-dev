package com.keaz.landbird.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by anhtran1810 on 3/6/18.
 */

public class SocialLinkModel implements Serializable {

    @SerializedName("name")
    public String name;
    @SerializedName("page")
    public String page;
    @SerializedName("id")
    public String id;
    @SerializedName("photo")
    public String photo;

    public SocialLinkModel(String name, String page, String id, String photo) {
        this.name = name;
        this.page = page;
        this.id = id;
        this.photo = photo;
    }
}
