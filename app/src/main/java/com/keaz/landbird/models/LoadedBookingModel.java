package com.keaz.landbird.models;

import com.google.gson.annotations.SerializedName;
import com.keaz.landbird.utils.BKNetworkResponse;

import java.util.ArrayList;

public class LoadedBookingModel extends BKNetworkResponse {

    @SerializedName("bookings")
    public
    ArrayList<KZBooking> bookings;

    @SerializedName("total")
    double total;
}
