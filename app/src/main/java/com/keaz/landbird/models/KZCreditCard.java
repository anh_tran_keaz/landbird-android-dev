package com.keaz.landbird.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by TamTran on 22/02/2017.
 */

public class KZCreditCard extends KZPaymentMethod implements Parcelable {

    public static final Creator<KZCreditCard> CREATOR = new Creator<KZCreditCard>() {
        @Override
        public KZCreditCard createFromParcel(Parcel in) {
            return new KZCreditCard(in);
        }

        @Override
        public KZCreditCard[] newArray(int size) {
            return new KZCreditCard[size];
        }
    };
    @SerializedName("updated_at")
    @Expose
    public String updatedAt;
    @SerializedName("issuing_bank")
    @Expose
    public String issuingBank;
    @SerializedName("last_4")
    @Expose
    public String last4;
    @SerializedName("expiration_month")
    @Expose
    public String expirationMonth;
    @SerializedName("customer_location")
    @Expose
    public String customerLocation;
    @SerializedName("gateway")
    @Expose
    public String gateway;
    @SerializedName("unique_number_identifier")
    @Expose
    public String uniqueNumberIdentifier;
    @SerializedName("country_of_issuance")
    @Expose
    public String countryOfIssuance;
    @SerializedName("debit")
    @Expose
    public String debit;
    @SerializedName("customer_id")
    @Expose
    public String customerId;
    @SerializedName("_setattrs")
    @Expose
    public String setattrs;
    @SerializedName("bin")
    @Expose
    public String bin;
    @SerializedName("billing_address")
    @Expose
    public String billingAddress;
    @SerializedName("prepaid")
    @Expose
    public String prepaid;
    @SerializedName("subscriptions")
    @Expose
    public String subscriptions;
    @SerializedName("durbin_regulated")
    @Expose
    public String durbinRegulated;
    @SerializedName("commercial")
    @Expose
    public String commercial;
    @SerializedName("payroll")
    @Expose
    public String payroll;
    @SerializedName("venmo_sdk")
    @Expose
    public String venmoSdk;
    @SerializedName("cardholder_name")
    @Expose
    public String cardholderName;
    @SerializedName("verifications")
    @Expose
    public String verifications;
    @SerializedName("expired")
    @Expose
    public String expired;
    @SerializedName("product_id")
    @Expose
    public String productId;
    @SerializedName("expiration_year")
    @Expose
    public String expirationYear;
    @SerializedName("default")
    @Expose
    public String _default;
    @SerializedName("created_at")
    @Expose
    public String createdAt;
    @SerializedName("card_type")
    @Expose
    public String cardType;
    /*@SerializedName("token")
    @Expose
    public String token;*/
    @SerializedName("image_url")
    @Expose
    public String imageUrl;
    @SerializedName("healthcare")
    @Expose
    public String healthcare;
    @SerializedName("is_expired")
    @Expose
    public String isExpired;


    protected KZCreditCard(Parcel in) {
        updatedAt = in.readString();
        issuingBank = in.readString();
        last4 = in.readString();
        expirationMonth = in.readString();
        customerLocation = in.readString();
        gateway = in.readString();
        uniqueNumberIdentifier = in.readString();
        countryOfIssuance = in.readString();
        debit = in.readString();
        customerId = in.readString();
        setattrs = in.readString();
        bin = in.readString();
        billingAddress = in.readString();
        prepaid = in.readString();
        subscriptions = in.readString();
        durbinRegulated = in.readString();
        commercial = in.readString();
        payroll = in.readString();
        venmoSdk = in.readString();
        cardholderName = in.readString();
        verifications = in.readString();
        expired = in.readString();
        productId = in.readString();
        expirationYear = in.readString();
        _default = in.readString();
        createdAt = in.readString();
        cardType = in.readString();
        token = in.readString();
        imageUrl = in.readString();
        healthcare = in.readString();
        isExpired = in.readString();
    }

    public static KZCreditCard fromJSON(String json) {
        return new Gson().fromJson(json, KZCreditCard.class);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(updatedAt);
        dest.writeString(issuingBank);
        dest.writeString(last4);
        dest.writeString(expirationMonth);
        dest.writeString(customerLocation);
        dest.writeString(gateway);
        dest.writeString(uniqueNumberIdentifier);
        dest.writeString(countryOfIssuance);
        dest.writeString(debit);
        dest.writeString(customerId);
        dest.writeString(setattrs);
        dest.writeString(bin);
        dest.writeString(billingAddress);
        dest.writeString(prepaid);
        dest.writeString(subscriptions);
        dest.writeString(durbinRegulated);
        dest.writeString(commercial);
        dest.writeString(payroll);
        dest.writeString(venmoSdk);
        dest.writeString(cardholderName);
        dest.writeString(verifications);
        dest.writeString(expired);
        dest.writeString(productId);
        dest.writeString(expirationYear);
        dest.writeString(_default);
        dest.writeString(createdAt);
        dest.writeString(cardType);
        dest.writeString(token);
        dest.writeString(imageUrl);
        dest.writeString(healthcare);
        dest.writeString(isExpired);
    }

    public String toJSON() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }
}
