package com.keaz.landbird.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.keaz.landbird.utils.BKNetworkResponse;

import java.util.ArrayList;


/**
 * Created by Harradine on 11/20/2014.
 */
public class KZSearchResult extends BKNetworkResponse implements Parcelable {
    public static final Creator<KZSearchResult> CREATOR = new Creator<KZSearchResult>() {
        @Override
        public KZSearchResult createFromParcel(Parcel in) {
            return new KZSearchResult(in);
        }

        @Override
        public KZSearchResult[] newArray(int size) {
            return new KZSearchResult[size];
        }
    };

    public  KZSearchResult() {
        vehicles = new ArrayList<>();
        bookings = new ArrayList<>();
    }

    private KZSearchResult(Parcel in) {
        vehicles = in.createTypedArrayList(KZVehicle.CREATOR);
    }

    @SerializedName("vehicles")
    public ArrayList<KZVehicle> vehicles;

    @SerializedName(("bookings"))
    public ArrayList<KZObjectTimeLine> bookings;

    @SerializedName("timezone_offset")
    public String timezone_offset;

    @SerializedName("start_timezone_offset")
    public String start__timezone_offset;

    @SerializedName("end_timezone_offset")
    public String end_timezone_offset;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(vehicles);
    }
}
