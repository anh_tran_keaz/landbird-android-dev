package com.keaz.landbird.models;

/**
 * Created by Administrator on 16/2/2017.
 */

public class KZObject<T> {
    private T t;
    private T[] ts;

    public void add(T t) {
        this.t = t;
    }

    public T get() {
        return t;
    }

    public void addList(T[] ts) {
        this.ts = ts;
    }

    public T[] getList() {
        return ts;
    }
}
