package com.keaz.landbird.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mac on 2/23/17.
 */
public class UserModel {

    @SerializedName("assets")
    public UserAssetsModel assets;

    @SerializedName("id")
    public int id;

    @SerializedName("name")
    public String name;

    @SerializedName("email")
    public String email;

    @SerializedName("password")
    public String password;

    @SerializedName("contact_number_second")
    public String contact_number_second;

    @SerializedName("contact_number")
    public String contact_number;

    @SerializedName("country_code")
    public String country_code;

    @SerializedName("country")
    public String country;

    @SerializedName("city")
    public String city;

    @SerializedName("address")
    public String address;

    @SerializedName("address_second")
    public String address_second;

    @SerializedName("rfid")
    public String rfid;

    @SerializedName("checked_terms_condition")
    public String checked_terms_condition;

    @SerializedName("checked_privacy_policy")
    public String checked_privacy_policy;

    @SerializedName("is_new")
    public boolean is_new;

    @SerializedName("checked_business_usage_policy")
    public boolean checked_business_usage_policy;

    @SerializedName("checked_private_usage_policy")
    public boolean checked_private_usage_policy;

    @SerializedName("checked_report_consent")
    public String checked_report_consent;

    @SerializedName("surname")
    public String surname;

    @SerializedName("last_name")
    public String last_name;

    @SerializedName("license_number")
    public String license_number;

    @SerializedName("license_expiry_month")
    public String license_expiry_month;

    @SerializedName("license_expiry_year")
    public String license_expiry_year;

    @SerializedName("license_expiry_day")
    public String license_expiry_day;

    @SerializedName("ccs")
    public Object ccs;

    @SerializedName("method")
    public String method; // != 'normal' -> SSO user

    @SerializedName("license_version")
    public String license_version;

    @SerializedName("phone")
    public String phone;

    @SerializedName("timezoneOffset")
    public int timezoneOffset;

    @SerializedName("license_state")
    public String license_state;

    @SerializedName("license_country")
    public String license_country;

    @SerializedName("post_code")
    public String post_code;

    @SerializedName("dob") //1990-12-23
    public String dob;

    @SerializedName("ssn")
    public String ssn;

    @SerializedName("is_acuant")
    public boolean is_acuant;

    @SerializedName("is_client_user")
    public boolean is_client_user;

    public UserAssetsModel getAssets() {
        return assets;
    }
}
