package com.keaz.landbird.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by TamTran on 22/02/2017.
 */

public class KZPaypal extends KZPaymentMethod implements Parcelable {

    public static final Creator<KZPaypal> CREATOR = new Creator<KZPaypal>() {
        @Override
        public KZPaypal createFromParcel(Parcel in) {
            return new KZPaypal(in);
        }

        @Override
        public KZPaypal[] newArray(int size) {
            return new KZPaypal[size];
        }
    };
    @SerializedName("payer_info")
    @Expose
    public String payerInfo;
    @SerializedName("limited_use_order_id")
    @Expose
    public String limitedUseOrderId;
    @SerializedName("subscriptions")
    @Expose
    public String subscriptions;
    @SerializedName("default")
    @Expose
    public String _default;
    @SerializedName("created_at")
    @Expose
    public String createdAt;
    @SerializedName("billing_agreement_id")
    @Expose
    public String billingAgreementId;
    @SerializedName("updated_at")
    @Expose
    public String updatedAt;
    @SerializedName("email")
    @Expose
    public String email;
    /*@SerializedName("token")
    @Expose
    public String token;*/
    @SerializedName("image_url")
    @Expose
    public String imageUrl;
    @SerializedName("is_channel_initiated")
    @Expose
    public String isChannelInitiated;
    @SerializedName("customer_id")
    @Expose
    public String customerId;
    @SerializedName("gateway")
    @Expose
    public String gateway;
    @SerializedName("_setattrs")
    @Expose
    public String setattrs;


    protected KZPaypal(Parcel in) {
        payerInfo = in.readString();
        limitedUseOrderId = in.readString();
        subscriptions = in.readString();
        _default = in.readString();
        createdAt = in.readString();
        billingAgreementId = in.readString();
        updatedAt = in.readString();
        email = in.readString();
        token = in.readString();
        imageUrl = in.readString();
        isChannelInitiated = in.readString();
        customerId = in.readString();
        gateway = in.readString();
        setattrs = in.readString();
    }

    public static KZPaypal fromJSON(String json) {
        return new Gson().fromJson(json, KZPaypal.class);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(payerInfo);
        dest.writeString(limitedUseOrderId);
        dest.writeString(subscriptions);
        dest.writeString(_default);
        dest.writeString(createdAt);
        dest.writeString(billingAgreementId);
        dest.writeString(updatedAt);
        dest.writeString(email);
        dest.writeString(token);
        dest.writeString(imageUrl);
        dest.writeString(isChannelInitiated);
        dest.writeString(customerId);
        dest.writeString(gateway);
        dest.writeString(setattrs);
    }

    public String toJSON() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }
}
