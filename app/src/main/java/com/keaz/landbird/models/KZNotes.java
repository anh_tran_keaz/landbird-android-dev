package com.keaz.landbird.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Harradine on 11/15/2014.
 */
public class KZNotes implements Parcelable {
    public static final Creator<KZNotes> CREATOR = new Creator<KZNotes>() {
        @Override
        public KZNotes createFromParcel(Parcel in) {
            return new KZNotes(in);
        }

        @Override
        public KZNotes[] newArray(int size) {
            return new KZNotes[size];
        }
    };
    @SerializedName("user_id")
    public String user_id;
    @SerializedName("user_name")
    public String user_name;
    @SerializedName("message")
    public String message;
    @SerializedName("type")
    public String type;
    @SerializedName("date_string")
    public String date_string;
    @SerializedName("date_epoch")
    public long data_epoch;
    @SerializedName("booking_id")
    public String booking_id;
    @SerializedName("id")
    public int note_id;

    public KZNotes() {

    }

    protected KZNotes(Parcel in) {
        user_id = in.readString();
        user_name = in.readString();
        message = in.readString();
        type = in.readString();
        date_string = in.readString();
        data_epoch = in.readLong();
        booking_id = in.readString();
        note_id = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(user_id);
        dest.writeString(user_name);
        dest.writeString(message);
        dest.writeString(type);
        dest.writeString(date_string);
        dest.writeLong(data_epoch);
        dest.writeString(booking_id);
        dest.writeInt(note_id);
    }
}
