package com.keaz.landbird.models;

import com.google.gson.annotations.SerializedName;
import com.keaz.landbird.utils.BKNetworkResponse;

/**
 * Created by anhtran1810 on 5/11/17.
 */

public class KZBookingState extends BKNetworkResponse {
    @SerializedName("state")
    public String state;
}
