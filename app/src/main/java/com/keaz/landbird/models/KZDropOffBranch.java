package com.keaz.landbird.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Harradine on 11/16/2014.
 */
public class KZDropOffBranch {
    @SerializedName("id")
    public int id;

    @SerializedName("name")
    public String name;

    @SerializedName("address")
    public String address;

    @SerializedName("lat")
    public String lat;

    @SerializedName("long")
    public String lon;

}
