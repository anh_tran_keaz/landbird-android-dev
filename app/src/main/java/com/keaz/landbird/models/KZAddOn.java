package com.keaz.landbird.models;

import com.google.gson.annotations.SerializedName;

public class KZAddOn {

    /*
    "name":"Danh 1",
    "date_updated":"",
    "company_id":"138",
    "createdby_userid":"4517",
    "location":"/addon/8",
    "date_created":"1591843651",
    "updatedby_userid":"",
    "id":"8"
    * */

    @SerializedName("name")
    public String name;

    @SerializedName("id")
    public String id;
}
