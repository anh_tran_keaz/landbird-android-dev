package com.keaz.landbird.models;

import java.util.ArrayList;

/**
 * Created by Harradine on 11/15/2014.
 */
public class KZAccount {
    private static KZAccount sharedAccount;
    public String logoCty, bgCty;
    public int activeCompanyIndex = 0;
    public ArrayList<KZCompany> companies;
    public ArrayList<String> perms;
    public UserModel user;
    private KZConfigData kzLandingData1;
    private String source_host;
    private KZDateTime dateTime;

    public KZCompany getCompany() {
        return company;
    }

    public void setCompany(KZCompany company) {
        this.company = company;
    }

    public KZCompany company;
    public KZPaymentMethod paymentDefault;
    public KZBranch branchSelected;

    private KZBranch[] branches;
    private KZCostCentre[] costCentres;
    private KZBodyType[] bodyTypes;
    private KZFeatures[] feature;
    private KZNode[] node;
    private String[] autocompleteOptions;
    private KZPolicy policy;

    private int passwordMinLength;
    private int passwordMaxLength;

    public static KZAccount getSharedAccount() {
        if (null == sharedAccount) {
            sharedAccount = new KZAccount();
        }
        return sharedAccount;
    }

    public KZBranch getBranchSelected() {
        return branchSelected;
    }

    public void setBranchSelected(KZBranch branchSelected) {
        this.branchSelected = branchSelected;
    }

    public String getLogoCty() {
        return logoCty;
    }

    public void setLogoCty(String logoCty) {
        this.logoCty = logoCty;
    }

    public String getBgCty() {
        return bgCty;
    }

    public void setBgCty(String bgCty) {
        this.bgCty = bgCty;
    }

    public int getActiveCompanyIndex() {
        return activeCompanyIndex;
    }

    public void setActiveCompanyIndex(int activeCompanyIndex) {
        this.activeCompanyIndex = activeCompanyIndex;
    }

    public ArrayList<KZCompany> getCompanies() {
        return companies;
    }

    public void setCompanies(ArrayList<KZCompany> companies) {
        this.companies = companies;
    }

    public ArrayList<String> getPerms() {
        return perms;
    }

    public void setPerms(ArrayList<String> perms) {
        this.perms = perms;
    }

    public UserModel getUser() {
        return user;
    }

    public void setUser(UserModel user) {
        this.user = user;
    }

    public KZBranch[] getBranches() {
        return branches;
    }

    public void setBranches(KZBranch[] branches) {
        this.branches = branches;
    }

    public KZCostCentre[] getCostCentres() {
        return costCentres;
    }

    public void setCostCentres(KZCostCentre[] costCentres) {
        this.costCentres = costCentres;
    }

    public KZBodyType[] getBodyTypes() {
        return bodyTypes;
    }

    public void setBodyTypes(KZBodyType[] bodyTypes) {
        this.bodyTypes = bodyTypes;
    }

    public KZFeatures[] getFeature() {
        return feature;
    }

    public void setFeature(KZFeatures[] feature) {
        this.feature = feature;
    }

    public KZNode[] getNode() {
        return node;
    }

    public void setNode(KZNode[] node) {
        this.node = node;
    }

    public KZPolicy getPolicy() {
        return policy;
    }

    public void setPolicy(KZPolicy policy) {
        this.policy = policy;
    }

    public void removeShareAccount() {
        sharedAccount = null;
    }


    public String[] getAutocompleteOptions() {
        return autocompleteOptions;
    }

    public void setAutocompleteOptions(String[] autocompleteOptions) {
        this.autocompleteOptions = autocompleteOptions;
    }

    public int getPasswordMinLength() {
        return passwordMinLength;
    }

    public void setPasswordMinLength(int passwordMinLength) {
        this.passwordMinLength = passwordMinLength;
    }

    public int getPasswordMaxLength() {
        return passwordMaxLength;
    }

    public void setPasswordMaxLength(int passwordMaxLength) {
        this.passwordMaxLength = passwordMaxLength;
    }

    public KZConfigData getKZLandingData1() {
        return kzLandingData1;
    }

    public void setKzLandingData1(KZConfigData kzLandingData1) {
        this.kzLandingData1 = kzLandingData1;
    }

    public void setSourceHost(String source_host) {
        this.source_host = source_host;
    }

    public String getSource_host() {
        return source_host;
    }

    public void setDateTime(KZDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public KZDateTime getDateTime() {
        return dateTime;
    }
}
