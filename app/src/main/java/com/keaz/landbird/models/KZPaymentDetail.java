package com.keaz.landbird.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by TamTran on 22/02/2017.
 */

public class KZPaymentDetail implements Parcelable {

    public static final Creator<KZPaymentDetail> CREATOR = new Creator<KZPaymentDetail>() {
        @Override
        public KZPaymentDetail createFromParcel(Parcel in) {
            return new KZPaymentDetail(in);
        }

        @Override
        public KZPaymentDetail[] newArray(int size) {
            return new KZPaymentDetail[size];
        }
    };
    @SerializedName("detail_method")
    @Expose
    public ArrayList<Object> detailMethod = new ArrayList<>();
    @SerializedName("token")
    @Expose
    public String token;
    @SerializedName("default_method")
    @Expose
    public String defaultMethod;
    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("method")
    @Expose
    public ArrayList<String> method = new ArrayList<>();

    protected KZPaymentDetail(Parcel in) {
        token = in.readString();
        defaultMethod = in.readString();
        id = in.readString();
        method = in.createStringArrayList();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(token);
        dest.writeString(defaultMethod);
        dest.writeString(id);
        dest.writeStringList(method);
    }

    public ArrayList<KZPaymentMethod> getPaymentList() {
        ArrayList<KZPaymentMethod> paymentList = new ArrayList<>();
        Gson gson = new Gson();
        if (detailMethod != null) {

            for (int i = 0; i < detailMethod.size(); i++) {
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject((Map<String, String>) detailMethod.get(i));

                    if (jsonObject.has("card_type")) {
                        KZCreditCard creditCard = gson.fromJson(jsonObject.toString(), KZCreditCard.class);
                        paymentList.add(creditCard);
                    } else if (jsonObject.has("email")) {
                        KZPaypal payPal = gson.fromJson(jsonObject.toString(), KZPaypal.class);
                        paymentList.add(payPal);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }

        return paymentList;

    }
}
