package com.keaz.landbird.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Harradine on 11/15/2014.
 */
public class KZPolicy {

    @SerializedName("business_usage_policy")
    public String business_usage_policy;

    @SerializedName("private_usage_policy")
    public String private_usage_policy;

}
