package com.keaz.landbird.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.HashMap;

/**
 * Created by Harradine on 11/16/2014.
 */
public class KZBranch implements Parcelable {
    public static final Creator<KZBranch> CREATOR = new Creator<KZBranch>() {
        @Override
        public KZBranch createFromParcel(Parcel in) {
            return new KZBranch(in);
        }

        @Override
        public KZBranch[] newArray(int size) {
            return new KZBranch[size];
        }
    };
    @SerializedName("id")
    public int id;
    @SerializedName("name")
    public String name;
    @SerializedName("address")
    public String address;
    @SerializedName("lat")
    public String lat;
    @SerializedName("long")
    public String lon;
    @SerializedName("totals")
    public HashMap<String, Object> totals;
    @SerializedName("assets")
    public KZAssets assets;
    @SerializedName("time_zone_id")
    public String time_zone_id;
    @SerializedName("time_zone_name")
    public String time_zone_name;
    @SerializedName("timezone_offset")
    public String time_zone_offset;
    @SerializedName("business_hours_start")
    public String business_hours_start;
    @SerializedName("business_hours_end")
    public String business_hours_end;
    @SerializedName("enable_migrate_vehicle")
    public String enable_migrate_vehicle;
    @SerializedName("booking_checkin")
    public String booking_checkin;
    @SerializedName("extend_booking_distance")
    public String extend_booking_distance;

    protected KZBranch(Parcel in) {
        id = in.readInt();
        name = in.readString();
        address = in.readString();
        lat = in.readString();
        lon = in.readString();
        time_zone_id = in.readString();
        time_zone_name = in.readString();
        time_zone_offset = in.readString();
        booking_checkin = in.readString();
        enable_migrate_vehicle = in.readString();
        extend_booking_distance = in.readString();
    }
    @Override
    public int describeContents() {
        return 0;
    }
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(address);
        dest.writeString(lat);
        dest.writeString(lon);
        dest.writeString(time_zone_id);
        dest.writeString(time_zone_name);
        dest.writeString(time_zone_offset);
        dest.writeString(booking_checkin);
        dest.writeString(enable_migrate_vehicle);
        dest.writeString(extend_booking_distance);
    }
}
