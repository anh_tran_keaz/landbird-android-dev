package com.keaz.landbird.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Harradine on 11/15/2014.
 */
public class KZCostCentre {
    @SerializedName("id")
    public int id;

    @SerializedName("name")
    public String name;
}
