package com.keaz.landbird.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by anhtran1810 on 3/29/18.
 */

public class KZAndroidDateTimeFormat {
    @SerializedName("date")
    public String date;

    @SerializedName("time")
    public String time;

    @SerializedName("date_time")
    public String date_time;

    @SerializedName("booking_list_group")
    public String booking_list_group;


}
