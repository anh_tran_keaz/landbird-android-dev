package com.keaz.landbird.models;

import com.google.gson.annotations.SerializedName;
import com.keaz.landbird.utils.BKNetworkResponse;

import java.util.ArrayList;


/**
 * Created by Harradine on 11/15/2014.
 */
public class KZLandingData extends BKNetworkResponse {
    @SerializedName("user")
    public UserModel user;

    @SerializedName("companies")
    public ArrayList<KZCompany> companies;
    @SerializedName("perms")
    public ArrayList<String> perms;

    @SerializedName("company")
    public KZCompany company;

    @SerializedName("datetime")
    public KZDateTime datetime;
}
