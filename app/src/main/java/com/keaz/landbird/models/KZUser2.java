package com.keaz.landbird.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by anhtran1810 on 3/26/18.
 */

public class KZUser2 {
    @SerializedName("id")
    public int id;

    @SerializedName("name")
    public String name;

    @SerializedName("email")
    public String email;

    @SerializedName("password")
    public String password;

    @SerializedName("contact_number")
    public String contact_number;

    @SerializedName("phone")
    public String phone;

    @SerializedName("country_code")
    public String country_code;

}
