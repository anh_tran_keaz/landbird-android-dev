package com.keaz.landbird.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Harradine on 11/15/2014.
 */
public class KZUser {
    @SerializedName("id")
    public int id;

    @SerializedName("name")
    public String name;

    @SerializedName("email")
    public String email;

    @SerializedName("assets")
    public KZAssets assets;

    @SerializedName("password")
    public String password;

    @SerializedName("phone")
    public String phone;

    @SerializedName("country_code")
    public String country_code;

    @SerializedName("rfid")
    public String rfid;

    @SerializedName("timezoneOffset")
    public int timezoneOffset;
}
