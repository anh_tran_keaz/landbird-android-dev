package com.keaz.landbird.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class StripePaymentModel implements Parcelable {

    private StripePaymentModel(Parcel in) {
        customer = in.readString();
        cvc_check = in.readString();
        exp_month = in.readDouble();
        exp_year = in.readDouble();
        tokenization_method = in.readString();
        address_line1_check = in.readString();
        brand = in.readString();
        address_state = in.readString();
        last4 = in.readString();
        dynamic_last4 = in.readString();
        address_zip_check = in.readString();
        address_country = in.readString();
        address_city = in.readString();
        id = in.readString();
        address_line1 = in.readString();
        address_line2 = in.readString();
        funding = in.readString();
        country = in.readString();
        name = in.readString();
        fingerprint = in.readString();
        card = in.readString();
    }

    public static final Creator<StripePaymentModel> CREATOR = new Creator<StripePaymentModel>() {
        @Override
        public StripePaymentModel createFromParcel(Parcel in) {
            return new StripePaymentModel(in);
        }

        @Override
        public StripePaymentModel[] newArray(int size) {
            return new StripePaymentModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(customer);
        parcel.writeString(cvc_check);
        parcel.writeDouble(exp_month);
        parcel.writeDouble(exp_year);
        parcel.writeString(tokenization_method);
        parcel.writeString(address_line1_check);
        parcel.writeString(brand);
        parcel.writeString(address_state);
        parcel.writeString(last4);
        parcel.writeString(dynamic_last4);
        parcel.writeString(address_zip_check);
        parcel.writeString(address_country);
        parcel.writeString(address_city);
        parcel.writeString(id);
        parcel.writeString(address_line1);
        parcel.writeString(address_line2);
        parcel.writeString(funding);
        parcel.writeString(country);
        parcel.writeString(name);
        parcel.writeString(fingerprint);
        parcel.writeString(card);
    }

    @SerializedName("customer")
    public String customer;

    @SerializedName("cvc_check")
    public String cvc_check;

    @SerializedName("exp_month")
    public double exp_month;

    @SerializedName("exp_year")
    public double exp_year;

    @SerializedName("tokenization_method")
    public String tokenization_method;

    @SerializedName("address_line1_check")
    public String address_line1_check;

    @SerializedName("brand")
    public String brand;

    @SerializedName("address_state")
    public String address_state;

    @SerializedName("last4")
    public String last4;

    @SerializedName("dynamic_last4")
    public String dynamic_last4;

    @SerializedName("address_zip_check")
    public String address_zip_check;

    @SerializedName("address_country")
    public String address_country;

    @SerializedName("address_city")
    public String address_city;

    @SerializedName("id")
    public String id;

    @SerializedName("address_line1")
    public String address_line1;

    @SerializedName("address_line2")
    public String address_line2;

    @SerializedName("funding")
    public String funding;

    @SerializedName("metadata")
    public Object metadata;

    @SerializedName("country")
    public String country;

    @SerializedName("name")
    public String name;

    @SerializedName("fingerprint")
    public String fingerprint;

    @SerializedName("object")
    public String card;

}
