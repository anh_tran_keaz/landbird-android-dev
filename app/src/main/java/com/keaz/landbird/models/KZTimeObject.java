package com.keaz.landbird.models;

import java.util.Date;

/**
 * Created by Administrator on 24/2/2017.
 */

public class KZTimeObject {
    Date start;
    Date end;

    public KZTimeObject(Date start, Date end) {
        this.start = start;
        this.end = end;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }
}
