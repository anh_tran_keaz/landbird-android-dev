package com.keaz.landbird.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class XModel {

    @SerializedName("vehicles")
    public ArrayList<KZVehicle> vehicles;
}
