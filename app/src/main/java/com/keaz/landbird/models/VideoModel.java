package com.keaz.landbird.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by anhtran1810 on 3/5/18.
 */

public class VideoModel implements Serializable {

    @SerializedName("name")
    public String name;
    @SerializedName("description")
    public String description;
    @SerializedName("id")
    public String id;
    @SerializedName("photo")
    public String photo;
    @SerializedName("video")
    public String video;

    public VideoModel(String name, String description, String id, String photo, String video) {
        this.name = name;
        this.description = description;
        this.id = id;
        this.photo = photo;
        this.video = video;
    }
}
