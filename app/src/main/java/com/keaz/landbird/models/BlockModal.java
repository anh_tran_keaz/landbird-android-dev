package com.keaz.landbird.models;

import com.google.gson.annotations.SerializedName;

public class BlockModal {
    @SerializedName("key")
    private String key;

    @SerializedName("value")
    private String value;

    public String getTextToShow() {
        return value;
    }

    public int getValueToUp() {
        return Integer.valueOf(key)/60;
    }

}
