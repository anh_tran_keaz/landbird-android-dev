package com.keaz.landbird.models;

/**
 * Created by mac on 3/6/17.
 */
public class StateModel {
    private final String stateName;
    private final String stateCode;

    public StateModel(String stateName, String stateCode) {
        this.stateName = stateName;
        this.stateCode = stateCode;
    }

    public String getName() {
        return stateName;
    }

    public String getCode() {
        return stateCode;
    }
}