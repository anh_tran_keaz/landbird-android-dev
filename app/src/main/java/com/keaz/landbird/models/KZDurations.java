package com.keaz.landbird.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Harradine on 11/15/2014.
 */
public class KZDurations {

    @SerializedName("booking_min_duration_time")
    public long booking_min_duration_time;

    @SerializedName("booking_max_duration_time")
    public long booking_max_duration_time;

    @SerializedName("booking_min_duration")
    public int booking_min_duration;

    @SerializedName("booking_max_duration")
    public double booking_max_duration;

    @SerializedName("booking_max_duration_type")
    public String booking_max_duration_type;

    @SerializedName("booking_min_duration_type")
    public String booking_min_duration_type;
}
