package com.keaz.landbird.models;

import com.google.gson.annotations.SerializedName;
import com.keaz.landbird.utils.BKNetworkResponse;

import java.util.ArrayList;


/**
 * Created by Harradine on 11/16/2014.
 */
public class KZBookingsList extends BKNetworkResponse {
    @SerializedName("bookings")
    public ArrayList<KZBooking> bookings;
}
