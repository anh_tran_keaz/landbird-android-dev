package com.keaz.landbird.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Administrator on 22/2/2017.
 */
public class KZObjectTimeLine implements Parcelable {
    public static final Creator<KZObjectTimeLine> CREATOR = new Creator<KZObjectTimeLine>() {
        @Override
        public KZObjectTimeLine createFromParcel(Parcel in) {
            return new KZObjectTimeLine(in);
        }

        @Override
        public KZObjectTimeLine[] newArray(int size) {
            return new KZObjectTimeLine[size];
        }
    };
    @SerializedName("vehicle_id")
    public int vehicle_id;
    @SerializedName("start_date")
    public String start_date;
    @SerializedName("end_date")
    public String end_date;

    protected KZObjectTimeLine(Parcel in) {
        vehicle_id = in.readInt();
        start_date = in.readString();
        end_date = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(vehicle_id);
        dest.writeString(start_date);
        dest.writeString(end_date);
    }
}
