package com.keaz.landbird.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.keaz.landbird.utils.BKNetworkResponse;

import java.util.ArrayList;


/**
 * Created by Harradine on 11/16/2014.
 */
public class KZBooking extends BKNetworkResponse implements Parcelable {
    public static final Creator<KZBooking> CREATOR = new Creator<KZBooking>() {
        @Override
        public KZBooking createFromParcel(Parcel in) {
            return new KZBooking(in);
        }

        @Override
        public KZBooking[] newArray(int size) {
            return new KZBooking[size];
        }
    };
    @SerializedName("id")
    public int id;
    @SerializedName("state")
    public String state;
    @SerializedName("sms_created")
    public String sms_created;
    @SerializedName("start_string")
    public String startString;
    @SerializedName("end_string")
    public String endString;
    @SerializedName("start")
    public long start;
    @SerializedName("end")
    public long end;
    @SerializedName("actual_start")
    public long actual_start;
    @SerializedName("actual_end")
    public long actual_end;
    @SerializedName("user")
    public KZUser user;
    @SerializedName("sub_user")
    public KZUser sub_user;
    @SerializedName("vehicle")
    public KZVehicle vehicle;
    @SerializedName("branch")
    public KZBranch branch;
    @SerializedName("cost_currency")
    public String cost_currency;
    @SerializedName("cost_sub")
    public String cost_sub;
    @SerializedName("cost_time")
    public String cost_time;
    @SerializedName("cost_total")
    public String cost_total;
    @SerializedName("cost_type")
    public String cost_type;
    @SerializedName("trip_type")
    public String trip_type;
    @SerializedName("node")
    public KZNode node;
    @SerializedName("drop_off_node")
    public KZDropOffNode drop_off_node;
    @SerializedName("drop_off_branch")
    public KZDropOffBranch drop_off_branch;
    @SerializedName("end_timezone_offset")
    public long end_timezone_offset;
    @SerializedName("start_timezone_offset")
    public long start_timezone_offset;
    @SerializedName("time_zone_name")
    public String time_zone_name;
    @SerializedName("start_timezone_abbr")
    public String start_timezone_abbr;
    @SerializedName("date_created")
    public long date_created;
    @SerializedName("date_updated")
    public long date_updated;
    @SerializedName("sort")
    public long sort;
    @SerializedName("allocated")
    public boolean allocated;
    @SerializedName("cost_charged")
    public String cost_charged;
    @SerializedName("cost_extend_late")
    public String cost_extend_late;
    @SerializedName("cost_overnight")
    public String cost_overnight;
    @SerializedName("time_pass_by")
    public double time_pass_by;
    @SerializedName("due_back_in")
    public double due_back_in;
    @SerializedName("progress")
    public int progress;
    @SerializedName("cost_preauth")
    public String cost_preauth;
    @SerializedName("cost_refund")
    public String cost_refund;
    @SerializedName("cost_checkout")
    public String cost_checkout;
    @SerializedName("cost_credit")
    public String cost_credit;
    @SerializedName("cost_gst")
    public String cost_gst;
    @SerializedName("membership_discount_cost")
    public String membership_discount_cost;
    @SerializedName("gst")
    public double gst;
    @SerializedName("cost_before_gst")
    public String cost_before_gst;
    @SerializedName("concierge_collection_cost")
    public String concierge_collection_cost;
    @SerializedName("concierge_collection_enable")
    public boolean concierge_collection_enable;
    @SerializedName("concierge_delivery_cost")
    public String concierge_delivery_cost;
    @SerializedName("concierge_delivery_enable")
    public boolean concierge_delivery_enable;
    @SerializedName("extra_costs")
    public ArrayList<KZExtraCost> extra_costs;
    @SerializedName("geotab_carshare_token")
    public String geotab_carshare_token;

    public KZBooking() {

    }

    protected KZBooking(Parcel in) {
        id = in.readInt();
        state = in.readString();
        sms_created = in.readString();
        startString = in.readString();
        endString = in.readString();
        start = in.readLong();
        end = in.readLong();
        cost_currency = in.readString();
        cost_sub = in.readString();
        cost_time = in.readString();
        cost_total = in.readString();
        cost_type = in.readString();
        trip_type = in.readString();
        end_timezone_offset = in.readLong();
        start_timezone_offset = in.readLong();
        time_zone_name = in.readString();
        start_timezone_abbr = in.readString();
        date_created = in.readLong();
        date_updated = in.readLong();
        sort = in.readLong();
        cost_charged = in.readString();
        cost_extend_late = in.readString();
        cost_overnight = in.readString();
        cost_preauth = in.readString();
        cost_refund = in.readString();
        cost_checkout = in.readString();
        cost_credit = in.readString();
        gst = in.readDouble();
        cost_before_gst = in.readString();
        geotab_carshare_token = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(state);
        parcel.writeString(sms_created);
        parcel.writeString(startString);
        parcel.writeString(endString);
        parcel.writeLong(start);
        parcel.writeLong(end);
        parcel.writeLong(actual_start);
        parcel.writeLong(actual_end);
        parcel.writeString(cost_currency);
        parcel.writeString(cost_sub);
        parcel.writeString(cost_time);
        parcel.writeString(cost_total);
        parcel.writeString(cost_type);
        parcel.writeString(trip_type);
        parcel.writeLong(end_timezone_offset);
        parcel.writeLong(start_timezone_offset);
        parcel.writeString(time_zone_name);
        parcel.writeString(start_timezone_abbr);
        parcel.writeLong(date_created);
        parcel.writeLong(date_updated);
        parcel.writeLong(sort);
        parcel.writeString(cost_charged);
        parcel.writeString(cost_extend_late);
        parcel.writeString(cost_overnight);
        parcel.writeString(cost_preauth);
        parcel.writeString(cost_refund);
        parcel.writeString(cost_checkout);
        parcel.writeString(cost_credit);
        parcel.writeDouble(gst);
        parcel.writeString(cost_before_gst);
        parcel.writeString(cost_gst);
        parcel.writeString(concierge_collection_cost);
        parcel.writeInt(concierge_collection_enable?1:0);
        parcel.writeString(concierge_delivery_cost);
        parcel.writeInt(concierge_delivery_enable?1:0);
        if(null == extra_costs){
            extra_costs = new ArrayList<>();
        }
        parcel.writeTypedList(extra_costs);
        parcel.writeString(geotab_carshare_token);
    }
}
