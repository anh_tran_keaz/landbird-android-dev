package com.keaz.landbird.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by TamTran on 22/02/2017.
 */

public abstract class KZPaymentMethod {

    @SerializedName("token")
    public String token;

    @SerializedName("state")
    public String state;
}
