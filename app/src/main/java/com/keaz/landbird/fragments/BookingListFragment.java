package com.keaz.landbird.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.PopupMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.RelativeLayout;

import com.google.gson.JsonObject;
import com.keaz.landbird.R;
import com.keaz.landbird.activities.ApprovedBookingActivity;
import com.keaz.landbird.activities.BaseActivity;
import com.keaz.landbird.activities.BookingListActivity;
import com.keaz.landbird.activities.DetailBookingActivity;
import com.keaz.landbird.activities.MainActivity;
import com.keaz.landbird.adapters.BookingListAdapter;
import com.keaz.landbird.interfaces.CustomListener2;
import com.keaz.landbird.interfaces.CustomListener6;
import com.keaz.landbird.interfaces.ItemClickListener;
import com.keaz.landbird.models.KZAccount;
import com.keaz.landbird.models.KZBooking;
import com.keaz.landbird.models.KZBookingsList;
import com.keaz.landbird.utils.BKGlobals;
import com.keaz.landbird.utils.BKNetworkResponseError;
import com.keaz.landbird.utils.CommonUtils;
import com.keaz.landbird.utils.ConstantsUtils;
import com.keaz.landbird.utils.DateUtils;
import com.keaz.landbird.utils.JsonUtils;
import com.keaz.landbird.utils.LoadingViewUtils;
import com.keaz.landbird.utils.OnResponseModel;
import com.keaz.landbird.utils.ViewUtil;
import com.keaz.landbird.utils.VolleyUtils;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashMap;

/**
 * Created by anhtran1810 on 12/5/17.
 */

public class BookingListFragment extends Fragment implements View.OnClickListener {

    private static boolean allowBackPress = true;
    private BookingListAdapter adapter;
    private RelativeLayout mRelEmptyVehicle;
    private SwipyRefreshLayout mSwipeRefreshLayout;
    private ExpandableListView expandableListView;

    public static Fragment newInstance(boolean b) {
        BookingListFragment fm = new BookingListFragment();
        Bundle upcomingBundle = new Bundle();
        upcomingBundle.putBoolean("isUpcoming", b);
        fm.setArguments(upcomingBundle);
        return fm;
    }

    private enum BookingFilter{ APPROVED, COMPLETED, ALL}
    LinkedHashMap<String, ArrayList<KZBooking>> mBookingHasMap;
    private boolean isUpcoming = false;
    private int mPage = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bookings, container, false);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            isUpcoming = bundle.getBoolean("isUpcoming", false);
        }

        mBookingHasMap = new LinkedHashMap<>();

        initViews(view);

        return view;
    }
    @Override
    public void onClick(final View v) {
        switch (v.getId()){
            case R.id.icMenu:
                ViewUtil.showMenu(v, getActivity(), R.menu.menu_2, new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()){
                            case R.id.item1_show_all_booking:
                                loadBookingListWhenRefreshBottom(BookingFilter.ALL);
                                break;
                            case R.id.item2_show_approved_bookings:
                                loadBookingListWhenRefreshBottom(BookingFilter.APPROVED);
                                break;
                            case R.id.item3_show_complete_bookings:
                                loadBookingListWhenRefreshBottom(BookingFilter.COMPLETED);
                                break;
                        }
                        return false;
                    }
                });
                break;
        }
    }
    @Override
    public void onResume() {
        super.onResume();
        adapter.notifyDataChanged(new LinkedHashMap<String, ArrayList<KZBooking>>());
        showOrHideProgressBar(true, getActivity());
        loadBookingListWhenRefreshTop(BookingFilter.ALL, new CustomListener2() {
            @Override
            public void success() {
                showOrHideProgressBar(false, getActivity());
            }

            @Override
            public void fail(BKNetworkResponseError error) {
                showOrHideProgressBar(false, getActivity());
            }
        });
    }

    public void onBackPressed() {
        if(allowBackPress){
            ((BookingListActivity)getActivity()).goBackToHome();
        }
    }

    private void initViews(View rootView) {
        expandableListView = rootView.findViewById(R.id.expandableListView);
        adapter = new BookingListAdapter(new LinkedHashMap<String, ArrayList<KZBooking>>(), getContext(), new ItemClickListener() {
            @Override
            public void onClick(KZBooking booking) {
                actionLoadBookingByID(booking, new CustomListener6() {
                    @Override
                    public void success(Object o) {
                        actionOpenBooking(getActivity(), (KZBooking) o);
                    }

                    @Override
                    public void fail(BKNetworkResponseError error) {

                    }
                });
            }
        });
        expandableListView.setAdapter(adapter);

        mRelEmptyVehicle = rootView.findViewById(R.id.rel_empty_vehicle);
        mRelEmptyVehicle.setVisibility(View.GONE);

        mSwipeRefreshLayout = rootView.findViewById(R.id.swipeToRefresh);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {
                if (direction == SwipyRefreshLayoutDirection.TOP) {
                    loadBookingListWhenRefreshTop(BookingFilter.ALL, null);
                } else {
                    loadBookingListWhenRefreshBottom(BookingFilter.ALL);
                }
            }
        });

        rootView.findViewById(R.id.btnMakeBooking).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Intent intent = new Intent(getActivity(), MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);*/
                if(getActivity() instanceof MainActivity){
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        ((MainActivity)getActivity()).actionSwitchTab(3, true);
                    }
                }
            }
        });
    }

    private void loadBookingListWhenRefreshTop(final BookingFilter all, final CustomListener2 listener) {
        int userId = KZAccount.getSharedAccount().getUser().id;
        final int totalItemEachPage = KZAccount.getSharedAccount().getCompany().page_item;

        mBookingHasMap.clear();
        VolleyUtils.getSharedNetwork().loadBookingsByUser(getContext(), userId, 0, isUpcoming, new OnResponseModel() {
            @Override
            public void onResponseSuccess(Object model) {
                JsonObject jsonObject = (JsonObject) model;
                KZBookingsList bookingListFromServer = JsonUtils.parseByModel(jsonObject.toString(), KZBookingsList.class);

                ArrayList<KZBooking> newBookingList = new ArrayList<>();

                for (int bookingServerIndex = 0; bookingServerIndex < bookingListFromServer.bookings.size(); bookingServerIndex++) {
                    KZBooking bookingFromServer = bookingListFromServer.bookings.get(bookingServerIndex);

                    boolean isExist = false;
                    int countItemEachPage = 0;

                    for (String key : mBookingHasMap.keySet()) {
                        ArrayList<KZBooking> bookingsInHasMap = mBookingHasMap.get(key);

                        for (int i = 0; i < bookingsInHasMap.size(); i++) {
                            KZBooking booking = bookingsInHasMap.get(i);

                            countItemEachPage++;
                            if (countItemEachPage > totalItemEachPage) {
                                break;
                            }

                            if (booking.id == bookingFromServer.id) {
                                isExist = true;
                                break;
                            }
                        }

                        if (isExist || countItemEachPage > totalItemEachPage) {
                            break;
                        }
                    }

                    if (!isExist) {
                        newBookingList.add(bookingFromServer);
                    }
                }

                if (newBookingList.size() > 0) {
                    mRelEmptyVehicle.setVisibility(View.GONE);

                    adapter.notifyDataChanged(splitBookingIntoGroup(isUpcoming, newBookingList, true));
                    for (int i = 0; i < adapter.getGroupCount(); i++) {
                        expandableListView.expandGroup(i);
                    }
                }else {
                    if(isUpcoming) mRelEmptyVehicle.setVisibility(View.VISIBLE);
                }

                mSwipeRefreshLayout.setRefreshing(false);

                if(listener!=null) listener.success();
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                if(listener!=null) listener.fail(error);
                VolleyUtils.handleErrorRespond((BaseActivity) getActivity(), error);
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    private void loadBookingListWhenRefreshBottom(final BookingFilter all) {
        int userId = KZAccount.getSharedAccount().getUser().id;

        VolleyUtils.getSharedNetwork().loadBookingsByUser(getContext(), userId, mPage, isUpcoming, new OnResponseModel() {
            @Override
            public void onResponseSuccess(Object model) {
                JsonObject jsonObject = (JsonObject) model;
                KZBookingsList bookingList = JsonUtils.parseByModel(jsonObject.toString(), KZBookingsList.class);

                adapter.notifyDataChanged(splitBookingIntoGroup(isUpcoming, bookingList.bookings, false));
                for (int i = 0; i < adapter.getGroupCount(); i++) {
                    expandableListView.expandGroup(i);
                }

                if (mBookingHasMap == null || mBookingHasMap.size() == 0) {
                    if(isUpcoming) mRelEmptyVehicle.setVisibility(View.VISIBLE);
                } else {
                    mRelEmptyVehicle.setVisibility(View.GONE);
                    mPage++;
                }

                mSwipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                VolleyUtils.handleErrorRespond((BaseActivity) getActivity(), error);
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    private void actionLoadBookingByID(KZBooking booking, final CustomListener6 customListener) {
        showOrHideProgressBar(true, getActivity());
        VolleyUtils.getSharedNetwork().loadBookingByBookingId(
                getActivity(),
                booking.id,
                new OnResponseModel<KZBooking>() {

                    @Override
                    public void onResponseSuccess(KZBooking model) {
                        customListener.success(model);
                        showOrHideProgressBar(false, getActivity());
                    }

                    @Override
                    public void onResponseError(BKNetworkResponseError error) {
                        showOrHideProgressBar(false, getActivity());
                        customListener.fail(error);
                        VolleyUtils.handleErrorRespond(getActivity(), error);
                    }
                });
    }

    private void actionOpenBooking(Context context, KZBooking bookingOnServer) {

        if (bookingOnServer.state.equalsIgnoreCase(BKGlobals.BookingStatus.APPROVED.toString())) {

            Intent intent = new Intent(context, ApprovedBookingActivity.class);
            ApprovedBookingActivity.booking = bookingOnServer;
            ((Activity) context).startActivityForResult(intent, CommonUtils.CODE_UPDATE_BOOKING);
            ((Activity) context).setResult(Activity.RESULT_OK);

        } else if (bookingOnServer.state.equalsIgnoreCase(BKGlobals.BookingStatus.INUSE.toString())) {

            /*Intent intent = new Intent(context, MainActivity.class);
            intent.putExtra(ConstantsUtils.INTENT_EXTRA_TAB, 2);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            context.startActivity(intent);
            ((Activity) context).finish();*/

            if(getActivity() instanceof MainActivity){
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    ((MainActivity)getActivity()).actionSetRideFragmentBooking(bookingOnServer);
                    ((MainActivity)getActivity()).actionSwitchTab(1, true);
                }
            }

        }else {

            DetailBookingActivity.booking = bookingOnServer;
            Intent intent = new Intent(getActivity(), DetailBookingActivity.class);
            //intent.putExtra(ConstantsUtils.INTENT_EXTRA_CAR_URL, bookingOnServer.vehicle.assets.photo);
            //intent.putExtra(ConstantsUtils.INTENT_EXTRA_VEHICLE, bookingOnServer.vehicle);
            //intent.putExtra(ConstantsUtils.INTENT_EXTRA_BRANCH, bookingOnServer.branch);
            intent.putExtra(ConstantsUtils.INTENT_EXTRA_START_TIME_IN_MILLIS, bookingOnServer.start);
            intent.putExtra(ConstantsUtils.INTENT_EXTRA_END_TIME_IN_MILLIS, bookingOnServer.end);
            intent.putExtra(ConstantsUtils.INTENT_EXTRA_TRIP_TYPE, bookingOnServer.trip_type);
            intent.putExtra(ConstantsUtils.INTENT_EXTRA_BOOKING_STATUS, bookingOnServer.state);

            getActivity().startActivity(intent);
            getActivity().overridePendingTransition(R.anim.fade_in_normal, R.anim.fade_out_normal);
        }
    }

    private LinkedHashMap<String, ArrayList<KZBooking>> splitBookingIntoGroup(boolean isUpcoming, ArrayList<KZBooking> bookings, boolean isRefreshTop) {

        //Sort bookings with time
        Collections.sort(bookings, new Comparator<KZBooking>(){
            public int compare(KZBooking obj1, KZBooking obj2) {
                return obj1.sort < obj2.sort ? 1 : (obj1.sort > obj2.sort ? -1 : 0);
            }
        });

        for (KZBooking booking : bookings) {
            Date date = DateUtils.getDate(booking.sort);
            String sectionName = DateUtils.compareTwoDate(isUpcoming, date);

            if (!mBookingHasMap.containsKey(sectionName)) {
                ArrayList<KZBooking> sectionBookings = new ArrayList<>();
                if (isRefreshTop)
                    sectionBookings.add(0, booking);
                else
                    sectionBookings.add(booking);
                mBookingHasMap.put(sectionName, sectionBookings);
            } else {
                if (isRefreshTop)
                    mBookingHasMap.get(sectionName).add(0, booking);
                else
                    mBookingHasMap.get(sectionName).add(booking);
            }
        }
        return mBookingHasMap;
    }

    public static void showOrHideProgressBar(boolean b, Activity activity) {
        allowBackPress = !LoadingViewUtils.showOrHideProgressBar(b, activity);
    }
}
