package com.keaz.landbird.fragments;


import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.keaz.landbird.R;
import com.keaz.landbird.activities.ActivityProfile;
import com.keaz.landbird.activities.AddAGuestActivity;
import com.keaz.landbird.activities.AddPaymentActivity;
import com.keaz.landbird.activities.AgreementsActivity;
import com.keaz.landbird.activities.DriverLicenseActivity;
import com.keaz.landbird.activities.DriverLicenseActivityEnvoy;
import com.keaz.landbird.activities.GetSocialActivity;
import com.keaz.landbird.activities.MembershipListActivity;
import com.keaz.landbird.activities.PromoCodeActivity;
import com.keaz.landbird.activities.RegisterCardActivity;
import com.keaz.landbird.interfaces.CustomListener;
import com.keaz.landbird.models.KZAccount;
import com.keaz.landbird.utils.ConfigUtil;
import com.keaz.landbird.utils.ConstantsUtils;
import com.keaz.landbird.utils.DialogUtils;
import com.keaz.landbird.view.CustomFontButton;
import com.keaz.landbird.view.CustomFontTextView;
import com.segment.analytics.Analytics;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


public class ProfileFragment extends Fragment implements View.OnClickListener {

    private static final String BENEFITS_STR = "Benefits";
    private static final String REFER_FRIEND_STR = "Refer A Friend";
    private static final String CHANGE_PASS_STR = "Change Password";
    private static final String REGISTER_KEY_CARD_STR = "Link your GO Pass";
    private static final String REPORT_LOST_KEY_CARD_STR = "Report Lost GO pass";
    private static final String AGREEMENTS_STR = "Agreements";
    private static final String GET_SOCIAL_STR = "Socials";
    private static final String MEMBERSHIP_STR = "Memberships";
    private static final String ADD_A_GUEST_STR = "Add a guest";
    private static final String PROMO_CODE_STRING = "Promotional Codes";
    private static final String DEACTIVATE_ACCOUNT_STRING = "Deactivate account";
    private static final String ACCOUNT_INFO_STR = "Account info";
    private static final String PAYMENT_METHOD_STR = "Payment Method";
    private static final String DRIVER_LICENSE_STR = "Driver License";

    private ListView listView;
    private String[] dataList;
    private View view;

    public ProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_account, container, false);

        initUI(view);
        initData();
        initListener();

        return view;
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case ConstantsUtils.REQUEST_CODE_REQUEST_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    actionCall();
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(getActivity(), getString(R.string.error_permission_denied), Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_logout:
                //tappedLogout();
                break;
        }
    }

    private boolean checkIfAlreadyHavePermission() {
        int result = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void initUI(View view) {
        listView = view.findViewById(R.id.lv_account);
        CustomFontButton btnLogout = view.findViewById(R.id.btn_logout);
        btnLogout.setOnClickListener(this);
    }

    private void initData() {
        List<String> temp = new ArrayList<>();
        //temp.add(BENEFITS_STR);
        //temp.add(ACCOUNT_INFO_STR);
        temp.add(REGISTER_KEY_CARD_STR);
        //temp.add(REFER_FRIEND_STR);
        if(ConfigUtil.checkIfEnablePromotion()){
            temp.add(PROMO_CODE_STRING);
        }
        temp.add(PAYMENT_METHOD_STR);
        //temp.add(CHANGE_PASS_STR);
        temp.add(GET_SOCIAL_STR);
        temp.add(DRIVER_LICENSE_STR);
        temp.add(DEACTIVATE_ACCOUNT_STRING);
        if(ConfigUtil.checkIfEnableMembership()){
            temp.add(MEMBERSHIP_STR);
        }
        //temp.add(ADD_A_GUEST_STR);

        //temp.add(REPORT_LOST_KEY_CARD_STR);
        //temp.add(AGREEMENTS_STR);

        String email = KZAccount.getSharedAccount().user != null ? KZAccount.getSharedAccount().user.email : "";
        String phone = KZAccount.getSharedAccount().user != null ? KZAccount.getSharedAccount().user.phone : "";
        ((TextView)view.findViewById(R.id.tvAccountEmail)).setText(email!=null?email:"..");
        ((TextView)view.findViewById(R.id.tvAccountContact)).setText(phone!=null?phone:"..");

        dataList = new String[temp.size()];
        dataList = temp.toArray(dataList);

        CustomProfileItemAdapter adapter = new CustomProfileItemAdapter(getActivity(), dataList);

        listView.setAdapter(adapter);

        try{
            Picasso.with(getActivity()).load(KZAccount.getSharedAccount().user.assets.avatar).placeholder(R.drawable.new_ic_tab_profile_d)
                    .error(R.drawable.new_ic_tab_profile_d)
                    .into((ImageView) view.findViewById(R.id.imvProfile));
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void initListener() {
        view.findViewById(R.id.viewProfile).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ActivityProfile.class);
                startActivity(intent);
            }
        });
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                switch (dataList[position]) {
                    case REFER_FRIEND_STR:
                        //startActivityWithName(ReferActivity.class);
                        break;

                    case CHANGE_PASS_STR:
                        break;

                    case REGISTER_KEY_CARD_STR:
                        Analytics.with(getContext()).track("Click RFID Register item");
                        startActivityWithName(RegisterCardActivity.class);
                        break;

                    case AGREEMENTS_STR:
                        Analytics.with(getContext()).track("Click Agreements");
                        startActivityWithName(AgreementsActivity.class);
                        break;

                    case GET_SOCIAL_STR:
                        Analytics.with(getContext()).track("Click Get Social item");
                        startActivityWithName(GetSocialActivity.class);
                        break;

                    case MEMBERSHIP_STR:
                        if(ConfigUtil.checkIfEnableMembership()){
                            Analytics.with(getContext()).track("Click Memberships item");
                            startActivityWithName(MembershipListActivity.class);
                        }
                        break;

                    case ADD_A_GUEST_STR:
                        Analytics.with(getContext()).track("Click Add A Guest item");
                        startActivityWithName(AddAGuestActivity.class);
                        break;

                    case PROMO_CODE_STRING:
                        if(ConfigUtil.checkIfEnablePromotion()){
                            Analytics.with(getContext()).track("Click Promos item");
                            startActivityWithName(PromoCodeActivity.class);
                        }
                        break;

                    case DEACTIVATE_ACCOUNT_STRING:
                        Analytics.with(getContext()).track("Click Deactivate item");
                        actionClickDeactivateAccount();
                        break;

                    case DRIVER_LICENSE_STR:
                        Analytics.with(getContext()).track("Click Driver license item");
                        DialogUtils.showCustomContextMenu(getActivity(), "Current DL Activity", "Envoy DL Screen", null,
                                new CustomListener() {
                                    @Override
                                    public void onClick() {
                                        Intent intent = new Intent(getActivity(), DriverLicenseActivity.class);
                                        intent.putExtra(ConstantsUtils.INTENT_EXTRA_VIEW_DRIVER_LICENSE_FROM_SETTING, true);
                                        getActivity().startActivity(intent);
                                    }
                                },
                                new CustomListener() {
                                    @Override
                                    public void onClick() {
                                        Intent intent = new Intent(getActivity(), DriverLicenseActivityEnvoy.class);
                                        intent.putExtra(ConstantsUtils.INTENT_EXTRA_VIEW_DRIVER_LICENSE_FROM_SETTING, true);
                                        getActivity().startActivity(intent);
                                    }
                                },null);
                        break;

                    case PAYMENT_METHOD_STR:
                        Analytics.with(getContext()).track("Click Payment Method item");
                        startActivity(new Intent(getActivity(), AddPaymentActivity.class));
                        break;

                    default:
                        break;
                }
            }
        });
    }

    private void actionClickDeactivateAccount() {
        DialogUtils.showCustomDialog(getActivity(),
                true,
                getString(R.string.text_deactivate_account),
                getString(R.string.confirm_to_deactivate_account),
                getString(R.string.text_call_now),
                getString(R.string.cancel),
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                    }
                },
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(!checkIfAlreadyHavePermission()){
                            ActivityCompat.requestPermissions(ProfileFragment.this.getActivity(), new String[]{Manifest.permission.CALL_PHONE}, ConstantsUtils.REQUEST_CODE_REQUEST_PERMISSION);
                            return;
                        }
                        actionCall();
                    }
                });
    }

    private void actionCall() {
        Intent callIntent = new Intent(Intent.ACTION_DIAL);
        callIntent.setData(Uri.parse(getString(R.string.text_tel) + KZAccount.getSharedAccount().company.contact_number));
        startActivity(callIntent);
    }

    private void startActivityWithName(Class<?> tClass) {
        getActivity().startActivity(new Intent(getActivity(), tClass));
    }

    private class CustomProfileItemAdapter extends ArrayAdapter<String> {
        private final Context context;
        private final String[] values;

        CustomProfileItemAdapter(Context context, String[] values) {
            super(context, -1, values);
            this.context = context;
            this.values = values;
        }

        @Override
        public int getCount() {
            return values != null ? values.length : 0;
        }

        @NonNull
        @Override
        public View getView(int position, View convertView, @NonNull ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if(convertView==null){
                convertView = inflater.inflate(R.layout.item_account_list, parent, false);
            }

            CustomFontTextView textView = (CustomFontTextView) convertView.findViewById(R.id.tv_title);

            textView.setText(values[position]);

            switch (values[position]){
                case CHANGE_PASS_STR:
                case ACCOUNT_INFO_STR:
                case REFER_FRIEND_STR:
                    textView.setTextColor(getResources().getColor(R.color.textLightGray));
                    break;
            }

            return convertView;
        }
    }
}
