package com.keaz.landbird.fragments;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.keaz.landbird.R;
import com.keaz.landbird.activities.Agreements2Activity;
import com.keaz.landbird.activities.BaseActivity;
import com.keaz.landbird.activities.HowToVideosActivity;
import com.keaz.landbird.activities.ReportLostKeyCardActivity;
import com.keaz.landbird.models.KZAccount;
import com.keaz.landbird.models.KZNotes;
import com.keaz.landbird.utils.BKNetworkResponseError;
import com.keaz.landbird.utils.LoadingViewUtils;
import com.keaz.landbird.utils.OnResponseModel;
import com.keaz.landbird.utils.VolleyUtils;
import com.keaz.landbird.view.CustomEditText;
import com.keaz.landbird.view.CustomFontButton;
import com.keaz.landbird.view.CustomFontTextView;
import com.segment.analytics.Analytics;
import com.segment.analytics.Properties;

/**
 * Created by anhtran1810 on 1/24/18.
 */

public class SupportFragment extends Fragment implements View.OnClickListener {

    private CustomEditText mExtMessage;
    private CustomFontButton mBtnSend;
    private CustomFontTextView mTxtCall;
    private CustomFontTextView mTxtDocuments;
    private CustomFontTextView mTxtLostKeyCard;
    private CustomFontTextView mTxtVehicleGuide;
    private CustomFontTextView mTxtHowToVideos;
    private CustomFontTextView mTxtFeesToKnowAbout;
    private View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_support, container, false);

        initUI(view);

        return view;
    }

    public void initUI(View view) {
        mExtMessage = (CustomEditText) view.findViewById(R.id.ext_support);

        mBtnSend = (CustomFontButton) view.findViewById(R.id.btn_send);
        mBtnSend.setOnClickListener(this);

        SpannableStringBuilder builder = new SpannableStringBuilder();

        String black = "Call Us Now ";
        SpannableString blackSpannable= new SpannableString(black);
        blackSpannable.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.landBird_textDarkColorX1)), 0, black.length(), 0);
        builder.append(blackSpannable);

        String red = /*"888-610-0506"*/ KZAccount.getSharedAccount().company.contact_number;
        SpannableString redSpannable= new SpannableString(red);
        redSpannable.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.landBird_textDarkColor)), 0, red.length(), 0);
        builder.append(redSpannable);

        mTxtCall = (CustomFontTextView) view.findViewById(R.id.txt_call);
        mTxtCall.setText(builder, TextView.BufferType.SPANNABLE);
        mTxtCall.setOnClickListener(this);

        mTxtDocuments = (CustomFontTextView) view.findViewById(R.id.txt_document);
        mTxtDocuments.setOnClickListener(this);

        mTxtLostKeyCard = (CustomFontTextView) view.findViewById(R.id.txt_lost_key_card);
        mTxtLostKeyCard.setOnClickListener(this);

        mTxtVehicleGuide = (CustomFontTextView) view.findViewById(R.id.txt_vehicle_guide);
        mTxtVehicleGuide.setOnClickListener(this);

        mTxtHowToVideos = (CustomFontTextView) view.findViewById(R.id.txt_how_to_videos);
        mTxtHowToVideos.setOnClickListener(this);

        mTxtFeesToKnowAbout = (CustomFontTextView) view.findViewById(R.id.txt_fees_to_know_about);
        mTxtFeesToKnowAbout.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_send:
                Analytics.with(getContext()).track("Click send_btn");
                tappedContactUs();
                break;

            case R.id.txt_call:
                Analytics.with(getContext()).track("Click Call Us Now item");
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse(getContext().getString(R.string.text_tel) + KZAccount.getSharedAccount().company.contact_number));
                startActivity(intent);
                break;

            case R.id.txt_document:
                Analytics.with(getContext()).track("Click Legal Documents item");
                startActivity(new Intent(getActivity(), Agreements2Activity.class));
                break;

            case R.id.txt_lost_key_card:
                Analytics.with(getContext()).track("Click Lost RFID Report item");
                startActivity(new Intent(getActivity(), ReportLostKeyCardActivity.class));
                break;

            case R.id.txt_vehicle_guide:
//                startActivity(new Intent(getActivity(), VehicleGuideActivity.class));
                break;

            case R.id.txt_how_to_videos:
                Analytics.with(getContext()).track("Clicking Support Articles and FAQ item");
                Intent intent1 = new Intent(getActivity(), HowToVideosActivity.class);
                intent1.putExtra(HowToVideosActivity.INTENT_EXTRA_API, "faq");
                startActivity(intent1);
                break;

            case R.id.txt_fees_to_know_about:
//                startActivity(new Intent(getActivity(), FeesToKnowActivity2.class));
                break;
        }
    }

    private void tappedContactUs() {
        final String message = mExtMessage.getText().toString();

        if (message == null || message.equals("")) {
            Toast.makeText(getContext(), getContext().getString(R.string.error_empty_message), Toast.LENGTH_SHORT).show();
            return;
        }

        LoadingViewUtils.showOrHideProgressBar(true, view);
        VolleyUtils.getSharedNetwork().postContactUs(getContext(), mExtMessage.getText().toString(), new OnResponseModel<KZNotes>() {
            @Override
            public void onResponseSuccess(KZNotes model) {
                LoadingViewUtils.showOrHideProgressBar(false, view);
                Toast.makeText(getContext(), getContext().getString(R.string.text_sent), Toast.LENGTH_SHORT).show();
                mExtMessage.setText("");

                Analytics.with(getContext()).track("Sent message", new Properties().putValue("message", message));
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                VolleyUtils.handleErrorRespond((BaseActivity) getActivity(), error);
                LoadingViewUtils.showOrHideProgressBar(false, view);
            }
        });
    }
}
