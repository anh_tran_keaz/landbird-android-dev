package com.keaz.landbird.fragments;

import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.geotab.carshare.CarShareClient;
import com.geotab.carshare.Command;
import com.geotab.carshare.Error;
import com.google.gson.JsonObject;
import com.keaz.landbird.R;
import com.keaz.landbird.activities.BaseActivity;
import com.keaz.landbird.activities.DetailBookingActivity;
import com.keaz.landbird.activities.GeoDeviceActivity;
import com.keaz.landbird.activities.MainActivity;
import com.keaz.landbird.activities.SubmitEndReservationActivity;
import com.keaz.landbird.dialogs.AddMoreTimeDialog;
import com.keaz.landbird.dialogs.UnAvailableDialog;
import com.keaz.landbird.enums.BluetoothCarShareStatus;
import com.keaz.landbird.interfaces.CustomListener1;
import com.keaz.landbird.models.KZBooking;
import com.keaz.landbird.utils.BKGlobals;
import com.keaz.landbird.utils.BKNetworkResponseError;
import com.keaz.landbird.utils.CommonUtils;
import com.keaz.landbird.utils.ConstantsUtils;
import com.keaz.landbird.utils.DateUtils;
import com.keaz.landbird.utils.FormatUtil;
import com.keaz.landbird.utils.JsonUtils;
import com.keaz.landbird.utils.OnResponseModel;
import com.keaz.landbird.utils.ToastUtil;
import com.keaz.landbird.utils.UIUtils;
import com.keaz.landbird.utils.VolleyUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by anhtran1810 on 1/24/18.
 */

public class RideFragment extends Fragment implements View.OnClickListener{

    private static final int CODE_UPDATE_BOOKING = 999;
    private static final String TAG = "anhtran";

    public static KZBooking booking;
    private boolean updateSuccess = false;
    private Runnable runnable;
    private ProgressBar progressBar;
    private Handler handler;
    private View view;
    private boolean isResumed;
    private boolean isStop;
    private View btnCarShareLock;
    private View btnCarShareUnlock;
    TextView tvBluetooth;
    TextView tvCarShare;
    TextView btnBluetoothEnable;
    TextView btnCarShareConnect;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_ride, container, false);

        setupView(view);
        setupListener(view);
        setupPeriodicAction();

        return view;
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (null == bluetoothAdapter()) {
                // Device doesn't support Bluetooth
                updateAllData(BluetoothCarShareStatus.BLUETOOTH_NOT_SUPPORT, null);
            } else {
                if (!bluetoothAdapter().isEnabled()) {
                    updateAllData(BluetoothCarShareStatus.BLUETOOTH_DISABLE, null);
                } else {
                    updateAllData(BluetoothCarShareStatus.BLUETOOTH_ENABLE, null);
                }
            }
        }
    }
    @Override
    public void onStart() {
        super.onStart();
        isStop = false;
    }
    @Override
    public void onStop() {
        super.onStop();
        handler.removeCallbacks(runnable);
        isStop = true;

    }
    @Override
    public void onDestroy() {
        super.onDestroy();
    }
    @Override
    public void onResume() {
        super.onResume();
        isResumed  = true;
        if(booking !=null){
            progressBar.setVisibility(View.VISIBLE);
            VolleyUtils.getSharedNetwork().loadBookingByBookingId(
                    getActivity(),
                    booking.id,
                    new OnResponseModel<KZBooking>() {
                        @Override
                        public void onResponseSuccess(KZBooking model) {
                            progressBar.setVisibility(View.INVISIBLE);
                            booking = model;

                            updateView();

                            if(getActivity()==null || isStop){
                                return;
                            }
                            handler.postDelayed(runnable,0);
                        }

                        @Override
                        public void onResponseError(BKNetworkResponseError error) {
                            progressBar.setVisibility(View.INVISIBLE);
                        }
                    });
        }
    }
    @Override
    public void onPause() {
        super.onPause();
        isResumed = false;
    }
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnEnd:
                CommonUtils.segmentTrackWithBooking(getContext(), "Click End_reservation_btn", booking);
                handleEndReservation();
                break;

            case R.id.addMoreTime:
                CommonUtils.segmentTrackWithBooking(getContext(), "Click Add_more_time_btn", booking);
                showAddMoreTimeDialog();
                break;

            /*case R.id.ivLock:
                CommonUtils.segmentTrackWithBooking(getContext(), "Click lock_btn", booking);
                handleLockVehicle();
                break;*/

            /*case R.id.ivUnLock:
                CommonUtils.segmentTrackWithBooking(getContext(), "Click unlock_btn", booking);
                handleUnLockVehicle();
                break;*/
            case R.id.btnBluetooth:
                ((MainActivity)getActivity()).actionRequestBluetooth();
                break;
            case R.id.btnConnect:
                ((MainActivity)getActivity()).actionConnect();
                break;
            case R.id.btnDemo:
                Intent enableBtIntent = new Intent(getActivity(), GeoDeviceActivity.class);
                enableBtIntent.putExtra("token", booking.geotab_carshare_token);
                startActivity(enableBtIntent);
                break;
            case R.id.btnLock:
                ((MainActivity)getActivity()).actionLock();
                break;
            case R.id.btnUnlock:
                ((MainActivity)getActivity()).actionUnlock();
                break;
        }
    }

    private void updateView() {
        if(booking !=null){
            if(booking.vehicle.door.equals("lock")){
                //view.findViewById(R.id.btnUnlock).setBackground(getResources().getDrawable(R.drawable.circle_transparent));
                //view.findViewById(R.id.btnLock).setBackground(getResources().getDrawable(R.drawable.circle_white));
                ((TextView)view.findViewById(R.id.tvLockStatus)).setText("Lock");
            }else {
                //view.findViewById(R.id.btnUnlock).setBackground(getResources().getDrawable(R.drawable.circle_white));
                //view.findViewById(R.id.btnLock).setBackground(getResources().getDrawable(R.drawable.circle_transparent));
                ((TextView)view.findViewById(R.id.tvLockStatus)).setText("Unlock");
            }
        }
    }

    private void setupPeriodicAction() {
        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                if(booking !=null){
                    actionLoadBookingCostRegularlyOnTimer(new CustomListener1() {
                        @Override
                        public void takeAction() {
                            if(getActivity()==null || isStop){
                                return;
                            }
                            handler.postDelayed(runnable, BKGlobals.RELOAD_TIME_60);
                        }
                    });
                }
            }
        };
    }

    private boolean setUpCostData(KZBooking booking) {
        Log.d(TAG, "setUpCostData");

        if(RideFragment.booking.state.equalsIgnoreCase(BKGlobals.BOOKING_STATE_COMPLETE) || RideFragment.booking.state.equalsIgnoreCase(BKGlobals.BOOKING_STATE_CANCELLED)){
            actionAutoOpenBookingDetailScreen();
            return false;
        }

        long endInSec = RideFragment.booking.end - DateUtils.getTimeZoneOffset() - RideFragment.booking.end_timezone_offset;
        Date date_end = DateUtils.getDate(endInSec);

        UIUtils.setTextViewText(getActivity(), R.id.txtPassBy, DateUtils.getDurationFormatText1((int) (booking.time_pass_by/60)));
        UIUtils.setTextViewText(getActivity(), R.id.txtDueBack, DateUtils.getDurationFormatText1((int) booking.due_back_in/60) /*DateUtils.getDurationFormatText1(nowInSec *//*bookingCost.now*//*, endInSec)*/);
        UIUtils.setTextViewText(getActivity(), R.id.txtReturnTime, DateUtils.formatDatePattern(date_end, ConstantsUtils.DATE_TIME_FORMAT /*ConfigUtil.getDateTimeFormat()*/));
        UIUtils.setTextViewText(getActivity(), R.id.txtTotal, "$" + FormatUtil.formatString(RideFragment.booking.cost_sub) + " / " + RideFragment.booking.cost_type);
        UIUtils.setTextViewText(getActivity(), R.id.tvValueCostTotal, "$" + FormatUtil.formatString(booking.cost_charged));


        Log.d("booking_info", "Time used, time_pass_by : "+booking.time_pass_by + " show "+DateUtils.getDurationFormatText1((int) (booking.time_pass_by/60)));
        Log.d("booking_info", "Due back in, due_back_in  : "+booking.due_back_in + "  show "+DateUtils.getDurationFormatText1((int) (booking.due_back_in/60)));
        Log.d("booking_info", "Return time, txtReturnTime  : "+date_end + "  show "+DateUtils.formatDatePattern(date_end, ConstantsUtils.DATE_TIME_FORMAT));
        Log.d("booking_info", "Cost, cost_sub  : "+booking.cost_sub + "  show "+"$" + FormatUtil.formatString(RideFragment.booking.cost_sub) + " / " + RideFragment.booking.cost_type);
        Log.d("booking_info", "Cost total, cost_charged  : "+booking.cost_charged + "  show "+"$" + FormatUtil.formatString(booking.cost_charged));

        return true;
    }

    private void setupListener(View view) {
        view.findViewById(R.id.btnEnd).setOnClickListener(this);
        view.findViewById(R.id.addMoreTime).setOnClickListener(this);
        view.findViewById(R.id.btnLock).setOnClickListener(this);
        view.findViewById(R.id.btnUnlock).setOnClickListener(this);
        view.findViewById(R.id.btnBluetooth).setOnClickListener(this);
        view.findViewById(R.id.btnConnect).setOnClickListener(this);
        view.findViewById(R.id.btnDemo).setOnClickListener(this);
        btnCarShareLock.setOnClickListener(this);
        btnCarShareUnlock.setOnClickListener(this);
    }

    private void setupView(View view) {
        progressBar = view.findViewById(R.id.progressBar_horizontal);
        btnCarShareLock = view.findViewById(R.id.btnLock);
        btnCarShareUnlock = view.findViewById(R.id.btnUnlock);
        tvBluetooth = view.findViewById(R.id.tvBluetooth);
        tvCarShare = view.findViewById(R.id.tvConnect);

        btnBluetoothEnable = view.findViewById(R.id.btnBluetooth);
        btnCarShareConnect = view.findViewById(R.id.btnConnect);
    }

    private void actionLoadBookingCostRegularlyOnTimer(final CustomListener1 customListener1) {
        Log.d(TAG, "actionLoadBookingCostRegularlyOnTimer");
        progressBar.setVisibility(View.VISIBLE);
        VolleyUtils.getSharedNetwork().loadBookingCostByBookingId(
                getContext(),
                booking.id,
                new OnResponseModel<Object>() {

                    @Override
                    public void onResponseSuccess(Object model) {
                        progressBar.setVisibility(View.INVISIBLE);
                        JsonObject jsonObject = (JsonObject) model;
                        KZBooking booking = JsonUtils.parseByModel(jsonObject.toString(), KZBooking.class);
                        if(booking!=null){
                            RideFragment.booking = booking;
                            boolean b = setUpCostData(booking);
                            if(!b) return;
                        }else {
                            Log.d("booking_info", "bookingCost is null");
                        }
                        if(customListener1!=null) customListener1.takeAction();
                    }

                    @Override
                    public void onResponseError(BKNetworkResponseError error) {
                        /*findViewById(R.id.smallProgressBar)*/
                        progressBar.setVisibility(View.INVISIBLE);
                        if(getActivity()==null || isStop){
                            return;
                        }
                        handler.postDelayed(runnable, BKGlobals.RELOAD_TIME_60);
                    }
                });
    }

    private void actionAutoOpenBookingDetailScreen() {
        if(getActivity()==null) return;
        handler.removeCallbacks(runnable);
        DetailBookingActivity.booking = booking;
        Intent intent = new Intent(getActivity(), DetailBookingActivity.class);
        intent.putExtra(ConstantsUtils.INTENT_EXTRA_BACK_TO_HOME, true);
        /*intent.putExtra(ConstantsUtils.INTENT_EXTRA_CAR_URL, booking.vehicle.assets.photo);
        intent.putExtra(ConstantsUtils.INTENT_EXTRA_VEHICLE, booking.vehicle);
        intent.putExtra(ConstantsUtils.INTENT_EXTRA_BRANCH, booking.branch);
        intent.putExtra(ConstantsUtils.INTENT_EXTRA_START_TIME_IN_MILLIS, booking.start);
        intent.putExtra(ConstantsUtils.INTENT_EXTRA_END_TIME_IN_MILLIS, booking.end);
        intent.putExtra(ConstantsUtils.INTENT_EXTRA_TRIP_TYPE, booking.trip_type);
        intent.putExtra(ConstantsUtils.INTENT_EXTRA_BOOKING_STATUS, booking.state);*/
        startActivity(intent);
    }

    private void handleLockVehicle() {
        progressBar.setVisibility(View.VISIBLE);
        VolleyUtils.getSharedNetwork().lockVehicle(
                getActivity(),
                booking.id,
                new OnResponseModel<JSONObject>() {
            @Override
            public void onResponseSuccess(JSONObject model) {
                progressBar.setVisibility(View.INVISIBLE);
                UIUtils.toast(getActivity(), "We just sent the command to lock your car. Give it about ten seconds and you're good to go!");
                //view.findViewById(R.id.btnUnlock).setBackground(getResources().getDrawable(R.drawable.circle_transparent));
                //view.findViewById(R.id.btnLock).setBackground(getResources().getDrawable(R.drawable.circle_white));
                ((TextView)view.findViewById(R.id.tvLockStatus)).setText("Lock");
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                progressBar.setVisibility(View.INVISIBLE);
                VolleyUtils.handleErrorRespond((BaseActivity) getActivity(), error);
            }
        });
    }

    private void handleUnLockVehicle() {
        progressBar.setVisibility(View.VISIBLE);
        VolleyUtils.getSharedNetwork().unLockVehicle(getContext(), booking.id, new OnResponseModel<JSONObject>() {
            @Override
            public void onResponseSuccess(JSONObject model) {
                progressBar.setVisibility(View.INVISIBLE);
                UIUtils.toast(getActivity(), "We just sent the command to unlock your car. Give it about ten seconds and you're good to go!");
                //view.findViewById(R.id.btnUnlock).setBackground(getResources().getDrawable(R.drawable.circle_white));
                //view.findViewById(R.id.btnLock).setBackground(getResources().getDrawable(R.drawable.circle_transparent));
                ((TextView)view.findViewById(R.id.tvLockStatus)).setText("Unlock");
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                progressBar.setVisibility(View.INVISIBLE);
                VolleyUtils.handleErrorRespond((BaseActivity) getActivity(), error);
            }
        });
    }

    private void handleEndReservation() {
        if(getActivity()==null) return;
        handler.removeCallbacks(runnable);
        Intent intent = new Intent(getActivity(), SubmitEndReservationActivity.class);
        SubmitEndReservationActivity.booking = booking;
        startActivityForResult(intent, CODE_UPDATE_BOOKING);
    }

    private void showAddMoreTimeDialog() {
        AddMoreTimeDialog dialog = new AddMoreTimeDialog(getContext(), booking);
        dialog.show();
        dialog.setOnAddMoreTimeDialogListener(new AddMoreTimeDialog.OnAddMoreTimeDialogListener() {
            @Override
            public void onOK(int minutes) {
                handleAddMoreTime(minutes);
            }

            @Override
            public void onCancel() {
            }
        });
        dialog.setUpData(booking.start, booking.end);
    }

    private void showUnAvailableDialog() {
        UnAvailableDialog dialog = new UnAvailableDialog(getContext());
        dialog.show();
    }

    private void handleAddMoreTime(int minutes) {
        int bookingId = booking.id;
        JSONObject jsonObject = new JSONObject();

        long startTimeInMilis = booking.start - DateUtils.getTimeZoneOffset() - booking.start_timezone_offset;
        long endTimeInMilis = booking.end - DateUtils.getTimeZoneOffset() - booking.end_timezone_offset;

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(startTimeInMilis * 1000);
        Date start = calendar.getTime();

        calendar.setTimeInMillis(endTimeInMilis * 1000);
        calendar.add(Calendar.MINUTE, minutes);
        final Date end = calendar.getTime();
        try {
            jsonObject.put("start_string", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(start));
            jsonObject.put("end_string", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(end));
            jsonObject.put("extend_confirm", "True");
            jsonObject.put("start_timezone_offset", DateUtils.getTimeZoneOffset1());
            jsonObject.put("end_timezone_offset", DateUtils.getTimeZoneOffset1());
            jsonObject.put("date_created", booking.date_created);
            jsonObject.put("date_updated", booking.date_updated);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        progressBar.setVisibility(View.VISIBLE);
        VolleyUtils.getSharedNetwork().updateBookingById(getContext(), bookingId, jsonObject, new OnResponseModel<KZBooking>() {
            @Override
            public void onResponseSuccess(KZBooking model) {
                progressBar.setVisibility(View.INVISIBLE);
                booking = model;
                updateSuccess = true;

                CommonUtils.segmentTrackWithBooking(getActivity(), "Change end time successfully", model);

                actionLoadBookingCostRegularlyOnTimer(null);
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                //VolleyUtils.handleErrorRespond((BaseActivity) getActivity(), error);
                progressBar.setVisibility(View.INVISIBLE);
                showUnAvailableDialog();
                updateSuccess = false;
            }
        });
    }

    public void setBooking(KZBooking booking) {
        RideFragment.booking = booking;
        GeoDeviceActivity.booking = booking;
        if(booking!=null && isResumed){
            onResume();
        }
    }

    //private String CARSHARE_TOKEN = "CiRGNEZBRUExMy1DQ0RDLTExRkYtMjBGRS1ERDgyN0IzMkMxMTAStAxNSUlFb3dJQkFBS0NBUUVBcGhCUFlaVC9PUjRzV2xXd2Ztb0JQTjgyL0dRdk40UFZvanJTbXh2U0NEaUF1cDNSRktBb0ZMcWRDSm81aXdHK09KL0NEYTJUdllwci9vK2VOelBleXFDdUZUSEhjTWM2eWZ2NUk4ME9USHdrZ1Y3dkVZY214ZlM1QTByRS9BMTdSMTJDTUNObkRVbWY3YVFEMHdvM0lGaDd3UWs1Kzh2dTVaZVI1dUFwVGF6LzZYU29SQTU5REhCRlgyUTl1ODRjZlRadzV4UmdxZU5jdWdCSFFkRkoxYlRTUjRDckFReVUvanhyWS9MS01UV0ZxdUljY3g2UjhvS0RUTmJ3eDMrQUkzTWJZWXFRTlk5dElFNmZoKzFZaGllaDB2UHA3Z3lVamlzNnk5NGpsbW9PTDVoTDd6eHExNkMwZ29xZG9aTlNkQmFxTzJqejJGOGdsNmk1Q2ptY3B3SURBUUFCQW9JQkFEbzVuTU9mck04WGs4Q0dxKy9lOWhra1FKREZCY01tdjhoMmJIS0RFaVRjdnRIdjFLZXhNZXgycyttWXZaVk9JcWFTNlVUSGQvWXNxd25oekQwWnZFTjZqQjhWU1pMeTM5dUROQXNIOGp3Q0NRQ3RTeEJ2dlRvS1FMWjFTRlVCMnhnZUlyQURNYlk0ZXRFbFFVd0ZHR2dJYmplMEp2Ynd2WTRWazB3S2k2RFdkaFp5SXlUL2hLN3FYN3NmMkJyWllQdzZ3Zy9tSWIzV1l0RTg5VXJsUjROaVBRSTk1UHVaZm14cWNSUndHSitYOS82bk9kb2lmTVNnTk9RWi9NWmJrTVNHV3l2L29aYWRjazlQWU5hekg4OVo4Vnpqd3JETTc1Tmp3OGlPWlVRakVQWk05b2FhSFBxaGZBaXJqKytLMklYb1lhcHhKRXpMRkdNcHAzb0RSQjBDZ1lFQTdPWXNDQ3BGOFIvNHNjMEdKemQ4WVAwZFBYZ2VHd0IrQzVQOHZERlBpYys4RU54bnVIZzZ3UnFCMVA2U2p2M3pHdHBFWm9QWTlOVHNuL2pwNmdmK0ZVNjZHRTJxZDRFcERuUnhCZXpEb1FnL01yMnVoMEl4RVlUVUNqdmRMUVdGaUVXOUVxNmtaeE83d0c5Z2t2MGNwQlZ4U2QyckpCNDRMMU9tZU9tWStpTUNnWUVBczNRR3plTmFXeUlTRmxabFA3eGhWTnBiNENvOHphTFg4WVp6bDRzNllnRCttRFAzMFUwbVJtc2k0SVIwK0NGVk1iU2ZTWHhSSGlSZGIrZTduam54KzZrMTh1bjBYY1owQjRJUUpQZFRkNHlZSm93QTFidTdWVGpBa3hyMktnQy9xQ3hQOVpxM1ZoZm0vdlV1NFk4MEp4OWZZNGc2cmxKeGU5M0RZQ1QyMGEwQ2dZQTRraFo5dEtxVEZCRnRWQXBlSHcvaWtVOEVPTVRucWN4dnMxMHRDNUYwMVZpUnBqQldpczl0QVlsemRYOUdDanFkZFBYVWpUT244NWkxOHRqWS9DT2VydmdkaVo1bGYwQndwVG5BTW5QYzVuMUEvcTdJdWZQQnZUaHRZVEpGNkFXbEx4SWhmZmJnYm9QdFJpdUhyazNDbVNRK2dXSzI4MGh6ZE1SU0oyWlNoUUtCZ0hlWlZvZVZoTHpFSWtYYzRySHR2UGl1WnhrcFRVakh4Ui9sT2RqQUkvTDVNclVCdmFJeXh0V2J4YXpZMnBRY3N3ampsbDJGaXhLSnJmUk0vMC9lMHBXbEdwMkFxeXBzZnh3WjVPU2VLWFM5c1JIUHNTYkJiWUxJeGgvRWtsZEFGSHZpdkkrMXc0YVBlNVprbGhBaVB1SHNHTWpmSEN6ZUtXQWlWbm9YYUtBcEFvR0JBT0R1LzUyeTZnV013cE5QSUt4U2JYamNGYWNkVmRiVnRlViswWi9VYTFyNlczY0dqbnk2UVU2SHhubTYwUlRPNlA4cjN2dmtYZEUyME1EWlpOeGtmNjNzcWNYVUg5Vk9FTjErbVZiN0xYMGFpc0ZyVENZSmNseXpZb1BQSkQ0bk14Smg1cTdVdTlpRFVHSEJvK2REODBqSmpYN3A2M1NBTHlBWTQvdERUTXhtGiDjWlJCmKe4Sd48njbT+wVJh30rtCPafbzjVIu1GzX1XCIg+Aln7extVp1xFVzmYlhTSokh/xVdkH+oN4ttS/TunGoq1wIKgAKmEE9hlP85HixaVbB+agE83zb8ZC83g9WiOtKbG9IIOIC6ndEUoCgUup0ImjmLAb44n8INrZO9imv+j543M97KoK4VMcdwxzrJ+/kjzQ5MfCSBXu8RhybF9LkDSsT8DXtHXYIwI2cNSZ/tpAPTCjcgWHvBCTn7y+7ll5Hm4ClNrP/pdKhEDn0McEVfZD27zhx9NnDnFGCp41y6AEdB0UnVtNJHgKsBDJT+PGtj8soxNYWq4hxzHpHygoNM1vDHf4AjcxthipA1j20gTp+H7ViGJ6HS8+nuDJSOKzrL3iOWag4vmEvvPGrXoLSCip2hk1J0Fqo7aPPYXyCXqLkKOZynEJ7SYBokOTlhYzQ1MzUtMGZmNy00N2Q1LWE5ODMtZTY1MzVkYTg0NTE4ILKjrogCKhEKCFRhbiBDb25nEgUN/w8AADC1xWA4ntJgUgcKBQ0QAAAAMoACMgoXnMJuhM9dlGuklc024o8Cl2p+UWUJNYrkxZllIF20+B/7euKmlYBC83KEMrkXeGMoioX6TENYrms6j1S9z7ktLZnuuOmQ/DB3DtB/SJifFmkhDT9w5EtAcyLnMiHbeLVwr667En6rqWJQ7UAfA6RSYFHvTgGzyj8D3ynMiI0dbpgGspw1geSPDlVwz4PN2RLgjGvABnT6+5BBX8p0HVR4o0nFmWgehMruRcL424Q9vXnON+HXwPSqfnR2EkxkB9vmwKGCEMvxqufjNdrMsi4P/zCd+uo7PrTKNM9fnVHwkXc3bf4ZxOXgUnVdra5IMpi2MpA9YEbyCLvHlA4mxA==";
    private String CARSHARE_TOKEN = "CiRGNEZBRUExMy1DQ0RDLTExRkYtMjBGRS1ERDgyN0IzMkMxMTAStAxNSUlFb1FJQkFBS0NBUUVBamNiM0J1dmhCUktSbTNuRlBmN01XK1RWUy9yOFpKWVBmY0xiUTkvMGFnaGlRTVlWaEh0eUt1ZnVNOVJQRDh1VEdCeGFuKzhvZ21XMmx3L3dFdFhCWFR5RVRPdFp0bi9LZ2p5QitTdDlwL2czSjNMYjVFNnkyYUpnZ09aZGFpVVVUeXVGUXdyd2xyM2dzcHpxdUZUOW80WHVlcjNiWUpUejhYMmlpSTVmb2xYSmlwWUVoTE0vb21XNWpOTW1lM0J5VklNYnlPcnd6MzlaN2pzU2hxcXllWXhjNnZDWVA0bkk3VTc2QlFVMjVjTHpkaE1LRGZEdGdHbWxNZGRITGRHS3k3aXRmREpGWElkdXo2b2wxcCtsaUQrQzN6cmcybDQ3eGYzYjF4cHY0TVltV0tIbzE1UjVHWUZLTEZDV080YlkyZk04Unhzam1ZWGI2NktxTFg3MytRSURBUUFCQW9IL0NTTkp5S2hyV0doWitpdGZDeC9oREFzenRESDVFb0taZlFHU0lFM084d2FYVHBWMXpaUW5SNXlyUGF2eGJKTXlaOXRyKytMdU9YbFV5MmU3a1hzMWUwenJPZ254UGxIK08yYXErZVJUdFAvS2prQlk2OTRtcU9CT1dBQ0JCTlplK0x0VlczelpCTjQ0SFVyLzFlR2tkMldxZVQ3RFpQWTB3dHZXSGZjUE11YzhBWlNBU0NCS3crNGhOazkwNy9kUzFBbll4N3hlZVN5UFdOUENYeFhGNE9tMWN2UTMrQ3RZZHZHRENhYUNtWEw2Y1pNc1FTTFFQa1pWcTlLZ2NsNm5hL1NzR0UwVEFGdU9jZUdmWUV1cGJTVkd3NmRmSEtET1hsTGVNYkNJenBnMjUveWRrVHJRcXpBcFFSeXZFQWRRTEhKZnRoVVhTWlhYUmx2VmZxNUpBb0dCQU1iT2RxL2J0MG9vTVVuOVhMN05GOEM5eE1jaWNRR3B4WXpIaDhwUW5RK1YvbTFtSmhUSkdJVTNyUmVTYXZVWjFOWlNZaEJDbTdVUjRPQ0t6cnZOZnFoOVpJN3k1a2pjZk5RUTZVeSt0VTVzazVnWDJKVTROYmJSUFhXeVZRRVE5cW5VVW5lSXBMM2U1MGd5d28zU0cvN3VVVmFqZU51bWErRHRhdytISzZkVkFvR0JBTGFRZEkwYlg2bVZ5WFVjQnkySXdVQnFWc1FSelBPbzc1YUZpdGlHQzJWa2pKU1J4K2wvOEVHaG9qL1U3dGtTWVVXWE9zM2xranBPSXJYNTdLWTVseU9hTTZHZys4bERDYlVWRFdDOEdFdm9XekgreFFsem5pZnErc3EzUmNReXFGMTViaUxrdTBDdTZqa3Jub0hTdWxxcGllTXY4dXpnMmUybHlVOVloVVlWQW9HQVpQYkh1ZW5vdk9XS21wdnNkUTN0YVRvMjdEYm1rMFNvQ2lYdkpXVXBUcGdMWFgzS0N6WlJtSjQ4N3NLeVJRVzRYSnY4SjJ2Y2JlbkZmZU9ndUNuVmtEWVdVcTZ6R3YrMitYQVpPNXVDc25CYnlKc2JxRFNpdXRTcTFOSWJiMjR4QnZESWJPSmRYL3dMRTFTTUQ4Qkw3WDBVbTk1cmE0bXFNZElCVkNKK25mMENnWUVBa01ZZlZ5TUZDcE80N1lDcmhJVjFVdUxCS2IwLy9WM1RtVmNQVTBPQldBZVRuK0czV2NqV093VzFoODhSRmQwSWZFcEFITFZYNzVxMDFnQjZsUnNJYXZoR3gzN0ZETVozdFQwYm5jR3J0czV0TzBsZzhXelpWNmgwOXpxOVBhZzZjZDN3NUFUbEpaZFFNTVFSYjd3dmdlUCtiOFkvYTIrb0FsZzlPd25rcmowQ2dZQUI5aWVySWpRMDZvdDVCdFNDYjQvcDc5Mkdhdk9RanNTVzBsN0pNZGpLRFJFOW8xdDlBcWpKMVRvdU04QWkwcDdzMExQOTl0ZDYxTjRPUVFzK1J3bUIrbVhCTlFBUkc2SW9hZVkyNEZiVmNleWxyTENXd09GT3AwWXlyOGNFTjMvRUV5bW1sVkdLMHM2eFdxU0dXRjRWU3VudTRKbEdEdUtZWHNmUGlkaTkwZz09GiBu6IhQhzTjwZFhKfK9ykinKbW//e433GLgUSmvubpW1yIgwfGy690XnMgFE2tYQo3sQMBw4oABH61vQ4LXGWwvAe4q3QIKgAKNxvcG6+EFEpGbecU9/sxb5NVL+vxklg99wttD3/RqCGJAxhWEe3Iq5+4z1E8Py5MYHFqf7yiCZbaXD/AS1cFdPIRM61m2f8qCPIH5K32n+DcnctvkTrLZomCA5l1qJRRPK4VDCvCWveCynOq4VP2jhe56vdtglPPxfaKIjl+iVcmKlgSEsz+iZbmM0yZ7cHJUgxvI6vDPf1nuOxKGqrJ5jFzq8Jg/icjtTvoFBTblwvN2EwoN8O2AaaUx10ct0YrLuK18MkVch27PqiXWn6WIP4LfOuDaXjvF/dvXGm/gxiZYoejXlHkZgUosUJY7htjZ8zxHGyOZhdvroqotfvf5ENfd9fMFGiRhNzEwMDk4MC1iYWE0LTQ3ODAtYjU0Zi1hOGRmOTVkYjQ2OWEgsqOuiAIqEQoIVGFuIFRlc3QSBQ3/DwAAMNfm3PIFONfd9fMFUgcKBQ0QAAAAMoACBXoFdFxHrFRuqxqXUVJT28sMPDpekouIJM1Fr9vFIej2jnvGyzUViEqwhL0S5sDHU1GD65cK9RMzKSkqoPsOQhr5mkC6C0R9AxfLDjg3yWWbJKBrOXyrAQhKxeYy6HPmaFKWtPziFDPEBV0P/M54G0OBVqs5BWlZ5kQiva+pugJ0sg66N0ANa7RvDgruqoGaSCbQdFd9y+h/IJ0njZoknT1FRc0IcYNvZjtn/ZVKihxQK/blWOh/DxM33SQHpcrHlMpYpKM1h3/JGjG0TJpI0mYonfeL1uYjO2VsSr5ZoaB2EuSlfnd/oUBGX1hkJT2T2YvjBfhRInVHvcxdamnmQw==";

    public String getCarshareToken() {
        return booking.geotab_carshare_token;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void updateAllData(BluetoothCarShareStatus bluetoothCarShareStatus, Command carShareCommand) {
        updateAllData(bluetoothCarShareStatus, carShareCommand, null);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void updateAllData(BluetoothCarShareStatus bluetoothCarShareStatus, Command carShareCommand, Error careShareError) {
        //ToastUtil.showToastMessage(getActivity(),"updateAllData bCSS Code: "+bluetoothCarShareStatus, true);
        if(view==null) return;

        if(null == bluetoothCarShareStatus)
            return;
        String msgBluetooth = "";
        String msgCarShare = "";
        int colorMsgBluetooth = R.color.geoTab_tvBrandColor;
        int colorMsgCarShare = R.color.geoTab_tvBrandColor;

        boolean enableButtonBluetoothConnect = true;
        boolean enableButtonCarShareConnect = true;
        boolean enableButtonCarShareLock = true;
        boolean enableButtonCarShareUnlock = true;

        //hideProcessView();
        switch (bluetoothCarShareStatus){
            case BLUETOOTH_NOT_SUPPORT:
                //ToastUtil.showToastMessage(getActivity(), "updateAllData() BLUETOOTH_NOT_SUPPORT", true);
                msgBluetooth = "Bluetooth adapter is not supported.";
                colorMsgBluetooth = R.color.red;

                msgCarShare = "Bluetooth adapter is not supported. \nCannot connect to the Carshare device.";
                colorMsgCarShare = R.color.red;

                enableButtonBluetoothConnect = false;
                enableButtonCarShareConnect = false;
                enableButtonCarShareLock = false;
                enableButtonCarShareUnlock = false;
                break;
            case BLUETOOTH_DISABLE:
            case BLUETOOTH_STATE_OFF:
                //ToastUtil.showToastMessage(getActivity(), "updateAllData() BLUETOOTH_DISABLE/STATE OFF", true);
                msgBluetooth = "Bluetooth adapter is off. \nPlease turn on the Bluetooth adapter.";
                colorMsgBluetooth = R.color.red;

                msgCarShare = "Bluetooth adapter is off. \nCannot connect to the Carshare device.";
                colorMsgCarShare = R.color.red;

                enableButtonBluetoothConnect = true;
                enableButtonCarShareConnect = false;
                enableButtonCarShareLock = false;
                enableButtonCarShareUnlock = false;
                break;
            case BLUETOOTH_ENABLE:
            case BLUETOOTH_STATE_ON:
                //ToastUtil.showToastMessage(getActivity(), "updateAllData() BLUETOOTH_ENABLE/STATE ON", true);
                msgBluetooth = "Bluetooth adapter is on, and ready for use.";
                colorMsgBluetooth = R.color.geoTab_tvBrandColor;

                msgCarShare = "Bluetooth adapter is on, and ready for use. \nPlease connect to the Carshare device.";
                colorMsgCarShare = R.color.geoTab_tvBrandColor;

                enableButtonBluetoothConnect = false;
                enableButtonCarShareConnect = true;
                enableButtonCarShareLock = false;
                enableButtonCarShareUnlock = false;
                break;
            case CAR_SHARE_CONNECTED:
                //ToastUtil.showToastMessage(getActivity(), "updateAllData() CAR_SHARE_CONNECTED", true);
                msgCarShare = "The Carshare device is connected and ready for lock/unlock.";
                colorMsgCarShare = R.color.geoTab_tvBrandColor;

                if(null != bluetoothAdapter() && bluetoothAdapter().enable()){
                    enableButtonBluetoothConnect = false;
                }
                enableButtonCarShareConnect = true;
//                enableButtonCarShareConnect = false;
                enableButtonCarShareLock = true;
                enableButtonCarShareUnlock = true;
                if(null != carShareClient()){
//                    showProcessView();
                    ((MainActivity)getActivity()).actionCheckIn();
                }
                break;
            case CAR_SHARE_DISCONNECT:
                //ToastUtil.showToastMessage(getActivity(), "updateAllData() CAR_SHARE_DISCONNECT", true);
                msgCarShare = "The Carshare device is disconnect. \nPlease connect to the Carshare device.";
                if(null != careShareError){
                    msgCarShare += "\n\n" + careShareError.toString();
                }
                colorMsgCarShare = R.color.red;

                if(null != bluetoothAdapter() && bluetoothAdapter().enable()){
                    enableButtonBluetoothConnect = false;
                }
                enableButtonCarShareConnect = true;
                enableButtonCarShareLock = false;
                enableButtonCarShareUnlock = false;
//                enableButtonCarShareLock = false;
//                enableButtonCarShareUnlock = false;
                break;
            case CAR_SHARE_COMMAND_SUCCESS:
                //ToastUtil.showToastMessage(getActivity(), "updateAllData() CAR_SHARE_COMMAND_SUCCESS", true);
                if(null == carShareCommand){
                    msgCarShare = "The Carshare device is commanded successful.";
                } else {
                    msgCarShare = "The Carshare device is " + carShareCommand.name() +" successful.";
                }
                colorMsgCarShare = R.color.geoTab_tvBrandColor;

                if(null != bluetoothAdapter() && bluetoothAdapter().enable()){
                    enableButtonBluetoothConnect = false;
                }
                enableButtonCarShareConnect = true;
//                enableButtonCarShareConnect = false;
                enableButtonCarShareLock = true;
                enableButtonCarShareUnlock = true;
                break;
            case CAR_SHARE_COMMAND_FAILED:
                //ToastUtil.showToastMessage(getActivity(), "updateAllData() CAR_SHARE_COMMAND_FAILED", true);
                if(null == carShareCommand){
                    msgCarShare = "The Carshare device is commanded failed.";
                    if(null != careShareError){
                        msgCarShare += "\n\n" + careShareError.toString();
                    }
                } else {
                    msgCarShare = "The Carshare device is " + carShareCommand.name() +" failed.";
                    if(null != careShareError){
                        msgCarShare += "\n\n" + careShareError.toString();
                    }
                }
                colorMsgCarShare = R.color.red;

                if(null != bluetoothAdapter() && bluetoothAdapter().enable()){
                    enableButtonBluetoothConnect = false;
                }
                enableButtonCarShareConnect = true;
//                enableButtonCarShareConnect = false;
                enableButtonCarShareLock = true;
                enableButtonCarShareUnlock = true;
                break;
            default:
                break;
        }
        if(! TextUtils.isEmpty(msgBluetooth)) {
            tvBluetooth.setText(msgBluetooth);
            tvBluetooth.setTextColor(getResources().getColor(colorMsgBluetooth));
        }
        if(! TextUtils.isEmpty(msgCarShare)) {
            tvCarShare.setText(bluetoothCarShareStatus+": "+msgCarShare);
            tvCarShare.setTextColor(getResources().getColor(colorMsgCarShare));
        }

        btnBluetoothEnable.setEnabled(enableButtonBluetoothConnect);
        btnBluetoothEnable.setClickable(enableButtonBluetoothConnect);

        btnCarShareConnect.setEnabled(enableButtonCarShareConnect);
        btnCarShareConnect.setClickable(enableButtonCarShareConnect);

        btnCarShareLock.setEnabled(enableButtonCarShareLock);
        btnCarShareLock.setClickable(enableButtonCarShareLock);

        btnCarShareUnlock.setEnabled(enableButtonCarShareUnlock);
        btnCarShareUnlock.setClickable(enableButtonCarShareUnlock);

        btnBluetoothEnable.setAlpha(enableButtonBluetoothConnect?1.0f:0.5f);
        btnCarShareConnect.setAlpha(enableButtonCarShareConnect?1.0f:0.5f);
        btnCarShareLock.setAlpha(enableButtonCarShareLock?1.0f:0.5f);
        btnCarShareUnlock.setAlpha(enableButtonCarShareUnlock?1.0f:0.5f);

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private CarShareClient carShareClient() {
        return ((MainActivity)getActivity()).carShareClient;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private BluetoothAdapter bluetoothAdapter() {
        return ((MainActivity)getActivity()).bluetoothAdapter;
    }

}
