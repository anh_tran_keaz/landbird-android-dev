package com.keaz.landbird.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.keaz.landbird.R;
import com.keaz.landbird.activities.AccountActivity;
import com.keaz.landbird.activities.AddPaymentActivity;
import com.keaz.landbird.activities.ApprovedBookingActivity;
import com.keaz.landbird.activities.BaseActivity;
import com.keaz.landbird.activities.BookingListActivity;
import com.keaz.landbird.activities.DriverLicenseActivity;
import com.keaz.landbird.activities.DriverLicenseActivityEnvoy;
import com.keaz.landbird.activities.RegisterCardActivity;
import com.keaz.landbird.adapters.CustomRecyclerAdapter_VehicleOfASelectedBranch;
import com.keaz.landbird.dialogs.ConfirmReservationDialog;
import com.keaz.landbird.enums.PAYMENT_REQUIRED_ENUM;
import com.keaz.landbird.helpers.TimeHelper;
import com.keaz.landbird.interfaces.CustomActionListener2;
import com.keaz.landbird.interfaces.CustomListener4;
import com.keaz.landbird.models.KZAccount;
import com.keaz.landbird.models.KZAddOn;
import com.keaz.landbird.models.KZBooking;
import com.keaz.landbird.models.KZBranch;
import com.keaz.landbird.models.KZObjectForMixpanel;
import com.keaz.landbird.models.KZObjectTimeLine;
import com.keaz.landbird.models.KZPaymentDetail;
import com.keaz.landbird.models.KZPaymentMethod;
import com.keaz.landbird.models.KZSearchResult;
import com.keaz.landbird.models.KZVehicle;
import com.keaz.landbird.models.XModel;
import com.keaz.landbird.utils.BKNetworkResponseError;
import com.keaz.landbird.utils.CommonUtils;
import com.keaz.landbird.utils.ConfigUtil;
import com.keaz.landbird.utils.ConstantsUtils;
import com.keaz.landbird.utils.DateUtils;
import com.keaz.landbird.utils.DialogUtils;
import com.keaz.landbird.utils.FormatUtil;
import com.keaz.landbird.utils.JsonUtils;
import com.keaz.landbird.utils.LoadingViewUtils;
import com.keaz.landbird.utils.MakeBookingUtil;
import com.keaz.landbird.utils.MyLog;
import com.keaz.landbird.utils.OnBookingFragmentListener;
import com.keaz.landbird.utils.OnResponseModel;
import com.keaz.landbird.utils.UIUtils;
import com.keaz.landbird.utils.ViewUtil;
import com.keaz.landbird.utils.VolleyUtils;
import com.keaz.landbird.view.CustomFontTextView;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;
import static com.keaz.landbird.utils.ConfigUtil.actionDefineTripType;
import static com.keaz.landbird.utils.LoadingViewUtils.showOrHideProgressBar;


/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnBookingFragmentListener}
 * interface.
 */
public class VehicleOfBranchFragment extends Fragment {

    private static final String TAG = VehicleOfBranchFragment.class.getSimpleName();
    public static boolean created = false;
    private static boolean allowBackPress = true;
    private ArrayList<KZVehicle> vehicles;
    private CustomRecyclerAdapter_VehicleOfASelectedBranch adapter;
    private int vehicleSelectedIndex = -1;
    private int mBlockBookingTime;
    private String mDuration = "";
    private String mTimeStart = "";
    private String mTimeEnd = "";
    private ArrayList<KZObjectTimeLine> bookings;
    private boolean isToDay;
    private KZSearchResult kzSearchResult;
    private KZBranch branch;
    private CustomFontTextView tvDate, tvTime;
    private Date mSelectedDate;
    private Date mSelectedDateTo;
    Calendar mSelectedCalendar;
    Calendar mSelectedCalendarTo;
    private Calendar mNowCalendar;
    private int duration = 0;
    private RelativeLayout mRelEmptyVehicle;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private boolean isConfirmDialogShowing;
    private View view;
    private int selected_drop_off_branch_id;
    private String selected_drop_off_time_zone_offset;
    private String selected_drop_off_branch_name;
    private CustomFontTextView tvTimeFrom;
    private CustomFontTextView tvDayFrom;
    private CustomFontTextView tvTimeTo;
    private CustomFontTextView tvDayTo;
    private KZBranch selected_drop_off_branch;
    private boolean concierge_delivery_enable;
    private String concierge_delivery_place;
    private String concierge_delivery_note;
    private boolean concierge_collection_enable;
    private String concierge_collection_place;
    private boolean resetTime = true;
    private String trip_type = ConstantsUtils.TRIP_TYPE_BUSINESS;
    private ArrayList<KZAddOn> kzAddOns;
    private JsonArray addOnsArrObj;
    private ArrayList<String> selectedAddOns = new ArrayList<>();

    public static VehicleOfBranchFragment newInstance(KZBranch branch, KZSearchResult kzSearchResult, int pos, ArrayList<KZVehicle> todayVehicles, ArrayList<KZObjectTimeLine> todayBookings, boolean ifTodaySoGrayOutThePass) {
        VehicleOfBranchFragment fragment = new VehicleOfBranchFragment();
        Bundle args = new Bundle();
        args.putParcelable("branch", branch);
        args.putParcelable("kz_search_result", kzSearchResult);
        args.putParcelableArrayList("vehicles", todayVehicles);
        args.putParcelableArrayList("bookings", todayBookings);
        args.putBoolean(ConstantsUtils.INTENT_EXTRA_IS_TODAY, ifTodaySoGrayOutThePass);
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBlockBookingTime = (int) (KZAccount.getSharedAccount().company.company_booking_block_duration / 60);

        if (getArguments() != null) {
            branch = getArguments().getParcelable("branch");
            vehicles = getArguments().getParcelableArrayList("vehicles");
            bookings = getArguments().getParcelableArrayList("bookings");
            isToDay = getArguments().getBoolean(ConstantsUtils.INTENT_EXTRA_IS_TODAY);
            kzSearchResult = getArguments().getParcelable("kz_search_result");
            selected_drop_off_branch_id = branch.id;
            selected_drop_off_time_zone_offset = branch.time_zone_offset;
            selected_drop_off_branch_name = branch.name;
            selected_drop_off_branch = branch;
        }

        switch (ConfigUtil.getConfigTripType()){
            case ConstantsUtils.TRIP_TYPE_BOTH:
                trip_type = ConstantsUtils.TRIP_TYPE_BUSINESS;
                break;
            case ConstantsUtils.TRIP_TYPE_BUSINESS:
                trip_type = ConstantsUtils.TRIP_TYPE_BUSINESS;
                break;
            case ConstantsUtils.TRIP_TYPE_PRIVATE:
                trip_type = ConstantsUtils.TRIP_TYPE_PRIVATE;
                break;
        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_vehicle_of_branch, container, false);

        mRelEmptyVehicle = view.findViewById(R.id.rel_empty_vehicle);
        mRelEmptyVehicle.setVisibility(View.GONE);

        iniView();
        initBookingList(view);
        initDateTimePicker(view);
        initDateTimePicker2(view);
        loadAddOn();
        return view;
    }

    private void iniView() {
        UIUtils.setTextViewText(view, R.id.branchName, branch.name);
        UIUtils.setTextViewText(view, R.id.branchAddress, branch.address);

        view.findViewById(R.id.rel_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actionBack();
            }
        });
        view.findViewById(R.id.icMenu).setVisibility(View.GONE);
        view.findViewById(R.id.icMenu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtil.showMenu(v, getActivity(),R.menu.menu_1,new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()){
                            case R.id.item1:
                                Intent intent = new Intent(getActivity(), AccountActivity.class);
                                startActivity(intent);
                                break;
                        }
                        return false;
                    }
                });
            }
        });
    }

    private void actionBack() {
        if(isVisible()) {
            onBackPressed();
        } else {
            getActivity().finish();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if(!isConfirmDialogShowing && resetTime) {
            setDateTimePresent();
        }
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }
    @Override
    public void onDetach() {
        super.onDetach();
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case ConstantsUtils.REQUEST_CODE_SELECT_OR_ADD_PAYMENT_METHOD:
                if(resultCode==RESULT_OK){
                    final String paymentMethodID = data.getStringExtra(ConstantsUtils.INTENT_EXTRA_PAYMENT_METHOD_ID);
                    resetTime = false;
                    actionCheckIfNeedShowBookingPolicy(new CustomListener4() {
                        @Override
                        public void returnFalse() {
                            actionCallApiToCreateBooking(vehicles.get(vehicleSelectedIndex), paymentMethodID, "actionClickOkBtnOfConfirmBookingDialog");
                        }

                        @Override
                        public void returnTrue(String s) {
                            DialogUtils.showMessageDialog(getActivity(), true,
                                    getActivity().getString(R.string.title_usage_policy),
                                    s,
                                    getActivity().getString(R.string.btn_yes),
                                    getActivity().getString(R.string.btn_no),
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.cancel();
                                            actionCallApiToCreateBooking(vehicles.get(vehicleSelectedIndex), paymentMethodID, "actionClickOkBtnOfConfirmBookingDialog");
                                        }
                                    },
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.cancel();
                                        }
                                    });
                        }
                    });
                }
                break;
        }
    }

    public void onBackPressed() {
        if(allowBackPress){
            getActivity().finish();
            if (VehicleOfBranchFragment.created) {
                getActivity().setResult(RESULT_OK);
            }
        }
    }

    public void initDateTimePicker(View view) {
        tvDate = (CustomFontTextView) view.findViewById(R.id.tv_title_day);
        tvTime = (CustomFontTextView) view.findViewById(R.id.tv_title_time);

        tvDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int year = mSelectedCalendar.get(Calendar.YEAR);
                int month = mSelectedCalendar.get(Calendar.MONTH);
                int day = mSelectedCalendar.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog;
                datePickerDialog = new DatePickerDialog(
                        getActivity(),
                        DatePickerDialog.THEME_HOLO_LIGHT, new DatePickerDialog.OnDateSetListener() {

                    public void onDateSet(DatePicker datePicker, int selectedYear, int selectedMonth, int selectedDay) {
                        Calendar tempSelectedCal = Calendar.getInstance();
                        tempSelectedCal.setTime(mSelectedCalendar.getTime());
                        tempSelectedCal.set(Calendar.YEAR, selectedYear);
                        tempSelectedCal.set(Calendar.MONTH, selectedMonth);
                        tempSelectedCal.set(Calendar.DAY_OF_MONTH, selectedDay);
                        Date date = tempSelectedCal.getTime();
                        String tvDateValue = new SimpleDateFormat("dd MMM", Locale.getDefault()).format(date);

                        CommonUtils.segmentTrackWithStartDate(getContext(), "Choose Start Date", branch, date);

                        if(checkAvailableDateTime(tempSelectedCal)) {
                            mSelectedCalendar.set(Calendar.YEAR, selectedYear);
                            mSelectedCalendar.set(Calendar.MONTH, selectedMonth);
                            mSelectedCalendar.set(Calendar.DAY_OF_MONTH, selectedDay);
                            tvDate.setText(tvDateValue);
                            actionSearchVehicle();
                        }else{
                            Toast.makeText(getActivity(),"Please choose booking time in the future" , Toast.LENGTH_SHORT).show();
                        }

                    }
                }, year, month, day);
                datePickerDialog.setTitle("Select Date");
                datePickerDialog.show();
            }
        });

        tvTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTimePickerListStyle();
            }
        });
    }

    private void initDateTimePicker2(View view) {

        tvTimeFrom = view.findViewById(R.id.tv_title_time_from);
        tvDayFrom = view.findViewById(R.id.tv_title_day_from);
        tvTimeTo = view.findViewById(R.id.tv_title_time_to);
        tvDayTo = view.findViewById(R.id.tv_title_day_to);

        View viewFrom = view.findViewById(R.id.from);
        View viewTo = view.findViewById(R.id.to);

        viewFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                actionClickFromView();
            }
        });
        viewTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                actionClickToView();
            }
        });

    }

    private void loadUserForMixPanel(String userId, final KZBooking booking) {
        showOrHideProgressBar(true, view);
        VolleyUtils.getSharedNetwork().loadUserForMixpanel(getContext(), userId, new OnResponseModel() {
            @Override
            public void onResponseSuccess(Object model) {
                // MixPanel
                Gson gson = new Gson();
                KZObjectForMixpanel kzObjectForMixpanel = gson.fromJson(model.toString(), KZObjectForMixpanel.class);

                CommonUtils.segmentIdentifyWithExpandUser(getContext(), KZAccount.getSharedAccount().user, kzObjectForMixpanel, 0.0f);
                CommonUtils.segmentTrackWithBooking(getContext(), "Create booking successfully", booking);
                // ------

                created = true;

                String nowDayMonthYear = new SimpleDateFormat("dd-MMM-yyyy hh:mm", Locale.getDefault()).format(mNowCalendar.getTime());
                String selectedDayMonthYear = new SimpleDateFormat("dd-MMM-yyyy hh:mm", Locale.getDefault()).format(mSelectedCalendar.getTime());

                if (nowDayMonthYear.equals(selectedDayMonthYear)) {
                    actionOpenBeginReservationBookingActivity(booking);
                } else {
                    Intent intent = new Intent(getActivity(), BookingListActivity.class);
                    getActivity().startActivity(intent);
                }
                showOrHideProgressBar(false, view);
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                VolleyUtils.handleErrorRespond(getContext(), error);
                showOrHideProgressBar(false, view);
            }
        });
    }

    private void actionCallApiToCreateBooking(KZVehicle vehicle, final String kzPaymentMethodId, String from) {
        Log.d("createbooking" , from);
        ConfigUtil.setUsagePolicy(actionDefineTripType());

        if (!(mSelectedCalendarTo.getTime().after(mSelectedCalendar.getTime()))) {
            UIUtils.toast(getActivity(), "Please choose time.");
            return;
        }
        JSONObject jsonParams = new JSONObject();
        try {
            final int drop_off_branch_id;
            int branch_id;
            String start_timezone_offset = vehicle.branch.time_zone_offset;
            String end_timezone_offset  = vehicle.branch.time_zone_offset;
            if(ConfigUtil.checkIfEnableDropOff() && branch.enable_migrate_vehicle.equalsIgnoreCase("true")){
                /*if(selected_drop_off_branch_id==-1){
                    ToastUtil.showToastMessage(getActivity(), "Please select drop-off branch", false);
                    return;
                }*/
                drop_off_branch_id = selected_drop_off_branch_id;
                end_timezone_offset = selected_drop_off_time_zone_offset;
                branch_id = vehicle.branch.id;
            }else {
                branch_id = drop_off_branch_id = vehicle.branch.id;
            }
            final int vehicle_id = vehicle.id;


            Date startDate = mSelectedCalendar.getTime();
            Date endDate = mSelectedCalendarTo.getTime();

            //Add start time zone offset
            long startTimeStamp;
            Calendar calendar_start = Calendar.getInstance();
            calendar_start.setTime(startDate);
            startTimeStamp = calendar_start.getTimeInMillis();
//            int start_timezone_offset = DateUtils.getTimeZoneOffset1(startTimeStamp);

            long endTimeStamp;
            Calendar calendar_end = Calendar.getInstance();
            calendar_end.setTime(endDate);
            /*calendar_end.setTimeInMillis(calendar_end.getTimeInMillis() + duration * 60 * 1000l);*/
            endTimeStamp = calendar_end.getTimeInMillis();

            endDate = calendar_end.getTime();
//            int end_timezone_offset = DateUtils.getTimeZoneOffset1(endTimeStamp);

            final String startDateStr = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(startDate);
            String startHourStr = calendar_start.get(Calendar.HOUR_OF_DAY) +"";
            String startMinStr = calendar_start.get(Calendar.MINUTE) +"";
            final String startTimeStr = startHourStr + ":" + startMinStr;
            final String endDateStr = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(endDate);
            String endHourStr = calendar_end.get(Calendar.HOUR_OF_DAY) +"";
            String endMinStr = calendar_end.get(Calendar.MINUTE) +"";
            String endTimeStr = endHourStr + ":" + endMinStr;

            jsonParams.put("concierge_delivery_enable", concierge_delivery_enable);
            jsonParams.put("concierge_delivery_place", concierge_delivery_place);
            jsonParams.put("concierge_delivery_note", concierge_delivery_note);
            jsonParams.put("concierge_collection_enable", concierge_collection_enable);
            jsonParams.put("concierge_collection_place", concierge_collection_place);

            jsonParams.put("branch_id", "" + branch_id);
            jsonParams.put("drop_off_branch_id", "" + drop_off_branch_id);
            jsonParams.put("start_timezone_offset", start_timezone_offset);
            jsonParams.put("end_timezone_offset", end_timezone_offset);
            jsonParams.put("start_date", startDateStr);
            jsonParams.put("start_time", startTimeStr);
            jsonParams.put("end_date", endDateStr);
            jsonParams.put("end_time", endTimeStr);
            jsonParams.put("vehicle_id", "" + vehicle_id);
            jsonParams.put("cost-centre_id", "");

            jsonParams.put("recurring_enable", false);
            jsonParams.put("trip_type", actionDefineTripType());
            jsonParams.put("trip_types", vehicle.trip_types);
            jsonParams.put("user-id", "");
            jsonParams.put("sub_user_id", "");
            jsonParams.put("trip_purpose", "");
            jsonParams.put("node_id", "");
            jsonParams.put("node_sel", "");
            jsonParams.put("drop_off_node_sel", "");
            jsonParams.put("drop_off_node_id", "");
            jsonParams.put("payment_method_token", kzPaymentMethodId);
            jsonParams.put("addons", addOnsArrObj);

            showOrHideProgressBar(true, view);
            VolleyUtils.getSharedNetwork().createBooking(getContext(), jsonParams, new OnResponseModel<KZBooking>() {
                @Override
                public void onResponseSuccess(KZBooking booking) {
                    showOrHideProgressBar(false, view);

                    loadUserForMixPanel("" + KZAccount.getSharedAccount().user.id, booking);
                }

                @Override
                public void onResponseError(BKNetworkResponseError error) {
                    showOrHideProgressBar(false, view);
                    created = false;

                    final BKNetworkResponseError networkResponse = error;
                    if(networkResponse != null) {
                        if (networkResponse.description != null && !networkResponse.description.equals("")) {

                            if (networkResponse.code != null && (networkResponse.code.equals("booking_driver_expired") || networkResponse.code.equals("booking_driver_empty"))) {
                                DialogUtils.showMessageDialog(getActivity(), true, "", networkResponse.description,
                                        "Update driver license","Cancel",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.cancel();
                                                Intent intent = new Intent(getActivity(), DriverLicenseActivityEnvoy.class);
                                                intent.putExtra(ConstantsUtils.INTENT_EXTRA_OPEN_FROM_CONFIRM_BOOKING, true);
                                                startActivity(intent);
                                            }
                                        },
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.cancel();
                                            }
                                        });

                            } else if (networkResponse.code != null && networkResponse.code.equals("booking_payment_empty")) {
                                actionWhenThereIsNoPaymentIsFound(ConstantsUtils.PAYMENT_SITUATION_DRIVER);

                            }else if (networkResponse.code != null && networkResponse.code.equals("booking_payment_invalid")) {
                                /*DialogUtils.showMessageDialog(getActivity(), false, "Warning!",
                                        getActivity().getResources().getString(R.string.message_invalid_payment)+" "+KZAccount.getSharedAccount().company.contact_email, "Select another", "Cancel",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                actionWhenThereIsNoPaymentIsFound(ConstantsUtils.PAYMENT_SITUATION_DRIVER);
                                            }
                                        },
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                dialogInterface.cancel();
                                            }
                                        });*/

                                DialogUtils.showCustomDialog(getActivity(), true, "Warning!", getActivity().getResources().getString(R.string.message_invalid_payment) + " " + KZAccount.getSharedAccount().company.contact_email, "Select another", "Cancel",
                                        new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                actionWhenThereIsNoPaymentIsFound(ConstantsUtils.PAYMENT_SITUATION_DRIVER);
                                            }
                                        },
                                        new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                            }
                                        });
                            }if (networkResponse.code != null && networkResponse.code.equals("in_the_past")) {
                                ((BaseActivity)getActivity()).showAlertDialog("",
                                        getResources().getString(R.string.message_you_can_not),
                                        getContext().getString(R.string.btn_okie),
                                        getContext().getString(R.string.btn_cancel),
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.cancel();
                                                actionWhenGetCreateBookingErrorRespondCase1_InThePass(kzPaymentMethodId);
                                            }
                                        },
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                if (networkResponse.code.equals("in_the_past")) {
                                                    setDateTimePresent();
                                                }
                                            }
                                        }
                                );

                            } else if (networkResponse.code != null && networkResponse.code.equals("booking_too_short")) {
                                ((BaseActivity)getActivity()).showAlertDialog("",
                                        getResources().getString(R.string.message_too_close) + " " +ConfigUtil.getConfigCompanyBookingBlockDurationInMinute() + " " + getResources().getString(R.string.message_too_close2),
                                        getContext().getString(R.string.btn_okie),
                                        getContext().getString(R.string.btn_cancel),
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                actionWhenGetCreateBookingError_BookingIsTooShort(kzPaymentMethodId);
                                            }
                                        },
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                if (networkResponse.code.equals("in_the_past")) {
                                                    setDateTimePresent();
                                                }
                                            }
                                        }
                                );
                            } else {
                                ((BaseActivity)getActivity()).showAlertDialog("",
                                        networkResponse.description,
                                        getContext().getString(R.string.btn_okie),
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                setDateTimePresent();
                                                dialog.cancel();
                                            }
                                        });
                            }

                        } else {
                            ((BaseActivity)getActivity()).showAlertDialog("", getContext().getString(R.string.error_from_server));
                        }
                    }else {
                        ((BaseActivity)getActivity()).showAlertDialog("", getContext().getString(R.string.error_from_server));
                    }
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void notifyVehicleListChanged(KZSearchResult kzSearchResult, ArrayList<KZVehicle> todayVehicles, ArrayList<KZObjectTimeLine> todayBookings) {
        this.bookings = todayBookings;
        this.vehicles = todayVehicles;
        this.kzSearchResult = kzSearchResult;
        if (this.bookings == null)
            this.bookings = new ArrayList<>();

        if (this.vehicles == null)
            this.vehicles = new ArrayList<>();

        if (this.kzSearchResult == null)
            this.kzSearchResult = new KZSearchResult();

        if (adapter != null)
            adapter.notifyDataChanged(this.vehicles, this.bookings, mSelectedDate, this.kzSearchResult);
    }

    public void initBookingList(View view) {
        final RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.list);
        if (recyclerView != null) {
            Context context = view.getContext();
            recyclerView.setLayoutManager(new LinearLayoutManager(context));

            long branchTimezoneOffset = DateUtils.getTimeZoneOffset();
            if (kzSearchResult != null) {
                branchTimezoneOffset = Long.parseLong(kzSearchResult.start__timezone_offset);
            }
            mSelectedDate = DateUtils.getTodayWithBranchTimezone(branchTimezoneOffset);
            adapter = new CustomRecyclerAdapter_VehicleOfASelectedBranch(ConfigUtil.actionDefineTripType(), kzSearchResult, isToDay, getActivity(), vehicles, bookings, mSelectedDate, new OnBookingFragmentListener() {
                @Override
                public void onVehicleSelected(final KZVehicle vehicle, int position) {
                    vehicleSelectedIndex = position;
                    if (adapter != null) {
                        adapter.fireNotifyDataSetChanged();
                    }
                    actionSelectDropOffBranch(new CustomActionListener2() {
                        @Override
                        public void takeAction(final KZBranch[] kzBranches) {
                            Date start = mSelectedCalendar.getTime();
                            Date end = mSelectedCalendarTo.getTime();
                            VolleyUtils.getSharedNetwork().loadVehicleById(getActivity(), branch, vehicle.id, start, end, branch, selected_drop_off_branch, new OnResponseModel<XModel>() {
                                @Override
                                public void onResponseSuccess(XModel o) {
                                    KZVehicle a = o.vehicles.get(0);
                                    showConfirmDialog(a, branch, kzBranches);

                                }

                                @Override
                                public void onResponseError(BKNetworkResponseError error) {
                                    VolleyUtils.handleErrorRespond(getActivity(), error);
                                }
                            });

                        }
                    });
                }
            });
            /*adapter.setOnDateChangeListener(new OnDateChangeListener() {
                @Override
                public void onChange(String duration, String timeStart, String timeEnd, int hhStart, int minStart, int hhEnd, int minEnd) {
                    mDuration = duration;
                    mTimeStart = timeStart;
                    mTimeEnd = timeEnd;
                }

                @Override
                public void onChooseBookingTime(int durationInMinute) {
                    VehicleOfBranchFragment.this.duration = durationInMinute;
                    if(ConfigUtil.checkIfBookingDurationIsValidWithConfigOrShowErrorDialog(getActivity(), durationInMinute*60)){
                        showConfirmDialog();
                    }
                }
            });*/
            recyclerView.setAdapter(adapter);
        }

        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeToRefresh);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                setDateTimePresent();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    private void actionSelectDropOffBranch(final CustomActionListener2 customActionListener) {
        if(ConfigUtil.checkIfEnableDropOff() && branch.enable_migrate_vehicle.equalsIgnoreCase("true")){
            showOrHideProgressBar(true, view);
            String included_ids = String.valueOf(branch.id);
            VolleyUtils.getSharedNetwork().loadDropOffBranches(included_ids, getActivity(), new OnResponseModel<KZBranch[]>() {
                @Override
                public void onResponseSuccess(final KZBranch[] model) {
                    showOrHideProgressBar(false, view);
                    if(model==null) return;
                    String[] data = new String[model.length];
                    String[] subData = new String[model.length];
                    int defaultPos = 0;
                    for (int i = 0; i < model.length; i++) {
                        data[i] = model[i].name;
                        subData[i] = model[i].address;
                        if(model[i].id==branch.id){
                            defaultPos = i;
                        }
                    }
                    if(customActionListener!=null) customActionListener.takeAction(model);
                    /*DialogUtils.showCustomListDialog(getActivity(), "Select drop-off branch", data, subData, null, null, defaultPos,
                            new ListSelectListener2() {
                                @Override
                                public void action(DialogInterface dialog, String[] resultNotNull, int selectionNotNull) {
                                    selected_drop_off_branch_id = model[selectionNotNull].id;
                                    selected_drop_off_branch_name = model[selectionNotNull].name;
                                    selected_drop_off_branch = model[selectionNotNull];
                                    if(customActionListener!=null) customActionListener.takeAction();
                                }
                            }, null, null);*/
                }

                @Override
                public void onResponseError(BKNetworkResponseError error) {
                    showOrHideProgressBar(false, view);
                    VolleyUtils.handleErrorRespond(getActivity(), error);
                }
            });
        }else {
            if(customActionListener!=null) customActionListener.takeAction(null);
        }
    }

    private boolean checkNeedToResetBookingTime() {
        long branchTimezoneOffset = 0;
        long timeZone = 0;
        if (branch != null) {
            branchTimezoneOffset = Long.parseLong(branch.time_zone_offset);
            timeZone = DateUtils.getTimeZoneOffset();
        }

        Calendar currentCalendar = Calendar.getInstance();
        Date currentDate = DateUtils.getDateFromCalendar(currentCalendar, timeZone, branchTimezoneOffset);

        if(null == mSelectedCalendar)
            return true;

        return mSelectedCalendar.getTime().getTime() < currentDate.getTime();
    }

    public void setDateTimePresent() {
        long branchTimezoneOffset = 0;
        long timeZone = 0;
        if (branch != null) {
            branchTimezoneOffset = Long.parseLong(branch.time_zone_offset);
            timeZone = DateUtils.getTimeZoneOffset();
        }
        mSelectedCalendar = Calendar.getInstance();
        mSelectedDate = DateUtils.getDateFromCalendar(mSelectedCalendar, timeZone, branchTimezoneOffset);
        String tvDateValue = new SimpleDateFormat("dd MMM").format(mSelectedDate);
        tvDate.setText(tvDateValue);

        int min = mSelectedDate.getMinutes();
        int minute = min / mBlockBookingTime * mBlockBookingTime;
        mSelectedDate.setMinutes(minute);

        mSelectedCalendar.set(Calendar.MINUTE, minute);

        mNowCalendar = Calendar.getInstance();
        mNowCalendar.setTimeInMillis(mSelectedCalendar.getTimeInMillis());

        String tvTimeValue = new SimpleDateFormat("hh:mm aa", Locale.getDefault()).format(mSelectedDate);
        tvTime.setText(tvTimeValue);

        tvDayFrom.setText(new SimpleDateFormat(/*ConfigUtil.getDateFormat()*/ConstantsUtils.FORMAT_DATE_2, Locale.getDefault()).format(mSelectedDate));
        tvTimeFrom.setText(new SimpleDateFormat(/*ConfigUtil.getTimeFormat()*/ConstantsUtils.TIME_FORMAT_1, Locale.getDefault()).format(mSelectedDate));

        automaticallyAdjustToTimeWithFromTimeFollowConfigDuration();

        actionSearchVehicle2();
    }

    private boolean checkAvailableDateTime(Calendar tempSelectedCal) {
        long branchTimezoneOffset = 0;
        long timeZone = 0;
        if (kzSearchResult != null) {
            branchTimezoneOffset = Long.parseLong(kzSearchResult.start__timezone_offset);
            timeZone = DateUtils.getTimeZoneOffset();
        }else {
            Log.d(TAG , "kzSearchResult is null");
        }
        Calendar currentDate = Calendar.getInstance();
        Date date = DateUtils.getDateFromCalendar(currentDate, timeZone, branchTimezoneOffset);
        int hour = date.getHours();
        int minute = date.getMinutes() / mBlockBookingTime * mBlockBookingTime;
        date.setMinutes(minute);
        currentDate.setTime(date);
        Log.d(TAG , "current : " + currentDate.toString());
        Log.d(TAG, "selected : " + tempSelectedCal.toString());

        boolean timeIsAllowed = TimeHelper.checkTimeToMakeSureSelectedTimeIsOnTheFuture(tempSelectedCal, currentDate);
        return timeIsAllowed;
    }

    private void showTimePickerListStyle() {
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_dialog_time_picker_list_view_style);
        dialog.setCanceledOnTouchOutside(true);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = width * 2 / 3;
        lp.height = height * 2 / 3;
        dialog.getWindow().setAttributes(lp);

        ListView listView = (ListView) dialog.findViewById(R.id.listView);
        ArrayList<TimeUnitModel> data = prepareTimeData();
        int currentPosition = 0;
        for (int i = 0; i < data.size(); i++) {
            if(data.get(i).green) currentPosition = i;
        }
        final CustomRecyclerAdapterTimePickerListStyle listAdapter = new CustomRecyclerAdapterTimePickerListStyle(data);
        listView.setAdapter(listAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int selectedHour = listAdapter.getItem(position).hour24;
                int selectedMinute = listAdapter.getItem(position).min;
                mSelectedCalendar.set(Calendar.HOUR_OF_DAY, selectedHour);
                mSelectedCalendar.set(Calendar.MINUTE, selectedMinute);

                String tvTimeValue = new SimpleDateFormat("hh:mm aa", Locale.getDefault()).format(mSelectedCalendar.getTime()).toString();
                tvTime.setText(tvTimeValue);

                CommonUtils.segmentTrackWithStartTime(getContext(), "Choose Start Time", branch, mSelectedCalendar.getTime());

                if(checkAvailableDateTime(mSelectedCalendar)) {
                    actionSearchVehicle();
                }else{
                    Toast.makeText(getActivity(),"Please choose booking time in the future" , Toast.LENGTH_SHORT).show();
                    return;
                }
                if(dialog!=null) dialog.cancel();
            }
        });
        listView.setSelection(currentPosition);

        dialog.show();
    }

    private void showConfirmDialog(KZVehicle vehicle, KZBranch branch, KZBranch[] branches) {
        mDuration = DateUtils.getDurationFormatText3(duration);
        mTimeStart = new SimpleDateFormat("hh:mm aa dd/MM/yyyy", Locale.getDefault()).format(mSelectedCalendar.getTime());
        String startDate = new SimpleDateFormat(ConstantsUtils.FORMAT_SEGMENT_DATE, Locale.getDefault()).format(mSelectedCalendar.getTime());
        String startTime = new SimpleDateFormat(ConstantsUtils.FORMAT_SEGMENT_TIME, Locale.getDefault()).format(mSelectedCalendar.getTime());

        /*Calendar calendar = Calendar.getInstance();
        calendar.setTime(mSelectedCalendarTo.getTime());
        calendar.setTimeInMillis(mSelectedCalendar.getTimeInMillis() + duration * 60 * 1000L);*/
        mTimeEnd = new SimpleDateFormat("hh:mm aa dd/MM/yyyy", Locale.getDefault()).format(mSelectedCalendarTo.getTime());
        String endDate = new SimpleDateFormat(ConstantsUtils.FORMAT_SEGMENT_DATE, Locale.getDefault()).format(mSelectedCalendar/*calendar*/.getTime());
        String endTime = new SimpleDateFormat(ConstantsUtils.FORMAT_SEGMENT_TIME, Locale.getDefault()).format(mSelectedCalendar/*calendar*/.getTime());

        /*KZVehicle vehicle = vehicles.get(vehicleSelectedIndex);*/

        //Analytics.with(getContext()).track("Book a vehicle",new Properties().putValue("start_date",startDate).putValue("start_time",startTime).putValue("end_date",endDate).putValue("end_time",endTime).putValue("vehicle_id","" + vehicle.id), null);

        isConfirmDialogShowing = true;
        if (vehicleSelectedIndex == -1) {
            UIUtils.toast(getActivity(), "No vehicles is selected.");
            return;
        }
        String bookingCostAsString ;
        if(actionDefineTripType().equals(ConstantsUtils.TRIP_TYPE_PRIVATE)){
            bookingCostAsString = "$" + FormatUtil.formatString(String.valueOf(vehicle.cost_private_total));
        }else {
            bookingCostAsString = null;
        }

        String dropOffBranch = selected_drop_off_branch_name;

        ConfirmReservationDialog dialog = new ConfirmReservationDialog(getActivity(), vehicle, branch, branches, kzAddOns, selectedAddOns);
        dialog.setOnConfirmReservationListener(
                new ConfirmReservationDialog.OnSelectDropOffBranch() {
                    @Override
                    public void onSelect(int id, String timeZoneOffset) {
                        selected_drop_off_branch_id = id;
                        selected_drop_off_time_zone_offset = timeZoneOffset;
                    }
                },
                new ConfirmReservationDialog.OnConfirmReservationListener() {
                    @Override
                    public void onOk() {
                        MyLog.debug(TAG, "confirm clicked");
                        if (vehicles.get(vehicleSelectedIndex) != null) {
                            if(ConfigUtil.checkIfShouldShowRFIDWarning()){
                                DialogUtils.showCustomDialogLinkable(getActivity(), true, "Warning!", getResources().getString(R.string.message_rfid_warning), "Update key coin", "Continue",
                                        new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                Intent intent = new Intent(getActivity(), RegisterCardActivity.class);
                                                startActivity(intent);
                                            }
                                        },
                                        new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                actionClickOkBtnOfConfirmBookingDialog();
                                                adapter.setSelectedIndex(-1);
                                            }
                                        });
                            }else {
                                actionClickOkBtnOfConfirmBookingDialog();
                                adapter.setSelectedIndex(-1);
                            }
                        }
                        isConfirmDialogShowing = false;
                    }
                    @Override
                    public void onCancel() {
                        if(checkNeedToResetBookingTime()) {
                            setDateTimePresent();
                        }
                        isConfirmDialogShowing = false;
                    }
                },
                new ConfirmReservationDialog.OnConciergeInfoUpdate() {
                    @Override
                    public void actionUpdate(boolean checked, String conciergeDeliveryLocation, String conciergeDeliveryNote, boolean checked1, String conciergeCollectionLocation) {
                        concierge_delivery_enable = checked;
                        concierge_delivery_place = conciergeCollectionLocation;
                        concierge_delivery_note = conciergeDeliveryNote;
                        concierge_collection_enable = checked1;
                        concierge_collection_place = conciergeCollectionLocation;
                    }
                },
                new ConfirmReservationDialog.OnAddOnUpdate(){
                    @Override
                    public void actionUpdate(ArrayList<String> adOnIds) {
                        selectedAddOns = new ArrayList<>(adOnIds);
                        addOnsArrObj = new JsonArray();
                        for (String addOnId: adOnIds) {
                            addOnsArrObj.add(addOnId);
                        }
                    }
                });

        dialog.setCancelable(false);
        dialog.show();

        dialog.setUpData(vehicle, mDuration, mTimeStart, mTimeEnd, mSelectedCalendar.getTime(), mSelectedCalendarTo/*calendar*/.getTime(),
                bookingCostAsString,
                dropOffBranch);
    }

    private void actionSearchVehicle() {
        Log.d("actionSearchVehicle", new SimpleDateFormat("dd MMM yyyy, hh:mm aa", Locale.getDefault()).format(mSelectedCalendar.getTime()));
        Date start = mSelectedCalendar.getTime();
        VolleyUtils.getSharedNetwork().searchVehicles(getContext(), start, String.valueOf(branch.id), new OnResponseModel<KZSearchResult>() {
            @Override
            public void onResponseSuccess(KZSearchResult model) {
                if (model != null) {
                    kzSearchResult = model;
                    vehicles = model.vehicles;
                    bookings = model.bookings;
                    if(vehicles != null && vehicles.size() > 0) {
                        mRelEmptyVehicle.setVisibility(View.GONE);
                    } else {
                        mRelEmptyVehicle.setVisibility(View.VISIBLE);
                    }
                }
                notifyVehicleListChanged(kzSearchResult, vehicles, bookings);
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                showOrHideProgressBar(false, view);

                final BKNetworkResponseError networkResponse = error;
                if(networkResponse != null) {
                    if (networkResponse.description != null && !networkResponse.description.equals("")) {
                        ((BaseActivity)getActivity()).showAlertDialog("", networkResponse.description, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (networkResponse.code.equals("in_the_past")) {
                                    setDateTimePresent();
                                }
                            }
                        });
                    } else {
                        ((BaseActivity)getActivity()).showAlertDialog("", getActivity().getString(R.string.error_from_server));
                    }
                }else {
                    ((BaseActivity)getActivity()).showAlertDialog("", getActivity().getString(R.string.error_from_server));
                }

            }
        });
    }

    private void actionSearchVehicle2() {
        //showOrHideProgressBar(true);
        Date start = mSelectedCalendar.getTime();
        Date end = mSelectedCalendarTo.getTime();
        VolleyUtils.getSharedNetwork().searchVehicles2(getActivity(), branch, start, end, String.valueOf(branch.id), new OnResponseModel<KZSearchResult>() {
            @Override
            public void onResponseSuccess(KZSearchResult model) {
                if (model != null) {
                    kzSearchResult = model;
                    vehicles = model.vehicles;
                    bookings = model.bookings;
                    if(vehicles != null && vehicles.size() > 0) {
                        mRelEmptyVehicle.setVisibility(View.GONE);
                    } else {
                        mRelEmptyVehicle.setVisibility(View.VISIBLE);
                    }
                }
                notifyVehicleListChanged(kzSearchResult, vehicles /*filterVehicleWithTripType(vehicles, trip_type)*/, bookings);
                //showOrHideProgressBar(false);
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                //showOrHideProgressBar(false);

                final BKNetworkResponseError networkResponse = error;
                if(networkResponse != null) {
                    if (networkResponse.description != null && !networkResponse.description.equals("")) {
                        ((BaseActivity)getActivity()).showAlertDialog("", networkResponse.description, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (networkResponse.code!=null && networkResponse.code.equals("in_the_past")) {
                                    setDateTimePresent();
                                }
                            }
                        });
                    } else {
                        ((BaseActivity)getActivity()).showAlertDialog("", "An error from server, please contact administrator");
                    }
                }else {
                    ((BaseActivity)getActivity()).showAlertDialog("", "An error from server, please contact administrator");
                }

            }
        });
    }

    private void actionClickOkBtnOfConfirmBookingDialog() {
        if(!KZAccount.getSharedAccount().user.is_acuant){
            DialogUtils.showMessageDialog(getActivity(), false, "Warning!", "You must validate your driver license to continue booking", "Ok", "Later",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Intent intent = new Intent(getActivity(), DriverLicenseActivityEnvoy.class);
                            intent.putExtra(ConstantsUtils.INTENT_EXTRA_OPEN_FROM_CONFIRM_BOOKING, true);
                            intent.putExtra(ConstantsUtils.INTENT_EXTRA_FORCE_USER_RE_UPLOAD_DL_WITH_ACUANT, true);
                            startActivity(intent);
                        }
                    },
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.cancel();
                        }
                    });
            return;
        }
        final KZVehicle vehicle = vehicles.get(vehicleSelectedIndex);
        showOrHideProgressBar(true, view);

        PAYMENT_REQUIRED_ENUM paymentRequiredEnum = ConfigUtil.checkIfPaymentIsRequired(trip_type);
        switch (paymentRequiredEnum){
            case REQUIRED_DRIVER:
                VolleyUtils.getSharedNetwork().loadPaymentMethodDriver(
                        getContext(),
                        new OnResponseModel<KZPaymentDetail>() {
                    @Override
                    public void onResponseSuccess(KZPaymentDetail model) {
                        showOrHideProgressBar(false, view);

                        if(model != null) {
                            int paymentMethods = model.getPaymentList().size();
                            switch (paymentMethods){
                                case 0:
                                    actionWhenThereIsNoPaymentIsFound(ConstantsUtils.PAYMENT_SITUATION_DRIVER);
                                    break;
                                case 1:
                                    final KZPaymentMethod kzPaymentMethod = model.getPaymentList().get(0);
                                    if (vehicles != null) {
                                        actionCheckIfNeedShowBookingPolicy(new CustomListener4() {
                                            @Override
                                            public void returnFalse() {
                                                actionCallApiToCreateBooking(vehicle, kzPaymentMethod.token, "actionClickOkBtnOfConfirmBookingDialog");
                                            }

                                            @Override
                                            public void returnTrue(String s) {
                                                DialogUtils.showMessageDialog(getActivity(), true,
                                                        getActivity().getString(R.string.title_usage_policy),
                                                        s,
                                                        getActivity().getString(R.string.btn_yes),
                                                        getActivity().getString(R.string.btn_no),
                                                        new DialogInterface.OnClickListener() {
                                                            @Override
                                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                                dialogInterface.cancel();
                                                                actionCallApiToCreateBooking(vehicles.get(vehicleSelectedIndex), kzPaymentMethod.token, "actionClickOkBtnOfConfirmBookingDialog");
                                                            }
                                                        },
                                                        new DialogInterface.OnClickListener() {
                                                            @Override
                                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                                dialogInterface.cancel();
                                                            }
                                                        });
                                            }
                                        });
                                    }
                                    break;
                                default:
                                    actionWhenThereIsMoreThanOnePayment(ConstantsUtils.PAYMENT_SITUATION_DRIVER);
                                    break;
                            }
                        }

                    }

                    @Override
                    public void onResponseError(final BKNetworkResponseError error) {
                        showOrHideProgressBar(false, view);
                        VolleyUtils.handleErrorRespond(getActivity(), error);
                        /*VolleyUtils.handleErrorRespond2(getActivity(), error, "Open payment", "Cancel", "",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        actionWhenThereIsNoPaymentIsFound(ConstantsUtils.PAYMENT_SITUATION_DRIVER);
                                    }
                                },
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.cancel();
                                    }
                                }, null);*/
                    }
                });
                break;
            case REQUIRED_COST_CENTER:
                VolleyUtils.getSharedNetwork().loadPaymentMethodCostCenter(
                        getContext(),
                        new OnResponseModel<KZPaymentDetail>() {
                    @Override
                    public void onResponseSuccess(KZPaymentDetail model) {
                        showOrHideProgressBar(false, view);

                        if(model != null) {
                            int paymentMethods = model.getPaymentList().size();
                            switch (paymentMethods){
                                case 0:
                                    actionWhenThereIsNoPaymentIsFound(ConstantsUtils.PAYMENT_SITUATION_COST_CENTER);
                                    break;
                                case 1:
                                    final KZPaymentMethod kzPaymentMethod = model.getPaymentList().get(0);
                                    if (vehicles != null) {
                                        actionCheckIfNeedShowBookingPolicy(new CustomListener4() {
                                            @Override
                                            public void returnFalse() {
                                                actionCallApiToCreateBooking(vehicles.get(vehicleSelectedIndex), kzPaymentMethod.token, "actionClickOkBtnOfConfirmBookingDialog");
                                            }

                                            @Override
                                            public void returnTrue(String s) {
                                                DialogUtils.showMessageDialog(getActivity(), true,
                                                        getActivity().getString(R.string.title_usage_policy),
                                                        s,
                                                        getActivity().getString(R.string.btn_yes),
                                                        getActivity().getString(R.string.btn_no),
                                                        new DialogInterface.OnClickListener() {
                                                            @Override
                                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                                dialogInterface.cancel();
                                                                actionCallApiToCreateBooking(vehicles.get(vehicleSelectedIndex), kzPaymentMethod.token, "actionClickOkBtnOfConfirmBookingDialog");
                                                            }
                                                        },
                                                        new DialogInterface.OnClickListener() {
                                                            @Override
                                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                                dialogInterface.cancel();
                                                            }
                                                        });
                                            }
                                        });

                                    }
                                    break;
                                default:
                                    actionWhenThereIsMoreThanOnePayment(ConstantsUtils.PAYMENT_SITUATION_COST_CENTER);
                                    break;
                            }
                        }
                    }

                    @Override
                    public void onResponseError(BKNetworkResponseError error) {
                    }
                });
                break;
            case REQUIRED_COMPANY:
                VolleyUtils.getSharedNetwork().loadPaymentMethodCompany(
                        getContext(),
                        new OnResponseModel<KZPaymentDetail>() {
                    @Override
                    public void onResponseSuccess(KZPaymentDetail model) {
                        showOrHideProgressBar(false, view);

                        if(model != null) {
                            int paymentMethods = model.getPaymentList().size();
                            switch (paymentMethods){
                                case 0:
                                    actionWhenThereIsNoPaymentIsFound(ConstantsUtils.PAYMENT_SITUATION_COMPANY);
                                    break;
                                case 1:
                                    final KZPaymentMethod kzPaymentMethod = model.getPaymentList().get(0);
                                    if (vehicles != null) {
                                        actionCheckIfNeedShowBookingPolicy(new CustomListener4() {
                                            @Override
                                            public void returnFalse() {
                                                actionCallApiToCreateBooking(vehicles.get(vehicleSelectedIndex), kzPaymentMethod.token, "actionClickOkBtnOfConfirmBookingDialog");
                                            }

                                            @Override
                                            public void returnTrue(String s) {
                                                DialogUtils.showMessageDialog(getActivity(), true,
                                                        getActivity().getString(R.string.title_usage_policy),
                                                        s,
                                                        getActivity().getString(R.string.btn_yes),
                                                        getActivity().getString(R.string.btn_no),
                                                        new DialogInterface.OnClickListener() {
                                                            @Override
                                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                                dialogInterface.cancel();
                                                                actionCallApiToCreateBooking(vehicles.get(vehicleSelectedIndex), kzPaymentMethod.token, "actionClickOkBtnOfConfirmBookingDialog");
                                                            }
                                                        },
                                                        new DialogInterface.OnClickListener() {
                                                            @Override
                                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                                dialogInterface.cancel();
                                                            }
                                                        });
                                            }
                                        });

                                    }
                                    break;
                                default:
                                    actionWhenThereIsMoreThanOnePayment(ConstantsUtils.PAYMENT_SITUATION_COMPANY);
                                    break;
                            }
                        }
                    }

                    @Override
                    public void onResponseError(BKNetworkResponseError error) {
                    }
                });
                break;
            case NO_REQUIRED:
                actionCheckIfNeedShowBookingPolicy(new CustomListener4() {
                    @Override
                    public void returnFalse() {
                        actionCallApiToCreateBooking(vehicles.get(vehicleSelectedIndex), null, "actionClickOkBtnOfConfirmBookingDialog");
                    }

                    @Override
                    public void returnTrue(String s) {
                        DialogUtils.showMessageDialog(getActivity(),
                                true,
                                getActivity().getString(R.string.title_usage_policy),
                                s,
                                getActivity().getString(R.string.btn_yes),
                                getActivity().getString(R.string.btn_no),
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.cancel();
                                        actionCallApiToCreateBooking(vehicles.get(vehicleSelectedIndex), null, "actionClickOkBtnOfConfirmBookingDialog");
                                    }
                                },
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.cancel();
                                    }
                                });
                    }
                });
                break;
        }
    }

    private void actionWhenThereIsMoreThanOnePayment(int c) {
        Intent intent = new Intent(getActivity(), AddPaymentActivity.class);
        intent.putExtra(ConstantsUtils.INTENT_EXTRA_SELECT_PAYMENT_DRIVER_OR_COMPANY_OR_COSTCENTER, c);
        intent.putExtra(ConstantsUtils.INTENT_EXTRA_OPEN_PAYMENT_ACTIVITY_IN_MAKE_BOOKING_WORKFLOW, true);
        startActivityForResult(intent, ConstantsUtils.REQUEST_CODE_SELECT_OR_ADD_PAYMENT_METHOD);
    }

    private void actionCheckIfNeedShowBookingPolicy(final CustomListener4 customListener4) {
        showOrHideProgressBar(false, view);
        if(!ConfigUtil.checkIfEnableUsagePolicy()) {
            customListener4.returnFalse();

        }else {
            switch (trip_type){
                case ConstantsUtils.TRIP_TYPE_BUSINESS:
                    if(ConfigUtil.checkIfEnableCheckBusinessUsagePolicy()){
                        ConfigUtil.checkIfBusinessPolicyIsEmpty(getContext(), new CustomListener4() {
                            @Override
                            public void returnFalse() {
                                customListener4.returnFalse();
                            }

                            @Override
                            public void returnTrue(String s) {
                                customListener4.returnTrue(s);
                            }
                        }, KZAccount.getSharedAccount().getCompany().id);
                        return;
                    }else {
                        customListener4.returnFalse();
                        return;
                    }

                case ConstantsUtils.TRIP_TYPE_PRIVATE:
                    if(ConfigUtil.checkIfEnableCheckPrivateUsagePolicy()){
                        ConfigUtil.checkIfPrivatePolicyIsEmpty(getContext(), new CustomListener4() {
                            @Override
                            public void returnFalse() {
                                customListener4.returnFalse();
                            }

                            @Override
                            public void returnTrue(String s) {
                                customListener4.returnTrue(s);
                            }
                        }, KZAccount.getSharedAccount().getCompany().id);
                        return;
                    }else {
                        customListener4.returnFalse();
                        return;
                    }
            }


            customListener4.returnFalse();
        }
    }

    private void actionWhenThereIsNoPaymentIsFound(int c) {
        switch (c){
            case ConstantsUtils.PAYMENT_SITUATION_DRIVER:
                Intent intent = new Intent(getActivity(), AddPaymentActivity.class);
                intent.putExtra(ConstantsUtils.INTENT_EXTRA_SELECT_PAYMENT_DRIVER_OR_COMPANY_OR_COSTCENTER, c);
                intent.putExtra(ConstantsUtils.INTENT_EXTRA_OPEN_PAYMENT_ACTIVITY_IN_MAKE_BOOKING_WORKFLOW, true);
                startActivityForResult(intent, ConstantsUtils.REQUEST_CODE_SELECT_OR_ADD_PAYMENT_METHOD);
                break;
            case ConstantsUtils.PAYMENT_SITUATION_COST_CENTER:
                DialogUtils.showMessageDialog(getActivity(), false, "No cost center payment", "No cost center payment has found.", "OK", "",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.cancel();
                            }
                        },null);
                break;
            case ConstantsUtils.PAYMENT_SITUATION_COMPANY:
                DialogUtils.showMessageDialog(getActivity(), false, "No company payment", "No company payment has found.", "OK", "",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.cancel();
                            }
                        },null);
                break;
        }
    }

    private void actionWhenGetCreateBookingErrorRespondCase1_InThePass(String kzPaymentMethodId){

        long branchTimezoneOffset = DateUtils.getTimeZoneOffset();
        if (kzSearchResult != null) {
            branchTimezoneOffset = Long.parseLong(kzSearchResult.start__timezone_offset);
        }
        mSelectedCalendar = Calendar.getInstance();
        mSelectedDate = DateUtils.getTodayWithBranchTimezone(branchTimezoneOffset);
        int minute = mSelectedDate.getMinutes() / mBlockBookingTime * mBlockBookingTime;
        mSelectedDate.setMinutes(minute);
        mSelectedCalendar.set(Calendar.MINUTE, minute);

        if (vehicles.get(vehicleSelectedIndex) != null) {
            actionCallApiToCreateBooking(vehicles.get(vehicleSelectedIndex), kzPaymentMethodId, "actionWhenGetCreateBookingErrorRespondCase1_InThePass");
            adapter.setSelectedIndex(-1);
        }
    }

    private void actionOpenBeginReservationBookingActivity(KZBooking model) {
        Intent intent = new Intent(getActivity(), ApprovedBookingActivity.class);
        ApprovedBookingActivity.booking = model;
        getActivity().startActivity(intent);
    }

    private void actionWhenGetCreateBookingError_BookingIsTooShort(String kzPaymentMethodId) {
        VehicleOfBranchFragment.this.duration = duration + mBlockBookingTime;

        if (vehicles.get(vehicleSelectedIndex) != null) {
            actionCallApiToCreateBooking(vehicles.get(vehicleSelectedIndex), kzPaymentMethodId, "actionCallApiToCreateBooking");
            adapter.setSelectedIndex(-1);
        }
    }

    private class CustomRecyclerAdapterTimePickerListStyle extends BaseAdapter {
        private ArrayList<TimeUnitModel> data;

        private CustomRecyclerAdapterTimePickerListStyle(ArrayList<TimeUnitModel> data) {
            this.data = data;
        }

        @Override
        public int getCount() {
            return data.size();
        }

        @Override
        public TimeUnitModel getItem(int position) {
            return data.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if(convertView==null){
                convertView = inflater.inflate(R.layout.custom_list_item_time_picker_list_style, parent, false);
            }
            TextView tv = (TextView)convertView.findViewById(R.id.tv);
            tv.setText(data.get(position).timeLabel);

            boolean state2 = data.get(position).green;
            if(state2){
                tv.setTextColor(getResources().getColor(R.color.tvColorGreen));
            }else {
                tv.setTextColor(getResources().getColor(R.color.tvColorDark));
            }

            return convertView;
        }
    }

    private static class TimeUnitModel {
        final int hour;
        final int min;
        final String label;
        final int hour24;
        String timeLabel;
        boolean green;

        TimeUnitModel(String timeLabel, int h, int m, String b, int h24, boolean green) {
            this.timeLabel = timeLabel;
            this.hour = h;
            this.min = m;
            this.label = b;
            this.hour24 = h24;
            this.green = green;
        }
    }

    private ArrayList<TimeUnitModel> prepareTimeData() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(mSelectedCalendar.getTimeInMillis());
        calendar.set(Calendar.MINUTE, mSelectedCalendar.get(Calendar.MINUTE) + mBlockBookingTime);

        int hourOfDay = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);

        ArrayList<TimeUnitModel> rs = new ArrayList<>();
        for (int i = 0; i < 24; i++) {
            String timeLabel;
            int h = 0;
            String b = "";
            if(i < 12) {
                if (i == 0) {
                    h = 12;
                    b = " AM";
                } else {
                    h = i;
                    b = " AM";
                }
            } else if(i > 12) {
                h = i - 12;
                b = " PM";
            } else {
                h = i;
                b = " PM";
            }

            int timeInHour = 60 / mBlockBookingTime; // 60 / 15 = 4 -- run 4 times
            for (int j = 0; j < timeInHour; j++) {
                int m = j*mBlockBookingTime;
                timeLabel = String.format("%02d",h) + ":" + String.format("%02d", m) + b;

                int amount1 = hourOfDay*60 + minute;
                int amount2 = i*60 + m;
                int delta = (amount2<amount1 ? amount1-amount2 : 0);

                boolean state = amount2<amount1 && delta<=mBlockBookingTime;
                boolean green;
                if(state){
                    green = true;
                }else {
                    green = false;
                }

                rs.add(new TimeUnitModel(timeLabel, h, m, b, i, green));
            }
        }
        return rs;
    }

    public static void showOrHideProgressBar(boolean b, View view) {
        allowBackPress = !LoadingViewUtils.showOrHideProgressBar(b, view);
    }

    private void actionClickFromView() {
        int year = mSelectedCalendar.get(Calendar.YEAR);
        int month = mSelectedCalendar.get(Calendar.MONTH);
        int day = mSelectedCalendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog;
        datePickerDialog = new DatePickerDialog(
                getActivity(),
                R.style.DateTimePickerDialogTheme, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker datePicker, int selectedYear, int selectedMonth, int selectedDay) {
                Calendar tempSelectedCal = Calendar.getInstance();
                tempSelectedCal.setTime(mSelectedCalendar.getTime());
                tempSelectedCal.set(Calendar.YEAR, selectedYear);
                tempSelectedCal.set(Calendar.MONTH, selectedMonth);
                tempSelectedCal.set(Calendar.DAY_OF_MONTH, selectedDay);

                if(true/*checkAvailableDateTime(tempSelectedCal)*/) {
                    mSelectedCalendar.set(Calendar.YEAR, selectedYear);
                    mSelectedCalendar.set(Calendar.MONTH, selectedMonth);
                    mSelectedCalendar.set(Calendar.DAY_OF_MONTH, selectedDay);
                    tvDayFrom.setText(new SimpleDateFormat(ConstantsUtils.FORMAT_DATE_2, Locale.getDefault()).format(mSelectedCalendar.getTime()));

                    int hour = mSelectedCalendar.get(Calendar.HOUR_OF_DAY);
                    int minute = mSelectedCalendar.get(Calendar.MINUTE);
                    TimePickerDialog timePickerDialog;
                    timePickerDialog = TimePickerDialog.newInstance(
                            new TimePickerDialog.OnTimeSetListener() {
                                @Override
                                public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
                                    Calendar tempSelectedCal = Calendar.getInstance();
                                    tempSelectedCal.set(Calendar.YEAR, mSelectedCalendar.get(Calendar.YEAR));
                                    tempSelectedCal.set(Calendar.MONTH, mSelectedCalendar.get(Calendar.MONTH));
                                    tempSelectedCal.set(Calendar.DAY_OF_MONTH, mSelectedCalendar.get(Calendar.DAY_OF_MONTH));
                                    tempSelectedCal.set(Calendar.HOUR_OF_DAY, hourOfDay);
                                    tempSelectedCal.set(Calendar.MINUTE, minute);

                                    if(checkAvailableDateTime(tempSelectedCal)) {
                                        mSelectedCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                                        mSelectedCalendar.set(Calendar.MINUTE, minute);
                                        tvTimeFrom.setText(new SimpleDateFormat(/*ConfigUtil.getTimeFormat()*/ConstantsUtils.TIME_FORMAT_1, Locale.getDefault()).format(mSelectedCalendar.getTime()));

                                        //Auto update 'to' time with config min booking duration
                                        automaticallyAdjustToTimeWithFromTimeFollowConfigDuration();

                                        actionClickToView();
                                    }else{
                                        Toast.makeText(getActivity(),"Please choose booking time in the future" , Toast.LENGTH_SHORT).show();
                                        setDateTimePresent();
                                    }
                                }
                            },
                            hour, minute,true
                    );
                    timePickerDialog.setTitle("Select start time");
                    timePickerDialog.setTimeInterval(1, 1);
                    timePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialogInterface) {
                            setDateTimePresent();
                        }
                    });
                    timePickerDialog.show(getActivity().getFragmentManager(), "start time");

                }else{
                    Toast.makeText(getActivity(),"Please choose booking time in the future" , Toast.LENGTH_SHORT).show();
                    setDateTimePresent();
                }

            }
        }, year, month, day);
        /*datePickerDialog.getDatePicker().setMinDate(DateUtils.getCurrentTimeWithBranchTimeZone(branch).getTimeInMillis());*/
        datePickerDialog.setTitle("Select start time");
        datePickerDialog.show();
    }

    private void actionClickToView() {
        int year = mSelectedCalendarTo.get(Calendar.YEAR);
        int month = mSelectedCalendarTo.get(Calendar.MONTH);
        int day = mSelectedCalendarTo.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog;
        datePickerDialog = new DatePickerDialog(
                getActivity(),
                R.style.DateTimePickerDialogTheme, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker datePicker, int selectedYear, int selectedMonth, int selectedDay) {
                Calendar tempSelectedCal = Calendar.getInstance();
                tempSelectedCal.setTime(mSelectedCalendarTo.getTime());
                tempSelectedCal.set(Calendar.YEAR, selectedYear);
                tempSelectedCal.set(Calendar.MONTH, selectedMonth);
                tempSelectedCal.set(Calendar.DAY_OF_MONTH, selectedDay);

                if(checkAvailableDateTime(tempSelectedCal)) {
                    mSelectedCalendarTo.set(Calendar.YEAR, selectedYear);
                    mSelectedCalendarTo.set(Calendar.MONTH, selectedMonth);
                    mSelectedCalendarTo.set(Calendar.DAY_OF_MONTH, selectedDay);
                    tvDayTo.setText(new SimpleDateFormat(ConstantsUtils.FORMAT_DATE_2, Locale.getDefault()).format(mSelectedCalendarTo.getTime()));

                    int hour = mSelectedCalendarTo.get(Calendar.HOUR_OF_DAY);
                    int minute = mSelectedCalendarTo.get(Calendar.MINUTE);
                    TimePickerDialog timePickerDialog;
                    timePickerDialog = TimePickerDialog.newInstance(
                            new TimePickerDialog.OnTimeSetListener() {
                                @Override
                                public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
                                    Calendar tempSelectedCal = Calendar.getInstance();
                                    tempSelectedCal.set(Calendar.YEAR, mSelectedCalendarTo.get(Calendar.YEAR));
                                    tempSelectedCal.set(Calendar.MONTH, mSelectedCalendarTo.get(Calendar.MONTH));
                                    tempSelectedCal.set(Calendar.DAY_OF_MONTH, mSelectedCalendarTo.get(Calendar.DAY_OF_MONTH));
                                    tempSelectedCal.set(Calendar.HOUR_OF_DAY, hourOfDay);
                                    tempSelectedCal.set(Calendar.MINUTE, minute);

                                    mSelectedCalendarTo.set(Calendar.HOUR_OF_DAY, hourOfDay);
                                    mSelectedCalendarTo.set(Calendar.MINUTE, minute);
                                    tvTimeTo.setText(new SimpleDateFormat(/*ConfigUtil.getTimeFormat()*/ConstantsUtils.TIME_FORMAT_1, Locale.getDefault()).format(mSelectedCalendarTo.getTime()));

                                    if(checkAvailableDateTime(tempSelectedCal)) {
                                        mSelectedCalendarTo.set(Calendar.HOUR_OF_DAY, hourOfDay);
                                        mSelectedCalendarTo.set(Calendar.MINUTE, minute);
                                        mSelectedDateTo = mSelectedCalendarTo.getTime();
                                        tvTimeTo.setText(new SimpleDateFormat(/*ConfigUtil.getTimeFormat()*/ConstantsUtils.TIME_FORMAT_1, Locale.getDefault()).format(mSelectedCalendarTo.getTime()));

                                        if(ConfigUtil.checkIfBookingDurationIsValidWithConfigOrShowErrorDialog(getActivity(), mSelectedCalendar, mSelectedCalendarTo)) {
                                            actionSearchVehicle2();
                                        }else {
                                            setDateTimePresent();
                                        }
                                    }else{
                                        Toast.makeText(getActivity(),"Please choose booking time in the future" , Toast.LENGTH_SHORT).show();
                                        setDateTimePresent();
                                    }
                                }
                            },hour, minute, true
                    );
                    timePickerDialog.setTimeInterval(1, 1);
                    timePickerDialog.setTitle("Select end time");
                    timePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialogInterface) {
                            setDateTimePresent();
                        }
                    });
                    timePickerDialog.show(getActivity().getFragmentManager(), "end time");

                }else {
                    Toast.makeText(getActivity(),"Please choose booking time in the future" , Toast.LENGTH_SHORT).show();
                    setDateTimePresent();
                }


            }
        }, year, month, day);
        /*datePickerDialog.getDatePicker().setMinDate(DateUtils.getCurrentTimeWithBranchTimeZone(branch).getTimeInMillis());*/
        datePickerDialog.setTitle("Select end time");
        datePickerDialog.show();
    }

    private void automaticallyAdjustToTimeWithFromTimeFollowConfigDuration() {
        mSelectedCalendarTo = MakeBookingUtil.adjustToTimeWithFromTimeFollowConfigDuration(ConfigUtil.getConfigBookingMinDurationTimeInSecond()/60, mSelectedCalendar);
        mSelectedDateTo = mSelectedCalendarTo.getTime();
        tvDayTo.setText(new SimpleDateFormat(/*ConfigUtil.getDateFormat()*/ConstantsUtils.FORMAT_DATE_2, Locale.getDefault() ).format(mSelectedDateTo));
        tvTimeTo.setText(new SimpleDateFormat(/*ConfigUtil.getTimeFormat()*/ConstantsUtils.TIME_FORMAT_1, Locale.getDefault()).format(mSelectedDateTo));
    }

    private void loadAddOn() {
        VolleyUtils.getSharedNetwork().loadAddOns(getActivity(), new OnResponseModel<JsonArray>() {
            @Override
            public void onResponseSuccess(JsonArray array) {
                kzAddOns = new ArrayList<>();
                for (int i = 0; i < array.size(); i++) {
                    String s = array.get(i).toString();
                    KZAddOn addOn = JsonUtils.parseByModel(s, KZAddOn.class);
                    kzAddOns.add(addOn);
                }
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {

            }
        });
    }
}
