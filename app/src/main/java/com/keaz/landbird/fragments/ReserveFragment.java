package com.keaz.landbird.fragments;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.keaz.landbird.R;
import com.keaz.landbird.activities.AddLocationActivity;
import com.keaz.landbird.activities.BaseActivity;
import com.keaz.landbird.adapters.LocationAdapter;
import com.keaz.landbird.models.KZAccount;
import com.keaz.landbird.models.KZBranch;
import com.keaz.landbird.models.KZLandingData;
import com.keaz.landbird.utils.BKNetworkResponseError;
import com.keaz.landbird.utils.LoadingViewUtils;
import com.keaz.landbird.utils.OnResponseModel;
import com.keaz.landbird.utils.VolleyUtils;
import com.segment.analytics.Analytics;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static android.app.Activity.RESULT_OK;
/**
 * A simple {@link Fragment} subclass.
 */
public class ReserveFragment extends Fragment implements View.OnClickListener {
    private static final int CODE_RELOAD_LOCATION = 997;
    private LocationAdapter adapter;
    private List<KZBranch> branchList;
    private RelativeLayout mRelEmptyVehicle;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private View view;

    public ReserveFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_reserve, container, false);

        iniViewAndListener();
        iniList();

        loadBranches();

        return view;
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CODE_RELOAD_LOCATION && resultCode == RESULT_OK) {
            loadBranches();
        }
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnAddLocation:
            case R.id.lnAddLocation:
                Analytics.with(getContext()).track("Click Add_a_location");

                Intent intent = new Intent(getActivity(), AddLocationActivity.class);
                startActivityForResult(intent, CODE_RELOAD_LOCATION);
                break;
        }
    }

    private void loadBranches() {
        Analytics.with(getContext()).track("Load branches");
        if(KZAccount.getSharedAccount().user==null){
            Analytics.with(getContext()).track("Null, 'user' has NULL value , reload landing data to get user info");
            VolleyUtils.getSharedNetwork().loadLandingData2(getActivity(), new OnResponseModel<KZLandingData>() {
                @Override
                public void onResponseSuccess(KZLandingData model) {
                    ((BaseActivity)getActivity()).saveUser(model.user);
                    loadBranches();
                }

                @Override
                public void onResponseError(BKNetworkResponseError error) {
                    VolleyUtils.handleErrorRespond(getActivity(),error);
                }
            });
            return;
        }
        String userId = String.valueOf(KZAccount.getSharedAccount().user.id);

        LoadingViewUtils.showOrHideProgressBar(true, view);
        VolleyUtils.getSharedNetwork().loadBranchesByUser(getContext(), userId, new OnResponseModel<KZBranch[]>() {
            @Override
            public void onResponseSuccess(KZBranch[] model) {
                Analytics.with(getContext()).track("Load branches successfully");
                LoadingViewUtils.showOrHideProgressBar(false, view);

                KZAccount.getSharedAccount().setBranches(model);
                branchList = Arrays.asList(model);
                adapter.notifyDataChanged(branchList);


                if (branchList == null || branchList.size() == 0) {
                    mRelEmptyVehicle.setVisibility(View.VISIBLE);
                } else {
                    mRelEmptyVehicle.setVisibility(View.GONE);
                }
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                LoadingViewUtils.showOrHideProgressBar(false, view);
                Analytics.with(getContext()).track("Load branches error");
                VolleyUtils.handleErrorRespond(getActivity(), error);
            }
        });
    }

    private void iniViewAndListener() {
        mRelEmptyVehicle = view.findViewById(R.id.rel_empty_vehicle);
        mRelEmptyVehicle.setVisibility(View.GONE);

        view.findViewById(R.id.btnAddLocation).setOnClickListener(this);
        view.findViewById(R.id.lnAddLocation).setOnClickListener(this);
        mSwipeRefreshLayout = view.findViewById(R.id.swipeToRefresh);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadBranches();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });

    }

    private void iniList() {
        RecyclerView recyclerView = view.findViewById(R.id.listLocation);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new LocationAdapter(getActivity(), new ArrayList<KZBranch>());
        recyclerView.setAdapter(adapter);
    }

}
