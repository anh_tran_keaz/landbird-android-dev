package com.keaz.landbird.enums;

/*
 * Created by anhtran1810 on 3/29/18.
 */

public enum PAYMENT_REQUIRED_ENUM {
    REQUIRED_DRIVER, REQUIRED_COST_CENTER, REQUIRED_COMPANY, NO_REQUIRED
}
