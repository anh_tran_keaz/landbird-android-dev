package com.keaz.landbird.adapters;

import java.util.TimeZone;

/**
 * Created by mac on 3/14/17.
 */
public class TimeZoneUtils {
    public static String[] getAvailable(long offsetByMillis) {
        //offset = 8 * 60 * 60 * 1000;
        String[] timezones = TimeZone.getAvailableIDs((int) offsetByMillis);

        return timezones;
    }

    static String getTimezoneName(int offset){
        switch (offset){
            case -7:
                return "Pacific Daylight Time (PDT)";
            case -8:
                return "Pacific Standard Time (PST)";
            case +7:
                return "Indochina Time (PST)";
        }
        return "";
    }
}
