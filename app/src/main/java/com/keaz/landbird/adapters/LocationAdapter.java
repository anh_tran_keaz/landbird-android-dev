package com.keaz.landbird.adapters;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.keaz.landbird.R;
import com.keaz.landbird.activities.VehicleOfABranchActivity;
import com.keaz.landbird.models.KZAccount;
import com.keaz.landbird.models.KZBranch;
import com.keaz.landbird.utils.CommonUtils;
import com.keaz.landbird.utils.UIUtils;
import com.makeramen.roundedimageview.RoundedImageView;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 14/2/2017.
 */
public class LocationAdapter extends RecyclerView.Adapter<LocationAdapter.ViewHolder> {

    private final Activity activity;
    private List<KZBranch> listBranch;
    private RoundedImageView ivLocation;


    public LocationAdapter(Activity activity, ArrayList<KZBranch> list) {
        this.listBranch = list;
        this.activity = activity;
    }

    public void notifyDataChanged(List<KZBranch> listBranch) {
        this.listBranch = listBranch;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_location_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        View convertView = holder.view;
        UIUtils.setTextViewText(convertView, R.id.txt_name, listBranch.get(position).name);

        String vehicles = ((Double) listBranch.get(position).totals.get("vehicles")).intValue() + " Vehicles";
        UIUtils.setTextViewText(convertView, R.id.txt_count, vehicles);

        UIUtils.setTextViewText(convertView, R.id.txt_address, listBranch.get(position).address);

        String time = listBranch.get(position).business_hours_start + " - " + listBranch.get(position).business_hours_end;
        UIUtils.setTextViewText(convertView, R.id.txt_time, time);

        ivLocation = convertView.findViewById(R.id.ivLocation);
        ivLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                KZBranch branch = listBranch.get(holder.getAdapterPosition());

                KZAccount.getSharedAccount().branchSelected = branch;

                CommonUtils.segmentTrackWithBranch(activity, "Click branch item", branch);

                Intent intent = new Intent(activity, VehicleOfABranchActivity.class);
                intent.putExtra("branch", branch);
                activity.startActivity(intent);
            }
        });

        // Image view
        String url = listBranch.get(position).assets.photo;
        /*if (url == null || url.equals("")) {
            notifyDataSetChanged();
        }*/
        ImageLoader imageLoader = ImageLoader.getInstance();
        /*imageLoader.loadImage(url, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {

            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                //notifyDataSetChanged();
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                if (loadedImage != null) {
                    ivLocation.setImageBitmap(loadedImage);
                }
                //notifyDataSetChanged();
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {
                notifyDataSetChanged();
            }
        });*/
        Picasso.with(activity).load(url)
                .placeholder(R.drawable.img_place_default)
                .resize(376,147)
                .error(R.drawable.img_place_default)
                .into(ivLocation);
    }

    @Override
    public int getItemCount() {
        return listBranch != null ? listBranch.size() : 0;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private final View view;

        ViewHolder(View view) {
            super(view);
            this.view = view;
        }
    }
}
