package com.keaz.landbird.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.keaz.landbird.R;

import java.util.ArrayList;

/**
 * Created by mac on 3/15/17.
 */
public class CustomTimezoneAdapter extends BaseAdapter {
    private final Context context;
    private ArrayList<String> data;

    public CustomTimezoneAdapter(Context context, ArrayList<String> data) {
        this.data = data;
        this.context = context;
    }

    @Override
    public int getCount() {
        return this.data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(convertView==null){
            convertView  = layoutInflater.inflate(R.layout.custom_list_item,null);
        }
        TextView tv = (TextView)convertView.findViewById(R.id.tv);
        tv.setText(data.get(position));

        return convertView;
    }
}
