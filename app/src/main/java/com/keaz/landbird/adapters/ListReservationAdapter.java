package com.keaz.landbird.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;

import com.keaz.landbird.R;
import com.keaz.landbird.activities.BaseActivity;
import com.keaz.landbird.activities.ApprovedBookingActivity;
import com.keaz.landbird.activities.DetailBookingActivity;
import com.keaz.landbird.activities.MainActivity;
import com.keaz.landbird.models.KZBooking;
import com.keaz.landbird.utils.BKGlobals;
import com.keaz.landbird.utils.BKNetworkResponseError;
import com.keaz.landbird.utils.BookingInfoHelper;
import com.keaz.landbird.utils.CommonUtils;
import com.keaz.landbird.utils.ConstantsUtils;
import com.keaz.landbird.utils.DateUtils;
import com.keaz.landbird.utils.FormatUtil;
import com.keaz.landbird.utils.OnResponseModel;
import com.keaz.landbird.utils.UIUtils;
import com.keaz.landbird.utils.VolleyUtils;
import com.keaz.landbird.view.BatteryView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * Created by Administrator on 20/2/2017.
 */

public class ListReservationAdapter extends BaseExpandableListAdapter {

    private LinkedHashMap<String, ArrayList<KZBooking>> bookingList;
    private Context mContext;

    public ListReservationAdapter(LinkedHashMap<String, ArrayList<KZBooking>> list, Context context) {
        this.bookingList = list;
        mContext = context;
    }

    public void notifyDataChanged(LinkedHashMap<String, ArrayList<KZBooking>> list) {
        this.bookingList = list;
        notifyDataSetChanged();
    }

    @Override
    public int getGroupCount() {
        return bookingList != null ? bookingList.size() : 0;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return bookingList != null ? getElementAt(bookingList, groupPosition).size() : 0;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return null;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return null;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.list_booking_group, parent, false);
        }
        String title = (String) bookingList.keySet().toArray()[groupPosition];
        UIUtils.setTextViewText(convertView, R.id.txtHeader, title);
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_booking, parent, false);
        }

        final KZBooking booking = getElementAt(bookingList, groupPosition).get(childPosition);

        UIUtils.setTextViewText(convertView, R.id.txtState, booking.state.toUpperCase());
        UIUtils.setTextViewText(convertView, R.id.txtRegistration, booking.vehicle.name);
        UIUtils.setTextViewText(convertView, R.id.txtFare, "$" + FormatUtil.formatString(booking.cost_sub) + "/" + booking.cost_type);
        UIUtils.setTextViewText(convertView, R.id.txtDate, DateUtils.formatDatePattern(DateUtils.getDate(booking.start), ConstantsUtils.DATE_TIME_FORMAT));
        UIUtils.setTextViewText(convertView, R.id.txtRef, "#" + booking.id);

        BatteryView batteryView = ((BatteryView) convertView.findViewById(R.id.battery));
        String battery = booking.vehicle.battery;
        int percent = (battery != null && !battery.equals("")) ? Integer.parseInt(battery) : 0;
        batteryView.drawByPercent(percent);

        UIUtils.setTextViewText(convertView, R.id.txtPercent, percent + "%");

        BookingInfoHelper.updateBookingTimeZone(booking, convertView, null);

        Picasso.with(mContext).load(booking.vehicle.assets.photo).placeholder(R.drawable.new_img_car)
                .error(R.drawable.new_img_car)
                .into((ImageView) convertView.findViewById(R.id.ivCar));


        BookingInfoHelper.updateBookingInfo1(mContext, booking, convertView, DateUtils.getTimeZoneOffset());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleOpenReservation(booking);
            }
        });


        return convertView;
    }

    private void handleOpenReservation(KZBooking booking) {
        ((BaseActivity)mContext).showCustomDialogLoading();
        VolleyUtils.getSharedNetwork().loadBookingByBookingId(
                mContext,
                booking.id,
                new OnResponseModel<KZBooking>() {

                    @Override
                    public void onResponseSuccess(KZBooking model) {
                        KZBooking uploadedBooking = model;
                        handleOpenBooking(mContext, uploadedBooking);

                        ((BaseActivity)mContext).dismissCustomDialogLoading();
                    }

                    @Override
                    public void onResponseError(BKNetworkResponseError error) {
                        VolleyUtils.handleErrorRespond(((BaseActivity)mContext), error);
                        ((BaseActivity)mContext).dismissCustomDialogLoading();
                    }
                });
    }

    private void actionOpenBookingDetailActivity(KZBooking bookingOnServer) {
        DetailBookingActivity.booking = bookingOnServer;
        Intent intent = new Intent(mContext, DetailBookingActivity.class);
        intent.putExtra(ConstantsUtils.INTENT_EXTRA_CAR_URL, bookingOnServer.vehicle.assets.photo);
        intent.putExtra(ConstantsUtils.INTENT_EXTRA_VEHICLE, bookingOnServer.vehicle);
        intent.putExtra(ConstantsUtils.INTENT_EXTRA_BRANCH, bookingOnServer.branch);
        intent.putExtra(ConstantsUtils.INTENT_EXTRA_START_TIME_IN_MILLIS, bookingOnServer.start);
        intent.putExtra(ConstantsUtils.INTENT_EXTRA_END_TIME_IN_MILLIS, bookingOnServer.end);
        intent.putExtra(ConstantsUtils.INTENT_EXTRA_TRIP_TYPE, bookingOnServer.trip_type);
        intent.putExtra(ConstantsUtils.INTENT_EXTRA_BOOKING_STATUS, bookingOnServer.state);

        mContext.startActivity(intent);
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    private ArrayList<KZBooking> getElementAt(LinkedHashMap<String, ArrayList<KZBooking>> map, int index) {
        return (ArrayList<KZBooking>) map.values().toArray()[index];
    }

    private void handleOpenBooking(Context context, KZBooking bookingOnServer) {

        if (bookingOnServer.state.equalsIgnoreCase(BKGlobals.BookingStatus.APPROVED.toString())) {

            Intent intent = new Intent(context, ApprovedBookingActivity.class);
            ApprovedBookingActivity.booking = bookingOnServer;
            ((Activity) context).startActivityForResult(intent, CommonUtils.CODE_UPDATE_BOOKING);
            ((Activity) context).setResult(Activity.RESULT_OK);

        } else if (bookingOnServer.state.equalsIgnoreCase(BKGlobals.BookingStatus.INUSE.toString())) {

            Intent intent = new Intent(context, MainActivity.class);
            intent.putExtra(ConstantsUtils.INTENT_EXTRA_TAB, 2);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            context.startActivity(intent);
            ((Activity) context).finish();

        }else {
            actionOpenBookingDetailActivity(bookingOnServer);
        }
    }
}
