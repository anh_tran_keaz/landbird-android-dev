package com.keaz.landbird.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.keaz.landbird.R;
import com.keaz.landbird.models.StateModel;
import com.keaz.landbird.view.CustomFontTextView;

import java.util.ArrayList;

/**
 * Created by anhtran1810 on 3/2/18.
 */

public class StateAdapter extends BaseAdapter {

    Context context;
    ArrayList<StateModel> countryNames;
    LayoutInflater layoutInflater;

    public StateAdapter(Context applicationContext, ArrayList<StateModel> countryNames) {
        this.context = applicationContext;
        this.countryNames = countryNames;
        layoutInflater = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return countryNames.size();
    }

    @Override
    public StateModel getItem(int i) {
        return countryNames.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = layoutInflater.inflate(R.layout.spinner_country_item, null);
        CustomFontTextView names = (CustomFontTextView) view.findViewById(R.id.tv_country);
        names.setText(countryNames.get(i).getName());
        return view;
    }
}
