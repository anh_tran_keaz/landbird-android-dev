package com.keaz.landbird.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.keaz.landbird.R;
import com.keaz.landbird.activities.BaseActivity;
import com.keaz.landbird.fragments.dummy.DummyContent.DummyItem;
import com.keaz.landbird.models.KZAccount;
import com.keaz.landbird.models.KZObjectTimeLine;
import com.keaz.landbird.models.KZSearchResult;
import com.keaz.landbird.models.KZTimeObject;
import com.keaz.landbird.models.KZVehicle;
import com.keaz.landbird.utils.BKGlobals;
import com.keaz.landbird.utils.DateUtils;
import com.keaz.landbird.utils.MyLog;
import com.keaz.landbird.utils.OnBookingFragmentListener;
import com.keaz.landbird.utils.OnDateChangeListener;
import com.keaz.landbird.utils.UIUtils;
import com.keaz.landbird.utils.Util;
import com.keaz.landbird.view.BatteryView;
import com.keaz.landbird.view.TimeRulerView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

/**
 * {@link RecyclerView.Adapter} that can display a {@link DummyItem} and makes a call to the
 * specified {@link OnBookingFragmentListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class MyKZBookingRecyclerViewAdapter extends RecyclerView.Adapter<MyKZBookingRecyclerViewAdapter.ViewHolder> {

    private static final String TAG = MyKZBookingRecyclerViewAdapter.class.getSimpleName();
    private static float MIN_DISTANCE = 10;
    private static int SEGMENT_HOUR = 80;
    private static final int MINUTES = 30;
    private static int THRESHOLD = SEGMENT_HOUR;
    private final Context mContext;
    private final OnBookingFragmentListener mListener;
    private KZSearchResult kzSearchResult;
    private boolean isToday;
    private List<KZVehicle> vehicles;
    private List<KZObjectTimeLine> bookings;
    private int mSelectedPos = -1;

    private OnDateChangeListener onDateChangeListener;
    private Date bookedDate;
    private ArrayList<KZTimeObject> arrayList;
    private int hh_start = 0;
    private int hh_end = 0;
    private int min_start = 0;
    private int min_end = 0;
    private boolean isScroll = false;

    public void setToday(boolean today) {
        isToday = today;
    }

    public MyKZBookingRecyclerViewAdapter(KZSearchResult kzSearchResult, boolean isToDay, Context context, List<KZVehicle> items, List<KZObjectTimeLine> bookings, Date date,
                                          OnBookingFragmentListener listener) {
        this.kzSearchResult = kzSearchResult;
        isToday = isToDay;
        vehicles = items;
        this.bookings = bookings;
        mListener = listener;
        mContext = context;
        bookedDate = date;
        SEGMENT_HOUR = Util.convertDp2Px(context,80);
        MIN_DISTANCE = Util.convertDp2Px(context,10);
        THRESHOLD = SEGMENT_HOUR * 3;
    }

    public void setOnDateChangeListener(OnDateChangeListener listener) {
        this.onDateChangeListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        if (position == mSelectedPos) {
            // Selected
            holder.lnAddTime.setVisibility(View.VISIBLE);
            holder.lnVehicle.setBackgroundColor(Color.rgb(44, 48, 57));

            holder.txtPercent.setTextColor(Color.rgb(255, 255, 255));
            holder.labelPrice.setTextColor(Color.rgb(255, 255, 255));
            holder.txtPrice.setTextColor(Color.rgb(255, 255, 255));
            holder.txtRegistration.setTextColor(Color.rgb(255, 255, 255));
            if (!holder.rulerView.getIsDrawn()) {
                holder.rulerView.drawCenterLine(getCenterX(holder));
            }
            holder.rulerView.reDrawWithState(true);
            holder.imvBattery.setBackgroundResource(R.drawable.icn_battery_white_fa_xf242);
            // Set time when scroll the ruler
            if (!holder.rulerView.getIsDrawn()) {
                setTextTime(holder, getCenterX(holder), getCenterX(holder));
            }
        } else {
            // Clear selected
            clearRuler();
            holder.rulerView.clear();
            resetTextTime(holder);
            holder.lnAddTime.setVisibility(View.GONE);
            holder.lnVehicle.setBackgroundColor(Color.rgb(255, 255, 255));

            holder.txtPercent.setTextColor(Color.rgb(26, 26, 26));
            holder.labelPrice.setTextColor(Color.rgb(102, 102, 102));
            holder.txtPrice.setTextColor(Color.rgb(26, 26, 26));
            holder.txtRegistration.setTextColor(Color.rgb(26, 26, 26));
            holder.imvBattery.setBackgroundResource(R.drawable.icn_battery_fa_xf242);
            holder.rulerView.reDrawWithState(false);
        }

        holder.txtRegistration.setText(vehicles.get(position).name);

        holder.lnVehicle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    mListener.onVehicleSelected(vehicles.get(position), position);
                    handleSelectedItem(holder, position, mSelectedPos);
                }
            }
        });
        String battery = vehicles.get(position).battery;
        int percent = (battery != null && !battery.equals("")) ? Integer.parseInt(battery) : 0;
        holder.batteryView.drawByPercent(percent);
        holder.txtPercent.setText(percent + "%");
        String cost = vehicles.get(position).vehicle_cost;
        String cost_type = vehicles.get(position).vehicle_cost_type;
        if (cost == null || cost.equals("")) {
            cost = "NA";
        }
        if (cost_type == null || cost_type.equals("")) {
            cost_type = "NA";
        }
        final String fare = "$" + cost + "/" + cost_type;
        holder.txtPrice.setText(fare);

        holder.reduceTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int oldX = 0;
                if (!holder.rulerView.getIsDrawn()) {
                    oldX = getCenterX(holder);
                    BKGlobals.getSharedGlobals().setOldCoordX(oldX);
                } else {
                    oldX = BKGlobals.getSharedGlobals().getOldCoordX();
                }

                int pixel = holder.rulerView.minuteToPixel(MINUTES);
                int newX = BKGlobals.getSharedGlobals().getNewCoordX() - pixel;
                if (newX <= oldX) {
                    clearRuler();
                    holder.rulerView.clear();
                    resetTextTime(holder);
                    if (!holder.rulerView.getIsDrawn()) {
                        holder.rulerView.drawCenterLine(getCenterX(holder));
                    }
                    // Set time when scroll the ruler
                    if (!holder.rulerView.getIsDrawn()) {
                        setTextTime(holder, getCenterX(holder), getCenterX(holder));
                    }
                    return;
                }
                BKGlobals.getSharedGlobals().setNewCoordX(newX);

                if (isScroll) {
                    int currentHour = BKGlobals.getSharedGlobals().getCurrentHour();
                    hh_start = normalizeHour(currentHour, hh_start);
                    hh_end = normalizeHour(currentHour, hh_end);
                    int startMin = (hh_start * 60) + min_start;
                    int endMin = (hh_end * 60) + min_end;
                    oldX = holder.rulerView.minuteToPixel(startMin);
                    newX = holder.rulerView.minuteToPixel(endMin) - pixel;
                    holder.rulerView.reDraw(oldX, newX);
                } else {
                    holder.rulerView.reDraw(oldX, newX);
                }
                setTextTime(holder, oldX, newX);
                //holder.rulerView.reDraw(oldX, newX);
            }
        });

        holder.increaseTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getDuration() >= getCompanyMaxDuration()) {
                    UIUtils.toast((BaseActivity)mContext, "You have reached max duration for the booking.");
                    return;
                }
                int oldX = 0;
                if (!holder.rulerView.getIsDrawn()) {
                    oldX = getCenterX(holder);
                    BKGlobals.getSharedGlobals().setOldCoordX(oldX);
                    BKGlobals.getSharedGlobals().setNewCoordX(oldX);
                } else {
                    oldX = BKGlobals.getSharedGlobals().getOldCoordX();
                }

                int pixel = holder.rulerView.minuteToPixel(MINUTES);
                int temp = BKGlobals.getSharedGlobals().getNewCoordX();
                int newX =  temp == -1 ? oldX : temp + pixel;
                if (newX < holder.rulerView.getWidth()) {

                    BKGlobals.getSharedGlobals().setNewCoordX(newX);
                    if (isScroll) {
                        int currentHour = BKGlobals.getSharedGlobals().getCurrentHour();
                        hh_start = normalizeHour(currentHour, hh_start);
                        hh_end = normalizeHour(currentHour, hh_end);
                        int startMin = (hh_start * 60) + min_start;
                        int endMin = (hh_end * 60) + min_end;
                        oldX = holder.rulerView.minuteToPixel(startMin);
                        newX = holder.rulerView.minuteToPixel(endMin) + pixel;
                        holder.rulerView.reDraw(oldX, newX);
                    } else {
                        holder.rulerView.reDraw(oldX, newX);
                    }
                    setTextTime(holder, oldX, newX);
                    //holder.rulerView.reDraw(oldX, newX);
                }
            }
        });

        String photo = vehicles.get(position).assets.photo;
        Picasso.with(mContext).load(photo)
                .placeholder(R.drawable.new_img_car)
                .error(R.drawable.new_img_car)
                .into(holder.imvCar);

        // Draw booked cars time
        handleDrawBookedCarTime(holder, position, isToday);
        //scrollTheViewToCurrent(holder);

        holder.scrollView.setSmoothScrollingEnabled(true);
        holder.scrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                if (mSelectedPos == -1 || (mSelectedPos != position)) return;
                int scrollX = holder.scrollView.getScrollX();
                int width = holder.scrollView.getWidth();
                int center = scrollX + (width /2);
                //holder.rulerView.updateStartPixel(center);
                MyLog.debug(TAG, "LEFT_X: " + scrollX);
                MyLog.debug(TAG, "WIDTH: " + width);
                MyLog.debug(TAG, "CENTER_X: " + center);
                if (!holder.rulerView.getIsDrawn()) {
                    MyLog.debug(TAG, "lineCenterX: " + Util.normalizePixel(mContext,center));
                    holder.rulerView.drawCenterLine(center);
                }
                /*if (scrollX - getScrollViewWidth(holder) == 0) {
                    int count = holder.scrollView.getWidth() / SEGMENT_HOUR;
                    holder.rulerView.drawRepeatAtPixel(scrollX, count/2);
                    scrollToEnd(holder);
                }*/


                int offset = scrollX + getScrollViewWidth(holder);
                int rulerWidth = holder.rulerView.getWidth();
                MyLog.debug(TAG, "OFFSET: " + offset);
                MyLog.debug(TAG, "RULER_WIDTH: " + rulerWidth);
                if (offset >= rulerWidth) {
                    int count = holder.scrollView.getWidth() / SEGMENT_HOUR;
                    holder.rulerView.drawRepeatAtPixel(scrollX, count/2);
                    scrollToStart(holder, 0);
                    handleDrawBookedCarTime(holder, position, isToday);
                    if (!holder.rulerView.getIsDrawn()) {
                        holder.rulerView.drawCenterLine(center);
                    }
                    isScroll = true;
                    holder.rulerView.setCurrentSelectedTime(hh_start, min_start, hh_end, min_end);
                }
                // Set time when scroll the ruler
                if (!holder.rulerView.getIsDrawn()) {
                    setTextTime(holder, center, center);
                }
            }
        });

    }

    private void scrollTheViewToCurrent(ViewHolder holder) {
        if(isToday){
            int pixelEnd = holder.rulerView.pixelEnd;
            holder.scrollView.smoothScrollTo((int) pixelEnd,0);

            MyLog.debug(TAG, "pixelEnd: " + pixelEnd);
            MyLog.debug(TAG, "pixelEnd: " + holder.scrollView.getScrollX());

        }
    }

    private void scrollToStart(ViewHolder holder, int scroll) {
        holder.scrollView.smoothScrollTo(scroll, 0);
    }

    private int getScrollViewWidth(ViewHolder holder) {
        int width = holder.scrollView.getWidth();
        return width;
    }

    private int getCenterX(ViewHolder holder) {
        int scrollX = holder.scrollView.getScrollX();
        int width = holder.scrollView.getWidth();
        int center = scrollX + (width /2);
        return center;
    }

    private void handleDrawBookedCarTime(ViewHolder holder, int position, boolean isToday) {

        long branchTimeZone = Long.parseLong(kzSearchResult.start__timezone_offset);

        /**
         * Gray out the ruler from the 00:00 to this moment.
         *
         * */
        if(isToday){
            TimeZone tz = TimeZone.getDefault();
            Date now = new Date();
            int timeZoneOffset = tz.getOffset(now.getTime()) / 1000;

            Calendar calendar1 = Calendar.getInstance();
            Date date = new Date();
            long startTimeAdjusted = calendar1.getTimeInMillis() - timeZoneOffset*1000 - branchTimeZone*1000;
            calendar1.setTimeInMillis(startTimeAdjusted);
            calendar1.set(Calendar.SECOND,0);
            calendar1.set(Calendar.HOUR_OF_DAY,0);
            calendar1.set(Calendar.MINUTE,0);
            calendar1.setTimeInMillis(calendar1.getTimeInMillis());
            Date pastTimeStart = calendar1.getTime();

            Calendar calendar2 = Calendar.getInstance();
            long endTimeAdjusted = calendar2.getTimeInMillis() - timeZoneOffset*1000 - branchTimeZone*1000;
            calendar2.setTimeInMillis(endTimeAdjusted);
            Date pastTimeEnd = calendar2.getTime();
            KZTimeObject pastTimeTimeObject = new KZTimeObject(pastTimeStart, pastTimeEnd);
            ArrayList<KZTimeObject> pastTime = new ArrayList<>();
            pastTime.add(pastTimeTimeObject);
            holder.rulerView.drawUnavailableTime(pastTime);
        }else {
            holder.rulerView.clearUnavailableTime();
        }

        /**
         * Draw booking times
         *
         * */
        if (bookings == null || bookings.size() == 0) {
            holder.rulerView.clearBookedCarTime();
            return;
        }
        arrayList = new ArrayList<>();
        for (int i = 0; i < bookings.size(); i++) {
            KZObjectTimeLine objectTimeLine = bookings.get(i);
            int id = objectTimeLine.vehicle_id;
            int id1 = vehicles.get(position).id;
            if (id == id1) {
                // Draw
                Date dateStart = DateUtils.getDateFromString("yyyy-MM-dd HH:mm", objectTimeLine.start_date);
                Date dateEnd = DateUtils.getDateFromString("yyyy-MM-dd HH:mm", objectTimeLine.end_date);
                if (bookedDate != null) {
                    String booked = DateUtils.formatDatePattern(bookedDate, "yyyy-MM-dd");
                    String start = DateUtils.formatDatePattern(dateStart, "yyyy-MM-dd");
                    if (booked.endsWith(start)) {
                        KZTimeObject timeObject = new KZTimeObject(dateStart, dateEnd);
                        arrayList.add(timeObject);
                    }
                }
            }
        }

        if (arrayList.size() > 0) {
            holder.rulerView.drawListBookedCarTime(arrayList);
        } else {
            holder.rulerView.clearBookedCarTime();
        }
    }

    private void resetTextTime(ViewHolder holder) {
        holder.txtStart.setText("NA");
        holder.txtEnd.setText("NA");
        holder.txtTime.setText("NA");
    }

    private int normalizePixel(int pixel) {
        if (pixel == 0) return 0;
        int sectorWidth = SEGMENT_HOUR / 4;
        int temp = pixel / sectorWidth;
        return temp * sectorWidth;
    }

    private int normalizeHour(int currentHour, int hour) {
        if (currentHour > hour) {
            hour = 24 - currentHour + hour;
        } else {
            hour = Math.abs(currentHour - hour);
        }
        return hour;
    }

    private void setTextTime(ViewHolder holder, int oldX, int newX) {
        int currentHour = BKGlobals.getSharedGlobals().getCurrentHour();
        oldX = normalizePixel(oldX);

        int hhStart = oldX / SEGMENT_HOUR;
        int minStart = (((oldX - (hhStart * SEGMENT_HOUR)) * 60 ) / SEGMENT_HOUR);
        String str_hhStart = "";
        String str_minStart = "";
        int newStart = 0;

        hhStart = hhStart + currentHour >= 24 ? hhStart + currentHour - 24 : hhStart + currentHour;
        //hhStart = normalizeHour(currentHour, hhStart);
        if (hhStart > 12) {
            newStart = hhStart - 12;
        } else {
            newStart = hhStart;
        }
        if (newStart < 10) {
            str_hhStart = "0" + newStart;
        } else {
            str_hhStart = "" + newStart;
        }
        if (minStart < 10) {
            str_minStart = "0" + minStart;
        } else {
            str_minStart = "" + minStart;
        }

        String timeStart = hhStart > 12 ? str_hhStart + ":" + str_minStart + " PM" : str_hhStart + ":" + str_minStart + " AM";
        holder.txtStart.setText(timeStart);

        newX = normalizePixel(newX);

        int hhEnd = newX / SEGMENT_HOUR;
        int minEnd = (((newX - (hhEnd * SEGMENT_HOUR)) * 60) / SEGMENT_HOUR );
        String str_hhEnd = "";
        String str_minEnd = "";
        int newEnd = 0;
        hhEnd = hhEnd + currentHour >= 24 ? hhEnd + currentHour - 24 : hhEnd + currentHour;
        //hhEnd = normalizeHour(currentHour, hhEnd);
        // Cannot book
        if (hhEnd > 12) {
            newEnd = hhEnd - 12;
        } else {
            newEnd = hhEnd;
        }
        if (newEnd < 10) {
            str_hhEnd = "0" + newEnd;
        } else {
            str_hhEnd = "" + newEnd;
        }
        if (minEnd < 10) {
            str_minEnd = "0" + minEnd;
        } else {
            str_minEnd = "" + minEnd;
        }

        String timeEnd = hhEnd > 12 ? str_hhEnd + ":" + str_minEnd + " PM" : str_hhEnd + ":" + str_minEnd + " AM";
        holder.txtEnd.setText(timeEnd);

        String duration = "";
        if (newX > oldX) {
            int hh = (newX - oldX) / SEGMENT_HOUR;
            int mm = (((newX - oldX - (hh * SEGMENT_HOUR)) * 60) / SEGMENT_HOUR);
            duration = hh > 1 ? hh + " hours " : hh + " hour ";
            if (mm > 0) {
                duration += mm + " minutes";
            }
            holder.txtTime.setText(duration);
        }

        if (onDateChangeListener != null) {
            onDateChangeListener.onChange(duration, timeStart, timeEnd, hhStart, minStart, hhEnd, minEnd);
            this.hh_start = hhStart;
            this.hh_end = hhEnd;
            this.min_start = minStart;
            this.min_end = minEnd;
        }
    }

    private int getDuration() {
        int start = (hh_start * 60) + min_start;
        int end = (hh_end * 60) + min_end;
        return end - start;
    }

    private int getCompanyMaxDuration() {
        String duration = KZAccount.getSharedAccount().company.booking_max_duration;
        String type = KZAccount.getSharedAccount().company.booking_max_duration_type;
        int minutes;
        if (type.equalsIgnoreCase(BKGlobals.BookingDuration.MINUTE.toString())) {
            minutes = BKGlobals.BookingDuration.MINUTE.getMinute();
        } else if (type.equalsIgnoreCase(BKGlobals.BookingDuration.HOUR.toString())) {
            minutes = BKGlobals.BookingDuration.HOUR.getMinute();
        } else if (type.equalsIgnoreCase(BKGlobals.BookingDuration.DAY.toString())) {
            minutes = BKGlobals.BookingDuration.DAY.getMinute();
        } else if (type.equalsIgnoreCase(BKGlobals.BookingDuration.WEEK.toString())) {
            minutes = BKGlobals.BookingDuration.WEEK.getMinute();
        } else {
            minutes = BKGlobals.BookingDuration.MONTH.getMinute();
        }

        return Integer.parseInt(duration) * minutes;
    }

    private void handleSelectedItem(ViewHolder holder, int newPos, int oldPos) {
        if (newPos == oldPos) {
            // Clear selected
            holder.lnAddTime.setVisibility(View.GONE);
            holder.lnVehicle.setBackgroundColor(Color.rgb(255, 255, 255));
            mSelectedPos = -1;
            clearRuler();
            holder.rulerView.clear();
            resetTextTime(holder);
            if (mListener != null) {
                mListener.onVehicleSelected(null, -1);
            }
            holder.rulerView.reDrawWithState(false);
            holder.txtPercent.setTextColor(Color.rgb(26, 26, 26));
            holder.labelPrice.setTextColor(Color.rgb(102, 102, 102));
            holder.txtPrice.setTextColor(Color.rgb(26, 26, 26));
            holder.txtRegistration.setTextColor(Color.rgb(26, 26, 26));
            holder.imvBattery.setBackgroundResource(R.drawable.icn_battery_fa_xf242);
        } else {
            clearRuler();
            holder.rulerView.clear();
            resetTextTime(holder);
            holder.lnAddTime.setVisibility(View.VISIBLE);
            holder.lnVehicle.setBackgroundColor(Color.rgb(44, 48, 57));
            mSelectedPos = newPos;

            //holder.rulerView.updateStartPixel(getCenterX(holder));
            if (!holder.rulerView.getIsDrawn()) {
                holder.rulerView.drawCenterLine(getCenterX(holder));
            }
            holder.rulerView.reDrawWithState(true);
            holder.txtPercent.setTextColor(Color.rgb(255, 255, 255));
            holder.labelPrice.setTextColor(Color.rgb(255, 255, 255));
            holder.txtPrice.setTextColor(Color.rgb(255, 255, 255));
            holder.txtRegistration.setTextColor(Color.rgb(255, 255, 255));
            holder.imvBattery.setBackgroundResource(R.drawable.icn_battery_white_fa_xf242);
            // Set time when scroll the ruler
            if (!holder.rulerView.getIsDrawn()) {
                setTextTime(holder, getCenterX(holder), getCenterX(holder));
            }
        }
    }

    private void clearRuler() {
        BKGlobals.getSharedGlobals().setOldCoordX(-1);
        BKGlobals.getSharedGlobals().setNewCoordX(-1);
        BKGlobals.getSharedGlobals().setCurrentHour(0);
        if (onDateChangeListener != null) {
            onDateChangeListener.onChange("NA", "NA", "NA", -1,-1,-1,-1);
        }
        hh_start = 0;
        hh_end = 0;
        min_start = 0;
        min_end = 0;
        isScroll = false;
    }

    @Override
    public int getItemCount() {
        return vehicles != null ? vehicles.size() : 0;
    }

    public void fireNotifyDatasetChanged() {
        notifyDataSetChanged();
    }

    public void notifyDataChanged(List<KZVehicle> list, List<KZObjectTimeLine> listBooking, Date date, KZSearchResult searchResult) {
        vehicles = list;
        bookings = listBooking;
        bookedDate = date;
        kzSearchResult = searchResult;
        notifyDataSetChanged();
    }

    public void setSelectedIndex(int index) {
        mSelectedPos = index;
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        private final ImageView imvBattery;
        private final TextView txtPercent;
        private final TextView labelPrice;
        public KZVehicle vehicle;
        private ImageView imvCar;
        private LinearLayout lnAddTime;
        private LinearLayout lnVehicle;
        private TextView txtRegistration;
        private TextView txtStart;
        private TextView txtEnd;
        private TextView txtTime;
        private TextView txtPrice;
        private TimeRulerView rulerView;
        private BatteryView batteryView;
        private LinearLayout reduceTime;
        private LinearLayout increaseTime;
        private HorizontalScrollView scrollView;

        ViewHolder(View view) {
            super(view);
            mView = view;
            imvCar = (ImageView) view.findViewById(R.id.imvCar);
            imvBattery = (ImageView) view.findViewById(R.id.ivBattery);
            txtRegistration = (TextView) view.findViewById(R.id.textRegistration);
            txtPercent = (TextView) view.findViewById(R.id.textPercent);
            labelPrice = (TextView) view.findViewById(R.id.labelPrice);
            txtPrice = (TextView) view.findViewById(R.id.txtFareRate);
            txtStart = (TextView) view.findViewById(R.id.txtStart);
            txtEnd = (TextView) view.findViewById(R.id.txtEnd);
            txtTime = (TextView) view.findViewById(R.id.txtTime);
            txtPrice = (TextView) view.findViewById(R.id.txtFareRate);
            lnVehicle = (LinearLayout) view.findViewById(R.id.lnVehicle);
            lnAddTime = (LinearLayout) view.findViewById(R.id.lnAddTime);
            lnAddTime.setVisibility(View.GONE);
            rulerView = (TimeRulerView) view.findViewById(R.id.rulerView);
            batteryView = (BatteryView) view.findViewById(R.id.battery);
            reduceTime = (LinearLayout) view.findViewById(R.id.reduceTime);
            increaseTime = (LinearLayout) view.findViewById(R.id.increaseTime);
            scrollView = (HorizontalScrollView) view.findViewById(R.id.scroll);
        }

        @Override
        public String toString() {
            return super.toString();
        }
    }

}
