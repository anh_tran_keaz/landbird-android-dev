package com.keaz.landbird.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;

import com.keaz.landbird.R;
import com.keaz.landbird.interfaces.ItemClickListener;
import com.keaz.landbird.models.KZBooking;
import com.keaz.landbird.utils.BookingInfoHelper;
import com.keaz.landbird.utils.ConstantsUtils;
import com.keaz.landbird.utils.DateUtils;
import com.keaz.landbird.utils.FormatUtil;
import com.keaz.landbird.utils.ToastUtil;
import com.keaz.landbird.utils.UIUtils;
import com.keaz.landbird.view.BatteryView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * Created by Administrator on 20/2/2017.
 */

public class BookingListAdapter extends BaseExpandableListAdapter {

    private ItemClickListener itemClickListener;
    private LinkedHashMap<String, ArrayList<KZBooking>> bookingList;
    private Context mContext;

    public BookingListAdapter(LinkedHashMap<String, ArrayList<KZBooking>> list, Context context, ItemClickListener itemClickListener) {
        this.bookingList = list;
        this.itemClickListener = itemClickListener;
        mContext = context;
    }

    public void notifyDataChanged(LinkedHashMap<String, ArrayList<KZBooking>> list) {
        this.bookingList = list;
        notifyDataSetChanged();
    }

    @Override
    public int getGroupCount() {
        return bookingList != null ? bookingList.size() : 0;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return bookingList != null ? getElementAt(bookingList, groupPosition).size() : 0;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return null;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return null;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.list_booking_group, parent, false);
        }
        String title = (String) bookingList.keySet().toArray()[groupPosition];
        UIUtils.setTextViewText(convertView, R.id.txtHeader, title);
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_booking, parent, false);
        }
        KZBooking booking = null;
        if(bookingList.isEmpty()){
            ToastUtil.showToastMessage(mContext, "Booking list empty", true);
            return convertView;
        }else {
            booking = getElementAt(bookingList, groupPosition).get(childPosition);

        }

        UIUtils.setTextViewText(convertView, R.id.txtState, (booking.state.equals("inuse")?"in use":booking.state).toUpperCase());
        UIUtils.setTextViewText(convertView, R.id.txtName, booking.vehicle.name);
        UIUtils.setTextViewText(convertView, R.id.txtRegistration, booking.vehicle.registration);
        UIUtils.setTextViewText(convertView, R.id.txtFare, "$" + FormatUtil.formatString(booking.cost_sub) + "/" + booking.cost_type);
        UIUtils.setTextViewText(convertView, R.id.txtDate, DateUtils.formatDatePattern(DateUtils.getDate(booking.start), ConstantsUtils.DATE_TIME_FORMAT));
        UIUtils.setTextViewText(convertView, R.id.txtRef, "#" + booking.id);

        BatteryView batteryView = ((BatteryView) convertView.findViewById(R.id.battery));
        String battery = booking.vehicle.battery;
        int percent = (battery != null && !battery.equals("")) ? Integer.parseInt(battery) : 0;
        batteryView.drawByPercent(percent);

        UIUtils.setTextViewText(convertView, R.id.txtPercent, percent + "%");

        BookingInfoHelper.updateBookingTimeZone(booking, convertView, null);

        Picasso.with(mContext).load(booking.vehicle.assets.photo).placeholder(R.drawable.new_img_car)
                .error(R.drawable.new_img_car)
                .into((ImageView) convertView.findViewById(R.id.ivCar));


        BookingInfoHelper.updateBookingInfo1(mContext, booking, convertView, DateUtils.getTimeZoneOffset());

        final KZBooking finalBooking = booking;
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemClickListener.onClick(finalBooking);
            }
        });

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }


    private ArrayList<KZBooking> getElementAt(LinkedHashMap<String, ArrayList<KZBooking>> map, int index) {
        return (ArrayList<KZBooking>) map.values().toArray()[index];
    }

}
