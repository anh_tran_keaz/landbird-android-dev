package com.keaz.landbird.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Rect;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.keaz.landbird.R;
import com.keaz.landbird.fragments.dummy.DummyContent.DummyItem;
import com.keaz.landbird.models.BlockModal;
import com.keaz.landbird.models.KZAccount;
import com.keaz.landbird.models.KZDurations;
import com.keaz.landbird.models.KZObjectTimeLine;
import com.keaz.landbird.models.KZSearchResult;
import com.keaz.landbird.models.KZVehicle;
import com.keaz.landbird.utils.ConfigUtil;
import com.keaz.landbird.utils.ConstantsUtils;
import com.keaz.landbird.utils.FormatUtil;
import com.keaz.landbird.utils.OnBookingFragmentListener;
import com.keaz.landbird.utils.Util;
import com.keaz.landbird.view.BatteryView;
import com.keaz.landbird.view.CustomFontTextView;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link DummyItem} and makes a call to the
 * specified {@link OnBookingFragmentListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class CustomRecyclerAdapter_VehicleOfASelectedBranch extends RecyclerView.Adapter<CustomRecyclerAdapter_VehicleOfASelectedBranch.ViewHolder> {

    private static int SEGMENT_HOUR = 80;

    private final Context mContext;
    private final OnBookingFragmentListener mListener;
    private List<KZVehicle> vehicles;
    private int mSelectedPos = -1;
    private String trip_type;

    class ViewHolder extends RecyclerView.ViewHolder {
        final View mView;
        private final ImageView imvBattery;
        private final TextView txtPercent;
        private final TextView labelPrice;
        public KZVehicle vehicle;
        private ImageView imvCar;
        private LinearLayout lnVehicle;
        private LinearLayout lilReasons;
        private TextView txtRegistration;
        private TextView txtFareRate;
        private BatteryView batteryView;
        private RecyclerView rvBookingTime;
        private TimeBookingAdapter adapter ;
        private ArrayList<BlockModal> timeArray ;

        ViewHolder(View view) {
            super(view);

            loadBookingTimeList();

            mView = view;
            imvCar = view.findViewById(R.id.imvCar);
            imvBattery = view.findViewById(R.id.ivBattery);
            txtRegistration = view.findViewById(R.id.textRegistration);
            txtPercent = view.findViewById(R.id.textPercent);
            labelPrice = view.findViewById(R.id.labelPrice);
            txtFareRate = view.findViewById(R.id.txtFareRate);
            lnVehicle = view.findViewById(R.id.lnVehicle);
            batteryView = view.findViewById(R.id.battery);
            rvBookingTime = view.findViewById(R.id.recycleView);
            lilReasons = view.findViewById(R.id.lil_reasons);

            timeArray = new ArrayList<>();
            rvBookingTime.setLayoutManager(new LinearLayoutManager(mView.getContext(), LinearLayoutManager.HORIZONTAL, false));
            rvBookingTime.addItemDecoration(new SpacesItemDecoration(5));

            rvBookingTime.setClickable(true);
            adapter = new TimeBookingAdapter(timeArray, this);
            rvBookingTime.setAdapter(adapter);
        }

        void loadBookingTimeList() {
            KZDurations durations = KZAccount.getSharedAccount().company.durations;

            int minDuration = (int) (durations.booking_min_duration_time / 60);
            int maxDuration = (int) ((durations.booking_max_duration_time) / 60);
            int blockDuration =  (int) (KZAccount.getSharedAccount().company.company_booking_block_duration / 60);
            int interval = (maxDuration-minDuration+blockDuration) / blockDuration;

            ArrayList<String> bookingTimeList = new ArrayList<>();

            for (int i = 0; i < interval; i++) {
                int time = (minDuration>blockDuration?minDuration:blockDuration) + blockDuration * i;
                bookingTimeList.add("" + time);
            }
        }

        void enableBookingViewTime(boolean isEnable) {
            if(adapter != null) adapter.enableTimeView(isEnable);
        }

        void bindTimeBookingData(ArrayList<BlockModal> timeArray) {
            this.timeArray = timeArray;
            adapter.notifyDataSetChanged(this.timeArray , vehicle.available_to_block!=null?Integer.parseInt(vehicle.available_to_block):0);
        }

        private class TimeBookingAdapter extends RecyclerView.Adapter {

            private final ViewHolder viewHolder;
            private int viewHolderPosition;
            private ArrayList<BlockModal> timeArray;
            private int block;

            private class TimeViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
                CustomFontTextView tvTime ;
                String strValue ;
                RelativeLayout rlBookingItem;
                int position;

                TimeViewHolder(View itemView) {
                    super(itemView);
                    tvTime = itemView.findViewById(R.id.tv_title);
                    rlBookingItem = itemView.findViewById(R.id.rlBookingItem);
                    rlBookingItem.setLayoutParams(new RelativeLayout.LayoutParams(Util.DEVICE_WIDTH / 4 - 5, RelativeLayout.LayoutParams.WRAP_CONTENT));
                    rlBookingItem.setOnClickListener(this);
                }

                void bindData(boolean isAvailable, int position) {
                    this.position = position;
                    this.strValue = (timeArray != null && timeArray.size() > 0) ? timeArray.get(position).getTextToShow() : "";
                    tvTime.setText(strValue/*FormatTimeUtils.getFormatDurationTimeValue(strValue)*/);
                    if (isAvailable) {
                        tvTime.setTextColor(Color.rgb(255, 255, 255));
                        rlBookingItem.setBackgroundResource(R.drawable.selector_booking_time);
                        rlBookingItem.setEnabled(true);
                    } else {
                        tvTime.setTextColor(ConstantsUtils.COLOR_BG_DARK);
                        rlBookingItem.setBackgroundResource(R.drawable.selector_unbooking_time);
                        rlBookingItem.setEnabled(false);
                    }
                }

                @Override
                public void onClick(View v) {
                    int id = v.getId();
                    boolean isSelected = selectVehicle(viewHolder, viewHolderPosition);
                    if(isSelected){
                        if(id == rlBookingItem.getId()) {
                            int durationInMinute = timeArray.get(this.position).getValueToUp();
                        }
                    }
                }
            }

            void setViewHolderPosition(int viewHolderPosition) {
                this.viewHolderPosition = viewHolderPosition;
            }

            TimeBookingAdapter(ArrayList<BlockModal> timeArray, ViewHolder viewHolder) {
                this.timeArray = timeArray;
                this.viewHolder = viewHolder;
            }

            void notifyDataSetChanged(ArrayList<BlockModal> timeArray, int block) {
                this.timeArray = timeArray ;
                this.block = block;
                this.notifyDataSetChanged();
            }

            void enableTimeView(boolean isEnable) {
                this.notifyDataSetChanged();
            }

            @NotNull
            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_booking_time, parent, false);
                return new TimeViewHolder(view);
            }

            @Override
            public void onBindViewHolder(@NotNull RecyclerView.ViewHolder holder, int position) {
                ((TimeViewHolder) holder).bindData(position < this.block , position);
            }

            @Override
            public int getItemCount() {
                return timeArray.size();
            }
        }

        class SpacesItemDecoration extends RecyclerView.ItemDecoration {
            private int space;

            SpacesItemDecoration(int space) {
                this.space = space;
            }

            @Override
            public void getItemOffsets(Rect outRect, @NotNull View view, @NotNull RecyclerView parent, RecyclerView.State state) {
                outRect.right = space;
            }
        }


    }

    public CustomRecyclerAdapter_VehicleOfASelectedBranch(String trip_type, KZSearchResult kzSearchResult, boolean isToDay, Context context, List<KZVehicle> items, List<KZObjectTimeLine> bookings, Date date, OnBookingFragmentListener listener) {
        vehicles = items;
        mListener = listener;
        mContext = context;
        SEGMENT_HOUR = Util.convertDp2Px(context,80);
        this.trip_type = trip_type;
    }

    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_item_vehicle, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NotNull final ViewHolder holder, int position) {
        if (position == mSelectedPos) {
            holder.enableBookingViewTime(true);
        } else {
            holder.enableBookingViewTime(false);
        }

        holder.vehicle = vehicles.get(position);

        Log.d("XXX","status: " + holder.vehicle.status);
        final boolean condition2 = holder.vehicle.containTripType(trip_type);
        final boolean condition1 = !holder.vehicle.status.equals("unavailable");

        if(condition1 && condition2){
            //holder.lnVehicle.setBackground(mContext.getResources().getDrawable(R.drawable.round_white));
            holder.lnVehicle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectVehicle(holder, holder.getAdapterPosition());
                }
            });
        }else {
            //holder.lnVehicle.setBackground(mContext.getResources().getDrawable(R.drawable.round_gray));
            holder.lnVehicle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(!condition1) Toast.makeText(mContext, "Sorry this vehicle is not available", Toast.LENGTH_SHORT).show();
                    if(!condition2) Toast.makeText(mContext, "Sorry this vehicle is not available for "+trip_type+" trip", Toast.LENGTH_SHORT).show();
                }
            });
        }

        showInfoRelatedToCost(holder);
        showVehicleInfo(holder,position);
        showVehiclePicture(holder);

        holder.lilReasons.removeAllViews();
        for (int i = 0; i < holder.vehicle.reasons.size(); i++) {
            TextView txtReason = new TextView(mContext);
            txtReason.setText(holder.vehicle.reasons.get(i));
            txtReason.setTextColor(Color.rgb(255, 0, 0));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.topMargin = 5;
            txtReason.setLayoutParams(params);

            holder.lilReasons.addView(txtReason);
        }

        /*String[] bookingTimeList = new String[mBookingTimeList.size()];
        for (int i = 0; i < mBookingTimeList.size(); i++) {
            bookingTimeList[i] = mBookingTimeList.get(i);
        }*/

        ArrayList<BlockModal> blockFromConfig = ConfigUtil.getConfigBlock();

        holder.bindTimeBookingData(blockFromConfig);
        holder.adapter.setViewHolderPosition(holder.getAdapterPosition());
    }

    private void showVehiclePicture(ViewHolder holder) {
        String photo = holder.vehicle.assets.photo;
        Picasso.with(mContext).load(photo)
                .placeholder(R.drawable.new_img_car)
                .error(R.drawable.new_img_car)
                .into(holder.imvCar);
    }

    private void showVehicleInfo(ViewHolder holder, int position) {
        holder.txtRegistration.setText(holder.vehicle.name);
        String battery = holder.vehicle.battery;
        int percent = (battery != null && !battery.equals("")) ? Integer.parseInt(battery) : 0;
        holder.batteryView.drawByPercent(percent);
        holder.txtPercent.setText(percent + "%");
    }

    @Override
    public int getItemCount() {
        return vehicles != null ? vehicles.size() : 0;
    }

    private void showInfoRelatedToCost(ViewHolder holder) {

        if(/*ConfigUtil.checkIfShowBookingCost(trip_type)*/trip_type.equals(ConstantsUtils.TRIP_TYPE_PRIVATE)){

            /*
            //holder.mView.findViewById(R.id.viewVehicleFareRate).setVisibility( View.VISIBLE);
            String cost = null, cost_type = null;
            switch (ConfigUtil.defineShowBookingCostType(trip_type)){
                case ConstantsUtils.SHOW_COST_PRIVATE:
                    if(ConfigUtil.checkIfShowEstimateCost()){
                        cost = holder.vehicle.cost_private_sub+"";
                        cost_type = holder.vehicle.cost_private_type;
                    }else {
                        cost = holder.vehicle.vehicle_private_cost;
                        cost_type = holder.vehicle.vehicle_private_cost_type;
                    }

                    break;
                case ConstantsUtils.SHOW_COST_BUSINESS:
                    if(ConfigUtil.checkIfShowEstimateCost()){
                        cost = holder.vehicle.cost_business_sub+"";
                        cost_type = holder.vehicle.cost_business_type;
                    }else {
                        cost = holder.vehicle.vehicle_cost;
                        cost_type = holder.vehicle.vehicle_cost_type;
                    }
                    break;
                case 0:
                    break;
            }
            if (cost == null || cost.equals("")) cost = "NA";
            if (cost_type == null || cost_type.equals("")) cost_type = "NA";
            final String fare = "$" + FormatUtil.formatString(cost) + "/" + cost_type;
            holder.txtFareRate.setText(fare);*/

            final String fare =
                    "$" + FormatUtil.formatString(String.valueOf(holder.vehicle.daily_estimate_cost)) + "/" + "Daily" +"   "
                            + "$" + FormatUtil.formatString(String.valueOf(holder.vehicle.weekly_estimate_cost)) + "/" + "Weekly" +"   "
                            + "$" + FormatUtil.formatString(String.valueOf(holder.vehicle.monthly_estimate_cost)) + "/" + "Monthly"
                    ;
            holder.txtFareRate.setText(fare);

        }else {
            holder.mView.findViewById(R.id.viewVehicleFareRate).setVisibility(View.GONE);
        }
    }

    private boolean selectVehicle(ViewHolder holder, int holderPos) {
        if (null != mListener) {
            mListener.onVehicleSelected(vehicles.get(holderPos), holderPos);
            //return handleSelectedItem(holder, holderPos, mSelectedPos);
        }
        return false;
    }

    public void fireNotifyDataSetChanged() {
        notifyDataSetChanged();
    }

    public void notifyDataChanged(List<KZVehicle> list, List<KZObjectTimeLine> listBooking, Date date, KZSearchResult searchResult) {
        vehicles = list;
        notifyDataSetChanged();
    }

    public void setSelectedIndex(int index) {
        mSelectedPos = index;
        notifyDataSetChanged();
    }

}
