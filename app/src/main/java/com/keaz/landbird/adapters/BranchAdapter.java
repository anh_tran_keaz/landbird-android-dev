package com.keaz.landbird.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.keaz.landbird.R;
import com.keaz.landbird.models.KZBranch;
import com.keaz.landbird.view.CustomFontTextView;

import java.util.List;

/**
 * Created by TamTran on 17/02/2017.
 */

public class BranchAdapter extends BaseAdapter {

    Context context;
    List<KZBranch> branchNames;
    LayoutInflater layoutInflater;

    public BranchAdapter(Context applicationContext, List<KZBranch> countryNames) {
        this.context = applicationContext;
        this.branchNames = countryNames;
        layoutInflater = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return branchNames.size();
    }

    @Override
    public KZBranch getItem(int i) {
        return branchNames.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = layoutInflater.inflate(R.layout.spinner_country_item, null);
        CustomFontTextView names = (CustomFontTextView) view.findViewById(R.id.tv_country);
        names.setText(branchNames.get(i).name);
        return view;
    }
}
