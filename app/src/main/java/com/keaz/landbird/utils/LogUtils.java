package com.keaz.landbird.utils;

import android.util.Log;

/**
 * Created by Thinh Lai on 6/10/15.
 * Copyright (c) 2015 Thinh Lai. All rights reserved.
 */
public class LogUtils {

    public static final String TAG = "Envoy";

    public static void log(String message) {
//        if(BuildConfig.IS_DEVELOPMENT)
            Log.v(TAG,message == null ? "LOG IS NULL" : message);
    }

    public static void log(String tagSourceHost, String s) {
        Log.d(tagSourceHost, s);
    }
}
