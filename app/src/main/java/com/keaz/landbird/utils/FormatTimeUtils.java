package com.keaz.landbird.utils;

import android.text.format.DateFormat;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by keaz on 10/12/17.
 */

public class FormatTimeUtils {

    public static final String FORMAT1 = "dd-MM-yyyy";
    private static final String FORMAT2 = "yyyy-MM-dd hh:mm:ss a";

    static String formatDurationInMillis1(long durationInMillis) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(durationInMillis);
        long day = durationInMillis/(24*60*60*1000);
        String dayText = "";
        String hourTxt = "";
        String minTxt = "";
        if(day>0) dayText = day + " day ";
        if(day>1) dayText = day + " days ";
        durationInMillis = durationInMillis - day*24*60*60*1000;
        long hour = durationInMillis/(60*60*1000);
        if(hour>0) hourTxt = hour + " hour ";
        if(hour>1) hourTxt = hour + " hours ";
        durationInMillis = durationInMillis - hour*60*60*1000;
        long min = durationInMillis/(60*1000);
        if(min>0) minTxt = min + " minute";
        if(min>1) minTxt = min + " minutes";

        return dayText.concat(hourTxt).concat(minTxt) ;
    }

    public static String formatDurationInMillis2(long durationInMillis) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(durationInMillis);
        long day = durationInMillis/(24*60*60*1000);
        String dayText = "";
        String hourTxt = "";
        String minTxt = "";
        if(day>0) dayText = day + " day ";
        if(day>1) dayText = day + " days ";
        durationInMillis = durationInMillis - day*24*60*60*1000;
        long hour = durationInMillis/(60*60*1000);
        if(hour>0) hourTxt = hour + " hour ";
        if(hour>1) hourTxt = hour + " hours ";
        durationInMillis = durationInMillis - hour*60*60*1000;
        long min = durationInMillis/(60*1000);
        if(min>0) minTxt = min + " minute";
        if(min>1) minTxt = min + " minutes";

        return dayText.concat(hourTxt).concat(minTxt) ;
    }

    public static String formatDurationInMillis3(long durationInMillis) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(durationInMillis);
        long day = durationInMillis/(24*60*60*1000);
        String dayText = "";
        String hourTxt = "";
        String minTxt = "";
        if(day>0) dayText = day + " day ";
        if(day>1) dayText = day + " days ";
        durationInMillis = durationInMillis - day*24*60*60*1000;
        long hour = durationInMillis/(60*60*1000);
        if(hour>0) hourTxt = hour + " hour ";
        if(hour>1) hourTxt = hour + " hours ";
        durationInMillis = durationInMillis - hour*60*60*1000;
        long min = durationInMillis/(60*1000);
        if(min>0) minTxt = min + " minute";
        if(min>1) minTxt = min + " minutes";

        return dayText.concat(hourTxt).concat(minTxt) ;
    }

    public static String formatDate2(Calendar calendar) {
        String rs = DateFormat.format(FORMAT1, calendar.getTime()).toString();
        return rs;
    }

    private static String formatDate(long timeStamp, String format){
        String rs = DateFormat.format(format, timeStamp).toString();
        return rs;
    }

    public static String changeFormatDate(String format1, String format2, String s) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format1, Locale.getDefault());
        try {
            Date date = simpleDateFormat.parse(s);
            return formatDate(date.getTime(), format2);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

}
