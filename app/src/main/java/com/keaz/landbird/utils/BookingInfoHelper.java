package com.keaz.landbird.utils;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.keaz.landbird.R;
import com.keaz.landbird.models.KZBooking;

import java.util.Calendar;

/**
 * Created by anhtran1810 on 3/20/18.
 */

public class BookingInfoHelper {

    public static void updateBookingInfo1(Context context, KZBooking booking, View convertView, int timeZoneOffset) {
        ((TextView) convertView.findViewById(R.id.txtState)).setTextColor(context.getResources().getColor(R.color.geoTab_textWhite));
        convertView.findViewById(R.id.txtState).setBackground(ColorUtil.getTextBackgroundOfBooking(context, booking.state));

        convertView.findViewById(R.id.view_range).setVisibility(View.VISIBLE);
        if (booking.state.equalsIgnoreCase(BKGlobals.BookingStatus.APPROVED.toString())) {
            UIUtils.setTextViewText(convertView, R.id.txtTitleDuration, "Duration: ");
            // duration = end - start
            UIUtils.setTextViewText(convertView, R.id.txtDuration, DateUtils.getDurationFormatText1(booking.actual_start, booking.end));

            // Title range
            UIUtils.setTextViewText(convertView, R.id.txtTitleRange, "Start at: ");
            // Range
            String startEnd = DateUtils.getTimeFromSec(booking.actual_start - timeZoneOffset - booking.start_timezone_offset);
            UIUtils.setTextViewText(convertView, R.id.txtStartEnd, startEnd);

        } else if (booking.state.equalsIgnoreCase(BKGlobals.BookingStatus.INUSE.toString())) {
            UIUtils.setTextViewText(convertView, R.id.txtTitleDuration, "Due back in: ");
            // duration = end - now
            Calendar calendar = DateUtils.getCalendarWithTimeZoneFrom(Calendar.getInstance(), DateUtils.getTimeZoneOffset(), booking.end_timezone_offset);
            UIUtils.setTextViewText(convertView, R.id.txtDuration, DateUtils.getDurationFormatText1(calendar.getTimeInMillis()/1000, booking.end - timeZoneOffset - booking.end_timezone_offset));

            // Title range
            UIUtils.setTextViewText(convertView, R.id.txtTitleRange, "Return in: ");
            // Range
            String end = DateUtils.getTimeFromSec(booking.end - timeZoneOffset - booking.end_timezone_offset);
            UIUtils.setTextViewText(convertView, R.id.txtStartEnd, end);

        } else if (booking.state.equalsIgnoreCase(BKGlobals.BookingStatus.COMPLETE.toString())) {
            UIUtils.setTextViewText(convertView, R.id.txtTitleDuration, "Duration: ");
            // duration = end - start
            UIUtils.setTextViewText(convertView, R.id.txtDuration, DateUtils.getDurationFormatText1(booking.actual_start, booking.actual_end));
            convertView.findViewById(R.id.view_range).setVisibility(View.VISIBLE);
            if(convertView.findViewById(R.id.viewDuration)!=null) {
                convertView.findViewById(R.id.viewDuration).setVisibility(View.GONE);
            }

            // Title range
            UIUtils.setTextViewText(convertView, R.id.txtTitleRange, "");
            // Range
            String startEnd = "Start at " + DateUtils.getTimeFromSec(booking.actual_start - timeZoneOffset - booking.start_timezone_offset)
                    + "\nEnd at " + DateUtils.getTimeFromSec(booking.actual_end - timeZoneOffset - booking.end_timezone_offset);
            UIUtils.setTextViewText(convertView, R.id.txtStartEnd, startEnd);
        } else {
            UIUtils.setTextViewText(convertView, R.id.txtTitleDuration, "Duration: ");
            // duration = end - start
            UIUtils.setTextViewText(convertView, R.id.txtDuration, DateUtils.getDurationFormatText1(booking.actual_start, booking.actual_end));

            // Title range
            UIUtils.setTextViewText(convertView, R.id.txtTitleRange, "");
            // Range
            String startEnd = "Start at "+DateUtils.getTimeFromSec(booking.actual_start - timeZoneOffset - booking.start_timezone_offset)
                    + "\nEnd at " + DateUtils.getTimeFromSec(booking.actual_end - timeZoneOffset - booking.end_timezone_offset);
            UIUtils.setTextViewText(convertView, R.id.txtStartEnd, startEnd);
        }
    }

    public static void updateBookingInfo2(KZBooking booking, Activity activity, int timeZoneOffset) {

        ((TextView) activity.findViewById(R.id.txtState)).setTextColor(activity.getResources().getColor(R.color.geoTab_textWhite));
        activity.findViewById(R.id.txtState).setBackground(ColorUtil.getTextBackgroundOfBooking(activity, booking.state));
        if (booking.state.equalsIgnoreCase(BKGlobals.BookingStatus.APPROVED.toString())) {
            UIUtils.setTextViewText(activity, R.id.txtTitleDuration, "Duration: ");
            // duration = end - start
            UIUtils.setTextViewText(activity, R.id.txtDuration, DateUtils.getDurationFormatText1(booking.actual_start, booking.end));

            // Title range
            UIUtils.setTextViewText(activity, R.id.txtTitleRange, "Start at: ");
            // Range
            String startEnd = DateUtils.getTimeFromSec(booking.actual_start - timeZoneOffset - booking.start_timezone_offset);
            UIUtils.setTextViewText(activity, R.id.txtStartEnd, startEnd);

        } else if (booking.state.equalsIgnoreCase(BKGlobals.BookingStatus.INUSE.toString())) {
            UIUtils.setTextViewText(activity, R.id.txtTitleDuration, "Due back in: ");
            // duration = end - now
            Calendar calendar = DateUtils.getCalendarWithTimeZoneFrom(Calendar.getInstance(), DateUtils.getTimeZoneOffset(), booking.end_timezone_offset);
            UIUtils.setTextViewText(activity, R.id.txtDuration, DateUtils.getDurationFormatText1(calendar.getTimeInMillis()/1000, booking.end - timeZoneOffset - booking.end_timezone_offset));

            // Title range
            UIUtils.setTextViewText(activity, R.id.txtTitleRange, "Return in: ");
            // Range
            String end = DateUtils.getTimeFromSec(booking.end - timeZoneOffset - booking.end_timezone_offset);
            UIUtils.setTextViewText(activity, R.id.txtStartEnd, end);

        } else {
            UIUtils.setTextViewText(activity, R.id.txtTitleDuration, "Duration: ");
            // duration = end - start
            UIUtils.setTextViewText(activity, R.id.txtDuration, DateUtils.getDurationFormatText1(booking.actual_start, booking.actual_end));

            // Title range
            UIUtils.setTextViewText(activity, R.id.txtTitleRange, "Range: ");
            // Range
            String startEnd = DateUtils.getTimeFromSec(booking.actual_start - timeZoneOffset - booking.start_timezone_offset)
                    + " - " + DateUtils.getTimeFromSec(booking.actual_end - timeZoneOffset - booking.end_timezone_offset);
            UIUtils.setTextViewText(activity, R.id.txtStartEnd, startEnd);
        }
    }

    public static void updateBookingTimeZone(KZBooking booking, View convertView, Activity activity) {
        String startTimeName = booking.start_timezone_abbr;
        if (startTimeName == null)
            startTimeName = "";
        long startTimezone = booking.start_timezone_offset / -3600;
        String strStartTimezone;
        if (startTimezone >= 0) {
            strStartTimezone = "+" + startTimezone;
        }else{
            strStartTimezone = "" + startTimezone;
        }
        if(convertView!=null) UIUtils.setTextViewText(convertView, R.id.txtTimeZone, startTimeName + " (UTC " + strStartTimezone + ")");
        if(activity!=null) UIUtils.setTextViewText(activity, R.id.txtTimeZone, startTimeName + " (UTC " + strStartTimezone + ")");
    }
}
