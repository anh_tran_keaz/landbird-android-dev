package com.keaz.landbird.utils;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 16/2/2017.
 */

public class JsonUtils {

    public static <T> T parseJsonObject(Class<T> clazz, String json) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        T rs = gson.fromJson(json, clazz);
        return rs;
    }

    public static <T> T parseByModel(String responseData, Class<T> classOfT) {
        T result = null;
        if (!TextUtils.isEmpty(responseData)) {
            GsonBuilder gsonBuilder = new GsonBuilder();
            Gson gson = gsonBuilder.create();
            try {
                result = gson.fromJson(responseData, classOfT);
            } catch (Exception e) {
                Log.d("parsejson",e.getMessage());
            }
        }
        return result;
    }

    public static <T> List<T> parseJsonArray(String json) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        Type listType = new TypeToken<List<T>>() {
        }.getType();
        return gson.fromJson(json, listType);
    }

    public static Map<String, Object> jsonToMap(JSONObject json) throws JSONException {
        Map<String, Object> retMap = new HashMap<String, Object>();

        if(json != JSONObject.NULL) {
            retMap = toMap(json);
        }
        return retMap;
    }

    public static Map<String, Object> toMap(JSONObject object) throws JSONException {
        Map<String, Object> map = new HashMap<String, Object>();

        Iterator<String> keysItr = object.keys();
        while(keysItr.hasNext()) {
            String key = keysItr.next();
            Object value = object.get(key);

            if(value instanceof JSONArray) {
                value = toList((JSONArray) value);
            }

            else if(value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            map.put(key, value);
        }
        return map;
    }

    public static List<Object> toList(JSONArray array) throws JSONException {
        List<Object> list = new ArrayList<Object>();
        for(int i = 0; i < array.length(); i++) {
            Object value = array.get(i);
            if(value instanceof JSONArray) {
                value = toList((JSONArray) value);
            }

            else if(value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            list.add(value);
        }
        return list;
    }

    public static ArrayList<JSONObject> parseJsonStringToListOfJsonObject(String wordIdJson) {
        if(wordIdJson==null) return new ArrayList<>();
        Type type = new TypeToken<ArrayList<String>>(){}.getType();
        Gson gson = new Gson();
        ArrayList<String> list = gson.fromJson(wordIdJson,type);
        ArrayList<JSONObject> jsonObjects = new ArrayList<>();
        for (String s: list) {
            try {
                JSONObject j = new JSONObject(s);
                jsonObjects.add(j);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return jsonObjects;
    }

    public static JSONArray loadJSONFromAsset(Context context, String fileName) {
        String json = null;
        try {
            InputStream is = context.getAssets().open(fileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        try {
            json = json.replaceAll("\n", "");
            JSONArray jsonObject = new JSONArray(json);
            return jsonObject;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static JSONArray createJSONArray(Context context, String fileName) {
        JSONArray jsonArray = JsonUtils.loadJSONFromAsset(context, fileName);
        return jsonArray;
    }

    public static String[] getArr(JSONArray jsonArray) {
        final String[] arr = new String[jsonArray.length()];
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject;
            try {
                jsonObject = jsonArray.getJSONObject(i);
                String name = jsonObject.getString("name"); // basketball
                arr[i] = name;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return arr;
    }
}
