package com.keaz.landbird.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.keaz.landbird.R;

/**
 * Created by keaz on 10/23/17.
 */

public class NetworkUtil {
    public static boolean checkInternetState(Context context) {
        ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = connMgr.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
        return isConnected;
    }

    public static boolean checkInternetStateAndShowDialog(Context context, DialogInterface.OnClickListener onClickListener){
        if(!checkInternetState(context)){
            DialogUtils.showMessageDialog(context,
                    false,
                    context.getResources().getString(R.string.tittle_network),
                    context.getResources().getString(R.string.message_network),
                    context.getString(R.string.btn_okie),
                    "",
                    onClickListener,
                    null);
            return false;
        }
        return true;
    }
}
