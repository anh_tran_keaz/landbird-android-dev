package com.keaz.landbird.utils;

import android.text.InputType;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.widget.EditText;
import android.widget.ImageView;

import com.keaz.landbird.R;


/**
 * Created by keaz on 5/19/16.
 */
public class ManagePasswordUtil {


    public static boolean changePasswordAndTextType(EditText passwordText, ImageView imvEyePassword) {
        boolean showPassword;

        if (passwordText.getInputType() != InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD) {
            passwordText.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
            passwordText.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            showPassword = true;

            imvEyePassword.setImageResource(R.drawable.new_ic_show_pass);

        } else {
            passwordText.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
            passwordText.setTransformationMethod(PasswordTransformationMethod.getInstance());
            showPassword = false;

            imvEyePassword.setImageResource(R.drawable.new_ic_hide_pass);
        }
        passwordText.setSelection(passwordText.getText().toString().length());

        return showPassword;

    }


}
