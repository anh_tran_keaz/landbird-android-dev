package com.keaz.landbird.utils;

/**
 * Created by anhtran1810 on 3/6/18.
 */

public class FormatUtil {
    public static String formatNumber(double v) {
        return String.format("%.2f", v);
    }

    public static String formatString(String s) {
        if(s==null) return null;
        if(s.isEmpty()) return "";
        double d = Double.parseDouble(s);
        return formatNumber(d);
    }

    public static String formatIn2Digit(int i) {
        return String.format("%02d",i);
    }
}
