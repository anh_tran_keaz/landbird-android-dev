package com.keaz.landbird.utils;

import android.content.Context;
import android.os.Handler;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.keaz.landbird.R;
import com.keaz.landbird.interfaces.CustomAnimationListener;

/**
 * Created by anhtran1810 on 12/5/17.
 */

public class CustomAnimationUtil {

    public void animation(Context mContext, final boolean appearOrDisappear, final View view, int animationId, int delayInMillis, int durationMillis, final CustomAnimationListener customAnimationListener){
        if(mContext==null)return;
        if(view==null) return;
        final Animation animation =  AnimationUtils.loadAnimation(mContext, animationId);
        if(durationMillis!=0){
            animation.setDuration(durationMillis);
        }
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                view.setVisibility(appearOrDisappear ? View.VISIBLE :View.INVISIBLE);
                if(customAnimationListener!=null) {
                    customAnimationListener.onAnimationEnd();
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        if(delayInMillis!=0){
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    view.startAnimation(animation);
                }
            },delayInMillis);
        }else {
            view.startAnimation(animation);
        }
    }

    public void toggleAnimation(Context mContext, int style, final View view1,final View view2, final CustomAnimationListener customAnimationListener) {
        Animation anim1;
        Animation anim2;
        switch (style){
            case 1:
                anim1 = AnimationUtils.loadAnimation(mContext, R.anim.fade_out_normal);
                anim1.setDuration(500);
                anim2 = AnimationUtils.loadAnimation(mContext,R.anim.fade_in_normal);
                anim2.setDuration(500);
                break;
            default:
                anim1 = AnimationUtils.loadAnimation(mContext,R.anim.fade_out_normal);
                anim2 = AnimationUtils.loadAnimation(mContext,R.anim.fade_in_normal);
                break;

        }

        final Animation finalAnim = anim2;
        anim1.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                view1.setVisibility(View.INVISIBLE);
                view2.startAnimation(finalAnim);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        anim2.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                view2.setVisibility(View.VISIBLE);
                if(customAnimationListener!=null) customAnimationListener.onAnimationEnd();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        view1.startAnimation(anim1);
    }
}
