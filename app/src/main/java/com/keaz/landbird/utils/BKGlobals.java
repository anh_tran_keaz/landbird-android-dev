package com.keaz.landbird.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;

/**
 * Created by Harradine on 9/12/2014.
 */
public class BKGlobals {

    public final static String BOOKING_STATE_REQUEST = "request";
    public final static String BOOKING_STATE_PENDING = "pending";
    public final static String BOOKING_STATE_APPROVED = "approved";
    public final static String BOOKING_STATE_INUSE = "inuse";
    public final static String BOOKING_STATE_INSPECTION = "inspection";
    public final static String BOOKING_STATE_COMPLETE = "complete";
    public final static String BOOKING_STATE_CANCELLED = "cancelled";
    public final static String BOOKING_STATE_EXPIRED = "expired";

    public static final String ENABLE_FIXED_TIMEZONE = "enable fixed timezone";
    public static final String ENABLE_BRANCH_TIMEZONE = "enable branch timezone";
    public static final String COMPANY_TIMEZONE_OFFSET = "company timezone offset";
    public static final String ACCESS_TOKEN = "access token";
    public static final int RELOAD_TIME = 10;
    public static final int RELOAD_TIME_10 = 10000;
    public static final int RELOAD_TIME_60 = 60000;
    static private BKGlobals sharedGlobals;
    private Context applicationContext;
    private int oldCoordX = -1;
    private int curCoordX = -1;
    private int newCoordX = -1;

    public int getCurrentHour() {
        return currentHour;
    }

    public void setCurrentHour(int currentHour) {
        this.currentHour = currentHour;
    }

    private int currentHour = 0;

    private BKGlobals(Context applicationContext) {
        this.applicationContext = applicationContext;
    }

    static public BKGlobals getSharedGlobals() {
        assert null != sharedGlobals;
        return sharedGlobals;
    }

    static public void createSharedGlobals(Context applicationContext) {
        if (null == sharedGlobals) {
            sharedGlobals = new BKGlobals(applicationContext);
        } else {
            Log.v("BKGlobals", "Warning: Shared globals already exist.");
        }
    }

    static String getMetadata(String key, String defaultValue) {
        String value = defaultValue;
        try {
            BKGlobals globals = BKGlobals.getSharedGlobals();
            ApplicationInfo ai = globals.getPackageManager().getApplicationInfo(
                    BKGlobals.getSharedGlobals().getPackageName(), PackageManager.GET_META_DATA);
            Bundle bundle = ai.metaData;
            value = bundle.getString(key);
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("BKUtils", "Failed to load meta-data, NameNotFound: " + e.getMessage());
        } catch (NullPointerException e) {
            Log.e("BKUtils", "Failed to load meta-data, NullPointer: " + e.getMessage());
        }
        return value;
    }

    public int getOldCoordX() {
        return oldCoordX;
    }

    public void setOldCoordX(int oldCoordX) {
        this.oldCoordX = oldCoordX;
    }

    public int getCurCoordX() {
        return curCoordX;
    }

    public void setCurCoordX(int curCoordX) {
        this.curCoordX = curCoordX;
    }

    public int getNewCoordX() {
        return newCoordX;
    }

    public void setNewCoordX(int newCoordX) {
        this.newCoordX = newCoordX;
    }

    public Context getApplicationContext() {
        return applicationContext;
    }

    String getPackageName() {
        return applicationContext.getPackageName();
    }

    PackageManager getPackageManager() {
        return applicationContext.getPackageManager();
    }

    public SharedPreferences getSharedPreferences() {
        return applicationContext.getSharedPreferences(getPackageName(), Context.MODE_PRIVATE);
    }

    public SharedPreferences.Editor getSharedPreferencesEditor() {
        return getSharedPreferences().edit();
    }

    public String getVersion() {
        try {
            return applicationContext.getPackageManager().getPackageInfo(applicationContext.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            Log.v("BKGlobals", e.getLocalizedMessage());
        }
        return "";
    }

    public void saveBoolPreferences(String key, boolean value) {
        getSharedPreferencesEditor().putBoolean(key, value).commit();
    }

    public void saveStringPreferences(String key, String value) {
        getSharedPreferencesEditor().putString(key, value).commit();
    }

    public void saveLongPreferences(String key, long value) {
        getSharedPreferencesEditor().putLong(key, value).commit();
    }

    public boolean getBoolPreferences(String key, boolean value) {
        return getSharedPreferences().getBoolean(key, value);
    }

    public String getStringPreferences(String key, String value) {
        return getSharedPreferences().getString(key, value);
    }

    public long getLongPreferences(String key, long value) {
        return getSharedPreferences().getLong(key, value);
    }

    public enum BookingStatus {
        REQUEST("request"),
        PENDING("pending"),
        APPROVED("approved"),
        INUSE("inuse"),
        INSPECTION("inspection"),
        COMPLETE("complete"),
        CANCELLED("cancelled"),
        EXPIRED("expired");

        private final String name;

        private BookingStatus(String s) {
            name = s;
        }

        public boolean equalsName(String otherName) {
            return (otherName == null) ? false : name.equals(otherName);
        }

        public String toString() {
            return this.name;
        }
    }

    public enum TripType {
        PRIVATE("Private"),
        BUSINESS("Business");

        private String name;

        private TripType(String name) {
            this.name = name;
        }

        public String toString() {
            return this.name;
        }
    }

    public enum BookingDuration {
        MINUTE("minute"),
        HOUR("hour"),
        DAY("day"),
        WEEK("week"),
        MONTH("month");

        private String name;

        BookingDuration(String name) {
            this.name = name;
        }

        public String toString() {
            return this.name;
        }

        public int getMinute() {
            switch (this.name) {
                case "minute":
                    return 1;
                case "hour":
                    return 60;
                case "day":
                    return 24 * 60;
                case "week":
                    return 7 * 24 * 60;
                case "month":
                    return 30 * 24 * 60;
            }
            return 60;
        }
    }
}
