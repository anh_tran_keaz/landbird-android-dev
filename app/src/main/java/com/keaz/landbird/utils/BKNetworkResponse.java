package com.keaz.landbird.utils;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Harradine on 3/29/2014.
 */
public class BKNetworkResponse {
    @SerializedName("status")
    public int status;

    @SerializedName("response_error")
    public BKNetworkResponseError response_error;
}
