package com.keaz.landbird.utils;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.thin.downloadmanager.DefaultRetryPolicy;
import com.thin.downloadmanager.DownloadRequest;
import com.thin.downloadmanager.DownloadStatusListener;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Random;

/**
 * Created by mac on 5/26/16.
 */
public class ImageUtils {
    private static int sColor;

    public static void deleteImage(FragmentActivity activity, String imagePath) {
        File fdelete = new File(imagePath);
        if (fdelete.exists()) {
            if (fdelete.delete()) {
                Log.e("-->", "file Deleted :" + imagePath);
                callBroadCast(activity);
            } else {
                Log.e("-->", "file not Deleted :" + imagePath);
            }
        }
    }

    private static void callBroadCast(FragmentActivity activity) {
        if (Build.VERSION.SDK_INT >= 14) {
            MediaScannerConnection.scanFile(activity, new String[]{
                    Environment.getExternalStorageDirectory().toString()}, null, new MediaScannerConnection.OnScanCompletedListener() {

                public void onScanCompleted(String path, Uri uri) {
                    Log.e("ExternalStorage", "Scanned " + path + ":");
                    Log.e("ExternalStorage", "-> uri=" + uri);
                }
            });
        } else {
            activity.sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED,
                    Uri.parse("file://" + Environment.getExternalStorageDirectory())));
        }
    }

    public static void buildPart(DataOutputStream dataOutputStream, byte[] fileData) throws IOException {

        ByteArrayInputStream fileInputStream = new ByteArrayInputStream(fileData);
        int bytesAvailable = fileInputStream.available();

        int maxBufferSize = 1024 * 1024;
        int bufferSize = Math.min(bytesAvailable, maxBufferSize);
        byte[] buffer = new byte[bufferSize];

        // read file and write it into form...
        int bytesRead = fileInputStream.read(buffer, 0, bufferSize);

        while (bytesRead > 0) {
            dataOutputStream.write(buffer, 0, bufferSize);
            bytesAvailable = fileInputStream.available();
            bufferSize = Math.min(bytesAvailable, maxBufferSize);
            bytesRead = fileInputStream.read(buffer, 0, bufferSize);
        }
    }

    public static Bitmap showImageFromPath(Context context, ImageView imageView, View parentView, String imagePath) {
        if (imagePath != null) {
            Bitmap bitmap = createBitmapFromFile(imagePath, parentView);
            if (bitmap != null) {
                imageView.setImageBitmap(bitmap);
                return bitmap;
            } else {
                //imageView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.img_avatar_default));
            }
        } else {
            //imageView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.img_avatar_default));
        }
        return null;
    }

    public static Bitmap createBitmapFromFile(String imagePath, View parent) {
        File image = new File(imagePath);
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        Bitmap bitmap = BitmapFactory.decodeFile(image.getAbsolutePath(), bmOptions);
        Bitmap rs;
        if (bitmap != null) {
            rs = Bitmap.createScaledBitmap(bitmap, parent.getLayoutParams().width/*500*/, parent.getLayoutParams().height/*500*/, true);
        } else {
            rs = null;
        }
        return rs;
    }

    public static Bitmap createBitmapFromFile(String imagePath) {
        File image = new File(imagePath);
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        Bitmap bitmap = BitmapFactory.decodeFile(image.getAbsolutePath(), bmOptions);
        return bitmap;
    }

    public static Bitmap createCircleBitmap(Bitmap scaleBitmapImage) {
        int targetWidth = 1000;
        int targetHeight = 1000;
        Bitmap targetBitmap = Bitmap.createBitmap(targetWidth,
                targetHeight, Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(targetBitmap);
        Path path = new Path();
        path.addCircle(((float) targetWidth - 1) / 2,
                ((float) targetHeight - 1) / 2,
                (Math.min(((float) targetWidth),
                        ((float) targetHeight)) / 2),
                Path.Direction.CCW);

        canvas.clipPath(path);
        canvas.drawBitmap(scaleBitmapImage,
                new Rect(0, 0, scaleBitmapImage.getWidth(),
                        scaleBitmapImage.getHeight()),
                new Rect(0, 0, targetWidth, targetHeight), null);
        return targetBitmap;
    }

    public static Drawable createRandomColorCircleDrawable() {
        int color;
        if (sColor == 0) {
            String[] colors = new String[]{"#f495df", "#59c5c3"};
            color = Color.parseColor(colors[randInt(0, colors.length - 1)]);
            sColor = color;
        } else {
            color = sColor;
        }
        GradientDrawable drawable = new GradientDrawable();
        drawable.setColor(color);
        drawable.setShape(GradientDrawable.OVAL);

        drawable.setStroke(3, color);
        drawable.setSize(300, 300);
        return drawable;
    }

    public static int randInt(int min, int max) {

        // NOTE: This will (intentionally) not run as written so that folks
        // copy-pasting have to think about how to initialize their
        // Random instance.  Initialization of the Random instance is outside
        // the main scope of the question, but some decent options are to have
        // a field that is initialized once and then re-used as needed or to
        // use ThreadLocalRandom (if using at least Java 1.7).
        Random rand = new Random();

        // nextInt is normally exclusive of the top value,
        // so add 1 to make it inclusive
        int randomNum = rand.nextInt((max - min) + 1) + min;

        return randomNum;
    }

    // DownLoad Image
    public static DownloadRequest downLoadUserImage(Context context, Uri downloadUri, Uri destinationUri, DownloadStatusListener downloadListener) {
        DownloadRequest downloadRequest = new DownloadRequest(downloadUri)
                //.addCustomHeader("Auth-Token", "YourTokenApiKey")
                .setRetryPolicy(new DefaultRetryPolicy())
                .setDestinationURI(destinationUri).setPriority(DownloadRequest.Priority.HIGH)
                .setDownloadContext(context)//Optional
                .setDownloadListener(downloadListener);
        return downloadRequest;
    }

    public static String getImagePath(Context context, Uri uri) {
        //1.Query
        Cursor cursor = context.getContentResolver().query(
                uri,
                new String[]{android.provider.MediaStore.Images.ImageColumns.DATA},
                null,
                null,
                null);

        String path;

        if (cursor != null) {
            cursor.moveToFirst();
            path = cursor.getString(0);
            //Nhớ close cursor để tránh memory leak
            cursor.close();
        } else {
            path = uri.getPath();
        }

        return path;
    }

    public static Bitmap drawableToBitmap (Drawable drawable) {
        Bitmap bitmap = null;

        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if(bitmapDrawable.getBitmap() != null) {
                return bitmapDrawable.getBitmap();
            }
        }

        if(drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
        } else {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }

    public static byte[] getFileDataFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }
}
