package com.keaz.landbird.utils;

import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.support.v4.content.FileProvider;

import java.io.File;

/**
 * Created by mac on 3/6/17.
 */
public class UriUtils {
    public static Uri getUri(Context context,File filePhotoCurrent) {
        Uri photoURI;
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.M){
            photoURI = FileProvider.getUriForFile(context, context.getApplicationContext().getPackageName() + ".provider", filePhotoCurrent);
        } else {
            photoURI = Uri.fromFile(filePhotoCurrent);
        }
        return photoURI;
    }
}
