package com.keaz.landbird.utils;

import android.app.Activity;
import android.view.View;

import com.keaz.landbird.R;

/**
 * Created by anhtran1810 on 4/6/18.
 */

public class LoadingViewUtils {

    public static boolean showOrHideProgressBar(boolean b, View view) {
        if(view==null) return b;
        if(view.findViewById(R.id.custom_loading_view)==null) return b;
        if(b){
            view.findViewById(R.id.custom_loading_view).setVisibility(View.VISIBLE);
            return b;
        }else {
            view.findViewById(R.id.custom_loading_view).setVisibility(View.GONE);
            return b;
        }
    }

    public static boolean showOrHideProgressBar(boolean b, Activity activity) {
        if(activity==null) return b;
        if(activity.findViewById(R.id.custom_loading_view)==null) return b;
        if(b){
            activity.findViewById(R.id.custom_loading_view).setVisibility(View.VISIBLE);
            return true;
        }else {
            activity.findViewById(R.id.custom_loading_view).setVisibility(View.GONE);
            return false;
        }
    }
}
