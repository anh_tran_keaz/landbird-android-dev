package com.keaz.landbird.utils;

import android.content.Intent;

/**
 * Created by anhtran1810 on 11/29/17.
 */

public class IntentUtil {

    //Add flag to clear last activity
    public static Intent addFlag1(Intent intent) {
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        return intent;
    }

    public static Intent addFlag2(Intent intent) {
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        return intent;
    }
}
