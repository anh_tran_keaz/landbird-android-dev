package com.keaz.landbird.utils;

import android.content.Context;
import android.net.Uri;

import com.thin.downloadmanager.DefaultRetryPolicy;
import com.thin.downloadmanager.DownloadRequest;
import com.thin.downloadmanager.DownloadStatusListener;

/**
 * Created by anhtran1810 on 12/13/17.
 */

public class DownloadUtils {

    public static DownloadRequest downLoadUserImage(Context context, Uri downloadUri, Uri destinationUri, DownloadStatusListener downloadListener) {
        return new DownloadRequest(downloadUri)
                .setRetryPolicy(new DefaultRetryPolicy())
                .setDestinationURI(destinationUri).setPriority(DownloadRequest.Priority.HIGH)
                .setDownloadContext(context)//Optional
                .setDownloadListener(downloadListener);
    }
}
