package com.keaz.landbird.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;

import com.keaz.landbird.BuildConfig;
import com.keaz.landbird.R;
import com.keaz.landbird.enums.PAYMENT_REQUIRED_ENUM;
import com.keaz.landbird.enums.SHOW_REGISTRATION;
import com.keaz.landbird.helpers.DevModeHelper;
import com.keaz.landbird.interfaces.CustomListener4;
import com.keaz.landbird.models.BlockModal;
import com.keaz.landbird.models.KZAccount;
import com.keaz.landbird.models.KZBooking;
import com.keaz.landbird.models.KZCompany;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by anhtran1810 on 3/23/18.
 */

public class ConfigUtil {

    public static int getTimeZoneOffset(){
        String s = KZAccount.getSharedAccount().getDateTime().default_timezone_offset;
        if(s.equals("client")){
            return KZAccount.getSharedAccount().getDateTime().user_timezone_offset;
        }else {
            return KZAccount.getSharedAccount().getDateTime().company_timezone_offset;
        }
    }

    public static boolean config1IsAvailable() {
        if(KZAccount.getSharedAccount()!=null){
            if(KZAccount.getSharedAccount().getKZLandingData1()!=null){
                return true;
            }
        }
        return false;
    }

    public static boolean configBookingDurationIsAvailable(){
        if(KZAccount.getSharedAccount().getCompany()!=null){
            if(KZAccount.getSharedAccount().getCompany().durations!=null){
                return true;
            }
        }
        return false;
    }

    public static int getConfigMinPasswordLength() {
        if(config1IsAvailable()){
            int minPassLength = KZAccount.getSharedAccount().getKZLandingData1().password_min_length;
            return minPassLength;
            /*if(minPassLength!=null && minPassLength.length()>0){
                return Integer.parseInt(minPassLength);
            }*/
        }
        return ConstantsUtils.DEFAULT_MIN_PASSWORD_LENGTH;
    }

    public static int getConfigMaxPasswordLength() {
        if(config1IsAvailable()){
            int maxPassLength = KZAccount.getSharedAccount().getKZLandingData1().password_max_length;
            return maxPassLength;
            /*if(maxPassLength!=null && maxPassLength.length()>0){
                return Integer.parseInt(maxPassLength);
            }*/
        }
        return ConstantsUtils.DEFAULT_MAX_PASSWORD_LENGTH;
    }

    public static double getConfigBookingMaxDurationTimeInSecond(){
        if(!configBookingDurationIsAvailable() ||
                KZAccount.getSharedAccount().getCompany().durations.booking_max_duration_time==0) {
            return ConstantsUtils.DEFAULT_MAX_BOOKING_DURATION_TIME_IN_SECOND;
        }
        return KZAccount.getSharedAccount().getCompany().durations.booking_max_duration_time;
    }

    public static int getConfigBookingMinDurationTimeInSecond(){
        if(!configBookingDurationIsAvailable() ||
                KZAccount.getSharedAccount().getCompany().durations.booking_min_duration_time==0) {
            return ConstantsUtils.DEFAULT_MIN_BOOKING_DURATION_TIME_SECOND;
        }
        return (int) KZAccount.getSharedAccount().getCompany().durations.booking_min_duration_time;
    }

    public static int getConfigBookingMinDurationText(){
        if(!configBookingDurationIsAvailable() ||
                KZAccount.getSharedAccount().getCompany().durations.booking_min_duration==0) {
            return ConstantsUtils.DEFAULT_MIN_BOOKING_DURATION_TEXT;
        }
        return KZAccount.getSharedAccount().getCompany().durations.booking_min_duration;
    }

    public static double getConfigBookingMaxDurationText(){
        if(!configBookingDurationIsAvailable() ||
                KZAccount.getSharedAccount().getCompany().durations.booking_max_duration==0) {
            return ConstantsUtils.DEFAULT_MAX_BOOKING_DURATION_TEXT;
        }
        return KZAccount.getSharedAccount().getCompany().durations.booking_max_duration;
    }

    public static String getConfigBookingMaxDurationType(){
        if(!configBookingDurationIsAvailable() ||
                KZAccount.getSharedAccount().getCompany().durations.booking_max_duration_type==null ||
                KZAccount.getSharedAccount().getCompany().durations.booking_max_duration_type.isEmpty()) {
            return ConstantsUtils.DEFAULT_MAX_BOOKING_DURATION_TYPE;
        }
        return KZAccount.getSharedAccount().getCompany().durations.booking_max_duration_type;
    }

    public static String getConfigBookingMinDurationType(){
        if(!configBookingDurationIsAvailable() ||
                KZAccount.getSharedAccount().getCompany().durations.booking_min_duration_type==null ||
                KZAccount.getSharedAccount().getCompany().durations.booking_min_duration_type.isEmpty()) {
            return ConstantsUtils.DEFAULT_MIN_BOOKING_DURATION_TYPE;
        }
        return KZAccount.getSharedAccount().getCompany().durations.booking_min_duration_type;
    }

    public static int getConfigCompanyBookingBlockDurationInMinute(){
        if(!configBookingDurationIsAvailable()
                || KZAccount.getSharedAccount().getCompany()==null
                //|| KZAccount.getSharedAccount().getCompany().company_booking_block_duration.isEmpty()
                ) {
            return ConstantsUtils.DEFAULT_BOOKING_BLOCK_DURATION_INTERVAL_IN_SECOND / 60;
        }
        long intervalInSec = KZAccount.getSharedAccount().getCompany().company_booking_block_duration;
        return (int) (intervalInSec/60);
    }

    public static boolean checkIfBookingDurationIsValidWithConfigOrShowErrorDialog(Context context, long bookingDurationInSecond /*Calendar selectedCalendarFrom, Calendar selectedCalendarTo*/) {
        //long bookingDurationInSecond = selectedCalendarTo.getTimeInMillis()/1000 - selectedCalendarFrom.getTimeInMillis()/1000;
        if(bookingDurationInSecond < getConfigBookingMinDurationTimeInSecond()){
            String message = context.getString(R.string.text_booking_minimum) + " "+ getConfigBookingMinDurationText()+" "+ getConfigBookingMinDurationType()+ "s\n" + context.getString(R.string.text_enter_new_dates);
            DialogUtils.showMessageDialog(context,
                    true,
                    "",
                    message,
                    context.getString(R.string.btn_okie),
                    "",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.cancel();
                        }
                    }, null);
            return false;
        }
        if(bookingDurationInSecond < getConfigBookingMinDurationTimeInSecond() || bookingDurationInSecond > getConfigBookingMaxDurationTimeInSecond()){
            String message = context.getString(R.string.text_booking_maximum) + " "+ getConfigBookingMaxDurationText()+" "+ getConfigBookingMaxDurationType()+ "s\n" + context.getString(R.string.text_enter_new_dates);
            DialogUtils.showMessageDialog(context,
                    true,
                    "", message,
                    context.getString(R.string.btn_okie),
                    "",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.cancel();
                        }
                    }, null);
            return false;
        }
        return true;
    }

    public static boolean checkIfBookingDurationIsValidWithConfigOrShowErrorDialog(Context context, Calendar selectedCalendarFrom, Calendar selectedCalendarTo) {
        long bookingDurationInSecond = selectedCalendarTo.getTimeInMillis()/1000 - selectedCalendarFrom.getTimeInMillis()/1000;
        if(bookingDurationInSecond < getConfigBookingMinDurationTimeInSecond()){
            String message = "Unfortunately, bookings are limited to minimum "+ getConfigBookingMinDurationText()+" "+ getConfigBookingMinDurationType()+ "s\nPlease enter new dates and try again.";
            DialogUtils.showMessageDialog(context, true, "", message, "Ok", "",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.cancel();
                        }
                    }, null);
            return false;
        }
        if(bookingDurationInSecond < getConfigBookingMinDurationTimeInSecond() || bookingDurationInSecond > getConfigBookingMaxDurationTimeInSecond()){
            String message = "Unfortunately, bookings are limited to maximum "+ getConfigBookingMaxDurationText()+" "+ getConfigBookingMaxDurationType()+ "s\nPlease enter new dates and try again.";
            DialogUtils.showMessageDialog(context, true, "", message, "Ok", "",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.cancel();
                        }
                    }, null);
            return false;
        }
        return true;
    }

    public static String getConfigTripType(){
        String s = KZAccount.getSharedAccount().getCompany().limit_booking_type;
        if(s.isEmpty()) return ConstantsUtils.TRIP_TYPE_BOTH;
        if(s.equals(ConstantsUtils.TRIP_TYPE_BUSINESS)) return ConstantsUtils.TRIP_TYPE_BUSINESS;
        if(s.equals(ConstantsUtils.TRIP_TYPE_PRIVATE)) return ConstantsUtils.TRIP_TYPE_PRIVATE;

        return ConstantsUtils.TRIP_TYPE_BOTH;
    }

    public static PAYMENT_REQUIRED_ENUM checkIfPaymentIsRequired(String trip_type) {
        boolean result;
        switch (trip_type){
            case ConstantsUtils.TRIP_TYPE_BUSINESS:
                result = KZAccount.getSharedAccount().company.enable_charge_business &&
                        !KZAccount.getSharedAccount().company.require_payment_business.equals("off") ;
                if(result){
                    if(KZAccount.getSharedAccount().company.booking_business_charge_to.equals("driver")) return PAYMENT_REQUIRED_ENUM.REQUIRED_DRIVER;
                    if(KZAccount.getSharedAccount().company.booking_business_charge_to.equals("cost_centre")) return PAYMENT_REQUIRED_ENUM.REQUIRED_COST_CENTER;
                    if(KZAccount.getSharedAccount().company.booking_business_charge_to.equals("company")) return PAYMENT_REQUIRED_ENUM.REQUIRED_COMPANY;

                }else {
                    return PAYMENT_REQUIRED_ENUM.NO_REQUIRED;
                }
                break;
            case ConstantsUtils.TRIP_TYPE_PRIVATE:
                result = KZAccount.getSharedAccount().company.enable_charge_private &&
                        !KZAccount.getSharedAccount().company.require_payment_private.equals("off");

                if(result){
                    if(KZAccount.getSharedAccount().company.booking_private_charge_to.equals("driver")) return PAYMENT_REQUIRED_ENUM.REQUIRED_DRIVER;
                    if(KZAccount.getSharedAccount().company.booking_private_charge_to.equals("cost_centre")) return PAYMENT_REQUIRED_ENUM.REQUIRED_COST_CENTER;
                    if(KZAccount.getSharedAccount().company.booking_private_charge_to.equals("company")) return PAYMENT_REQUIRED_ENUM.REQUIRED_COMPANY;

                }else {
                    return PAYMENT_REQUIRED_ENUM.NO_REQUIRED;
                }
                break;
            default:
                return PAYMENT_REQUIRED_ENUM.NO_REQUIRED;
        }
        return PAYMENT_REQUIRED_ENUM.NO_REQUIRED;
    }

    public static boolean checkIfShouldShowRFIDWarning() {
        if(KZAccount.getSharedAccount().getCompany().enable_warning_rfid && KZAccount.getSharedAccount().getUser().rfid==null) return true;
        if(KZAccount.getSharedAccount().getCompany().enable_warning_rfid && KZAccount.getSharedAccount().getUser().rfid.isEmpty()) return true;
        return false;
    }

    public static Calendar adjustCurrentTimeFollowConfigBlockDuration(Calendar calendar) {
        int a = getConfigCompanyBookingBlockDurationInMinute();

        double minute = calendar.get(Calendar.MINUTE);
        double s = minute/a;

        int result = (int) Math.ceil(s);
        int rs = result * a;

        if(rs==60){
            calendar.set(Calendar.MINUTE,0);
            calendar.set(Calendar.HOUR, calendar.get(Calendar.HOUR)+1);
            return calendar;
        }else {
            calendar.set(Calendar.MINUTE, rs);
            return calendar;
        }
    }

    public static String getConfigBookingCheckout(){
        return KZAccount.getSharedAccount().getCompany().booking_checkout;
    }

    public static String getConfigEndBookingScreenTimeStepInSec(){
        return KZAccount.getSharedAccount().getCompany().company_booking_min_duration;
    }

    public static KZCompany getCompany() {
        return KZAccount.getSharedAccount().getCompany();
    }

    public static long getConfigStartBookingEarlyTimeInSec() {
        return KZAccount.getSharedAccount().getCompany().start_booking_early_time;
    }

    public static String getConfigSourceHost(){
        /*if(!config1IsAvailable()){
            return BuildConfig.HOST;
        }*/
        String s = KZAccount.getSharedAccount().getSource_host();
        if(s==null) {
            LogUtils.log(ConstantsUtils.TAG_SOURCE_HOST, BuildConfig.HOST);
            return BuildConfig.HOST;
        }
        if(s.isEmpty()) {
            LogUtils.log(ConstantsUtils.TAG_SOURCE_HOST, BuildConfig.HOST);
            return BuildConfig.HOST;
        }
        LogUtils.log(ConstantsUtils.TAG_SOURCE_HOST, s);
        return s;
    }

    public static void setConfigSourceHost(String source_host) {
        KZAccount.getSharedAccount().setSourceHost(source_host);
    }

    public static String getSSO_URL() {
        return KZAccount.getSharedAccount().getKZLandingData1().ss_saml.url;
    }

    public static boolean checkIfNeedCheckDriverLicense() {
        return KZAccount.getSharedAccount().getCompany().enable_first_profile;
    }

    public static boolean checkIfNeedCheckPaymentMethod(){
        return KZAccount.getSharedAccount().getCompany().enable_first_payment;
    }

    public static boolean isZendesk() {
        return KZAccount.getSharedAccount().company.enable_zendesk;
    }

    public static boolean checkIfNeedTermOfService() {
        if(KZAccount.getSharedAccount().getKZLandingData1()!=null) {
            return KZAccount.getSharedAccount().getKZLandingData1().enable_agreements_register;
        }
        return KZAccount.getSharedAccount().getCompany().enable_agreements_register;
    }

    /*public static void checkIfTermOfServiceIsEmpty(final CustomListener3 customListener){
        VolleyUtils.getSharedNetwork().loadAgreementInfo(new OnResponseModel() {

            @Override
            public void onResponseSuccess(Object model) {

                JSONObject object = (JSONObject) model;
                try {
                    JSONObject obj = new JSONObject(object.toString());
                    String terms_service = obj.getString("terms_service");
                    String privacy_policy = obj.getString("privacy_policy");

                    if(customListener!=null) {
                        if(terms_service==null || terms_service.isEmpty()) {
                            customListener.returnFalse();
                            return;
                        }
                        if(privacy_policy==null || privacy_policy.isEmpty()) {
                            customListener.returnFalse();
                            return;
                        }
                        customListener.returnTrue();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    if(customListener!=null) customListener.returnFalse();
                }

            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                if(customListener!=null) customListener.returnFalse();
            }
        });
    }*/

    public static boolean checkIfShowEstimateCost(){
        return KZAccount.getSharedAccount().getCompany().enable_estimate_cost_list;
    }

    public static boolean checkIfShowBookingCost(String trip_type) {
        boolean result;
        switch (trip_type){
            case ConstantsUtils.TRIP_TYPE_BUSINESS:
                result = KZAccount.getSharedAccount().company.enable_charge_business &&
                        KZAccount.getSharedAccount().company.booking_business_charge_to.equals("driver");
                return result ;
            case ConstantsUtils.TRIP_TYPE_PRIVATE:
                result = KZAccount.getSharedAccount().company.enable_charge_private &&
                        KZAccount.getSharedAccount().company.booking_private_charge_to.equals("driver");
                return result ;
        }
        return false;
    }

    public static int defineShowBookingCostType(String trip_type) {
        boolean result;
        switch (trip_type){
            case ConstantsUtils.TRIP_TYPE_BUSINESS:
                result = KZAccount.getSharedAccount().company.enable_charge_business &&
                        KZAccount.getSharedAccount().company.booking_business_charge_to.equals("driver");
                return result ? ConstantsUtils.SHOW_COST_BUSINESS : ConstantsUtils.SHOW_COST_NO_SHOW_COST;
            case ConstantsUtils.TRIP_TYPE_PRIVATE:
                result = KZAccount.getSharedAccount().company.enable_charge_private &&
                        KZAccount.getSharedAccount().company.booking_private_charge_to.equals("driver");
                return result ? ConstantsUtils.SHOW_COST_PRIVATE : ConstantsUtils.SHOW_COST_NO_SHOW_COST;
        }
        return ConstantsUtils.SHOW_COST_NO_SHOW_COST;
    }

    public static boolean checkIfEnableUsagePolicy(){
        LogUtils.log(ConstantsUtils.TAG_POLICY, "enable_usage_policy: "+KZAccount.getSharedAccount().getCompany().enable_usage_policy);
        return /*true*/ KZAccount.getSharedAccount().getCompany().enable_usage_policy;
    }

    public static boolean checkIfEnableCheckPrivateUsagePolicy(){
        boolean rs = /*true*/!KZAccount.getSharedAccount().getUser().checked_private_usage_policy;
        LogUtils.log(ConstantsUtils.TAG_POLICY, "checked_private_usage_policy: "+KZAccount.getSharedAccount().getUser().checked_private_usage_policy);
        return rs;
    }

    public static boolean checkIfEnableCheckBusinessUsagePolicy(){
        boolean rs = /*true*/!KZAccount.getSharedAccount().getUser().checked_business_usage_policy;
        LogUtils.log(ConstantsUtils.TAG_POLICY, "checked_business_usage_policy: "+KZAccount.getSharedAccount().getUser().checked_business_usage_policy);
        return rs;
    }

    public static void setUsagePolicy(String trip_type){
        if(checkIfShowOneTime()){
            switch (trip_type){
                case ConstantsUtils.TRIP_TYPE_BUSINESS:
                    KZAccount.getSharedAccount().getUser().checked_business_usage_policy = false;
                    break;
                case ConstantsUtils.TRIP_TYPE_PRIVATE:
                    KZAccount.getSharedAccount().getUser().checked_private_usage_policy = false ;
                    break;
            }
        }
    }

    public static void checkIfBusinessPolicyIsEmpty(Context context, final CustomListener4 customListener, int companyID){
        VolleyUtils.getSharedNetwork().loadPolicyInfo(context, new OnResponseModel() {

            @Override
            public void onResponseSuccess(Object model) {

                JSONObject object = (JSONObject) model;
                try {
                    JSONObject obj = new JSONObject(object.toString());
                    String business_usage_policy = obj.getString("business_usage_policy");
                    LogUtils.log(ConstantsUtils.TAG_POLICY, "business_usage_policy: "+business_usage_policy);
                    if(customListener!=null) {
                        if(business_usage_policy==null || business_usage_policy.isEmpty()) {
                            customListener.returnFalse();
                            return;
                        }
                        customListener.returnTrue(business_usage_policy);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    if(customListener!=null) customListener.returnFalse();
                }

            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                if(customListener!=null) customListener.returnFalse();
            }
        }, companyID);
    }

    public static void checkIfPrivatePolicyIsEmpty(Context context, final CustomListener4 customListener, int companyID){
        VolleyUtils.getSharedNetwork().loadPolicyInfo(context,
                new OnResponseModel() {

            @Override
            public void onResponseSuccess(Object model) {

                JSONObject object = (JSONObject) model;
                try {
                    JSONObject obj = new JSONObject(object.toString());
                    String private_usage_policy = obj.getString("private_usage_policy");
                    LogUtils.log(ConstantsUtils.TAG_POLICY, "private_usage_policy: "+private_usage_policy);
                    if(customListener!=null) {
                        if(private_usage_policy==null || private_usage_policy.isEmpty()) {
                            customListener.returnFalse();
                            return;
                        }
                        customListener.returnTrue(private_usage_policy);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    if(customListener!=null) customListener.returnFalse();
                }

            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                if(customListener!=null) customListener.returnFalse();
            }
        }, companyID);
    }

    public static boolean checkIfShowOneTime(){
        LogUtils.log(ConstantsUtils.TAG_POLICY, "enable_usage_policy_onetime: "+KZAccount.getSharedAccount().getCompany().enable_usage_policy_onetime);
        boolean rs = KZAccount.getSharedAccount().getCompany().enable_usage_policy_onetime;
        return rs;
    }

    public static CustomModel checkIfShowBookingCostWithBookingStatus(KZBooking booking) {
        switch (booking.state){
            case BKGlobals.BOOKING_STATE_COMPLETE:
            case BKGlobals.BOOKING_STATE_INSPECTION:
                if(ConfigUtil.checkIfShowBookingCost(booking.trip_type)){
                    return new CustomModel(true, "Cost");
                }else {
                    return new CustomModel(false, "");
                }

            case BKGlobals.BOOKING_STATE_REQUEST:
            case BKGlobals.BOOKING_STATE_PENDING:
            case BKGlobals.BOOKING_STATE_APPROVED:
            case BKGlobals.BOOKING_STATE_INUSE:
                if(ConfigUtil.checkIfShowBookingCost(booking.trip_type)){
                    return new CustomModel(true, "Estimated cost");
                }else {
                    return new CustomModel(false,"");
                }

            case BKGlobals.BOOKING_STATE_CANCELLED:
            case BKGlobals.BOOKING_STATE_EXPIRED:

                return new CustomModel(false,"");

        }
        return new CustomModel(true, "Cost");
    }

    public static String getTimeFormat(){
        String df = KZAccount.getSharedAccount().getCompany().date_time_format.android.time;
        String rs = !df.isEmpty()? df :ConstantsUtils.TIME_FORMAT_1;
        return rs;
    }

    public static String getDateFormat(){
        String df = KZAccount.getSharedAccount().getCompany().date_time_format.android.date;
        String rs = !df.isEmpty()? df :ConstantsUtils.FORMAT_DATE;
        return rs;
    }

    public static String getDateFormatWithDateOfWeekInfo() {
        String df = KZAccount.getSharedAccount().getCompany().date_time_format.android.booking_list_group;
        if(df==null) return ConstantsUtils.FORMAT_DATE;
        String rs = !df.isEmpty()? df :ConstantsUtils.FORMAT_DATE;
        return rs;
    }

    public static String getDateTimeFormat(){
        String df = KZAccount.getSharedAccount().getCompany().date_time_format.android.date_time;
        String rs = !df.isEmpty()? df :ConstantsUtils.FORMAT_DATE_TIME;
        return rs;
    }

    public static boolean checkIfShowCarRegistration(KZBooking booking) {
        boolean allocated = booking.allocated;
        boolean timeOk = true;

        long start = booking.start - DateUtils.getTimeZoneOffset() - booking.start_timezone_offset;
        long acStart = booking.actual_start - DateUtils.getTimeZoneOffset() - booking.start_timezone_offset;
        long end = booking.end - DateUtils.getTimeZoneOffset() - booking.end_timezone_offset;
        long now = DateUtils.getCalendarWithTimeZoneFrom(Calendar.getInstance(), DateUtils.getTimeZoneOffset(), booking.end_timezone_offset).getTimeInMillis() / 1000;

        Log.d(ConstantsUtils.TAG_TIME, "Start:        "+DateUtils.formatDatePattern(start*1000, ConfigUtil.getDateTimeFormat()));
        Log.d(ConstantsUtils.TAG_TIME, "Actual start: "+DateUtils.formatDatePattern(acStart*1000, ConfigUtil.getDateTimeFormat()));
        Log.d(ConstantsUtils.TAG_TIME, "End:          "+DateUtils.formatDatePattern(end*1000, ConfigUtil.getDateTimeFormat()));
        Log.d(ConstantsUtils.TAG_TIME, "Now:          "+DateUtils.formatDatePattern(now*1000, ConfigUtil.getDateTimeFormat()));

        if(ConfigUtil.getConfigBookingCheckout().equals("any")){
            timeOk = true;
        }
        if(ConfigUtil.getConfigBookingCheckout().equals("before")){
            timeOk =  now >= start - ConfigUtil.getConfigStartBookingEarlyTimeInSec();
        }
        if(ConfigUtil.getConfigBookingCheckout().equals("after")){
            timeOk =  now >= start;
        }
        return allocated && timeOk;
    }

    public static SHOW_REGISTRATION checkIfShowCarRegistration0(KZBooking booking) {
        switch (booking.state){
            case BKGlobals.BOOKING_STATE_APPROVED:
                return SHOW_REGISTRATION.CHECK;
            case BKGlobals.BOOKING_STATE_INUSE:
            case BKGlobals.BOOKING_STATE_COMPLETE:
                return SHOW_REGISTRATION.SHOW_REAL_CAR;
            case BKGlobals.BOOKING_STATE_INSPECTION:
            case BKGlobals.BOOKING_STATE_REQUEST:
            case BKGlobals.BOOKING_STATE_PENDING:
            case BKGlobals.BOOKING_STATE_CANCELLED:
            case BKGlobals.BOOKING_STATE_EXPIRED:
                return SHOW_REGISTRATION.SHOW_VIRTUAL_CAR;

        }
        return SHOW_REGISTRATION.SHOW_VIRTUAL_CAR;
    }

    public static boolean checkIfEnableMembershipIsNotDiscount() {
        boolean rs = KZAccount.getSharedAccount().company.enable_membership_is_not_discount;
        return rs;
    }

    public static ArrayList<BlockModal> getConfigBlock() {
        ArrayList<BlockModal> s = KZAccount.getSharedAccount().company.company_booking_block_mobile_app_parsed;
        return s;
    }

    public static boolean checkIfEnableDropOff() {
        return KZAccount.getSharedAccount().company.enable_drop_off;
    }

    public static boolean checkIfEnableGeofenceDistance() {
        return "true".equals(KZAccount.getSharedAccount().company.enable_geofence_distance);
    }

    public static boolean checkIfEnableOverrideGPS() {
        return ("true").equals(KZAccount.getSharedAccount().company.enable_override_gps);
    }

    public static class CustomModel{
        public boolean showCost;
        String tittle;

        CustomModel(boolean b, String cost) {
            showCost = b;
            tittle = cost;
        }
    }

    public static boolean checkIfEnableMembership(){
        boolean s = KZAccount.getSharedAccount().company.enable_membership;
        return s;
    }

    public static boolean checkIfEnablePromotion(){
        boolean s = KZAccount.getSharedAccount().company.enable_promotion;
        return s;
    }

    public static String actionDefineTripType() {
        return KZAccount.getSharedAccount().user.is_client_user ? ConstantsUtils.TRIP_TYPE_BUSINESS : ConstantsUtils.TRIP_TYPE_PRIVATE;
    }

    public static boolean checkIfShowConciergeOption(){
        boolean a = KZAccount.getSharedAccount().company.enable_concierge;
        boolean b = "additional_fee".equals(KZAccount.getSharedAccount().company.concierge_option);
        boolean rs = a && b;
        return rs;
    }
}
