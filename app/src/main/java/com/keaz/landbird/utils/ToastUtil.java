package com.keaz.landbird.utils;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by anhtran1810 on 3/2/18.
 */

public class ToastUtil {
    public static void showToastMessage(Context context, String message, boolean isLong) {
        Toast.makeText(context, message, isLong?Toast.LENGTH_LONG:Toast.LENGTH_SHORT).show();
    }
}
