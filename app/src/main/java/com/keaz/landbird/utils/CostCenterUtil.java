package com.keaz.landbird.utils;

import android.content.Context;
import android.support.v7.widget.PopupMenu;
import android.view.Menu;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;
import com.keaz.landbird.R;
import com.keaz.landbird.interfaces.CustomListener5;
import com.keaz.landbird.models.KZCostCentre;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anhtran1810 on 4/2/18.
 */

public class CostCenterUtil {

    public static String getCostCenterTittle(Object ccs, List<KZCostCentre> costCentres) {
        ArrayList<String> list = new ArrayList<>();
        try {
            list = ((ArrayList<String>) ccs);
        }catch (Exception e){
            return "";
        }
        if(list.isEmpty()) return "";
        String s = list.get(0);
        for (KZCostCentre o: costCentres){
            if(s.equals(o.id+"")) return o.name;
        }
        return null;
    }

    private static ArrayList<Integer> parseCostCenters(Object ccs) {
        try {
            ArrayList<String> list = ((ArrayList<String>) ccs);
            ArrayList<Integer> rs = new ArrayList<>();
            for (String s : list) {
                rs.add(Integer.parseInt(s));
            }
            return rs;
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    public static JSONArray getJsonArray(Object ccs) {
        /*ArrayList<Integer> l = CostCenterUtil.parseCostCenters(ccs);
        JSONArray jsonArray = new JSONArray();
        for (Integer i: l) {
            jsonArray.put(i);
        }
        return jsonArray;*/

        return new JSONArray();
    }

    public static void iniCostCenterInfo(Context context, final CustomListener5 customListener4) {
        if(customListener4!=null) customListener4.actionBeforeCallApi();

        VolleyUtils.getSharedNetwork().loadCostCenter(context, new OnResponseModel() {
            @Override
            public void onResponseSuccess(Object model) {
                JsonArray jsonObject = (JsonArray) model;
                TypeToken<List<KZCostCentre>> token = new TypeToken<List<KZCostCentre>>(){};
                GsonBuilder gsonBuilder = new GsonBuilder();
                Gson gson = gsonBuilder.create();
                List<KZCostCentre> costCentres = gson.fromJson(jsonObject.toString(), token.getType());
                if(customListener4!=null) customListener4.actionCallApiSuccess(costCentres);
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                if(customListener4!=null) customListener4.actionCallApiFail(error);
            }
        });
    }

    public static void showPopUpCostCenterMenu(Context context, final TextView tvCostCenter, final List<KZCostCentre> costCentres, PopupMenu.OnMenuItemClickListener listener) {
        PopupMenu popup = new PopupMenu(context, tvCostCenter);
        popup.getMenuInflater().inflate(R.menu.empty_menu, popup.getMenu());


        Menu menu = popup.getMenu();
        for (int i = 0; i < costCentres.size(); i++) {
            KZCostCentre str = costCentres.get(i);
            menu.add(0, i, i, str.name);
        }

        popup.setOnMenuItemClickListener(listener);

        popup.show();
    }
}
