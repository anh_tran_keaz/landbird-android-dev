package com.keaz.landbird.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.text.Html;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.keaz.landbird.R;
import com.keaz.landbird.interfaces.CustomListener;
import com.keaz.landbird.interfaces.ListSelectListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by mac on 3/6/17.
 */

public class DialogUtils {

    public static void showCustomDialog(Context context,boolean cancelable, String tittle, String message, String btn1, String btn2, final View.OnClickListener listener1, final View.OnClickListener listener2) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_custom_2_option);
        dialog.setCancelable(cancelable);
        WindowManager.LayoutParams layoutParam = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        layoutParam.copyFrom(window.getAttributes());
        layoutParam.width = WindowManager.LayoutParams.MATCH_PARENT;
        layoutParam.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setAttributes(layoutParam);

        ((TextView)dialog.findViewById(R.id.tvTittle)).setText(tittle);
        ((TextView)dialog.findViewById(R.id.tvMessage)).setText(message);
        ((TextView)dialog.findViewById(R.id.btn1)).setText(btn1);
        ((TextView)dialog.findViewById(R.id.btn2)).setText(btn2);

        dialog.findViewById(R.id.btn1).setVisibility(listener1!=null ? View.VISIBLE : View.GONE);
        dialog.findViewById(R.id.btn2).setVisibility(listener2!=null ? View.VISIBLE : View.GONE);

        dialog.findViewById(R.id.btn1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener1 != null) listener1.onClick(dialog.findViewById(R.id.btn1));
                dialog.cancel();
            }
        });
        dialog.findViewById(R.id.btn2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener2 != null) listener2.onClick(dialog.findViewById(R.id.btn2));
                dialog.cancel();
            }
        });

        dialog.show();
    }

    public static void showCustomDialogLinkable(Context context,boolean cancelable, String tittle, String message, String btn1, String btn2, final View.OnClickListener listener1, final View.OnClickListener listener2) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_custom_2_option);
        dialog.setCancelable(cancelable);
        WindowManager.LayoutParams layoutParam = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        layoutParam.copyFrom(window.getAttributes());
        layoutParam.width = WindowManager.LayoutParams.MATCH_PARENT;
        layoutParam.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setAttributes(layoutParam);

        ((TextView)dialog.findViewById(R.id.tvTittle)).setText(tittle);
        ((TextView)dialog.findViewById(R.id.tvMessage)).setText(message);

        Spanned policy = Html.fromHtml(message);
        ((TextView)dialog.findViewById(R.id.tvMessage)).setText(policy);
        ((TextView)dialog.findViewById(R.id.tvMessage)).setMovementMethod(LinkMovementMethod.getInstance());

        ((TextView)dialog.findViewById(R.id.btn1)).setText(btn1);
        ((TextView)dialog.findViewById(R.id.btn2)).setText(btn2);

        dialog.findViewById(R.id.btn1).setVisibility(listener1!=null ? View.VISIBLE : View.GONE);
        dialog.findViewById(R.id.btn2).setVisibility(listener2!=null ? View.VISIBLE : View.GONE);

        dialog.findViewById(R.id.btn1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener1 != null) listener1.onClick(dialog.findViewById(R.id.btn1));
                dialog.cancel();
            }
        });
        dialog.findViewById(R.id.btn2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener2 != null) listener2.onClick(dialog.findViewById(R.id.btn2));
                dialog.cancel();
            }
        });

        dialog.show();
    }

    public static void showMessageDialog(Context context, boolean cancelable , String tittle, String message, String button1Tittle, String button2Tittle, DialogInterface.OnClickListener clickOk, DialogInterface.OnClickListener clickCancel){
        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setCancelable(cancelable);
        //dialog.setIcon(android.R.drawable.ic_dialog_alert);
        if(tittle!=null)dialog.setTitle(tittle);
        if(message!=null)dialog.setMessage(message);
        if(clickOk!=null)dialog.setPositiveButton(button1Tittle, clickOk);
        if(clickCancel!=null)dialog.setNegativeButton(button2Tittle, clickCancel);

        dialog.create().show();
    }

    public static void showMessageDialog3Options(Context context, boolean cancelable ,int iconID, String tittle, String message, String button1Tittle, String button2Tittle, String button3Tittle, DialogInterface.OnClickListener clickBtn1, DialogInterface.OnClickListener clickBtn2, DialogInterface.OnClickListener clickBtn3){
        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setCancelable(cancelable);
        if(iconID!=0) dialog.setIcon(iconID);
        if(tittle!=null)dialog.setTitle(tittle);
        if(message!=null)dialog.setMessage(message);
        if(clickBtn1!=null)dialog.setPositiveButton(button1Tittle, clickBtn1);
        if(clickBtn2!=null)dialog.setNegativeButton(button2Tittle, clickBtn2);
        if(clickBtn3!=null)dialog.setNeutralButton(button3Tittle, clickBtn3);
        dialog.create().show();
    }

    public static void showCustomContextMenu(Context context, String option1, String option2, String option3, final CustomListener customListener1, final CustomListener customListener2, final CustomListener customListener3) {
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.custom_dialog_n_option, null);

        view.findViewById(R.id.ll_option1).setVisibility(View.GONE);
        view.findViewById(R.id.ll_option2).setVisibility(View.GONE);
        view.findViewById(R.id.ll_option3).setVisibility(View.GONE);

        if(option1!=null) view.findViewById(R.id.ll_option1).setVisibility(View.VISIBLE);
        if(option2!=null) view.findViewById(R.id.ll_option2).setVisibility(View.VISIBLE);
        if(option3!=null) view.findViewById(R.id.ll_option3).setVisibility(View.VISIBLE);

        ((TextView) view.findViewById(R.id.tv1)).setText(option1);
        ((TextView) view.findViewById(R.id.tv2)).setText(option2);
        ((TextView) view.findViewById(R.id.tv3)).setText(option3);

        alertBuilder.setView(view).setView(view);
        final AlertDialog dialog = alertBuilder.create();
        if(option1!=null) view.findViewById(R.id.ll_option1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customListener1.onClick();
                dialog.cancel();
            }
        });
        if(option2!=null) view.findViewById(R.id.ll_option2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customListener2.onClick();
                dialog.cancel();
            }
        });
        if(option3!=null) view.findViewById(R.id.ll_option3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customListener3.onClick();
                dialog.cancel();
            }
        });


        dialog.show();
    }

    public static class SimpleListAdapter extends ArrayAdapter<String> {
        private int mSelection;
        private Activity mContext;
        ArrayList<String> mAdapterData;
        List<RadioButton> mRadioButtonList = new ArrayList<>();
        List<TextView> mTextViewList = new ArrayList<>();
        List<View> mViewList = new ArrayList<>();

        public SimpleListAdapter(Activity context, ArrayList<String> data) {
            super(context, 0, data);
            this.mAdapterData = data;
            mContext = context;
        }

        public SimpleListAdapter(Activity context, ArrayList<String> data, int defaultPosition) {
            super(context,0,data);
            mAdapterData = data;
            mSelection = defaultPosition;
            mContext = context;
        }

        @Override
        public int getCount() {
            return mAdapterData.size();
        }
        @NonNull
        @Override
        public View getView(final int position, View convertView, @NonNull ViewGroup parent) {
            //Log.d(TAG, "getView()");

            if(convertView == null){
                convertView = mContext.getLayoutInflater().inflate(R.layout.custom_listview_item_03_choose_collection, null);
            }
            final LinearLayout ll = (LinearLayout) convertView.findViewById(R.id.custom_listview_item_03_ll);
            final TextView textView = (TextView) convertView.findViewById(R.id.custom_listviewitem_textview_collection);
            final RadioButton radioButton = (RadioButton) convertView.findViewById(R.id.custom_listviewitem_radiobutton);

            radioButton.setChecked(false);

            mViewList.add(convertView);
            mRadioButtonList.add(radioButton);
            mTextViewList.add(textView);

            final String s = mAdapterData.get(position);
            textView.setText(s);

            ll.setBackgroundColor(Color.WHITE);
            radioButton.setChecked(false);

            if(mSelection !=-1){
                if(position== mSelection) {
                    radioButton.setChecked(true);
                }
            }
            return convertView;
        }

        void setSelection(int position){
            mSelection = position;
            notifyDataSetChanged();
        }

        int getSelection() {
            return mSelection;
        }

        String getSelectionResult() {
            return mAdapterData.get(mSelection);
        }
    }

    public static void showCustomListDialog(Activity context, boolean isCancelable, String tittle, String[] data, int defaultPosition, final ListSelectListener listSelectListener){
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
        View view = context.getLayoutInflater().inflate(R.layout.custom_dialog_2, null);
        ListView listView = (ListView) view.findViewById(R.id.custom_dialog_2_listview);
        TextView textView_tittle = (TextView) view.findViewById(R.id.custom_dialog_2_textview_tittle);

        textView_tittle.setText(tittle);
        ArrayList<String> adapter_data = new ArrayList<>();
        Collections.addAll(adapter_data, data);

        final SimpleListAdapter adapter = new SimpleListAdapter(context, adapter_data, defaultPosition);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                adapter.setSelection(position);
            }
        });

        alertBuilder.setView(view)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(listSelectListener!=null) listSelectListener.action(dialog, adapter.getSelectionResult(), adapter.getSelection());
                    }
                });
        alertBuilder.setCancelable(isCancelable);
        AlertDialog dialog = alertBuilder.create();
        dialog.show();
    }

}
