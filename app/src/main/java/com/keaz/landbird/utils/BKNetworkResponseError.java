package com.keaz.landbird.utils;

import com.android.volley.VolleyError;
import com.google.gson.annotations.SerializedName;

import org.json.JSONObject;

/**
 * Created by Harradine on 9/18/2014.
 */
public class BKNetworkResponseError extends VolleyError {
//    @SerializedName("title")
//    public String title;

    @SerializedName("description")
    public String description;

    @SerializedName("status")
    public String status;

    @SerializedName("code")
    public String code;

    @SerializedName("data")
    public JSONObject data;

}
