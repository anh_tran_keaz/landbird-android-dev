package com.keaz.landbird.utils;

import com.keaz.landbird.models.KZBranch;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by Administrator on 16/2/2017.
 */

public class DateUtils {

    public static final String FORMAT_DATE_2 = "dd/MM/yyyy";

    public static Date beginOfDay(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        return cal.getTime();
    }

    public static Date endOfDay(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        cal.set(Calendar.MILLISECOND, 999);

        return cal.getTime();
    }
    /**
     * Mr Sam add a 'minus' for some reasons
     *
     * */
    public static int getTimeZoneOffset1() {
        TimeZone tz = TimeZone.getDefault();
        Date now = new Date();
        int offsetFromUtc = tz.getOffset(now.getTime()) / 1000;
        return -offsetFromUtc;
    }
    /**
     * Correct timezone offset
     *
     * */
    public static int getTimeZoneOffset() {
        TimeZone tz = TimeZone.getDefault();
        Date now = new Date();
        int offsetFromUtc = tz.getOffset(now.getTime()) / 1000;
        return offsetFromUtc;
    }

    public static int getTimeZoneOffset1(long timeStamp){
        TimeZone tz = TimeZone.getDefault();
        int offsetFromUtc = tz.getOffset(timeStamp) / 1000;
        return -offsetFromUtc;
    }

    public static int getTimeZoneOffset2() {
        Calendar mCalendar = new GregorianCalendar();
        TimeZone timeZone = mCalendar.getTimeZone();
        return timeZone.getRawOffset()/1000;
    }

    public static Date getToday() {
        Calendar calendar = Calendar.getInstance();
        return calendar.getTime();
    }

    public static Date getTodayWithBranchTimezone(long branchOffsetInSecond) {
        Calendar calendar = Calendar.getInstance();
        Date date1 = calendar.getTime();

        long timeZoneOffset = DateUtils.getTimeZoneOffset();
        calendar.setTimeInMillis(calendar.getTimeInMillis() - timeZoneOffset*1000 -  branchOffsetInSecond*1000);
        Date date2 = calendar.getTime();
        return date2;
    }

    public static Date getDateFromCalendar(Calendar calendar,long timeZoneOffset , long branchOffsetInSecond) {
        calendar.setTimeInMillis(calendar.getTimeInMillis() - timeZoneOffset*1000 -  branchOffsetInSecond*1000);
        Date date2 = calendar.getTime();
        return date2;
    }

    public static Calendar getCalendarWithTimeZoneFrom(Calendar calendar,long timeZoneOffset , long branchOffsetInSecond) {
        calendar.setTimeInMillis(calendar.getTimeInMillis() - timeZoneOffset*1000 -  branchOffsetInSecond*1000);
        return calendar;
    }

    private static Date getYesterday() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -1);
        return calendar.getTime();
    }

    public static Date getTomorrow() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, 1);
        return calendar.getTime();
    }

    public static Date getTomorrowWithBranchTimezone(long branchOffsetInSecond) {
        Calendar calendar = Calendar.getInstance();
        Date date1 = calendar.getTime();

        int timezoneOffset = DateUtils.getTimeZoneOffset();
        calendar.setTimeInMillis(calendar.getTimeInMillis() - timezoneOffset*1000 -  branchOffsetInSecond*1000);
        calendar.add(Calendar.DATE, 1);
        Date date2 = calendar.getTime();
        return date2;
    }

    public static Date getStartToday() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MINUTE, 0);
        return calendar.getTime();
    }

    public static Date getDayFromToday(int count) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MINUTE, 0);
        calendar.add(Calendar.DATE, count);
        return calendar.getTime();
    }

    public static String formatDatePattern(Date date, String pattern) {
        return new SimpleDateFormat(pattern).format(date);
    }

    public static String formatDatePattern(long timeStamp, String pattern) {
        Date date = new Date();
        date.setTime(timeStamp);
        return new SimpleDateFormat(pattern, Locale.getDefault()).format(date);
    }

    public static Date getDate(long sec) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(sec * 1000);
        return calendar.getTime();
    }

    public static Date getDateWithMili(long mili) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(mili);
        return calendar.getTime();
    }

    public static String getTimeFromSec(long sec) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(sec * 1000);
        Date date = calendar.getTime();
        return new SimpleDateFormat(ConstantsUtils.DATE_TIME_FORMAT).format(date);
    }

    public static String getDurationFormatText1(long start, long end) {
        /*long offset = end - start;
        int diffHours = (int) (offset / (60 * 60));
        int diffMinutes = (int) ((offset % (60 * 60)) / 60);

        String result = "";
        if (diffHours > 0) {
            String hour = diffHours > 1 ? "hours" : "hour";
            result = diffHours + " " + hour;
        }
        if (diffMinutes > 0) {
            String min = diffMinutes > 1 ? "minutes" : "minute";
            result += " " + diffMinutes + " " + min;
        }

        if (diffHours <= 0 && diffMinutes <= 0)
            result = "1 minute";*/

        int durationInMins = (int) ((end-start)/60);
        String result = FormatTimeUtils.formatDurationInMillis1(durationInMins*60*1000);
        return result;
    }

    public static String getDurationFormatText1(int durationInMins) {
        /*long offset = durationInMins*60;
        int diffHours = (int) (offset / (60 * 60));
        int diffMinutes = (int) ((offset % (60 * 60)) / 60);

        String result = "";
        if (diffHours > 0) {
            String hour = diffHours > 1 ? "hours" : "hour";
            result = diffHours + " " + hour;
        }
        if (diffMinutes > 0) {
            String min = diffMinutes > 1 ? "minutes" : "minute";
            result += " " + diffMinutes + " " + min;
        }

        if (diffHours <= 0 && diffMinutes <= 0)
            result = "0 minute";*/
        String result = FormatTimeUtils.formatDurationInMillis1(durationInMins*60*1000);
        return result;
    }

    public static String getDurationFormatText2(int durationInMins) {
        String result = FormatTimeUtils.formatDurationInMillis2(durationInMins*60*1000);
        return result;
    }

    public static String getDurationFormatText3(int durationInMins) {
        String result = FormatTimeUtils.formatDurationInMillis3(durationInMins*60*1000);
        return result;
    }

    public static Date getDateFromString(String pattern, String date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
        try {
            return dateFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String compareTwoDate(Date date) {
        Date today = getToday();
        String str1 = new SimpleDateFormat("yyyy-MM-dd").format(date);
        String str2 = new SimpleDateFormat("yyyy-MM-dd").format(today);
        if (str1.equalsIgnoreCase(str2)) {
            return "Today";
        }
        Date tomorrow = getTomorrow();
        if (date.after(today)) {
            str1 = new SimpleDateFormat("yyyy-MM-dd").format(date);
            str2 = new SimpleDateFormat("yyyy-MM-dd").format(tomorrow);
            if (str1.equalsIgnoreCase(str2)) {
                return "Tomorrow";
            } else {
                return str1;
            }
        } else {
            return "";
        }
    }

    public static String compareTwoDate(boolean allowThePastIsGroupedAsToday, Date date) {

        String df = ConstantsUtils.FORMAT_DATE;
        Date today = getToday();

        String str1 = new SimpleDateFormat(df, Locale.getDefault()).format(date);
        String str2 = new SimpleDateFormat(df, Locale.getDefault()).format(today);
        if (str1.equalsIgnoreCase(str2)) {
            return "Today";
        }
        if(date.before(today) && allowThePastIsGroupedAsToday) return "Today";

        Date tomorrow = getTomorrow();
        if (date.after(today)) {
            str1 = new SimpleDateFormat(df,Locale.getDefault()).format(date);
            str2 = new SimpleDateFormat(df,Locale.getDefault()).format(tomorrow);
            if (str1.equalsIgnoreCase(str2)) {
                return "Tomorrow";
            } else {
                return str1;
            }
        }

        Date yesterday = getYesterday();
        if (date.before(today)) {
            str1 = new SimpleDateFormat(df,Locale.getDefault()).format(date);
            str2 = new SimpleDateFormat(df,Locale.getDefault()).format(yesterday);
            if (str1.equalsIgnoreCase(str2)) {
                return "Yesterday";
            } else {
                return str1;
            }
        } else {
            return "";
        }
    }

    public static long getTimeNow() {
        return Calendar.getInstance().getTimeInMillis() / 1000;
    }

    public static Date getTimeNowOfABranch(KZBranch branch) {
        long branchTimeZoneOffset = Long.parseLong(branch.time_zone_offset);
        long timeInMillis = Calendar.getInstance().getTimeInMillis() - DateUtils.getTimeZoneOffset()*1000 - branchTimeZoneOffset*1000;
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timeInMillis);
        return calendar.getTime();
    }


}
