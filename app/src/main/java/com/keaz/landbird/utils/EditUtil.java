package com.keaz.landbird.utils;

import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by anhtran1810 on 4/2/18.
 */

public class EditUtil {
    public static void setTextOrHint(EditText editText, String text, String hint) {
        if(text!=null && text.length() > 0){
            editText.setText(text);
        }else {
            editText.setHint(hint);
        }
    }

    public static void setTextOrDefaultText(TextView editText, String text, String defaultText) {
        if(text!=null && text.length() > 0){
            editText.setText(text);
        }else {
            editText.setText(defaultText);
        }
    }
}
