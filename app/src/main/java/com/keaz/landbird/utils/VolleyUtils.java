package com.keaz.landbird.utils;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.keaz.landbird.BuildConfig;
import com.keaz.landbird.R;
import com.keaz.landbird.activities.KZRegistrationResponse;
import com.keaz.landbird.activities.LoginActivity;
import com.keaz.landbird.models.KZAccount;
import com.keaz.landbird.models.KZBooking;
import com.keaz.landbird.models.KZBookingsList;
import com.keaz.landbird.models.KZBranch;
import com.keaz.landbird.models.KZCompany;
import com.keaz.landbird.models.KZConfigData;
import com.keaz.landbird.models.KZLandingData;
import com.keaz.landbird.models.KZLoginResponse;
import com.keaz.landbird.models.KZNotes;
import com.keaz.landbird.models.KZPaymentDetail;
import com.keaz.landbird.models.KZPaymentMethodsDetail;
import com.keaz.landbird.models.KZSearchResult;
import com.keaz.landbird.models.KZVerifyResponse;
import com.keaz.landbird.models.KzAgreement;
import com.keaz.landbird.models.KzPage;
import com.keaz.landbird.models.XModel;
import com.segment.analytics.Analytics;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

/*
 * Created by Anh Tran on 14/2/2017.
 * Từ prject này Hayk, ở đây chỉ thêm vào chứ ko bỏ bớt
 */

public class VolleyUtils {

    private static final String TAG = VolleyUtils.class.getSimpleName();

    private final static String API_LINK = BuildConfig.API_LINK;
    private final static String URI_LOGIN = API_LINK + "v1/login/";
    private final static String URI_REGISTER = API_LINK + "v1/user/";
    private final static String URI_ACTIVATE = API_LINK + "v1/activate/";
    private final static String URI_COMPANY_LANDING = API_LINK + "v1/";
    public final static String URI_UPLOAD_LICENSE_PICTURE = API_LINK + "v1/";
    public static final String URI_UPLOAD_NOTE_IMAGE = API_LINK + "v1/note/%s/assets";
    private static final String URI_BRANCHES = API_LINK + "v1/branches";
    private static final String URI_USER = API_LINK + "v1/user/";
    private static final String URI_VEHICLE = API_LINK + "v1/vehicle/";
    private static final String URI_BOOKING = API_LINK + "v1/booking";
    private static final String URI_UNLOCK = URI_BOOKING + "/%s/command/unlock";
    private static final String URI_LOCK = URI_BOOKING + "/%s/command/lock";
    private static final String URI_PAYMENT = API_LINK + "v1/payment/braintree";
    private static final String URI_CREATE_NOTE = API_LINK + "v1/booking/%s/notes";
    private final static String URI_RESET_PASSWORD = API_LINK + "v1/login/reset";
    private final static String URI_BRANCH = API_LINK + "v1/branch";
    private final static String URI_SEARCH_VEHICLE = API_LINK + "v1/branch/%s/vehicles_shortpoint/%s/%s/%s";
    private final static String URI_SEARCH_VEHICLE_2 = API_LINK + "v1/branch/%s/vehicles/%s/%s/%s/%s/list/%s/%s";
    private final static String URI_LOGOUT = API_LINK + "v1/logout";
    private final static String URI_CONTACT_US = API_LINK + "v1/helpdesk";
    private final static String URI_MEMBERSHIP_LIST = API_LINK + "v1/memberships";
    private final static String URI_MEMBERSHIP_APPLY = API_LINK + "v1/profile/membership";
    private final static String URI_MEMBERSHIP_DELETE = API_LINK + "v1/profile/membership/%s";
    private final static String URI_MEMBERSHIP_DETAIL = API_LINK + "v1/membership/%s";
    private final static String URI_REPORT_LOST_KEY_CARD = API_LINK + "v1/helpdesk";
    private final static String URI_REFER_A_FRIEND = API_LINK + "v1/media/refer";
    private static final String URI_ADD_A_GUEST = API_LINK + "v1/branch/%s/invite_user";
    private static final String URI_GET_VIDEOS = API_LINK + "v1//media/video_guide";
    private static final String URI_GET_SOCIAL_LINKS = API_LINK + "v1//media/social_link";
    private final static String URI_INVITE_USER = API_LINK + "v1/login/invite";
    private static final String URI_PAYMENT_COST_CENTER = API_LINK + "v1/cost-centre/braintree";
    private static final String URI_PAYMENT_COMPANY = API_LINK + "v1/company/braintree";
    private static final String URI_LOAD_COST_CENTER = API_LINK + "v1/cost-centres";
    private final static String URI_LOAD_POLICY = API_LINK + "v1/company/%s/policy";
    private final static String URI_LOAD_USER_FOR_MIXPANEL = API_LINK + "v1/mix-panel/user/";
    private static final String URI_CREATE_ERROR = API_LINK + "v1/errors";
    private static final String URI_PAGE = API_LINK + "v1/faq/";
    private final static String URI_LOGIN_CHECK = API_LINK + "v1/login/check/";
    private final static String URI_GET_VEHICLE = API_LINK + "v1/vehicle/%s/available/%s/%s/%s/%s/%s/%s/%s";
    private static final String URI_CHECK_GEO_FENCE_AREA_INSIDE = API_LINK + "v1/branch/find_by_location/%s/%s";
    private static final String URI_COMPANIES = API_LINK + "v1/company/sub_companies";
    private static final String URI_LOAD_DROP_OFF_BRANCHES = API_LINK + "v1/branch/drop_off_branches";
    private static final String URI_AGREEMENT = API_LINK + "v1/company/agreements";
    private static final String URI_PAYMENT_GATEWAY = API_LINK + "v1/profile/payment_gateway";
    private static final String URI_PAYMENT_GATEWAY_USER = API_LINK + "v1/profile/payment_gateway/";
    private static final String URI_CONCIERGE_SERVICE_COST = API_LINK + "v1/booking/concierge_services_cost/";
    private static final String URI_ADD_ON = API_LINK + "v1/addons/";
    private static final String HOST = BuildConfig.HOST;

    private static VolleyUtils sharedNetwork;
    private String subDomainHeader;
    /*
     * Load branch asset
     * */
    public void loadBranchAsset(Context context, String branchID, final OnResponseModel<JSONObject> onResponseModel){
        String url = URI_BRANCH  +"/"+ branchID+"/assets";

        CustomModelRequest<JsonObject> request = new CustomModelRequest<>(
                context,
                Request.Method.GET,
                url, JsonObject.class, genHeaders(), null, new OnResponseModel() {
            @Override
            public void onResponseSuccess(Object model) {
                JsonObject object = (JsonObject) model;
                try {
                    JSONObject obj = new JSONObject(object.toString());
                    onResponseModel.onResponseSuccess(obj);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                onResponseModel.onResponseError(error);
            }
        });
        CustomVolleyRequestQueue.getInstance().setRetryPolicy(request);
        CustomVolleyRequestQueue.getInstance().getRequestQueue().add(request);
    }

    public static String getUrlUploadLicenseImageFront(String userId) {
        return API_LINK + "v1/user/" + userId + "/assets/license";
    }

    public static String getUrlUploadLicenseImageBack(String userId) {
        return API_LINK + "v1/user/" + userId + "/assets/license_after";
    }

    public static String getUrlUploadLicenseImageFace(String userId) {
        return API_LINK + "v1/user/" + userId + "/assets/avatar";
    }

    public static VolleyUtils getSharedNetwork() {
        if (null == sharedNetwork) {
            sharedNetwork = new VolleyUtils();
        }
        return sharedNetwork;
    }

    private static Map<String, String> genHeadersWithoutAccessToken() {
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("X-Source-Host", HOST);
        headers.put("Content-Type", "application/json; charset=utf-8");
        headers.put("version", BuildConfig.VERSION_NAME);
        headers.put("AppName", "Landbird");
        headers.put("DeviceType", "ANDROID");

        return headers;
    }
    /*
     * Gen Header
     *
     * @return
     */
    private static Map<String, String> genHeaders() {
        Map<String, String> headers = new HashMap<>();
        headers.put("X-Source-Host", HOST);
        headers.put("Content-Type", "application/json; charset=utf-8");
        headers.put("Token", getAccessToken());
        headers.put("version", BuildConfig.VERSION_NAME);
        headers.put("AppName", "Landbird");
        headers.put("DeviceType", "ANDROID");

        return headers;
    }
    /*
     * Header for upload license images
     */
    public static Map<String, String> genHeaders3() {
        Map<String, String> headers = new HashMap<>();
        headers.put("X-Source-Host", HOST);
        headers.put("Content-Type", "image/png");
        headers.put("Token", getAccessToken());
        headers.put("version", BuildConfig.VERSION_NAME);
        headers.put("Content-Disposition", "attachment; filename=sang.png");
        headers.put("AppName", "Landbird");
        headers.put("DeviceType", "ANDROID");

        return headers;
    }
    /*
     * Gen Header not included token
     */
    private static Map<String, String> genHeaders2() {
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("X-Source-Host", HOST);
        headers.put("Content-Type", "application/json; charset=utf-8");
        headers.put("version", BuildConfig.VERSION_NAME);
        headers.put("AppName", "Landbird");
        headers.put("DeviceType", "ANDROID");

        return headers;
    }

    private static String getAccessToken() {
        return BKGlobals.getSharedGlobals().getStringPreferences(BKGlobals.ACCESS_TOKEN, null);
    }

    public void setActiveSlug(String slug) {
        subDomainHeader = slug + "." + BKGlobals.getMetadata("network-api", "");
        MyLog.debug("subDomainHeader", subDomainHeader);
    }
    /*
     * Append Access Token
     *
     * @param uri
     * @return
     */
    private String appendAccessToken(String uri) {
        return uri + "?token=" + BKGlobals.getSharedGlobals().getStringPreferences(BKGlobals.ACCESS_TOKEN, null);
    }
    /*
     * Login User
     *
     * @param email
     * @param pass
     * @param networkResponse
     */
    public void loginUserByEmail(Context context, String email, String pass, final OnResponseModel networkResponse) {
        String url = URI_LOGIN;
        final JSONObject json = new JSONObject();
        TimeZone tz = TimeZone.getDefault();
        Date now = new Date();
        int offsetTime = 0 - tz.getOffset(now.getTime()) / 1000;
        try {
            json.put("email", email);
            json.put("password", pass);
            json.put("timezone_offset", offsetTime);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        CustomModelRequest<KZLoginResponse> request = new CustomModelRequest<KZLoginResponse>(
                context,
                Request.Method.POST,
                url, KZLoginResponse.class,
                genHeaders(),
                null,
                new OnResponseModel() {
                    @Override
                    public void onResponseSuccess(Object model) {
                        networkResponse.onResponseSuccess(model);
                    }

                    @Override
                    public void onResponseError(BKNetworkResponseError error) {
                        networkResponse.onResponseError(error);
                    }
                }) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    byte[] bytes = json.toString().getBytes("utf-8");
                    return bytes;
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                return super.getBody();
            }
        };
        CustomVolleyRequestQueue.getInstance().setRetryPolicy(request);
        CustomVolleyRequestQueue.getInstance().getRequestQueue().add(request);
    }

    public void loginUserByPhone(Context context, String phone, String countryCode, String pass, final OnResponseModel networkResponse) {
        if(countryCode==null) countryCode="1";
        String url = URI_LOGIN;
        final JSONObject json = new JSONObject();
        TimeZone tz = TimeZone.getDefault();
        Date now = new Date();
        int offsetTime = 0 - tz.getOffset(now.getTime()) / 1000;
        String phoneEdited = phone.replaceFirst("^0+(?!$)", "");
        String phoneUp = countryCode + phoneEdited;
        try {
            json.put("phone", phoneUp);
            json.put("password", pass);
            json.put("timezone_offset", offsetTime);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        CustomModelRequest<KZLoginResponse> request = new CustomModelRequest<KZLoginResponse>(
                context,
                Request.Method.POST,
                url, KZLoginResponse.class,
                genHeaders(),
                null,
                new OnResponseModel() {
                    @Override
                    public void onResponseSuccess(Object model) {
                        networkResponse.onResponseSuccess(model);
                    }

                    @Override
                    public void onResponseError(BKNetworkResponseError error) {
                        networkResponse.onResponseError(error);
                    }
                }) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    byte[] bytes = json.toString().getBytes("utf-8");
                    return bytes;
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                return super.getBody();
            }
        };
        CustomVolleyRequestQueue.getInstance().setRetryPolicy(request);
        CustomVolleyRequestQueue.getInstance().getRequestQueue().add(request);
    }
    /*
     * Register new user
     */
    public void registerUser(Context context, String firstName, String lastName, String email, String countryCode, String phoneNumber, String password, boolean checkedAgreements, final OnResponseModel networkResponse) {

        String url = URI_REGISTER;
        final JSONObject json = new JSONObject();
        TimeZone tz = TimeZone.getDefault();
        Date now = new Date();
        int offsetTime = 0 - tz.getOffset(now.getTime()) / 1000;
        try {
            json.put("timezone_offset", offsetTime);
            json.put("last_name",lastName);
            json.put("name",firstName);
            if(email!=null) {

                json.put("email", email);
            }
            if (phoneNumber != null) {
                json.put("contact_number", phoneNumber);
            }
            if (phoneNumber != null) {
                json.put("password", password);
            }
            json.put("country_code", countryCode);

            if(checkedAgreements){
                json.put("checked_privacy_policy", true);
                json.put("checked_report_consent", true);
                json.put("checked_terms_condition", true);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        CustomModelRequest<KZRegistrationResponse> request = new CustomModelRequest<KZRegistrationResponse>(
                context,
                Request.Method.POST,
                url, KZRegistrationResponse.class,
                genHeaders2(),
                null,
                new OnResponseModel() {
                    @Override
                    public void onResponseSuccess(Object model) {
                        networkResponse.onResponseSuccess(model);
                    }

                    @Override
                    public void onResponseError(BKNetworkResponseError error) {
                        networkResponse.onResponseError(error);
                    }
                }) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    byte[] bytes = json.toString().getBytes("utf-8");
                    return bytes;
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                return super.getBody();
            }
        };
        CustomVolleyRequestQueue.getInstance().setRetryPolicy(request);
        CustomVolleyRequestQueue.getInstance().getRequestQueue().add(request);
    }
    /*
     * Activate new user
     */
    public void activateUser(Context context, String activateCode, final OnResponseModel networkResponse) {
        String url = URI_ACTIVATE + activateCode;
        final JSONObject json = new JSONObject();
        TimeZone tz = TimeZone.getDefault();
        Date now = new Date();
        int offsetTime = 0 - tz.getOffset(now.getTime()) / 1000;
        try {
            json.put("timezone_offset", offsetTime);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        CustomModelRequest<KZVerifyResponse> request = new CustomModelRequest<KZVerifyResponse>(
                context,
                Request.Method.GET,
                url, KZVerifyResponse.class,
                genHeaders2(),
                null,
                new OnResponseModel() {
                    @Override
                    public void onResponseSuccess(Object model) {
                        networkResponse.onResponseSuccess(model);
                    }

                    @Override
                    public void onResponseError(BKNetworkResponseError error) {
                        networkResponse.onResponseError(error);
                    }
                }) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    byte[] bytes = json.toString().getBytes("utf-8");
                    return bytes;
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                return super.getBody();
            }
        };
        CustomVolleyRequestQueue.getInstance().setRetryPolicy(request);
        CustomVolleyRequestQueue.getInstance().getRequestQueue().add(request);
    }
    /*
     * Resend activate code new user
     */
    public void resentActivateCode(Context context, String userId, final OnResponseModel networkResponse) {
        String url = URI_REGISTER + userId + "/resend";
        final JSONObject json = new JSONObject();
        TimeZone tz = TimeZone.getDefault();
        Date now = new Date();
        int offsetTime = 0 - tz.getOffset(now.getTime()) / 1000;
        try {
            json.put("timezone_offset", offsetTime);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        CustomModelRequest<KZLoginResponse> request = new CustomModelRequest<KZLoginResponse>(
                context,
                Request.Method.POST,
                url, KZLoginResponse.class,
                genHeaders2(),
                null,
                new OnResponseModel() {
                    @Override
                    public void onResponseSuccess(Object model) {
                        networkResponse.onResponseSuccess(model);
                    }

                    @Override
                    public void onResponseError(BKNetworkResponseError error) {
                        networkResponse.onResponseError(error);
                    }
                }) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    byte[] bytes = json.toString().getBytes("utf-8");
                    return bytes;
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                return super.getBody();
            }
        };
        CustomVolleyRequestQueue.getInstance().setRetryPolicy(request);
        CustomVolleyRequestQueue.getInstance().getRequestQueue().add(request);
    }
    /*
     * Handle Volley Error
     *
     * @param volleyError
     * @return
     */
    public BKNetworkResponseError handleVolleyError(Context context, VolleyError volleyError) {
        NetworkResponse networkResponse = volleyError.networkResponse;
        BKNetworkResponseError responseError = null;

        if (networkResponse != null) {
            int statusCode = networkResponse.statusCode;
            String error = "";
            if (networkResponse != null && networkResponse.data != null) {
                error = new String(networkResponse.data);
                try {
                    error = error.replaceAll("\n    ", "");
                    error = error.replaceAll("\n", "");
                    JSONObject jsonObject = new JSONObject(error);
                    responseError = new BKNetworkResponseError();
                    responseError.description = jsonObject.getString("description");
                    responseError.code = jsonObject.getString("code");
                    responseError.status = String.valueOf(statusCode);
                    JSONObject jsonData = jsonObject.getJSONObject("data");
                    responseError.data = jsonData;
                } catch (Exception e) {
                    e.printStackTrace();

                    responseError = new BKNetworkResponseError();
                    responseError.status = String.valueOf(statusCode);
                    responseError.description = context.getString(R.string.error_from_server);
                }
            }

        } else {
            if (volleyError.toString().contains("com.android.volley.NoConnectionError")) {
                responseError = new BKNetworkResponseError();
                responseError.description = context.getString(R.string.error_network_problem);
            } else if (volleyError.toString().contains("com.android.volley.TimeoutError")) {
                responseError = new BKNetworkResponseError();
                responseError.description = context.getString(R.string.error_request_timeout);
            } else {
                responseError = new BKNetworkResponseError();
                responseError.description = volleyError.getMessage();
            }
        }
        return responseError;
    }

    public void loadConfigData(Context context, final OnResponseModel<KZConfigData> onResponseModel) {
        String url = URI_LOGIN;
        CustomModelRequest<KZConfigData> request = new CustomModelRequest<>(
                context,
                Request.Method.GET,
                url,
                KZConfigData.class,
                genHeadersWithoutAccessToken(),
                null, new OnResponseModel() {

            @Override
            public void onResponseSuccess(Object model) {
                onResponseModel.onResponseSuccess((KZConfigData) model);
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                onResponseModel.onResponseError(error);
            }
        });
        CustomVolleyRequestQueue.getInstance().setRetryPolicy(request);
        CustomVolleyRequestQueue.getInstance().getRequestQueue().add(request);
    }
    /*
     * Load Landing Data
     *
     * @param onResponseModel
     */
    public void loadLandingData2(Context context, final OnResponseModel<KZLandingData> onResponseModel) {
        String url = URI_COMPANY_LANDING;
        CustomModelRequest<KZLandingData> request = new CustomModelRequest<>(
                context,
                Request.Method.GET,
                url,
                KZLandingData.class,
                genHeaders(),
                null, new OnResponseModel() {
            @Override
            public void onResponseSuccess(Object model) {
                onResponseModel.onResponseSuccess((KZLandingData) model);
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                onResponseModel.onResponseError(error);
            }
        });
        CustomVolleyRequestQueue.getInstance().setRetryPolicy(request);
        CustomVolleyRequestQueue.getInstance().getRequestQueue().add(request);
    }
    /*
     * Load Branches
     *
     * @param onResponseModel
     */
    public void loadBranches(Context context, final OnResponseModel<KZBranch[]> onResponseModel) {
        String url = URI_BRANCHES;
        CustomModelRequest<KZBranch[]> request = new CustomModelRequest<>(
                context,
                Request.Method.GET,
                url,
                KZBranch[].class,
                genHeaders(),
                null,
                new OnResponseModel() {
            @Override
            public void onResponseSuccess(Object model) {
                onResponseModel.onResponseSuccess((KZBranch[]) model);
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                onResponseModel.onResponseError(error);
            }
        });
        CustomVolleyRequestQueue.getInstance().setRetryPolicy(request);
        CustomVolleyRequestQueue.getInstance().getRequestQueue().add(request);
    }
    /*
     * Load Branches By User
     *
     * @param userId
     * @param responseModel
     */
    public void loadBranchesByUser(Context context, String userId, final OnResponseModel<KZBranch[]> responseModel) {
        String url = URI_USER + userId + "/" + "branches";
        CustomModelRequest<KZBranch[]> request = new CustomModelRequest<>(
                context,
                Request.Method.GET,
                url,
                KZBranch[].class,
                genHeaders(),
                null,
                new OnResponseModel() {
            @Override
            public void onResponseSuccess(Object model) {
                responseModel.onResponseSuccess((KZBranch[]) model);
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                responseModel.onResponseError(error);
            }
        });
        CustomVolleyRequestQueue.getInstance().setRetryPolicy(request);
        CustomVolleyRequestQueue.getInstance().getRequestQueue().add(request);
    }
    /*
     * Search Vehicle
     *
     * @param startDate
     * @param endDate
     * @param branchId
     * @param onResponseModel
     */
    public void searchVehicles(Context context, Date startDate, Date endDate, String branchId, final OnResponseModel<KZSearchResult> onResponseModel) {
        String url = API_LINK + "v1/branch/" + branchId + "/vehicles";
        url += "/" + new SimpleDateFormat("yyyy-MM-dd").format(startDate) + "/"
                + new SimpleDateFormat("HH:mm").format(startDate)
                + "/" + new SimpleDateFormat("yyyy-MM-dd").format(endDate) + "/"
                + new SimpleDateFormat("HH:mm").format(endDate);
        url += "/timeline" + "/" + DateUtils.getTimeZoneOffset1() + "/" + DateUtils.getTimeZoneOffset1();
        CustomVolleyRequestQueue.getInstance().getRequestQueue().cancelAll(url);
        CustomModelRequest<KZSearchResult> request = new CustomModelRequest<>(
                context,
                Request.Method.GET,
                url,
                KZSearchResult.class,
                genHeaders(),
                null,
                new OnResponseModel() {
            @Override
            public void onResponseSuccess(Object model) {
                onResponseModel.onResponseSuccess((KZSearchResult) model);
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                onResponseModel.onResponseError(error);
            }
        });
        request.setTag(url);
        CustomVolleyRequestQueue.getInstance().setRetryPolicy(request);
        CustomVolleyRequestQueue.getInstance().getRequestQueue().add(request);
    }

    public void searchVehicles(Context context, Date startDate, String branchId, final OnResponseModel<KZSearchResult> onResponseModel) {
        String date = new SimpleDateFormat("yyyy-MM-dd").format(startDate);
        String time = new SimpleDateFormat("HH:mm").format(startDate);
        int timezone = DateUtils.getTimeZoneOffset1();

        String url = String.format(URI_SEARCH_VEHICLE,branchId,date, time,timezone);
        CustomModelRequest<KZSearchResult> request = new CustomModelRequest<>(
                context,
                Request.Method.GET,
                url,
                KZSearchResult.class,
                genHeaders(),
                null,
                new OnResponseModel() {
            @Override
            public void onResponseSuccess(Object model) {
                onResponseModel.onResponseSuccess((KZSearchResult) model);
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                onResponseModel.onResponseError(error);
            }
        });
        CustomVolleyRequestQueue.getInstance().setRetryPolicy(request);
        CustomVolleyRequestQueue.getInstance().getRequestQueue().add(request);
    }

    public void searchVehicles2(Context context, KZBranch branch, Date startDate, Date endDate, String branchId, final OnResponseModel<KZSearchResult> onResponseModel) {
        String date1 = new SimpleDateFormat("yyyy-MM-dd").format(startDate);
        String time1 = new SimpleDateFormat("HH:mm").format(startDate);
        String date2 = new SimpleDateFormat("yyyy-MM-dd").format(endDate);
        String time2 = new SimpleDateFormat("HH:mm").format(endDate);
        //int timezoneFromLocal = DateUtils.getTimeZoneOffset1();
        int timezone = Integer.parseInt(branch.time_zone_offset);

        String url = String.format(URI_SEARCH_VEHICLE_2,branchId, date1, time1, date2, time2, timezone, timezone);
        CustomModelRequest<KZSearchResult> request = new CustomModelRequest<>(context, Request.Method.GET,
                url, KZSearchResult.class, genHeaders(), null, new OnResponseModel() {
            @Override
            public void onResponseSuccess(Object model) {
                onResponseModel.onResponseSuccess((KZSearchResult) model);
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                onResponseModel.onResponseError(error);
            }
        });
        CustomVolleyRequestQueue.getInstance().setRetryPolicy(request);
        CustomVolleyRequestQueue.getInstance().getRequestQueue().add(request);
    }
    /*
     * Add location
     *
     * @param userId
     * @param jsonObject
     * @param onResponseModel
     */
    public void addLocation(Context context, String userId, final JsonObject jsonObject, final OnResponseModel<JsonObject> onResponseModel) {
        String url = URI_USER + userId + "/branches";
        CustomModelRequest<JsonObject> request = new CustomModelRequest<JsonObject>(
                context,
                Request.Method.POST,
                url, JsonObject.class, genHeaders(), null, new OnResponseModel() {
            @Override
            public void onResponseSuccess(Object model) {
                onResponseModel.onResponseSuccess((JsonObject) model);
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                onResponseModel.onResponseError(error);
            }
        }) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return jsonObject.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                return super.getBody();
            }
        };
        CustomVolleyRequestQueue.getInstance().setRetryPolicy(request);
        CustomVolleyRequestQueue.getInstance().getRequestQueue().add(request);
    }
    /*
     * Create Booking
     *
     * @param jsonObject
     * @param responseModel
     */
    public void createBooking(Context context, final JSONObject jsonObject, final OnResponseModel<KZBooking> responseModel) {
        String uri = URI_BOOKING;
        Log.d(TAG , "url : " + uri);
        Log.d(TAG , "param : " + jsonObject.toString());
        CustomModelRequest<KZBooking> request = new CustomModelRequest<KZBooking>(
                context,
                Request.Method.POST,
                uri,
                KZBooking.class,
                genHeaders(),
                null,
                new OnResponseModel() {
            @Override
            public void onResponseSuccess(Object model) {
                responseModel.onResponseSuccess((KZBooking) model);
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                responseModel.onResponseError(error);
            }
        }) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return jsonObject.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                return super.getBody();
            }
        };
        CustomVolleyRequestQueue.getInstance().setRetryPolicy(request);
        CustomVolleyRequestQueue.getInstance().getRequestQueue().add(request);
    }
    /*
     * Load Booking By User
     *
     * @param userId
     * @param onResponseModel
     */
    public void loadBookingByUser(Context context, int userId, final OnResponseModel<KZBookingsList> onResponseModel) {
        String url = URI_USER + userId + "/bookings/";
        Date today = DateUtils.getStartToday();
        Date end = DateUtils.getDayFromToday(90);
        url += DateUtils.formatDatePattern(today, "yyyy-MM-dd") + "/" + DateUtils.formatDatePattern(today, "HH:mm");
        url += "/" + DateUtils.formatDatePattern(end, "yyyy-MM-dd") + "/" + DateUtils.formatDatePattern(end, "HH:mm");
        CustomModelRequest<KZBookingsList> request = new CustomModelRequest<>(
                context,
                Request.Method.GET,
                url,
                KZBookingsList.class,
                genHeaders(),
                null,
                new OnResponseModel() {
            @Override
            public void onResponseSuccess(Object model) {
                onResponseModel.onResponseSuccess((KZBookingsList) model);
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                onResponseModel.onResponseError(error);
            }
        });
        CustomVolleyRequestQueue.getInstance().setRetryPolicy(request);
        CustomVolleyRequestQueue.getInstance().getRequestQueue().add(request);
    }

    public void loadBookingsByUser(Context context, int userId, int page, boolean isUpcoming, final OnResponseModel onResponseModel) {
        String url;
        if (isUpcoming) {
            url = URI_USER + userId + "/upcomming_bookings/" + page;
        } else {
            url = URI_USER + userId + "/past_bookings/" + page;
        }

        CustomModelRequest<JsonObject> request = new CustomModelRequest<>(
                context,
                Request.Method.GET,
                url,
                JsonObject.class,
                genHeaders(),
                null,
                new OnResponseModel() {
            @Override
            public void onResponseSuccess(Object model) {
                onResponseModel.onResponseSuccess( model);
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                onResponseModel.onResponseError(error);
            }
        });
        CustomVolleyRequestQueue.getInstance().setRetryPolicy(request);
        CustomVolleyRequestQueue.getInstance().getRequestQueue().add(request);
    }

    public void loadInUseBookingByUser(Context context, int userId, final OnResponseModel<KZBookingsList> onResponseModel) {
        String url = URI_USER + userId + "/bookings/inuse/";

        Date prev = DateUtils.getDayFromToday(-1);
        Date tomorrow = DateUtils.getDayFromToday(1);

        url += DateUtils.formatDatePattern(prev, "yyyy-MM-dd") + "/00:00";
        url += "/" + DateUtils.formatDatePattern(tomorrow, "yyyy-MM-dd") + "/00:00";

        CustomModelRequest<KZBookingsList> request = new CustomModelRequest<>(
                context,
                Request.Method.GET,
                url, KZBookingsList.class, genHeaders(), null, new OnResponseModel() {
            @Override
            public void onResponseSuccess(Object model) {
                onResponseModel.onResponseSuccess((KZBookingsList) model);
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                onResponseModel.onResponseError(error);
            }
        });
        CustomVolleyRequestQueue.getInstance().setRetryPolicy(request);
        CustomVolleyRequestQueue.getInstance().getRequestQueue().add(request);
    }
    /*public void loadInUseBookingByUser(Context context, int userId, final OnResponseModel<LoadedBookingModel> onResponseModel) {
        String url = URI_USER + userId + "/paging_bookings/inuse/";

        Date prev = DateUtils.getDayFromToday(-1);
        Date tomorrow = DateUtils.getDayFromToday(1);

        url += DateUtils.formatDatePattern(prev, "yyyy-MM-dd") + "/00:00";
        url += "/" + DateUtils.formatDatePattern(tomorrow, "yyyy-MM-dd") + "/00:00";
        url += "/0";

        CustomModelRequest<LoadedBookingModel> request = new CustomModelRequest<>(
                context,
                Request.Method.GET,
                url, LoadedBookingModel.class, genHeaders(), null, new OnResponseModel() {
            @Override
            public void onResponseSuccess(Object model) {
                onResponseModel.onResponseSuccess((LoadedBookingModel) model);
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                onResponseModel.onResponseError(error);
            }
        });
        CustomVolleyRequestQueue.getInstance().setRetryPolicy(request);
        CustomVolleyRequestQueue.getInstance().getRequestQueue().add(request);
    }*/
    public void loadBookingByBookingId(Context context, int bookingId, final OnResponseModel<KZBooking> onResponseModel) {
        String url = URI_BOOKING + "/" + bookingId;
        CustomModelRequest<KZBooking> request = new CustomModelRequest<>(
                context,
                Request.Method.GET,
                url, KZBooking.class, genHeaders(), null, new OnResponseModel() {
            @Override
            public void onResponseSuccess(Object model) {
                onResponseModel.onResponseSuccess((KZBooking) model);
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                onResponseModel.onResponseError(error);
            }
        });
        CustomVolleyRequestQueue.getInstance().setRetryPolicy(request);
        CustomVolleyRequestQueue.getInstance().getRequestQueue().add(request);
    }
    /*
     * Begin reservation
     *
     * @param jsonParams
     * @param booking_id
     * @param responseModel
     */
    public void beginReservation(Context context, final JSONObject jsonParams, int booking_id, final OnResponseModel<KZBooking> responseModel) {
        String url = URI_BOOKING +"/" + booking_id;
        CustomModelRequest<KZBooking> request = new CustomModelRequest<KZBooking>(
                context,
                Request.Method.PUT,
                url, KZBooking.class, genHeaders(), null, new OnResponseModel() {
            @Override
            public void onResponseSuccess(Object model) {
                responseModel.onResponseSuccess((KZBooking) model);
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                responseModel.onResponseError(error);
            }
        }) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return jsonParams.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                return super.getBody();
            }
        };
        CustomVolleyRequestQueue.getInstance().setRetryPolicy(request);
        CustomVolleyRequestQueue.getInstance().getRequestQueue().add(request);
    }
    /*
     * Update Booking By Id
     *
     * @param bookingId
     * @param jsonObject
     * @param responseModel
     */
    public void updateBookingById(Context context, int bookingId, final JSONObject jsonObject, final OnResponseModel<KZBooking> responseModel) {
        String url = URI_BOOKING +"/"+ bookingId;
        CustomModelRequest<KZBooking> request = new CustomModelRequest<KZBooking>(
                context,
                Request.Method.PUT,
                url, KZBooking.class, genHeaders(), null, new OnResponseModel() {
            @Override
            public void onResponseSuccess(Object model) {
                responseModel.onResponseSuccess((KZBooking) model);
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                responseModel.onResponseError(error);
            }
        }) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return jsonObject.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                return super.getBody();
            }
        };
        CustomVolleyRequestQueue.getInstance().setRetryPolicy(request);
        CustomVolleyRequestQueue.getInstance().getRequestQueue().add(request);
    }

    public void updateEndBooking(Context context, int bookingId, final JSONObject jsonObject, final OnResponseModel<JSONObject> responseModel) {
        String url = URI_BOOKING +"/"+ bookingId;
        CustomModelRequest<JsonObject> request = new CustomModelRequest<JsonObject>(
                context,
                Request.Method.PUT,
                url, JsonObject.class, genHeaders(), null, new OnResponseModel() {
            @Override
            public void onResponseSuccess(Object model) {
                JsonObject object = (JsonObject) model;
                try {
                    JSONObject obj = new JSONObject(object.toString());
                    responseModel.onResponseSuccess(obj);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                responseModel.onResponseError(error);
            }
        }) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return jsonObject.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                return super.getBody();
            }
        };
        CustomVolleyRequestQueue.getInstance().setRetryPolicy(request);
        CustomVolleyRequestQueue.getInstance().getRequestQueue().add(request);
    }

    public void updateCancelBooking(Context context, int bookingId, final OnResponseModel<Object> responseModel) {
        String url = URI_BOOKING + "/" + bookingId;
        CustomModelRequest<JsonObject> request = new CustomModelRequest<>(
                context,
                Request.Method.DELETE,
                url, JsonObject.class, genHeaders(), null, new OnResponseModel() {
            @Override
            public void onResponseSuccess(Object model) {
                responseModel.onResponseSuccess(model);
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                responseModel.onResponseError(error);
            }
        });
        CustomVolleyRequestQueue.getInstance().setRetryPolicy(request);
        CustomVolleyRequestQueue.getInstance().getRequestQueue().add(request);
    }

    public void logout(Context context, final OnResponseModel<Object> responseModel) {
        String url = URI_LOGOUT;
        CustomModelRequest<JsonObject> request = new CustomModelRequest<>(
                context,
                Request.Method.GET,
                url, JsonObject.class, genHeaders(), null, new OnResponseModel() {
            @Override
            public void onResponseSuccess(Object model) {
                responseModel.onResponseSuccess(model);
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                responseModel.onResponseError(error);
            }
        });
        CustomVolleyRequestQueue.getInstance().setRetryPolicy(request);
        CustomVolleyRequestQueue.getInstance().getRequestQueue().add(request);
    }

    public void loadUserForMixpanel(Context context, String userId, final OnResponseModel networkResponse) {
        String url = URI_LOAD_USER_FOR_MIXPANEL + userId;
        final JSONObject json = new JSONObject();

        CustomModelRequest<JsonObject> request = new CustomModelRequest<JsonObject>(
                context,
                Request.Method.GET,
                url, JsonObject.class,
                genHeaders(),
                null,
                new OnResponseModel() {
                    @Override
                    public void onResponseSuccess(Object model) {
                        networkResponse.onResponseSuccess(model);
                    }

                    @Override
                    public void onResponseError(BKNetworkResponseError error) {
                        networkResponse.onResponseError(error);
                    }
                }) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    byte[] bytes = json.toString().getBytes("utf-8");
                    return bytes;
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                return super.getBody();
            }
        };
        CustomVolleyRequestQueue.getInstance().setRetryPolicy(request);
        CustomVolleyRequestQueue.getInstance().getRequestQueue().add(request);
    }

    public void loadUserInfo(Context context, String userID, final OnResponseModel networkResponse) {
        String url = URI_REGISTER + userID;
        final JSONObject json = new JSONObject();
        TimeZone tz = TimeZone.getDefault();
        Date now = new Date();

        CustomModelRequest<JsonObject> request = new CustomModelRequest<JsonObject>(
                context,
                Request.Method.GET,
                url, JsonObject.class,
                genHeaders(),
                null,
                new OnResponseModel() {
                    @Override
                    public void onResponseSuccess(Object model) {
                        networkResponse.onResponseSuccess(model);
                    }

                    @Override
                    public void onResponseError(BKNetworkResponseError error) {
                        networkResponse.onResponseError(error);
                    }
                }) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    byte[] bytes = json.toString().getBytes("utf-8");
                    return bytes;
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                return super.getBody();
            }
        };
        CustomVolleyRequestQueue.getInstance().setRetryPolicy(request);
        CustomVolleyRequestQueue.getInstance().getRequestQueue().add(request);
    }

    public void updateUserInfo(Context context, String userID, final JSONObject jsonObject, final OnResponseModel networkResponse) {
        String url = URI_REGISTER + userID;

        CustomModelRequest<JsonObject> request = new CustomModelRequest<JsonObject>(
                context,
                Request.Method.PUT,
                url, JsonObject.class,
                genHeaders(),
                null,
                new OnResponseModel() {
                    @Override
                    public void onResponseSuccess(Object model) {
                        networkResponse.onResponseSuccess(model);
                    }

                    @Override
                    public void onResponseError(BKNetworkResponseError error) {
                        networkResponse.onResponseError(error);
                    }
                }) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    byte[] bytes = jsonObject.toString().getBytes("utf-8");
                    return bytes;
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                return super.getBody();
            }
        };
        CustomVolleyRequestQueue.getInstance().setRetryPolicy(request);
        CustomVolleyRequestQueue.getInstance().getRequestQueue().add(request);
    }

    public void getVehicleById(Context context, int vehicleId, final OnResponseModel<JSONObject> onResponseModel) {
        String url = URI_VEHICLE + vehicleId;
        CustomModelRequest<JsonObject> request = new CustomModelRequest<>(
                context,
                Request.Method.GET,
                url, JsonObject.class, genHeaders(), null, new OnResponseModel() {
            @Override
            public void onResponseSuccess(Object model) {
                JsonObject object = (JsonObject) model;
                try {
                    JSONObject obj = new JSONObject(object.toString());
                    onResponseModel.onResponseSuccess(obj);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                onResponseModel.onResponseError(error);
            }
        });
        CustomVolleyRequestQueue.getInstance().setRetryPolicy(request);
        CustomVolleyRequestQueue.getInstance().getRequestQueue().add(request);
    }
    /*
     * Load Branches
     *
     * @param onResponseModel
     */
    public void loadPaymentMethodDriver(Context context, final OnResponseModel<KZPaymentDetail> onResponseModel) {
        String url = URI_PAYMENT;
        CustomModelRequest<KZPaymentDetail> request = new CustomModelRequest<>(
                context,
                Request.Method.GET,
                url, KZPaymentDetail.class, genHeaders(), null, new OnResponseModel() {
            @Override
            public void onResponseSuccess(Object model) {
                onResponseModel.onResponseSuccess((KZPaymentDetail) model);
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                onResponseModel.onResponseError(error);
            }
        });
        CustomVolleyRequestQueue.getInstance().setRetryPolicy(request);
        CustomVolleyRequestQueue.getInstance().getRequestQueue().add(request);
    }
    /*
     * Unlock vehicle
     *
     * @param id
     * @param onResponseModel
     */
    public void unLockVehicle(Context context, int id, final OnResponseModel<JSONObject> onResponseModel) {
        String url = String.format(URI_UNLOCK, id);
        CustomModelRequest<JSONObject> request = new CustomModelRequest<>(
                context,
                Request.Method.GET, url,
                JSONObject.class, genHeaders(), null, new OnResponseModel() {
            @Override
            public void onResponseSuccess(Object model) {
                onResponseModel.onResponseSuccess((JSONObject) model);
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                onResponseModel.onResponseError(error);
            }
        });
        CustomVolleyRequestQueue.getInstance().setRetryPolicy(request);
        CustomVolleyRequestQueue.getInstance().getRequestQueue().add(request);
    }

    public void createBookingNotes(Context context, int bookingId, KZNotes notes, final OnResponseModel<KZNotes> responseModel) {
        String url = String.format(URI_CREATE_NOTE, bookingId);

        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("type", notes.type);
            jsonObject.put("message", notes.message);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        CustomModelRequest<KZNotes> request = new CustomModelRequest<KZNotes>(
                context,
                Request.Method.POST,
                url, KZNotes.class, genHeaders(), null, new OnResponseModel() {
            @Override
            public void onResponseSuccess(Object model) {
                responseModel.onResponseSuccess((KZNotes) model);
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                responseModel.onResponseError(error);
            }
        }) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return jsonObject.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                return super.getBody();
            }
        };

        CustomVolleyRequestQueue.getInstance().setRetryPolicy(request);
        CustomVolleyRequestQueue.getInstance().getRequestQueue().add(request);
    }

    public void postContactUs(Context context, String message, final OnResponseModel<KZNotes> responseModel) {
        String url = URI_CONTACT_US;

        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("subject", "Envoy Helpdesk");
            jsonObject.put("content", message);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        CustomModelRequest<KZNotes> request = new CustomModelRequest<KZNotes>(
                context,
                Request.Method.POST,
                url, KZNotes.class, genHeaders(), null, new OnResponseModel() {
            @Override
            public void onResponseSuccess(Object model) {
                responseModel.onResponseSuccess((KZNotes) model);
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                responseModel.onResponseError(error);
            }
        }) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return jsonObject.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                return super.getBody();
            }
        };

        CustomVolleyRequestQueue.getInstance().setRetryPolicy(request);
        CustomVolleyRequestQueue.getInstance().getRequestQueue().add(request);
    }
    /*
     * Lock Vehicle
     *
     * @param id
     * @param onResponseModel
     */
    public void lockVehicle(Context context, int id, final OnResponseModel<JSONObject> onResponseModel) {
        String url = String.format(URI_LOCK, id);
        CustomModelRequest<JSONObject> request = new CustomModelRequest<>(
                context,
                Request.Method.GET, url,
                JSONObject.class, genHeaders(), null, new OnResponseModel() {
            @Override
            public void onResponseSuccess(Object model) {
                onResponseModel.onResponseSuccess((JSONObject) model);
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                onResponseModel.onResponseError(error);
            }
        });
        CustomVolleyRequestQueue.getInstance().setRetryPolicy(request);
        CustomVolleyRequestQueue.getInstance().getRequestQueue().add(request);
    }

    public void uploadPictureApi(String url, byte[] fileData, final OnResponseModel<NetworkResponse> responseModel) {
        byte[] multipartBody = null;
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        DataOutputStream dataOutputStream = new DataOutputStream(byteArrayOutputStream);
        try {
            buildPart(dataOutputStream, fileData);
            multipartBody = byteArrayOutputStream.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
        }

        String mimeType = "multipart/form-data";
        MultipartRequest multipartRequest = new MultipartRequest(
                url,
                VolleyUtils.genHeaders3(),
                mimeType,
                multipartBody,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {
                        responseModel.onResponseSuccess(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        BKNetworkResponseError responseError = new BKNetworkResponseError();
                        responseError.description = error.getMessage();
                        responseModel.onResponseError(responseError);

                    }
                }
        );

        CustomVolleyRequestQueue.getInstance().setRetryPolicy(multipartRequest);
        CustomVolleyRequestQueue.getInstance().getRequestQueue().add(multipartRequest);
    }

    private void buildPart(DataOutputStream dataOutputStream, byte[] fileData) throws IOException {

        ByteArrayInputStream fileInputStream = new ByteArrayInputStream(fileData);
        int bytesAvailable = fileInputStream.available();

        int maxBufferSize = 1024 * 1024;
        int bufferSize = Math.min(bytesAvailable, maxBufferSize);
        byte[] buffer = new byte[bufferSize];

        // read file and write it into form...
        int bytesRead = fileInputStream.read(buffer, 0, bufferSize);

        while (bytesRead > 0) {
            dataOutputStream.write(buffer, 0, bufferSize);
            bytesAvailable = fileInputStream.available();
            bufferSize = Math.min(bytesAvailable, maxBufferSize);
            bytesRead = fileInputStream.read(buffer, 0, bufferSize);
        }
    }
    /*
     * Call api to get active code to start reset password
     *
    * */
    public void getActiveCodeToResetPassword(Context context, String email, String contactNumber, String countryCode, final OnResponseModel networkResponse) {
        String url = URI_RESET_PASSWORD;
        final JSONObject json = new JSONObject();
        TimeZone tz = TimeZone.getDefault();
        Date now = new Date();
        int offsetTime = 0 - tz.getOffset(now.getTime()) / 1000;
        String phone = "";
        if(contactNumber!=null){
            String phoneEdited = contactNumber.replaceFirst("^0+(?!$)", "");
            phone = countryCode + phoneEdited;
        }
        try {
            json.put("email", email);
            json.put("country_code", countryCode);
            json.put("contact_number", contactNumber);
            json.put("phone", phone);
            json.put("timezone_offset", offsetTime);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        CustomModelRequest<KZLoginResponse> request = new CustomModelRequest<KZLoginResponse>(
                context,
                Request.Method.POST,
                url, KZLoginResponse.class,
                genHeaders(),
                null,
                new OnResponseModel() {
                    @Override
                    public void onResponseSuccess(Object model) {
                        networkResponse.onResponseSuccess(model);
                    }

                    @Override
                    public void onResponseError(BKNetworkResponseError error) {
                        networkResponse.onResponseError(error);
                    }
                }) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    byte[] bytes = json.toString().getBytes("utf-8");
                    return bytes;
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                return super.getBody();
            }
        };
        CustomVolleyRequestQueue.getInstance().setRetryPolicy(request);
        CustomVolleyRequestQueue.getInstance().getRequestQueue().add(request);
    }

    public void verifyToResetPassword(Context context, String code, final OnResponseModel networkResponse) {
        String url = URI_RESET_PASSWORD  +"/" +code;
        final JSONObject json = new JSONObject();

        CustomModelRequest<KZLoginResponse> request = new CustomModelRequest<KZLoginResponse>(
                context,
                Request.Method.GET,
                url, KZLoginResponse.class,
                genHeaders(),
                null,
                new OnResponseModel() {
                    @Override
                    public void onResponseSuccess(Object model) {
                        networkResponse.onResponseSuccess(model);
                    }

                    @Override
                    public void onResponseError(BKNetworkResponseError error) {
                        networkResponse.onResponseError(error);
                    }
                }) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    byte[] bytes = json.toString().getBytes("utf-8");
                    return bytes;
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                return super.getBody();
            }
        };
        CustomVolleyRequestQueue.getInstance().setRetryPolicy(request);
        CustomVolleyRequestQueue.getInstance().getRequestQueue().add(request);
    }

    public void resetPassword(Context context, String code, String password, final OnResponseModel networkResponse) {
        String url = URI_RESET_PASSWORD  +"/"+ code;
        final JSONObject json = new JSONObject();
        TimeZone tz = TimeZone.getDefault();
        Date now = new Date();
        int offsetTime = 0 - tz.getOffset(now.getTime()) / 1000;

        try {
            json.put("timezone_offset", offsetTime);
            json.put("password", password);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        CustomModelRequest<KZLoginResponse> request = new CustomModelRequest<KZLoginResponse>(
                context,
                Request.Method.POST,
                url, KZLoginResponse.class,
                genHeaders(),
                null,
                new OnResponseModel() {
                    @Override
                    public void onResponseSuccess(Object model) {
                        networkResponse.onResponseSuccess(model);
                    }

                    @Override
                    public void onResponseError(BKNetworkResponseError error) {
                        networkResponse.onResponseError(error);
                    }
                }) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    byte[] bytes = json.toString().getBytes("utf-8");
                    return bytes;
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                return super.getBody();
            }
        };
        CustomVolleyRequestQueue.getInstance().setRetryPolicy(request);
        CustomVolleyRequestQueue.getInstance().getRequestQueue().add(request);
    }

    public static void handleErrorRespond(final Context context, BKNetworkResponseError networkResponse) {
        if(networkResponse != null) {

            if(networkResponse.description.startsWith("com.google.gson.JsonSyntaxException:")){
                VolleyUtils.getSharedNetwork().pushUnKnowError(context, networkResponse.description, new OnResponseModel() {
                    @Override
                    public void onResponseSuccess(Object model) {

                    }

                    @Override
                    public void onResponseError(BKNetworkResponseError error) {

                    }
                });

                networkResponse.description = "Something wrong happened!";
            }


            if(networkResponse.status != null &&
                    networkResponse.code != null &&
                    networkResponse.status.contains("401") &&
                    networkResponse.code.equals("token_required"))
            {
                Analytics.with(context).track(networkResponse.description);

                DialogUtils.showMessageDialog(context, true, "",
                        networkResponse.description,
                        context.getString(R.string.btn_okie),
                        "",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.cancel();
                                Intent intent = new Intent(context, LoginActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                context.startActivity(intent);
                                ((Activity)context).finish();
                            }
                        },null);

            } else if (networkResponse.description != null && !networkResponse.description.equals("")) {
                Analytics.with(context).track(networkResponse.description);

                DialogUtils.showMessageDialog(context,
                        true,
                        "",
                        networkResponse.description,
                        context.getString(R.string.btn_okie),
                        "",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.cancel();
                            }
                        },null);

            } else {
                Analytics.with(context).track(context.getString(R.string.error_from_server));
                // 500
                DialogUtils.showMessageDialog(context,
                        true,
                        "", context.getString(R.string.error_from_server),
                        context.getString(R.string.btn_okie),
                        "",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.cancel();
                            }
                        },null);
            }
        }else {
            Analytics.with(context).track(context.getString(R.string.error_network_problem));
            // Request timeout
            DialogUtils.showMessageDialog(context,
                    true,
                    "",
                    context.getString(R.string.error_network_problem),
                    context.getString(R.string.btn_okie),
                    "",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.cancel();
                        }
                    },null);
        }
    }
    /*
     * Load membership list
     * */
    public void loadMembershipList(Context context, final OnResponseModel<JsonArray> onResponseModel) {
        String url = URI_MEMBERSHIP_LIST;

        CustomModelRequest<JsonArray> request = new CustomModelRequest<>(
                context,
                Request.Method.GET,
                url, JsonArray.class, genHeaders(), null, new OnResponseModel() {
            @Override
            public void onResponseSuccess(Object model) {
                try {
                    JsonArray array = (JsonArray) model;
                    onResponseModel.onResponseSuccess(array);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                onResponseModel.onResponseError(error);
            }
        });
        CustomVolleyRequestQueue.getInstance().setRetryPolicy(request);
        CustomVolleyRequestQueue.getInstance().getRequestQueue().add(request);
    }

    public void loadMembershipListOfUser(Context context, final OnResponseModel<JsonArray> onResponseModel){

        int userId = KZAccount.getSharedAccount().getUser().id;

        String url = API_LINK + "v1/user/" + userId + "/memberships";

        CustomModelRequest<JsonArray> request = new CustomModelRequest<>(
                context,
                Request.Method.GET,
                url, JsonArray.class, genHeaders(), null, new OnResponseModel() {
            @Override
            public void onResponseSuccess(Object model) {
                try {
                    JsonArray array = (JsonArray) model;
                    onResponseModel.onResponseSuccess(array);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                onResponseModel.onResponseError(error);
            }
        });
        CustomVolleyRequestQueue.getInstance().setRetryPolicy(request);
        CustomVolleyRequestQueue.getInstance().getRequestQueue().add(request);
    }

    public void applyMembership(Context context, String membershipId, final OnResponseModel networkResponse) {
        String url = URI_MEMBERSHIP_APPLY;

        final JSONObject json = new JSONObject();
        try {
            json.put("membership_id", membershipId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        CustomModelRequest<KZLoginResponse> request = new CustomModelRequest<KZLoginResponse>(
                context,
                Request.Method.POST,
                url, KZLoginResponse.class,
                genHeaders(),
                null,
                new OnResponseModel() {
                    @Override
                    public void onResponseSuccess(Object model) {
                        networkResponse.onResponseSuccess(model);
                    }

                    @Override
                    public void onResponseError(BKNetworkResponseError error) {
                        networkResponse.onResponseError(error);
                    }
                }) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    byte[] bytes = json.toString().getBytes("utf-8");
                    return bytes;
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                return super.getBody();
            }
        };
        CustomVolleyRequestQueue.getInstance().setRetryPolicy(request);
        CustomVolleyRequestQueue.getInstance().getRequestQueue().add(request);
    }

    public void removeMembership(Context context, String membershipId, final OnResponseModel networkResponse) {
        String url = String.format(URI_MEMBERSHIP_DELETE, membershipId);

        CustomModelRequest<JsonObject> request = new CustomModelRequest<>(
                context,
                Request.Method.DELETE,
                url, JsonObject.class, genHeaders(), null, new OnResponseModel() {
            @Override
            public void onResponseSuccess(Object model) {
                networkResponse.onResponseSuccess(model);
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                networkResponse.onResponseError(error);
            }
        });
        CustomVolleyRequestQueue.getInstance().setRetryPolicy(request);
        CustomVolleyRequestQueue.getInstance().getRequestQueue().add(request);
    }

    public void loadMembership(Context context, String membershipId, final OnResponseModel networkResponse) {
        String url = String.format(URI_MEMBERSHIP_DETAIL, membershipId);

        CustomModelRequest<JsonObject> request = new CustomModelRequest<>(
                context,
                Request.Method.GET,
                url, JsonObject.class, genHeaders(), null, new OnResponseModel() {
            @Override
            public void onResponseSuccess(Object model) {
                networkResponse.onResponseSuccess(model);
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                networkResponse.onResponseError(error);
            }
        });
        CustomVolleyRequestQueue.getInstance().setRetryPolicy(request);
        CustomVolleyRequestQueue.getInstance().getRequestQueue().add(request);
    }
    /*
     * Load benefit list
     * */
    public void loadBenefitList(Context context, final OnResponseModel<JsonArray> onResponseModel){
        int userId = KZAccount.getSharedAccount().getUser().id;

        String url = API_LINK + "v1/user/" + userId + "/benefits";

        CustomModelRequest<JsonArray> request = new CustomModelRequest<>(
                context,
                Request.Method.GET,
                url, JsonArray.class, genHeaders(), null, new OnResponseModel() {
            @Override
            public void onResponseSuccess(Object model) {
                try {
                    JsonArray array = (JsonArray) model;
                    onResponseModel.onResponseSuccess(array);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                onResponseModel.onResponseError(error);
            }
        });
        CustomVolleyRequestQueue.getInstance().setRetryPolicy(request);
        CustomVolleyRequestQueue.getInstance().getRequestQueue().add(request);
    }

    public void loadPromoList(Context context, final OnResponseModel<JsonObject> onResponseModel){
        int userId = KZAccount.getSharedAccount().getUser().id;

        String url = API_LINK + "v1/user/" + userId + "/promotions";

        CustomModelRequest<JsonObject> request = new CustomModelRequest<>(
                context,
                Request.Method.GET,
                url, JsonObject.class, genHeaders(), null, new OnResponseModel() {
            @Override
            public void onResponseSuccess(Object model) {
                try {
                    JsonObject array = (JsonObject) model;
                    onResponseModel.onResponseSuccess(array);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                onResponseModel.onResponseError(error);
            }
        });
        CustomVolleyRequestQueue.getInstance().setRetryPolicy(request);
        CustomVolleyRequestQueue.getInstance().getRequestQueue().add(request);
    }

    public void applyPromo(Context context, String promoCode, final OnResponseModel<JsonObject> onResponseModel){

        int userId = KZAccount.getSharedAccount().getUser().id;

        String url = API_LINK + "v1/user/" + userId + "/promotions";

        final JSONObject json = new JSONObject();
        try {
            json.put("promotion_code", promoCode);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        CustomModelRequest<JsonObject> request = new CustomModelRequest<JsonObject>(
                context,
                Request.Method.POST,
                url, JsonObject.class,
                genHeaders(),
                null,
                new OnResponseModel() {
                    @Override
                    public void onResponseSuccess(Object model) {
                        try {
                            JsonObject array = (JsonObject) model;
                            onResponseModel.onResponseSuccess(array);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onResponseError(BKNetworkResponseError error) {
                        onResponseModel.onResponseError(error);
                    }
                }) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    byte[] bytes = json.toString().getBytes("utf-8");
                    return bytes;
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                return super.getBody();
            }
        };
        CustomVolleyRequestQueue.getInstance().setRetryPolicy(request);
        CustomVolleyRequestQueue.getInstance().getRequestQueue().add(request);
    }

    void uploadDeviceId(Context context, String deviceId, final OnResponseModel<JsonObject> onResponseModel){

        int userId = KZAccount.getSharedAccount().getUser().id;

        String url = API_LINK + "v1/user/" + userId;

        final JSONObject json = new JSONObject();
        try {
            json.put("device_id", deviceId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        CustomModelRequest<JsonObject> request = new CustomModelRequest<JsonObject>(
                context,
                Request.Method.PUT,
                url, JsonObject.class,
                genHeaders(),
                null,
                new OnResponseModel() {
                    @Override
                    public void onResponseSuccess(Object model) {
                        try {
                            JsonObject array = (JsonObject) model;
                            onResponseModel.onResponseSuccess(array);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onResponseError(BKNetworkResponseError error) {
                        onResponseModel.onResponseError(error);
                    }
                }) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    byte[] bytes = json.toString().getBytes("utf-8");
                    return bytes;
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                return super.getBody();
            }
        };
        CustomVolleyRequestQueue.getInstance().setRetryPolicy(request);
        CustomVolleyRequestQueue.getInstance().getRequestQueue().add(request);
    }

    public void reportLostKeyCard(Context context, final JSONObject jsonObject, final OnResponseModel responseModel) {
        String url = URI_REPORT_LOST_KEY_CARD ;
        CustomModelRequest<JsonObject> request = new CustomModelRequest<JsonObject>(
                context,
                Request.Method.POST,
                url, JsonObject.class, genHeaders(), null, new OnResponseModel() {
            @Override
            public void onResponseSuccess(Object model) {
                responseModel.onResponseSuccess(model);
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                responseModel.onResponseError(error);
            }
        }) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return jsonObject.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                return super.getBody();
            }
        };
        CustomVolleyRequestQueue.getInstance().setRetryPolicy(request);
        CustomVolleyRequestQueue.getInstance().getRequestQueue().add(request);
    }

    public void referAFriend(Context context, final JSONObject jsonObject, final OnResponseModel responseModel) {
        String url = URI_REFER_A_FRIEND ;
        CustomModelRequest<JsonObject> request = new CustomModelRequest<JsonObject>(
                context,
                Request.Method.POST,
                url, JsonObject.class, genHeaders(), null, new OnResponseModel() {
            @Override
            public void onResponseSuccess(Object model) {
                responseModel.onResponseSuccess(model);
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                responseModel.onResponseError(error);
            }
        }) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return jsonObject.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                return super.getBody();
            }
        };
        CustomVolleyRequestQueue.getInstance().setRetryPolicy(request);
        CustomVolleyRequestQueue.getInstance().getRequestQueue().add(request);
    }

    public void addAGuest(Context context, int branchID, final JSONObject jsonObject, final OnResponseModel responseModel) {
        String url = String.format(URI_ADD_A_GUEST, branchID);
        CustomModelRequest<JsonObject> request = new CustomModelRequest<JsonObject>(
                context,
                Request.Method.POST,
                url, JsonObject.class, genHeaders(), null, new OnResponseModel() {
            @Override
            public void onResponseSuccess(Object model) {
                responseModel.onResponseSuccess(model);
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                responseModel.onResponseError(error);
            }
        }) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return jsonObject.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                return super.getBody();
            }
        };
        CustomVolleyRequestQueue.getInstance().setRetryPolicy(request);
        CustomVolleyRequestQueue.getInstance().getRequestQueue().add(request);
    }

    public void loadVideo(Context context, final OnResponseModel onResponseModel) {
        String url = URI_GET_VIDEOS;
        CustomModelRequest<Object> request = new CustomModelRequest<>(
                context,
                Request.Method.GET,
                url,
                Object.class,
                genHeaders(),
                null, new OnResponseModel<Object>() {
            @Override
            public void onResponseSuccess(Object model) {
                onResponseModel.onResponseSuccess( model);
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                onResponseModel.onResponseError(error);
            }
        });
        CustomVolleyRequestQueue.getInstance().setRetryPolicy(request);
        CustomVolleyRequestQueue.getInstance().getRequestQueue().add(request);
    }

    public void loadSocialLink(Context context, final OnResponseModel<Object> onResponseModel) {
        String url = URI_GET_SOCIAL_LINKS;
        CustomModelRequest<Object> request = new CustomModelRequest<>(
                context,
                Request.Method.GET,
                url,
                Object.class,
                genHeaders(),
                null, new OnResponseModel<Object>() {
            @Override
            public void onResponseSuccess(Object model) {
                onResponseModel.onResponseSuccess(model);
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                onResponseModel.onResponseError(error);
            }
        });
        CustomVolleyRequestQueue.getInstance().setRetryPolicy(request);
        CustomVolleyRequestQueue.getInstance().getRequestQueue().add(request);
    }

    public void loadUpdatedBookingByBookingId(Context context, int bookingId, final OnResponseModel<Object> onResponseModel) {
        String url = URI_BOOKING + "/" + bookingId + "/update" ;
        CustomModelRequest<JsonObject> request = new CustomModelRequest<>(
                context,
                Request.Method.GET,
                url, JsonObject.class, genHeaders(), null, new OnResponseModel() {
            @Override
            public void onResponseSuccess(Object model) {
                onResponseModel.onResponseSuccess( model);
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                onResponseModel.onResponseError(error);
            }
        });
        CustomVolleyRequestQueue.getInstance().setRetryPolicy(request);
        CustomVolleyRequestQueue.getInstance().getRequestQueue().add(request);
    }

    public void loadBookingCostByBookingId(Context context, int bookingId, final OnResponseModel<Object> onResponseModel) {
        String url = URI_BOOKING + "/" + bookingId + "/inuse_costs" ;
        CustomModelRequest<JsonObject> request = new CustomModelRequest<>(
                context,
                Request.Method.GET,
                url,
                JsonObject.class,
                genHeaders(),
                null,
                new OnResponseModel() {
            @Override
            public void onResponseSuccess(Object model) {
                onResponseModel.onResponseSuccess( model);
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                onResponseModel.onResponseError(error);
            }
        });
        CustomVolleyRequestQueue.getInstance().setRetryPolicy(request);
        CustomVolleyRequestQueue.getInstance().getRequestQueue().add(request);
    }

    public void verifyToInviteUser(Context context, String email, String contactNumber, String countryCode,String code, final OnResponseModel networkResponse) {
        String url = URI_INVITE_USER  +"/" +code;
        final JSONObject json = new JSONObject();
        TimeZone tz = TimeZone.getDefault();
        Date now = new Date();
        int offsetTime = 0 - tz.getOffset(now.getTime()) / 1000;
        String phone = "";
        if(contactNumber!=null){
            String phoneEdited = contactNumber.replaceFirst("^0+(?!$)", "");
            phone = countryCode + phoneEdited;
        }
        try {
            json.put("email", email);
            json.put("country_code", countryCode);
            json.put("contact_number", contactNumber);
            json.put("contact_number_second", phone);
            json.put("timezone_offset", offsetTime);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        CustomModelRequest<KZLoginResponse> request = new CustomModelRequest<KZLoginResponse>(
                context,
                Request.Method.POST,
                url, KZLoginResponse.class,
                genHeaders(),
                null,
                new OnResponseModel() {
                    @Override
                    public void onResponseSuccess(Object model) {
                        networkResponse.onResponseSuccess(model);
                    }

                    @Override
                    public void onResponseError(BKNetworkResponseError error) {
                        networkResponse.onResponseError(error);
                    }
                }) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    byte[] bytes = json.toString().getBytes("utf-8");
                    return bytes;
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                return super.getBody();
            }
        };
        CustomVolleyRequestQueue.getInstance().setRetryPolicy(request);
        CustomVolleyRequestQueue.getInstance().getRequestQueue().add(request);
    }

    public void getActiveCodeToInviteUser(Context context, String email, String contactNumber, String countryCode, final OnResponseModel networkResponse) {
        String url = URI_INVITE_USER;
        final JSONObject json = new JSONObject();
        TimeZone tz = TimeZone.getDefault();
        Date now = new Date();
        int offsetTime = 0 - tz.getOffset(now.getTime()) / 1000;
        String phone = "";
        if(contactNumber!=null){
            String phoneEdited = contactNumber.replaceFirst("^0+(?!$)", "");
            phone = countryCode + phoneEdited;
        }
        try {
            json.put("email", email);
            json.put("country_code", countryCode);
            json.put("contact_number", contactNumber);
            json.put("contact_number_second", phone);
            json.put("timezone_offset", offsetTime);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        CustomModelRequest<KZLoginResponse> request = new CustomModelRequest<KZLoginResponse>(
                context,
                Request.Method.POST,
                url, KZLoginResponse.class,
                genHeaders(),
                null,
                new OnResponseModel() {
                    @Override
                    public void onResponseSuccess(Object model) {
                        networkResponse.onResponseSuccess(model);
                    }

                    @Override
                    public void onResponseError(BKNetworkResponseError error) {
                        networkResponse.onResponseError(error);
                    }
                }) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    byte[] bytes = json.toString().getBytes("utf-8");
                    return bytes;
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                return super.getBody();
            }
        };
        CustomVolleyRequestQueue.getInstance().setRetryPolicy(request);
        CustomVolleyRequestQueue.getInstance().getRequestQueue().add(request);
    }

    public void updateDefaultPaymentMethod(Context context, String paymentID, final OnResponseModel onResponseModel) {
        String url = URI_PAYMENT + "/" + paymentID ;
        final JsonObject json = new JsonObject();
        CustomModelRequest request = new CustomModelRequest<JsonObject>(
                context,
                Request.Method.PUT,
                url, JsonObject.class, genHeaders(), null, new OnResponseModel() {
            @Override
            public void onResponseSuccess(Object model) {
                onResponseModel.onResponseSuccess(model);
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                onResponseModel.onResponseError(error);
            }
        }){
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    byte[] bytes = json.toString().getBytes("utf-8");
                    return bytes;
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                return super.getBody();
            }
        };
        CustomVolleyRequestQueue.getInstance().setRetryPolicy(request);
        CustomVolleyRequestQueue.getInstance().getRequestQueue().add(request);
    }

    public void deletePaymentMethod(Context context, String paymentID, final OnResponseModel onResponseModel) {
        String url = URI_PAYMENT + "/" + paymentID ;
        final JsonObject json = new JsonObject();
        CustomModelRequest request = new CustomModelRequest<JsonObject>(
                context,
                Request.Method.DELETE,
                url, JsonObject.class, genHeaders(), null, new OnResponseModel() {
            @Override
            public void onResponseSuccess(Object model) {
                onResponseModel.onResponseSuccess(model);
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                onResponseModel.onResponseError(error);
            }
        }){
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    byte[] bytes = json.toString().getBytes("utf-8");
                    return bytes;
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                return super.getBody();
            }
        };
        CustomVolleyRequestQueue.getInstance().setRetryPolicy(request);
        CustomVolleyRequestQueue.getInstance().getRequestQueue().add(request);
    }

    public void loadPaymentMethodCostCenter(Context context, final OnResponseModel<KZPaymentDetail> onResponseModel) {
        String url = URI_PAYMENT_COST_CENTER;
        CustomModelRequest<KZPaymentDetail> request = new CustomModelRequest<>(
                context,
                Request.Method.GET,
                url, KZPaymentDetail.class, genHeaders(), null, new OnResponseModel() {
            @Override
            public void onResponseSuccess(Object model) {
                onResponseModel.onResponseSuccess((KZPaymentDetail) model);
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                onResponseModel.onResponseError(error);
            }
        });
        CustomVolleyRequestQueue.getInstance().setRetryPolicy(request);
        CustomVolleyRequestQueue.getInstance().getRequestQueue().add(request);
    }

    public void loadPaymentMethodCompany(Context context, final OnResponseModel<KZPaymentDetail> onResponseModel) {
        String url = URI_PAYMENT_COMPANY;
        CustomModelRequest<KZPaymentDetail> request = new CustomModelRequest<>(
                context,
                Request.Method.GET,
                url, KZPaymentDetail.class, genHeaders(), null, new OnResponseModel() {
            @Override
            public void onResponseSuccess(Object model) {
                onResponseModel.onResponseSuccess((KZPaymentDetail) model);
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                onResponseModel.onResponseError(error);
            }
        });
        CustomVolleyRequestQueue.getInstance().setRetryPolicy(request);
        CustomVolleyRequestQueue.getInstance().getRequestQueue().add(request);
    }

    public void loadCostCenter(Context context, final OnResponseModel onResponseModel) {
        String url = URI_LOAD_COST_CENTER;

        CustomModelRequest<JsonArray> request = new CustomModelRequest<>(
                context,
                Request.Method.GET,
                url, JsonArray.class, genHeaders(), null, new OnResponseModel<JsonArray>() {
            @Override
            public void onResponseSuccess(JsonArray model) {
                onResponseModel.onResponseSuccess(model);
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                onResponseModel.onResponseError(error);
            }
        });
        CustomVolleyRequestQueue.getInstance().setRetryPolicy(request);
        CustomVolleyRequestQueue.getInstance().getRequestQueue().add(request);
    }

    public void loadPolicyInfo(Context context, final OnResponseModel onResponseModel, int companyID) {
        String url = String.format(URI_LOAD_POLICY, companyID);

        CustomModelRequest<JsonObject> request = new CustomModelRequest<>(
                context,
                Request.Method.GET,
                url, JsonObject.class, genHeaders(), null, new OnResponseModel() {
            @Override
            public void onResponseSuccess(Object model) {
                JsonObject object = (JsonObject) model;
                try {
                    JSONObject obj = new JSONObject(object.toString());
                    onResponseModel.onResponseSuccess(obj);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                onResponseModel.onResponseError(error);
            }
        });
        CustomVolleyRequestQueue.getInstance().setRetryPolicy(request);
        CustomVolleyRequestQueue.getInstance().getRequestQueue().add(request);
    }

    private void pushUnKnowError(Context context, String description, final OnResponseModel responseModel) {
        String url = URI_CREATE_ERROR;

        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("description", description);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        CustomModelRequest<Object> request = new CustomModelRequest<Object>(
                context,
                Request.Method.POST,
                url, Object.class, genHeaders(), null, new OnResponseModel() {
            @Override
            public void onResponseSuccess(Object model) {
                responseModel.onResponseSuccess(model);
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                responseModel.onResponseError(error);
            }
        }) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return jsonObject.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                return super.getBody();
            }
        };

        CustomVolleyRequestQueue.getInstance().setRetryPolicy(request);
        CustomVolleyRequestQueue.getInstance().getRequestQueue().add(request);
    }

    public void checkInsideGeoFenceArea(Context context, int bookingId, String lat, String lng, final OnResponseModel<JsonObject> networkResponse) {
        String url = String.format(URI_CHECK_GEO_FENCE_AREA_INSIDE, lat, lng);
        final JSONObject json = new JSONObject();
        try {
            json.put("booking_id", bookingId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        CustomModelRequest<JsonObject> request = new CustomModelRequest<JsonObject>(
                context,
                Request.Method.POST,
                url, JsonObject.class,
                genHeaders(),
                null,
                new OnResponseModel<JsonObject>() {
                    @Override
                    public void onResponseSuccess(JsonObject model) {
                        networkResponse.onResponseSuccess(model);
                    }

                    @Override
                    public void onResponseError(BKNetworkResponseError error) {
                        networkResponse.onResponseError(error);
                    }
                }) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    byte[] bytes = json.toString().getBytes("utf-8");
                    return bytes;
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                return super.getBody();
            }
        };
        CustomVolleyRequestQueue.getInstance().setRetryPolicy(request);
        CustomVolleyRequestQueue.getInstance().getRequestQueue().add(request);
    }

    public void loadDropOffBranches(String included_ids, Context context, final OnResponseModel<KZBranch[]> onResponseModel) {
        String url = URI_LOAD_DROP_OFF_BRANCHES+"?included_ids="+included_ids;

        CustomModelRequest<KZBranch[]> request = new CustomModelRequest<>(
                context,
                Request.Method.GET,
                url, KZBranch[].class, genHeaders(), null, new OnResponseModel<KZBranch[]>() {
            @Override
            public void onResponseSuccess(KZBranch[] model) {
                onResponseModel.onResponseSuccess(model);
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                onResponseModel.onResponseError(error);
            }
        });
        CustomVolleyRequestQueue.getInstance().setRetryPolicy(request);
        CustomVolleyRequestQueue.getInstance().getRequestQueue().add(request);
    }

    public void getSubCompanies(Context context, final OnResponseModel<KZCompany[]> objectOnResponseModel) {
        String url = URI_COMPANIES;
        CustomModelRequest<KZCompany[]> request = new CustomModelRequest<>(
                context,
                Request.Method.GET,
                url, KZCompany[].class, genHeaders(), null, new OnResponseModel<KZCompany[]>() {
            @Override
            public void onResponseSuccess(KZCompany[] model) {
                objectOnResponseModel.onResponseSuccess(model);
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                objectOnResponseModel.onResponseError(error);
            }
        });
        CustomVolleyRequestQueue.getInstance().setRetryPolicy(request);
        CustomVolleyRequestQueue.getInstance().getRequestQueue().add(request);
    }

    public void checkUserByEmail(Context context, String email, final OnResponseModel networkResponse) {
        String url = URI_LOGIN_CHECK;
        final JSONObject json = new JSONObject();
        TimeZone tz = TimeZone.getDefault();
        Date now = new Date();
        int offsetTime = 0 - tz.getOffset(now.getTime()) / 1000;
        try {
            json.put("email", email);
            json.put("timezone_offset", offsetTime);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        CustomModelRequest<JsonObject> request = new CustomModelRequest<JsonObject>(
                context,
                Request.Method.POST,
                url, JsonObject.class,
                genHeaders(),
                null,
                new OnResponseModel() {
                    @Override
                    public void onResponseSuccess(Object model) {
                        networkResponse.onResponseSuccess(model);
                    }

                    @Override
                    public void onResponseError(BKNetworkResponseError error) {
                        networkResponse.onResponseError(error);
                    }
                }) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    byte[] bytes = json.toString().getBytes("utf-8");
                    return bytes;
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                return super.getBody();
            }
        };
        CustomVolleyRequestQueue.getInstance().setRetryPolicy(request);
        CustomVolleyRequestQueue.getInstance().getRequestQueue().add(request);
    }

    public void getAgreements(Context context, final OnResponseModel<KzAgreement> onResponseModel) {
        String url = URI_AGREEMENT;
        CustomModelRequest<KzAgreement> request = new CustomModelRequest<>(
                context,
                Request.Method.GET,
                url,
                KzAgreement.class,
                genHeaders(),
                null, new OnResponseModel() {

            @Override
            public void onResponseSuccess(Object model) {
                onResponseModel.onResponseSuccess((KzAgreement) model);
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                onResponseModel.onResponseError(error);
            }
        });
        CustomVolleyRequestQueue.getInstance().setRetryPolicy(request);
        CustomVolleyRequestQueue.getInstance().getRequestQueue().add(request);
    }

    public void getPage(Context context, String page, final OnResponseModel<KzPage> onResponseModel) {
        String url = URI_PAGE.concat(page);
        CustomModelRequest<KzPage> request = new CustomModelRequest<>(
                context,
                Request.Method.GET,
                url,
                KzPage.class,
                genHeaders(),
                null, new OnResponseModel() {

            @Override
            public void onResponseSuccess(Object model) {
                onResponseModel.onResponseSuccess((KzPage) model);
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                onResponseModel.onResponseError(error);
            }
        });
        CustomVolleyRequestQueue.getInstance().setRetryPolicy(request);
        CustomVolleyRequestQueue.getInstance().getRequestQueue().add(request);
    }

    public void postPaymentMethod(Context context, String token_card_id, final OnResponseModel onResponseModel) {
        String url = URI_PAYMENT_GATEWAY /*+ "/" + token_card_id*/ ;
        final JSONObject json = new JSONObject();
        try {
            json.put("token_card_id", token_card_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        CustomModelRequest request = new CustomModelRequest<JsonObject>(
                context,
                Request.Method.POST,
                url, JsonObject.class, genHeaders(), null, new OnResponseModel() {
            @Override
            public void onResponseSuccess(Object model) {
                onResponseModel.onResponseSuccess(model);
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                onResponseModel.onResponseError(error);
            }
        }){
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    byte[] bytes = json.toString().getBytes("utf-8");
                    return bytes;
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                return super.getBody();
            }
        };
        CustomVolleyRequestQueue.getInstance().setRetryPolicy(request);
        CustomVolleyRequestQueue.getInstance().getRequestQueue().add(request);
    }

    public void getPayments(Context context, final OnResponseModel<KZPaymentMethodsDetail> onResponseModel) {
        String url = URI_PAYMENT_GATEWAY;
        CustomModelRequest<KZPaymentMethodsDetail> request = new CustomModelRequest<>(
                context,
                Request.Method.GET,
                url,
                KZPaymentMethodsDetail.class,
                genHeaders(),
                null, new OnResponseModel<KZPaymentMethodsDetail>() {

            @Override
            public void onResponseSuccess(KZPaymentMethodsDetail model) {
                onResponseModel.onResponseSuccess(model);
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                onResponseModel.onResponseError(error);
            }
        });
        CustomVolleyRequestQueue.getInstance().setRetryPolicy(request);
        CustomVolleyRequestQueue.getInstance().getRequestQueue().add(request);
    }

    public void loadVehicleById(Context context, KZBranch branch, int vehicleID, Date startDate, Date endDate, KZBranch startBranch, KZBranch endBranch, final OnResponseModel<XModel> onResponseModel) {
        String date1 = new SimpleDateFormat("yyyy-MM-dd").format(startDate);
        String time1 = new SimpleDateFormat("HH:mm").format(startDate);
        String date2 = new SimpleDateFormat("yyyy-MM-dd").format(endDate);
        String time2 = new SimpleDateFormat("HH:mm").format(endDate);
        //int timezoneFromLocal = DateUtils.getTimeZoneOffset1();
        int timezoneStart = Integer.parseInt(startBranch.time_zone_offset);
        int timezoneEnd = Integer.parseInt(endBranch.time_zone_offset);

        String url = String.format(URI_GET_VEHICLE,vehicleID, branch.id, date1, time1, date2, time2, timezoneStart, timezoneEnd);
        CustomModelRequest<XModel> request = new CustomModelRequest<>(context, Request.Method.GET,
                url, XModel.class, genHeaders(), null, new OnResponseModel<XModel>() {
            @Override
            public void onResponseSuccess(XModel model) {
                onResponseModel.onResponseSuccess(model);
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                onResponseModel.onResponseError(error);
            }
        });
        CustomVolleyRequestQueue.getInstance().setRetryPolicy(request);
        CustomVolleyRequestQueue.getInstance().getRequestQueue().add(request);
    }

    public void callGoogleAPIServiceLocationInfo(Context context, String url, final OnResponseModel<JSONObject> onResponseModel){
        CustomModelRequest<JsonObject> request = new CustomModelRequest<>(context,
                Request.Method.GET,
                url, JsonObject.class, genHeaders(), null, new OnResponseModel() {
            @Override
            public void onResponseSuccess(Object model) {
                JsonObject object = (JsonObject) model;
                try {
                    JSONObject obj = new JSONObject(object.toString());
                    onResponseModel.onResponseSuccess(obj);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                onResponseModel.onResponseError(error);
            }
        });
        CustomVolleyRequestQueue.getInstance().setRetryPolicy(request);
        CustomVolleyRequestQueue.getInstance().getRequestQueue().add(request);
    }

    public void loadConciergeCost(Context context, String concierge_delivery_place, String concierge_collection_place, int branch_id, int drop_off_branch_id, int vevehicle_id, final OnResponseModel<Object> onResponseModel) {
        String url = URI_CONCIERGE_SERVICE_COST;
        final JSONObject json = new JSONObject();
        try {
            json.put("concierge_delivery_place", concierge_delivery_place);
            json.put("concierge_collection_place", concierge_collection_place);
            json.put("branch_id", branch_id);
            json.put("drop_off_branch_id", drop_off_branch_id);
            json.put("vehicle_id", vevehicle_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        CustomModelRequest<Object> request = new CustomModelRequest<Object>(
                context,
                Request.Method.POST,
                url, Object.class,
                genHeaders(),
                null,
                new OnResponseModel() {
                    @Override
                    public void onResponseSuccess(Object model) {
                        onResponseModel.onResponseSuccess(model);
                    }

                    @Override
                    public void onResponseError(BKNetworkResponseError error) {
                        onResponseModel.onResponseError(error);
                    }
                }) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    byte[] bytes = json.toString().getBytes("utf-8");
                    return bytes;
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                return super.getBody();
            }
        };
        CustomVolleyRequestQueue.getInstance().setRetryPolicy(request);
        CustomVolleyRequestQueue.getInstance().getRequestQueue().add(request);
    }

    public void loadAddOns(Context context, final OnResponseModel<JsonArray> onResponseModel) {
        String url = URI_ADD_ON;

        CustomModelRequest<JsonArray> request = new CustomModelRequest<>(
                context,
                Request.Method.GET,
                url, JsonArray.class, genHeaders(), null, new OnResponseModel<JsonArray>() {
            @Override
            public void onResponseSuccess(JsonArray model) {
                onResponseModel.onResponseSuccess(model);
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                onResponseModel.onResponseError(error);
            }
        });
        CustomVolleyRequestQueue.getInstance().setRetryPolicy(request);
        CustomVolleyRequestQueue.getInstance().getRequestQueue().add(request);
    }
}
