package com.keaz.landbird.utils;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by keaz on 10/19/17.
 */

public class PermissionUtils {
    public static boolean checkImportantPermission(Context context, String permission) {
        int locationPermission = ContextCompat.checkSelfPermission(context, permission);
        if (locationPermission != PackageManager.PERMISSION_GRANTED) {
            return false;
        }else {
            return true;
        }
    }

    public static boolean checkAndRequestPermissions(Activity context, String[] pArr, int code) {
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String s: pArr) {
            if(ContextCompat.checkSelfPermission(context, s) != PackageManager.PERMISSION_GRANTED){
                listPermissionsNeeded.add(s);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(context,
                    listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),
                    code);
            return false;
        }
        return true;
    }
}
