package com.keaz.landbird.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Hashtable;

/**
 * Created by mac on 6/18/16.
 */
public class FontUtil {
    private static Hashtable<String, Typeface> sTypeFaces = new Hashtable<String, Typeface>(
            10);

    public static Typeface linearFree(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/linearicons-free.ttf");
    }

    public static Typeface robotoLight(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/roboto-light.ttf");
    }

    public static Typeface robotoRegular(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/roboto-regular.ttf");
    }

    public static Typeface robotoThin(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/roboto-thin.ttf");
    }

    public static Typeface robotoBold(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/roboto-bold.ttf");
    }

    public static Typeface SF_Regular(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/SF-UI-Display-Regular.otf");
    }

    public static Typeface SF_Light(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/SF-UI-Display-Light.otf");
    }

    public static Typeface getTypeFace(Context mContext, String font) {
        Typeface tempTypeface = sTypeFaces.get(font);

        if (tempTypeface == null) {
            tempTypeface = Typeface.createFromAsset(mContext.getAssets(), font);
            sTypeFaces.put(font, tempTypeface);
        }
        return tempTypeface;
    }

    public static void setFont(Context mContext, String fontName, View view) {
        if (mContext != null && view != null) {
            if (view instanceof TextView) {
                ((TextView) view).setTypeface(getTypeFace(mContext, fontName));
                return;
            }
            if (view instanceof Button) {
                ((Button) view).setTypeface(getTypeFace(mContext, fontName));
                return;
            }
            if (view instanceof EditText) {
                ((EditText) view).setTypeface(getTypeFace(mContext, fontName));
                return;
            }
            if (view instanceof CheckBox) {
                ((CheckBox) view).setTypeface(getTypeFace(mContext, fontName));
                return;
            }
        }
    }
}
