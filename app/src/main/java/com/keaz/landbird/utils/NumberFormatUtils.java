package com.keaz.landbird.utils;

/**
 * Created by anhtran1810 on 2/8/18.
 */

public class NumberFormatUtils {

    public static String format(String s) {
        double d = Double.parseDouble(s);
        try{
            String rs = String.format("%.2f", d);
            return rs;
        }catch (Exception e){
            e.printStackTrace();
        }
        return "";
    }
}
