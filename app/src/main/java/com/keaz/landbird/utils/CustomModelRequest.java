package com.keaz.landbird.utils;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.JsonSyntaxException;

import java.io.UnsupportedEncodingException;
import java.util.Map;

/**
 * Created by Administrator on 16/2/2017.
 */

public class CustomModelRequest<T> extends Request<T> {

    private static final String TAG = CustomModelRequest.class.getSimpleName();

    private Context mContext;
    private Class<T> tClass;
    private OnResponseModel<T> responseModel;
    private Map<String, String> headers;
    private Map<String, String> params;

    public CustomModelRequest(Context context, int method, String url, Class<T> tClass,
                              Map<String, String> headers,
                              Map<String, String> params,
                              OnResponseModel responseModel) {
        super(method, url, null);

        this.mContext = context;
        this.tClass = tClass;
        this.responseModel = responseModel;
        this.params = params;
        this.headers = headers;
        MyLog.debug(TAG, "REQUEST: " + url);
    }

    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {
        try {
            String json = new String(
                    response.data,
                    HttpHeaderParser.parseCharset(response.headers));
            T clazz = JsonUtils.parseJsonObject(tClass, json);
            return Response.success(clazz,
                    HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JsonSyntaxException e) {
            return Response.error(new ParseError(e));
        }
    }

    @Override
    protected void deliverResponse(T response) {
        responseModel.onResponseSuccess(response);
    }

    @Override
    public void deliverError(VolleyError error) {
        responseModel.onResponseError(VolleyUtils.getSharedNetwork().handleVolleyError(mContext, error));
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        return headers != null ? headers : super.getHeaders();
    }

    @Override
    protected Map<String, String> getParams() throws AuthFailureError {
        return params != null ? params : super.getParams();
    }
}
