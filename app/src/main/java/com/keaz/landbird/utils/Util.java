package com.keaz.landbird.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.text.format.DateUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.Surface;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import com.acuant.mobilesdk.util.Utils;
import com.google.gson.Gson;
import com.keaz.landbird.R;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by Tam Tran on 3/30/2015.
 */
public class Util {

    public static final String DATE_FORMAT_1 = "yyyy-MM-dd'T'HH:mm:ss";
    public static final String DATE_FORMAT_2 = "yyyy-MM-dd'T'HH:mm:ss'Z'";

    private static final String TAG = Util.class.getName();

    /**
     * to show the debugs logs or not. Set it to false in production.
     */
    public static final boolean LOG_ENABLED = true;

    public static int DEVICE_WIDTH;
    public static int DEVICE_HEIGHT;

    public static boolean isNetworkAvailable(Context c) {
        ConnectivityManager cm = (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting())
        {
            return true;
        }
        return false;
    }

    public static int normalizePixel(Context context,int pixel) {
        if (pixel == 0) return 0;
        int mHourWidth = Util.convertDp2Px(context, 80);
        int sectorWidth = mHourWidth / 4;
        int temp = pixel / sectorWidth;
        return temp * sectorWidth;
    }

    public static int getResourceValue(Context context, String resName, String resType) {
        int resourceValue = context.getResources().getIdentifier(resName, resType,
                context.getPackageName());
        return resourceValue;
    }

    public static String getPackageName(Context context) {
        String packageName = context.getPackageName();
        return packageName;
    }

    public static int convertPx2Dp(Context context, int px) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px / (metrics.densityDpi / 160f);
        return (int) dp;
    }

    public static int convertDp2Px(Context context, int dp) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return (int) px;
    }

    public static boolean isLandscape(Context context) {
        return context.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE;
    }

    public static void hideKeyboard(Context mContext) {
        InputMethodManager inputManager = (InputMethodManager) mContext
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        // check if no view has focus:
        View view = ((Activity) mContext).getCurrentFocus();
        if (view != null) {
            inputManager.hideSoftInputFromWindow(view.getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    public static String getStringFromHTMLFile(InputStream stream, Context context) {
        String html = "";
        try {
            // TODO Auto-generated method stub

            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(stream, "UTF-8"));

            // do reading, usually loop until end of file reading
            String mLine = reader.readLine();

            while (mLine != null) {
                // process line
                mLine = reader.readLine();
                if (mLine != null)
                    html += mLine;
            }

            reader.close();
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
//        html = html.replace(
//                context.getResources().getString(R.string.terms_privacy_special), "&quot;");
        return html;
    }

    public static boolean isLollipop() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;
    }

    public static void getScreenSize(Context context) {
        Display display = ((WindowManager) context
                .getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();

        DisplayMetrics metrics = new DisplayMetrics();
        display.getMetrics(metrics);
        int widthPixels = metrics.widthPixels;
        int heightPixels = metrics.heightPixels;

        if (Build.VERSION.SDK_INT >= 14 && Build.VERSION.SDK_INT < 17) {
            try {
                widthPixels = (Integer) Display.class.getMethod("getWidth")
                        .invoke(display);
                heightPixels = (Integer) Display.class.getMethod("getHeight")
                        .invoke(display);

            } catch (Exception ignored) {
                ignored.printStackTrace();
            }
        }
        // includes window decorations (statusbar bar/menu bar)
        else if (Build.VERSION.SDK_INT >= 17) {
            try {
                Point realSize = new Point();
                Display.class.getMethod("getSize", Point.class).invoke(display,
                        realSize);
                widthPixels = realSize.x;
                heightPixels = realSize.y;
            } catch (Exception ignored) {
                ignored.printStackTrace();
            }
        } else {
            widthPixels = display.getWidth();
            heightPixels = display.getHeight();
        }
//		Log.d(TAG, "Device width : " + widthPixels);
//		Log.d(TAG, "Device height : " + heightPixels);
        DEVICE_WIDTH = widthPixels;
        DEVICE_HEIGHT = heightPixels;

    }


    public static void setLocalization(Context context) {
        Configuration config = new Configuration(context.getResources().getConfiguration());
        config.locale = Locale.FRANCE;
//        config.locale = Locale.UK ;
        context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
    }

    /*CHECK DENSITY OF DEVICE*/
    public static double getDensityDevice(Context context) {
        // return 0.75 if it's LDPI
        // return 1.0 if it's MDPI
        // return 1.5 if it's HDPI
        // return 2.0 if it's XHDPI
        // return 1.33 if it's TV-HDPI
        double density = 0;
        switch (context.getResources().getDisplayMetrics().densityDpi) {
            case DisplayMetrics.DENSITY_LOW:
                density = 0.75;
                break;
            case DisplayMetrics.DENSITY_MEDIUM:
                density = 1;
                break;
            case DisplayMetrics.DENSITY_HIGH:
                density = 1.5;
                break;
            case DisplayMetrics.DENSITY_XHIGH:
                density = 2;
                break;
            case DisplayMetrics.DENSITY_XXHIGH:
                density = 3;
                break;
            default:
                density = 1.33;
                break;
        }
        return density;
    }

//    public static boolean isCheckedCategory(Context ctx) {
//        SharedPreferences pre = ctx.getSharedPreferences("check_category_one_time", ctx.MODE_PRIVATE);
//        return pre.getBoolean("Checked", false);
//    }
//
//    public static void unCheckedCategory(Context ctx) {
//        SharedPreferences pre = ctx.getSharedPreferences("check_category_one_time", ctx.MODE_PRIVATE);
//        SharedPreferences.Editor edit = pre.edit();
//        edit.putBoolean("Checked", false);
//        edit.commit();
//    }

    public static Boolean isActivityRunning(Class activityClass, Context context) {
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> tasks = activityManager.getRunningTasks(Integer.MAX_VALUE);

        for (ActivityManager.RunningTaskInfo task : tasks) {
            if (activityClass.getCanonicalName().equalsIgnoreCase(task.baseActivity.getClassName()))
                return true;
        }

        return false;
    }

//    // TODO: Apply rules for my interests
//    public static ArrayList<Issue> applyRuleForFrontStore(ArrayList<Issue> issues) {
//        ArrayList<Issue> iss = filterOneIssuePerPublication(issues);
//        if (iss.size() > 0) {
//            Collections.sort(iss, new SortTopSalesUtil());
//        }
//        return iss;
//    }

//    // TODO: Filter one issue per publication
//    private static ArrayList<Issue> filterOneIssuePerPublication(ArrayList<Issue> issues) {
//
//        ArrayList<Issue> results = new ArrayList<Issue>();
//        ArrayList<Issue> tmp = new ArrayList<Issue>();
//        // Store indicators of the same issues
//        ArrayList<Integer> indicators = new ArrayList<Integer>();
//
//        while (issues.size() > 0) {
//            for (int i = 0; i < issues.size(); i++) {
//                // Compare publication name
//                if (issues.get(0).getPublication().getName().equals(issues.get(i).getPublication().getName())) {
//                    tmp.add(issues.get(i));
//                    indicators.add(i);
//                }
//            }
//            // Add issue to results
//            Collections.sort(tmp, new SortCoverDateUtil());
//            results.add(tmp.get(0));
//            // Remove the same issues from list
//            for (int k = indicators.size() - 1; k >= 0; k--) {
//                issues.remove(indicators.get(k).intValue());
//            }
//            // Reset indicators, tmp
//            indicators.clear();
//            tmp.clear();
//        }
//        return results;
//    }

    public static String convertObject(Object object) {
        Gson gson = new Gson();
        String json = gson.toJson(object);
        return json;
    }

    public static int getStatusBarHeight(Context context) {
        int result = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    @SuppressLint("NewApi")
    public static int getSoftbuttonsbarHeight(Activity context) {
        // getRealMetrics is only available with API 17 and +
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            DisplayMetrics metrics = new DisplayMetrics();
            context.getWindowManager().getDefaultDisplay().getMetrics(metrics);
            int usableHeight = metrics.heightPixels;
            context.getWindowManager().getDefaultDisplay().getRealMetrics(metrics);
            int realHeight = metrics.heightPixels;
            if (realHeight > usableHeight)
                return realHeight - usableHeight;
            else
                return 0;
        }
        return 0;
    }

    public static long getMilisecondsFromDateString(String dateString, String format) {
        try {
            SimpleDateFormat utc = new SimpleDateFormat(format);
            utc.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date d = utc.parse(dateString);
            return d.getTime();
        } catch (Exception ex) {
            return 0;
        }
    }

    public static long getCurrentTimeStampInMiliseconds() {
        return new Date().getTime();
    }

    public static String getRelativeTime(String date, String format) {
        return DateUtils.getRelativeTimeSpanString(getMilisecondsFromDateString(date, format), getCurrentTimeStampInMiliseconds(), DateUtils.SECOND_IN_MILLIS).toString();
    }

    public static String convertLongtoDate(long val) {
        Calendar c = Calendar.getInstance();
        try {
            c.setTimeInMillis(val * 1000l);
            Date d = c.getTime();
            SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy");
            String time = format.format(d);//this variable time contains the time in the format of "day/month/year".
            return time;
        } catch (Exception ex) {
            Date d = c.getTime();
            c.setTimeInMillis(d.getTime() * 1000);
            SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy");
            String time = format.format(d);//this variable time contains the time in the format of "day/month/year".
            return time;
        }
    }

    /**
     *
     * @return An ActivityInfo.SCREEN_ORIENTATION_ .. something, indicating the current orientation
     */
    public static int getScreenOrientation(Activity activity)
    {

        if (activity.getRequestedOrientation() != ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED)
        {
            return activity.getRequestedOrientation();
        }

        int retVal;
        int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
        DisplayMetrics dm = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width = dm.widthPixels;
        int height = dm.heightPixels;


        // if the device's natural orientation is portrait:
        if (((rotation == Surface.ROTATION_0 || rotation == Surface.ROTATION_180) && height > width)
                ||
                ((rotation == Surface.ROTATION_90 || rotation == Surface.ROTATION_270) && width > height))
        {

            switch (rotation)
            {
                case Surface.ROTATION_0:
                    retVal = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
                    break;
                case Surface.ROTATION_90:
                    retVal = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
                    break;
                case Surface.ROTATION_180:
                    retVal = ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT;
                    break;
                case Surface.ROTATION_270:
                    retVal = ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE;
                    break;
                default:
                    Utils.appendLog(TAG, "Unknown screen orientation. Defaulting to " + "portrait.");
                    retVal = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
                    break;
            }
        }
        // if the device's natural orientation is landscape or if the device
        // is square:
        else
        {
            switch (rotation)
            {
                case Surface.ROTATION_0:
                    retVal = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
                    break;
                case Surface.ROTATION_90:
                    retVal = ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT;
                    break;
                case Surface.ROTATION_180:
                    retVal = ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE;
                    break;
                case Surface.ROTATION_270:

                    retVal = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
                    break;
                default:
                    Utils.appendLog(TAG, "Unknown screen orientation. Defaulting to " + "landscape.");
                    retVal = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
                    break;
            }
        }

        return retVal;
    }

    /**
     *
     * @param context
     * @param message
     */
    public static AlertDialog showDialog(final Activity context, String message)
    {
        Util.lockScreen(context);

        DialogInterface.OnClickListener clickListener = new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                Util.dismissDialog((Dialog) dialog);

                Util.unLockScreen(context);
            }
        };
        return showDialog(context, message, clickListener);
    }

    /**
     * @param context
     * @param message
     * @param clickListener
     * @return
     */
    public static AlertDialog showDialog(Activity context, String message, DialogInterface.OnClickListener clickListener)
    {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, context.getString(R.string.btn_okie), clickListener);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();

        return alertDialog;
    }

    /**
     * Encapsulated behavior for dismissing Dialogs, because of several android problems
     * related.
     */
    public static void dismissDialog(Dialog dialog)
    {
        if (dialog != null && dialog.isShowing())
        {
            try
            {
                dialog.setCancelable(true);
                dialog.dismiss();
            } catch (IllegalArgumentException e) // even sometimes happens?: http://stackoverflow.com/questions/12533677/illegalargumentexception-when-dismissing-dialog-in-async-task
            {
                Log.i(TAG, "Error when attempting to dismiss dialog, it is an android problem.", e);
            }
        }
    }

    /**
     * blocks the change screen orientation android feature.
     */
    public static void lockScreen(Activity activity)
    {
        if (isTablet(activity)) // with tablet the screen rotates, block it.
        {
            activity.setRequestedOrientation(getScreenOrientation(activity));
        }
    }

    /**
     *
     */
    public static void unLockScreen(Activity activity)
    {
        if (isTablet(activity)) // with tablet the screen rotates, unblock it.
        {
            activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
        }
    }

    public static boolean isTablet(Activity activity) {
        return activity.getResources().getBoolean(R.bool.isTablet);
    }

    /**
     * @param context
     * @param message
     * @return
     */
    public static ProgressDialog showProgessDialog(Activity context, String message)
    {
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(message);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.show();

        return progressDialog;
    }

    /**
     *
     * @param bitmap
     * @param context
     * @return
     */
    public static Bitmap getRoundedCornerBitmap(Bitmap bitmap, Context context)
    {
        if (bitmap == null)
        {
            return null;
        }

        //dpi corner circle radius
        int cornerCircleRadiusDpi = 20;

        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);
        final float roundPx = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, (float) cornerCircleRadiusDpi, context.getResources()
                .getDisplayMetrics());

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

        paint.setXfermode(new PorterDuffXfermode(android.graphics.PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return output;
    }

    public static String getInMMddyyFormat(int year,int month,int day){
        String retString = null;
        Date date = new Date(year, month, day);
        SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yy");
        retString = sdf.format(date);
        return retString;
    }

    public static int get4DigitYear(int year,int month,int day){
        Date date = new Date(year, month, day);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        int ret_year = Integer.parseInt(sdf.format(date).split("/")[0]);
        return ret_year;
    }
}
