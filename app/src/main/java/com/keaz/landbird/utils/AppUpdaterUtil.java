package com.keaz.landbird.utils;

import android.content.Context;

import com.github.javiersantos.appupdater.AppUpdaterUtils;

public class AppUpdaterUtil {
    public static void start(Context context, AppUpdaterUtils.UpdateListener listener) {
        /*AppUpdater appUpdater = new AppUpdater(context)
                .setUpdateFrom(UpdateFrom.GOOGLE_PLAY)
                .setDisplay(Display.DIALOG)
                .setCancelable(true)
                .setTitleOnUpdateAvailable("Update available")
                .setContentOnUpdateAvailable("Check out the latest version available of my app!")
                .setTitleOnUpdateNotAvailable("Update not available")
                .setContentOnUpdateNotAvailable("No update available. Check for updates again later!")
                .setButtonUpdate("Update now?")
                .setButtonUpdateClickListener(new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                })
	            .setButtonDismiss("Maybe later")
                .setButtonDismissClickListener(new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                })
	            .setButtonDoNotShowAgain("Huh, not interested")
                .setButtonDoNotShowAgainClickListener(new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                })
	            //.setIcon(R.drawable.ic_update) // Notification icon
                .setCancelable(false) ;
        appUpdater.start();*/


        AppUpdaterUtils appUpdaterUtils = new AppUpdaterUtils(context)
                //.setUpdateFrom(UpdateFrom.AMAZON)
                //.setUpdateFrom(UpdateFrom.GITHUB)
                //.setGitHubUserAndRepo("javiersantos", "AppUpdater")
                //...
                .withListener(listener);
        appUpdaterUtils.start();
    }
}
