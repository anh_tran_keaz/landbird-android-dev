package com.keaz.landbird.utils;

import com.keaz.landbird.models.KZObject;

/**
 * Created by Administrator on 16/2/2017.
 */

public interface OnNetworkResponse {
    public void onResponseSuccess(KZObject object);

    public void onResponseError(BKNetworkResponseError error);
}

