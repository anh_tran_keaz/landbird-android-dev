package com.keaz.landbird.utils;

/*
 * Created by tapasbehera on 5/11/16.
 */
public interface ConfirmationListener {

    public void confirmed();
    public void retry();
    public void tryNewWay();
}
