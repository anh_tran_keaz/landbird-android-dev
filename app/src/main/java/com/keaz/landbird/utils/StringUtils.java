package com.keaz.landbird.utils;

import android.os.Build;
import android.util.Base64;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Thinh Lai on 6/10/15.
 * Copyright (c) 2015 Thinh Lai. All rights reserved.
 */
public class StringUtils {

    private static final Pattern urlPattern = Pattern.compile(
            "(?:^|[\\W])((ht|f)tp(s?):\\/\\/|www\\.)"
                    + "(([\\w\\-]+\\.){1,}?([\\w\\-.~]+\\/?)*"
                    + "[\\p{Alnum}.,%_=?&#\\-+()\\[\\]\\*$~@!:/{};']*)",
            Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);

    public static String md5(String inputString) {
        try {
            MessageDigest digest = MessageDigest
                    .getInstance("MD5");
            digest.update(inputString.getBytes());
            byte messageDigest[] = digest.digest();
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < messageDigest.length; i++) {
                String h = Integer.toHexString(0xFF & messageDigest[i]);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String encodeBase64(String inputString) {
        try {
            return Base64.encodeToString(inputString.getBytes("UTF-8"), Base64.DEFAULT);
        } catch (Exception ex) {
            return "";
        }
    }

    public static String decodeBase64(String base64) throws UnsupportedEncodingException {
        byte[] data = Base64.decode(base64, Base64.DEFAULT);
        String text = new String(data, "UTF-8");
        return text;
    }

    public static boolean isEmailValid(String email) {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public static boolean isYoutubeLink(String link) {
        return (link.contains("youtube.com/") || link.contains("youtu.be/"));
    }

//    public static void removeUnderlines(Spannable p_Text) {
//        URLSpan[] spans = p_Text.getSpans(0, p_Text.length(), URLSpan.class);
//
//        for(URLSpan span:spans) {
//            int start = p_Text.getSpanStart(span);
//            int end = p_Text.getSpanEnd(span);
//            p_Text.removeSpan(span);
//            span = new URLSpanNoUnderline(span.getURL());
//            p_Text.setSpan(span, start, end, 0);
//        }
//    }

    public static boolean isEmptyString(String text) {
        if (text != null && !text.trim().equalsIgnoreCase(""))
            return false;
        else
            return true;
    }

    public static String getURLFromString(String text) {
        String url = "";
        Matcher matcher = urlPattern.matcher(text);
        while (matcher.find()) {
            int matchStart = matcher.start(1);
            int matchEnd = matcher.end();
            url = text.substring(matchStart, matchEnd);
            return url;
        }
        return url;
    }

    public static String replaceURLWithHTMLFormat(String text) {
        if (text == null)
            return "";
        String result = text;
        result = result.replaceAll("(\r\n|\n)", "<br />");
        Matcher matcher = urlPattern.matcher(text);
        ArrayList<String> arrayList = new ArrayList<String>();
        while (matcher.find()) {
            int matchStart = matcher.start(1);
            int matchEnd = matcher.end();
            String url = text.substring(matchStart, matchEnd);
            arrayList.add(url);
        }
        if (arrayList != null && arrayList.size() > 0) {
            for (String url : arrayList) {
                result.replaceAll(url, "<a href=\"" + url + "\">" + url + "</a>");
            }
        }
        return result;
    }

    public static boolean isValidWebsite(String website) {
        Matcher matcher = urlPattern.matcher(website);
        if (matcher.matches()) {
            return true;
        } else {
            return false;
        }
    }

    public static String getHost(String url) {
        try {
            URI uri = new URI(url);
            String domain = uri.getHost();
            return domain.startsWith("www.") ? domain.substring(4) : domain;
        } catch (Exception e) {
            e.printStackTrace();
            return url;
        }
    }

    public static boolean isGifFile(String url) {
        boolean isGif = false;
        if (url != null && url.length() > 4) {
            if (url.toUpperCase().endsWith(".GIF")) {
                isGif = true;
            }
        }
        return isGif;
    }

    public static String getYoutubeId(String url) {
        String pattern = "(?<=watch\\?v=|/videos/|embed\\/|youtu.be\\/|\\/v\\/|\\/e\\/|watch\\?v%3D|watch\\?feature=player_embedded&v=|%2Fvideos%2F|embed%\u200C\u200B2F|youtu.be%2F|%2Fv%2F)[^#\\&\\?\\n]*";
        Pattern compiledPattern = Pattern.compile(pattern);
        Matcher matcher = compiledPattern.matcher(url);
        if (matcher.find()) {
            return matcher.group();
        }
        return url;
    }

    public static String getDeviceName() {
        try {
            String manufacturer = Build.MANUFACTURER;
            String model = Build.MODEL;
            if (model.startsWith(manufacturer)) {
                return capitalize(model);
            } else {
                return capitalize(manufacturer) + " " + model;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "Android Unknown";
        }
    }


    private static String capitalize(String s) throws Exception {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }
}
