package com.keaz.landbird.utils;

import java.util.Calendar;
import java.util.Date;

public class MakeBookingUtil {

    public static Calendar adjustToTimeWithFromTimeFollowConfigDuration(int minDurationOfBookingInMinute, Calendar mSelectedDateFrom) {
        Date mSelectedDateTo = new Date();
        mSelectedDateTo.setTime(mSelectedDateFrom.getTimeInMillis());
        Calendar mSelectedCalendarTo = Calendar.getInstance();
        mSelectedCalendarTo.setTimeInMillis(mSelectedDateTo.getTime());
        mSelectedCalendarTo.set(Calendar.MINUTE, mSelectedCalendarTo.get(Calendar.MINUTE) + minDurationOfBookingInMinute);
        mSelectedDateTo = mSelectedCalendarTo.getTime();
        return mSelectedCalendarTo;
    }
}
