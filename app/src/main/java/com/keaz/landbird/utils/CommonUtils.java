package com.keaz.landbird.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.keaz.landbird.R;
import com.keaz.landbird.activities.ApprovedBookingActivity;
import com.keaz.landbird.activities.BookingListActivity;
import com.keaz.landbird.activities.MainActivity;
import com.keaz.landbird.interfaces.CustomListener2;
import com.keaz.landbird.models.KZAccount;
import com.keaz.landbird.models.KZBooking;
import com.keaz.landbird.models.KZBranch;
import com.keaz.landbird.models.KZMembership;
import com.keaz.landbird.models.KZObjectForMixpanel;
import com.keaz.landbird.models.UserModel;
import com.segment.analytics.Analytics;
import com.segment.analytics.Properties;
import com.segment.analytics.Traits;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/*
 * Created by anhtran1810 on 5/11/17.
 */

public class CommonUtils {

    public static final int CODE_UPDATE_BOOKING = 999;

    public static void handleOpenBooking(Context context, String statusBookingOnDevice, KZBooking bookingOnServer) {
        if (bookingOnServer.state.equalsIgnoreCase(BKGlobals.BookingStatus.APPROVED.toString())) {
            Intent intent = new Intent(context, ApprovedBookingActivity.class);
            ApprovedBookingActivity.booking = bookingOnServer;
            ((Activity) context).startActivityForResult(intent, CODE_UPDATE_BOOKING);
            ((Activity) context).setResult(Activity.RESULT_OK);
            ((Activity) context).finish();

        } else if (bookingOnServer.state.equalsIgnoreCase(BKGlobals.BookingStatus.INUSE.toString())) {

            Intent intent = new Intent(context, MainActivity.class);
            intent.putExtra(ConstantsUtils.INTENT_EXTRA_TAB, 2);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            context.startActivity(intent);
            ((Activity) context).finish();

        } else {
            Intent intent = new Intent(context, BookingListActivity.class);
            ((Activity) context).startActivity(intent);
            ((Activity) context).setResult(Activity.RESULT_OK);
            ((Activity) context).finish();
        }
    }

    public static void uploadDeviceId(final Context context, final CustomListener2 customListener) {
        String deviceId = BKGlobals.getSharedGlobals().getStringPreferences(ConstantsUtils.PREF_KEY_DEVICE_ID, "");

        if (deviceId == null || deviceId.equals("")) {
            Toast.makeText(context, context.getString(R.string.error_no_device_id_found), Toast.LENGTH_SHORT).show();
            if(customListener!=null) customListener.fail(null);
            return;
        }

        VolleyUtils.getSharedNetwork().uploadDeviceId(context, deviceId, new OnResponseModel<JsonObject>() {
            @Override
            public void onResponseSuccess(JsonObject object) {
                if(customListener!=null) customListener.success();
                Log.d("deviceID","Device ID uploaded");
            }

            @Override
            public void onResponseError(BKNetworkResponseError error) {
                VolleyUtils.handleErrorRespond(context, error);
                if(customListener!=null) customListener.fail(error);
                Log.d("deviceID","Device ID can not be uploaded");
            }
        });
    }

    public static void segmentIdentifyWithUser(Context context, UserModel user) {
        if (user == null) {
            Analytics.with(context).reset();
            return;
        }

        String id = user.id == 0 ? null : ("" + user.id);
        String email = user.email == null ? "" : user.email;
        String phone = user.phone == null ? "" : user.phone;
        String firstName = user.name == null ? "" : user.name;
        String lastName = user.last_name == null ? "" : user.last_name;
        String name = "";
        if (!firstName.equals("") || !lastName.equals("")) {
            name = firstName + " " + lastName;
        }

        Analytics.with(context).identify(id, new Traits()
                .putValue(ConstantsUtils.MIXPANEL_NAME,name)
                .putValue(ConstantsUtils.MIXPANEL_EMAIL,email)
                .putValue(ConstantsUtils.MIXPANEL_FISTNAME,firstName)
                .putValue(ConstantsUtils.MIXPANEL_LASTNAME,lastName)
                .putValue(ConstantsUtils.MIXPANEL_PHONE,phone), null);
    }

    public static void segmentIdentifyWithDefaultUser(Context context) {
        UserModel userModel = KZAccount.getSharedAccount().user;
        segmentIdentifyWithUser(context, userModel);
    }

    public static void segmentTrackWithBranch(Context context, String title, KZBranch branch) {
        Analytics.with(context).track(title,
                new Properties()
                        .putValue(ConstantsUtils.MIXPANEL_BRANCH_ID, "" + branch.id)
                        .putValue(ConstantsUtils.MIXPANEL_BRANCH_NAME, "" + branch.name));
    }

    public static void segmentScreenkWithTime(Context context, String title, KZBranch branch, Date start, Date end) {
        String startDateStr = new SimpleDateFormat(ConstantsUtils.FORMAT_SEGMENT_DATE, Locale.getDefault()).format(start);
        String startTimeStr = new SimpleDateFormat(ConstantsUtils.FORMAT_SEGMENT_TIME, Locale.getDefault()).format(start);
        String endDateStr = new SimpleDateFormat(ConstantsUtils.FORMAT_SEGMENT_DATE, Locale.getDefault()).format(end);
        String endTimeStr = new SimpleDateFormat(ConstantsUtils.FORMAT_SEGMENT_TIME, Locale.getDefault()).format(end);

        Analytics.with(context).screen(title,
                new Properties()
                        .putValue(ConstantsUtils.MIXPANEL_BRANCH_ID, "" + branch.id)
                        .putValue(ConstantsUtils.MIXPANEL_BRANCH_NAME, "" + branch.name)
                        .putValue(ConstantsUtils.MIXPANEL_START_DATE, startDateStr)
                        .putValue(ConstantsUtils.MIXPANEL_START_TIME, startTimeStr)
                        .putValue(ConstantsUtils.MIXPANEL_END_DATE, endDateStr)
                        .putValue(ConstantsUtils.MIXPANEL_END_TIME, endTimeStr)
        );
    }

    public static void segmentTrackWithStartDate(Context context, String title, KZBranch branch, Date start) {
        String dateFormatForSegment = new SimpleDateFormat(ConstantsUtils.FORMAT_SEGMENT_DATE).format(start).toString();

        Analytics.with(context).track(title,
                new Properties()
                        .putValue(ConstantsUtils.MIXPANEL_BRANCH_ID, "" + branch.id)
                        .putValue(ConstantsUtils.MIXPANEL_BRANCH_NAME, "" + branch.name)
                        .putValue(ConstantsUtils.MIXPANEL_START_DATE, dateFormatForSegment)
        );
    }

    public static void segmentTrackWithStartTime(Context context, String title, KZBranch branch, Date start) {
        String dateFormatForSegment = new SimpleDateFormat(ConstantsUtils.FORMAT_SEGMENT_DATE).format(start).toString();
        String timeFormatForSegment = new SimpleDateFormat(ConstantsUtils.FORMAT_SEGMENT_TIME).format(start).toString();

        Analytics.with(context).track(title,
                new Properties()
                        .putValue(ConstantsUtils.MIXPANEL_BRANCH_ID, "" + branch.id)
                        .putValue(ConstantsUtils.MIXPANEL_BRANCH_NAME, "" + branch.name)
                        .putValue(ConstantsUtils.MIXPANEL_START_DATE, dateFormatForSegment)
                        .putValue(ConstantsUtils.MIXPANEL_START_TIME, timeFormatForSegment)
        );
    }

    public static void segmentScreenWithBooking(Context context, String title, KZBooking booking) {

        long startWithTimezone = booking.actual_start - DateUtils.getTimeZoneOffset() - booking.start_timezone_offset;
        long endWithTimezone = booking.actual_end - DateUtils.getTimeZoneOffset() - booking.end_timezone_offset;

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(startWithTimezone * 1000);
        Date startDate = calendar.getTime();

        calendar.setTimeInMillis(endWithTimezone * 1000);
        Date endDate = calendar.getTime();

        String startDateStr = new SimpleDateFormat(ConstantsUtils.FORMAT_SEGMENT_DATE, Locale.getDefault()).format(startDate);
        String startTimeStr = new SimpleDateFormat(ConstantsUtils.FORMAT_SEGMENT_TIME, Locale.getDefault()).format(startDate);
        String endDateStr = new SimpleDateFormat(ConstantsUtils.FORMAT_SEGMENT_DATE, Locale.getDefault()).format(endDate);
        String endTimeStr = new SimpleDateFormat(ConstantsUtils.FORMAT_SEGMENT_TIME, Locale.getDefault()).format(endDate);

        Analytics.with(context).screen(title,
                new Properties()
                        .putValue(ConstantsUtils.MIXPANEL_BRANCH_ID, "" + booking.branch.id)
                        .putValue(ConstantsUtils.MIXPANEL_BRANCH_NAME, "" + booking.branch.name)
                        .putValue(ConstantsUtils.MIXPANEL_START_DATE, startDateStr)
                        .putValue(ConstantsUtils.MIXPANEL_START_TIME, startTimeStr)
                        .putValue(ConstantsUtils.MIXPANEL_END_DATE, endDateStr)
                        .putValue(ConstantsUtils.MIXPANEL_END_TIME, endTimeStr)
                        .putValue(ConstantsUtils.MIXPANEL_VEHICLE_ID, "" + booking.vehicle.id)
                        .putValue(ConstantsUtils.MIXPANEL_BOOKING_ID, "" + booking.id)
                        .putValue(ConstantsUtils.MIXPANEL_BOOKING_STATUS, booking.state)
        );
    }

    public static void segmentTrackWithBooking(Context context, String title, KZBooking booking) {

        long startWithTimezone = booking.actual_start - DateUtils.getTimeZoneOffset() - booking.start_timezone_offset;
        long endWithTimezone = booking.actual_end - DateUtils.getTimeZoneOffset() - booking.end_timezone_offset;

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(startWithTimezone * 1000);
        Date startDate = calendar.getTime();

        calendar.setTimeInMillis(endWithTimezone * 1000);
        Date endDate = calendar.getTime();

        String startDateStr = new SimpleDateFormat(ConstantsUtils.FORMAT_SEGMENT_DATE, Locale.getDefault()).format(startDate);
        String startTimeStr = new SimpleDateFormat(ConstantsUtils.FORMAT_SEGMENT_TIME, Locale.getDefault()).format(startDate);
        String endDateStr = new SimpleDateFormat(ConstantsUtils.FORMAT_SEGMENT_DATE, Locale.getDefault()).format(endDate);
        String endTimeStr = new SimpleDateFormat(ConstantsUtils.FORMAT_SEGMENT_TIME, Locale.getDefault()).format(endDate);

        Analytics.with(context).track(title,
                new Properties()
                        .putValue(ConstantsUtils.MIXPANEL_BRANCH_ID, "" + booking.branch.id)
                        .putValue(ConstantsUtils.MIXPANEL_BRANCH_NAME, "" + booking.branch.name)
                        .putValue(ConstantsUtils.MIXPANEL_START_DATE, startDateStr)
                        .putValue(ConstantsUtils.MIXPANEL_START_TIME, startTimeStr)
                        .putValue(ConstantsUtils.MIXPANEL_END_DATE, endDateStr)
                        .putValue(ConstantsUtils.MIXPANEL_END_TIME, endTimeStr)
                        .putValue(ConstantsUtils.MIXPANEL_VEHICLE_ID, "" + booking.vehicle.id)
                        .putValue(ConstantsUtils.MIXPANEL_BOOKING_ID, "" + booking.id)
                        .putValue(ConstantsUtils.MIXPANEL_BOOKING_STATUS, booking.state)
        );
    }

    public static void segmentScreenWithBookingAndRevenue(Context context, String title, KZBooking booking) {

        long startWithTimezone = booking.actual_start - DateUtils.getTimeZoneOffset() - booking.start_timezone_offset;
        long endWithTimezone = booking.actual_end - DateUtils.getTimeZoneOffset() - booking.end_timezone_offset;

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(startWithTimezone * 1000);
        Date startDate = calendar.getTime();

        calendar.setTimeInMillis(endWithTimezone * 1000);
        Date endDate = calendar.getTime();

        String startDateStr = new SimpleDateFormat(ConstantsUtils.FORMAT_SEGMENT_DATE, Locale.getDefault()).format(startDate);
        String startTimeStr = new SimpleDateFormat(ConstantsUtils.FORMAT_SEGMENT_TIME, Locale.getDefault()).format(startDate);
        String endDateStr = new SimpleDateFormat(ConstantsUtils.FORMAT_SEGMENT_DATE, Locale.getDefault()).format(endDate);
        String endTimeStr = new SimpleDateFormat(ConstantsUtils.FORMAT_SEGMENT_TIME, Locale.getDefault()).format(endDate);

        Analytics.with(context).screen(title,
                new Properties()
                        .putValue(ConstantsUtils.MIXPANEL_BRANCH_ID, "" + booking.branch.id)
                        .putValue(ConstantsUtils.MIXPANEL_BRANCH_NAME, "" + booking.branch.name)
                        .putValue(ConstantsUtils.MIXPANEL_START_DATE, startDateStr)
                        .putValue(ConstantsUtils.MIXPANEL_START_TIME, startTimeStr)
                        .putValue(ConstantsUtils.MIXPANEL_END_DATE, endDateStr)
                        .putValue(ConstantsUtils.MIXPANEL_END_TIME, endTimeStr)
                        .putValue(ConstantsUtils.MIXPANEL_VEHICLE_ID, "" + booking.vehicle.id)
                        .putValue(ConstantsUtils.MIXPANEL_BOOKING_ID, "" + booking.id)
                        .putValue(ConstantsUtils.MIXPANEL_BOOKING_STATUS, booking.state)
                        .putValue(ConstantsUtils.MIXPANEL_REVENUE, booking.cost_charged)
        );
    }

    public static void segmentTrackWithMembership(Context context, String title, KZMembership membership) {
        Analytics.with(context).track(title,
                new Properties()
                        .putValue(ConstantsUtils.MIXPANEL_MEMBERSHIP_ID, "" + membership.id)
                        .putValue(ConstantsUtils.MIXPANEL_MEMBERSHIP_NAME, "" + membership.name)
        );
    }

    public static void segmentIdentifyWithTotalBookingCount(Context context, String totalBookingCount) {
        Analytics.with(context).identify(null, new Traits()
                .putValue("total_booking_count", totalBookingCount), null);
    }

    public static void segmentIdentifyWithLastCompletedBookingDate(Context context, KZBooking booking) {

        long endWithTimezone = booking.actual_end - DateUtils.getTimeZoneOffset() - booking.end_timezone_offset;

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(endWithTimezone * 1000);
        Date endDate = calendar.getTime();
        String endDateStr = new SimpleDateFormat(ConstantsUtils.FORMAT_SEGMENT_DATE, Locale.getDefault()).format(endDate);

        Analytics.with(context).identify(null, new Traits()
                .putValue("last_completed_booking_date", endDateStr), null);
    }

    public static void segmentIdentifyWithExpandUser(Context context, UserModel userModel, KZObjectForMixpanel kzObjectForMixpanel, float lastCompleteBookingCost) {
        try {
            segmentIdentifyWithUser(context, userModel);
            segmentIdentifyWithTotalBookingCount(context, kzObjectForMixpanel.total);

            if (kzObjectForMixpanel.lastCompleteBooking != null && kzObjectForMixpanel.lastCompleteBooking.actual_end > 0) {
                segmentIdentifyWithLastCompletedBookingDate(context, kzObjectForMixpanel.lastCompleteBooking);
            }

        } catch (Exception e) {
            Toast.makeText(context, "Error: " + e.toString(), Toast.LENGTH_SHORT).show();
        }
    }
}
