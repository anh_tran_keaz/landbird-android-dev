package com.keaz.landbird.utils;

/*
 * Created by Administrator on 20/2/2017.
 */

public interface OnDateChangeListener {
    void onChange(String duration, String timeStart, String timeEnd, int hhStart, int minStart, int hhEnd, int minEnd);
    void onChooseBookingTime(int duration);
}
