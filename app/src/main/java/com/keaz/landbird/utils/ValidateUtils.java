package com.keaz.landbird.utils;

import android.content.Context;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
 * Created by mac on 2/25/17.
 */
public class ValidateUtils {

    static final String regexStr1 = "^(?:(?:\\+?1\\s*(?:[.-]\\s*)?)?(?:\\(\\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9])\\s*\\)|([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\\s*(?:[.-]\\s*)?)?([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})\\s*(?:[.-]\\s*)?([0-9]{4})(?:\\s*(?:#|x\\.?|ext\\.?|extension)\\s*(\\d+))?$";
    //matches numbers only
    static String regexStr2 = "^[0-9]*$";

    //matches 10-digit numbers only
    static String regexStr3 = "^[0-9]{10}$";

    //matches numbers and dashes, any order really.
    static String regexStr4 = "^[0-9\\-]*$";

    //matches 9999999999, 1-999-999-9999 and 999-999-9999
    static String regexStr5 = "^(1\\-)?[0-9]{3}\\-?[0-9]{3}\\-?[0-9]{4}$";


    public static boolean validateEmail(Context context, String email){
        Pattern pattern = Pattern.compile(ConstantsUtils.SCOPE);
        Matcher matcher = pattern.matcher(email.trim());
        return matcher.matches();
    }

    public static boolean validatePassword(String password, Context context) {
        List<String> errorList = new ArrayList<>();

        Pattern specailCharPatten = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
        Pattern UpperCasePatten = Pattern.compile("[A-Z ]");
        Pattern lowerCasePatten = Pattern.compile("[a-z ]");
        Pattern digitCasePatten = Pattern.compile("[0-9 ]");
        errorList.clear();

        boolean flag=true;


        ConfigUtil.getConfigMinPasswordLength();

        if (password.length() < ConfigUtil.getConfigMinPasswordLength()) {
            Toast.makeText(context, "Password length must have at least " + ConfigUtil.getConfigMinPasswordLength() + " characters !!", Toast.LENGTH_SHORT).show();
            flag=false;
            return false;
        }
        if (password.length() > ConfigUtil.getConfigMaxPasswordLength()) {
            Toast.makeText(context, "Password length must have no more "+ ConfigUtil.getConfigMaxPasswordLength()+ " characters !!", Toast.LENGTH_SHORT).show();
            flag=false;
            return false;
        }
        if (!UpperCasePatten.matcher(password).find()) {
            Toast.makeText(context, "Password must have at least one uppercase character !!", Toast.LENGTH_SHORT).show();
            flag=false;
            return false;
        }
        if (!lowerCasePatten.matcher(password).find()) {
            Toast.makeText(context, "Password must have at least one lowercase character !!", Toast.LENGTH_SHORT).show();
            flag=false;
            return false;
        }
        if (!digitCasePatten.matcher(password).find()) {
            Toast.makeText(context, "Password must have at least one digit character !!", Toast.LENGTH_SHORT).show();
            flag=false;
            return false;
        }
        /*if (!specailCharPatten.matcher(passwordhere).find()) {
            Toast.makeText(context, "Password must have at least one special character !!", Toast.LENGTH_SHORT).show1();
            flag=false;
            return false;
        }*/

        return flag;
    }

    public static boolean validatePhone(String phone){
        boolean rs;
        rs = subValidatePhone1(phone) && subValidatePhone2(phone);
        return rs;
    }

    private static boolean subValidatePhone1(String phone){
        return phone.matches(regexStr1) ||
                phone.matches(regexStr2) ||
                phone.matches(regexStr3) ||
                phone.matches(regexStr4) ||
                phone.matches(regexStr5);
    }

    private static boolean subValidatePhone2(String phone) {
        boolean check;
        if(!Pattern.matches("[a-zA-Z]+", phone)) {
            if(phone.length() < 9 || phone.length() > 13) {
                check = false;
            } else {
                check = true;
            }
        } else {
            check=false;
        }
        return check;
    }
}
