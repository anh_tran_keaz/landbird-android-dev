package com.keaz.landbird.utils;

import android.content.Context;
import android.support.v7.widget.PopupMenu;
import android.view.MenuInflater;
import android.view.View;

import com.acuant.mobilesdk.Region;
import com.keaz.landbird.R;
import com.keaz.landbird.models.StateModel;

import java.util.ArrayList;

/**
 * Created by mac on 3/24/17.
 */
public class ViewUtil {

    static public void showMenu(View v, Context context, int menuId, PopupMenu.OnMenuItemClickListener onMenuItemClickListener){

        PopupMenu popupMenu = new PopupMenu(context, v);

        MenuInflater inflater = popupMenu.getMenuInflater();
        inflater.inflate(menuId, popupMenu.getMenu());

        popupMenu.setOnMenuItemClickListener(onMenuItemClickListener);
        popupMenu.show();
    }

    static public void showMenuStates(View v, Context context, int menuId, PopupMenu.OnMenuItemClickListener onMenuItemClickListener){

        PopupMenu popupMenu = new PopupMenu(context, v);

        MenuInflater inflater = popupMenu.getMenuInflater();
        inflater.inflate(menuId, popupMenu.getMenu());

        ArrayList<StateModel> states = getStates(context,Region.REGION_AUSTRALIA);
        for (int i = 0; i < states.size(); i++) {
            popupMenu.getMenu().add(states.get(i).getName());
        }

        popupMenu.setOnMenuItemClickListener(onMenuItemClickListener);
        popupMenu.show();
    }

    public static ArrayList<StateModel> getStates(Context context, int n) {
        ArrayList<StateModel> rs = new ArrayList<>();
        String[] states = null ;
        switch (n){
            case Region.REGION_UNITED_STATES:
                states = context.getResources().getStringArray(R.array.states);
                break;
            case Region.REGION_CANADA:
                states = context.getResources().getStringArray(R.array.canada_states);
                break;
            case Region.REGION_AUSTRALIA:
                states = context.getResources().getStringArray(R.array.australia_states);
                break;
        }
        for (String s : states) {
            String[] array = s.split("---");
            rs.add(new StateModel(array[0].trim(),array[1].trim()));
        }
        return rs;
    }

    static public void showMenu(View v, Context context, ArrayList<MenuItemHelperModal> o, int menuId, PopupMenu.OnMenuItemClickListener onMenuItemClickListener){

        PopupMenu popupMenu = new PopupMenu(context, v);

        MenuInflater inflater = popupMenu.getMenuInflater();
        inflater.inflate(menuId, popupMenu.getMenu());

        popupMenu.setOnMenuItemClickListener(onMenuItemClickListener);
        if(o!=null){
            for (MenuItemHelperModal s : o) {
                popupMenu.getMenu().add(s.groupID, s.itemId, s.order, s.title);
            }
        }
        popupMenu.show();
    }

    public static class MenuItemHelperModal {
        public String title;
        public int groupID;
        public int itemId;
        public int order;

        public MenuItemHelperModal(String title, int groupID, int itemId, int order) {
            this.title = title;
            this.groupID = groupID;
            this.itemId = itemId;
            this.order = order;
        }
    }
}
