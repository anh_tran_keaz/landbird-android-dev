package com.keaz.landbird.utils;

import android.util.Log;

import com.keaz.landbird.BuildConfig;

/*
 * Created by Administrator on 5/1/2017.
 */

public class MyLog {
    private static boolean isDebug = BuildConfig.DEBUG;

    public static void debug(String TAG, String msg) {
        if (isDebug) {
            Log.d(TAG, msg);
        }
    }
}
