package com.keaz.landbird.utils;

public interface OnResponseModel<T> {
    public void onResponseSuccess(T model);

    public void onResponseError(BKNetworkResponseError error);

}
