package com.keaz.landbird.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

/*
 * Created by Administrator on 14/2/2017.
 */

public class UIUtils {
    static public TextView setTextViewText(Activity activity, int resource, CharSequence text) {
        if(activity==null) return null;
        TextView textView = (TextView) activity.findViewById(resource);
        if (null != textView) {
            textView.setText(text);
        }
        return textView;
    }

    static public TextView setTextViewText(View rootView, int resource, CharSequence text) {
        if(rootView==null) return null;
        TextView textView = (TextView) rootView.findViewById(resource);
        if (null != textView) {
            textView.setText(text);
        }
        return textView;
    }

    static public void setColorText(View rootView, int resource, int color) {
        TextView textView = (TextView) rootView.findViewById(resource);
        if (null != textView) {
            textView.setTextColor(color);
        }
    }

    static public Button setButtonText(Activity activity, int resource, CharSequence text) {
        Button button = (Button) activity.findViewById(resource);
        if (null != button) {
            button.setText(text);
        }
        return button;
    }

    static public Button setButtonText(View rootView, int resource, CharSequence text) {
        Button button = (Button) rootView.findViewById(resource);
        if (null != button) {
            button.setText(text);
        }
        return button;
    }

    static public void setSwitchChecked(Activity activity, int resource, boolean checked) {
        Switch s = (Switch) activity.findViewById(resource);
        if (null != s) {
            s.setChecked(checked);
        }
    }

    static public void setSwitchChecked(View rootView, int resource, boolean checked) {
        Switch s = (Switch) rootView.findViewById(resource);
        if (null != s) {
            s.setChecked(checked);
        }
    }

    static public void alert(Activity activity, String title, String message) {
        new AlertDialog.Builder(activity)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .show();
    }

    static public void confirmation(Activity activity, String title, String message, DialogInterface.OnClickListener onClickListener) {
        new AlertDialog.Builder(activity)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(android.R.string.yes, onClickListener)
                .setNegativeButton(android.R.string.cancel, null)
                .show();
    }

    static public void runAfterMilliseconds(Runnable runnable, int milliseconds) {
        final Handler handler = new Handler();
        handler.postDelayed(runnable, milliseconds);
    }

    public static void toast(Activity activity, String message) {
        Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
    }
}
