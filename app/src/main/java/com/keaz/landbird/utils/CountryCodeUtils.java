package com.keaz.landbird.utils;

/**
 * Created by anhtran1810 on 4/2/18.
 */

public class CountryCodeUtils {
    public static String getCountryCodeText(String countryCode) {
        if(countryCode==null || countryCode.isEmpty()){
            return "AU +61";
        }
        switch (Integer.parseInt(countryCode)){
            case 1:
                return "US +1";
            case 61:
                return "AU +61";
            case 64:
                return "NZ +64";
            case 84:
                return "VN +84";
            default:
                return "";
        }
    }
}
