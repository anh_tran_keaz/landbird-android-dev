package com.keaz.landbird.utils;

import android.graphics.Color;

/**
 * Created by mac on 2/20/17.
 */
public class ConstantsUtils {

    public static final String FORMAT_DATE_TIME = "hh:mm aa dd/MM/yy";
    public static final String FORMAT_TIME = "kk:mm";
    public static final String FORMAT_SEGMENT_DATE = "yyyy - MM - dd";
    public static final String FORMAT_SEGMENT_TIME = "hh:mm aa";
    public static final String FORMAT_SEGMENT_DATE_TIME = "yyyy - MM - dd hh:mm aa";

    public static final int SHOW_COST_BUSINESS = 1;
    public static final int SHOW_COST_PRIVATE = 2;
    public static final int SHOW_COST_NO_SHOW_COST = 0;

    public static final int DEFAULT_BOOKING_BLOCK_DURATION_INTERVAL_IN_SECOND = 5;
    public static final int DEFAULT_MIN_PASSWORD_LENGTH = 6;
    public static final int DEFAULT_MAX_PASSWORD_LENGTH = 12;
    public static final int DEFAULT_MAX_BOOKING_DURATION_TIME_IN_SECOND = 38880000;
    public static final int DEFAULT_MIN_BOOKING_DURATION_TIME_SECOND = 1800;
    public static final int DEFAULT_MIN_BOOKING_DURATION_TEXT = 15;
    public static final int DEFAULT_MAX_BOOKING_DURATION_TEXT = 30;
    public static final String DEFAULT_MAX_BOOKING_DURATION_TYPE = "minute";
    public static final String DEFAULT_MIN_BOOKING_DURATION_TYPE = "month";

    public static final int PAYMENT_SITUATION_DRIVER = 1;
    public static final int PAYMENT_SITUATION_COST_CENTER = 2;
    public static final int PAYMENT_SITUATION_COMPANY = 3;

    public static final String TRIP_TYPE_BUSINESS = "Business";
    public static final String TRIP_TYPE_PRIVATE = "Private";
    public static final String TRIP_TYPE_BOTH = "Both";

    public static final String INTENT_EXTRA_EMAIL = "intent extra email";
    public static final String INTENT_EXTRA_PASSWORD = "intent extra password";
    public static final String INTENT_EXTRA_PHONE = "intent extra phone";
    public static final String INTENT_EXTRA_COUNTRY_CODE = "intent extra country code";
    public static final String INTENT_EXTRA_ACCESS_USER_ID = "intent extra user id";
    public static final String INTENT_EXTRA_NAME = "intent extra name";
    public static final String INTENT_EXTRA_VERIFY_EMAIL_OR_PHONE = "intent extra register case";
    public static final String INTENT_EXTRA_TAB = "intent extra tab";
    public static final String INTENT_EXTRA_OPEN_BOOKING_LIST_FROM = "open booking list from";

    public static final String PREF_KEY_EMAIL = "pref key email";
    public static final String PREF_KEY_CONTACT_NUMBER = "pref key contact number";
    public static final String PREF_KEY_COUNTRY_CODE = "pref key country code";
    public static final String PREF_KEY_PASSWORD = "pref key password";
    public static final String PREF_KEY_NAME = "pref key user name";
    public static final String PREF_KEY_KEEP_SIGN_IN = "pref_key_sign_in";
    public static final String PREF_KEY_DEVICE_ID = "pref_key_device_id";

    public static final String INTENT_EXTRA_CALL_API_RESEND_CODE = "intent extra call api resend code";
    public static final String VERIFY_PHONE_CASE_RESET_PASSWORD = "reset password";
    public static final String INTENT_EXTRA_ACTIVATE_ACCOUNT_OR_RESET_PASSWORD = "intent extra verify phone case";
    public static final String VERIFY_PHONE_CASE_ACTIVATE_ACCOUNT = "activate account";
    public static final String INTENT_EXTRA_CODE = "code";
    public static final String INTENT_EXTRA_IS_TODAY = "is the day today";
    public static final String INTENT_EXTRA_TITLE = "title";
    public static final String BOOKING_STATE_APPROVED = "APPROVED";
    public static final String BOOKING_STATE_COMPLETED = "COMPLETE";
    public static final String INTENT_EXTRA_CAR_URL = "intent extra car url";
    public static final String INTENT_EXTRA_VEHICLE = "intent extra car name";
    public static final String INTENT_EXTRA_START_TIME_IN_MILLIS = "intent extra start time";
    public static final String INTENT_EXTRA_END_TIME_IN_MILLIS = "intent extra end time";
    public static final String INTENT_EXTRA_BRANCH = "intent extra branch";
    public static final java.lang.String INTENT_EXTRA_TRIP_TYPE = "intent extra trip type";

    public static final String INTENT_EXTRA_OUTER_REPORT_SITUATION = "intent extra outer report";
    public static final String INTENT_EXTRA_OPEN_FROM_REGISTERING = "open driver license from registering";
    public static final String INTENT_EXTRA_OPEN_FROM_CONFIRM_BOOKING = "open driver license from confirm booking";
    public static final String INTENT_EXTRA_VIEW_DRIVER_LICENSE_FROM_SETTING = "open driver license from setting";
    public static final String INTENT_EXTRA_SSO_LOG_IN = "intent extra sso login";
    public static final String INTENT_EXTRA_SSO_REGISTER = "intent extra sso register";
    public static final String INTENT_EXTRA_REMEMBER_LOG_IN = "intent extra remember login";
    public static final String INTENT_EXTRA_ASK_PAYMENT_AFTER_DRIVER_LICENSE = "intent extra ask payment";
    public static final String INTENT_EXTRA_OPEN_PAYMENT_ACTIVITY_IN_REGISTERING = "intent add payment method in register process";
    public static final String INTENT_EXTRA_SELECT_PAYMENT_DRIVER_OR_COMPANY_OR_COSTCENTER = "selecr payment driver or company or cstcenter";
    public static final String INTENT_EXTRA_SHOW_FINISH_SIGN_UP = "intent extra show1 finish sign up";
    public static final String INTENT_EXTRA_OPEN_PAYMENT_ACTIVITY_IN_MAKE_BOOKING_WORKFLOW = "intent extra select payment method";
    public static final String INTENT_EXTRA_PAYMENT_METHOD_ID = "intent extra payment method id";
    public static final String INTENT_EXTRA_SIGN_UP_BY_CLICK_SIGN_IN_BTN_OF_REGISTER_BTN = "intent extra sign up case";

    public static final int REQUEST_CODE_TERMS_AND_CONDITIONS = 11;
    public static final int REQUEST_CODE_SELECT_OR_ADD_PAYMENT_METHOD = 1;
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 99;
    public static final int REQUEST_CODE_ADD_DRIVER_LICENSE = 89;
    public static final int REQUEST_CODE_FINISH_SIGN_UP_SCREEN = 99;
    public static final int REQUEST_CODE_SHOW_SSO_UPDATE = 122;
    public static final int REQUEST_CODE_INTERNAL_REPORT = 4;

    public static final int VERIFY_EMAIL = 1;
    public static final int VERIFY_PHONE = 2;

//    public static final int COLOR_FOCUS = Color.rgb(252, 86, 78);
//    public static final int COLOR_UNFOCUS = Color.rgb(179, 179, 179);
    public static final int COLOR_BG_DARK = Color.parseColor("#222222");
    public static final int COLOR_FOCUS = Color.parseColor("#000000") /*Color.rgb(252, 86, 78)*/;
    public static final int COLOR_UNFOCUS = Color.parseColor("#000000");
//    public static final int COLOR_BG_DARK = Color.parseColor("#c0c0c0");

    public static final String ERROR_CODE = "booking_out_update";

    public static final int REQUEST_CODE_PRIVACY_POLICY = 10;
    public static final int REQUEST_CODE_VEHICLE_REPORT = 11;
    public static final int REQUEST_CODE_TERMS_OF_SERVICE = 12;
    public static final int REQUEST_CODE_ACCEPT = 13;

    public static final String LICENSE_KEY = "B381655A9E88";
    public static final String SCOPE = "[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+\\.+[a-z.]+";

    public static final String TIME_FORMAT_1 = "hh:mm aa";
    public static final String FORMAT_DATE_2 = "dd/MM/yyyy";
    public static final String DATE_TIME_FORMAT = "hh:mm aa dd/MM/yy";
    public static final String FORMAT_DATE = "dd/MM/yy";
    public static final String INTENT_EXTRA_BOOKING_STATUS = "intent extra booking status";
    public static final String INTENT_EXTRA_BACK_TO_HOME = "back to home";
    public static final int REQUEST_CODE_REQUEST_PERMISSION = 12;
    public static final String VERIFY_PHONE_CASE_INVITE_USER = "invite user";

    public static final String TAG_SOURCE_HOST = "SourceHost";
    public static final java.lang.String TAG_POLICY = "policy";
    public static final String TAG_TIME = "time_tag";
    public static final int REQUEST_CODE_RC_SIGN_IN = 9001;
    public static final String INTENT_EXTRA_FORCE_USER_RE_UPLOAD_DL_WITH_ACUANT = "intent extra force user to re upload DL with Acuant";
    public static final String INTENT_EXTRA_END_BOOKING_LAT = "intent extra end booking lat";
    public static final String INTENT_EXTRA_END_BOOKING_LNG = "intent extra end booking lng";
    public static final String PREF_KEY_PROFILE_IMAGE_PATH = "pref key profile image path";

    public static String DEFAULT_COUNTRY_CODE = "61";

    public static String MIXPANEL_NAME = "name";
    public static String MIXPANEL_EMAIL = "email";
    public static String MIXPANEL_FISTNAME = "user_firstname";
    public static String MIXPANEL_LASTNAME = "user_lastname";
    public static String MIXPANEL_PHONE = "phone";
    public static String MIXPANEL_BRANCH_ID = "branch_id";
    public static String MIXPANEL_BRANCH_NAME = "branch_name";
    public static String MIXPANEL_START_DATE = "start_date";
    public static String MIXPANEL_START_TIME = "start_time";
    public static String MIXPANEL_END_DATE = "end_date";
    public static String MIXPANEL_END_TIME = "end_time";
    public static String MIXPANEL_MEMBERSHIP_ID = "membership_id";
    public static String MIXPANEL_MEMBERSHIP_NAME = "membership_name";
    public static String MIXPANEL_VEHICLE_ID = "vehicle_id";
    public static String MIXPANEL_BOOKING_ID = "booking_id";
    public static String MIXPANEL_BOOKING_STATUS = "booking_status";
    public static String MIXPANEL_REVENUE = "revenue";

}
