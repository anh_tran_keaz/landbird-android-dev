package com.keaz.landbird.utils;

import android.content.Context;
import android.content.res.Resources;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;

import com.keaz.landbird.BuildConfig;
import com.keaz.landbird.R;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by anhtran1810 on 11/7/17.
 */

public class GooglePlaceUtil {

    private static final String GOOGLE_API_ROOT_URI = "https://maps.googleapis.com/maps/api/";
    private static final String GOOGLE_PLACE_SERVICE_URL = GOOGLE_API_ROOT_URI + "place/autocomplete/json?types=&language=en&input=";
    private static final String GOOGLE_SEARCH_COMPONENT_KEY = "&components=country:";

    public static float getItemHeight(Context context) {
        try{
            TypedValue value = new TypedValue();
            DisplayMetrics metrics = new DisplayMetrics();

            context.getTheme().resolveAttribute(
                    android.R.attr.listPreferredItemHeight, value, true);
            ((WindowManager) (context.getSystemService(Context.WINDOW_SERVICE)))
                    .getDefaultDisplay().getMetrics(metrics);

            return TypedValue.complexToDimension(value.data, metrics);
        } catch (Exception ex){
            return 0;
        }
    }

    public static TextWatcher createTextChangeListener(final Context context, final String countryCode,
                                                       final AutoCompleteTextView etLocation, final View imvClose,
                                                       final CheckBox cbConcierge, final String conciergeDefault,
                                                       final boolean isNeedCalculatePositionDropdown) {
        TextWatcher textWatcher = new TextWatcher() {

            @Override
            public void beforeTextChanged(final CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                imvClose.setVisibility(editable.length() > 0 ? View.VISIBLE : View.INVISIBLE);
                cbConcierge.setText(conciergeDefault);
                int s = editable.length();
                if (s > 15) {
                    return;
                }
                String url = GOOGLE_PLACE_SERVICE_URL + editable.toString();
                //Where we get countryCode: From user's country info
                /*if (countryCode != null) {
                    //Restricted to a specific country
                    //url = url + ConstantsUtils.GOOGLE_SEARCH_COMPONENT_KEY + countryCode;
                }*/

                //Restricted to some specific country (Australia)
                url = url + GOOGLE_SEARCH_COMPONENT_KEY + "au" /*+ "|country:" + "au"*/;

                url += ("&key=" + BuildConfig.GOOGLE_KEY_PRODUCTION);

                VolleyUtils.getSharedNetwork().callGoogleAPIServiceLocationInfo(context, url, new OnResponseModel<JSONObject>() {
                    @Override
                    public void onResponseSuccess(JSONObject model) {
                        ArrayList<String> listString = ParseJSonUtil.parseJsonGooglePlaceServiceIntoListString(model);
                        String[] countries = new String[listString.size()];
                        for (int j = 0; j < listString.size(); j++) {
                            countries[j] = listString.get(j);
                        }
                        ArrayAdapter<String> adapter2 = new ArrayAdapter<>(context, R.layout.simple_list_item_1, countries);
                        etLocation.setAdapter(adapter2);
                        if(isNeedCalculatePositionDropdown){
                            float itemHeight = getItemHeight(context);
                            int dropDownHeight = (int)(itemHeight*countries.length);//(int)(200 * Resources.getSystem().getDisplayMetrics().density);
                            etLocation.setDropDownVerticalOffset(
                                    -(dropDownHeight + (int)(etLocation.getMeasuredHeight() * Resources.getSystem().getDisplayMetrics().density)));
                            etLocation.setDropDownHeight(dropDownHeight);
                        }
                        etLocation.showDropDown();
                    }

                    @Override
                    public void onResponseError(BKNetworkResponseError error) {
                    }
                });

            }

        };
        return textWatcher;
    }
}
