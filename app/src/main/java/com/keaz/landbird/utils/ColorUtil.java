package com.keaz.landbird.utils;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;

import com.keaz.landbird.R;

import static com.keaz.landbird.utils.BKGlobals.BookingStatus.APPROVED;

/**
 * Created by anhtran1810 on 3/7/18.
 */

public class ColorUtil {
    public static int getTextColorOfBooking(Context context, String state) {
        if(state.equalsIgnoreCase(APPROVED.toString())){
            return context.getResources().getColor(R.color.approved);
        }
        if(state.equalsIgnoreCase(BKGlobals.BookingStatus.INUSE.toString())){
            return context.getResources().getColor(R.color.inused);
        }
        if(state.equalsIgnoreCase(BKGlobals.BookingStatus.REQUEST.toString())){
            return context.getResources().getColor(R.color.request);
        }
        if(state.equalsIgnoreCase(BKGlobals.BookingStatus.PENDING.toString())){
            return context.getResources().getColor(R.color.pending);
        }
        if(state.equalsIgnoreCase(BKGlobals.BookingStatus.CANCELLED.toString())){
            return context.getResources().getColor(R.color.canceled);
        }
        if(state.equalsIgnoreCase(BKGlobals.BookingStatus.INSPECTION.toString())){
            return context.getResources().getColor(R.color.inspection);
        }
        if(state.equalsIgnoreCase(BKGlobals.BookingStatus.EXPIRED.toString())){
            return context.getResources().getColor(R.color.expired);
        }
        return Color.rgb(194, 194, 194);
    }

    public static Drawable getTextBackgroundOfBooking(Context context, String state) {
        int di =  R.drawable.rect_expired_status;
        if(state.equalsIgnoreCase(BKGlobals.BookingStatus.APPROVED.toString())){
            di =  R.drawable.rect_approve_status_blue;
        }
        if(state.equalsIgnoreCase(BKGlobals.BookingStatus.INUSE.toString())){
            di =  R.drawable.rect_inused_status;
        }
        if(state.equalsIgnoreCase(BKGlobals.BookingStatus.REQUEST.toString())){
            di =  R.drawable.rect_request_status;
        }
        if(state.equalsIgnoreCase(BKGlobals.BookingStatus.PENDING.toString())){
            di =  R.drawable.rect_pending_status;
        }
        if(state.equalsIgnoreCase(BKGlobals.BookingStatus.CANCELLED.toString())){
            di =  R.drawable.rect_canceled_status;
        }
        if(state.equalsIgnoreCase(BKGlobals.BookingStatus.INSPECTION.toString())){
            di =  R.drawable.rect_inspection_status;
        }
        if(state.equalsIgnoreCase(BKGlobals.BookingStatus.EXPIRED.toString())){
            di =  R.drawable.rect_expired_status;
        }
        return context.getResources().getDrawable(di);
    }
}
