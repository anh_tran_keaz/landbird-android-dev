package com.keaz.landbird.utils;

import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by keaz on 5/16/16.
 */
public class ParseJSonUtil {
    public static <T> T parseByModel(String responseData, Class<T> classOfT) {
        T result = null;
        if (!TextUtils.isEmpty(responseData)) {
            GsonBuilder gsonBuilder = new GsonBuilder();
            Gson gson = gsonBuilder.create();
            try {
                result = gson.fromJson(responseData, classOfT);
            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public static ArrayList<String> parseJsonGooglePlaceServiceIntoListString(String strJson) {

        List<String> listStringResult = new ArrayList<String>();

        try {
            JSONObject jsonRootObject = new JSONObject(strJson);
            //Get the instance of JSONArray that contains JSONObjects
            JSONArray jsonArray = jsonRootObject.optJSONArray("predictions");

            //Iterate the jsonArray and print the info of JSONObjects
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                if (jsonObject != null) {
                    String description = jsonObject.getString("description");
                    listStringResult.add(description);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return (ArrayList<String>) listStringResult;
    }

    public static ArrayList<JSONObject> parseJsonStringToListOfJsonObject(String wordIdJson) {
        if(wordIdJson==null) return new ArrayList<>();
        Type type = new TypeToken<ArrayList<String>>(){}.getType();
        Gson gson = new Gson();
        ArrayList<String> list = gson.fromJson(wordIdJson,type);
        ArrayList<JSONObject> jsonObjects = new ArrayList<>();
        for (String s: list) {
            try {
                JSONObject j = new JSONObject(s);
                jsonObjects.add(j);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return jsonObjects;
    }

    public static ArrayList<String> parseJsonGooglePlaceServiceIntoListString(JSONObject jsonRootObject) {

        List<String> listStringResult = new ArrayList<>();

        try {
            //Get the instance of JSONArray that contains JSONObjects
            JSONArray jsonArray = jsonRootObject.optJSONArray("predictions");

            //Iterate the jsonArray and print the info of JSONObjects
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                if (jsonObject != null) {
                    String description = jsonObject.getString("description");
                    listStringResult.add(description);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return (ArrayList<String>) listStringResult;
    }

}
