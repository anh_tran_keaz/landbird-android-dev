package com.keaz.landbird.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.keaz.landbird.R;
import com.keaz.landbird.models.KZAccount;
import com.keaz.landbird.models.KZBooking;
import com.keaz.landbird.utils.CommonUtils;
import com.keaz.landbird.utils.DateUtils;
import com.keaz.landbird.utils.FormatTimeUtils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Administrator on 21/2/2017.
 */

public class AddMoreTimeDialog extends Dialog {

    private final Context mContext;
    private Dialog dialog;
    private OnAddMoreTimeDialogListener listener;
    private TextView textAdded;
    private TextView textDuration;
    private TextView textStartEnd;
    private int count;
    private long mStart = 0;
    private long mEnd = 0;
    private long mNewEnd = 0;
    private int mBlockBookingTime;
    private KZBooking mBooking;

    public AddMoreTimeDialog(Context context, KZBooking booking) {
        super(context);
        mContext = context;
        mBooking = booking;
        dialog = new Dialog(mContext);

        mBlockBookingTime = (int) (KZAccount.getSharedAccount().company.company_booking_block_duration / 60);
    }

    public void show() {
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_add_more_time);

        CommonUtils.segmentScreenWithBooking(mContext, "Add More Time", mBooking);

        WindowManager.LayoutParams layoutParam = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        layoutParam.copyFrom(window.getAttributes());
        layoutParam.width = WindowManager.LayoutParams.MATCH_PARENT;
        layoutParam.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setAttributes(layoutParam);

        textStartEnd = (TextView) dialog.findViewById(R.id.textStartEnd);
        textDuration = (TextView) dialog.findViewById(R.id.textDuration);
        textAdded = (TextView) dialog.findViewById(R.id.textAdded);

        dialog.findViewById(R.id.btnConfirm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onOK(count * mBlockBookingTime);
                }
                dialog.cancel();
            }
        });
        dialog.findViewById(R.id.btnNope).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onCancel();
                }
                dialog.cancel();
            }
        });

        dialog.findViewById(R.id.btnAdd).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                long duration = mEnd - mStart;
                long maxDuration = KZAccount.getSharedAccount().company.durations.booking_max_duration_time;
                int minutesAvaiable = (int) ((maxDuration - duration)/60);

                int moreMinute = (count + 1) * mBlockBookingTime;

                if (moreMinute > minutesAvaiable) {
                    String strMaxDuration = DateUtils.getDurationFormatText3((int) (maxDuration/60));
                    Toast.makeText(mContext, "Add more time must limit in " + strMaxDuration, Toast.LENGTH_SHORT).show();
                    return;
                }

                count++;
                String formatDuration = FormatTimeUtils.formatDurationInMillis2(count*mBlockBookingTime*60*1000);
                String text = "+" + formatDuration /*count * mBlockBookingTime + "mins"*/;
                textAdded.setText(text);
                textStartEnd.setText(updateStartEnd(mBlockBookingTime));
            }
        });

        dialog.findViewById(R.id.btnMinus).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (count <= 0) {
                    textAdded.setText("");
                    count = 0;
                    return;
                }
                count--;
                String text = "+" + count * mBlockBookingTime + "mins";
                textAdded.setText(text);
                textStartEnd.setText(updateStartEnd(-mBlockBookingTime));
            }
        });
        dialog.show();
    }

    private String updateStartEnd(int mins) {
        Date start = DateUtils.getDate(mStart);
        Date end = DateUtils.getDate(mNewEnd);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(end);
        calendar.add(Calendar.MINUTE, mins);
        mNewEnd = calendar.getTimeInMillis() / 1000;
        String str_start = new SimpleDateFormat("hh:mm aa").format(start);
        String str_end = new SimpleDateFormat("hh:mm aa").format(calendar.getTime());
        return str_start + " - " + str_end;
    }

    public void setOnAddMoreTimeDialogListener(OnAddMoreTimeDialogListener listener) {
        this.listener = listener;
    }

    public void setUpData(long start, long end) {
        mStart = start;
        mEnd = end;
        mNewEnd = end;

        textDuration.setText(DateUtils.getDurationFormatText2((int) ((end-start)/60)));

        String str_start = new SimpleDateFormat("hh:mm aa").format(DateUtils.getDate(start));
        String str_end = new SimpleDateFormat("hh:mm aa").format(DateUtils.getDate(end));
        textStartEnd.setText(str_start + " - " + str_end);
        textAdded.setText("");
    }

    public interface OnAddMoreTimeDialogListener {
        void onOK(int minutes);

        void onCancel();
    }
}
