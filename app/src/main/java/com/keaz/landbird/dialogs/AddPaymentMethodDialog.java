package com.keaz.landbird.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import com.keaz.landbird.R;

/**
 * Created by Administrator on 15/2/2017.
 */

public class AddPaymentMethodDialog extends Dialog {
    private OnAddPaymentMethod listener;
    private Dialog dialog;

    public AddPaymentMethodDialog(Context context) {
        super(context);
        dialog = new Dialog(context);
    }

    public void setOnAddPaymentMethodListener(OnAddPaymentMethod listener) {
        this.listener = listener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void show() {
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_add_payment_method);
        dialog.setCanceledOnTouchOutside(true);
        Window window = dialog.getWindow();
        final WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        final RelativeLayout rlCreditCard = (RelativeLayout) dialog.findViewById(R.id.rl_credit_card);

        rlCreditCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null)
                    listener.onAddCreditCard();
                dialog.dismiss();
            }
        });


        dialog.show();
    }

    public void cancel() {
        if (dialog != null)
            dialog.dismiss();
    }

    public interface OnAddPaymentMethod {
        void onAddCreditCard();

        void onAddPaypal();
    }
}
