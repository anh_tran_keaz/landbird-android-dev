package com.keaz.landbird.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.keaz.landbird.R;

/**
 * Created by Administrator on 15/2/2017.
 */

public class ConfirmBillingDialog extends Dialog {
    private OnConfirmReservationListener listener;
    private Dialog dialog;

    public ConfirmBillingDialog(Context context) {
        super(context);
        dialog = new Dialog(context);
    }

    public void setOnConfirmReservationListener(OnConfirmReservationListener listener) {
        this.listener = listener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void show() {
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_confirm_billing_address);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        Button confirm = (Button) dialog.findViewById(R.id.btnConfirm);
        final Button cancel = (Button) dialog.findViewById(R.id.btnCancel);
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onOk();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancel();
            }
        });
        dialog.show();
    }

    public void cancel() {
        dialog.cancel();
    }

    public interface OnConfirmReservationListener {
        void onOk();

        void onCancel();
    }
}
