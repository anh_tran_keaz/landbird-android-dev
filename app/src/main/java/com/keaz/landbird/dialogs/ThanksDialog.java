package com.keaz.landbird.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.keaz.landbird.R;
import com.keaz.landbird.models.KZBooking;
import com.keaz.landbird.models.KZExtraCost;
import com.keaz.landbird.utils.CommonUtils;
import com.keaz.landbird.utils.UIUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Administrator on 21/2/2017.
 */

public class ThanksDialog extends Dialog {

    private final Context mContext;
    private Dialog dialog;
    private OnThanksDialogListener listener;
    private KZBooking mBooking;

    public ThanksDialog(Context context, KZBooking booking) {
        super(context);
        mContext = context;
        mBooking = booking;
        dialog = new Dialog(mContext);
    }

    public void show() {
        CommonUtils.segmentScreenWithBookingAndRevenue(mContext, "Thank You", mBooking);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_thanks);
        WindowManager.LayoutParams layoutParam = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        layoutParam.copyFrom(window.getAttributes());
        layoutParam.width = WindowManager.LayoutParams.MATCH_PARENT;
        layoutParam.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setAttributes(layoutParam);

        dialog.findViewById(R.id.btnGotIt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    CommonUtils.segmentTrackWithBooking(mContext, "Click Got_it_btn", mBooking);
                    listener.onOK();
                }
                dialog.cancel();
            }
        });

        dialog.show();
    }

    public void setupData(String photo, String registration,
                          String duration, String time,
                          String total, String subTotal,
                          String promote, String discountMembership,
                          String gst, String concierge_delivery, String concierge_collection,
                          ArrayList<KZExtraCost> extra_costs) {
        ((TextView) dialog.findViewById(R.id.txtRegistration)).setText(registration);
        ((TextView) dialog.findViewById(R.id.txtDuration)).setText(duration);
        ((TextView) dialog.findViewById(R.id.txtTime)).setText(time);
        ((TextView) dialog.findViewById(R.id.txtTotal)).setText(total);
        ((TextView) dialog.findViewById(R.id.txtConciergeDelivery)).setText(concierge_delivery);
        ((TextView) dialog.findViewById(R.id.txtConciergeCollection)).setText(concierge_collection);
        ImageView ivCar = dialog.findViewById(R.id.ivCar);
        Picasso.with(mContext).load(photo).placeholder(R.drawable.new_img_car)
                .error(R.drawable.new_img_car)
                .into(ivCar);

        ((TextView) dialog.findViewById(R.id.tvValueBookingCost)).setText(subTotal);
        ((TextView) dialog.findViewById(R.id.txtPromote)).setText(promote);
        ((TextView) dialog.findViewById(R.id.txtGst)).setText(gst);
        /*((TextView) dialog.findViewById(R.id.txtDiscountMembership)).setText(discountMembership);*/

        if(total==null) dialog.findViewById(R.id.txtTotal).setVisibility(View.GONE);
        if(subTotal==null) dialog.findViewById(R.id.view_subtotal).setVisibility(View.GONE);
        if(promote==null) dialog.findViewById(R.id.view_duration).setVisibility(View.GONE);
        if(gst==null) dialog.findViewById(R.id.view_gst).setVisibility(View.GONE);
        if(concierge_delivery==null) dialog.findViewById(R.id.view_concierge_delivery).setVisibility(View.GONE);
        if(concierge_collection==null) dialog.findViewById(R.id.view_concierge_collection).setVisibility(View.GONE);
        LinearLayout lnExtraCost = (LinearLayout) dialog.findViewById(R.id.lnExtraCost);
        lnExtraCost.removeAllViews();
        lnExtraCost.removeAllViewsInLayout();
        lnExtraCost.setVisibility(View.GONE);
        if(null != extra_costs && extra_costs.size() > 0){
            lnExtraCost.setVisibility(View.VISIBLE);
            for (KZExtraCost extraCost : extra_costs) {
                View viewCost = getLayoutInflater().inflate(R.layout.item_extra_cost_thanks, null);
                UIUtils.setTextViewText(viewCost, R.id.txtTitleCost, extraCost.text);
                UIUtils.setTextViewText(viewCost, R.id.txtValueCost, "$"+extraCost.cost);
                lnExtraCost.addView(viewCost);
            }
        }

    }

    public void setOnThanksDialogListener(OnThanksDialogListener listener) {
        this.listener = listener;
    }

    public interface OnThanksDialogListener {
        void onOK();
    }
}
