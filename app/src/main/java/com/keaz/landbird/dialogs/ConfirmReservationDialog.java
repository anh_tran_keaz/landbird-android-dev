package com.keaz.landbird.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.internal.LinkedTreeMap;
import com.keaz.landbird.R;
import com.keaz.landbird.activities.BaseActivity;
import com.keaz.landbird.models.KZAccount;
import com.keaz.landbird.models.KZAddOn;
import com.keaz.landbird.models.KZBranch;
import com.keaz.landbird.models.KZVehicle;
import com.keaz.landbird.utils.BKNetworkResponseError;
import com.keaz.landbird.utils.CommonUtils;
import com.keaz.landbird.utils.ConfigUtil;
import com.keaz.landbird.utils.ConstantsUtils;
import com.keaz.landbird.utils.FormatUtil;
import com.keaz.landbird.utils.GooglePlaceUtil;
import com.keaz.landbird.utils.OnResponseModel;
import com.keaz.landbird.utils.ViewUtil;
import com.keaz.landbird.utils.VolleyUtils;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import java.util.Date;

import static com.keaz.landbird.utils.ConfigUtil.actionDefineTripType;

/**
 * Created by Administrator on 15/2/2017.
 */

public class ConfirmReservationDialog extends Dialog {
    private final ArrayList<KZAddOn> kzAddOns;
    private final ArrayList<String> selectedAddOns;
    private Dialog dialog;
    private KZVehicle vehicle;
    private final Context context;
    private final KZBranch[] kzBranches;
    private final KZBranch kzBranch;
    private OnConfirmReservationListener onConfirmReservationListener;
    private OnSelectDropOffBranch onSelectDropOffBranchListener;
    private String conciergeCollectionLocation;
    private String conciergeDeliveryLocation;
    private int dropOffBranchId;
    private OnConciergeInfoUpdate onConciergeInfoUpdate;

    private AutoCompleteTextView etStreetAddress1;
    private AutoCompleteTextView etStreetAddress2;

    private double bookingCost;
    private double conciergeCollectionCost;
    private double conciergeDeliveryCost;
    private ArrayList<String> adOnIds = new ArrayList<>();
    private OnAddOnUpdate onAddOnUpdate;

    public ConfirmReservationDialog(Context context, KZVehicle vehicle, KZBranch branch, KZBranch[] kzBranches, ArrayList<KZAddOn> kzAddOns, ArrayList<String> selectedAddOns) {
        super(context);
        this.context = context;
        dialog = new Dialog(context);
        this.vehicle = vehicle;
        this.kzBranch = branch;
        this.kzBranches = kzBranches;
        this.kzAddOns = kzAddOns;
        this.selectedAddOns = selectedAddOns;
        if(null != branch){
            this.dropOffBranchId = branch.id;
        }
    }

    public void setOnConfirmReservationListener(OnSelectDropOffBranch onSelectDropOffBranch, OnConfirmReservationListener onConfirmReservationListener, OnConciergeInfoUpdate onConciergeInfoUpdate, OnAddOnUpdate onAddOnUpdate) {
        this.onConfirmReservationListener = onConfirmReservationListener;
        this.onSelectDropOffBranchListener = onSelectDropOffBranch;
        this.onConciergeInfoUpdate = onConciergeInfoUpdate;
        this.onAddOnUpdate = onAddOnUpdate;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void show() {
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_confirm_reservation);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        dialog.findViewById(R.id.btnConfirm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(((CheckBox)dialog.findViewById(R.id.cbConcierge1)).isChecked()
                        && TextUtils.isEmpty(etStreetAddress1.getText().toString()))
                {
                    if(null != (Activity)context && ((Activity)context) instanceof BaseActivity){
                        ((BaseActivity)context).showAlertDialogNoTitle("Please choose Concierge Delivery Address");
                    }
                    return;
                }
                else if(((CheckBox)dialog.findViewById(R.id.cbConcierge2)).isChecked()
                        && TextUtils.isEmpty(etStreetAddress2.getText().toString()))
                {
                    if(null != (Activity)context && ((Activity)context) instanceof BaseActivity){
                        ((BaseActivity)context).showAlertDialogNoTitle("Please choose Concierge Collection Address");
                    }
                    return;
                }
                onConciergeInfoUpdate.actionUpdate(
                        ((CheckBox)dialog.findViewById(R.id.cbConcierge1)).isChecked(),
                        etStreetAddress1.getText().toString(),
                        ((EditText)dialog.findViewById(R.id.etDeliveryNote)).getText().toString(),
                        ((CheckBox)dialog.findViewById(R.id.cbConcierge2)).isChecked(),
                        etStreetAddress2.getText().toString()
                );

                onAddOnUpdate.actionUpdate(adOnIds);

                onConfirmReservationListener.onOk();

                cancel();
            }
        });
        dialog.findViewById(R.id.btnCancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onConfirmReservationListener.onCancel();
                cancel();
            }
        });

        if(ConfigUtil.checkIfShowConciergeOption()){
            dialog.findViewById(R.id.view_concierge).setVisibility(View.VISIBLE);
        }else {
            dialog.findViewById(R.id.view_concierge).setVisibility(View.GONE);
        }

        etStreetAddress1 = dialog.findViewById(R.id.etStrAddress1);
        etStreetAddress1.addTextChangedListener(
                GooglePlaceUtil.createTextChangeListener(context, null,
                        etStreetAddress1, dialog.findViewById(R.id.imvClose),
                        ((CheckBox)dialog.findViewById(R.id.cbConcierge1)),
                        "Concierge Delivery@ $0.00", false));

        etStreetAddress2 = dialog.findViewById(R.id.etStrAddress2);
        etStreetAddress2.addTextChangedListener(
                GooglePlaceUtil.createTextChangeListener(context, null,
                        etStreetAddress2, dialog.findViewById(R.id.imvClose2),
                        ((CheckBox)dialog.findViewById(R.id.cbConcierge2)),
                        "Concierge Collection@ $0.00", true));

        etStreetAddress1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                conciergeDeliveryLocation = etStreetAddress1.getText().toString();
                actionUpdateConciergeInfo();
            }
        });
        etStreetAddress2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                conciergeCollectionLocation = etStreetAddress2.getText().toString();
                actionUpdateConciergeInfo();
            }
        });
        ((CheckBox)dialog.findViewById(R.id.cbConcierge1)).setChecked(false);
        ((CheckBox)dialog.findViewById(R.id.cbConcierge2)).setChecked(false);
        conciergeDeliveryCost = 0;
        conciergeCollectionCost = 0;
        dialog.findViewById(R.id.view_concierge_location1).setVisibility(View.GONE);
        dialog.findViewById(R.id.viewDeliveryNote).setVisibility(View.GONE);
        dialog.findViewById(R.id.view_concierge_location2).setVisibility(View.GONE);
        dialog.findViewById(R.id.cbSameAsAbove).setVisibility(View.GONE);

        dialog.findViewById(R.id.imvClose).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                etStreetAddress1.setText("");
                actionUpdateConciergeInfo();
            }
        });

        dialog.findViewById(R.id.imvClose2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                if(etStreetAddress2.isEnabled()){
                    etStreetAddress2.setText("");
                    actionUpdateConciergeInfo();
                }
            }
        });

        ((CheckBox)dialog.findViewById(R.id.cbConcierge1)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                dialog.findViewById(R.id.view_concierge_location1).setVisibility(b?View.VISIBLE:View.GONE);
                dialog.findViewById(R.id.viewDeliveryNote).setVisibility(b?View.VISIBLE:View.GONE);
                updateCostDetail();
            }
        });
        ((CheckBox)dialog.findViewById(R.id.cbConcierge2)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                dialog.findViewById(R.id.view_concierge_location2).setVisibility(b?View.VISIBLE:View.GONE);
                ((CheckBox)dialog.findViewById(R.id.cbSameAsAbove)).setVisibility(b?View.VISIBLE:View.GONE);
                updateCostDetail();
            }
        });
        ((CheckBox)dialog.findViewById(R.id.cbSameAsAbove)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b) {
                    if(!TextUtils.isEmpty(etStreetAddress1.getText().toString())) {
                        etStreetAddress2.setText(etStreetAddress1.getText()
                                .toString());
                        conciergeCollectionLocation = etStreetAddress2.getText()
                                .toString();
                        actionUpdateConciergeInfo();

                        etStreetAddress2.setEnabled(false);
                    }
                } else {
                    etStreetAddress2.setEnabled(true);
                }
            }
        });

        if(null != kzBranch){
            ((TextView)dialog.findViewById(R.id.tvDropOffValue)).setText(kzBranch.name);
        }
        if(kzBranches!=null) {
            dialog.findViewById(R.id.view_dropOffBranch).setVisibility(View.VISIBLE);
            dialog.findViewById(R.id.view_dropOffBranch).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ArrayList<ViewUtil.MenuItemHelperModal> list = new ArrayList<>();
                    int i = 0;
                    for (KZBranch br: kzBranches) {
                        list.add(new ViewUtil.MenuItemHelperModal(br.name, 1, i, i));
                        i++;
                    }
                    ViewUtil.showMenu(view, context, list, R.menu.empty_menu, new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            ((TextView)dialog.findViewById(R.id.tvDropOffValue)).setText(item.getTitle());
                            onSelectDropOffBranchListener.onSelect(kzBranches[item.getItemId()].id,
                                    kzBranches[item.getItemId()].time_zone_offset);
                            dropOffBranchId = kzBranches[item.getItemId()].id;
                            if(ConfigUtil.checkIfShowConciergeOption()){
                                actionUpdateConciergeInfo();
                            }
                            return false;
                        }
                    });
                }
            });
        } else {
            dialog.findViewById(R.id.view_dropOffBranch).setVisibility(View.GONE);
        }

        updateAddOnsView(kzAddOns);

        dialog.show();
    }

    private void updateAddOnsView(final ArrayList<KZAddOn> kzAddOns) {
        if(kzAddOns==null) return;
        final CheckBox[] addOnCbs = new CheckBox[]{
                dialog.findViewById(R.id.cbAddOn1),dialog.findViewById(R.id.cbAddOn2),
                dialog.findViewById(R.id.cbAddOn3),dialog.findViewById(R.id.cbAddOn4),
                dialog.findViewById(R.id.cbAddOn5),dialog.findViewById(R.id.cbAddOn6),
                dialog.findViewById(R.id.cbAddOn7),dialog.findViewById(R.id.cbAddOn8),
                dialog.findViewById(R.id.cbAddOn9),dialog.findViewById(R.id.cbAddOn10),
        };
        int i = -1;
        for (CheckBox cb: addOnCbs) {
            i++;
            if(i<kzAddOns.size()){
                cb.setChecked(selectedAddOns.contains(kzAddOns.get(i).id));
                cb.setText(kzAddOns.get(i).name);
                final int finalI = i;
                cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if(adOnIds.contains(kzAddOns.get(finalI).id)){
                            adOnIds.remove(kzAddOns.get(finalI).id);
                        }else{
                            adOnIds.add(kzAddOns.get(finalI).id);
                        }
                    }
                });
            }else {
                cb.setVisibility(View.GONE);
            }
        }
    }

    private void actionUpdateConciergeInfo() {
        VolleyUtils.getSharedNetwork().loadConciergeCost(context,
                etStreetAddress1.getText().toString(), etStreetAddress2.getText().toString(),
                kzBranch.id, dropOffBranchId, vehicle.id,
                new OnResponseModel<Object>() {
                    @Override
                    public void onResponseSuccess(Object model) {
                        LinkedTreeMap linkedTreeMap = (LinkedTreeMap) model;
                        Double a = (Double) linkedTreeMap.get("concierge_delivery_cost");
                        Double b = (Double) linkedTreeMap.get("concierge_collection_cost");
                        conciergeDeliveryCost = a;
                        conciergeCollectionCost = b;
                        if(!actionDefineTripType().equals(ConstantsUtils.TRIP_TYPE_BUSINESS)){
                            if(!TextUtils.isEmpty(etStreetAddress1.getText().toString())){
                                ((CheckBox)dialog.findViewById(R.id.cbConcierge1)).setText("Concierge Delivery@ $".concat(a+""));
                            } else {
                                ((CheckBox)dialog.findViewById(R.id.cbConcierge1)).setText("Concierge Delivery@ $0.00");
                            }
                            if(!TextUtils.isEmpty(etStreetAddress2.getText().toString())){
                                ((CheckBox)dialog.findViewById(R.id.cbConcierge2)).setText("Concierge Collection@ $".concat(b+""));
                            } else {
                                ((CheckBox)dialog.findViewById(R.id.cbConcierge2)).setText("Concierge Collection@ $0.00");
                            }
                        }

                        updateCostDetail();
                    }

                    @Override
                    public void onResponseError(BKNetworkResponseError error) {
                        conciergeDeliveryCost = 0;
                        conciergeCollectionCost = 0;
                        ((CheckBox)dialog.findViewById(R.id.cbConcierge1)).setText("Concierge Delivery@");
                        ((CheckBox)dialog.findViewById(R.id.cbConcierge2)).setText("Concierge Collection@");
                        updateCostDetail();
                    }
                });
    }

    public void setUpData(KZVehicle vehicle, String duration,
                          String startStr, String endStr,
                          Date startDate, Date endDate,
                          String bookingCostAsString, String dropOffBranchName) {
        ((TextView) dialog.findViewById(R.id.txtRegistration)).setText(vehicle.name);
        ((TextView) dialog.findViewById(R.id.txtDuration)).setText(duration);
        ((TextView) dialog.findViewById(R.id.txtStartEnd)).setText(startStr + " - " + endStr);
        ((TextView) dialog.findViewById(R.id.tvBookingCost)).setText(bookingCostAsString);
        if(dropOffBranchName!=null){
            //dialog.findViewById(R.id.tvDropOffBranch).setVisibility(View.VISIBLE);
            ((TextView) dialog.findViewById(R.id.tvDropOffBranch)).setText(dropOffBranchName);
        }else {
            dialog.findViewById(R.id.tvDropOffBranch).setVisibility(View.GONE);
        }

        updateCostDetail();

        ImageView ivCar = dialog.findViewById(R.id.ivCar);
        Picasso.with(context).load(vehicle.assets.photo)
                .placeholder(R.drawable.new_img_car)
                .error(R.drawable.new_img_car)
                .into(ivCar);

        CommonUtils.segmentScreenkWithTime(getContext(), "Confirm Booking", vehicle.branch, startDate, endDate);
    }

    private void updateCostDetail(){
        if(actionDefineTripType().equals(ConstantsUtils.TRIP_TYPE_BUSINESS)){
            dialog.findViewById(R.id.view_cost).setVisibility(View.GONE);
            ((CheckBox)dialog.findViewById(R.id.cbConcierge1)).setText("Concierge Delivery@");
            ((CheckBox)dialog.findViewById(R.id.cbConcierge2)).setText("Concierge Collection@");
            return;
        }
        try {
            String bookingCostAsString = "$" + FormatUtil.formatString(String.valueOf(vehicle.cost_private_total));
            ((TextView) dialog.findViewById(R.id.tvValueBookingCost)).setText(bookingCostAsString);

            double conciergeDelivery = 0;
            double conciergeCollection = 0;
            if(((CheckBox)dialog.findViewById(R.id.cbConcierge1)).isChecked()){
                dialog.findViewById(R.id.lnConciergeDelivery).setVisibility(View.VISIBLE);
                if(!TextUtils.isEmpty(etStreetAddress1.getText().toString())){
                    conciergeDelivery = conciergeDeliveryCost;
                }
                ((TextView) dialog.findViewById(R.id.txtConciergeDelivery)).setText("$" + FormatUtil.formatString(String.valueOf(conciergeDelivery)));
            } else {
                dialog.findViewById(R.id.lnConciergeDelivery).setVisibility(View.GONE);
            }
            if(((CheckBox)dialog.findViewById(R.id.cbConcierge2)).isChecked()){
                dialog.findViewById(R.id.lnConciergeCollection).setVisibility(View.VISIBLE);
                if(!TextUtils.isEmpty(etStreetAddress2.getText().toString())){
                    conciergeCollection = conciergeCollectionCost;
                }
                ((TextView) dialog.findViewById(R.id.txtConciergeCollection)).setText("$" + FormatUtil.formatString(String.valueOf(conciergeCollection)));
            } else {
                dialog.findViewById(R.id.lnConciergeCollection).setVisibility(View.GONE);
            }

            boolean enableIncludeGST = KZAccount.getSharedAccount().getCompany().enable_include_gst;
            double bookingCostGST = KZAccount.getSharedAccount().getCompany().booking_cost_gst;
            if(enableIncludeGST){
                dialog.findViewById(R.id.viewTotalCostIncludeGst).setVisibility(View.VISIBLE);
                dialog.findViewById(R.id.view_gst).setVisibility(View.GONE);

                double totalCost = vehicle.cost_private_total + conciergeDelivery + conciergeCollection;
                double totalCostIncludeGST = (totalCost * bookingCostGST) / (100 + bookingCostGST);

                ((TextView) dialog.findViewById(R.id.txtCostTotalCostIncludeGstLabel)).setText("Total Include GST \n(" + bookingCostGST + "%)");
                ((TextView) dialog.findViewById(R.id.txtCostTotalCostIncludeGst)).setText("$" + FormatUtil.formatString(String.valueOf(totalCostIncludeGST)));

                ((TextView) dialog.findViewById(R.id.tvValueCostTotal)).setText("$" + FormatUtil.formatString(String.valueOf(totalCost)));
            } else {
                dialog.findViewById(R.id.viewTotalCostIncludeGst).setVisibility(View.GONE);
                dialog.findViewById(R.id.view_gst).setVisibility(View.VISIBLE);

                double gstCost = ((vehicle.cost_private_total + conciergeDelivery + conciergeCollection) * bookingCostGST) / 100;
                double totalCost = vehicle.cost_private_total + conciergeDelivery + conciergeCollection + gstCost;

                ((TextView) dialog.findViewById(R.id.tvTitleGst)).setText("GST (" + bookingCostGST + "%)");
                ((TextView) dialog.findViewById(R.id.tvValueGst)).setText("$" + FormatUtil.formatString(String.valueOf(gstCost)));

                ((TextView) dialog.findViewById(R.id.tvValueCostTotal)).setText("$" + FormatUtil.formatString(String.valueOf(totalCost)));
            }
        } catch (Exception ex){
            ex.printStackTrace();
        }
    }

    public void cancel() {
        dialog.cancel();
    }

    public interface OnConfirmReservationListener {
        void onOk();

        void onCancel();
    }

    public interface OnSelectDropOffBranch{
        void onSelect(int id, String timeZoneOffset);
    }

    public interface OnConciergeInfoUpdate{
        void actionUpdate(boolean checked, String conciergeDeliveryLocation, String viewById, boolean checked1, String conciergeCollectionLocation);
    }

    public interface OnAddOnUpdate{
        void actionUpdate(ArrayList<String> adOnIds);
    }
}
