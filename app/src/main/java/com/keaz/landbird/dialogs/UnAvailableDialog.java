package com.keaz.landbird.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.keaz.landbird.R;

/**
 * Created by Administrator on 21/2/2017.
 */

public class UnAvailableDialog extends Dialog {
    private final Context mContext;
    private Dialog dialog;
    private OnUnAvailableListener listener;

    public UnAvailableDialog(Context context) {
        super(context);
        mContext = context;
        dialog = new Dialog(mContext);
    }

    public void show() {
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_un_available);
        WindowManager.LayoutParams layoutParam = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        layoutParam.copyFrom(window.getAttributes());
        layoutParam.width = WindowManager.LayoutParams.MATCH_PARENT;
        layoutParam.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setAttributes(layoutParam);

        dialog.findViewById(R.id.btnGotIt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });
        dialog.show();
    }

    public void setOnUnAvailableListener(OnUnAvailableListener listener) {
        this.listener = listener;
    }

    public interface OnUnAvailableListener {
        void onOK();
    }
}
