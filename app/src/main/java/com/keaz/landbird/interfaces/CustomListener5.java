package com.keaz.landbird.interfaces;

import com.keaz.landbird.models.KZCostCentre;
import com.keaz.landbird.utils.BKNetworkResponseError;

import java.util.List;

/**
 * Created by anhtran1810 on 4/2/18.
 */

public interface CustomListener5 {
    void actionBeforeCallApi();

    void actionCallApiSuccess(List<KZCostCentre> costCentres);

    void actionCallApiFail(BKNetworkResponseError error);
}
