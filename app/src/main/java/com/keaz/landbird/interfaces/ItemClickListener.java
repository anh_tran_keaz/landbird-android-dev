package com.keaz.landbird.interfaces;

import com.keaz.landbird.models.KZBooking;

/**
 * Created by keaz on 10/17/17.
 */

public interface ItemClickListener {
    void onClick(KZBooking booking);
}
