package com.keaz.landbird.interfaces;

/**
 * Created by anhtran1810 on 12/5/17.
 */

public interface CustomAnimationListener {
    void onAnimationEnd();
}
