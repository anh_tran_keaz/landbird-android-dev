package com.keaz.landbird.interfaces;

import com.keaz.landbird.utils.BKNetworkResponseError;

/**
 * Created by keaz on 10/16/17.
 */

public interface CustomListener2 {
    void success();

    void fail(BKNetworkResponseError error);
}
