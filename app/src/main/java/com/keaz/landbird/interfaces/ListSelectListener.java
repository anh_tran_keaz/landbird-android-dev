package com.keaz.landbird.interfaces;

import android.content.DialogInterface;

public interface ListSelectListener {
    void action(DialogInterface dialog, String selectionResult, int selection);
}
