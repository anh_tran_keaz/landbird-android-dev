package com.keaz.landbird.interfaces;

import com.keaz.landbird.utils.BKNetworkResponseError;

public interface CustomListener6 {
    void success(Object o);

    void fail(BKNetworkResponseError error);
}
