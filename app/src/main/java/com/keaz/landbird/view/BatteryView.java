package com.keaz.landbird.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.View;

/*
 * Created by Administrator on 21/2/2017.
 */

public class BatteryView extends View {
    private int mWidth;
    private int mHeight;
    private int mPercent = -1;
    private Paint greenPaint;
    private Paint grayPaint;

    public BatteryView(Context context) {
        super(context);
    }

    public BatteryView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BatteryView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        //TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.BatteryView);
        //mHeight = typedArray.getDimensionPixelOffset(R.styleable.BatteryView_bHeight, mHeight);
        //mWidth = typedArray.getDimensionPixelOffset(R.styleable.BatteryView_bWidth, mWidth);
        //mPercent = typedArray.getDimensionPixelOffset(R.styleable.BatteryView_bPercent, mPercent);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public BatteryView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected synchronized void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        mWidth = MeasureSpec.getSize(widthMeasureSpec);
        mHeight = MeasureSpec.getSize(heightMeasureSpec);
        setMeasuredDimension(mWidth, mHeight);
    }

    @Override
    protected synchronized void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        greenPaint = new Paint();
        greenPaint.setColor(Color.parseColor("#66B079"));
        greenPaint.setStyle(Paint.Style.FILL);

        grayPaint = new Paint();
        grayPaint.setColor(Color.parseColor("#eeeeee"));
        grayPaint.setStyle(Paint.Style.FILL);

        if (mPercent != -1) {
            int percent = (mWidth * mPercent) / 100;
            canvas.drawRect(0, 0, percent, mHeight, greenPaint);
            canvas.drawRect(percent, 0, mWidth, mHeight, grayPaint);
        } else {
            canvas.drawRect(0, 0, mWidth, mHeight, grayPaint);
        }
    }

    public void drawByPercent(int percent) {
        mPercent = percent;
        invalidate();
    }
}
