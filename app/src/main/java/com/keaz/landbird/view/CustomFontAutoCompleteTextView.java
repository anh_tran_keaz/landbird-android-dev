package com.keaz.landbird.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.AppCompatAutoCompleteTextView;
import android.util.AttributeSet;

import com.keaz.landbird.R;
import com.keaz.landbird.utils.FontUtil;

/**
 * Created by Kousei on 02/06/16.
 */
public class CustomFontAutoCompleteTextView extends AppCompatAutoCompleteTextView {

    public CustomFontAutoCompleteTextView(Context context) {
        super(context);

        applyCustomFont(context, null);
    }

    public CustomFontAutoCompleteTextView(Context context, AttributeSet attrs) {
        super(context, attrs);

        applyCustomFont(context, attrs);
    }

    public CustomFontAutoCompleteTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        applyCustomFont(context, attrs);
    }

    public void applyCustomFont(Context context, AttributeSet attrs) {
        TypedArray attributeArray = context.obtainStyledAttributes(attrs, R.styleable.CustomFontTextView);
        String fontName = attributeArray.getString(R.styleable.CustomFontTextView_custom_font);
        FontUtil.setFont(context, fontName, this);
        attributeArray.recycle();
    }

}
