package com.keaz.landbird.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.View;

import com.keaz.landbird.R;
import com.keaz.landbird.models.KZTimeObject;
import com.keaz.landbird.utils.BKGlobals;
import com.keaz.landbird.utils.MyLog;
import com.keaz.landbird.utils.Util;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/*
 * Custom view that draws a vertical time "ruler" representing the chronological
 * progression of a single day. Usually shown along with {@link "BlockView"}
 * instances to give a spatial sense of time.
 */
public class TimeRulerView extends View {

    private static final String TAG = TimeRulerView.class.getSimpleName();
    private int mHeaderHeight = Util.convertDp2Px(getContext(),100);
    private int mHourHeight = Util.convertDp2Px(getContext(),90);
    private int mHourWidth = Util.convertDp2Px(getContext(),80);
    private boolean mHorizontalDivider = true;
    private int mLabelTextSize = Util.convertDp2Px(getContext(),15);
    private int mLabelPaddingLeft = 0;
    private int mLabelColor = Color.BLACK;
    private int mDividerColor = Color.LTGRAY;
    private int mDividerColorSmall = Color.LTGRAY;
    private int mStartHour = 0;
    private int mEndHour = 24;
    private Paint mDividerPaint = new Paint();
    private Paint mDividerPaintSmall = new Paint();
    private Paint mLabelPaint = new Paint();
    private Paint mGreenPaint = new Paint();
    //private int lineStartX = -1;
    private int lineCenterX = -1;
    private int lineEndX = -1;
    private int pixelStart = 0;
    public int pixelEnd = 0;
    private Paint grayPaint = new Paint();
    private boolean selected = false;
    private ArrayList<Pixel> pixelArrayList,unAvailablePixelArrayList;
    private boolean isDrawing = false;
    private int currentHour = 0;

    public TimeRulerView(Context context) {
        this(context, null);
    }

    public TimeRulerView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TimeRulerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        final TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.TimeRulerView,
                defStyle, 0);

        mHeaderHeight = a.getDimensionPixelSize(R.styleable.TimeRulerView_headerHeight,
                mHeaderHeight);
        mHourHeight = a
                .getDimensionPixelSize(R.styleable.TimeRulerView_hourHeight, mHourHeight);
        mHourWidth = a
                .getDimensionPixelSize(R.styleable.TimeRulerView_hourWidth, mHourWidth);
        mHorizontalDivider = a.getBoolean(R.styleable.TimeRulerView_horizontalDivider,
                mHorizontalDivider);
        mLabelTextSize = a.getDimensionPixelSize(R.styleable.TimeRulerView_labelTextSize,
                mLabelTextSize);
        mLabelPaddingLeft = a.getDimensionPixelSize(R.styleable.TimeRulerView_labelPaddingLeft,
                mLabelPaddingLeft);
        mLabelColor = a.getColor(R.styleable.TimeRulerView_labelColor, mLabelColor);
        mDividerColor = a.getColor(R.styleable.TimeRulerView_dividerColor, mDividerColor);
        mDividerColorSmall = a.getColor(R.styleable.TimeRulerView_dividerColorSmall, mDividerColorSmall);
        mStartHour = a.getInt(R.styleable.TimeRulerView_startHour, mStartHour);
        mEndHour = a.getInt(R.styleable.TimeRulerView_endHour, mEndHour);

        a.recycle();
    }

    public void setIsDrawn(boolean isDrawn) {
        isDrawing = isDrawn;
    }

    public boolean getIsDrawn() {
        MyLog.debug(TAG, "isDrawing: " + isDrawing);
        return isDrawing;
    }

    @Override
    protected synchronized void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        final int hours = mEndHour - mStartHour;
        int width = mHourWidth * hours + mHourWidth/4;
        int height = mHeaderHeight;

        setMeasuredDimension(resolveSize(width, widthMeasureSpec),
                resolveSize(height, heightMeasureSpec));
    }
    @Override
    protected synchronized void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        final int hourWidth = mHourWidth;

        final Paint dividerPaint = mDividerPaint;
        if (!selected) {
            dividerPaint.setColor(Color.rgb(26, 26, 26));
        } else {
            dividerPaint.setColor(Color.rgb(94, 94, 96));
        }
        dividerPaint.setStyle(Style.FILL);

        final Paint dividerPaintSmall = mDividerPaintSmall;
        if (!selected) {
            dividerPaintSmall.setColor(Color.rgb(102, 102, 102));
        } else {
            dividerPaintSmall.setColor(Color.rgb(255, 255, 255));
        }
        dividerPaintSmall.setStyle(Style.FILL);

        final Paint labelPaint = mLabelPaint;
        if (!selected) {
            labelPaint.setColor(Color.rgb(26, 26, 26));
        } else {
            labelPaint.setColor(Color.rgb(255, 255, 255));
        }
        labelPaint.setTextSize(mLabelTextSize);
        labelPaint.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/roboto-regular.ttf"));
        labelPaint.setAntiAlias(true);

        final Paint greenPaint = mGreenPaint;
        greenPaint.setColor(Color.rgb(66, 229, 141));
        greenPaint.setStyle(Style.FILL);
        greenPaint.setStrokeWidth(2);

        final Paint yellowPaint = new Paint();
        yellowPaint.setColor(Color.rgb(255, 255, 0));
        yellowPaint.setStyle(Style.FILL);
        yellowPaint.setStrokeWidth(2);


        if (!selected) {
            grayPaint.setColor(Color.rgb(221, 221, 221));
        } else {
            grayPaint.setColor(Color.rgb(77, 77, 77));
        }
        grayPaint.setStyle(Style.FILL);
        int dp_20 = Util.convertDp2Px(getContext(), 20);
        int dp_10 = Util.convertDp2Px(getContext(), 8);

        // Walk left side of canvas drawing timestamps
        String label = String.valueOf("00AM");
        Rect result = new Rect();
        labelPaint.getTextBounds(label, 0, label.length(), result);
        // padding: Padding of label (pre define) + label rectangle's bound / 2
        int padding = mLabelPaddingLeft + result.width() / 2;
        int startY = mHeaderHeight - (result.height() * 3);
        /*
         * Draw booking time
         * */
        if (pixelArrayList != null) {
            for (int i = 0; i < pixelArrayList.size(); i++) {
                int start = pixelArrayList.get(i).start;
                int end = pixelArrayList.get(i).end;
                if (start != 0 && end != 0) {
                    canvas.drawRect(start + padding, dp_20, end + padding, startY, grayPaint);
                }
            }
        }
        /*
         * Draw unavailable time
         * */
        if (unAvailablePixelArrayList != null) {
            for (int i = 0; i < unAvailablePixelArrayList.size(); i++) {
                int start = unAvailablePixelArrayList.get(i).start;
                int end = unAvailablePixelArrayList.get(i).end;
                if (start != end) {
                    canvas.drawRect(start + padding, dp_20, end + padding, startY, grayPaint);
                }
            }
        }

        if (lineCenterX != -1) {
            canvas.drawLine(lineCenterX + padding, startY, lineCenterX + padding, dp_20, yellowPaint);
            canvas.drawCircle(lineCenterX + padding, dp_10, dp_10, yellowPaint);
        }

        if (lineEndX != -1) {
            canvas.drawLine(lineEndX + padding, startY, lineEndX + padding, dp_20, greenPaint);
            canvas.drawCircle(lineEndX + padding, dp_10, dp_10, greenPaint);
            canvas.drawRect(lineCenterX + padding, dp_20, lineEndX + padding, startY, greenPaint);
        }

        final int hours = mEndHour - mStartHour;
        for (int i = 0; i <= hours; i++) {
            final int dividerX = hourWidth * i;
            int j = currentHour + i;
            if (j >= 24) {
                j = j-24;
            }
            /*if (j < 10) {
                label = "0" + j;
            } else {
                label = "" + j;
            }*/
            //Change the label
            if(j<12){
                label = String.format("%02d",j)+"AM";
            }else if(j>12){
                label = String.format("%02d",(j-12))+"PM";
            }else if(j==12){
                label = "12PM";
            }

            result = new Rect();
            labelPaint.getTextBounds(label, 0, label.length(), result);
            startY = mHeaderHeight - (result.height() * 3);

            canvas.drawText(label, 0, label.length(), dividerX + mLabelPaddingLeft, mHeaderHeight - result.height(), labelPaint);
            // Draw main line
            int centerText = result.width() / 2;
            int startX = dividerX + mLabelPaddingLeft + centerText;
            canvas.drawLine(startX, startY, startX, dp_20, dividerPaint);
            if (i==hours) return;

            //Draw short white line
            for (int k = 0; k < 3; k++) {
                int offSetX = (hourWidth / 4) * (k + 1);
                canvas.drawLine(startX + offSetX, startY, startX + offSetX, startY / 2 + (dp_20/2), dividerPaint);
            }
        }
    }

    public void reDraw(int oldX, int newX) {
        if (oldX == -1) return;
        if (newX >= getWidth()) {
            return;
        }
        lineCenterX = normalizePixel(oldX);

        if (newX == -1) return;
        lineEndX = normalizePixel(newX);
        setIsDrawn(true);
        invalidate();
    }

    public void reDraw(int hhStart, int minStart, int hhEnd, int minEnd) {
        lineCenterX = timeToPixel(hhStart, minStart);
        lineEndX = timeToPixel(hhEnd, minEnd);
        setIsDrawn(true);
        invalidate();
    }

     public int normalizePixel(int pixel) {
        if (pixel == 0) return 0;
        int sectorWidth = mHourWidth / 4;
        int temp = pixel / sectorWidth;
        return temp * sectorWidth;
    }

    public void clear() {
        lineEndX = -1;
        lineCenterX = -1;
        currentHour = 0;
        setIsDrawn(false);
        invalidate();
    }

    public int minuteToPixel(int minutes) {
        int width = mHourWidth * mEndHour;
        int pixel = ((minutes * width) / (mEndHour * 60));
        return pixel;
    }

    public void drawBookedCarTime(Date dateStart, Date dateEnd) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dateStart);
        int hh_start = calendar.get(Calendar.HOUR_OF_DAY);

        hh_start = normalizeHour(hh_start);

        int mm_start = calendar.get(Calendar.MINUTE);
        int minuteStart = (hh_start * 60) + mm_start;

        calendar.setTime(dateEnd);
        int hh_end = calendar.get(Calendar.HOUR_OF_DAY);

        hh_end = normalizeHour(hh_end);

        int mm_end = calendar.get(Calendar.MINUTE);
        int minuteEnd = (hh_end * 60) + mm_end;

        int total = mEndHour * 60;
        final int hours = mEndHour - mStartHour;
        int width = mHourWidth * hours;
        if (hh_start == 0 && hh_end == 0) {
            minuteStart = 0;
            minuteEnd = 0;
        }
        pixelStart = (minuteStart * width) / total;
        pixelEnd = (minuteEnd * width) / total;
        pixelStart = normalizePixel(pixelStart);
        pixelEnd = normalizePixel(pixelEnd);
        pixelArrayList.add(new Pixel(pixelStart, pixelEnd));
    }

    public void drawListBookedCarTime(ArrayList<KZTimeObject> timeObjects) {
        pixelArrayList = new ArrayList<>();
        for (int i = 0; i < timeObjects.size(); i++) {
            drawBookedCarTime(timeObjects.get(i).getStart(), timeObjects.get(i).getEnd());
        }
        invalidate();
    }

    public void clearBookedCarTime() {
        if (pixelArrayList != null) {
            pixelArrayList.clear();
            invalidate();
        }
    }

    public void drawUnavailableTime(Date dateStart, Date dateEnd) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dateStart);
        int hh_start = calendar.get(Calendar.HOUR_OF_DAY);

        hh_start = normalizeHour(hh_start);

        int mm_start = calendar.get(Calendar.MINUTE);
        int minuteStart = (hh_start * 60) + mm_start;

        calendar.setTime(dateEnd);
        int hh_end = calendar.get(Calendar.HOUR_OF_DAY);

        hh_end = normalizeHour(hh_end);

        int mm_end = calendar.get(Calendar.MINUTE);
        int minuteEnd = (hh_end * 60) + mm_end;

        int total = mEndHour * 60;
        final int hours = mEndHour - mStartHour;
        int width = mHourWidth * hours;
        if (hh_start == 0 && hh_end == 0) {
            minuteStart = 0;
            minuteEnd = 0;
        }
        pixelStart = (minuteStart * width) / total;
        pixelEnd = (minuteEnd * width) / total;
        pixelStart = normalizePixel(pixelStart);
        pixelEnd = normalizePixel(pixelEnd);
        MyLog.debug(TAG, "pixelEnd: " + pixelEnd);
        unAvailablePixelArrayList.add(new Pixel(pixelStart, pixelEnd));
    }

    public void drawUnavailableTime(ArrayList<KZTimeObject> timeObjects) {
        unAvailablePixelArrayList = new ArrayList<>();
        for (int i = 0; i < timeObjects.size(); i++) {
            drawUnavailableTime(timeObjects.get(i).getStart(), timeObjects.get(i).getEnd());
        }
        invalidate();
    }

    public void clearUnavailableTime() {
        if (unAvailablePixelArrayList != null) {
            unAvailablePixelArrayList.clear();
            invalidate();
        }
    }

    public void reDrawWithState(boolean selected) {
        this.selected = selected;
        invalidate();
    }

    public void drawCenterLine(int center) {
        lineCenterX = normalizePixel(center);
        MyLog.debug(TAG, "lineCenterX: " + lineCenterX);
        invalidate();
    }

    public void drawRepeatAtPixel(int scrollX, int count) {
        scrollX = normalizePixel(scrollX);
        int hour = BKGlobals.getSharedGlobals().getCurrentHour();
        if (hour != 0) {
            currentHour = hour - count;
        } else {
            currentHour = pixelToHour(scrollX);
        }

        MyLog.debug(TAG, "CURRENT HOUR: " + currentHour);
        BKGlobals.getSharedGlobals().setCurrentHour(currentHour);
        invalidate();
    }

    private int pixelToHour(int pixel) {
        int hour = (pixel / mHourWidth);
        return hour;
    }

    private int timeToPixel(int hh, int mm) {
        hh = normalizeHour(hh);
        int minutes = (hh * 60) + mm;
        minutes = minuteToPixel(minutes);
        minutes = normalizePixel(minutes);
        return minutes;
    }

    private int normalizeHour(int hour) {
        if (currentHour > hour) {
            hour = 24 - currentHour + hour;
        } else {
            hour = Math.abs(currentHour - hour);
        }
        return hour;
    }

    public void setCurrentSelectedTime(int hhStart, int minStart, int hhEnd, int minEnd) {
        if (getIsDrawn()) {
            hhStart = normalizeHour(hhStart);
            hhEnd = normalizeHour(hhEnd);
            int start = (hhStart * 60) + minStart;
            int end = (hhEnd * 60) + minEnd;

            start = (start * mHourWidth * mEndHour) / (60 * mEndHour);
            start = normalizePixel(start);
            end = (end * mHourWidth * mEndHour) / (60 * mEndHour);
            end = normalizePixel(end);
            lineCenterX = start;
            lineEndX = end;
            invalidate();
        }
    }

    public class Pixel {
        int start;
        int end;
        public Pixel(int start, int end) {
            this.start = start;
            this.end = end;
        }
    }

}