package com.keaz.landbird.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.Button;

import com.keaz.landbird.R;
import com.keaz.landbird.utils.FontUtil;


/*
 * Created by Kousei on 02/06/16.
 */
public class CustomFontButton extends Button {

    public CustomFontButton(Context context) {
        super(context);

        applyCustomFont(context, null);
    }

    public CustomFontButton(Context context, AttributeSet attrs) {
        super(context, attrs);

        applyCustomFont(context, attrs);
    }

    public CustomFontButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        applyCustomFont(context, attrs);
    }

    public void applyCustomFont(Context context, AttributeSet attrs) {
        TypedArray attributeArray = context.obtainStyledAttributes(attrs, R.styleable.CustomFontTextView);
        String fontName = attributeArray.getString(R.styleable.CustomFontTextView_custom_font);
        FontUtil.setFont(context, fontName, this);
        attributeArray.recycle();
    }

}