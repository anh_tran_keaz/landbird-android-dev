package com.keaz.landbird.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.keaz.landbird.R;
import com.keaz.landbird.interfaces.CustomTabSelected;


/*
 * Created by TamTran on 17/02/2017.
 */

public class CustomBottomTab extends LinearLayout implements View.OnClickListener {

    private View view;
    private Context context;
    private String[] label;
    private int[][] icons;

    public CustomBottomTab(Context context) {
        super(context);
        initUI(context);
    }

    public CustomBottomTab(Context context, AttributeSet attrs) {
        super(context, attrs);
        initUI(context);
    }

    public CustomBottomTab(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initUI(context);
    }
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public CustomBottomTab(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initUI(context);
    }

    public void initUI(Context context) {
        LayoutInflater inflater = LayoutInflater.from(context);
        inflater.inflate(R.layout.view_custom_bottom_tab, this, true);
        this.view = findViewById(R.id.layout_bottom_tab);
        this.context = context;

    }

    public void initData(final Context context, String[] label, int[][] icons, int selected, final CustomTabSelected customTabSelected) {
        this.label = label;
        this.icons = icons;
        view.findViewById(R.id.tab1).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                updateView(context, 1);
                customTabSelected.select(1);
            }
        });
        view.findViewById(R.id.tab2).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                updateView(context, 2);
                customTabSelected.select(2);
            }
        });
        view.findViewById(R.id.tab3).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                updateView(context, 3);
                customTabSelected.select(3);
            }
        });
        view.findViewById(R.id.tab4).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                updateView(context, 4);
                customTabSelected.select(4);
            }
        });
        view.findViewById(R.id.tab5).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                updateView(context, 5);
                customTabSelected.select(5);
            }
        });
        updateView(context, selected);
    }

    private void updateView(Context context, int selected) {
        ((CustomViewTab)view.findViewById(R.id.tab1)).initData(context, label[0], icons[0], selected==1);
        ((CustomViewTab)view.findViewById(R.id.tab2)).initData(context, label[1], icons[1], selected==2);
        ((CustomViewTab)view.findViewById(R.id.tab3)).initData(context, label[2], icons[2], selected==3);
        ((CustomViewTab)view.findViewById(R.id.tab4)).initData(context, label[3], icons[3], selected==4);
        ((CustomViewTab)view.findViewById(R.id.tab5)).initData(context, label[4], icons[4], selected==5);
        invalidate();
    }

    @Override
    public void onClick(View v) {
    }

    public void selectTab( int i) {
        updateView(context, i);
    }

    public void disable(int i) {
        switch (i){
            case 1:
                view.findViewById(R.id.tab1).setEnabled(false);
                ((CustomViewTab)view.findViewById(R.id.tab1)).disable(context);
                break;
        }
    }

    public void enable(int i) {
        switch (i){
            case 1:
                view.findViewById(R.id.tab1).setEnabled(true);
                ((CustomViewTab)view.findViewById(R.id.tab1)).enable(context);
                break;
        }
    }

    public void notifyTab(int i, int count) {
        switch (i){
            case 2:
                ((CustomViewTab)view.findViewById(R.id.tab2)).notifyTab(count);
                break;
        }
    }
}
