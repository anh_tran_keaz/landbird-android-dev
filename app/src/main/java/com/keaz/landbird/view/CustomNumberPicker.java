package com.keaz.landbird.view;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.NumberPicker;

import com.keaz.landbird.utils.FontUtil;

/*
 * Created by Administrator on 18/2/2017.
 */

public class CustomNumberPicker extends NumberPicker {
    public CustomNumberPicker(Context context) {
        super(context);
    }

    public CustomNumberPicker(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomNumberPicker(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public CustomNumberPicker(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    public void addView(View child) {
        super.addView(child);
        applyCustomFont(child);
    }

    @Override
    public void addView(View child, int index) {
        super.addView(child, index);
        applyCustomFont(child);
    }

    @Override
    public void addView(View child, int index, ViewGroup.LayoutParams params) {
        super.addView(child, index, params);
        applyCustomFont(child);
    }

    @Override
    public void addView(View child, ViewGroup.LayoutParams params) {
        super.addView(child, params);
        applyCustomFont(child);
    }

    @Override
    public void addView(View child, int width, int height) {
        super.addView(child, width, height);
        applyCustomFont(child);
    }

    private void applyCustomFont(View view) {
        if (view instanceof EditText) {
            ((EditText) view).setTypeface(FontUtil.SF_Regular(getContext()));
        }
    }
}
