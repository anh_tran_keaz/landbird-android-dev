package com.keaz.landbird.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.keaz.landbird.R;


/*
 * Created by TamTran on 17/02/2017.
 */

public class CustomViewTabCenter extends RelativeLayout implements View.OnClickListener {

    private View view;
    private Context context;

    public CustomViewTabCenter(Context context) {
        super(context);
        initUI(context);
    }

    public CustomViewTabCenter(Context context, AttributeSet attrs) {
        super(context, attrs);
        initUI(context);
    }

    public CustomViewTabCenter(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initUI(context);
    }
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public CustomViewTabCenter(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initUI(context);
    }

    public void initUI(Context context) {
        LayoutInflater inflater = LayoutInflater.from(context);
        inflater.inflate(R.layout.view_custom_tab_item_center, this, true);
        this.view = findViewById(R.id.layout_tab);
        this.context = context;

    }

    public void initData(Context context, String label, int[] icons, boolean selected) {
        ((ImageView)view.findViewById(R.id.imvTabIcon)).setImageResource(selected?icons[0]:icons[1]);
        ((TextView)view.findViewById(R.id.tabLabel)).setText(label);
        ((TextView)view.findViewById(R.id.tabLabel)).setTextColor(context.getResources().getColor(selected?R.color.geoTab_tvBrandColor :R.color.tvTabUnselected));
    }

    public void initData() {

    }

    @Override
    public void onClick(View v) {
    }

    public void reset() {

    }
}
