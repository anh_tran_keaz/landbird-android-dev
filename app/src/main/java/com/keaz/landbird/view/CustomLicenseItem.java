package com.keaz.landbird.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.keaz.landbird.R;
import com.nostra13.universalimageloader.core.ImageLoader;

/*
 * Created by TamTran on 17/02/2017.
 */

public class CustomLicenseItem extends LinearLayout implements View.OnClickListener {

    private static final String TAG = "CustomLicenseItem";
    private ImageView imgLicense;
    private CustomFontTextView tvLicenseTitle;
    private CustomFontTextView tvLicenseDesc;
    private LinearLayout llImgLicenseContainer;
    private boolean isVerified = false;
    private int id;
    private int defaultId;
    private String url;
    private OnUploadLicenseListener listener;
    private LinearLayout llRightPart;

    private boolean isEnable = true;

    public CustomLicenseItem(Context context) {
        super(context);
        initUI(context);
    }

    public CustomLicenseItem(Context context, AttributeSet attrs) {
        super(context, attrs);
        initUI(context);
    }

    public CustomLicenseItem(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initUI(context);
    }
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public CustomLicenseItem(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initUI(context);
    }

    public ImageView getImgLicense() {
        return imgLicense;
    }

    public CustomFontTextView getTvLicenseTitle() {
        return tvLicenseTitle;
    }

    public CustomFontTextView getTvLicenseDesc() {
        return tvLicenseDesc;
    }

    public LinearLayout getLlImgLicenseContainer() {
        return llImgLicenseContainer;
    }

    public void initUI(Context context) {
        LayoutInflater inflater = LayoutInflater.from(context);
        inflater.inflate(R.layout.item_driver_license, this, true);

        llImgLicenseContainer = (LinearLayout) findViewById(R.id.llImageContainer);
        imgLicense = (ImageView) findViewById(R.id.img_license);
        llRightPart = (LinearLayout)findViewById(R.id.ll_rightPart);
        tvLicenseDesc = (CustomFontTextView) findViewById(R.id.tv_lisence_desc);
        tvLicenseTitle = (CustomFontTextView) findViewById(R.id.tv_license_title);
        tvLicenseTitle.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);

        imgLicense.setOnClickListener(this);
        llRightPart.setOnClickListener(this);
    }

    public void initData(int imgId, int titleId, int desId) {
        defaultId = imgId;
        imgLicense.setImageResource(imgId);
        tvLicenseTitle.setText(getContext().getString(titleId));
        tvLicenseDesc.setText(getContext().getString(desId));
    }

    public void setOnUploadLicenseListener(OnUploadLicenseListener listener, int viewId) {
        this.listener = listener;
        this.id = viewId;
    }

    @Override
    public void onClick(View v) {
        if (isEnable == false)
            return;

        if (v.getId() == imgLicense.getId() || v.getId() == llRightPart.getId()) {
            if (listener != null) {
                listener.onUploadLicenseListener(this.id);
            }
        }

    }

    public void updateLicenseImage(String url) {
        this.url = url;

        if (url.equals("")) {
            imgLicense.setImageResource(defaultId);
        } else {
            ImageLoader.getInstance().displayImage("file://" + url, imgLicense);
        }
    }

    public ImageView getImageView() {
        return imgLicense;
    }

    public String getUrl() {
        return url;
    }

    public void updateVerifyLicense(boolean isVerified) {
        if (isVerified) {
            tvLicenseTitle.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.btn_check_box_checked, 0);
        } else {
            tvLicenseTitle.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        }
    }

    public interface OnUploadLicenseListener {
        void onUploadLicenseListener(int id);
    }
}
